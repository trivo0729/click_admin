﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="availability_pricing.aspx.cs" Inherits="CutAdmin.availability_pricing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
    <script src="Scripts/availability_pricing.js?v=1.0"></script>
    <%--<script src="Kendo/JS/jquery.min.js"></script>
    <script src="Kendo/JS/kendo.all.min.js"></script>

    <link href="Kendo/Styles/kendo.common.min.css" rel="stylesheet" />--%>
    <style>
        label span {
            margin: 0px;
            float: left
        }
    </style>
    <script type="text/javascript">
        var date = new Date();
        var time = new Date(date.getTime());
        time.setMonth(date.getMonth() + 1);
        time.setDate(0);
        // var days = time.getDate() > date.getDate() ? time.getDate() - date.getDate() : 0;
        var days = 10;
        $(document).ready(function () {
            $("#datepicker_start").datepicker({
                autoclose: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                onSelect: insertDepartureDate,
                minDate: "dateToday",
            });
            $("#datepicker_end").datepicker({
                dateFormat: "dd-mm-yy",
                autoclose: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
            });
            $("#datepicker_start").datepicker("setDate", new Date());
            var dateObject = $("#datepicker_start").datepicker('getDate', days);
            dateObject.setDate(dateObject.getDate() + days);
            $("#datepicker_end").datepicker("setDate", dateObject);
            var date = $("#datepicker_end").datepicker("setDate", dateObject);
            setTimeout(function () {
                GetInventory();
            }, 1000);
        });


    </script>
    <script>
        var splitMonth = "";
        var lastDay = "";
        function insertDepartureDate() {
            var StartSelDate = $("#datepicker_start").val();
            var splitDate = StartSelDate.split('-');
            var splitMonth = splitDate[1];
            var date = new Date(), y = date.getFullYear(), m = new Date().getMonth()
            var lastDay = new Date(y, splitMonth, 0);
            lastDay = moment(lastDay).format("DD-MM-YYYY")
            $("#datepicker_end").val(lastDay);
            GetInventory();
        }
        var endDate = new Date();
        var endDates = new Date();
        function DateDisable(chk) {
            var dateObject = $("#datepicker_end").datepicker('getDate', '+1d');

            if (chk == 1) {
                $("#datepicker_end").datepicker("destroy");
                $('#datepicker_end').datepicker({
                    dateFormat: 'DD-MM-YYYY',
                    minDate: endDate,
                    onSelect: function (date) {
                        DateDisable(2);
                    }
                });

            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h3 class="font24"><b>Availability Calander & Rates</b>
                <p class="button-group compact float-right">
                    <label for="Freesale_Id" class="button green-active">
                        <input type="radio" name="radio-buttons" id="Freesale_Id" onchange="GetInventory()" value="" checked>
                        Freesale
                    </label>
                    <label for="Allocation_Id" class="button green-active">
                        <input type="radio" name="radio-buttons" id="Allocation_Id" onchange="GetInventory()" value="">
                        Alotment
                    </label>
                    <label for="Allotmnet_Id" class="button green-active">
                        <input type="radio" name="radio-buttons" id="Allotmnet_Id" onchange="GetInventory()" value="">
                        Allocation
                    </label>
                </p>
            </h3>
            <hr />
        </hgroup>
        <div class="with-padding ">
            <div class="columns">
                <div class="three-columns button-height">
                    <span class="input">
                        <label for="" class="button grey-gradient glossy icon-chevron-thin-left" onclick="Previous();" title="Previous"></label>
                        <input type="text" id="datepicker_start" onchange="GetInventory()" class="input-unstyled dt1" name="datepicker_From" style="width: 70px" value="">
                    </span>
                    <span class="input">
                        <input type="text" id="datepicker_end" onchange="GetInventory()" class="input-unstyled dt2" name="datepicker_From" style="width: 70px" value="">
                        <label for="" class="button grey-gradient glossy icon-chevron-thin-right" onclick="Next();" title="Next"></label>
                    </span>
                </div>
                
                <div class="nine-columns button-height small-margin-bottom">
                    <details class="">
                        <summary class="pointer"><b>Update Inventory</b></summary>
                        <div class="columns">
                            <div class="five-columns no-margin-bottom">
                                <input type="radio" class="" name="avlupdate" id="freesaleupdate" value="">
                                <label for="freesaleupdate" class="label">Freesale</label>
                                <input type="radio" class="" name="avlupdate" id="allocationupdate" value="">
                                <label for="allocationupdate" class="label mid-margin-right">Allocation</label>
                                <input type="radio" class="" name="avlupdate" id="alotmentupdate" value="">
                                <label for="alotmentupdate" class="label mid-margin-right">Alotment</label>
                            </div>
                            <div class="six-columns no-margin-bottom">
                                <span style="display: ">
                                    <input type="radio" class="" name="startstop" id="startradio" value="">
                                    <label for="startradio" class="label">Open</label>
                                    <input type="radio" class="" name="startstop" id="stopradio" value="">
                                    <label for="stopradio" class="label mid-margin-right">Close</label>
                                    <a class="font9 strong pointer red" onclick="openModal()">Restrictions</a>
                                </span>
                                <span style="display:none ">Total
                                    <span class="input">                                        
                                        <input type="text" id="" class="input-unstyled dt2" name="" style="width: 60px" value="" >
                                        <label for="" class="button grey-gradient glossy" title="">Rooms</label>
                                    </span>                                    
                                    <a class="font9 strong pointer red" onclick="openModal()">Restrictions</a>
                                </span>
                                 <span style="display:none ">Total
                                    <span class="input">                                        
                                        <input type="text" id="" class="input-unstyled dt2" name="" style="width: 60px" value="" >
                                        <label for="" class="button grey-gradient glossy" title="">Rooms</label>
                                    </span>                                    
                                    <a class="font9 strong pointer red" onclick="openModal()">Restrictions</a>
                                </span>
                            </div>
                        </div>
                        <hr class="hline1" />
                        <div class="columns">
                            <div class="eight-columns">
                                <p class="no-margin-bottom">
                                    <input type="checkbox" class="checkbox" name="avlupdate" id="updatemon" value="">
                                    <label for="updatemon" class="label">Mon</label>
                                    <input type="checkbox" class="checkbox" name="avlupdate" id="updatetue" value="">
                                    <label for="updatetue" class="label mid-margin-right">Tue</label>
                                    <input type="checkbox" class="checkbox" name="avlupdate" id="updatewed" value="">
                                    <label for="updatewed" class="label mid-margin-right">Wed</label>
                                    <input type="checkbox" class="checkbox" name="avlupdate" id="updatethu" value="">
                                    <label for="updatethu" class="label mid-margin-right">Thu</label>
                                    <input type="checkbox" class="checkbox" name="avlupdate" id="updatefri" value="">
                                    <label for="updatefri" class="label mid-margin-right">Fri</label>
                                    <input type="checkbox" class="checkbox" name="avlupdate" id="updatesat" value="">
                                    <label for="updatesat" class="label mid-margin-right">Sat</label>
                                    <input type="checkbox" class="checkbox" name="avlupdate" id="updatesun" value="">
                                    <label for="updatesun" class="label mid-margin-right">Sun</label>
                                </p>
                            </div>
                            <div class="four-columns no-margin-bottom">
                                <a href="#" class="button compact">
                                    <span class="button-icon green-gradient glossy"><span class="icon-tick"></span></span>
                                    Update
                                </a>
                                <a href="#" class="button compact">
                                    <span class="button-icon red-gradient glossy"><span class="icon-cross"></span></span>
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </details>
                </div>
        </div>
            <%--<div class="eight-columns">
                                <h5 class="no-margin-bottom">Rate Update</h5>
                                <div class="columns">
                                    <div class="four-columns no-margin-bottom">
                                        <label class="label">Standard Room</label>
                                        <span class="input">
                                            <label for="" class="button green-gradient glossy" title="">AED</label>
                                            <input type="text" id="" class="input-unstyled dt2" name="" style="width: 60px" value="">
                                        </span>
                                    </div>
                                    <div class="four-columns no-margin-bottom">
                                        <label class="label">Superior Room</label>
                                        <span class="input">
                                            <label for="" class="button green-gradient glossy" title="">AED</label>
                                            <input type="text" id="" class="input-unstyled dt2" name="" style="width: 60px" value="">
                                        </span>
                                    </div>
                                    <div class="four-columns small-margin-bottom">
                                        <label class="label">Deluxe Sea View Room</label>
                                        <span class="input">
                                            <label for="" class="button green-gradient glossy" title="">AED</label>
                                            <input type="text" id="" class="input-unstyled dt2" name="" style="width: 60px" value="">
                                        </span>
                                    </div>
                                </div>
                                
                            </div>--%>

            <div id="table-scroll" class="table-scroll">
                <div class="respTable" id="div_tbl_InvList">
                </div>
            </div>
        </div>
        <%--<div class="with-padding">
            <div class="columns">
                <div class="avl-roomname">Superior Deluxe Room</div>
                <div class="avl-column"><input type="text" class="input-unstyled" value="99999" tabindex="1"><input class="checkbox" type="checkbox" /></div>
                <div class="avl-column"><input type="text" class="input-unstyled" value="99999" tabindex="1"></div>
                <div class="avl-column not-avl-room"><input type="text" class="input-unstyled" value="99999" tabindex="1"></div>
                <div class="avl-column"><input type="text" class="input-unstyled" value="99999" tabindex="1"></div>
                <div class="avl-column"><input type="text" class="input-unstyled" value="99999" tabindex="1"></div>
                <div class="avl-column not-avl-room"><input type="text" class="input-unstyled" value="99999" tabindex="1"></div>
                <div class="avl-column not-avl-room">1000</div>
                <div class="avl-column avl-room"><input type="text" class="input-unstyled" value="99999" tabindex="1"></div>
                <div class="avl-column avl-room"><input type="text" class="input-unstyled" value="99999" tabindex="1"></div>
                <div class="avl-column avl-room"><input type="text" class="input-unstyled" value="99999" tabindex="1"></div>
                
                
            </div>
        </div>--%>
    </section>
    <script>
        function openModal(content, buttons) {
            $.modal({
                title: 'Modal window',
                content: content,
                buttons: buttons,
                beforeContent: '<div class="carbon">',
                afterContent: '</div>',
                buttonsAlign: 'center',
                resizable: false
            });
        }
    </script>
</asp:Content>
