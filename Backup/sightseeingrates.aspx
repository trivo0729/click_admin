﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="sightseeingrates.aspx.cs" Inherits="CutAdmin.sightseeingrates" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/styles/form.css?v=3">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
    <!-- DatePicker-->
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.css">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h3 class="font24"><b>Add rates for</b>
                <label id="">Dhow Cruise</label>
                </h3>
            <hr />
        </hgroup>
        <div class="with-padding" id="div_Main">
            <form method="post" action="#" class="block margin-bottom wizard same-height" id="">
                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Main</legend>
                    <div class="field-block button-height">
                        <small class="input-info">Select supplier for which these rates are being added / updated</small>
                        <label for="supplier" class="label"><b>Supplier</b></label>
                        <select name="country" class="select expandable-list loading" style="width: 180px" id="">
                            <option value="0">Rayna</option>
                            <option value="1">ClickUrTrip</option>                            
                        </select>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">Select in which currency you wish to add / update rates</small>
                        <label for="currency" class="label"><b>Currency</b></label>
                        <%--Use dropdown--%>
                        <select name="Exchange" class="select multiple-as-single expandable-list input validate[required]" style="width: 180px" id="SelCurrency" onchange="SetCurrency(this.value)">
                        </select>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">Select one or more nationality / market for which rates are valid</small>
                        <label for="market" class="label "><b>Valid for market</b></label>
                        <%--Use dynamic dropdown--%>
                        <select name="country" class="select multiple-as-single easy-multiple-selection check-list expandable-list input validate[required]" id="sel_Country" multiple style="width: 180px">
                        </select>

                    </div>
                </fieldset>
                <fieldset class="wizard-fieldset fields-list">
                     <legend class="legend">Rates</legend>
                </fieldset>
            </form>
        </div>
    </section>
</asp:Content>
