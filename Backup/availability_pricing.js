﻿var arrDate = new Array();
var arrInventory = new Array();
var Startdt;
var Enddt;
var HotelCode = 0;
function GetInventory() {
    if (location.href.indexOf('?') != -1) {
        HotelCode = GetQueryStringParams('sHotelID');
    }
    Startdt = $('#datepicker_start').val();
    Enddt = $('#datepicker_end').val();
    var InventoryType = "FreeSale";
    if (Freesale_Id.checked) {
        InventoryType = "FreeSale";
        $("#AmM").hide();
        $("#btn_US").hide();
        $("#btn_St").show();
        $("#btn_SS").show();
    }
    if (Allocation_Id.checked) {
        InventoryType = "Allocation";
        $("#btn_US").show();
        $("#btn_SS").show();
        $("#btn_St").hide();
    }

    if (Allotmnet_Id.checked) {
        InventoryType = "Allotment";
        $("#btn_US").show();
        $("#btn_SS").hide();
        $("#btn_St").hide();

    }
    $.ajax({
        type: "Post",
        url: "../Inventoryhandler.asmx/GetInventory",
        data: JSON.stringify({ Startdt: Startdt, Enddt: Enddt, InventoryType: InventoryType, HotelCode: HotelCode }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrDate = result.ListDates;
                arrInventory = result.arrInventory;
                GenrateTable();
            }
        },
        error: function () {

        },
    });
}

function GenrateTable() {
    $("#div_tbl_InvList").empty();
    var html = '';
    html += '<table class="table responsive-table  colorTable responsive-table-on" id="tbl_HotelList">';
    html += '<thead>';
    html += '<tr>';
    html += '<th >Room Name <input type="hidden" id="txt_HotelCode"></th>';
    for (var i = 0; i < arrDate.length; i++) {
        html += '<th class="align-center header" class="align-center header">' + arrDate[i] + '</th>';
    }
    html += '</tr>';
    html += '</thead>';

    html += '<tbody>';
    //for (var i = 0; i < arrInventory.length; i++) {
    //    html += '<tr>';
    //    html += '<td style="width:20%; padding"><div class=" SelBox"><label>' + arrInventory[i].HotelName + '</label><input type="hidden" class="HotelCode" value="' + arrInventory[i].HotelCode + '" /></div></td>';
    //    for (var d = 0; d < arrDate.length; d++) {
    //        var Count = $.grep(arrInventory[i].Dates, function (h) { var yr = h.datetime.split('-')[2]; return h.datetime == arrDate[d] + "-" + yr })
    //                    .map(function (h) { return h.NoOfCount; })[0];
    //        var bCount = $.grep(arrInventory[i].Dates, function (h) { var yr = h.datetime.split('-')[2]; return h.datetime == arrDate[d] + "-" + yr })
    //                  .map(function (h) { return h.Type; })[0];
    //        html += '<td class="align-center header" class="align-center header"><input type="checkbox" class="checkbox Date"   value="' + arrDate[d] + '" onclick="SelectByDate(' + arrDate[d] + ')"/>' + bCount + '/' + Count + '</td>';
    //    }
    //    html += '</tr>';
    //}
    //html += '</tbody>';
    $("#txt_HotelCode").val(HotelCode)
    var arrRoom = $.grep(arrInventory, function (p) { return p.HotelCode == HotelCode; })
           .map(function (p) { return p.ListRoom; })[0];
    for (var i = 0; i < arrRoom.length; i++) {
        html += GetRoomDetails(arrRoom[i], i + HotelCode);
    }
    //html += '<tr class="row-drop"><td colspan="' + (arrDate.length + 1) + '"><div class="columns" style="overflow:scroll height:300px;margin-left:auto;margin-right:auto;"><div id="div_Dates0" class="twelve-columns  twelve-columns-mobile"> <div class="new-row coloumns"><div class="three-row"><b>Note:</b>  Availability  <label style=""><span class="wrapped green-gradient " style="margin-bottom: -10px;"></span></label>  No Availability <label style="margin-bottom: -22px;"><span class="margin-bottom wrapped red-gradient with-mid-padding align-center"></span></label></div> </div></div></div></td></tr>';
    html += '</tbody>';
    html += '</table>';
    $("#div_tbl_InvList").append(html)
    //$("#tbl_HotelList").append(html)
    $("#tbl_HotelList").dataTable({
        bSort: false,
        sPaginationType: 'full_numbers',
        sSearch: false,
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        fixedColumns: {
            leftColumns: 1,
        },
    });
    $("#tbl_HotelList_wrapper .dataTables_scrollBody table thead tr").addClass("hidden")
    //$("#tbl_HotelList_wrapper").addClass("respTable");

    $(".ChkRoom").click(function () {
        var ndRoom = $(this).hasClass("checked");
        var arrRate = $($(this)[0].childNodes[1]).val();
        if (ndRoom == false)
            $(".Room" + arrRate).addClass("checked");
        else
            $(".Room" + arrRate).removeClass("checked");
        debugger
    });
    $(".Date").click(function () {
        var ndRoom = $(this).hasClass("checked");
        var arrRate = $($(this)[0].childNodes[1]).val().split('-')[0] + '-' + $($(this)[0].childNodes[1]).val().split('-')[1];
        if (ndRoom == false)
            $("." + arrRate).addClass("checked");
        else
            $("." + arrRate).removeClass("checked");
        debugger
    });
    // Call template init (optional, but faster if called manually)

}

function dropdown1(RoomID) {
    //$().on('click', 'tbody', function (event) {
      

    //}).on('sortStart', function () {
    //    var rows = $(this).find('.row-drop');
    //    if (rows.length > 0) {
    //        rows.prev().children().removeClass('anthracite-gradient glossy');
    //        rows.remove();
    //    }
    //});
    var dropRoomId = this.id;
    // Do not process if something else has been clicked
    //if (event.target !== this) {
    //    return;
    //}
    var tr = $("#tbl_HotelList").children()[1].firstChild,
        row = tr.next('.row-drop'),
        rows;
    // If click on a special row
    if (tr.hasClass('row-drop')) {
        return;
    }
    // If there is already a special row
    if (row.length > 0) {
        // Un-style row
        tr.children().removeClass('anthracite-gradient glossy');
        // Remove row
        row.remove();
        $(".row-drop").remove();
        return;
    }
    // Remove existing special rows
    rows = tr.siblings('.row-drop');
    if (rows.length > 0) {
        // Un-style previous rows
        rows.prev().children().removeClass('anthracite-gradient glossy');
        // Remove rows
        rows.remove();

    }
    // Style row
    tr.children().addClass('anthracite-gradient glossy');
    $('<tr class="row-drop"><td colspan="' + arrDate.length + '"></td></tr>').insertAfter(tr);

    $('<tr class="row-drop Rate"  id="div_Rate' + dropRoomId + HotelCode + '" ></tr>').insertAfter(tr);
    GetRoomRates(dropRoomId, HotelCode, arrDate);

}

function GetRoomDetails(arrRoom, HotelCode) {
    try {
        var html = '';
        html += '<tr class="RateDetails"  id="div_Class' + arrRoom.RoomTypeId + '" >'
        html += '<th style="min-width:250px; padding:0 !important;">'
        //   html += '<div class="SelBox">'
        html += '<div class="SelBox">'           
        html += '<div class="font11 red with-small-padding" style = "border-bottom: .5px solid lightgray;">Availability</div>'
        html += '<div class="font11 red with-small-padding" style = "border-bottom: .5px solid lightgray;">' + arrRoom.RoomTypeName + '</div>'
        html += '<div class="font10 with-small-padding" style = "border-bottom: .5px solid lightgray;">Extra Bed</div>'
        html += '<div class="font10 with-small-padding" style = "border-bottom: .5px solid lightgray;">Child With Bed</div>'
        html += '<div class="font10 with-small-padding" style = "border-bottom: .5px solid lightgray;">Child No Bed</div>'
        html += '<div class="with-small-padding"> <span class="font9">Market</span>  <span class="font9">Contract</span></div>'
        //html += '<input type="checkbox" class="checkbox ChkRoom roomtype" name="ChkTotal" id="chk' + HotelCode + '" value="' + arrRoom.RoomTypeId + '" onclick="ChakedByRoom("' + HotelCode + '")">'
        //html += '<label class="font12 small-margin-left" for="chk' + HotelCode + '">' + arrRoom.RoomTypeName + '</label>'
        //html += ' <i class="icon-chevron-small-down" onclick="dropdown1(\'' + arrRoom.RoomTypeId + '\')"></i>'
        html += '</div></th>'
        for (var dt = 0; dt < arrRoom.dates.length; dt++) {
            var cssClass = "";
            if (arrRoom.dates[dt].InventoryType == "0")
                cssClass = "room-not-available";
            else
                cssClass = "room-available";
            if (cssClass != "") {
                var date = arrRoom.dates[dt].datetime.split('-')[0] + '-' + arrRoom.dates[dt].datetime.split('-')[1];
                if (arrRoom.dates[dt].InventoryType == "FreeSale") {
                    if (arrRoom.dates[dt].discount == "fs" || arrRoom.dates[dt].discount == "FS")
                        arrRoom.dates[dt].discount = "FS";
                    else
                        arrRoom.dates[dt].discount = "SS";
                    html += '<td class="" id = "' + arrRoom.dates[dt].RateTypeId + '">'
                    //html += '<span class="AddTxt">'
                    //  html += '<span>' + arrRoom.dates[dt].Type + '/' + arrRoom.dates[dt].discount + ' </span>'
                    //html += '<span style>' + arrRoom.dates[dt].Type + ' </span>'
                    //html += '<input type="checkbox" name="checkbox-2" id="checkbox-2" value="' + arrRoom.dates[dt].datetime + '-' + arrRoom.dates[dt].RateTypeId + '" class="checkbox avl-checkbox Room' + arrRoom.RoomTypeId + ' ' + date + '" tabindex="-1">'
                    //html += '<span> 99,999.99 </span>'
                    
                    html += '<div class="font11 with-small-padding align-center ' + cssClass + '" style = "border-bottom: .5px solid lightgray;">' + arrRoom.dates[dt].Type + '</div>'
                    html += '<div class="font11 with-small-padding" style = "border-bottom: .5px solid lightgray;"><input type="text" class="input-unstyled font11" size="6" value="99,999.99"></div>'
                    html += '<div class="font11 with-small-padding" style = "border-bottom: .5px solid lightgray;">99,999.99</div>'
                    html += '<div class="font11 with-small-padding" style = "border-bottom: .5px solid lightgray;">99,999.99</div>'
                    html += '<div class="font11 with-small-padding">99,999.99</div>'
                    html += '<div class="font11 with-small-padding white">.</div>'
                    html += '</td>'
                    //html += '</span></td>'
                    //</span></span></span></span>
                }

                else {
                    if (arrRoom.dates[dt].discount == "ss" || arrRoom.dates[dt].discount == "SS") {
                        arrRoom.dates[dt].discount = "SS";
                        html += '<td class="" id = "' + arrRoom.dates[dt].RateTypeId + '">'
                        //html += '<span class="AddTxt">'
                        html += '<div class="font11 with-small-padding align-center ' + cssClass + '" style = "border-bottom: .5px solid lightgray;">' + arrRoom.dates[dt].Type + '</div>'
                        html += '<div class="font11 with-small-padding" style = "border-bottom: .5px solid lightgray;">99,999.99</div>'
                        html += '<div class="font11 with-small-padding" style = "border-bottom: .5px solid lightgray;">99,999.99</div>'
                        html += '<div class="font11 with-small-padding" style = "border-bottom: .5px solid lightgray;">99,999.99</div>'
                        html += '<div class="font11 with-small-padding">99,999.99</div>'
                        html += '<div class="font11 with-small-padding white">.</div>'
                        html += '</td>'
                        // html += '<span>' + arrRoom.dates[dt].Type + '/' + arrRoom.dates[dt].discount + ' </span>'
                        //html += '<span>' + arrRoom.dates[dt].Type + '/' + arrRoom.dates[dt].discount + ' </span>'
                        //html += '<input type="checkbox" name="checkbox-2" id="checkbox-2" value="' + arrRoom.dates[dt].datetime + '-' + arrRoom.dates[dt].RateTypeId + '" class="checkbox avl-checkbox Room' + arrRoom.RoomTypeId + ' ' + date + '" tabindex="-1">'
                        //html += '<span>99,999.99</span>'
                        //html += '</span></td>'

                    }
                    else {
                        html += '<td class="" id = "' + arrRoom.dates[dt].RateTypeId + '">'
                    //html += '<span class="AddTxt">'
                    //if (arrRoom.dates[dt].InventoryType == "FreeSale" || arrRoom.dates[dt].InventoryType == "0")
                    //    html += '<span> ' + arrRoom.dates[dt].Type + '  </span>'
                    //else 
                        html += '<div class="font11 with-small-padding align-center ' + cssClass + '" style = "border-bottom: .5px solid lightgray;">' + arrRoom.dates[dt].Type + '/' + arrRoom.dates[dt].NoOfCount + '</div>'
                        html += '<div class="font11 with-small-padding" style = "border-bottom: .5px solid lightgray;"><input type="text" class="input-unstyled" value="99,999.99"></div>'
                        html += '<div class="font11 with-small-padding" style = "border-bottom: .5px solid lightgray;">99,999.99</div>'
                        html += '<div class="font11 with-small-padding" style = "border-bottom: .5px solid lightgray;">99,999.99</div>'
                        html += '<div class="font11 with-small-padding">99,999.99</div>'
                        html += '<div class="font11 with-small-padding white">.</div>'
                        html += '</td>'
                    }
                    //html += '<span> ' + arrRoom.dates[dt].Type + '/' + arrRoom.dates[dt].NoOfCount + '  </span>'
                    //html += '<input type="checkbox" name="checkbox-2" id="checkbox-2" value="' + arrRoom.dates[dt].datetime + '-' + arrRoom.dates[dt].RateTypeId + '" class="checkbox avl-checkbox Room' + arrRoom.RoomTypeId + ' ' + date + '" tabindex="-1">'
                    //html += '<span>99,999.99</span>'
                    //html += '</span></td>'
                }

            }

            else
                html += '<td>-</td> '
        }
        html += '</tr>'
        //$("#div_Class" + HotelCode).append(html)

    } catch (e) {

    }
    return html;
}


function GetRoomRates(dropRoomId, HotelCode, arrDate) {
    //try {
    //    var html = '';
    //    html += '<tr class="RateDetails"  id="div_Class' + HotelCode + '" >'
    //    html += '<th style="padding:20px 10px ;min-width:200px">'
    //    //   html += '<div class="SelBox">'
    //    html += '<div class="SelBox">'
    //    html += '<input type="checkbox" class="checkbox ChkRoom roomtype" name="ChkTotal" id="chk' + HotelCode + '" value="' + arrRoom.RoomTypeId + '" onclick="ChakedByRoom("' + HotelCode + '")">'
    //    html += '<label class="font12 small-margin-left" for="chk' + HotelCode + '">' + arrRoom.RoomTypeName + '</label>'
    //    html += '</div></th>'
    //    for (var dt = 0; dt < arrRoom.dates.length; dt++) {
    //        var cssClass = "";
    //        if (arrRoom.dates[dt].InventoryType == "0")
    //            cssClass = "sold";
    //        else
    //            cssClass = "available";
    //        if (cssClass != "") {
    //            var date = arrRoom.dates[dt].datetime.split('-')[0] + '-' + arrRoom.dates[dt].datetime.split('-')[1];
    //            if (arrRoom.dates[dt].InventoryType == "FreeSale") {
    //                if (arrRoom.dates[dt].discount == "fs" || arrRoom.dates[dt].discount == "FS")
    //                    arrRoom.dates[dt].discount = "FS";
    //                else
    //                    arrRoom.dates[dt].discount = "SS";
    //                html += '<td class="' + cssClass + '" id = "' + arrRoom.dates[dt].RateTypeId + '">'
    //                html += '<span class="AddTxt">'
    //                //  html += '<span>' + arrRoom.dates[dt].Type + '/' + arrRoom.dates[dt].discount + ' </span>'
    //                html += '<span>' + arrRoom.dates[dt].Type + ' </span>'
    //                html += '<input type="checkbox" name="checkbox-2" id="checkbox-2" value="' + arrRoom.dates[dt].datetime + '-' + arrRoom.dates[dt].RateTypeId + '" class="checkbox avl-checkbox Room' + arrRoom.RoomTypeId + ' ' + date + '" tabindex="-1">'
    //                html += '<span> 99,999.99 </span>'
    //                html += '</span></td>'
    //                //</span></span></span></span>
    //            }

    //            else {
    //                if (arrRoom.dates[dt].discount == "ss" || arrRoom.dates[dt].discount == "SS") {
    //                    arrRoom.dates[dt].discount = "SS";
    //                    html += '<td class="' + cssClass + '" id = "' + arrRoom.dates[dt].RateTypeId + '">'
    //                    html += '<span class="AddTxt">'
    //                    // html += '<span>' + arrRoom.dates[dt].Type + '/' + arrRoom.dates[dt].discount + ' </span>'
    //                    html += '<span>' + arrRoom.dates[dt].Type + '/' + arrRoom.dates[dt].discount + ' </span>'
    //                    html += '<input type="checkbox" name="checkbox-2" id="checkbox-2" value="' + arrRoom.dates[dt].datetime + '-' + arrRoom.dates[dt].RateTypeId + '" class="checkbox avl-checkbox Room' + arrRoom.RoomTypeId + ' ' + date + '" tabindex="-1">'
    //                    html += '<span>99,999.99</span>'
    //                    html += '</span></td>'
    //                }
    //                else
    //                    html += '<td class="' + cssClass + '" id = "' + arrRoom.dates[dt].RateTypeId + '">'
    //                html += '<span class="AddTxt">'
    //                if (arrRoom.dates[dt].InventoryType == "FreeSale" || arrRoom.dates[dt].InventoryType == "0")
    //                    html += '<span> ' + arrRoom.dates[dt].Type + '  </span>'
    //                else
    //                    html += '<span> ' + arrRoom.dates[dt].Type + '/' + arrRoom.dates[dt].NoOfCount + '  </span>'
    //                html += '<input type="checkbox" name="checkbox-2" id="checkbox-2" value="' + arrRoom.dates[dt].datetime + '-' + arrRoom.dates[dt].RateTypeId + '" class="checkbox avl-checkbox Room' + arrRoom.RoomTypeId + ' ' + date + '" tabindex="-1">'
    //                html += '<span>99,999.99</span>'
    //                html += '</span></td>'
    //            }

    //        }

    //        else
    //            html += '<td>-</td> '
    //    }
    //    html += '</tr>'
    //    //$("#div_Class" + HotelCode).append(html)

    //} catch (e) {

    //}
    //return html;
}

function ChakedByRoom(HotelCode) {
    $(".Room" + HotelCode).click();
}

function SelectByDate(Date) {
    $("." + Date).click();
}


function Next() {
    var days = 10;
    var StartDate = $("#datepicker_end").val();
    $("#datepicker_start").val(StartDate);
    var dateObject = $("#datepicker_end").datepicker('getDate', days);
    dateObject.setDate(dateObject.getDate() + days);
    var stDate = $("#datepicker_end").datepicker("setDate", dateObject);
    Startdt = StartDate;
    Enddt = $("#datepicker_end").val();
    GetInventory()
}

function Previous() {
    var days = -10;
    var StartDate = $("#datepicker_start").val();
    $("#datepicker_end").val(StartDate);
    var dateObject = $("#datepicker_start").datepicker('getDate', days);
    dateObject.setDate(dateObject.getDate() + days);
    var stDate = $("#datepicker_start").datepicker("setDate", dateObject);
    Startdt = StartDate;
    Enddt = $("#datepicker_start").val();
    GetInventory()

}




var HotelName;

function StartSaleModal() {
    ListRoom = new Array();
    var Room = $('.RateDetails');
    HotelName = $.grep(arrInventory, function (p) { return p.HotelCode == $("#txt_HotelCode").val(); })
                   .map(function (p) { return p.HotelName; })[0];
    if (Room.length != 0) {
        for (var c = 0; c < Room.length; c++) {
            var ndListDate = $(Room[c]).find(".checkbox");
            var arrDate = new Array();
            var sRoom = $($(ndListDate[0]).find("input:checkbox")).val().split('-')[0];
            var arrRoom = $.grep(arrInventory, function (p) { return p.HotelCode == $("#txt_HotelCode").val(); })
                   .map(function (p) { return p.ListRoom; })[0];
            var RoomName = $.grep(arrRoom, function (p) { return p.RoomTypeId == sRoom; })
                  .map(function (p) { return p.RoomTypeName; })[0];
            for (var i = 1; i < ndListDate.length; i++) {
                var sID = $($(ndListDate[i]).find("input:checkbox")).val().split('-')[3];
                var IsChecked = $(ndListDate[i]).hasClass("checked");
                if (IsChecked) {
                    var sDate = $($(ndListDate[i]).find("input:checkbox")).val();
                    sDate = sDate.split('-')[0] + '-' + sDate.split('-')[1] + '-' + sDate.split('-')[2];
                    arrDate.push(sDate)
                }
                else {
                    if (arrDate.length != 0)
                        ListRoom.push({ ID: sID, Name: RoomName, Date: arrDate });
                    arrDate = new Array();
                }

            }
            if (arrDate.length != 0)
                ListRoom.push({ ID: sID, Name: RoomName, Date: arrDate });
        }
    }

    if (ListRoom.length == 0) {
        Success("Please Select  Dates");
        return false;
    }
    $.modal.confirm(EditModal(), function () {
        UpdateInventory("Start Sale")

    }, function () {
        $('#modals').remove();
    });
}

function StopSaleModal() {
    ListRoom = new Array();
    var Room = $('.RateDetails');
    HotelName = $.grep(arrInventory, function (p) { return p.HotelCode == $("#txt_HotelCode").val(); })
                   .map(function (p) { return p.HotelName; })[0];
    if (Room.length != 0) {
        for (var c = 0; c < Room.length; c++) {
            var ndListDate = $(Room[c]).find(".checkbox");
            var arrDate = new Array();
            var sRoom = $($(ndListDate[0]).find("input:checkbox")).val().split('-')[0];
            var arrRoom = $.grep(arrInventory, function (p) { return p.HotelCode == $("#txt_HotelCode").val(); })
                   .map(function (p) { return p.ListRoom; })[0];
            var RoomName = $.grep(arrRoom, function (p) { return p.RoomTypeId == sRoom; })
                  .map(function (p) { return p.RoomTypeName; })[0];
            for (var i = 1; i < ndListDate.length; i++) {
                var sID = $($(ndListDate[i]).find("input:checkbox")).val().split('-')[3];
                var IsChecked = $(ndListDate[i]).hasClass("checked");
                if (IsChecked) {
                    var sDate = $($(ndListDate[i]).find("input:checkbox")).val();
                    sDate = sDate.split('-')[0] + '-' + sDate.split('-')[1] + '-' + sDate.split('-')[2];
                    arrDate.push(sDate)
                }
                else {
                    if (arrDate.length != 0)
                        ListRoom.push({ ID: sID, Name: RoomName, Date: arrDate });
                    arrDate = new Array();
                }

            }
            if (arrDate.length != 0)
                ListRoom.push({ ID: sID, Name: RoomName, Date: arrDate });
        }
    }
    $.modal.confirm(EditModalStop(), function () {
        UpdateInventory("Stop Sale")

    }, function () {
        $('#modals').remove();
    });
}

function EditModalStop() {
    arrRate = new Array();
    var tab = '';
    tab += 'Please Confirm Selected Dates & Room For Stop Sale<br/><br/>' + HotelName + '<br/><br/>';
    tab += '<div class="respTable">'
    tab += '<table class="table responsive-table evenodd" id="sorting-advanced">'
    for (var i = 0; i < ListRoom.length; i++) {
        tab += '<tr><td class="even">' + ListRoom[i].Name + '</td><td>' + ListRoom[i].Date[0] + '  to ' + ListRoom[i].Date[ListRoom[i].Date.length - 1] + '</td></tr>'
    }
    tab += '</tr>'
    '</table>'
    tab += '<div>'

    return tab;
}

function EditModal() {
    arrRate = new Array();
    var tab = '';
    tab += 'Please Confirm Selected Dates & Room For Start Sale<br/><br/>' + HotelName + '<br/><br/>';
    tab += '<div class="respTable">'
    tab += '<table class="table responsive-table evenodd" id="sorting-advanced">'
    for (var i = 0; i < ListRoom.length; i++) {
        tab += '<tr><td class="even">' + ListRoom[i].Name + '</td><td>' + ListRoom[i].Date[0] + '  to ' + ListRoom[i].Date[ListRoom[i].Date.length - 1] + '</td></tr>'
    }
    tab += '</tr>'
    '</table>'
    tab += '<div>'

    return tab;
}

function UpdateInventory(value) {

    $.ajax({
        type: "POST",
        url: "Inventoryhandler.asmx/UpdateInventory",
        data: JSON.stringify({ ListRoomRate: ListRoom, status: value }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Inventory Updated Successfully");
                GetInventory();

            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })

}