﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="RateList.aspx.cs" Inherits="CutAdmin.RateList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/RateList.js?v=3.6"></script>
    <script src="Scripts/AddRate2.js?v=1.6"></script>
    <script src="Scripts/Rooms.js"></script>
    <!-- Additional styles -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.fixed.css?v=1" />
    <!-- DataTables -->
    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        
        <hgroup id="main-title" class="thin">
            <h1>Rate List</h1>
            <h2 id="div_btn"></h2> 
            <hr />
           </hgroup>
        <div class="with-padding">
            
            <span class="">
                <h3 id="lbl_Hotel" style="margin-bottom: 5px"></h3>
             <label id="lbl_address"></label>
            </span><br />
                      <br />
            <table class="table responsive-table" id="tbl_Ratelist" width="100%">

                <thead>
                    <tr>

                        <th align="center" scope="col">S.N </th>
                        <th align="center" scope="col">Season</th>
                        <th align="center" scope="col">MealPlan </th>
                        <th align="center" scope="col">Currency</th>
                        <th align="center" scope="col">Nationality </th>
                        <th align="center" scope="col">Min Stay </th>
                        <th align="center" scope="col">Max Stay </th>
                        <th align="center" scope="col">Min Room</th>
                    </tr>
                </thead>

            </table>
            <div class="three-columns twelve-columns-mobile SelBox" id="Currency" style="display: none">
                <label>Currency</label>
                <select id="sel_CurrencyCode" name="validation-select" class="select full-width">
                    <option value="-" selected="selected" disabled>Please select</option>
                </select>
            </div>

            <div id="filter" class="" style="display: none"></div>

        </div>

    </section>

    <script>

        // Call template init (optional, but faster if called manually)
        $.template.init();


        // Table sort - styled
        $('#tbl_Ratelist').tablesorter({
            headers: {
                0: { sorter: false },
                6: { sorter: false }
            }
        }).on('click', 'tbody td', function (event) {
            var drophotelId = this.id;

            // Do not process if something else has been clicked
            if (event.target !== this) {
                return;
            }

            var tr = $(this).parent(),
                row = tr.next('.row-drop'),
                rows;

            // If click on a special row
            if (tr.hasClass('row-drop')) {
                return;
            }

            // If there is already a special row
            if (row.length > 0) {

                // Un-style row
                tr.children().removeClass('anthracite-gradient glossy');

                // Remove row
                row.remove();

                return;
            }

            // Remove existing special rows
            rows = tr.siblings('.row-drop');
            if (rows.length > 0) {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }

            // Style row
            tr.children().addClass('anthracite-gradient glossy');


            // Add fake row
            $('<tr  class="row-drop">' +
                '<td  colspan="' + tr.children().length + '">' +
                '<div class="columns">' +
                '<div id="DropLeft"  class="five-columns"> </div>' +
                '<div id="Dropmiddle"  class="four-columns align-center"> </div>' +
                '<div id="DropRight"  class="three-columns"> </div>' +
                '</div>' +
                '</td>' +
                '</tr>').insertAfter(tr);
            DropRowwithId(drophotelId)
        }).on('sortStart', function () {
            var rows = $(this).find('.row-drop');
            if (rows.length > 0) {
                // Un-style previous rows
                rows.prev().children().removeClass('anthracite-gradient glossy');

                // Remove rows
                rows.remove();
            }
        });



    </script>

    <script>

        // Call template init (optional, but faster if called manually)
        $.template.init();

        // Color
        $('#anthracite-inputs').change(function () {
            $('#main')[this.checked ? 'addClass' : 'removeClass']('black-inputs');
        });

        // Switches mode
        $('#switch-mode').change(function () {
            $('#switch-wrapper')[this.checked ? 'addClass' : 'removeClass']('reversed-switches');
        });

        // Disabled switches
        $('#switch-enable').change(function () {
            $('#disabled-switches').children()[this.checked ? 'enableInput' : 'disableInput']();
        });

        // Tooltip menu
        $('#select-tooltip').menuTooltip($('#select-context').hide(), {
            classes: ['no-padding']
        });

        // Date picker
        $('.datepicker').glDatePicker({ zIndex: 100 });

        // Form validation
        $('form').validationEngine();

    </script>


</asp:Content>
