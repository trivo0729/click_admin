﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace CutAdmin.Models
{
    public class DTResult
    {
        public partial class data
        {
            public string Name { get; set; }
            public object Value { get; set; }
        }
        public static List<data> arrTable { get { return JsonConvert.DeserializeObject<List<data>>(HttpContext.Current.Request.Params["data"]); } }
        public static int iDisplayLength
        {
            get
            {
                return Convert.ToInt16(arrTable.Where(d => d.Name == "length").FirstOrDefault().Value);
            }
        }
        public static int iDisplayStart
        {
            get
            {
                return Convert.ToInt16(arrTable.Where(d => d.Name == "start").FirstOrDefault().Value);
            }
        }

        public static int iSortingCols
        {
            get
            {

                var order = JsonConvert.DeserializeObject<dynamic>(arrTable.Where(d => d.Name == "order").FirstOrDefault().Value.ToString());
                return Convert.ToInt16(order[0].column.ToString());
            }
        }

        public static string iSortCol_0
        {
            get
            {
                return arrTable.Where(d => d.Name == "iSortCol_0").FirstOrDefault().Value.ToString();
            }
        }

        public static string sSortDir_0
        {
            get
            {
                var order = JsonConvert.DeserializeObject<dynamic>(arrTable.Where(d => d.Name == "order").FirstOrDefault().Value.ToString());
                return order[0].dir.ToString();
            }
        }

        public static String sSearch
        {
            get
            {
                var serach = JsonConvert.DeserializeObject<dynamic>(arrTable.Where(d => d.Name == "search").FirstOrDefault().Value.ToString());
                return serach.value.ToString();
            }
        }

        public static int sEcho
        {
            get
            {
                return Convert.ToInt16(arrTable.Where(d => d.Name == "draw").FirstOrDefault().Value);
            }
        }

        public static object GetDataTable(object aaData, int TotalCount)
        {
            object data = new { };

            try
            {
                int sEcho = DTResult.sEcho;
                data = new
                {
                    sEcho = sEcho,
                    draw = sEcho,
                    aaData = aaData,
                    recordsFiltered = TotalCount,
                    recordsTotal = TotalCount,
                    iTotalRecords = TotalCount,
                    iTotalDisplayRecords = TotalCount,
                };
                HttpContext.Current.Response.ContentType = "application/json";
                HttpContext.Current.Response.Write(JsonConvert.SerializeObject(data));
                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
            }
            catch (Exception)
            {
                HttpContext.Current.Response.ContentType = "application/json";
                HttpContext.Current.Response.Write(JsonConvert.SerializeObject(new { retCode = 0 }));
                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndR
            }
            return data;

        }

        public static void setResponse (object aaData)
        {

            try
            {
                HttpContext.Current.Response.ContentType = "application/json";
                HttpContext.Current.Response.Write(JsonConvert.SerializeObject(aaData));
                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
            }
            catch (Exception)
            {
                HttpContext.Current.Response.ContentType = "application/json";
                HttpContext.Current.Response.Write(JsonConvert.SerializeObject(new { retCode = 0 }));
                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndR
            }
        }
    }
}