﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.Models
{
    public class CommonHotels
    {
        public float MinPrice { get; set; }
        public int CountHotel { get; set; }
        public List<CommonLib.Response.Facility> Facility { get; set; }
        public List<CommonLib.Response.CommonHotelDetails> CommonHotelDetails { get; set; }
        public List<CommonLib.Response.Location> Location { get; set; }
        public List<CommonLib.Response.Category> Category { get; set; }
        public float MaxPrice { get; set; }
        //public MGHAvailRequest DisplayRequest { get; set; }
        public int Page { get; set; }
        public int Limit { get; set; }
        public AvailCommonRequest DisplayRequest { get; set; }
    }
}