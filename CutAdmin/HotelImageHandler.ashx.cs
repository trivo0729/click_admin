﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using CutAdmin.EntityModal;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for HotelImageHandler
    /// </summary>
    public class HotelImageHandler : IHttpHandler
    {
        string ImageNames;
        //Click_Hotel db  = new Click_Hotel();
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    DataManager.DBReturnCode retCode = DataManager.DBReturnCode.EXCEPTION;
                    string sid = System.Convert.ToString(context.Request.QueryString["sid"]);
                    string HiddenCode = System.Convert.ToString(context.Request.QueryString["HiddenCode"]);
                    string ImgPath = System.Convert.ToString(context.Request.QueryString["ImagePath"]);
                    string[] tokens = ImgPath.Split('^');
                    int TotalImgs = tokens.Count() - 1;
                    string[] newFiles;

                    Int64 srno = Convert.ToInt64(sid);
                    string OldImages = (from obj in DB.tbl_CommonHotelMaster where obj.sid == srno select obj.SubImages).FirstOrDefault();
                    //string[] OldTokens = OldImages.Split('^');
                    string NewImages = "";
                    string SavedImages = "";


                    for (int img = 0; img < tokens.Count(); img++)
                    {
                        if (tokens[img] != "")
                        {
                            string token1 = tokens[img].Split('.')[0];
                            string ext1 = tokens[img].Split('.')[1];
                            string ext = ext1.Split(',')[0];
                            string extension = "jpg";
                            if (OldImages != null && OldImages.Contains(token1))
                            {
                                SavedImages = OldImages;
                            }
                            else
                            {
                                NewImages += sid + "-" + token1 + '.' + extension + "^";
                            }
                        }

                    }
                    string[] New = NewImages.Split('^');
                    ImageNames = SavedImages + "^" + NewImages;

                    if (context.Request.Files.Count > 0)
                    {

                        HttpFileCollection files = context.Request.Files;
                        if (TotalImgs == files.Count)
                        {
                            for (int i = 0; i < files.Count; i++)
                            {
                                HttpPostedFile file = files[i];
                                string token1 = tokens[i].Split('.')[0];
                                string myFilePath = sid + "-" + token1;
                                string ext1 = tokens[i].Split('.')[1];
                                string ext = "jpg";
                                if (ext != ".pdf")
                                {
                                    string FileName = myFilePath;
                                    FileName = Path.Combine(context.Server.MapPath("HotelImages/"), myFilePath + '.' + ext);
                                    file.SaveAs(FileName);

                                    //retCode = ActivityManager.UpdateImages(sFileName, sid);
                                    context.Response.Write("1");
                                }
                                else
                                {
                                    context.Response.Write("0");

                                }
                            }
                            if (ImageNames != null)
                            {
                                ImgPath = "";
                                ImgPath = ImageNames;
                                // retCode = HotelMappingManager.HotelImages(ImgPath, sid);

                                string[] Path = ImgPath.Split('^');
                                string MainImage = ImgPath.Split('^')[0];
                                int i = 0; ImgPath = "";
                                foreach (string Image in Path)
                                {


                                    if (Image != "")
                                    {
                                        if ((Path.Length - 1) != i)
                                            ImgPath += Image + "^";
                                        else
                                            ImgPath += Image;

                                    }
                                    i++;
                                }

                                using (var db = new Click_Hotel())
                                {
                                    Int64 Sid = Convert.ToInt64(sid);
                                    tbl_CommonHotelMaster Update = db.tbl_CommonHotelMaster.Single(x => x.sid == Sid);
                                    Update.HotelImage = MainImage;
                                    Update.SubImages = ImgPath;
                                    db.SaveChanges();
                                }



                            }

                        }
                        else
                        {
                            string Name;
                            for (int i = 0; i < files.Count; i++)
                            {
                                HttpPostedFile file = files[i];
                                string myFilePath = New[i];
                                string ext = Path.GetExtension(myFilePath);
                                if (ext != ".pdf")
                                {
                                    string FileName = myFilePath;
                                    FileName = Path.Combine(context.Server.MapPath("HotelImages/"), myFilePath);
                                    file.SaveAs(FileName);
                                    //retCode = ActivityManager.UpdateImages(sFileName, sid);
                                    context.Response.Write("1");
                                }
                                else
                                {
                                    context.Response.Write("0");

                                }
                            }
                            if (ImageNames != null)
                            {
                                ImgPath = "";
                                ImgPath = ImageNames;
                                //retCode = HotelMappingManager.HotelImages(ImgPath, sid);

                                string[] Path = ImgPath.Split('^');
                                string MainImage = ImgPath.Split('^')[0];
                                int i = 0; ImgPath = "";
                                foreach (string Image in Path)
                                {


                                    if (Image != "")
                                    {
                                        if ((Path.Length - 1) != i)
                                            ImgPath += Image + "^";
                                        else
                                            ImgPath += Image;

                                    }
                                    i++;
                                }

                                using (var db = new Click_Hotel())
                                {
                                    Int64 Sid = Convert.ToInt64(sid);
                                    tbl_CommonHotelMaster Update = db.tbl_CommonHotelMaster.Single(x => x.sid == Sid);
                                    Update.HotelImage = MainImage;
                                    Update.SubImages = ImgPath;
                                    db.SaveChanges();
                                }

                            }
                        }


                    }

                    //Method for saving image of new hotel reference by old hotel images by Atique on 04-02-19
                    if (HiddenCode != "")
                    {
                        string NewImag = "";
                        var extension = "jpg";
                        string Images = (from obj in DB.tbl_CommonHotelMaster where obj.sid == Convert.ToInt32(HiddenCode) select obj.SubImages).FirstOrDefault();
                        List<string> arrImage = new List<string>();
                        List<string> arrNewImages = new List<string>();
                        if (Images != null)
                        {
                            var img = Images.Split('^');
                            arrImage = img.ToList();
                            foreach (var itemImg in img)
                            {
                                if(itemImg != "")
                                {
                                    var nameImg = itemImg.Split('-')[1];
                                    NewImag += sid + "-" + nameImg + "^";
                                    arrNewImages.Add(sid + "-" + nameImg);
                                }       
                            }
                        }
                        using (var db = new Click_Hotel())
                        {
                            Int64 Sid = Convert.ToInt64(sid);
                            tbl_CommonHotelMaster Update = db.tbl_CommonHotelMaster.Single(x => x.sid == Sid);
                            if(Update.SubImages != null)
                            {
                                if (Update.SubImages.Contains(HiddenCode))
                                {
                                    var imag = Update.SubImages.Split('^');
                                    foreach (var itemImg in imag)
                                    {
                                        if (itemImg != "" && !itemImg.Contains(HiddenCode))
                                        {
                                            var nameImg = itemImg.Split('-')[1];
                                            NewImag += sid + "-" + nameImg + "^";
                                            arrNewImages.Add(sid + "-" + nameImg);
                                        }
                                    }
                                    Update.SubImages = NewImag;
                                    db.SaveChanges();
                                }

                            }

                            else
                            {
                                Update.SubImages =  NewImag;
                                db.SaveChanges();
                            }

                            string Path = context.Server.MapPath("~/HotelImages/");

                            DirectoryInfo dir = new DirectoryInfo(Path);
                            FileInfo[] files = null;

                            files = dir.GetFiles().Where(d => arrImage.Contains(d.Name)).ToArray();
                            int i = 0;
                            foreach (FileInfo file in files.ToList())
                            {

                                file.CopyTo(Path + arrNewImages[i]);
                                i++;
                            }
                        }

                    }


                    else
                    {
                        //DBHelper.DBReturnCode retCode = ImageManager.InsertSliderImage(sid, sFileName, facility, service, start, details, Notes, "");
                        if (retCode == DataManager.DBReturnCode.SUCCESS)
                        {
                            context.Response.Write("1");
                        }
                        else
                        {
                            context.Response.Write("0");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
           
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }      
    }
}