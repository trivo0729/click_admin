﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.handler;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.EntityModal;
using System.Net;
using CutAdmin.Services;
using CutAdmin.dbml;
using Elmah;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for GenralHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class GenralHandler : System.Web.Services.WebService
    {
        string json = "";
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string jsonString = "";
        JavaScriptSerializer objserialize = new JavaScriptSerializer();
        //Click_Hotel DB = new Click_Hotel();
        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        #region MailSettings
        [WebMethod(EnableSession = true)]
        public string GetHotelActivityMails(string Type)
        {
            using (var DB = new CutAdmin.dbml.helperDataContext())
            {
                string jsonString = "";
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Uid = objGlobalDefault.sid;
                if (objGlobalDefault.UserType == "SupplierStaff")
                {
                    Uid = objGlobalDefault.ParentId;
                }
                //DBHelper.DBReturnCode retcode = VisaManager.GetVisaMails(Activity,Type, out dtResult);

                var HotelMailsList = (from obj in DB.tbl_ActivityMails
                                      where obj.Type == Type && obj.ParentID == 232
                                      select new
                                      {
                                          obj.Activity
                                      }).ToList();

                var MailsList = (from obj in DB.tbl_ActivityMails
                                 where obj.Type == Type && obj.ParentID == Uid
                                 select new
                                 {
                                     obj.sid,
                                     obj.Activity,
                                     //  obj.BCcMail,
                                     obj.CcMail,
                                     obj.Email,
                                     obj.ErroMessage
                                 }).ToList();

                if (HotelMailsList.Count > 0 && HotelMailsList != null)
                {

                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = HotelMailsList, MailsList = MailsList });

                }
                else
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                return jsonString;
            }
        }

        [WebMethod(EnableSession = true)]
        public string UpdateVisaMails(string Activity, string Type, string MailsId, string CcMails, string BCcMail, string Message)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = objGlobalDefault.sid;
            if (objGlobalDefault.UserType == "SupplierStaff")
            {
                Uid = objGlobalDefault.ParentId;
            }
            try
            {
                using (var DB = new CutAdmin.dbml.helperDataContext())
                {
                    var sActivity = (from obj in DB.tbl_ActivityMails where obj.Activity == Activity && obj.Type == Type && obj.ParentID == Uid select obj).FirstOrDefault();

                    if (sActivity == null)
                    {
                        CutAdmin.dbml.tbl_ActivityMail activity = new CutAdmin.dbml.tbl_ActivityMail();
                        activity.Type = "Hotel";
                        activity.Activity = Activity;
                        activity.ParentID = objGlobalDefault.sid;
                        activity.Email = MailsId;
                        activity.CcMail = CcMails;
                        activity.BCcMail = BCcMail;
                        activity.ErroMessage = Message;
                        DB.tbl_ActivityMails.InsertOnSubmit(activity);
                        DB.SubmitChanges();
                    }
                    else
                    {
                        sActivity.Email = MailsId;
                        sActivity.CcMail = CcMails;
                        sActivity.BCcMail = BCcMail;
                        sActivity.ErroMessage = Message;
                        DB.SubmitChanges();
                    }
                    return jsSerializer.Serialize(new { retCode = 1 });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });

            }
        }
        #endregion

        #region CountryCityCode

        [WebMethod(EnableSession = true)]
        public string GetCountry()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CountryCityCodeManager.GetCountry(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Country\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetCity(string country)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CountryCityCodeManager.GetCity(country, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"City\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetCity1(string country)
        {
            string jsonString = "";
            JavaScriptSerializer objserialize = new JavaScriptSerializer();

            CutAdmin.dbml.helperDataContext db = new CutAdmin.dbml.helperDataContext();
            var CityList = (from obj in db.tbl_HCities where obj.Countryname == country select obj.Description).Distinct().ToList();

            if (CityList.Count > 0)
            {
                return objserialize.Serialize(new { Session = 1, retCode = 1, CityList = CityList });
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetCity2(string country)
        {
            string jsonString = "";
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            CutAdmin.dbml.helperDataContext db = new CutAdmin.dbml.helperDataContext();
            var CityList = (from obj in db.tbl_HCities where obj.Country == country select obj.Description).Distinct().ToList();

            if (CityList.Count > 0)
            {
                return objserialize.Serialize(new { Session = 1, retCode = 1, CityList = CityList });
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }


        [WebMethod(EnableSession = true)]
        public string GetCityCode(string Description)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CountryCityCodeManager.GetCityCode(Description, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"CityCode\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        #endregion CountryCityCode

        #region GetCountryCity
        [WebMethod(EnableSession = true)]
        public string GetCountryCity(string country)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            string json = "";

            try
            {
                using (var DB = new CutAdmin.dbml.helperDataContext())
                {
                    var City = (from obj in DB.tbl_HCities
                                where obj.Countryname == country

                                select new
                                {
                                    obj.Code,
                                    obj.Description

                                }).Distinct().ToList();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, CityList = City });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });

            }
            return json;
        }
        #endregion

        #region Currency

        [WebMethod(EnableSession = true)]
        public string GetCurrency()
        {
            using (var DB = new Click_Hotel())
            {
                var CurrencyList = (from obj in DB.tbl_CommonCurrency select obj).ToList();
                if (CurrencyList.Count() > 0)
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 1, CurrencyList = CurrencyList });
                }
                else
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 0 });
                }
            }
        }

        #endregion

        #region Get Country City Mapping
        [WebMethod(EnableSession = true)]
        public string GetCityCodeMapping(string Countryname, string CityName)
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {

                DataSet dsResult;
                DataTable dtHotelBeds, dtDotw, dtExpedia, dtGrn;
                DBHelper.DBReturnCode retcode = CountryCityCodeManager.GetCountryCityMapping(Countryname, CityName, out dsResult);
                if (retcode == DBHelper.DBReturnCode.SUCCESS)
                {
                    dtHotelBeds = dsResult.Tables[0];
                    dtHotelBeds.Columns.Add("Supplier", typeof(String));
                    dtDotw = dsResult.Tables[1];
                    dtDotw.Columns.Add("Supplier", typeof(String));
                    dtExpedia = dsResult.Tables[2];
                    dtExpedia.Columns.Add("Supplier", typeof(String));
                    dtGrn = dsResult.Tables[3];
                    dtGrn.Columns.Add("Supplier", typeof(String));
                    jsonString = "";
                    int i = 0;
                    foreach (DataRow dr in dtHotelBeds.Rows)
                    {
                        dtHotelBeds.Rows[i]["Supplier"] = "HotelBeds";
                        i++;
                    }
                    i = 0;
                    foreach (DataRow dr in dtDotw.Rows)
                    {
                        dtDotw.Rows[i]["Supplier"] = "Dotw";
                        i++;
                    }
                    i = 0;
                    foreach (DataRow dr in dtExpedia.Rows)
                    {
                        dtExpedia.Rows[i]["Supplier"] = "Expedia";
                        i++;
                    }
                    i = 0;
                    foreach (DataRow dr in dtGrn.Rows)
                    {
                        dtGrn.Rows[i]["Supplier"] = "Grn";
                        i++;
                    }
                    // jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Staff\":[" + jsonString.Trim(',') + "]}";
                    //dtHotelBeds.Dispose();
                    List<Dictionary<string, object>> HotelBedsList = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> DotwList = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> ExpediaList = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> GrnList = new List<Dictionary<string, object>>();

                    HotelBedsList = JsonStringManager.ConvertDataTable(dtHotelBeds);
                    DotwList = JsonStringManager.ConvertDataTable(dtDotw);
                    ExpediaList = JsonStringManager.ConvertDataTable(dtExpedia);
                    GrnList = JsonStringManager.ConvertDataTable(dtGrn);
                    return jsSerializer.Serialize(new { Session = 1, retCode = 1, HotelBedsList = HotelBedsList, DotwList = DotwList, ExpediaList = ExpediaList, GrnList = GrnList });

                }
                else
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch (Exception ex)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                string LineNo = ex.StackTrace.ToString();
            }
            return jsonString;

        }
        #endregion Get Country City Mapping

        [WebMethod(true)]
        public string AddMapping(string CityName, string CityCode, string CountryName, string CountryCode, string dotwCityCode, string ExpediaCityCode, string GrnCityCode)
        {
            //CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];

            DBHelper.DBReturnCode retCode = CountryCityCodeManager.AddMapping(CityName, CityCode, CountryName, CountryCode, dotwCityCode, ExpediaCityCode, GrnCityCode);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetMappedCities()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CountryCityCodeManager.GetMappedCities(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"MappedCitiesList\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        #region Suppliers
        [WebMethod(EnableSession = true)]
        public string GetSuppliers(string Service)
        {
            try
            {
                return objserialize.Serialize(new { Session = 1, retCode = 1, SupplierList = GenralManager.GetSupplier(Service) });
            }
            catch (Exception)
            {

                return objserialize.Serialize(new { Session = 1, retCode = 0 });
            }
        }

        #endregion

        #region Cancelation Policies
        [WebMethod(EnableSession = true)]
        public string GetCancelationPolicies()
        {
            using (var DB = new Click_Hotel())
            {
                Int64 Id = DataLayer.AccountManager.GetSupplierByUser();
                //var CancelationPolicies = (from obj in DB.tbl_CommonCancelationPolicies where obj.SupplierID==Id select obj).Skip(1).ToList();
                var CancelationPolicies = (from obj in DB.tbl_CommonCancelationPolicy where obj.SupplierID == Id select obj).ToList();
                if (CancelationPolicies.Count() > 0)
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 1, CancelationPolicies = CancelationPolicies });
                }
                else
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 0 });
                }
            }
        }

        #endregion

        #region GetCancelation Policies
        [WebMethod(EnableSession = true)]
        public string GetCancelations(Int64 CancelId)
        {
            using (var DB = new Click_Hotel())
            {
                var CancelationPolicies = (from obj in DB.tbl_CommonCancelationPolicy where obj.CancelationID == CancelId select obj).FirstOrDefault();
                var arrRelated = new List<tbl_CommonCancelationPolicy>();
                if (CancelationPolicies.PolicyType == "DaysPrior")
                {

                    arrRelated = (from obj in DB.tbl_CommonCancelationPolicy
                                  where obj.PolicyType == "DaysPrior" && obj.DaysPrior < CancelationPolicies.DaysPrior
                                  orderby obj.CancelationPolicy
                                  select obj).ToList();


                }
                else if (CancelationPolicies.PolicyType == "Date")
                {

                    var sDateCancellation = (from obj in DB.tbl_CommonCancelationPolicy
                                             where obj.PolicyType == "Date"
                                             orderby obj.CancelationPolicy
                                             select obj).ToList(); ;
                    foreach (var Cancellation in sDateCancellation)
                    {

                        if (DateTime.ParseExact(Cancellation.Date, "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture) < DateTime.ParseExact(CancelationPolicies.Date, "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture))
                        {
                            arrRelated.Add(Cancellation);
                        }

                    }

                    //arrRelated = sDate.Select(d => d.CancelationID).ToList();
                    //arrRelated = (from obj in DB.tbl_CommonCancelationPolicies
                    //              where obj.PolicyType == "Date" && DateTime.ParseExact(obj.Date, "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture) < DateTime.ParseExact(CancelationPolicies.Date, "dd-mm-yyyy", System.Globalization.CultureInfo.InvariantCulture)
                    //              orderby obj.CancelationPolicy
                    //              select obj).ToList();
                }
                else
                {
                    arrRelated = (from obj in DB.tbl_CommonCancelationPolicy
                                  where obj.PolicyType == "None"
                                  orderby obj.CancelationPolicy
                                  select obj).ToList();

                }
                if (arrRelated.Count() > 0)
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 1, NewCancelationPolicies = arrRelated });
                }
                else
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 0 });
                }
            }
        }

        #endregion

        #region Meal Plan
        [WebMethod(EnableSession = true)]
        public string GetMealPlans()
        {
            using (var DB = new Click_Hotel())
            {
                var MealPlanList = (from obj in DB.tbl_CommonMealPlan select obj).ToList();
                if (MealPlanList.Count > 0)
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 1, MealPlanList = MealPlanList });
                }
                else
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 0 });
                }
            }
        }

        #endregion

        #region Rate Types
        [WebMethod(EnableSession = true)]
        public string GetRateType(string AdminID)
        {
            try
            {
                return objserialize.Serialize(new { Session = 1, retCode = 1, arrRateType = GenralManager.GetRateType(AdminID) });
            }
            catch (Exception ex)
            {

                return objserialize.Serialize(new { ex = ex.Message, retCode = 0 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string SavetRateType(tbl_RateType arrRateType)
        {
            try
            {
                GenralManager.SaveRateType(arrRateType);
                return objserialize.Serialize(new { Session = 1, retCode = 1 });
            }
            catch (Exception)
            {

                return objserialize.Serialize(new { Session = 1, retCode = 0 });
            }
        }

        #endregion

        #region ListtoDataTable
        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }
        #endregion

        [WebMethod(EnableSession = true)]
        public Int64 GetUserID(string UserType)
        {
            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];

            Int64 Uid = 0;
            if (UserType == "Supplier")
            {
                Uid = objGlobalDefault.sid;
            }
            else if (objGlobalDefault.UserType == "SupplierStaff")
            {
                Uid = objGlobalDefault.ParentId;
            }
            else if (objGlobalDefault.UserType == "Admin")
            {
                Uid = objGlobalDefault.sid;
            }
            else if (objGlobalDefault.UserType == "AdminStaff")
            {
                Uid = objGlobalDefault.ParentId;
            }

            return Uid;

        }

        [WebMethod(EnableSession = true)]
        public string GetALLOffer()
        {
            using (var DB = new Click_Hotel())
            {
                Int64 Uid = 0;
                Uid = AccountManager.GetUserByLogin();
                var ALLOffer = (from obj in DB.tbl_CommonHotelOffer where obj.SupplierID == Uid select obj).ToList();
                if (ALLOffer.Count() > 0)
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 1, ALLOffer = ALLOffer });
                }
                else
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 0 });
                }
            }
        }

        #region AgencyDepositeDetails
        [WebMethod(EnableSession = true)]
        public string GetBankDepositDetail()
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = agencyDepositeManager.GetBankDepositDetail(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Dictionary<string, object>> BankDeposit = new List<Dictionary<string, object>>();
                BankDeposit = JsonStringManager.ConvertDataTable(dtResult);
                dtResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, tbl_BankDeposit = BankDeposit });
            }
            else
            {
                return jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

        }

        [WebMethod(true)]
        public string UserLogin(string sUserName, string sPassword)
        {
            DBHelper.DBReturnCode retCode = agencyDepositeManager.UserLogin(sUserName, sPassword);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                if (objGlobalDefault.UserType == "Admin" || objGlobalDefault.UserType == "Franchisee")
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Admin\"}";
                }
                else if (objGlobalDefault.UserType == "Agent")
                {
                    return "{\"Session\":\"1\",\"retCode\":\"1\",\"roleID\":\"Agent\"}";
                }
                else
                    return "{\"Session\":\"1\",\"retCode\":\"0\",\"roleID\":\"Null\"}";

            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\",\"roleID\":\"Null\"}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string UnApproveDeposit(int uid, Int64 sId)
        {
            try
            {
                using (var DB = new CutAdmin.dbml.helperDataContext())
                {
                    CutAdmin.dbml.tbl_BankDeposit List = DB.tbl_BankDeposits.Where(d => d.sId == sId).FirstOrDefault();
                    List.UnApproveFlag = true;
                    DB.SubmitChanges();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(true)]
        public string ApproveDeposit(int uid, decimal dDepositAmount, string sTypeofCash, string sTypeofcheque, string sComment, string sLastUpdatedDate, Int64 sId)
        {
            int rows;
            string jsonString = "";
            DataTable dtResult = null;
            DBHelper.DBReturnCode retcode1 = agencyDepositeManager.ValidateLimit(uid, out dtResult);

            DBHelper.DBReturnCode retcode = agencyDepositeManager.ApproveDeposit(uid, dDepositAmount, sTypeofCash, sTypeofcheque, sComment, sLastUpdatedDate, sId, out rows);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        #endregion

        #region BankDetails
        [WebMethod(EnableSession = true)]
        public string AddBankDetails(string BankName, string AccountNo, string Branch, string SwiftCode, string IFSCCode, string Country)
        {
            try
            {
                using (var DB = new CutAdmin.dbml.helperDataContext())
                {
                    CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    CutAdmin.dbml.tbl_BankDetail Add = new CutAdmin.dbml.tbl_BankDetail();
                    Add.ParentId = objGlobalDefault.sid;
                    Add.BankName = BankName;
                    Add.AccountNo = AccountNo;
                    Add.Branch = Branch;
                    Add.SwiftCode = SwiftCode;
                    Add.IFSCCode = IFSCCode;
                    Add.Country = Country;
                    DB.tbl_BankDetails.InsertOnSubmit(Add);
                    DB.SubmitChanges();

                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetBankDetails()
        {
            try
            {
                using (var DB = new CutAdmin.dbml.helperDataContext())
                {
                    CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    var List = (from obj in DB.tbl_BankDetails where obj.ParentId == AccountManager.GetAdminByLogin() select obj).ToList();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BankDetails = List });
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateBankDetail(string sid, string BankName, string AccountNo, string Branch, string SwiftCode, string IFSCCode, string Country)
        {
            try
            {
                using (var DB = new CutAdmin.dbml.helperDataContext())
                {
                    Int64 ID = Convert.ToInt64(sid);
                    CutAdmin.dbml.tbl_BankDetail Update = DB.tbl_BankDetails.Single(x => x.sid == ID);
                    Update.BankName = BankName;
                    Update.AccountNo = AccountNo;
                    Update.Branch = Branch;
                    Update.SwiftCode = SwiftCode;
                    Update.IFSCCode = IFSCCode;
                    Update.Country = Country;
                    DB.SubmitChanges();

                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteBankDetail(string sid)
        {
            try
            {
                using (var DB = new CutAdmin.dbml.helperDataContext())
                {
                    Int64 ID = Convert.ToInt64(sid);
                    CutAdmin.dbml.tbl_BankDetail Delete = DB.tbl_BankDetails.Single(x => x.sid == ID);
                    DB.tbl_BankDetails.DeleteOnSubmit(Delete);
                    DB.SubmitChanges();

                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
        #endregion


        [WebMethod(EnableSession = true)]
        public string GetAgentDetailsOnAdmin(string sid)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            HttpContext.Current.Session["AgentDetailsOnAdmin"] = null;
            HttpContext.Current.Session["AgentMarkupsOnAdmin"] = null;
            CutAdmin.DataLayer.AgentDetailsOnAdmin objAgentDetailsOnAdmin = new CutAdmin.DataLayer.AgentDetailsOnAdmin();
            CutAdmin.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes = new CutAdmin.DataLayer.MarkupsAndTaxes();
            CutAdmin.DataLayer.MarkupsAndTaxesTrans objMarkupsAndTaxesTrans = new CutAdmin.DataLayer.MarkupsAndTaxesTrans();
            DataSet dsResult;
            string jsonString = "";
            DBHelper.DBReturnCode retcode = CutAdmin.HotelAdmin.DataLayer.AdminDetailsManager.GetAgentDetailsOnAdmin(sid, out dsResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                DataTable dtIndividualMarkup, dtGroupMarkup, dtGlobalMarkup, dtAgentOwnMarkp, dtServiceTax, dtTransIndividualMarkup, dtTransGroupMarkup, dtTransGlobalMarkup, dtTransAgentOwnMarkp, dtSupplierCommision;

                dtIndividualMarkup = dsResult.Tables[2];
                dtGroupMarkup = dsResult.Tables[1];
                dtGlobalMarkup = dsResult.Tables[0];
                dtAgentOwnMarkp = dsResult.Tables[3];
                dtServiceTax = dsResult.Tables[4];
                dtTransIndividualMarkup = dsResult.Tables[5];
                dtTransGroupMarkup = dsResult.Tables[6];
                dtTransGlobalMarkup = dsResult.Tables[7];
                dtTransAgentOwnMarkp = dsResult.Tables[8];
                dtSupplierCommision = dsResult.Tables[9];
                DataTable dt1 = dsResult.Tables[10];
                //DataTable dt2 = dsResult.Tables[1];
                //DataTable dt3 = dsResult.Tables[2];
                //DataTable dt4 = dsResult.Tables[3];
                if (dt1.Rows.Count != 0)
                {
                    objAgentDetailsOnAdmin.sid = Convert.ToInt64(dt1.Rows[0]["sid"]);
                    objAgentDetailsOnAdmin.uid = dt1.Rows[0]["uid"].ToString();
                    objAgentDetailsOnAdmin.UserType = dt1.Rows[0]["UserType"].ToString();
                    objAgentDetailsOnAdmin.AgencyName = dt1.Rows[0]["AgencyName"].ToString();
                    objAgentDetailsOnAdmin.ContactPerson = dt1.Rows[0]["ContactPerson"].ToString();
                    objAgentDetailsOnAdmin.Last_Name = dt1.Rows[0]["Last_Name"].ToString();
                    objAgentDetailsOnAdmin.RoleID = Convert.ToInt64(dt1.Rows[0]["RoleID"]);
                    objAgentDetailsOnAdmin.Agentuniquecode = dt1.Rows[0]["Agentuniquecode"].ToString();
                    objAgentDetailsOnAdmin.GroupId = Convert.ToInt64(dt1.Rows[0]["GroupId"]);
                    objAgentDetailsOnAdmin.AvailableCredit = Convert.ToInt64(dt1.Rows[0]["AvailableCredit"]);
                    objAgentDetailsOnAdmin.SupplierVisible = Convert.ToBoolean(dt1.Rows[0]["SupplierVisible"]);
                    objAgentDetailsOnAdmin.Currency = dt1.Rows[0]["CurrencyCode"].ToString();
                    HttpContext.Current.Session["LoginUser"] = objGlobalDefault;
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";


                #region Markup new
                //objMarkupsAndTaxes.PerAgentMarkup = Convert.ToSingle(dt2.Rows[0]["AgentMarkup"]);
                //objMarkupsAndTaxes.PerServiceTax = Convert.ToSingle(dt3.Rows[0]["ServiceTax"]);
                //objMarkupsAndTaxes.PerCutMarkup = Convert.ToSingle(dt4.Rows[0]["CUTMarkup"]);
                HttpContext.Current.Session["AgentDetailsOnAdmin"] = objAgentDetailsOnAdmin;
                //HttpContext.Current.Session["AgentMarkupsOnAdmin"] = objMarkupsAndTaxes;
                //DataTable dtIndividualMarkup, dtGroupMarkup, dtGlobalMarkup, dtAgentOwnMarkp, dtServiceTax, dtTransIndividualMarkup, dtTransGroupMarkup, dtTransGlobalMarkup, dtTransAgentOwnMarkp;
                //DataTable dtIndividualMarkup, dtGroupMarkup, dtGlobalMarkup, dtAgentOwnMarkp, dtServiceTax;
                //dtIndividualMarkup = dsResult.Tables[5];
                //dtGroupMarkup = dsResult.Tables[4];
                //dtGlobalMarkup = dsResult.Tables[3];
                //dtAgentOwnMarkp = dsResult.Tables[6];
                //dtServiceTax = dsResult.Tables[7];
                //List<CUT.DataLayer.MarkupsAndTaxes> objMarkupsAndTaxes = new List<MarkupsAndTaxes>();
                List<CutAdmin.DataLayer.IndividualMarkup> listIndividualMarkup = new List<CutAdmin.DataLayer.IndividualMarkup>();
                List<CutAdmin.DataLayer.GroupMarkup> listGroupMarkup = new List<CutAdmin.DataLayer.GroupMarkup>();
                List<CutAdmin.DataLayer.GlobalMarkup> listGlobalMarkup = new List<CutAdmin.DataLayer.GlobalMarkup>();
                List<CutAdmin.DataLayer.AgentMarkup> listAgentMarkup = new List<CutAdmin.DataLayer.AgentMarkup>();
                List<CutAdmin.DataLayer.SupplierCommision> listCommision = new List<CutAdmin.DataLayer.SupplierCommision>();

                if (dtIndividualMarkup.Rows.Count != 0)
                {
                    //List<IndividualMarkup> listIndividualMarkup = new List<IndividualMarkup>();
                    for (int i = 0; i < dtIndividualMarkup.Rows.Count; i++)
                    {
                        CutAdmin.DataLayer.IndividualMarkup objIndividualMarkup = new CutAdmin.DataLayer.IndividualMarkup();
                        objIndividualMarkup.Supplier = Convert.ToString(dtIndividualMarkup.Rows[i]["Supplier"]);
                        objIndividualMarkup.IndMarkupPer = Convert.ToSingle(dtIndividualMarkup.Rows[i]["MarkupPercentage"]);
                        objIndividualMarkup.IndMarkAmt = Convert.ToSingle(dtIndividualMarkup.Rows[i]["MarkupAmmount"]);
                        objIndividualMarkup.IndCommPer = Convert.ToSingle(dtIndividualMarkup.Rows[i]["CommessionPercentage"]);
                        objIndividualMarkup.IndCommAmt = Convert.ToSingle(dtIndividualMarkup.Rows[i]["CommessionAmmount"]);
                        listIndividualMarkup.Add(objIndividualMarkup);
                    }
                    //objMarkupsAndTaxes.listIndividualMarkup = listIndividualMarkup;

                }
                if (dtGroupMarkup.Rows.Count != 0)
                {
                    //List<GroupMarkup> listGroupMarkup = new List<GroupMarkup>();
                    for (int i = 0; i < dtGroupMarkup.Rows.Count; i++)
                    {
                        CutAdmin.DataLayer.GroupMarkup objGroupMarkup = new CutAdmin.DataLayer.GroupMarkup();
                        objGroupMarkup.Supplier = dtGroupMarkup.Rows[i]["Supplier"].ToString();
                        objGroupMarkup.GroupMarkupPer = Convert.ToSingle(dtGroupMarkup.Rows[i]["MarkupPercentage"]);
                        objGroupMarkup.GroupMarkAmt = Convert.ToSingle(dtGroupMarkup.Rows[i]["MarkupAmmount"]);
                        objGroupMarkup.GroupCommPer = Convert.ToSingle(dtGroupMarkup.Rows[i]["CommessionPercentage"]);
                        objGroupMarkup.GroupCommAmt = Convert.ToSingle(dtGroupMarkup.Rows[i]["CommessionAmmount"]);
                        objGroupMarkup.TaxOnMarkup = Convert.ToBoolean(dtGroupMarkup.Rows[i]["TaxApplicable"]);
                        listGroupMarkup.Add(objGroupMarkup);
                    }
                    //objMarkupsAndTaxes.listGroupMarkup = listGroupMarkup;
                }
                if (dtGlobalMarkup.Rows.Count != 0)
                {
                    //List<GlobalMarkup> listGlobalMarkup = new List<GlobalMarkup>();
                    for (int i = 0; i < dtGlobalMarkup.Rows.Count; i++)
                    {
                        CutAdmin.DataLayer.GlobalMarkup objGlobalMarkup = new CutAdmin.DataLayer.GlobalMarkup();
                        objGlobalMarkup.Supplier = dtGlobalMarkup.Rows[i]["Supplier"].ToString();
                        objGlobalMarkup.GlobalMarkupPer = Convert.ToSingle(dtGlobalMarkup.Rows[i]["MarkupPercentage"]);
                        objGlobalMarkup.GlobalMarkAmt = Convert.ToSingle(dtGlobalMarkup.Rows[i]["MarkupAmmount"]);
                        objGlobalMarkup.GlobalCommPer = Convert.ToSingle(dtGlobalMarkup.Rows[i]["CommessionPercentage"]);
                        objGlobalMarkup.GlobalCommAmt = Convert.ToSingle(dtGlobalMarkup.Rows[i]["CommessionAmmount"]);
                        listGlobalMarkup.Add(objGlobalMarkup);
                    }
                    //objMarkupsAndTaxes.listGlobalMarkup = listGlobalMarkup;
                }
                if (dtAgentOwnMarkp.Rows.Count != 0)
                {
                    for (int i = 0; i < dtAgentOwnMarkp.Rows.Count; i++)
                    {
                        CutAdmin.DataLayer.AgentMarkup objAgentMarkup = new CutAdmin.DataLayer.AgentMarkup();
                        objAgentMarkup.AgentOwnMarkupPer = Convert.ToSingle(dtAgentOwnMarkp.Rows[i]["Percentage"]);
                        objAgentMarkup.AgentOwnMarkupAmt = Convert.ToSingle(dtAgentOwnMarkp.Rows[i]["Amount"]);
                        objAgentMarkup.SupplierType = dtAgentOwnMarkp.Rows[i]["ServiceType"].ToString();
                        //objMarkupsAndTaxes.AgentOwnMarkupPer = Convert.ToSingle(dtAgentOwnMarkp.Rows[0]["Percentage"]);
                        //objMarkupsAndTaxes.AgentOwnMarkupAmt = Convert.ToSingle(dtAgentOwnMarkp.Rows[0]["Amount"]);
                        listAgentMarkup.Add(objAgentMarkup);
                    }
                }
                if (dtServiceTax.Rows.Count != 0)
                {
                    objMarkupsAndTaxes.PerServiceTax = Convert.ToSingle(dtServiceTax.Rows[0]["ServiceTax"]);
                    objMarkupsAndTaxes.TimeGap = Convert.ToInt32(dtServiceTax.Rows[0]["TimeGap"]);
                }
                if (dtSupplierCommision.Rows.Count != 0)
                {
                    for (int i = 0; i < dtSupplierCommision.Rows.Count; i++)
                    {
                        CutAdmin.DataLayer.SupplierCommision objComm = new CutAdmin.DataLayer.SupplierCommision();
                        objComm.Supplier = dtSupplierCommision.Rows[i]["Supplier"].ToString();
                        objComm.Commision = Convert.ToSingle(dtSupplierCommision.Rows[i]["HotelComm"].ToString()) / 100;
                        objComm.TDS = Convert.ToSingle(dtSupplierCommision.Rows[i]["HotelTDS"]) / 100;
                        listCommision.Add(objComm);
                    }

                }
                objMarkupsAndTaxes.SupplierCommision = listCommision;


                if (dtIndividualMarkup.Rows.Count != 0)
                {
                    objMarkupsAndTaxes.listIndividualMarkup = listIndividualMarkup;
                }
                if (dtGroupMarkup.Rows.Count != 0)
                {
                    objMarkupsAndTaxes.listGroupMarkup = listGroupMarkup;
                }
                if (dtGlobalMarkup.Rows.Count != 0)
                {
                    objMarkupsAndTaxes.listGlobalMarkup = listGlobalMarkup;
                }
                if (dtAgentOwnMarkp.Rows.Count != 0)
                {
                    objMarkupsAndTaxes.listAgentMarkup = listAgentMarkup;
                }
                listIndividualMarkup = new List<CutAdmin.DataLayer.IndividualMarkup>();
                listGroupMarkup = new List<CutAdmin.DataLayer.GroupMarkup>();
                listGlobalMarkup = new List<CutAdmin.DataLayer.GlobalMarkup>();
                listAgentMarkup = new List<AgentMarkup>();
                if (dtTransIndividualMarkup.Rows.Count != 0)
                {
                    for (int i = 0; i < dtTransIndividualMarkup.Rows.Count; i++)
                    {
                        CutAdmin.DataLayer.IndividualMarkup objIndividualMarkup = new CutAdmin.DataLayer.IndividualMarkup();
                        objIndividualMarkup.Supplier = Convert.ToString(dtTransIndividualMarkup.Rows[i]["Supplier"]);
                        objIndividualMarkup.IndMarkupPer = Convert.ToSingle(dtTransIndividualMarkup.Rows[i]["MarkupPercentage"]);
                        objIndividualMarkup.IndMarkAmt = Convert.ToSingle(dtTransIndividualMarkup.Rows[i]["MarkupAmmount"]);
                        objIndividualMarkup.IndCommPer = Convert.ToSingle(dtTransIndividualMarkup.Rows[i]["CommessionPercentage"]);
                        objIndividualMarkup.IndCommAmt = Convert.ToSingle(dtTransIndividualMarkup.Rows[i]["CommessionAmmount"]);
                        listIndividualMarkup.Add(objIndividualMarkup);
                    }
                }

                if (dtTransGroupMarkup.Rows.Count != 0)
                {
                    for (int i = 0; i < dtTransGroupMarkup.Rows.Count; i++)
                    {
                        CutAdmin.DataLayer.GroupMarkup objGroupMarkup = new CutAdmin.DataLayer.GroupMarkup();
                        objGroupMarkup.Supplier = dtTransGroupMarkup.Rows[i]["Supplier"].ToString();
                        objGroupMarkup.GroupMarkupPer = Convert.ToSingle(dtTransGroupMarkup.Rows[i]["MarkupPercentage"]);
                        objGroupMarkup.GroupMarkAmt = Convert.ToSingle(dtTransGroupMarkup.Rows[i]["MarkupAmmount"]);
                        objGroupMarkup.GroupCommPer = Convert.ToSingle(dtTransGroupMarkup.Rows[i]["CommessionPercentage"]);
                        objGroupMarkup.GroupCommAmt = Convert.ToSingle(dtTransGroupMarkup.Rows[i]["CommessionAmmount"]);
                        objGroupMarkup.TaxOnMarkup = Convert.ToBoolean(dtTransGroupMarkup.Rows[i]["TaxApplicable"]);
                        listGroupMarkup.Add(objGroupMarkup);
                    }
                }

                if (dtTransGlobalMarkup.Rows.Count != 0)
                {
                    for (int i = 0; i < dtTransGlobalMarkup.Rows.Count; i++)
                    {
                        CutAdmin.DataLayer.GlobalMarkup objGlobalMarkup = new CutAdmin.DataLayer.GlobalMarkup();
                        objGlobalMarkup.Supplier = dtTransGlobalMarkup.Rows[i]["Supplier"].ToString();
                        objGlobalMarkup.GlobalMarkupPer = Convert.ToSingle(dtTransGlobalMarkup.Rows[i]["MarkupPercentage"]);
                        objGlobalMarkup.GlobalMarkAmt = Convert.ToSingle(dtTransGlobalMarkup.Rows[i]["MarkupAmmount"]);
                        objGlobalMarkup.GlobalCommPer = Convert.ToSingle(dtTransGlobalMarkup.Rows[i]["CommessionPercentage"]);
                        objGlobalMarkup.GlobalCommAmt = Convert.ToSingle(dtTransGlobalMarkup.Rows[i]["CommessionAmmount"]);
                        listGlobalMarkup.Add(objGlobalMarkup);
                    }
                }

                if (dtTransAgentOwnMarkp.Rows.Count != 0)
                {
                    for (int i = 0; i < dtTransAgentOwnMarkp.Rows.Count; i++)
                    {
                        CutAdmin.DataLayer.AgentMarkup objAgentMarkup = new CutAdmin.DataLayer.AgentMarkup();
                        objAgentMarkup.AgentOwnMarkupPer = Convert.ToSingle(dtTransAgentOwnMarkp.Rows[i]["Percentage"]);
                        objAgentMarkup.AgentOwnMarkupAmt = Convert.ToSingle(dtTransAgentOwnMarkp.Rows[i]["Amount"]);
                        objAgentMarkup.SupplierType = dtTransAgentOwnMarkp.Rows[i]["ServiceType"].ToString();
                        //objMarkupsAndTaxes.AgentOwnMarkupPer = Convert.ToSingle(dtAgentOwnMarkp.Rows[0]["Percentage"]);
                        //objMarkupsAndTaxes.AgentOwnMarkupAmt = Convert.ToSingle(dtAgentOwnMarkp.Rows[0]["Amount"]);
                        listAgentMarkup.Add(objAgentMarkup);
                    }
                }
                if (dtTransIndividualMarkup.Rows.Count != 0)
                {

                    objMarkupsAndTaxesTrans.listIndividualMarkup = listIndividualMarkup;
                }
                if (dtTransGroupMarkup.Rows.Count != 0)
                {
                    objMarkupsAndTaxesTrans.listGroupMarkup = listGroupMarkup;
                }
                if (dtTransGlobalMarkup.Rows.Count != 0)
                {
                    objMarkupsAndTaxesTrans.listGlobalMarkup = listGlobalMarkup;
                }
                if (dtTransAgentOwnMarkp.Rows.Count != 0)
                {
                    objMarkupsAndTaxesTrans.listAgentMarkup = listAgentMarkup;
                }
                HttpContext.Current.Session["markups"] = objMarkupsAndTaxes;
                #endregion
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        #region Destination
        [WebMethod(EnableSession = true)]
        public string GetDestinationCode(string name)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = DestinationManager.Get(name, "ENG", out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("DestinationCode"),
                    value = data.Field<String>("Destination")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        #endregion Destination

        [WebMethod(EnableSession = true)]
        public string GetHotel(string name, string destination)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CutAdmin.DataLayer.HotelManager.Get(name, destination, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("HotelCode"),
                    value = data.Field<String>("Name")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }

        #region Email Templates

        [WebMethod(EnableSession = true)]
        public string SaveTemplateAdmin(string path, string name, string bit)
        {
            try
            {
                Int64 ParentID = AccountManager.GetSuperAdminID();

                using (var DB = new Trivo_AmsHelper())
                {
                    var List = (from obj in DB.tbl_EmailTemplates where obj.nAdminID == ParentID && obj.sTemplateName == name.Replace("%20", " ") select obj).ToList();

                    if (List.Count != 0)
                    {
                        tbl_EmailTemplates Update = DB.tbl_EmailTemplates.Single(x => x.nAdminID == ParentID && x.sTemplateName == name.Replace("%20", " "));
                        DB.tbl_EmailTemplates.Remove(Update);
                        DB.SaveChanges();
                    }

                    if (bit != "true")
                    {
                        tbl_EmailTemplates Add = new tbl_EmailTemplates();

                        Add.nAdminID = ParentID;
                        Add.sTemplateName = name.Replace("%20", " ");
                        Add.sPath = path.Replace("%20", " ");
                        Add.Activate = true;
                        DB.tbl_EmailTemplates.Add(Add);
                        DB.SaveChanges();
                    }




                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;

        }

        [WebMethod(EnableSession = true)]
        public string GetTemplate(Int64 AdminID, string TemplateName)
        {
            string Doc = "";
            try
            {
                using (var DB = new Trivo_AmsHelper())
                {
                    var List = (from obj in DB.tbl_Admin where obj.ID == AdminID select obj).FirstOrDefault();
                    var ListName = (from obj in DB.tbl_EmailTemplates where obj.nAdminID == AdminID && obj.sTemplateName == TemplateName select obj).FirstOrDefault();

                    string ServerPath = List.ConntectionString + "/" + ListName.sPath;

                    bool Res = Post(ServerPath, out Doc);

                    // Doc = System.IO.File.ReadAllText(ServerPath + "/" + ListName.sPath);
                }
            }
            catch
            {
            }
            return Doc;
        }

        [WebMethod(EnableSession = true)]
        public bool Post(string data, out string response)
        {
            response = "";
            try
            {
                var oRequest = WebRequest.Create(data);
                oRequest.Method = "GET";
                oRequest.Timeout = 300000;
                using (var oResponse = oRequest.GetResponse())
                {
                    using (var oResponseStream = oResponse.GetResponseStream())
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(oResponseStream);
                        response = sr.ReadToEnd();
                        if (response.Contains("error"))
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
            }
            catch (WebException oWebException)
            {
                if (oWebException.Response != null)
                {
                    using (var oResponseStream = oWebException.Response.GetResponseStream())
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(oResponseStream);
                        response = sr.ReadToEnd();
                    }
                }
                return false;
            }
            catch (Exception oException)
            {
                response = oException.Message;
                return false;
            }
        }


        [WebMethod(EnableSession = true)]
        public string SendPackagesDetails(string ID, string Email,string Name , string Remark)
        {
            string Doc = ""; Int64 nID = Convert.ToInt64(ID);
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            Int64 AdminSuperID = AccountManager.GetSuperAdminID();
            Int64 AdminID = AccountManager.GetAdminByLogin();
            CUT.DataLayer.GlobalDefault objGlobalDefaults = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
            if (objGlobalDefaults == null)
            {
                return objSerializer.Serialize(new { Session = 0 });
            }
            try
            {
                DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.SUCCESS;
                #region List_PackageDetail
                DataTable dtResult = new DataTable();
                retCode = PackageManager.SinglePackageDetails(nID, out dtResult);

                var List_PackageDetail = dtResult.AsEnumerable()
                     .Select(data => new
                     {
                         nID = data.Field<Int64>("nID"),
                         sPackageName = data.Field<string>("sPackageName"),
                         sPackageDestination = data.Field<string>("sPackageDestination"),
                         sPackageCategory = data.Field<string>("sPackageCategory"),
                         sPackageThemes = data.Field<string>("sPackageThemes"),
                         nDuration = data.Field<Int64>("nDuration"),
                         sPackageDescription = data.Field<string>("sPackageDescription"),
                         dValidityFrom = data.Field<string>("dValidityFrom"),
                         dValidityTo = data.Field<string>("dValidityTo"),
                         dTax = data.Field<Decimal>("dTax"),
                         nCancelDays = data.Field<Int64>("nCancelDays"),
                         dCancelCharge = data.Field<Decimal>("dCancelCharge"),
                         sTermsCondition = data.Field<String>("sTermsCondition"),
                         sInventory = data.Field<String>("sInventory"),
                         IsDomestic = data.Field<bool>("IsDomestic"),
                         Transfer = data.Field<bool>("Transfer")
                     }).ToList();

                #endregion

                #region List_pricingDetail
                retCode = PackageManager.GetPricingDetail(nID, out dtResult);

                var List_pricingDetail = dtResult.AsEnumerable()
                     .Select(data => new
                     {
                         nID = data.Field<Int64>("nID"),
                         nPackageID = data.Field<Int64>("nPackageID"),
                         nCategoryID = data.Field<Int64>("nCategoryID"),
                         sCategoryName = data.Field<string>("sCategoryName"),
                         dSingleAdult = data.Field<Decimal>("dSingleAdult").ToString() == "0.00" ? "" : data.Field<Decimal>("dSingleAdult").ToString(),
                         dCouple = data.Field<Decimal>("dCouple").ToString() == "0.00" ? "" : data.Field<Decimal>("dCouple").ToString(),
                         dExtraAdult = data.Field<Decimal>("dExtraAdult").ToString() == "0.00" ? "" : data.Field<Decimal>("dExtraAdult").ToString(),
                         dInfantKid = data.Field<Decimal>("dInfantKid").ToString() == "0.00" ? "" : data.Field<Decimal>("dInfantKid").ToString(),
                         dKidWBed = data.Field<Decimal>("dKidWBed").ToString() == "0.00" ? "" : data.Field<Decimal>("dKidWBed").ToString(),
                         dKidWOBed = data.Field<Decimal>("dKidWOBed").ToString() == "0.00" ? "" : data.Field<Decimal>("dKidWOBed").ToString(),
                         sStaticInclusions = data.Field<string>("sStaticInclusions").Trim(','),
                         sDynamicInclusion = data.Field<string>("sDynamicInclusion").Trim(','),
                         sStaticExclusion = data.Field<string>("sStaticExclusion").Trim(','),
                         sDynamicExclusion = data.Field<string>("sDynamicExclusion").Trim(',')
                     }).ToList();
                Int64 PricingDetailnCount = Convert.ToInt64(dtResult.Rows.Count);
                #endregion

                #region List_ItineraryDetail
                retCode = PackageManager.GetItineraryDetail(nID, out dtResult);

                var List_ItineraryDetail = dtResult.AsEnumerable()
                     .Select(data => new
                     {
                         nID = data.Field<Int64>("nID"),
                         nPackageID = data.Field<Int64>("nPackageID"),
                         nCategoryID = data.Field<Int64>("nCategoryID"),
                         nDuration = data.Field<Int64>("nDuration"),
                         sCategoryName = data.Field<String>("nCategoryName"),
                         sItinerary_1 = data.Field<string>("sItinerary_1"),
                         sItinerary_2 = data.Field<string>("sItinerary_2"),
                         sItinerary_3 = data.Field<string>("sItinerary_3"),
                         sItinerary_4 = data.Field<string>("sItinerary_4"),
                         sItinerary_5 = data.Field<string>("sItinerary_5"),
                         sItinerary_6 = data.Field<string>("sItinerary_6"),
                         sItinerary_7 = data.Field<string>("sItinerary_7"),
                         sItinerary_8 = data.Field<string>("sItinerary_8"),
                         sItinerary_9 = data.Field<string>("sItinerary_9"),
                         sItinerary_10 = data.Field<string>("sItinerary_10"),
                         sItinerary_11 = data.Field<string>("sItinerary_11"),
                         sItinerary_12 = data.Field<string>("sItinerary_12"),
                         sItinerary_13 = data.Field<string>("sItinerary_13"),
                         sItinerary_14 = data.Field<string>("sItinerary_14"),
                         sItinerary_15 = data.Field<string>("sItinerary_15")
                     }).ToList();
                Int64 ItineraryDetailnCount = dtResult.Rows.Count;
                #endregion

                #region List_ItineraryHotelDetail
                retCode = PackageManager.GetItineraryHotelDetail(nID, out dtResult);

                var List_ItineraryHotelDetail = dtResult.AsEnumerable()
                     .Select(data => new
                     {
                         nID = data.Field<Int64>("nID"),
                         nPackageID = data.Field<Int64>("nPackageID"),
                         nCategoryID = data.Field<Int64>("nCategoryID"),
                         nDuration = data.Field<Int64>("nDuration"),
                         sCategoryName = data.Field<String>("sCategoryName"),
                         sHotelName_1 = data.Field<string>("sHotelName_1"),
                         sHotelName_2 = data.Field<string>("sHotelName_2"),
                         sHotelName_3 = data.Field<string>("sHotelName_3"),
                         sHotelName_4 = data.Field<string>("sHotelName_4"),
                         sHotelName_5 = data.Field<string>("sHotelName_5"),
                         sHotelName_6 = data.Field<string>("sHotelName_6"),
                         sHotelName_7 = data.Field<string>("sHotelName_7"),
                         sHotelName_8 = data.Field<string>("sHotelName_8"),
                         sHotelName_9 = data.Field<string>("sHotelName_9"),
                         sHotelName_10 = data.Field<string>("sHotelName_10"),
                         sHotelName_11 = data.Field<string>("sHotelName_11"),
                         sHotelName_12 = data.Field<string>("sHotelName_12"),
                         sHotelName_13 = data.Field<string>("sHotelName_13"),
                         sHotelName_14 = data.Field<string>("sHotelName_14"),
                         sHotel_Code_1 = data.Field<string>("sHotel_Code_1"),
                         sHotel_Code_2 = data.Field<string>("sHotel_Code_2"),
                         sHotel_Code_3 = data.Field<string>("sHotel_Code_3"),
                         sHotel_Code_4 = data.Field<string>("sHotel_Code_4"),
                         sHotel_Code_5 = data.Field<string>("sHotel_Code_5"),
                         sHotel_Code_6 = data.Field<string>("sHotel_Code_6"),
                         sHotel_Code_7 = data.Field<string>("sHotel_Code_7"),
                         sHotel_Code_8 = data.Field<string>("sHotel_Code_8"),
                         sHotel_Code_9 = data.Field<string>("sHotel_Code_9"),
                         sHotel_Code_10 = data.Field<string>("sHotel_Code_10"),
                         sHotel_Code_11 = data.Field<string>("sHotel_Code_11"),
                         sHotel_Code_12 = data.Field<string>("sHotel_Code_12"),
                         sHotel_Code_13 = data.Field<string>("sHotel_Code_13"),
                         sHotel_Code_14 = data.Field<string>("sHotel_Code_14"),
                         sHotelDescrption_1 = data.Field<string>("sHotelDescrption_1"),
                         sHotelDescrption_2 = data.Field<string>("sHotelDescrption_2"),
                         sHotelDescrption_3 = data.Field<string>("sHotelDescrption_3"),
                         sHotelDescrption_4 = data.Field<string>("sHotelDescrption_4"),
                         sHotelDescrption_5 = data.Field<string>("sHotelDescrption_5"),
                         sHotelDescrption_6 = data.Field<string>("sHotelDescrption_6"),
                         sHotelDescrption_7 = data.Field<string>("sHotelDescrption_7"),
                         sHotelDescrption_8 = data.Field<string>("sHotelDescrption_8"),
                         sHotelDescrption_9 = data.Field<string>("sHotelDescrption_9"),
                         sHotelDescrption_10 = data.Field<string>("sHotelDescrption_10"),
                         sHotelDescrption_11 = data.Field<string>("sHotelDescrption_11"),
                         sHotelDescrption_12 = data.Field<string>("sHotelDescrption_12"),
                         sHotelDescrption_13 = data.Field<string>("sHotelDescrption_13"),
                         sHotelDescrption_14 = data.Field<string>("sHotelDescrption_14"),
                         sHotelImage1 = data.Field<string>("sHotelImage1"),
                         sHotelImage2 = data.Field<string>("sHotelImage2"),
                         sHotelImage3 = data.Field<string>("sHotelImage3"),
                         sHotelImage4 = data.Field<string>("sHotelImage4"),
                         sHotelImage5 = data.Field<string>("sHotelImage5"),
                         sHotelImage6 = data.Field<string>("sHotelImage6"),
                         sHotelImage7 = data.Field<string>("sHotelImage7"),
                         sHotelImage8 = data.Field<string>("sHotelImage8"),
                         sHotelImage9 = data.Field<string>("sHotelImage9"),
                         sHotelImage10 = data.Field<string>("sHotelImage10"),
                         sHotelImage11 = data.Field<string>("sHotelImage11"),
                         sHotelImage12 = data.Field<string>("sHotelImage12"),
                         sHotelImage13 = data.Field<string>("sHotelImage13"),
                         sHotelImage14 = data.Field<string>("sHotelImage14")
                     }).ToList();
                Int64 ItineraryHotelDetailnCount = dtResult.Rows.Count;
                #endregion

                #region List_ImageDetail
                retCode = PackageManager.GetProductImages(nID, out dtResult);

                var List_ImageDetail = dtResult.AsEnumerable()
                    .Select(data => new
                    {
                        nID = data.Field<Int64>("nID"),
                        nPackageID = data.Field<Int64>("nPackageID"),
                        ImageArray = data.Field<string>("ImageArray"),
                    }).ToList();
                #endregion

                #region List_TransferDetail
                CUT_LIVE_UATSTEntities DB = new CUT_LIVE_UATSTEntities();

                var List_TransferDetail = (from obj in DB.tbl_PackageCab where obj.PackageId == nID select obj).ToList();
                #endregion

                #region List_PackageActivityDetail
                var List_PackageActivityDetail = (from obj in DB.tbl_PackageActivity
                                                  join Det in DB.tbl_SightseeingMaster on obj.ActivityId equals Det.Activity_Id
                                                  where obj.PackageId == nID && Det.ParentID == AdminID
                                                  select new
                                                  {
                                                      obj.ActivityId,
                                                      Det.Act_Name,
                                                  }).ToList();
                Int64 PackageActivityDetailnCount = List_PackageActivityDetail.Count();
                #endregion

         
                using (var dbb = new helperDataContext())
                {
                    Int64 ParentID = AccountManager.GetSupplierByUser();
                    Int64 ContactID = 0;
                    ContactID = dbb.tbl_AdminLogins.Where(d => d.sid == ParentID).FirstOrDefault().ContactID;

                    #region SupplierDetails
                    var dtSupplierDetails = (from obj in dbb.tbl_AdminLogins
                                             join objc in dbb.tbl_Contacts on obj.ContactID equals objc.ContactID
                                             from objh in dbb.tbl_HCities
                                             where obj.ContactID == ContactID && objc.Code == objh.Code
                                             select new
                                             {
                                                 obj.AgencyName,
                                                 obj.Agentuniquecode,
                                                 objc.Address,
                                                 objc.email,
                                                 objc.Mobile,
                                                 objc.phone,
                                                 objc.PinCode,
                                                 objc.Fax,
                                                 objc.sCountry,
                                                 objc.Website,
                                                 objc.StateID,
                                                 objh.Countryname,
                                                 objh.Description
                                             }).FirstOrDefault();
                    #endregion

                    using (var db = new Trivo_AmsHelper())
                    {
                        //Doc = GetTemplate(AdminID, "Details");
                        string html = "";
                        string PriceDetailhtml = "";

                        foreach (var item in List_pricingDetail)
                        {
                            PriceDetailhtml += "<tr>";
                            PriceDetailhtml += "    <td style='width:102px;height:33px; text-align: center;'>";
                            PriceDetailhtml += "        <strong>" + item.sCategoryName + "</strong>";
                            PriceDetailhtml += "    </td>";
                            PriceDetailhtml += "    <td style='width:96px;height:33px; text-align: center;'>";
                            PriceDetailhtml += "        <strong>" + item.dSingleAdult + "</strong>";
                            PriceDetailhtml += "    </td>";
                            PriceDetailhtml += "    <td style='width:114px;height:33px; text-align: center;'>";
                            PriceDetailhtml += "        <strong>" + item.dCouple + "</strong>";
                            PriceDetailhtml += "    </td>";
                            PriceDetailhtml += "    <td style='width:96px;height:33px; text-align: center;'>";
                            PriceDetailhtml += "        <strong>" + item.dExtraAdult + "</strong>";
                            PriceDetailhtml += "    </td>";
                            PriceDetailhtml += "    <td style='width:78px;height:33px; text-align: center;'>";
                            PriceDetailhtml += "        <strong>" + item.dInfantKid + "</strong>";
                            PriceDetailhtml += "    </td>";
                            PriceDetailhtml += "    <td style='width:126px;height:33px; text-align: center;'>";
                            PriceDetailhtml += "        <strong>" + item.dKidWOBed + "</strong>";
                            PriceDetailhtml += "    </td>";
                            PriceDetailhtml += "    <td style='width:96px;height:33px; text-align: center;'>";
                            PriceDetailhtml += "        <strong>" + item.dKidWBed + "</strong>";
                            PriceDetailhtml += "    </td>";
                            PriceDetailhtml += "</tr>";
                        }

                        for (int i = 0; i < List_PackageDetail[0].nDuration; i++)
                        {
                            TemplateGenrator.GetTemplatePath(AdminSuperID, "Package_Days");
                            dynamic arrItineraryHotelDetail; string HotelImage = "";
                            string HotelDescription = ""; string HotelName = "";
                            if (List_ItineraryHotelDetail.Count!=0)
                            {
                                arrItineraryHotelDetail = List_ItineraryHotelDetail[0];

                                 HotelName = "sHotelName_" + (i + 1);
                                HotelName = arrItineraryHotelDetail.GetType().GetProperty(HotelName).GetValue(arrItineraryHotelDetail, null);

                                 HotelDescription = "sHotelDescrption_" + (i + 1);
                                HotelDescription = arrItineraryHotelDetail.GetType().GetProperty(HotelDescription).GetValue(arrItineraryHotelDetail, null);

                                 HotelImage = "sHotelImage" + (i + 1);
                                HotelImage = arrItineraryHotelDetail.GetType().GetProperty(HotelImage).GetValue(arrItineraryHotelDetail, null);
                            }

                            dynamic arrItineraryDetail; string ItenaryDetail = "";
                            string Url = "";
                            if (List_ItineraryDetail.Count!=0)
                            {
                                arrItineraryDetail = List_ItineraryDetail[0];
                                 ItenaryDetail = "sItinerary_" + (i + 1);
                                ItenaryDetail = arrItineraryDetail.GetType().GetProperty(ItenaryDetail).GetValue(arrItineraryDetail, null);
                                if (ItenaryDetail != "")
                                {
                                    ItenaryDetail = ItenaryDetail.Split('<')[1];
                                    ItenaryDetail = "<" + ItenaryDetail;
                                }

                                 Url = CutAdmin.DataLayer.AccountManager.GetAdminDomain() + "/HotelImagesFolder/" + List_ItineraryDetail[0].nPackageID + "/" + List_ItineraryDetail[0].sCategoryName + "/" + HotelImage.Replace("-", "_");
                            }

                            //  string Url = CutAdmin.DataLayer.AccountManager.GetAdminDomain() + "/HotelImagesFolder/" + "148" + "/Standard/1_0.jpg";

                            string Image = "<img width='30%'   alt='' src='" + Url + "'  />";

                         TemplateGenrator.arrParms = new Dictionary<string, string> {
                         {"Title","Days -"+(i+1) },
                         {"HotelImage", Image },
                         {"HotelName", HotelName},
                         {"HotelDescription",  HotelDescription}  ,
                         {"ItenaryDetail",ItenaryDetail},
                         };
                            html += TemplateGenrator.GetTemplate;
                        }

                        string UrlPack = CutAdmin.DataLayer.AccountManager.GetAdminDomain() + "/ImagesFolder/" + List_ItineraryDetail[0].nPackageID + "/" + List_ImageDetail[0].ImageArray.Split('^')[0].Replace("-", "_");
                        string ImagePackage = "<img width='50%'   alt='' src='" + UrlPack + "'  />";

                        string urll = AccountManager.GetAdminDomain();

                        string URLLogo = urll + "/Agencylogo/" + dtSupplierDetails.Agentuniquecode + ".jpg";
                        string ImageLogo = "<img width='50%'   alt='' src='" + URLLogo + "'  />";

                        TemplateGenrator.GetTemplatePath(AdminSuperID, "Package_Details");
                        TemplateGenrator.arrParms = new Dictionary<string, string> {
                      {"Title", List_PackageDetail[0].sPackageName},
                      {"PackFrom", List_PackageDetail[0].dValidityFrom},
                      {"PackTo", List_PackageDetail[0].dValidityTo},
                      {"Name", Name},
                       {"TermsandConditions", List_PackageDetail[0].sTermsCondition},
                      {"Email", Email},
                      {"Remark", Remark},
                      {"Logo", ImageLogo},
                      {"PackageImage", ImagePackage},
                      {"DaysDetails", html},
                      {"PriceDetail",PriceDetailhtml},
                      {"CompanyName", dtSupplierDetails.AgencyName},
                      {"CompanyAddress",dtSupplierDetails.Address},
                      {"CompanyMail", dtSupplierDetails.email},
                      {"Description", List_PackageDetail[0].sPackageDescription}  ,
                      {"Copyright", dtSupplierDetails.AgencyName},
                    };
                        string sMail = TemplateGenrator.GetTemplate;

                        CutAdmin.DataLayer.EmailManager.GenrateAttachment(sMail, List_PackageDetail[0].sPackageName.Replace(" ",""), "PackageDetail");
                        string DocPath = urll + "/PackageDetailPdf/" + List_PackageDetail[0].sPackageName.Replace(" ", "") + "_PackageDetail.pdf";

                        List<string> from = new List<string>();
                            from.Add(Convert.ToString(AccountManager.GetUserMailByAdmin()));
                            List<string> attachmentList = new List<string>();
                            attachmentList.Add(DocPath);
                           string mailbody = "Please see below attachment for details.";
                            Dictionary<string, string> Email1List = new Dictionary<string, string>();
                            string accessKey =EmailManager.GetAccessKey();
                            Email1List.Add(Email, "");
                            bool reponse = true;
                            reponse = MailManager.SendMail(accessKey, Email1List, "Package Detail", mailbody, from, attachmentList);

                        if(reponse)
                            return objSerializer.Serialize(new { Session = 1, retCode=1 });
                        else
                            return objSerializer.Serialize(new { Session = 1, retCode = 0 });
                    }
                }
            }
            catch (Exception ex)
            {
                return objSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
        }

        #endregion

        #region Get Agency
        [WebMethod(EnableSession = true)]
        public string Getagency()
        {
            try
            {
                var arrAgencyDetail = Services.AgencyDetails.GetAgencyList();
                return jsSerializer.Serialize(new { retCode = 1, arrAgencyDetail = arrAgencyDetail });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        #endregion

        [WebMethod(EnableSession = true)]
        public string BookSightseeing(tbl_SightseeingBooking arrBookingDetail)
        {
            try
            {
                ActivityManager.BookSightseeing(arrBookingDetail);
                return jsSerializer.Serialize(new { Session = 1, retCode = 1 });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
        }


        [WebMethod(EnableSession = true)]
        public string GetExchangeRate(string Currency)
        {
            double Rate = 1;
            try
            {
                CUT.DataLayer.GlobalDefault objGlobalDefault = new CUT.DataLayer.GlobalDefault();
                objGlobalDefault= (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                if (objGlobalDefault.ExchangeRate.Count!=0)
                {
                    foreach (var item in objGlobalDefault.ExchangeRate)
                    {
                        if (item.Currency== Currency)
                        {
                            //Rate = item.Exchange;
                            Rate= System.Math.Round(item.Exchange, 2);
                        }
                    }
                }
                return jsSerializer.Serialize(new { retCode = 1, Rate = Rate });
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
    }
}

