﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
using CutAdmin.EntityModal;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for OfflineBookingHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class OfflineBookingHandler : System.Web.Services.WebService
    {
        Click_Hotel DB  = new Click_Hotel();
        helperDataContext db = new helperDataContext();
        string jsonString = "";


        [WebMethod(EnableSession = true)]
        public string SaveOfflineBooking(List<tbl_BookedRoom> arrRoom, tbl_HotelReservation arrHotel, List<tbl_BookedPassenger> Guest)
        {

            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            try
            {
                GenralHandler objGenralHandler = new GenralHandler();

                objGenralHandler.GetAgentDetailsOnAdmin(arrHotel.Uid.ToString());

                retCode = HotelBookingTxnManager.HotelBookingTxn(arrHotel.ReservationID, arrHotel.ReservationDate, Convert.ToSingle(arrHotel.TotalFare), Convert.ToSingle(arrHotel.Servicecharge), Convert.ToSingle(arrHotel.TotalFare), 0, 0, 0, 0, 0, 0, 0, arrHotel.ReservationDate, arrHotel.Remarks, 0, arrHotel.Source, 0, 0, 0, "", "Vouchered");


                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    var arrLastReservationID = (from obj in db.tbl_HotelReservations select obj).ToList().Count;
                    if (arrLastReservationID == 0)
                        arrLastReservationID = 0;
                   var ReservationID = (arrLastReservationID + 1).ToString("D" + 6);
                    arrHotel.VoucherID = "VCH-" + ReservationID;
                    arrHotel.InvoiceID = "INV-" + ReservationID;
                    arrHotel.AffilateCode = "HTL-AE-" + ReservationID;

                    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    arrHotel.ExchangeValue = Convert.ToDecimal(objGlobalDefault.ExchangeRate.Single(d => d.Currency == arrHotel.SupplierCurrency));



                    db.tbl_BookedRooms.InsertAllOnSubmit(arrRoom);
                    db.SubmitChanges();

                    db.tbl_BookedPassengers.InsertAllOnSubmit(Guest);
                    db.SubmitChanges();

                    arrHotel.ReservationDate = arrHotel.ReservationDate.Split(' ')[0];
                    db.tbl_HotelReservations.InsertOnSubmit(arrHotel);
                    db.SubmitChanges();

                    CUT.Agent.DataLayer.AutoEmailOnBooking.SendMail(arrHotel.ReservationID);

                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
                else
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }


            }
            catch
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateBooking(List<tbl_BookedRoom> arrRoom, tbl_HotelReservation arrHotel, List<tbl_BookedPassenger> Guest)
        {
            try
            {
                foreach (var Passenger in Guest)
                {
                    tbl_BookedPassenger Update = db.tbl_BookedPassengers.Single(x => x.sid == Passenger.sid);
                    Update.ReservationID = Passenger.ReservationID;
                    Update.RoomNumber = Passenger.RoomNumber;
                    Update.Name = Passenger.Name;
                    Update.LastName = Passenger.LastName;
                    Update.Age = Passenger.Age;
                    Update.PassengerType = Passenger.PassengerType;
                    db.SubmitChanges();
                }

                foreach (var Rooms in arrRoom)
                {
                    tbl_BookedRoom HotelRooms = db.tbl_BookedRooms.Single(x => x.sid == Rooms.sid);
                    HotelRooms.RoomType = Rooms.RoomType;
                    HotelRooms.RoomNumber = Rooms.RoomNumber;
                    HotelRooms.BoardText = Rooms.BoardText;
                    HotelRooms.TotalRooms = Rooms.TotalRooms;
                    HotelRooms.LeadingGuest = Rooms.LeadingGuest;
                    HotelRooms.Adults = Rooms.Adults;
                    HotelRooms.Child = Rooms.Child;
                    HotelRooms.ChildAge = Rooms.ChildAge;
                    HotelRooms.CutCancellationDate = Rooms.CutCancellationDate;
                    HotelRooms.CancellationAmount = Rooms.CancellationAmount;
                    HotelRooms.CanAmtWithTax = Rooms.CanAmtWithTax;
                    HotelRooms.RoomAmount = Rooms.RoomAmount;
                    HotelRooms.RoomAmtWithTax = Rooms.RoomAmtWithTax;
                    HotelRooms.SupplierCurrency = Rooms.SupplierCurrency;
                    HotelRooms.SupplierAmount = Rooms.SupplierAmount;
                    HotelRooms.TaxDetails = Rooms.TaxDetails;
                    HotelRooms.RoomServiceTax = Rooms.RoomServiceTax;
                    db.SubmitChanges();
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";

            }
            catch
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }


        [WebMethod(EnableSession = true)]
        public string SetGSTPercent(string Amount, string AgentCode)
        {
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            List<GSTdetails> ListGstDetails = new List<GSTdetails>();

           CUT.DataLayer.MarkupTaxManager.objMarkupsAndTaxes = new CUT.DataLayer.MarkupsAndTaxes();
            MarkupTaxManager.objMarkupsAndTaxes.ListGstTax = MarkupTaxManager.GetGstList(AccountManager.GetAdminByLogin(), Convert.ToInt64(AgentCode));

            ListGstDetails = MarkupTaxManager.GetGstDetails(Convert.ToInt16(Amount), "Hotel");


            jsonString = objSerlizer.Serialize(ListGstDetails);
            return jsonString;
        }
    }
}
