﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="VisaDetails.aspx.cs" Inherits="CutAdmin.VisaDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/VisaSearch.js?v=1.3"></script>
    <script src="Scripts/VisaMaster.js"></script>
    <script src="Scripts/VisaAttachments.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>Search Visa Application</h1>
            <h2><a href="#" class="addnew"><i class="formTab fa fa-angle-left"></i></a></h2>
            <div id="filter" class="with-padding anthracitef-gradient" style="display: none;">
                <form class="form-horizontal">
                    <div class="columns">
                       <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                            <label>Ref No.:</label>
                            <div class="input full-width">
                                <input type="text" class="input-unstyled full-width" id="txt_RefNo">
                            </div>
                        </div>
                        <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                            <label>Application Name :</label>
                            <div class="input full-width">
                                <input type="text" class="input-unstyled full-width" id="txt_AppName">
                            </div>
                        </div>
                        <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                            <label>Document No.:</label>
                            <div class="input full-width">
                                <input type="text" class="input-unstyled full-width" id="txt_DocNo">
                            </div>
                        </div>
                        <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                            <label>Status:</label>
                            <div class="full-width button-height">
                                <select id="selStatus" class="select" onchange="ServiceType()">
                                    <option value="" selected="selected">Select Status</option>
                                        <option value="Under Process">Not Posted</option>
                                        <option value="Posted">Posted</option>
                                        <option value="Approved">Application Approved</option>
                                        <option value="Rejected">Application Rejected</option>
                                        <option value="Entered the in country">Visa Holder entered in country</option>
                                        <option value="Left the country">Visa Holder left the country</option>
                                        <option value="Documents Required">Documents Required for further processing</option>
                                        <option value="Processing error">Application processing error</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                            <label>From Date:</label>
                            <span class="input full-width">
                                <span class="icon-calendar"></span>
                                <input type="text" name="datepicker" id="txt_Fdate" class="input-unstyled datepicker" value="">
                            </span>
                        </div>
                        <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                            <label>To date:</label>
                            <span class="input full-width">
                                <span class="icon-calendar"></span>
                                <input type="text" name="datepicker" id="txt_Todate" class="input-unstyled datepicker" value="">
                            </span>
                        </div>
                        <div class="six-columns four-columns-tablet twelve-columns-mobile">
                            <label>Visa Type:</label>
                            <div class="full-width button-height">
                                <select id="selService" class="select" onchange="Notification()">
                                     <option value="-" selected="selected">--Salect Visa Type--</option>
                                    <option value="6">96 hours Visa transit Single Entery (Compulsory Confirm Ticket Copy)</option>
                                    <option value="1">14 days Service VISA</option>
                                    <option value="2">30 days Tourist Single Entry</option>
                                    <option value="3">30 days Tourist Multiple Entry</option>
                                    <option value="4">90 days Tourist Single Entry</option>
                                    <option value="5">90 days Tourist Multiple Entry</option>
                                </select>
                            </div>
                        </div>
                        <%--<div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>Visa Type:</label>
                            <div class="full-width button-height">
                                <select id="selService" class="form-control" onchange="Notification()">
                                    <option value="-" selected="selected">--Salect Visa Type--</option>
                                    <option value="6">96 hours Visa transit Single Entery (Compulsory Confirm Ticket Copy)</option>
                                    <option value="1">14 days Service VISA</option>
                                    <option value="2">30 days Tourist Single Entry</option>
                                    <option value="3">30 days Tourist Multiple Entry</option>
                                    <option value="4">90 days Tourist Single Entry</option>
                                    <option value="5">90 days Tourist Multiple Entry</option>
                                </select>
                            </div>
                        </div>--%>
                    </div>

                    <div class="columns">
                        <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>Agency Name:</label>
                            <div class="input full-width">
                                <input type="text" class="input-unstyled full-width" id="txt_AgentName">
                                 <input type="hidden" id="hdn_uid" />
                            </div>
                        </div>


                        <div class="three-columns twelve-columns-mobile four-columns-tablet text-alignright">
                            <button type="button" id="btn_Search" class="button anthracite-gradient" onclick="Search()">Search</button>
                            &nbsp;
                             <button type="button" id="btnExcel" class="button anthracite-gradient" onclick="ExportToExcel()">Export To Excel</button>
                            &nbsp;
                            <button type="button" id="btn_Back" class="button anthracite-gradient" onclick="Getall()">Back</button>
                        </div>

                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </hgroup>

        <div class="with-padding">
            <div class="respTable">
                <table class="table responsive-table" id="tbl_VisaDetails">

                    <thead>
                        <tr>
                            <th scope="col" class="align-center">Sr. No</th>
                            <th scope="col" class="align-center">Agent Name</th>
                            <th scope="col" class="align-center">Applicant</th>
                            <th scope="col" class="align-center">Doc.No </th>
                            <th scope="col" class="align-center">Visa Country</th>
                            <th scope="col" class="align-center">Visa Type</th>
                            <th scope="col" class="align-center">Status</th>
                            <th scope="col" class="align-center">Genrated</th>
                            <th scope="col" class="align-center">Completed</th>
                            <th scope="col" class="align-center">Application</th>
                            <th scope="col" class="align-center">Upload</th>
                            <th scope="col" class="align-center">Invoice</th>
                            <th scope="col" class="align-center">Supplier</th>

                        </tr>

                    </thead>

                    <tbody>
                        
                    </tbody>

                </table>
            </div>
        </div>

    </section>
    
    <script>
        jQuery(document).ready(function () {
            jQuery('.formTab').click(function () {
                jQuery(' #filter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #filter').slideUp();
                });
            });
        });
        jQuery(".formTab").toggleClass("fa-angle-right");
        jQuery(".formTab").removeClass("fa-angle-left");

        //jQuery(document).ready(function () {
        //    jQuery('.formTab').click(function () {
        //        jQuery(' #filter1').slideToggle();
        //        jQuery('.searchBox li a ').on('click', function () {
        //            jQuery('.filterBox #filter1').slideUp();
        //        });
        //    });
        //});
        //jQuery(".formTab").toggleClass("fa-angle-right");
        //jQuery(".formTab").removeClass("fa-angle-left");
    </script>

</asp:Content>
