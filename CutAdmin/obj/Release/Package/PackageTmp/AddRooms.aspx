﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddRooms.aspx.cs" Inherits="CutAdmin.CreateRoom" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/UploadDoc.js"></script>
       <link rel="stylesheet" href="css/styles/form.css?v=3">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
     <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
    <script src="Scripts/AddRooms.js?v=1.0"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main" >
        <hgroup id="main-title" class="thin">
            <h1>Rooms</h1>
            <hr />
        </hgroup>
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <div style="margin-top:-30px" class="with-padding hotelmap">
           
               <form class="block wizard same-height"  >

                    <h3 class="block-title">Room Details</h3>

                    <fieldset class="wizard-fieldset fields-list">
                        <legend class="legend">Room Details</legend>
                      
                            <div class="columns" style="height: 300px;">

                                <div class="four-columns  twelve-columns-mobile">

                                    <label class="input-info">Room Type:</label><br />
                                    <input type="text" id="txtRoomType" class="input validate[required]" style="width: 92%" list="RoomType" autocomplete="off" />
                                    <datalist id="RoomType"></datalist>
                                 
                                </div>
                                <div class="four-columns  twelve-columns-mobile">
                                    <label class="input-info">Room Occupacy</label>
                                    <input type="number" id="QtyOccupacy" class="input full-width  validate[required,min[1],max[30]]" value="0">
                                </div>

                                <div class="four-columns  twelve-columns-mobile">
                                    <label class="input-info">Bedding Type</label>
                                    <select id="BeddingType" name="validation-select  validate[required]" class="select full-width" data-errormessage-value-missing="Please Select Bed Type">
                                        <option value="" selected="selected" disabled>Please select</option>
                                        <option value="Single Bed">Single Bed</option>
                                        <option value="Twin Bed">Twin Bed</option>
                                        <option value="Triple Bed">Triple Bed</option>
                                        <option value="Quard Bed">Quard Bed</option>
                                        <option value="Queen Bed">Queen Bed</option>
                                        <option value="6 bed">6 bed</option>
                                         <option value="King Size">King Size</option>
                                        <option value="Not Specified">Not Specified</option>
                                    </select>
                                </div>

                                <div class="new-row four-columns  twelve-columns-mobile">
                                    <label class="input-info">Room Size:</label>
                                    <input type="text" id="RoomSize" class="input full-width" placeholder="Sqm">
                                </div>

                                <div class="four-columns  twelve-columns-mobile">
                                    <label class="input-info">Max Extra Bed Allowed:</label>
                                    <input type="number" id="MaxExtrabedAllowed" min="0" value="0" class="input full-width">
                                </div>
                                <%-- <div class="new-row four-columns">
                                          <small class="input-info">Maximum Children Allowed:</small>
                                          <input type="number" id="MaxChildsAllowed" class="input full-width" value="0">
                                      </div>--%>



                                <%--  <div class="three-columns">
                                          <small class="input-info">Adults with Childs:</small>
                                          <input type="number" id="AdultsWithChilds" class="input full-width" value="0">
                                      </div>--%>
                                <div class="four-columns twelve-columns-mobile">
                                    <label class="input-info">No Of Child Without Bed</label>
                                    <input type="number" id="AdultsWithoutChilds" min="0" value="0" class="input full-width" >
                                </div>

                                <div class="new-row twelve-columns">
                                    <label class="input-info">Room Description:</label>
                                    <textarea id="RoomDescription" class="input full-width  validate[required]" style="min-height: 100px;" data-prompt-position="topLeft"></textarea>
                                </div>

                            </div>
                       
                        <hr />
                    </fieldset>

                    <fieldset class="wizard-fieldset" id="tab_Amenities">
                        <legend class="legend">Amenities</legend>
                        <div class="columns" style="min-height: 280px;">
                            <div class="twelve-columns">
                                <label class="input-info">Select Room Amenities</label>
                                <div id="divAddAmenities" class="boxed" style="border-style: groove; border-width: 2px; background-color: white">
                                </div>
                            </div>
                            <div class="five-columns">
                                <label class="input-info">New Amenity</label><br />
                                <input type="text" id="txtAmenities" class="input full-width">
                            </div>
                            <div class="one-columns">
                                <br />
                                <span class="button icon-plus blue" onclick="SaveAmenities()"></span>
                            </div>
                        </div>
                        <hr />
                    </fieldset>

                    <fieldset class="wizard-fieldset">
                        <legend class="legend">Policies</legend>
                        <div class="">
                            <div class="columns" style="height: 300px;" id="tab_Policies">
                                <%-- <div class="four-columns">
                                          <small class="input-info">Max Extra Bed Allowed:</small>
                                          <input type="number" id="MaxExtrabedAllowed" class="input full-width" value="0">
                                      </div>--%>

                                <%-- <div class="four-columns">
                                          <small class="input-info">Bedding Type</small>
                                           <select id="BeddingType" name="validation-select" class="select full-width">
                                              <option value="-" selected="selected" disabled>Please select</option>
                                              <option value="Single Bed">Single Bed</option>
                                              <option value="Twin Bed">Twin Bed</option>
                                               <option value="Not Specified">Not Specified</option>
                                          </select>
                                      </div>--%>

                                <div class="four-columns">
                                    <label class="input-info">Smoking Allowed</label>
                                    <select id="SmokingAllowed" name="validation-select" class="select full-width">
                                        <option value="-" selected="selected" disabled>Please select</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="Not Specified">Not Specified</option>
                                    </select>
                                </div>

                                <%-- <div class="four-columns">
                                          <small class="input-info">Internet Connection</small>
                                           <select id="Interconnection" name="validation-select" class="select full-width">
                                              <option value="-" selected="selected" disabled>Please select</option>
                                               <option value="Yes">Yes</option>
                                              <option value="No">No</option>
                                               <option value="Not Specified">Not Specified</option>
                                          </select>
                                      </div>
                                --%>
                                <div class="twelve-columns">
                                    <label class="input-info">Room Notes:</label>
                                    <textarea id="RoomNotes" class="input full-width" style="min-height: 100px;"></textarea>
                                </div>

                            </div>
                        </div>
                        <hr />
                    </fieldset>

                    <fieldset class="wizard-fieldset">
                        <legend class="legend">Images</legend>
                        <div class="content-panel mobile-panels margin-bottom">
                        <div class="panel-content linen">
                            <div class="panel-control align-right">
                                <span class="progress thin" style="width: 75px">
                                    <span class="progress-bar green-gradient" style="width: 0%"></span>
                                </span>
                                You may add <span class="ImgCount">0</span> more images
                                  <input type="file" name="images[]" id="RoomImages" size="9" class="hidden" onchange="preview_images(this,'image_preview');" multiple>
                        <a class="button icon-cloud-upload margin-left file withClearFunctions" id="file-input" onclick="($('#RoomImages').click())">Add file...</a>
                            </div>
                            <div class="panel-load-target scrollable with-padding" style="height: auto">
                                <p class="message icon-info-round white-gradient">Add can add upto 10 images / photos related to this activity</p>
                                <ul class="blocks-list fixed-size-200" id="image_preview">
                                </ul>
                            </div>
                        </div>
                    </div>
                        <hr />
                        <button type="button" class="button glossy anthracite-gradient float-right" onclick="AddRoomDetails();">Add Room</button>
                    </fieldset>

                </form> 
           
        </div>
    </section>
    <script>

        $(document).ready(function () {
            // Elements
            var form = $('.wizard'),

				// If layout is centered
				centered;

            // Handle resizing (mostly for debugging)
            function handleWizardResize() {
                centerWizard(false);
            };

            // Register and first call
            $(window).on('normalized-resize', handleWizardResize);

            /*
			 * Center function
			 * @param boolean animate whether or not to animate the position change
			 * @return void
			 */
            function centerWizard(animate) {
                form[animate ? 'animate' : 'css']({ marginTop: Math.max(0, Math.round(($.template.viewportHeight - 30 - form.outerHeight()) / 2)) + 'px' });
            };

            // Initial vertical adjust
            centerWizard(false);

            // Refresh position on change step
            form.on('wizardchange', function () { centerWizard(true); });

            // Validation
            if ($.validationEngine) {
                form.validationEngine();
            }
        });

    </script>


</asp:Content>