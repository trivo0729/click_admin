﻿$(function () {
    GetMarkets();
});
function GetMarkets() {
    try {
        post("GenralHandler.asmx/GetCountry", {}, function (data) {
            $(data.Country).each(function (index, arrCountry) {
                if (index == 0)
                    $('#sel_Market').append($('<option selected="selected"></option>').val("All Market").html("All Market"));
                else
                    $('#sel_Market').append($('<option></option>').val(arrCountry.Country).html(arrCountry.Countryname));
                $('#sel_Market').change();
            });
        }, function (errordata) {
            AlertDanger(errordata.ex)
        })
    } catch (e) { AlertDanger('Something we wrong while getting Markets'); }
}
