﻿var arrHotels = new Array();
$(function () {
    setTimeout(function () {
        GetHotels();
    }, 1000)
    GetHotelFacility();
    GetRatings();
    $("#div_Opening_Time").kendoTimePicker({
        interval: 30,
        format: "HH:mm"
    }); /*Date Time Picker*/
    $("#div_Closing_Time").kendoTimePicker({
        interval: 30,
        format: "HH:mm"
    }); /*Date Time Picker*/
    if (location.href.indexOf('?') != -1) {
        $("#HotelID").val(GetQueryStringParams('sHotelID'))
        GetHotelDetails($("#HotelID").val());
    }
    else
    {
        AutoSelect("txt_name", {}, "../HotelHandler.asmx/GetContractHotelsList", function (result) {
            $("#OldHotelID").val(result)
            GetHotelDetails(result)
        })
    }
})
function GetHotels() {
    try {
        post("../handler/GenralHandler.asmx/GetHotels", { AdminID: HotelAdminID }, function (data) {
            $('#sel_Hotel').empty();
            arrHotels = data.arrHotels;
            $(data.arrHotels).each(function (index, Hotel) { // GETTING Sucees HERE
                $('#sel_Hotel').append($('<option></option>').val(Hotel.HotelID).html(Hotel.Name + " , " + Hotel.Address));
            });
            if (location.href.indexOf('?') != -1) {
                HotelCode = GetQueryStringParams('sHotelID');
                $("#sel_Hotel").val(HotelCode);
                $('#sel_Hotel').change();
            }
            else {
                HotelCode = $("#sel_Hotel").val();
                $('#sel_Hotel').change();
            }
        }, function (error_data) {// GETTING ERROR HERE
            AlertDanger(error_data.ex)
        });
    } catch (e) {
        AlertDanger('Something we wrong while getting Hotels');
    }
}

function GetHotelFacility() {
    try {
        post("../HotelHandler.asmx/GetFacilities", {}, function (result) {
            var arrFacility = result.HotelFacilities;
            $(arrFacility).each(function (index, Facility) {
                $('#sel_Facility').append($('<option></option>').val(Facility.HotelFacilityID).html(Facility.HotelFacilityName));
            });
            $("#sel_Facility").kendoMultiSelect({ filterable :true});/*kendoMultiSelect*/

        })
    } catch (e) { }
}
function GetRatings() {
    var data = [{ ID: "0Star", value: 0 }, { ID: "1Star", value: 1 }, { ID: "2Star", value: 2 }, { ID: "3Star", value: 3 }, { ID: "4Star", value: 4 }, { ID: "5Star", value: 5 }]
    $("#sel_Ratings").kendoComboBox({
        filter: "startswith",
        dataTextField: "ID",
        dataValueField: "value",
        placeholder:"Rattings",
        template: '<img  src="../img/#:data.ID #.png"/>',
        dataSource: data,
        height: 400
    });
    $(".k-widget").removeClass('validate[required]');
}
var arrFacility = new Array();
function GetHotelDetails(ID) {
    post("../HotelHandler.asmx/GetHotelByHotelId", { HotelCode: ID }, function (result) {
        arrHotels = result.HotelListbyId[0];
        /*Hotel Details */
        $('#txt_name').val(arrHotels.HotelName);
        $('#sel_Ratings').data("kendoComboBox").value(arrHotels.HotelCategory);
        $('#txt_description').val(arrHotels.HotelDescription);
        $('#txt_Address').val(arrHotels.HotelAddress);
        $('#txt_PinCode').val(arrHotels.HotelZipCode);
         
        /*arrFaility*/
        arrFacility = result.arrFacility;
        var ID = [];
        $(arrFacility).each(function () {
            ID.push(this.ID);
        });
        $('#sel_Facility').data("kendoMultiSelect").value(ID);

        /*Hotel Policy */
        $('#txtChildAgeFrm').val(arrHotels.ChildAgeFrom);
        $('#txtChildAgeTo').val(arrHotels.ChildAgeTo);
        $('#txtTripAdviserLink').val(arrHotels.TripAdviserLink);
        $('#txtHotelGroup').val(arrHotels.HotelGroup);
        $('#div_Opening_Time').data("kendoTimePicker").value(arrHotels.CheckinTime);
        $('#div_Closing_Time').data("kendoTimePicker").value(arrHotels.CheckoutTime);
        $('#txt_impnote').val(arrHotels.HotelNote);

        /*Smoking*/
        var ndSmoking = $(".Smooking").find('input:radio');
        var sSmooking = "";
        $(ndSmoking).each(function (index, Smoking) {
            if ($(Smoking).val() == arrHotels.Smooking) {
                $(Smoking).prop("checked", true);
            }
        });
        /*Pets*/
        var ndPets = $(".Pets").find('input:radio');
        $(ndPets).each(function (index, Pet) {
            if ($(Pet).val() == arrHotels.PatesAllowed) {
                $(Pet).prop("checked", true);
            }
        });

        $('#TotalRooms').val(arrHotels.TotalRooms);
        $('#TotalFloors').val(arrHotels.TotalFloors);
        $('#BuildYear').val(arrHotels.BuildYear);
       

        /*  Location Details  */
        var arrLocation = result.arrLocation;
        $("#hdnName").val(arrLocation.Name);
        $('#City_Name').val(arrLocation.CityName);
        $('#hdCountryCode').val(arrLocation.Country_Code);
        $("#lbl_country").text(arrLocation.Country_Name);
        $("#hdnlatitude").val(arrLocation.Loc_Lat);
        $("#hdnlongitude").val(arrLocation.Loc_Long);
        $("#hdnPlaceid").val(arrLocation.Loc_PlaceID);
        $("#lat_City").val(arrLocation.City_Lat);
        $("#log_City").val(arrLocation.City_Long);
        $("#CityPlaceID").val(arrLocation.City_PlaceID);
        $("#lbl_city").text(arrLocation.CityName);
        $("#txt_location").val(arrLocation.Name + ", " + arrLocation.CityName + "," + arrLocation.Country_Name)

        /*  Contact Details  */
        var arrContacts = result.ContactList;
        SetContacts(arrContacts);

        /*Load Images*/
        if (arrHotels.SubImages != null)
            LoadPreview(arrHotels.SubImages.split(','), "HotelImages/" + arrHotels.sid, 'result');

    }, function (error) {
        AlertDanger("No Details Found")
    })
}

function SetChildPolicy(elem) {
    try{
        if($(elem).is(":checked"))
        {
            $("#div_childpolicy").show(500);
        }
        else
        {
            $("#div_childpolicy").hide(500);
        }
    }catch(e){}
}


function AddContacts(Type) {
        var divRequest = '';
        divRequest += ' <div  class="div_' + Type + '"> ';
        divRequest += ' <div class="columns"><div class="six-columns twelve-columns-mobile"><input type="text"   size="9" class="input full-width Person validate[required]" data-prompt-position="topLeft" value="" placeholder="Contact Person Name*"></div>';
        divRequest += ' <div class="six-columns twelve-columns-mobile"><input type="text"  class="input full-width Email validate[required,custom[email]]" value="" placeholder="Email Id*"></div></div>';
        divRequest += '<div class="columns"><div class="four-columns twelve-columns-mobile"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" data-prompt-position="topLeft"  size="9" class="input full-width Phone validate[required]" value="" placeholder="Phone Number*"></div>';
        divRequest += ' <div class="four-columns twelve-columns-mobile"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" data-prompt-position="topLeft"  size="9" class="input full-width Mobile validate[required]" value="" placeholder="Mobile Number*"></div>';
        divRequest += ' <div class="four-columns twelve-columns-mobile"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" data-prompt-position="topLeft" size="9" class="input full-width Fax validate[required]" value="" placeholder="Fax Number"></div></div>';
        divRequest += ' <div class="columns remCF"><div class="one-columns button icon-minus-round red"  style="float:right;"> <input  class="HotelContacts " type="button" value="Less"  style="background-color: transparent; cursor:default; border: none;" /></div></div>';
        divRequest += ' <div class="clear"></div></div>';
        $('#id' + Type).append(divRequest);
        $(".remCF").on('click', function () {
            $(this).parent().remove();
        });
}

function SameAbove(elem, Patent,Child) {
    try {
        if ($(elem).is(":checked"))
        {
            var ndParent = $('.div_' + Patent);
            var ndChild = $('.div_' + Child);
            if (ndParent.length>ndChild.length)
            {
                while (ndParent.length != ndChild.length) {
                    AddContacts(Child);
                    ndChild = $('.div_' + Child);
                }
            }
            $(ndParent).each(function (r, arrParent) {
                var Person = $($(arrParent).find('.Person')).val();
                $($(ndChild[r]).find('.Person')).val(Person)

                var Phone = $($(arrParent).find('.Phone')).val();
                $($(ndChild[r]).find('.Phone')).val(Phone)

                var Mobile = $($(arrParent).find('.Mobile')).val();
                $($(ndChild[r]).find('.Mobile')).val(Mobile)

                var Email = $($(arrParent).find('.Email')).val();
                $($(ndChild[r]).find('.Email')).val(Email)

                var Fax = $($(arrParent).find('.Fax')).val();
                $($(ndChild[r]).find('.Fax')).val(Fax)
            });
        }
    } catch (e) { }
}


function SaveHotel() {
    try {
        if ($("#frmHotel").validationEngine('validate')) {
        arrHotels = new Array();
        var arrMarket = $("#sel_Facility").val();
        var HotelFacilities = "";
        $(arrMarket).each(function (index, item) {
            HotelFacilities += item + ',';
        });
        var ndSmoking = $(".Smooking").find('input:radio');
        var sSmooking = "";
        $(ndSmoking).each(function (index, Smoking) {
            if ($(Smoking).is(":checked"))
            {
                sSmooking = $(Smoking).val();
            }
        });
        var sPetesAllowed = "";
        var ndPets = $(".Pets").find('input:radio');
        $(ndPets).each(function (index, Pet) {
            if ($(Pet).is(":checked")) {
                sPetesAllowed = $(Pet).val();
            }
        });
        arrHotels = {
            sid:$("#HotelID").val(),
            HotelName: $("#txt_name").val(), HotelAddress: $("#txt_Address").val(),
            HotelCategory: $("#sel_Ratings").val()[0], PropertyTypeId: "0",
            HotelFacilities: HotelFacilities, HotelZipCode: $("#txt_PinCode").val(),
            CityId: $("#City_Name").val(), CountryId: $("#lbl_country").text(),
            HotelLangitude: $("#hdnlatitude").val(), HotelLatitude: $("#hdnlongitude").val(),
            HotelDescription: $("#txt_description").val(), CheckinTime: $("#div_Opening_Time").val(),
            CheckoutTime: $("#div_Closing_Time").val(), ChildAgeFrom: $("#txtChildAgeFrm").val(),
            ChildAgeTo: $("#txtChildAgeTo").val(), HotelImage: arrImages.toString(), SubImages: arrImages.toString(),
            DotwCode: "", HotelBedsCode: "", MGHCode: "", GRNCode: "", ExpediaCode: "",
            HotelGroup: "", TripAdviserLink: "", BuildYear: "", RenovationYear: "",
            TotalFloors: 0, HotelNote: $("#txt_impnote").val(),
            TotalRooms: 0, PatesAllowed: sPetesAllowed, LiquorPolicy: "", Smooking: sSmooking,
            ParentID: HotelAdminID, OldHotelCode: $("#OldHotelID").val()
        }
            /*array Location detail for tbl_araegroup*/
        var arrCity = {
            Name: $("#City_Name").val(),
            CountryName: $("#lbl_country").text(),
            Country_Code: $("#hdCountryCode").val(),
            latitude: $("#lat_City").val(),
            longitude: $("#log_City").val(),
            Placeid: $("#CityPlaceID").val(),
        };
            /*array Location detail for tbl_commomlocation*/
        var arrLocation = {
            LocationName: $("#hdnName").val(),
            City: $('#City_Name').val(),
            CityCode: $('#hdCountryCode').val(),
            Country: $("#lbl_country").text(),
            CountryCode: $('#hdCountryCode').val(),
            Latitude: $("#hdnlatitude").val(),
            Longitutde: $("#hdnlongitude").val(),
            Placeid: $("#hdnPlaceid").val(),
        };
        /*Get Contacts Details*/
        var arrContacts = GetContacts();
        post("Handler/HotelHandler.asmx/SaveHotel", { arrHotels: arrHotels, arrCity: arrCity, arrLocation: arrLocation, arrContacts: arrContacts }, function (result) {
            SaveDoc('images',"HotelImages/"+ result.HotelCode, function() {
                Success("Hotel Details Saved Successfully");
                setTimeout(function () {
                    window.location.href = 'HotelList.aspx';
                }, 500);
            });
        }, function() {

        })

        }
    } catch (e) { }
}

var arrType = ["HotelContacts", "ReservationContacts", "AccountsContacts"]
function GetContacts() {
    var arrContacts = new Array();
    try {
        $(arrType).each(function (index, Type) {
            var ndParent = $('.div_' + Type);
            $(ndParent).each(function (r, arrParent) {
                var Person = $($(arrParent).find('.Person')).val();
                var Phone = $($(arrParent).find('.Phone')).val();
                var Mobile = $($(arrParent).find('.Mobile')).val();
                var Email = $($(arrParent).find('.Email')).val();
                var Fax = $($(arrParent).find('.Fax')).val();
                arrContacts.push({
                    Type: Type,
                    Phone: Phone,
                    Fax: Fax,
                    ContactPerson: Person,
                    email: Email,
                    MobileNo: Mobile,
                    SupplierCode: HotelAdminID
                });
            });
        });
    } catch (e) {
        return new Array();
    }
    return arrContacts;
}

function SetContacts(arrContacts) {
    try {
        $(arrType).each(function (index, Type) {
            var arrData = $.grep(arrContacts, function (H) { return H.Type == Type })
                .map(function (H) { return H; });
          
            $(arrData).each(function (r, Data) {
                if (r !=0)
                     AddContacts(Type);
                var ndParent = $('.div_' + Type)[r];
                $($(ndParent).find('.Person')).val(Data.ContactPerson);
                $($(ndParent).find('.Phone')).val(Data.Phone);
                $($(ndParent).find('.Mobile')).val(Data.MobileNo);
                $($(ndParent).find('.Email')).val(Data.email);
                $($(ndParent).find('.Fax')).val(Data.Fax);
            });
        });
    } catch (e) {
    }
}