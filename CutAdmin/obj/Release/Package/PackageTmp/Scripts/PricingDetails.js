var global_CategoryCount = 0;
var sPricingData = [];
var globalDynamicInclusionCount = 0;
var globalDynamicExclusionCount = 0;
$(document).ready(function () {
    //GetPricingDetail(121);
});
function GetCategoryName(categoryID) {
    if (categoryID == 1) {
        return "Standard";
    }
    else if (categoryID == 2) {
        return "Deluxe";
    }
    else if (categoryID == 3) {
        return "Premium";
    }
    else if (categoryID == 4) {
        return "Luxury";
    }
}
function GetPricingDetail(nProductID) {
    $.ajax({
        url: "../handler/PackageDetailHandler.asmx/GetPricingDetail",
        type: "post",
        data: '{"nID":"' + nProductID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.Session == 0) {
                window.location.href = "Default.aspx";
            }
            if (result.retCode == 0) {
                alert("No packages found");
            }
            else if (result.retCode == 1) {
                global_CategoryCount = result.nCount;
                var sPricDetail = result.List_pricingDetail;
                sPricingData = result.List_pricingDetail;
                MakePriceTab(sPricDetail);
            }
        },
        error: function () {
        }
    });
}

function MakePriceTab(sPricDetail) {
    var sTabWidth = 100 / sPricDetail.length;
    var sTabRow = '';
    var sTabRowContent = '';
    var sclass = '';
    var sDisplay = '';
    sTabRowContent += '<ul class="tabs">';
    for (var Cat = 0; Cat < sPricDetail.length; Cat++) {
        var sTabID = GetCategoryName(sPricDetail[Cat].nCategoryID);
        if (i == 0) {
            sclass = "active";
            sDisplay = ";display:block";
        }
        else {
            sclass = "";
            sDisplay = ";";
        }
        sTabRowContent += '<li class="' + sclass + '"><a href="#' + sTabID + '">' + sTabID + '</a></li>';
    }
    sTabRowContent += '</ul>';
    sTabRowContent += '<div class="tabs-content">';
    $("#div_pricingTabContent").html(sTabRowContent);
    for (var i = 0; i < sPricDetail.length; i++) {
        var sTabID = GetCategoryName(sPricDetail[i].nCategoryID);
        sTabRowContent += '<div id="' + sTabID + '" class="with-padding">';
        sTabRowContent += '<div class="columns">';
        sTabRowContent += '<div class="four-columns twelve-columns-mobile">';
        sTabRowContent += '<label>Single:</label><div class="input full-width">';
        sTabRowContent += '<input  id="txt_PriceSingle' + sTabID + '" value="' + sPricDetail[i].dSingleAdult + '" class="input-unstyled full-width" placeholder="0.00" type="text"> ';
        sTabRowContent += '</div>';
        sTabRowContent += '</div>';
        sTabRowContent += '<div class="four-columns twelve-columns-mobile">';
        sTabRowContent += '<label>Twin Sharing</label><div class="input full-width">                                                                                                                                                                                                                         ';
        sTabRowContent += '<input  id="txt_PriceTwin' + sTabID + '" value="' + sPricDetail[i].dCouple + '" class="input-unstyled full-width" placeholder="0.00" type="text"> ';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                        ';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                                            ';
        sTabRowContent += '<div class="four-columns twelve-columns-mobile">                                                                                                                                                                                                                                  ';
        sTabRowContent += '<label>Extra Adult:</label><div class="input full-width">                                                                                                                                                                                                                     ';
        sTabRowContent += '<input id="txt_PriceExtraAdult' + sTabID + '" value="' + sPricDetail[i].dExtraAdult + '"  class="input-unstyled full-width" placeholder="0.00" type="text">                                     ';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                        ';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                                        ';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                                            ';
        sTabRowContent += '<div class="columns">                                                                                                                                                                                                                                                             ';
        sTabRowContent += '<div class="four-columns twelve-columns-mobile">                                                                                                                                                                                                                              ';
        sTabRowContent += '<label>Infant Kid:</label><div class="input full-width">                                                                                                                                                                                                                  ';
        sTabRowContent += '<input id="txt_PriceInfantKid' + sTabID + '" value="' + sPricDetail[i].dInfantKid + '" class="input-unstyled full-width" placeholder="0.00" type="text">                                     ';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                        ';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                                    ';
        sTabRowContent += '<div class="four-columns twelve-columns-mobile">                                                                                                                                                                                                                          ';
        sTabRowContent += '<label>Kid rate without bed:</label><div class="input full-width">                                                                                                                                                                                                    ';
        sTabRowContent += '<input id="txt_PriceKidWithoutBed' + sTabID + '" value="' + sPricDetail[i].dKidWBed + '" class="input-unstyled full-width" placeholder="0.00" type="text">                                     ';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                        ';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                                ';
        sTabRowContent += '<div class="four-columns twelve-columns-mobile">                                                                                                                                                                                                                      ';
        sTabRowContent += '<label>Kid rate with bed:</label><div class="input full-width">                                                                                                                                                                                                   ';
        sTabRowContent += '<input id="txt_PriceKidWithBed' + sTabID + '" value="' + sPricDetail[i].dKidWOBed + '" class="input-unstyled full-width" placeholder="0.00" type="text">                                     ';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                        ';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                            ';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                                ';
        sTabRowContent += '<div class="columns listchkBox">                                                                                                                                                                                                                                      ';
        sTabRowContent += '<div class="six-columns twelve-columns-mobile">                                                                                                                                                                                                                   ';
        sTabRowContent += '<strong>Inclusions:</strong>                                                                                                                                                                                                                                  ';
        sTabRowContent += '<ul id="tblDynamicInclusion' + sTabID + '">';
        sTabRowContent += '<li>';
        if (sPricDetail[i].sStaticInclusions != "" && sPricDetail[i].sStaticInclusions == 1)
            sTabRowContent += '<input type="checkbox" name="Inclusion" id="chkAirfairIn' + sTabID + '" checked="checked" value="1" class="checkbox mid-margin-left">';
        else
            sTabRowContent += '<input type="checkbox" name="Inclusion" id="chkAirfairIn' + sTabID + '" value="1" class="checkbox mid-margin-left">';
        sTabRowContent += '<label for="Inclusion" class="label">Airfare with Airport transfer</label></li>                              ';
        sTabRowContent += '<li>                                                                                             ';
        if (sPricDetail[i].sStaticInclusions != "" && sPricDetail[i].sStaticInclusions == 2)
            sTabRowContent += '<input type="checkbox" name="Inclusion" id="chkSightIn' + sTabID + '" checked="checked" value="2" class="checkbox mid-margin-left">';
        else
            sTabRowContent += '<input type="checkbox" name="Inclusion" id="chkSightIn' + sTabID + '" value="2" class="checkbox mid-margin-left">';
        sTabRowContent += '<label for="Inclusion" class="label">Sight Seeing</label></li>                              ';
        sTabRowContent += '<li>                                                                                             ';
        if (sPricDetail[i].sStaticInclusions != "" && sPricDetail[i].sStaticInclusions == 3)
            sTabRowContent += '<input type="checkbox" name="Inclusion" id="chkBreakfastIn' + sTabID + '" checked="checked" value="3" class="checkbox mid-margin-left">';
        else
            sTabRowContent += '<input type="checkbox" name="Inclusion" id="chkBreakfastIn' + sTabID + '" value="3" class="checkbox mid-margin-left">';
        sTabRowContent += '<label for="Inclusion" class="label">Breakfast</label></li>                                 ';
        sTabRowContent += '<li>                                                                                             ';
        if (sPricDetail[i].sStaticInclusions != "" && sPricDetail[i].sStaticInclusions == 4)
            sTabRowContent += '<input type="checkbox" name="Inclusion" id="chkGuideIn' + sTabID + '" checked="checked" value="4" class="checkbox mid-margin-left">';
        else
            sTabRowContent += '<input type="checkbox" name="Inclusion" id="chkGuideIn' + sTabID + '" value="4" class="checkbox mid-margin-left">';
        sTabRowContent += '<label for="Inclusion" class="label">Tour Guide</label></li>                                ';
        sTabRowContent += '<li>                                                                                             ';
        if (sPricDetail[i].sStaticInclusions != "" && sPricDetail[i].sStaticInclusions == 5)
            sTabRowContent += '<input type="checkbox" name="Inclusion" id="chkLunchIn' + sTabID + '" checked="checked" value="5" class="checkbox mid-margin-left">';
        else
            sTabRowContent += '<input type="checkbox" name="Inclusion" id="chkLunchIn' + sTabID + '" value="5"  class="checkbox mid-margin-left">';
        sTabRowContent += '<label for="Inclusion" class="label">Lunch</label></li>                                     ';
        sTabRowContent += '<li>                                                                                             ';
        if (sPricDetail[i].sStaticInclusions != "" && sPricDetail[i].sStaticInclusions == 6)
            sTabRowContent += '<input type="checkbox" name="Inclusion" id="chkDinnerIn' + sTabID + '" checked="checked" value="6" class="checkbox mid-margin-left">';
        else
            sTabRowContent += '<input type="checkbox" name="Inclusion" id="chkDinnerIn' + sTabID + '" value="6" class="checkbox mid-margin-left">';

        sTabRowContent += '<label for="Inclusion" class="label">Dinner</label></li>                                    ';
        sTabRowContent += '</ul>    ';
        sTabRowContent += '<div class="clear">&nbsp;</div>                                                                                                                                                                                           ';
        sTabRowContent += '<div class="columns">                                                                                                                                                                                                     ';
        sTabRowContent += '<div class="nine-columns ten-columns-mobile">                                                                                                                                                                         ';
        sTabRowContent += '<div class="input full-width">                                                                                                                                                                                    ';
        sTabRowContent += '<input value="" id="txt_AddInclusion' + sTabID + '" class="input-unstyled full-width" type="text">';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                ';
        sTabRowContent += '</div>                                                                                                                                                                                                            ';
        sTabRowContent += '<div class="one-column">                                                                                                                                                                                          ';
        sTabRowContent += '    <i aria-hidden="true"  onclick="AddInclsion(\'' + sTabID + '\')">'
        sTabRowContent += '        <label for="pseudo-input-2" class="button anthracite-gradient"><span class="icon-plus"></span></label>'
        sTabRowContent += '    </i>'
        //sTabRowContent += '<i class="fa fa-plus button anthracite-gradient" style="cursor:pointer" onclick="AddInclsion(\'' + sTabID + '\');"></i>';
        sTabRowContent += '</div>                                                                                                                                                                                                            ';
        sTabRowContent += '</div>                                                                                                                                                                                                                ';
        sTabRowContent += '</div>                                                                                                                                                                                                                    ';
        sTabRowContent += '<div class="six-columns twelve-columns-mobile">                                                                                                                                                                           ';
        sTabRowContent += '<strong>Exclusion:</strong>                                                                                                                                                                                           ';
        sTabRowContent += '<ul id="tblDynamicExclusion' + sTabID + '">                                                                                                 ';
        sTabRowContent += '<li>                                                                                             ';
        if (sPricDetail[i].sStaticExclusion != "" && sPricDetail[i].sStaticExclusion == 1) 
            sTabRowContent += '<input type="checkbox" name="exclusion" id="chkAirfairEx' + sTabID + '" checked="checked" value="1" class="checkbox mid-margin-left">    ';
        else
            sTabRowContent += '<input type="checkbox" name="exclusion" id="chkAirfairEx' + sTabID + '" value="1" class="checkbox mid-margin-left">    ';
        sTabRowContent += '<label for="exclusion" class="label">Airfare with Airport transfer</label></li>             ';
        sTabRowContent += '<li>                                                                                             ';
        if (sPricDetail[i].sStaticExclusion != "" && sPricDetail[i].sStaticExclusion == 2) 
            sTabRowContent += '<input type="checkbox" name="exclusion" id="chkSightEx' + sTabID + '" checked="checked" value="2" class="checkbox mid-margin-left">    ';
        else
            sTabRowContent += '<input type="checkbox" name="exclusion" id="chkSightEx' + sTabID + '" value="2" class="checkbox mid-margin-left">    ';
        sTabRowContent += '<label for="exclusion" class="label">Sight Seeing</label></li>                              ';
        sTabRowContent += '<li>                                                                                             ';
        if (sPricDetail[i].sStaticExclusion != "" && sPricDetail[i].sStaticExclusion == 3) 
            sTabRowContent += '<input type="checkbox" name="exclusion" id="chkBreakfastEx' + sTabID + '" checked="checked" value="3" class="checkbox mid-margin-left">    ';
        else
            sTabRowContent += '<input type="checkbox" name="exclusion" id="chkBreakfastEx' + sTabID + '" value="3" class="checkbox mid-margin-left">    ';
        sTabRowContent += '<label for="exclusion" class="label">Breakfast</label></li>                                 ';
        sTabRowContent += '<li>                                                                                             ';
        if (sPricDetail[i].sStaticExclusion != "" && sPricDetail[i].sStaticExclusion == 4) 
            sTabRowContent += '<input type="checkbox" name="exclusion" id="chkGuideEx' + sTabID + '" checked="checked" value="4" class="checkbox mid-margin-left">    ';
        else
            sTabRowContent += '<input type="checkbox" name="exclusion" id="chkGuideEx' + sTabID + '"  value="4" class="checkbox mid-margin-left">    ';
        sTabRowContent += '<label for="exclusion" class="label">Tour Guide</label></li>                                ';
        sTabRowContent += '<li>';
        if (sPricDetail[i].sStaticExclusion != "" && sPricDetail[i].sStaticExclusion == 5) 
            sTabRowContent += '<input type="checkbox" name="exclusion" id="chkLunchEx' + sTabID + '" checked="checked" value="5" class="checkbox mid-margin-left">    ';
        else
            sTabRowContent += '<input type="checkbox" name="exclusion" id="chkLunchEx' + sTabID + '" value="5" class="checkbox mid-margin-left">    ';
        sTabRowContent += '<label for="exclusion" class="label">Lunch</label></li>                                ';
        sTabRowContent += '<li>';
        if (sPricDetail[i].sStaticExclusion != "" && sPricDetail[i].sStaticExclusion == 6) 
            sTabRowContent += '<input type="checkbox" name="exclusion" id="chkDinnerEx' + sTabID + '" checked="checked" value="6" class="checkbox mid-margin-left">    ';
        else
            sTabRowContent += '<input type="checkbox" name="exclusion" id="chkDinnerEx' + sTabID + '" value="6" class="checkbox mid-margin-left">    ';
        sTabRowContent += '<label for="exclusion" class="label">Dinner</label></li>                                ';
        sTabRowContent += '</ul>';
        sTabRowContent += '<div class="clear">&nbsp;</div>                                                                                                                                                       ';
        sTabRowContent += '<div class="columns">                                                                                                                                                                 ';
        sTabRowContent += '<div class="nine-columns ten-columns-mobile">                                                                                                                                     ';
        sTabRowContent += '<div class="input full-width">                                                                                                                                                ';
        sTabRowContent += '<input id="txt_AddExclusion' + sTabID + '" value="" class="input-unstyled full-width" type="text">';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                ';
        sTabRowContent += '</div>                                                                                                                                                                        ';
        sTabRowContent += '<div class="one-column">                                                                                                                                                      ';
        sTabRowContent += '    <i aria-hidden="true"  onclick="AddExclusion(\'' + sTabID + '\')">'
        sTabRowContent += '        <label for="pseudo-input-2" class="button anthracite-gradient"><span class="icon-plus"></span></label>'
        sTabRowContent += '    </i>'
        //sTabRowContent += '<i class="fa fa-plus button anthracite-gradient" style="cursor:pointer" onclick="AddExclusion(\'' + sTabID + '\');"></i>';
        sTabRowContent += '</div>                                                                                                                                                                        ';
        sTabRowContent += '</div>                                                                                                                                                                            ';
        sTabRowContent += '</div>                                                                                                                                                                                ';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                                ';
        sTabRowContent += '</div>                                                                                                                                                                                                                                                                   ';
    }
    sTabRowContent += '</div>';
    sTabRowContent += '    <hr />';
    sTabRowContent += '    <p class="text-alignright">';
    sTabRowContent += '        <button type="button" class="button anthracite-gradient" onclick="SavePricingDetails();">Save</button>';
    sTabRowContent += '    </p>';
    $("#div_pricingTabContent").html(sTabRowContent);


    for (var k = 0; k < sPricDetail.length; k++) {
        var sTabName = GetCategoryName(sPricDetail[k].nCategoryID);
        var sStaticInclusion = sPricDetail[k].sStaticInclusions.split(',');
        var sStaticExclusion = sPricDetail[k].sStaticExclusion.split(',');
        var sDynamicInclusion = sPricDetail[k].sDynamicInclusion.split(',');
        var sDynamicExclusion = sPricDetail[k].sDynamicExclusion.split(',');
        for (var sInc = 0; sInc < sStaticInclusion.length; sInc++) {
            setStaticInclusion(sTabName, sStaticInclusion[sInc]);
        }
        for (var sInc = 0; sInc < sStaticExclusion.length; sInc++) {
            setStaticExclusion(sTabName, sStaticExclusion[sInc]);
        }

        for (var sDynInc = 0; sDynInc < sDynamicInclusion.length; sDynInc++) {
            CreateDynamicInclusionEdit(sTabName, sDynInc, sDynamicInclusion[sDynInc]);
        }
        for (var sDynEx = 0; sDynEx < sDynamicExclusion.length; sDynEx++) {
            CreateDynamicExclusionEdit(sTabName, sDynEx, sDynamicExclusion[sDynEx]);
        }
    }
}
function AddInclsion(sTabName) {
    var inclusionName = $("#txt_AddInclusion" + sTabName).val().trim();
    if (inclusionName == "") {
        alert("Enter Inclusion");
    }
    else {
        var tRow = '<li class="Inclusion' + sTabName + '"><input type="checkbox" name="exclusion" id="chkIn' + sTabName + globalDynamicInclusionCount + '" value="' + inclusionName + '" checked="checked" class="checkbox mid-margin-left"><label for="inclusion" class="label">&nbsp;' + inclusionName + '</label></li>';
        $("#tblDynamicInclusion" + sTabName).append(tRow);
        $("#txt_AddInclusion" + sTabName).val("");
        globalDynamicInclusionCount = globalDynamicInclusionCount + 1;
    }
}
function AddExclusion(sTabName) {
    var exclusionName = $("#txt_AddExclusion" + sTabName).val().trim();
    if (exclusionName == "") {
        alert("Add a Exclusion");
    }
    else {
        var tRow = '<li class="Exclusion' + sTabName + '"><input type="checkbox" name="exclusion" id="chkEx' + sTabName + globalDynamicExclusionCount + '" value="' + exclusionName + '" checked="checked" class="checkbox mid-margin-left"><label for="exclusion" class="label">&nbsp;' + exclusionName + '</label></li>';
        $("#tblDynamicExclusion" + sTabName).append(tRow);
        $("#txt_AddExclusion" + sTabName).val("");
        globalDynamicExclusionCount = globalDynamicExclusionCount + 1;
    }
}
function setStaticInclusion(sTabName, nIndexValue) {
    if (nIndexValue == 1) {
        $("#chkAirfairIn" + sTabName).click();
    }
    else if (nIndexValue == 2) {
        $("#chkSightIn" + sTabName).click();
    }
    else if (nIndexValue == 3) {
        $("#chkBreakfastIn" + sTabName).click();
    }
    else if (nIndexValue == 4) {
        $("#chkGuideIn" + sTabName).click();
    }
    else if (nIndexValue == 5) {
        $("#chkLunchIn" + sTabName).click();
    }
    else if (nIndexValue == 6) {
        $("#chkDinnerIn" + sTabName).click();
    }
}
function setStaticExclusion(sTabName, nIndexValue) {
    if (nIndexValue == 1) {
        $("#chkAirfairEx" + sTabName).click();
    }
    else if (nIndexValue == 2) {
        $("#chkSightEx" + sTabName).click();
    }
    else if (nIndexValue == 3) {
        $("#chkBreakfastEx" + sTabName).click();
    }
    else if (nIndexValue == 4) {
        $("#chkGuideEx" + sTabName).click();
    }
    else if (nIndexValue == 5) {
        $("#chkLunchEx" + sTabName).click();
    }
    else if (nIndexValue == 6) {
        $("#chkDinnerEx" + sTabName).click();
    }
}
function CreateDynamicInclusionEdit(sTabName, nCount, inclusionName) {
    if (inclusionName == "") {
        return false;
    }
    else {
        var tRow = '<li class="Inclusion' + sTabName + '"><input type="checkbox" name="exclusion" id="chkIn' + sTabName + nCount + '" value="' + inclusionName + '" checked="checked" class="checkbox mid-margin-left"><label for="inclusion" class="label">&nbsp;' + inclusionName + '</label></li>';
        $("#tblDynamicInclusion" + sTabName).append(tRow);
        $("#txt_AddInclusion" + sTabName).val("");
    }
}
function CreateDynamicExclusionEdit(sTabName, nCount, exclusionName) {
    if (exclusionName == "") {
        return false;
    }
    else {
        var tRow = '<li class="Exclusion' + sTabName + '"><input type="checkbox" name="exclusion" id="chkEx' + sTabName + nCount + '" checked="checked" value="' + exclusionName + '" class="checkbox mid-margin-left"><label for="exclusion" class="label">&nbsp;' + exclusionName + '</label></li>';
        $("#tblDynamicExclusion" + sTabName).append(tRow);
        $("#txt_AddExclusion" + sTabName).val("");
    }
}
function SavePricingDetails() {
    ;
    for (var i = 0; i < global_CategoryCount; i++) {
        var sTabName = GetCategoryName(sPricingData[i].nCategoryID);
        var sStaticInclusion = '';
        var sStaticExclusion = '';
        var sDynamicInclusion = '';
        var sDynamicExclusion = '';
        var nDynamicInclusion = 1;
        var nDynamicExclusion = 1;
        var sSingleAdult = $("#txt_PriceSingle" + sTabName).val();
        var sCouple = $("#txt_PriceTwin" + sTabName).val();
        var sExtraAdult = $("#txt_PriceExtraAdult" + sTabName).val();
        var sInfantKid = $("#txt_PriceInfantKid" + sTabName).val();
        var sKidWBed = $("#txt_PriceKidWithoutBed" + sTabName).val();
        var sKidWOBed = $("#txt_PriceKidWithBed" + sTabName).val();

        if (sSingleAdult == "") {
            $("#txt_PriceSingle" + sTabName).focus();
            alert("Please fill single person price for category " + sTabName);
            return false;
        }
        else if (sCouple == "") {
            alert("Please fill couple price for category " + sTabName);
            $("#txt_PriceTwin" + sTabName).focus();
            return false;
        }
        else if (sExtraAdult == "") {
            alert("Please fill extra adult person price for category " + sTabName);
            $("#txt_PriceInfantKid" + sTabName).focus();
            return false;
        }
        else if (sInfantKid == "") {
            alert("Please fill infant kid price for category " + sTabName);
            $("#txt_PriceSingle" + sTabName).focus();
            return false;
        }
        else if (sKidWBed == "") {
            alert("Please kid without bed price for category " + sTabName);
            $("#txt_PriceKidWithoutBed" + sTabName).focus();
            return false;
        }
        else if (sKidWOBed == "") {
            alert("Please fill kid with bed price for category " + sTabName);
            $("#txt_PriceKidWithBed" + sTabName).focus();
            return false;
        }
    }
    var sSuccessfulEntry = 0;
    for (var i = 0; i < global_CategoryCount; i++) {
        var sTabName = GetCategoryName(sPricingData[i].nCategoryID);
        var hdpackageId = $("#hdpackageId").val();
        var sStaticInclusion = '';
        var sStaticExclusion = '';
        var sDynamicInclusion = '';
        var sDynamicExclusion = '';
        var nDynamicInclusion = 1;
        var nDynamicExclusion = 1;
        var sSingleAdult = $("#txt_PriceSingle" + sTabName).val();
        var sCouple = $("#txt_PriceTwin" + sTabName).val();
        var sExtraAdult = $("#txt_PriceExtraAdult" + sTabName).val();
        var sInfantKid = $("#txt_PriceInfantKid" + sTabName).val();
        var sKidWBed = $("#txt_PriceKidWithoutBed" + sTabName).val();
        var sKidWOBed = $("#txt_PriceKidWithBed" + sTabName).val();
        //============ Checking Static Inclusion Start ================//
        if ($("#chkAirfairIn" + sTabName).is(':checked')) {
            sStaticInclusion = sStaticInclusion + $("#chkAirfairIn" + sTabName).val() + ',';
        }
        if ($("#chkSightIn" + sTabName).is(':checked')) {
            sStaticInclusion = sStaticInclusion + $("#chkSightIn" + sTabName).val() + ',';
        }
        if ($("#chkBreakfastIn" + sTabName).is(':checked')) {
            sStaticInclusion = sStaticInclusion + $("#chkBreakfastIn" + sTabName).val() + ',';
        }
        if ($("#chkGuideIn" + sTabName).is(':checked')) {
            sStaticInclusion = sStaticInclusion + $("#chkGuideIn" + sTabName).val() + ',';
        }
        if ($("#chkLunchIn" + sTabName).is(':checked')) {
            sStaticInclusion = sStaticInclusion + $("#chkLunchIn" + sTabName).val() + ',';
        }
        if ($("#chkDinnerIn" + sTabName).is(':checked')) {
            sStaticInclusion = sStaticInclusion + $("#chkDinnerIn" + sTabName).val() + ',';
        }
        //============ Checking Static Inclusion End ================//

        //============ Checking Static Exclusion Start================//
        if ($("#chkAirfairEx" + sTabName).is(':checked')) {
            sStaticExclusion = sStaticExclusion + $("#chkAirfairEx" + sTabName).val() + ',';
        }
        if ($("#chkSightEx" + sTabName).is(':checked')) {
            sStaticExclusion = sStaticExclusion + $("#chkSightEx" + sTabName).val() + ',';
        }
        if ($("#chkBreakfastEx" + sTabName).is(':checked')) {
            sStaticExclusion = sStaticExclusion + $("#chkBreakfastEx" + sTabName).val() + ',';
        }
        if ($("#chkGuideEx" + sTabName).is(':checked')) {
            sStaticExclusion = sStaticExclusion + $("#chkGuideEx" + sTabName).val() + ',';
        }
        if ($("#chkLunchEx" + sTabName).is(':checked')) {
            sStaticExclusion = sStaticExclusion + $("#chkLunchEx" + sTabName).val() + ',';
        }
        if ($("#chkDinnerEx" + sTabName).is(':checked')) {
            sStaticExclusion = sStaticExclusion + $("#chkDinnerEx" + sTabName).val() + ',';
        }
        //============ Checking Static Exclusion End================//


        //============ Checking Dynamic Inclusion Start ================//
        $(".Inclusion" + sTabName + "").each(function () {
            if ($(this).find('input:checked').val() != undefined) {
                sDynamicInclusion += $(this).find('input:checked').val() + ',';
                nDynamicInclusion = nDynamicInclusion + 1;
            }
        });
        //============ Checking Dynamic Inclusion End ================//

        //============ Checking Dynamic Exclusion Start================//
        $(".Exclusion" + sTabName + "").each(function () {
            if ($(this).find('input:checked').val() != undefined) {
                sDynamicExclusion += $(this).find('input:checked').val() + ',';
                nDynamicExclusion = nDynamicExclusion + 1;
            }
        });
        //============ Checking Dynamic Exclusion End================//
        var globe_urlParamDecoded = $("#hdpackageId").val();
        $.ajax({
            url: "../Handler/PackageDetailHandler.asmx/UpdatePricingDetails",
            type: "post",
            data: '{"nPackageID":"' + globe_urlParamDecoded + '","nCategoryID":"' + sPricingData[i].nCategoryID + '","nCategoryName":"' + sTabName + '","sSingleAdult":"' + sSingleAdult + '","sCouple":"' + sCouple + '","sExtraAdult":"' + sExtraAdult + '","sInfantKid":"' + sInfantKid + '","sKidWBed":"' + sKidWBed + '","sKidWOBed":"' + sKidWOBed + '","sStaticInclusion":"' + sStaticInclusion + '","sDynamicInclusion":"' + sDynamicInclusion + '","sStaticExclusion":"' + sStaticExclusion + '","sDynamicExclusion":"' + sDynamicExclusion + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.Session == 0) {
                    window.location.href = "Default.aspx";
                }
                if (result.retCode == 0) {
                    Successs("No packages found");
                }
                else if (result.retCode == 1) {
                    sSuccessfulEntry = sSuccessfulEntry + 1;
                    if (sSuccessfulEntry == global_CategoryCount) {
                        Success("Entry Added Successfully, Please add Itinerary Details");
                        GetItineraryDetail(globe_urlParamDecoded);
                    }
                }
            },
            error: function () {
            }
        });
    }
    //=== End For Loop =====//
}