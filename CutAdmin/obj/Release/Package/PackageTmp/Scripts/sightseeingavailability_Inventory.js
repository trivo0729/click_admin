﻿var ActivityID = 0;
var TicketType = "";
var Language = "";
var arrRatesDetail = new Array();
var arrTourOption = new Array();
var arrChildPolicy = new Array();
var arrSlots = new Array();
var SlotCount = new Array();
var arrPax = new Array();
var days = 10;
var InventoryType = "";
var Pricing = "";
var Supplier = "";
var Currency = "";
var Startdt = "";
$(function () {
    $("#datepicker_start").datepicker({
        autoclose: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        onSelect: GenrateDatesSlots,
        minDate: "dateToday",
    });
    $("#datepicker_start").datepicker("setDate", new Date());
    var dateObject = $("#datepicker_start").datepicker('getDate', days);

    ActivityName = GetQueryStringParams('SightseeingName').replace(/%20/g, ' ');
    ActivityID = GetQueryStringParams('sSightseeingID');
    Location = GetQueryStringParams('Location');
    City = GetQueryStringParams('City').replace(/%20/g, ' ');
    Country = GetQueryStringParams('Country').replace(/%20/g, ' ');

    GetActivitylist();
    GetChildPolicy();
    $("#sel_Activity").change(function () {
        ActivityID = $("#sel_Activity").val();
        GetLanguage();
        GetTicketType();
    });
    $("#sel_TicketType").change(function () {
        TicketType = $("#sel_TicketType").val();
        //GetLanguage();
        //setTimeout(function () {
        //    GenrateDatesSlots()
        //}, 1000)
    });
    $("#Sel_Language").change(function () {
        Language = $("#Sel_Language").val();
        setTimeout(function () {
            GenrateDatesSlots()
        }, 1000)
    });
    setTimeout(function () {
        GetLanguage();
    }, 3000)


})

function GetActivitylist() {
    try {

        post("../Handler/ActivityHandller.asmx/GetActivityList", {}, function (data) {
            $('#sel_Activity').empty();
            $(data.arrActivity).each(function (index, Activity) { // GETTING Sucees HERE
                $('#sel_Activity').append($('<option></option>').val(Activity.Activity_Id).html(Activity.Act_Name));
            });
            $('#sel_Activity').val(ActivityID);
            //if (location.href.indexOf('?') != -1) {
            //    ActivityID = GetQueryStringParams('sSightseeingID');
            //}
            //else {
            //    ActivityID = $("#sel_Activity").val();
            //}
            $('#sel_Activity').change();
        }, function (error_data) {// GETTING ERROR HERE
            AlertDanger(error_data.ex)
        });
    }
    catch (e) {
    }

}

var AgeOfChild1 = "";
var AgeOfChild2 = "";
var AgeOfInfant = "";
function GetChildPolicy() {
    var data = {
        id: ActivityID
    }
    post("../Handler/ActivityHandller.asmx/GetChildPolicy", data, function (data) {
        arrChildPolicy = data.ArrChildPolicy
        if (arrChildPolicy.length != 0) {
            AgeOfChild1 = arrChildPolicy[0].Child_Age_Upto + " " + "to" + arrChildPolicy[0].Child_Age_From + " yrs";
            AgeOfChild2 = arrChildPolicy[0].Small_Child_Age_Upto + " " + "to" + arrChildPolicy[0].Child_Age_Upto + " yrs";
            if (arrChildPolicy[0].Infant_Allow == true) {
                AgeOfInfant = "Below " + arrChildPolicy[0].Small_Child_Age_Upto + " yrs";
            }
        }

    }, function (error) {
        AlertDanger("server not responds..")
    })
}

var arr_Priorities = new Array();
function GetTicketType() {
    $('#sel_TicketType').empty();
    try {
        post("../Handler/ActivityHandller.asmx/GetActivitypriority", { ActivityID: ActivityID }, function (data) {
            arr_Priorities = data.dtresult;
            $(arr_Priorities).each(function (index, arrTourType) {
                $('#sel_TicketType').append($('<option></option>').val(arrTourType.Sid).html(arrTourType.PriorityType));
                $('#sel_TicketType').change();
            });
            $('#sel_TicketType').click();
        }, function (errordata) {
            alertDanger(errordata.ex)
        })
    } catch (e) { }
}

function GetLanguage() {
    $('#Sel_Language').empty();
    if ($("#Freesale_Id").is(":checked")) {
        InventoryType = $("#Freesale_Id").val();
    }
    else if ($("#Allocation_Id").is(":checked")) {
        InventoryType = $("#Allocation_Id").val();
    }
    else {
        InventoryType = $("#FreePurchased_Id").val();
    }
    try {
        post("../Handler/ActivityHandller.asmx/GetLanguage", {
            ActivityID: ActivityID,
            SupplierID: $('#Sel_Supplier').val(),
            InventoryType: InventoryType
        }, function (data) {
            arr_Language = data.dtresult;
            $(arr_Language).each(function (index, arrLanguage) {
                $('#Sel_Language').append($('<option></option>').val(arrLanguage.LanguageCode).html(arrLanguage.Languages));
                $('#Sel_Language').change();
            });
            $('#Sel_Language').click();
        }, function (errordata) {
            alertDanger(errordata.ex)
        })
    } catch (e) { }
}

function GenrateDatesSlots() {
    if (Language != "") {
        try {
            arrRatesDetail = new Array();
            SlotCount = new Array();
            /*Check Invetory Type*/
            if ($("#Freesale_Id").is(":checked")) {
                InventoryType = $("#Freesale_Id").val();
            }
            else if ($("#Allocation_Id").is(":checked")) {
                InventoryType = $("#Allocation_Id").val();
            }
            else {
                InventoryType = $("#FreePurchased_Id").val();
            }

            /*Check Price Type*/
            if ($("#rdb_Cost").is(":checked")) {
                Pricing = $("#rdb_Cost").val();
            }
            else if ($("#rdb_Selling").is(":checked")) {
                Pricing = $("#rdb_Selling").val();
            }
            else {
                Pricing = $("#rdb_Offer").val();
            }
            Startdt = $('#datepicker_start').val();
            Supplier = $('#Sel_Supplier').val();
            post("../Handler/ActivityHandller.asmx/GenrateSlots", { Startdt: Startdt, ActivityID: ActivityID, TicketType: TicketType, Supplier: Supplier, InventoryType: InventoryType, Pricing: Pricing, Language: Language }, function (data) {
                arrRatesDetail = data.arrRatesDetail;
                for (var i = 0; i < arrRatesDetail[0].SlotsCount.length; i++) {
                    SlotCount.push(arrRatesDetail[0].SlotsCount[i].ID)
                }
                if (arrRatesDetail[0].TourType.length!=0) {
                    GenratrTable();
                    GetInventoryBySlot();
                }
                else {
                    GenerateNoData();
                }
            }, function (errorData) {
                if (errorData.ex == "No Rates found") {
                    GenerateNoData();
                }
                else {
                    AlertDanger(errorData.ex)
                }
            });
        }
        catch (e) {
            AlertDanger(e.message)
        }
    }
}


function dropdown1(Tour, Rate) {
    if ($(Rate).hasClass("icon-minus-round")) {
        $("." + Tour).addClass("hidden");
        $(Rate).removeClass("icon-minus-round red")
        $(Rate).addClass("icon-plus-round black")

    }
    else {
        $("." + Tour).removeClass("hidden")
        $(Rate).addClass("icon-minus-round red");
        $(Rate).removeClass("icon-plus-round black")

    }

}
var arrSlotRate = new Array();
function GenratrTable() {
    //$("#tbl_Actlist").dataTable().fnClearTable();
    //$("#tbl_Actlist").dataTable().fnDestroy();
    $("#div_tbl_InvList").empty();
    var TourType = "";
    var html = '';
    html += '<table class="table responsive-table font11" id="tbl_Actlist" style="width:100%">'
    html += ' <thead style="width:100%"><tr>'
    html += ' <th>Tour Option</th>'
    if (arrRatesDetail[0].TourType.length != 0) {
        Currency = arrRatesDetail[0].TourType[0].Currency;
    }
    for (var i = 0; i < arrRatesDetail[0].SlotsCount.length; i++) {
        html += '<th  id="' + arrRatesDetail[0].SlotsCount[i].ID + '" style="text-align: center"><small class="black center">' + arrRatesDetail[0].SlotsCount[i].SlotsStartTime + '</small></th>'
    }
    html += '</tr></thead>'
    html += '<tbody>'
    for (var j = 0; j < arrRatesDetail[0].RateType.length; j++) {
        html += '<tr>'
        html += '<td>'
        if (arrRatesDetail[0].RateType[j].RateTypes == "TKT") {
            TourType = "Entry Ticket";
        }
        else if (arrRatesDetail[0].RateType[j].RateTypes == "SIC") {
            TourType = "Sharing (SIC)";
        }
        else if (arrRatesDetail[0].RateType[j].RateTypes == "SIC") {
            TourType = "Private (PVT)";
        }
        html += '<div class="font11 red with-small-padding" style="border-bottom: .5px solid lightgray;">' + TourType + '<div class="font12" style="float: right"><a class="  icon-marker with-tooltip black  pointer Inventory" onclick="GetMarket(\'' + arrRatesDetail[0].RateType[j].RateTypes + '\',\'' + ActivityID + '\')" title="Valid Market"></a></div></div>'
        var PaxRow = $.grep(arrRatesDetail[0].TourType, function (p) { return p.TourOption == arrRatesDetail[0].RateType[j].RateTypes })
          .map(function (p) { return p });

        var sClass = "hidden";
        var sTitle = "Show Rates";
        var sColapse = "icon-plus-round black"
        var Age = "";


        $(arrRatesDetail[0].SlotsCount).each(function (s, SlotDetails) {
            //if (s == 0) {
            var PaxRows = $.grep(arrRatesDetail[0].TourType, function (p) { return p.TourOption == arrRatesDetail[0].RateType[j].RateTypes })
     .map(function (p) { return p });

            var arrSlotRate = $.grep(PaxRows, function (p) { return p.arrSlots.ID == SlotDetails.ID && p.TourOption == arrRatesDetail[0].RateType[j].RateTypes })
            .map(function (p) { return p });
            if (arrSlotRate.length != 0 && s < arrRatesDetail[0].RateType.length) {
                $(arrSlotRate).each(function (r, SlotRate) {
                    if (r == 0)
                        html += '<div class="font11 red with-small-padding" style="border-bottom: .5px solid lightgray;">' + SlotRate.arrPaxRate.Pax_Type + '<div class="font12" style="float: right"><a class="' + sColapse + '  with-tooltip right pointer" title="' + sTitle + '"  onclick="dropdown1(\'' + arrRatesDetail[0].RateType[j].RateTypes + '\',this)"></a></div> </div>'
                    else {
                        html += '<div class="SelBox ' + SlotRate.TourOption + ' ' + sClass + '">'
                        if (SlotRate.arrPaxRate.Pax_Type == "child1") {
                            Age = AgeOfChild1;
                        }
                        else if (SlotRate.arrPaxRate.Pax_Type == "child2") {
                            Age = AgeOfChild2;
                        }
                        else if (SlotRate.arrPaxRate.Pax_Type == "infant") {
                            Age = AgeOfInfant;
                        }
                        html += '<div class="font10 with-small-padding" style = "border-bottom: .5px solid lightgray;">' + SlotRate.arrPaxRate.Pax_Type + '(' + Age + ')' + ' <div class="font12" style="float:right "><a class="with-tooltip right pointer" title=""></a></div></div>'
                        html += '</div>'
                    }

                });
            }
            // }
        });


        //$(PaxRow).each(function (index, PaxRow) {
        //    if (index == 0) {
        //        html += '<div class="font11 red with-small-padding" style="border-bottom: .5px solid lightgray;">' + PaxRow.arrPaxRate.Pax_Type + '<div class="font12" style="float: right"><a class="' + sColapse + '  with-tooltip right pointer" title="' + sTitle + '"  onclick="dropdown1(\'' + PaxRow.TourOption + PaxRow.arrSlots.ID + '\',this)"></a></div> </div>'
        //    }
        //    if (index != 0) {
        //        html += '<div class="SelBox ' + PaxRow.TourOption + PaxRow.arrSlots.ID + ' ' + sClass + '">'
        //        html += '<div class="font10 with-small-padding" style = "border-bottom: .5px solid lightgray;">' + PaxRow.arrPaxRate.Pax_Type + ' <div class="font12" style="float:right "><a class="with-tooltip right pointer" title=""></a></div></div>'
        //        html += '</div>'
        //    }
        //});

        html += '</td>'
        //Slot Rate td//
        $(arrRatesDetail[0].SlotsCount).each(function (s, SlotDetails) {
            html += '<td class="center">'
            arrSlotRate = $.grep(PaxRow, function (p) { return p.arrSlots.ID == SlotDetails.ID })
           .map(function (p) { return p });
            $(arrSlotRate).each(function (r, SlotRate) {

                if (r == 0) {
                    html += '<div class="controls show-on-parent-hover" style="height:5px">'
                    html += '<span class="button-group compact children-tooltip">'
                    html += '<a href="#" onclick="SaveAvailability(\'' + SlotRate.arrSlots.ID + '\')" class="button icon-cart" title="Inventory"></a>'
                    html += '<a href="#" onclick="Book(\'' + arrRatesDetail[0].RateType[j].RateTypes + '\',\'' + SlotRate.arrSlots.ID + '\')" class="button icon-ticket btnbook' + SlotRate.arrSlots.ID + '" title="Book"></a>'
                    html += '</span>'
                    html += '</div>'

                    html += '<div class="font11 with-small-padding align-center pointer ' + SlotRate.arrSlots.ID + 'slot" room-not-available" onclick="SetInventory(\'' + SlotRate.arrSlots.ID + '\',this,\'' + s + '\')" style="border-bottom: .5px solid lightgray;margin-bottom: 6px">'
                    html += '</div>'
                }
                else
                    html += '<div class="' + SlotRate.TourOption + ' ' + sClass + '" >';
                html += '<div class="font11 with-small-padding" style="border-bottom: .5px solid lightgray;">'
                if (SlotRate.arrPaxRate.Rate != "") {
                    html += '<input style="text-align:center" id="' + arrRatesDetail[0].RateType[j].RateTypes + '_' + SlotRate.arrSlots.ID + '_' + SlotRate.arrPaxRate.Pax_Type + '" type="text" ondblclick="Enable(this)" onchange="ChangeRate(this,\'' + SlotRate.arrSlots.ID + '\',\'' + SlotRate.arrPaxRate.Pax_Type + '\',\'' + SlotDetails.SlotsStartTime + '\')" class="input-unstyled unstyled Rate font11" size="6" value="' + SlotRate.arrPaxRate.Rate + '" readonly="true" style="text-align: center">'
                }
                else {
                    html += '<input style="text-align:center" type="text" class="input-unstyled unstyled Rate font11" size="6" value="0.00" readonly="true" ondblclick="Enable(this)" style="text-align: center" disabled="">'
                }
                html += '</div>'
            });
            html += '</td>'
        });
        html += '</tr>'
    }
    html += '</tbody>'
    html += '</table>'
    $("#div_tbl_InvList").append(html);
    $("#tbl_Actlist").dataTable(
       {
           bSort: false,
           sPaginationType: 'full_numbers',
           bSearch: false,
           scrollY: "200px",
           scrollX: true,
           scrollCollapse: true,
           paging: true,
       });
    $("#tbl_Actlist").removeAttr("style");

    $('.Inventory').menuTooltip($('#Inventoryfilter').hide(), {
        classes: ['with-small-padding', 'full-width', 'anthracite-gradient']
    });

}
function Enable(ID) {
    $(ID).prop("readonly", "")
}

//function SetInventory(ActivityID, ActivityName, ActivityLocation, ActivityCity, ActivityCountry, RateType, TicketType) {
//    try {
//        $("#txt_ActivityID").val(ActivityID);
//        $("#txt_ActivityName").val(ActivityName);
//        $("#txt_ActivityLocation").val(ActivityLocation);
//        $("#txt_ActivityCity").val(ActivityCity);
//        $("#txt_ActivityCountry").val(ActivityCountry);
//        $("#txt_RateType").val(RateType);
//        $("#txt_TicketType").val(TicketType);
//    } catch (e) { }
//}

//function AddInventory() {
//    try {
//        if ($("#frm_Inventory").validationEngine('validate')) {
//            window.location.href = $("#sel_Inventory").val() + "?AID=" + $("#txt_ActivityID").val() + "&AName=" + $("#txt_ActivityName").val() + "&Location=" + $("#txt_ActivityLocation").val() + "&City=" + $("#txt_ActivityCity").val() + "&Country=" + $("#txt_ActivityCountry").val() + "&RateType=" + $("#txt_RateType").val() + "&TicketType=" + $("#txt_TicketType").val();
//        }
//    }
//    catch (e) {
//    }
//}

function GenerateNoData() {
    //$("#tbl_Actlist").dataTable().fnClearTable();
    //$("#tbl_Actlist").dataTable().fnDestroy();
    $("#div_tbl_InvList").empty();
    var TourType = "";
    var html = '';
    html += '<table class="table responsive-table font11" id="tbl_Actlist" style="width:100%">'
    html += ' <thead style="width:100%"><tr>'
    html += ' <th></th>'
    html += '</tr></thead>'
    html += '<tbody>'
    html += '<tr>'
    html += '<td class="align-center">No rates available for this selected inventory </td>'
    html += '</tr>'
    html += '</tbody>'
    html += '</table>'
    $("#div_tbl_InvList").append(html);
    $("#tbl_Actlist").dataTable(
       {
           bSort: false,
           sPaginationType: 'full_numbers',
           bSearch: false,
           scrollY: "200px",
           scrollX: true,
           scrollCollapse: true,
           paging: true,
       });
    $("#tbl_Actlist").removeAttr("style");

    $('.Inventory').menuTooltip($('#Inventoryfilter').hide(), {
        classes: ['with-small-padding', 'full-width', 'anthracite-gradient']
    });

}

function GetMarket(TourType, ActivityID) {
    try {
        $('#sel_Market').val("");
        if ($("#Freesale_Id").is(":checked")) {
            InventoryType = $("#Freesale_Id").val();
        }
        post("../handler/SightseeingInventoryHandler.asmx/GetValidMarket",
        {
            FromDate: $('#datepicker_start').val(),
            ActivityID: ActivityID,
            TourType: TourType,
            TicketType: $("#sel_TicketType").val(),
            InventoryType: InventoryType,
            Supplier: $("#Sel_Supplier").val(),
        }, function (result) {
            $('#sel_Market').val(result.arrMarket);
        }, function (error) {
            AlerDanger(error.ex)
        });
    }
    catch (ex) {
        AlerDanger(ex.message)
    }
}

function SaveAvailability(SlotID) {
    var sPage = "";
    var sInvtype = '';
    if (Freesale_Id.checked) {
        sPage = "freesale";
        sInvtype = "FR"
    }
    if (Allocation_Id.checked) {
        sPage = "Allocation";
        sInvtype = "AL"
    }

    if (FreePurchased_Id.checked) {
        sPage = "freepurchased";
        sInvtype = "FP"
    }
    $.modal({
        title: sPage + "-   " + ActivityName + ':<label class="green underline right" style="margin-left: 1%;">' + ActivityName + '</label><br/>',
        url: 'SightseeingInventory_' + sPage + '.aspx?AID=' + ActivityID + '&AName=' + ActivityName + '&Suppler=' + Supplier + '&TicketType=' + TicketType + '&sIntype=' + sInvtype + '&SlotId=' + SlotID + '&Location=' + Location + '&City=' + City + '&Country=' + Country + "&Modal=yes",
        useIframe: true,
        resizable: false,
        scrolling: true,
        width: 800,
        height: 600,
        spacing: 5,
        classes: ['black-gradient'],
        animateMove: 1,
        buttons: {

            'Close': {
                classes: 'black-gradient small hidden',
                click: function (modal) { modal.closeModal(); }
            },
        }
    });

}

function GetInventoryBySlot() {
    try {
        postasync("../handler/SightseeingInventoryHandler.asmx/GetInventoryBySlot",
        { Startdt: Startdt, InventoryType: InventoryType, ActivityID: ActivityID, arrSlots: SlotCount, TicketType: TicketType, SupplierID: Supplier }, function (Successsdata) {
            var result = Successsdata;
            $(result.arrRates).each(function (index, Slot) {
                debugger
                var arrInventory = $.grep(result.arrRates, function (H) { return H.SlotId == Slot.SlotId })
                            .map(function (H) { return H; });
                if (arrInventory.length != 0) {
                    $(arrInventory).each(function (d, Inventory) {
                        var sTicketType = "";
                        if (Inventory.Type.split('_').length == 2)
                            sTicketType = Inventory.Type.split('_')[1]
                        var cssClass = "";
                        var TicketCount = "0";
                        if (Inventory.InventoryType == "0") {
                            cssClass = "room-not-available"
                            $(".btnbook" + Inventory.SlotId + "").hide();
                        }
                        if (Inventory.InventoryType == "FreeSale") {
                            TicketCount = Inventory.Type.split('_')[0];
                            if (Inventory.discount == "ss") {
                                cssClass = "room-not-available"
                                TicketCount = TicketCount
                                $(".btnbook" + Inventory.SlotId + "").hide();
                            }
                            else
                                cssClass = "room-available"
                        }
                        else if (Inventory.InventoryType != "0") {
                            TicketCount = Inventory.Type.split('_')[0];
                            if ((Inventory.discount == "ss" || Inventory.NoOfCount == 0) && Inventory.InventoryType == "Allocation") {
                                cssClass = "room-not-available"
                                TicketCount = TicketCount + '/' + Inventory.NoOfCount
                                $(".btnbook" + Inventory.SlotId + "").hide();
                            }
                            else if (Inventory.Type != 0 && Inventory.NoOfCount != 0) {
                                cssClass = "room-available"
                                TicketCount = TicketCount + '/' + Inventory.NoOfCount
                            }
                            else {
                                cssClass = "room-not-available"
                                TicketCount = TicketCount + '/' + Inventory.NoOfCount
                                $(".btnbook" + Inventory.SlotId + "").hide();
                            }
                        }
                        //$('.' + Slot.ID + "slot").removeClass('loading');
                        $('.' + Inventory.SlotId + "slot").addClass(cssClass);
                        $('.' + Inventory.SlotId + "slot").text(TicketCount)
                        //if ($('.' + Slot.ID + "RateType" + d).val() == "")
                        //    $('.' + Slot.ID + "RateType" + d).val(sTicketType);
                    });
                }
                else {
                    $(arrInventory).each(function (c, Dates) {
                        //if (Dates.mData != "RoomName" || Dates.mData != "RoomID") {
                        //$('.' + Slot.ID + "slot").removeClass("loading");
                        $('.' + Inventory.SlotId + "slot").addClass("room-not-available");
                        $('.' + Inventory.SlotId + "slot").text("0")
                        $(".btnbook" + Inventory.SlotId + "").hide();
                        //}
                    });
                }
            });
            if (result.arrRates.length == 0) {
                //GenerateNoData();
                $(SlotCount).each(function (index, Slot) {
                    $('.' + Slot + "slot").addClass("room-not-available");
                    $('.' + Slot + "slot").text("0")
                    $(".btnbook" + Slot + "").hide();
                });
            }

        }, function (errorData) { })

    } catch (e) { }
}

var arrBooking = new Array();
function Book(RateTypes, SlotID) {

    arrBooking = {
        arrChildPolicy: arrChildPolicy,
        RateTypes: RateTypes,
        SlotID: SlotID,
        Currency: Currency,
        ActivityID: ActivityID,
        ActivityName: ActivityName,
        TicketType: TicketType,
        InventoryType: InventoryType,
        Sightseeing_Date: Startdt,
        Supplier: Supplier
    };

    $.modal({
        title: '<label class="green underline right" style="margin-left: 1%;">' + ActivityName + '</label><br/>',
        url: 'sightseeingbooking.aspx?AID=' + ActivityID + '&AName=' + ActivityName + '&Suppler=' + Supplier + '&TicketType=' + TicketType + '&SlotId=' + SlotID + '&Location=' + Location + '&City=' + City + '&Country=' + Country + "&Modal=yes",
        useIframe: true,
        resizable: false,
        scrolling: true,
        width: 900,
        height: 600,
        spacing: 5,
        classes: ['black-gradient'],
        animateMove: 1,
        buttons: {

            'Close': {
                classes: 'black-gradient small hidden',
                click: function (modal) { modal.closeModal(); }
            },
        }
    });

}


/*Individual Date Inventory Update*/
function SetInventory(SlotID, elem, index) {
    try {

        var Ticket = [];
        Ticket.push(TicketType)
        var Slot = [];
        Slot.push(SlotID);

        var InType = "";
        var html = "";
        /*Check Inventory Type*/
        if ($("#Freesale_Id").is(":checked")) {
            InType = "FreeSale";
        }
        else if ($("#Allocation_Id").is(":checked")) {
            InType = "Allocation";
        }
        else if ($("#Allotmnet_Id").is(":checked"))
            InType = "Allotment";
        /*Check Inventory Open or Close*/
        var InventoryState = "";
        if ($(elem).hasClass('room-available') && (InType == "FreeSale" || InType == "Allocation"))
            InventoryState = "ss";
        else if ($(elem).hasClass('room-not-available') && (InType == "FreeSale" || InType == "Allocation"))
            InventoryState = "fs";
        else
            InventoryState = "Allotment";

        if (InType == "FreeSale")
            html = "<b>Are you sure want to " + InventoryState.replace("fs", "Open").replace("ss", "Close") + " Inventory for Slot " + arrRatesDetail[0].SlotsCount[index].SlotsStartTime + "</b>";
        else {
            if (InType == "Allocation") {
                html += '<span class="">'
                html += '<input type="radio" class="" name="startstop" id="startradio" value="Start" checked>'
                html += '<label for="startradio" class="label">Open Inventory</label>'
                html += '<input type="radio" class="" name="startstop" id="stopradio" value="Stop">'
                html += '<label for="stopradio" class="label mid-margin-right">Close Inventory</label>'
                html += '</span>'
            }
            html += '<div class="field-block ">' +
                 ' ' +
                 ' <span class="number input small-margin-right">' +
                 '     <button type="button" class="button number-down">-</button>' +
                 '     <input type="text" value="1" size="3" class="input-unstyled validate[required,min[1],max[30]]" id="txt_MaxTicketperbook">' +
                 '<button type="button" class="button number-up">+</button>' +
                 ' </span> <small class="input-info">Input total no of rooms you want to open for booking</small>' +
              '</div>'
        }
        var MaxTicket = ""
        ConfirmModal(html, function () {
            if (InType == "Allocation" || InType == "Allotment")
                MaxTicket = $('#txt_MaxTicketperbook').val();
            if (InType == "Allocation") {
                if ($("#startradio").is(":checked"))
                    InventoryState = "fs";
                else
                    InventoryState = "ss";
            }
            var DateInvFr = []
            DateInvFr.push(Startdt.toString());
            var DateInvTo = [];
            DateInvTo.push(Startdt.toString());
            var arrDays = [];
            var data =
                 {
                     ActivityID: ActivityID,
                     Supplier: Supplier,
                     Slot: Slot,
                     TicketType: Ticket,
                     MaxTicket: MaxTicket,
                     DtTill: "",
                     DateInvFr: DateInvFr,
                     DateInvTo: DateInvTo,
                     OptTicketperDate: "",
                     InvLiveOrReq: "Live",
                     InType: InType,
                     InventoryState: InventoryState,
                     arrDays: arrDays,
                 }
            $(elem).addClass("loading");
            post("../handler/SightseeingInventoryHandler.asmx/SaveInventory", data, function (data) {
                Success("Inventory Updated Sucessfully");
                $(elem).removeClass("loading");
                if ($(elem).hasClass('room-available')) {
                    $(elem).removeClass("room-available");
                    $(elem).addClass("room-not-available");
                    $(".btnbook" + SlotID + "").hide();
                }
                else {
                    $(elem).removeClass("room-not-available");
                    $(elem).addClass("room-available");
                    $(".btnbook" + SlotID + "").show();
                }
                if ($(elem).text() != "0") {
                    $(elem).text($(elem).text() + "/" + MaxTicket);
                }


            }, function (error) {
                AlertDanger("Something Went Wrong")
            });
        })

    } catch (e) { AlertDanger(e.message) }
}

function ChangeRate(textRate, SlotID, PaxType, strTime) {
    try {
        ConfirmModal('Are you sure want to Update ' +
        PaxType + ' Rate for Slot' + strTime + ' ?',
        function (data) {
            var Supplier = $("#Sel_Supplier").val();
            var TicketType = $("#sel_TicketType").val();
            var ActivityID = $("#sel_Activity").val();
            var Startdt = $('#datepicker_start').val();
            var Enddt = $('#datepicker_start').val();
            var RateType = "S";
            if (!rdb_Selling.checked)
                RateType = "P";
            postasync("../handler/ActivityHandller.asmx/UpdateRate", {

                Date: Startdt,
                TicketType: TicketType,
                Supplier: Supplier,
                ActivityID: ActivityID,
                SlotID: SlotID,
                Rate: textRate.value,
                PaxType: PaxType,
                RateType: RateType

            }, function (successdata) {
                Success(PaxType + 'Rate Updated for Slot' + strTime)
            }, function (error) {
            })
        });
    }
    catch (e) {
    }
    $(this).addClass("confirm");
}
