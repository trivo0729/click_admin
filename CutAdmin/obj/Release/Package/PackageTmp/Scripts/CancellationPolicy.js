﻿function OpenCancellationModel(ReservationID, Status) {

    var data = {
        ReservationID: ReservationID
    }
    $.ajax({
        type: "POST",
        url: "../handler/BookingHandler.asmx/GetBookingDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            result = (typeof result) == 'string' ? eval('(' + result + ')') : result;
            var Cancle = "";
            var Cancleamnt = "";
            var Policy = "";
            if (result.retCode == 1) {
                var Detail = result.Detail;
                arrCancellationPolicy = result.BookedRooms;
                //for (var j = 0; j < arrCancellationPolicy.length; j++) {
                //    Cancle += Detail[j].CutCancellationDate.split('|');
                //    Cancleamnt += Detail[j].CancellationAmount.split('|');
                //}
                //if (Cancle.split(',').length >= 2) {
                //    var Cancle = Cancle.split(',');
                //    var Cancleamnt = Cancleamnt.split(',');
                //}
                //for (var i = 0; i < Cancle.length - 1; i++) {
                //    Policy += "<p><b>Room " + (i + 1) + "</b></p><p style='margin-left:5px'>In the event of cancellation after " + Cancle[i] + ",  Rs. " + Cancleamnt[i] + " will be applicable.<p>"
                //}
                var arrAmount = new Array();
                var arrCanDate = new Array();
                for (var i = 0; i < arrCancellationPolicy.length; i++) {
                    var arTempAmount = new Array();
                    var arTempDate = new Array();
                    //arTempAmount = arrCancellationPolicy[i].CanAmtWithTax.split("|");
                    arTempAmount = arrCancellationPolicy[i].CancellationAmount.split("|");
                    arTempDate = arrCancellationPolicy[i].CutCancellationDate.split("|");
                    if (arTempAmount.length == 1) {
                        if (i == 0) {
                            //arrAmount[0] = parseFloat(arrCancellationPolicy[i].CanAmtWithTax);
                            arrAmount[0] = parseFloat(arrCancellationPolicy[i].CancellationAmount);
                            arrCanDate[0] = arrCancellationPolicy[i].CutCancellationDate;
                        }
                        else {
                            //arrAmount[0] = (parseFloat(arrAmount[0]) + parseFloat(arrCancellationPolicy[i].CanAmtWithTax));
                            arrAmount[0] = (parseFloat(arrAmount[0]) + parseFloat(arrCancellationPolicy[i].CancellationAmount));
                            arrCanDate[0] = arrCancellationPolicy[i].CutCancellationDate;
                        }
                    }
                    else if (arTempAmount.length == 2) {
                        if (i == 0) {
                            arrAmount[0] = parseFloat(arTempAmount[0]);
                            arrCanDate[0] = arTempDate[0];
                            arrAmount[1] = parseFloat(arTempAmount[1]);
                            arrCanDate[1] = arTempDate[1];
                        }
                        else {
                            arrAmount[0] = parseFloat(arrAmount[0]) + parseFloat(arTempAmount[0]);
                            arrCanDate[0] = arTempDate[0];
                            arrAmount[1] = parseFloat(arrAmount[1]) + parseFloat(arTempAmount[1]);
                            arrCanDate[1] = arTempDate[1];
                        }
                    }
                    else if (arTempAmount.length == 3) {
                        if (i == 0) {
                            arrAmount[0] = parseFloat(arTempAmount[0]);
                            arrCanDate[0] = arTempDate[0];
                            arrAmount[1] = parseFloat(arTempAmount[1]);
                            arrCanDate[1] = arTempDate[1];
                            arrAmount[2] = parseFloat(arTempAmount[2]);
                            arrCanDate[2] = arTempDate[2];
                        }
                        else {
                            arrAmount[0] = parseFloat(arrAmount[0]) + parseFloat(arTempAmount[0]);
                            arrCanDate[0] = arTempDate[0];
                            arrAmount[1] = parseFloat(arrAmount[1]) + parseFloat(arTempAmount[1]);
                            arrCanDate[1] = arTempDate[1];
                            arrAmount[2] = parseFloat(arrAmount[2]) + parseFloat(arTempAmount[2]);
                            arrCanDate[2] = arTempDate[2];
                        }
                    }
                }
                if (Status == "Vouchered") {
                    for (var j = 0; j < arrAmount.length; j++)
                        if (arrAmount[j].toString() != 'NaN')
                            Policy += "In the event of cancellation after <b>" + arrCanDate[j] + "</b>, <b><i class=\"fa fa-inr\"></i> " + numberWithCommas(arrAmount[j].toFixed(2)) + "</b> plus Service Tax will be applicable.<br />";
                }
                else if (Status == "Booking" || Status == "Hold")
                    Policy = "No cancellation charges shall be applicable on holded booking.";
                var arrResrvation = result.HotelReservation[0];
                arrCancellationPolicy = result.BookedRooms;
                $.modal({
                    content:
                        '<div class="modal-body">' +
                        '<input type="hidden" id="hndIsCancelable" value="' + result.IsCancelable + '" >' +
                        '<input type="hidden" id="hndReservatonID" value="' + ReservationID + '" >' +
                        '<input type="hidden" id="hndCancellationAmount" value="' + result.CancellationAmount + '" >' +
                        '<input type="hidden" id="hndSupplier" value="' + arrResrvation.Source + '" >' +
                        '<input type="hidden" id="hdn_ServiceCharge" value="' + result.ServiceCharge + '" >' +
                        '<input type="hidden" id="hdn_Total" value="' + result.TotalFare + '" >' +
                        '<input type="hidden" id="hndReferenceCode" value="' + arrResrvation.ReferenceCode + '" >' +
                        '<input type="hidden" id="hdn_AffiliateCode" value="' + arrResrvation.AffilateCode + '" >' +
                        '<input type="hidden" id="hndStatus" value="' + arrResrvation.Status + '" >' +
                        '<input type="hidden" id="hdn_TotalFare" value="' + result.TotalFare + '" >' +
                        '<table class="table table-hover table-responsive" id="tbl_CancleBooking" style="width: 100%">' +
                        '<tr>' +
                        '<h4>Booking Detail</h4>' +
                        '</tr>' +
                        '<tr>' +
                        '<td style="border: none;">' +
                        '<span class="text-left">Hotel:&nbsp;&nbsp;' + arrResrvation.HotelName + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '<td style="border: none;">' +
                        '<span class="text-left">CheckIn:&nbsp;&nbsp;' + arrResrvation.CheckIn + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '<td style="border: none;">' +
                        '<span class="text-left">CheckOut:&nbsp;&nbsp;' + arrResrvation.CheckOut + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td style="border: none;">' +
                        '<span class="text-left">Passenger: &nbsp;&nbsp;' + arrResrvation.bookingname + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '<td style="border: none;">' +
                        '<span class="text-left">Location:&nbsp;&nbsp; ' + arrResrvation.City + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '<td style="border: none;">' +
                        '<span class="text-left">Nights:&nbsp;&nbsp; ' + arrResrvation.NoOfDays + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td style="border: none;">' +
                        '<span class="text-left">Booking Id:&nbsp;&nbsp; ' + ReservationID + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '<td style="border: none;">' +
                        '<span class="text-left">Booiking Date:&nbsp;&nbsp; ' + arrResrvation.ReservationDate + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '<td style="border: none;">' +
                        '<span class="text-left">Amount:&nbsp;&nbsp;' + arrResrvation.TotalFare + '</span>&nbsp;&nbsp;' +
                        '' +
                        '</td>' +
                        '</tr>' +
                        '</table>' +


                        '<table class="table table-hover table-responsive" id="tbl_CancleBooking" style="width: 100%">' +
                        '<tr>' +
                        '<h4>Cancellation Policy</h4>' +
                        '</tr>' +
                        '<tr>' +
                        '<td style="border: none;">' +
                        '<span class="text-left">' + Policy + '</span>&nbsp;&nbsp;' +
                        '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td style="border: none;">' +
                        '<textarea rows="4" cols="140" id="txtRemark" class="input full-width"></textarea>' +
                        '</td>' +
                        '</tr>' +
                        '</table>' +


                        '<table class="table table-hover table-responsive" id="tbl_CancleBooking" style="width: 100%">' +

                        '<tr>' +
                        '<td style="border: none;">' +
                        '<input id="btn_CancleBooking" type="button" value="Cancel Booking" class="button anthracite-gradient" style="width: 40%; margin-left: 30%" onclick="ProceedToCancellation();" />' +
                        '</td>' +
                        '</tr>' +
                        '</table>' +
                        '' +
                        '</div>',
                    title: 'Cancel Booking',
                    width: 700,
                    height: 400,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'huge anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                });
            }
        },
        error: function () {
            Success("something went wrong");
        }
    });




}


function ProceedToCancellation() {
    var Supplier = $("#hndSupplier").val()
    debugger;
    if ($("#btn_CancleBooking").val() == "Cancel Booking") {
        if ($("#hndIsCancelable").val() == "0") {
            Success("Sorry! You cannot cancel this booking.")
        }
        else {
            $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Do you want to Cancel Booking?</p>', function () {
                var data = {
                    ReservationID: $("#hndReservatonID").val(),
                    ReferenceCode: $("#hndReferenceCode").val(),
                    CancellationAmount: $("#hndCancellationAmount").val(),
                    BookingStatus: $("#hndStatus").val(),
                    Remark: $("#txtRemark").val(),
                    TotalFare: $("#hdn_TotalFare").val(),
                    ServiceCharge: $("#hdn_ServiceCharge").val(),
                    Total: $("#hdn_Total").val(),
                    Type: Supplier
                }
                $("#dlgLoader").css("display", "initial");
                $.ajax({
                    type: "POST",
                    url: "../handler/BookingHandler.asmx/HotelCancelBooking",
                    data: JSON.stringify(data),//'{"ReservationID":"' + $("#hndReservatonID").val() + '","ReferenceCode":"' + $("#hndReferenceCode").val() + '","CancellationAmount":"' + $("#hndCancellationAmount").val() + '","Remark":"' + $("#txtRemark").val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        result = (typeof result) == 'string' ? eval('(' + result + ')') : result;
                        if (result.retCode == 1) {
                            Success("Your booking has been cancelled. An Email has been sent with Cancelation Detail.");
                            document.getElementById("btn_CancleBooking").setAttribute("style", "Display:none");
                            setTimeout(function () { location.reload(); }, 5000);
                        }
                        else if (result.retCode == 0) {
                            Success(result.ex);
                        }
                    },
                    error: function () {
                        Success("something went wrong");
                    },
                    complete: function () {
                        $("#dlgLoader").css("display", "none");
                    }
                });
            }, function () {
                $('#modals').remove();
            });
        }
    } else if ($("#btnCancelBooking").val() == "Confirm Booking") {
        debugger;
        if ($("#hndIsConfirmable").val() == "0") {
            $('#AgencyBookingCancelModal').modal('hide')
            $('#SpnMessege').text("Sorry! You cannot confirm this booking.")
            $('#ModelMessege').modal('show')
        }
    }
}