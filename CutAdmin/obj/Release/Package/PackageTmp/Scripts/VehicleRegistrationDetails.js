﻿$(document).ready(function () {

    GetAllVehicleDetails();
    GetVehicleModel();
    $("#txt_RegistrationDate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    //$('.fa-filter').click(function () {
    //    $(' #filter').slideToggle();
    //    $('.searchBox li a ').on('click', function () {
    //        jQ$uery('.filterBox #filter').slideUp();
    //    });
    //});
});

function GetVehicleModel() {
    debugger;
    // Ajax request to get categories
    $.ajax({
        type: "POST",
        url: "../Handler/VehicleRegistrationHandler.asmx/GetVehicleModel",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            try {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    var List_VehicleModel = result.tbl_VehicleModel;
                    if (List_VehicleModel.length > 0) {
                        $("#selVehicleModal").empty();

                        var ddlRequest = '<option selected="selected" value="-">-Select Vehicle Model-</option>';
                        for (i = 0; i < List_VehicleModel.length; i++) {
                            ddlRequest += '<option   value="' + List_VehicleModel[i].ID + '">' + List_VehicleModel[i].VehicleBrand + '</option>';
                            //ddlRequest += '<option   value="' + List_VehicleModel[i].ID + '">' + List_VehicleModel[i].VehicleBrand + ' [Seatting Capacity : ' + List_VehicleModel[i].SeattingCapacity + ', Baggage Capacity : ' + List_VehicleModel[i].BaggageCapacity + ' ]</option>';
                        }
                        $("#selVehicleModal").append(ddlRequest);
                    }
                }

            }
            catch (e) { }
        },
        error: function () {
            alert("Error getting");
        }
    });

}

function GetAllVehicleDetails() {
    $("#tbl_VehicleDetails").dataTable().fnClearTable();
    $("#tbl_VehicleDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../Handler/VehicleRegistrationHandler.asmx/GetAllVehicleDetails",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_VehicleDetail = result.tbl_VehicleDetails;
                for (var i = 0; i < List_VehicleDetail.length; i++) {
                    tRow += '<tr>';
                    tRow += '<td align="center">' + (i + 1) + '</td>';
                    tRow += '<td align="center">' + List_VehicleDetail[i].Supplier + '</td>';
                    tRow += '<td align="center">' + List_VehicleDetail[i].VehicleBrand + '</td>';
                    tRow += '<td align="center">' + List_VehicleDetail[i].RegistrationValidity + '</td>';
                    tRow += '<td align="center">' + List_VehicleDetail[i].InsuranceDueDate + '</td>';
                    //tRow += '<td>Adults- ' + List_VehicleDetail[i].Adults + ', Child-' + List_VehicleDetail[i].Child + ', Infant- ' + List_VehicleDetail[i].Infant + ' </td>';
                    tRow += '<td class="align-center">'
                    tRow += '<span class="button-group">'
                    tRow +='<a class="button" style = "cursor:pointer" href = "#" ><span class="icon-pencil" title="Update" style="cursor:pointer" onclick="UpdateVehicle(\'' + List_VehicleDetail[i].ID + '\')"></span></a > ';
                    tRow += '<a class="button" style="cursor:pointer" href="#"><span class="icon-trash" title="Delete" style="cursor:pointer" onclick="DeleteVehicle(\'' + List_VehicleDetail[i].ID + '\',\'' + List_VehicleDetail[i].VehicleBrand + '\')"></span></a></span>';
                    tRow += '</td>';
                    tRow +='</tr>';
                }
                $("#tbl_VehicleDetails tbody").html(tRow);
                $("#tbl_VehicleDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_VehicleDetails").removeAttribute("style")
            }
            else if (result.retCode == 0) {
                $("#tbl_VehicleDetails tbody").remove();
                var tRow = '<tbody>';
                tRow += '<tr> <td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_VehicleDetails").append(tRow);

                $("#tbl_VehicleDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
        }

    });
}
function Search() {
    $("#tbl_VehicleDetails").dataTable().fnClearTable();
    $("#tbl_VehicleDetails").dataTable().fnDestroy();
  

    var Supplier = $("#selSupplier").val();
    var VehicleBrand = $("#selVehicleModal option:selected").text();
    //var VehicleModal = $("#selVehicleModal").val();


    var RegistrationDate = $("#txt_RegistrationDate").val();
    var data = {
        Supplier: Supplier,
        VehicleBrand: VehicleBrand,
        RegistrationDate: RegistrationDate,
    }

    $.ajax({
        type: "POST",
        url: "../Handler/VehicleRegistrationHandler.asmx/Search",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            // var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_VehicleDetail = result.List_Vehicle;
                for (var i = 0; i < List_VehicleDetail.length; i++) {
                    tRow += '<tr>';
                    tRow += '<td>' + (i + 1) + '</td>';
                    tRow += '<td>' + List_VehicleDetail[i].Supplier + '</td>';
                    tRow += '<td>' + List_VehicleDetail[i].VehicleBrand + '</td>';
                    tRow += '<td>' + List_VehicleDetail[i].RegistrationValidity + '</td>';
                    tRow += '<td>' + List_VehicleDetail[i].InsuranceDueDate + '</td>';
                    //tRow += '<td>Adults- ' + List_VehicleDetail[i].Adults + ', Child-' + List_VehicleDetail[i].Child + ', Infant- ' + List_VehicleDetail[i].Infant + ' </td>';

                    tRow += '<td data-title="Update" style="text-align:center;"><a style="cursor:pointer" href="#"><span class="icon-pencil" title="Update" style="cursor:pointer" onclick="UpdateVehicle(\'' + List_VehicleDetail[i].ID + '\')"></span></a></td>';

                    tRow += '<td data-title="Delete" style="text-align:center;"><a style="cursor:pointer" href="#"><span class="icon-trash" title="Delete" style="cursor:pointer" onclick="DeleteVehicle(\'' + List_VehicleDetail[i].ID + '\',\'' + List_VehicleDetail[i].VehicleBrand + '\')"></span></a></td>';

                    tRow += '</tr>';
                }
                $("#tbl_VehicleDetails tbody").html(tRow);
                $("#tbl_VehicleDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_VehicleDetails").removeAttribute("style")
            }
            else if (result.retCode == 0) {
                $("#tbl_VehicleDetails tbody").remove();
                var tRow = '<tbody>';
                //tRow += '<tr><td align="center" style="padding-top: 2%" colspan="4"><span><b>No record found</b></span></td></tr>';
                tRow += '<tr> <td align="center" colspan="6" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_VehicleDetails").append(tRow);

                //$("#tbl_VehicleDetails").dataTable({
                //    "bSort": false
                //});
            }
        },
        error: function () {
            //Success("An error occured while loading details.")
        }
    });


}
function ExportVehicleDetailsToExcel(Type) {
    debugger;
    window.location.href = "../Handler/ExportToExcelHandler.ashx?datatable=VehicleDetails&Type=" + Type;
}
//function ExportExcel(Document) {
//    debugger;

//    var Type = $('#TransactionType').val();
//    var RefNo = $('#hdnDCode').val();
//    var From = $('#datepicker3').val();
//    var To = $('#datepicker4').val();

//    if (Type == "Both" && From == "" && To == "" && RefNo == "") {
//        SearchBit = false;
//    }

//    if (SearchBit == false) {
//        window.location.href = "ExportToExcelHandler.ashx?datatable=AgencyStatement&uid=" + RefNo + "&dFrom=" + From + "&dTo=" + To + "&TransType=" + Type + "&Document=" + Document;
//    }
//    else {
//        window.location.href = "ExportToExcelHandler.ashx?datatable=AgencyStatement&uid=" + RefNo + "&dFrom=" + From + "&dTo=" + To + "&TransType=" + Type + "&Document=" + Document;
//    }

//    //   window.location.href = "ExportToExcelHandler.ashx?datatable=AgencyStatement&sid=" + sid + "&dFrom=" + dFrom + "&dTo=" + dTo + "&TransType=" + TransType;

//}


function UpdateVehicle(ID) {

    window.location.href = "VehicleRegistration.aspx?ID=" + ID;

}



function DeleteVehicle(ID, VehicleBrand) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete<br/> <span class=\"orange\">' + VehicleBrand + '</span>?</span>?</p>',
        function () {

            $.ajax({
                url: "../Handler/VehicleRegistrationHandler.asmx/DeleteVehicleByID",
                type: "post",
                data: '{"ID":"' + ID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Vehicle has been deleted successfully!");
                        window.location.reload();
                    } else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}

function reset() {
    $('#selSupplier').val('');
    $('#selVehicleModal').val('');
    $('#txt_RegistrationDate').val('');
}

