﻿$(document).ready(function () {

    GetAllQuotationDetails();


});

var List_QuotationDetail = new Array();
function GetAllQuotationDetails() {
    $("#tbl_QuotationDetails").dataTable().fnClearTable();
    $("#tbl_QuotationDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetAllQuotationDetail",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_QuotationDetail = result.Arr;
                for (var i = 0; i < List_QuotationDetail.length; i++) {
                    tRow += '<tr >';
                    tRow += '<td width="20px" class="align-center vertical-center">' + (i + 1) + '</td>';
                    tRow += '<td width="50px" class="align-center vertical-center">PQ-' + List_QuotationDetail[i].Sid + '</td>';
                    tRow += '<td width="50px" class="align-center vertical-center">' + List_QuotationDetail[i].Date + '</td>';
                    tRow += '<td width="100px" class="vertical-center">' + List_QuotationDetail[i].Companyname + '</td>';
                    tRow += '<td width="100px" class="vertical-center">' + List_QuotationDetail[i].Leadguestname + '</td>';
                    tRow += '<td width="50px" class="vertical-center align-center no-padding" style="max-width:70px;">';
                    tRow += '<div width="100%" class="black-bg">Adults <span class="small-margin-left">' + List_QuotationDetail[i].Adults + ' </span></div>';
                    tRow += '<div width="100%" class="anthracite-bg">Child <span class="small-margin-left">' + List_QuotationDetail[i].Child + '</span></div>';
                    tRow += '<div width="100%" class="grey-bg">Infant <span class="small-margin-left"> ' + List_QuotationDetail[i].Infant + '</span></div>';
                    tRow += '</td > ';
                    tRow += '<td width="50px" class="vertical-center align-center">' + List_QuotationDetail[i].location + '</td>';
                    tRow += '<td width="50px" class="vertical-center align-center">' + List_QuotationDetail[i].TourStart + '<br>to<br>' + List_QuotationDetail[i].TourEnd + '</td>';
                    tRow += '<td width="50px" class="vertical-center align-center">' + List_QuotationDetail[i].ContactPerson + '</td>';
                    tRow += '<td width="50px" class="vertical-center align-center">'
                    tRow += '<a title = "Click to View Details" class="voucher-btn"  onclick="OpenDetail(\'' + List_QuotationDetail[i].Sid + '\')" >View</a >';
                    tRow += '<a title = "Click to Requote" class="modify-btn"  onclick="UpdateQuotation(\'' + List_QuotationDetail[i].Sid + '\')" >Requote</a >';
                    tRow += '<a title = "Click if query is lost" class="cancel-btn" onclick="DeleteQuotation(\'' + List_QuotationDetail[i].Sid + '\'">Lost</a> ';
                    tRow += '</td > ';
                    //tRow += '<td class="vertical-center"><button type="button" class="button glossy blue-gradient" onclick="OpenDetail(\'' + List_QuotationDetail[i].Sid + '\')">View</button></td>';
                    //tRow += '<td style="max-width:50px;" class="vertical-center align-center"><span class="tag">View</span></td>';

                   // tRow += '<td width="50px" class="align-center vertical-center ">';
                    //tRow += '<div class="button-group compact">';
                   // tRow += '<a href="#" class="button icon-pencil with-tooltip" title="Update" onclick="UpdateQuotation(\'' + List_QuotationDetail[i].Sid + '\')"></a>';
                   // tRow += '<a href="#" class="button icon-trash with-tooltip" title="Delete" onclick="DeleteQuotation(\'' + List_QuotationDetail[i].Sid + '\')"></a>';
                    
                   // tRow += '<a href="#" class="button icon-trash with-tooltip confirm" title="Delete"></a>';
                    //tRow += '</div></td>'
                  // tRow += '<td width="50px" class="vertical-center" data-title="Delete" >';
                  // tRow += '<a title = "Click for Voucher" class="voucher-btn"  onclick="UpdateQuotation(\'' + List_QuotationDetail[i].Sid + '\')" >View</a >';
                  // tRow += '<a title = "Click for Invoice" class="invoice-btn">Requote</a >';
                  // tRow += '<a title = "Click to Modify Booking" class="modify-btn">Lost</a >';
                  // tRow += '<a title = "Click to Cancel the Booking" class="cancel-btn" onclick="DeleteQuotation(\'' + List_QuotationDetail[i].Sid + '\'">Delete</a> ';
                    //tRow += '<a style="cursor:pointer" href="#"><span class="icon-trash" title="Delete" style="cursor:pointer" onclick="DeleteQuotation(\'' + List_QuotationDetail[i].Sid + '\')"></span></a></td > ';
                    //tRow += '<td style="max-width: 55px;" class="align-center action-button">';
                   // tRow += '<a title = "Click for Voucher" class="voucher-btn"  onclick="UpdateQuotation(\'' + List_QuotationDetail[i].Sid + '\')" >View</a >';
                   // tRow += '<a title = "Click for Invoice" class="invoice-btn">Requote</a >';

                    //if (BookingList[i].Status != 'Cancelled') {
                        //tRow += '<a title = "Click to Modify Booking" class="modify-btn">Lost</a >';
                       // tRow += '<a title = "Click to Cancel the Booking" class="cancel-btn" onclick="DeleteQuotation(\'' + List_QuotationDetail[i].Sid + '\'">Delete</a> ';
                    //}
                    //trow += ' </td>';
                    tRow += '</tr>';
                }
                $("#tbl_QuotationDetails tbody").html(tRow);
                $("#tbl_QuotationDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_QuotationDetails").removeAttribute("style")
            }
            else if (result.retCode == 0) {
                $("#tbl_QuotationDetails tbody").remove();
                var tRow = '<tbody>';
                tRow += '<tr> <td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_QuotationDetails").append(tRow);

                $("#tbl_QuotationDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
        }

    });
}

function Search() {

    $("#tbl_QuotationDetails").dataTable().fnClearTable();
    $("#tbl_QuotationDetails").dataTable().fnDestroy();


    var CompanyName = $("#txt_Name").val();
  
    var data = {
        CompanyName: CompanyName

    }

    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/SearchQuotationDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            // var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_QuotationDetail = result.Arr;
                for (var i = 0; i < List_QuotationDetail.length; i++) {
                    tRow += '<tr>';
                    tRow += '<td>' + (i + 1) + '</td>';
                    tRow += '<td>CUT/Quot-' + List_QuotationDetail[i].Sid + '</td>';
                    tRow += '<td>' + List_QuotationDetail[i].Date + '</td>';
                    tRow += '<td>' + List_QuotationDetail[i].Companyname + '</td>';
                    tRow += '<td>' + List_QuotationDetail[i].Leadguestname + '</td>';
                    tRow += '<td>Adults- ' + List_QuotationDetail[i].Adults + ', Child-' + List_QuotationDetail[i].Child + ', Infant- ' + List_QuotationDetail[i].Infant + ' </td>';
                    tRow += '<td><button type="button" class="button glossy blue-gradient" >View</button></td>';

                    tRow += '<td data-title="Update" style="text-align:center;"><a style="cursor:pointer" href="#"><span class="icon-pencil" title="Update" style="cursor:pointer" onclick="UpdateQuotation(\'' + List_QuotationDetail[i].Sid + '\')"></span></a></td>';

                    tRow += '<td data-title="Delete" style="text-align:center;"><a style="cursor:pointer" href="#"><span class="icon-trash" title="Delete" style="cursor:pointer" onclick="DeleteQuotation(\'' + List_QuotationDetail[i].Sid + '\')"></span></a></td>';

                    tRow += '</tr>';
                }
                $("#tbl_QuotationDetails tbody").html(tRow);
                $("#tbl_QuotationDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_QuotationDetails").removeAttribute("style")
            }
            else if (result.retCode == 0) {
                $("#tbl_QuotationDetails tbody").remove();
                var tRow = '<tbody>';
                //tRow += '<tr><td align="center" style="padding-top: 2%" colspan="4"><span><b>No record found</b></span></td></tr>';
                tRow += '<tr> <td align="center" colspan="6" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_QuotationDetails").append(tRow);

                //$("#tbl_AgentDetails").dataTable({
                //    "bSort": false
                //});
            }
        },
        error: function () {
            //Success("An error occured while loading details.")
        }
    });

}

function UpdateQuotation(ID) {

    window.location.href = "AddQuotation.aspx?ID=" + ID;

}

function ChangeStatusModal(Sid, Currentstatus) {

    $('#ChangeStatusModal').modal("show");
    sid = Sid;
    $("#hdModelId").val(sid);

}

//function ViewQuotation(ID) {

//    window.location.href = "ViewQuotationDetails.aspx?ID=" + ID;

//}
function DeleteQuotation(Id) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete<br/> <span class=\"orange\">CUT/Quot -' + Id + '</span>?</span>?</p>',
        function () {
           
            $.ajax({
                url: "../Handler/QuotationHandler.asmx/DeleteQuotationByID",
                type: "post",
                data: '{"Id":"' + Id + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Quotation has been deleted successfully!");
                        window.location.reload();
                    } else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}
function ChangeStatus() {

    var rowid;
    var status;
    rowid = $("#hdModelId").val();
    status = $("#selCurrentstatus").val();

    //var selCurrentstatus = $("#selCurrentstatus  option:selected").val();
    var data = {
        ID: rowid,
        Currentstatus: status,
    }
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/UpdateCurrentstatusById",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {

                Success("Quotation Status Updated successfully");
                $('#ChangeStatusModal').modal('hide');
                GetAllQuotationDetails();
            }
            else {
                alert("Something Went Wrong");
                window.location.reload();
            }
        }
    });
}

function OpenDetail(sid) {

    var QDetail = $.grep(List_QuotationDetail, function (p) { return p.Sid == sid; })
        .map(function (p) { return p; });

    $.modal({
        title: 'Quotation Detail',
        content: ' <div class="with-padding">' +


            '    <div class="columns">' +
            '        <div class="four-columns twelve-columns-mobile">' +
            '            <label>DATE:</label>&nbsp <label id="lbl_Date"><b style="font-size:16px">' + QDetail[0].Date + '</b></label>' +
            '        </div>' +
            '        <div class="four-columns twelve-columns-mobile">' +
            '            <label>QUOTE TO:</label>&nbsp <label id="lbl_ QuoteTo"><b style="font-size:16px">' + QDetail[0].Companyname + '</b></label>' +
            '        </div>' +
            '        <div class="four-columns twelve-columns-mobile" style="display:none">' +
            '            <label>COMPANY NAME:</label>&nbsp <label id="lbl_ CmpnyName"><b style="font-size:16px">' + QDetail[0].CompanynameLeadguestname + '</b></label>' +
            '        </div>' +
            '    </div>' +

            '    <div class="columns">' +
            '        <div class="four-columns twelve-columns-mobile">' +
            '            <label>LEAD GUEST NAME:</label>&nbsp <label id="lbl_LeadName"><b style="font-size:16px">' + QDetail[0].Leadguestname + '</b></label>' +
            '        </div>' +
            '        <div class="four-columns twelve-columns-mobile">' +
            '            <label>ADULTS:</label>&nbsp <label id="lbl_Adult"> <b style="font-size:16px">' + QDetail[0].Adults + '</b></label>' +
            '        </div>' +
            '        <div class="four-columns twelve-columns-mobile">' +
            '            <label>CHILD:</label>&nbsp <label id="lbl_Child"><b style="font-size:16px">' + QDetail[0].Child + '</b></label>' +
            '        </div>' +
            '    </div>' +

            '    <div class="columns">' +
            '        <div class="four-columns twelve-columns-mobile">' +
            '            <label>INFANT:</label>&nbsp <label id="lbl_Infant"><b style="font-size:16px">' + QDetail[0].Infant + '</b></label>' +
            '        </div>' +
            '        <div class="four-columns twelve-columns-mobile">' +
            '            <label>TOUR START:</label>&nbsp <label id="lbl_TourStart"><b style="font-size:16px">' + QDetail[0].TourStart + '</b></label>' +
            '        </div>' +
            '        <div class="four-columns twelve-columns-mobile">' +
            '            <label>TOUR END:</label>&nbsp <label id="lbl_TourEnd"><b style="font-size:16px">' + QDetail[0].TourEnd + '</b></label>' +
            '        </div>' +
            '    </div>' +

            '    <div class="columns">' +
            '        <div class="four-columns twelve-columns-mobile">' +
            '            <label>QUOTED BY:</label>&nbsp <label id="lbl_QuotBy"><b style="font-size:16px">' + QDetail[0].ContactPerson + '</b></label>' +
            '        </div>' +
            '        <div class="four-columns twelve-columns-mobile">' +
            '            <label>CURRENT STATUS:</label>&nbsp <label id="lbl_Status"><b style="font-size:16px">' + QDetail[0].Currentstatus + '</b></label>' +
            '        </div>' +
            '        <div class="four-columns twelve-columns-mobile">' +
            '            <label>NOTE / REMARK:</label>&nbsp <label id="lbl_Remark"><b style="font-size:16px">' + QDetail[0].Remark + '</b></label>' +
            '        </div>' +
            '    </div>' +

            '    <div class="columns">' +
            '        <div class="twelve-columns twelve-columns-mobile">' +
            '            <label>QUOTION DETAIL:</label>&nbsp <p id="lbl_Detail"><b style="font-size:16px">' + QDetail[0].QuotationDetails.replace("<p>", " ").replace("</p>"," ") + '</b></p>' +
            '        </div>' +
           
            '    </div>' +
            '</div>',

        width: 500,
    });

}

