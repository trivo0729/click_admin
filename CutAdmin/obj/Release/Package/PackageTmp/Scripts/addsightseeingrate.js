﻿
var Market = "";
var touroption = "";
var Transfer = false;
var InclusionsLength = "";
var ExclusionsLength = "";
var InventoryType = "";
var arrInventory = new Array();
$(document).ready(function () {
    if (location.href.indexOf('?') != -1) {
        id = GetQueryStringParams('sSightseeingID');
        city = GetQueryStringParams('City').replace(/%20/g, ' ');
        ActName = GetQueryStringParams('SightseeingName').replace(/%20/g, ' ');
        country = GetQueryStringParams('Country').replace(/%20/g, ' ');
        locationn = GetQueryStringParams('Location').replace(/%20/g, ' ');
        $('#ActivityName').text(ActName);
    }
    GetCountry();
    GetLanguage();
    GenrateDates('GN');
    GenrateSlot();
    GetTourType()
    // Getpriority();
    GetChildPolicy();
    $('.wizard #wiz_main').on('wizardleave', function () {
        var supplier = $("#Sel_Supplier option:selected").text();
        arrInventory = new Array();
        InventoryType = "";
        var Inventory = $("#sel_InventoryType").val();
        for (var i = 0; i < Inventory.length; i++) {
            if (Inventory.length == 3)
                InventoryType = "All";
            else
                InventoryType += Inventory[i] + ',';

            arrInventory.push(Inventory[i]);
        }
        $('.lbl_supplier').text(supplier);
        $('.lbl_Inventory').text(InventoryType);
        var touroptionname = $("#touroptions option:selected").text();
        $(".lbl_TourOptions").text(touroptionname);
        var arrMarket = $("#sel_Country").val();
        Market = "";
        for (var i = 0; i < arrMarket.length; i++) {
            if (arrMarket[i] == "All Market") {
                Market += arrMarket[i] + ',';
                break;
            }
            else {
                Market += arrMarket[i] + ',';
            }
        }
        $('.lbl_Market').text(Market);

        /*For Show Languages*/
        var Language = $("#sel_Language").val();
        var Lang = "";
        for (var i = 0; i < Language.length; i++) {
            if (Language[i] == "All Language") {
                Lang += Language[i] + ',';
                break;
            }
            else {
                Lang += Language[i] + ',';
            }
        }
        $('.lbl_Language').text(Lang);

        /*Set Slot*/
        SetSlotTime();
    })
    $('.wizard #wiz_rates').on('wizardleave', function () {
        SetUIDates('GN');

        /*Set Validation for Rate tabs*/
        for (var i = 0; i < arrActivityType.length; i++) {
            if ($('#txt_adultRate_' + arrActivityType[i] + '').val() == "") {
                $('#li_' + arrActivityType[i] + '').attr("class", "active");
                $('#li_' + arrActivityType[i] + '').show();
                $('#div_' + arrActivityType[i] + '').show();
                return false;
            }
            else {
                if (i != arrActivityType.length - 1) {
                    $('#li_' + arrActivityType[i] + '').attr("class", "");
                    $('#div_' + arrActivityType[i] + '').hide();
                }
            }
        }
    })
    $('.wizard #wiz_review').on('wizardleave', function () {
        if ($(".lbl_Inclusions").length == 0) {
            $("#div_inclusions").addClass("validate[required]");
            return false;
        }
        if ($(".lbl_Exclusions").length == 0) {
            $("#div_exclusions").addClass("validate[required]");
            return false;
        }
        Review();
    })
});

function GetCountry() {
    try {
        post("GenralHandler.asmx/GetCountry", {}, function (data) {
            $(data.Country).each(function (index, arrCountry) {
                if (index == 0)
                    $('#sel_Country').append($('<option selected="selected"></option>').val("All Market").html("All Market"));
                else
                    $('#sel_Country').append($('<option></option>').val(arrCountry.Country).html(arrCountry.Countryname));
                $('#sel_Country').change();
            });
        }, function (errordata) {
            alertDanger(errordata.ex)
        })
    } catch (e) { }
}

function GetLanguage() {
    try {
        post("../handler/GenralHandler.asmx/GetLanguages", {}, function (data) {
            $(data.arrLang).each(function (index, arrLanguage) {
                if (index == 0)
                    $('#sel_Language').append($('<option selected="selected"></option>').val("All Language").html("All Language"));
                else
                    $('#sel_Language').append($('<option></option>').val(arrLanguage.Code).html(arrLanguage.Languages));
                $('#sel_Language').change();
            });
        }, function (errordata) {
            alertDanger(errordata.ex)
        })
    } catch (e) { }
}

var datepickersOpt = {
    dateFormat: 'dd-mm-yy'
}
function GenrateDates(RateType) {
    debugger
    var html = "";
    try {
        var elem = $(".dte" + RateType);
        html += '<div class="columns dte' + RateType + '">'
        html += '<div class="four-columns ten-columns-mobile five-columns-tablet">'
        html += '<span class="input">'
        html += '<span class="icon-calendar"></span>'
        html += '<input type="text" class="input-unstyled datepicker validate[required] ' + RateType + 'From" value="" placeholder="Valid from">'
        html += '</span>'
        html += '</div>'
        html += '<div class="five-columns twelve-columns-mobile seven-columns-tablet">'
        html += '<span class="input">'
        html += '<span class="icon-calendar"></span>'
        html += '<input type="text" class="input-unstyled datepicker validate[required] ' + RateType + 'To" value="" onchange="" placeholder="Valid till">'
        html += '</span>';
        if (elem.length == 0) {

            html += '<span class="mid-margin-left icon-size2 icon-plus-round icon-black" onclick="GenrateDates(\'' + RateType + '\')" id="btn_Add' + RateType + 'Date"></span>'
        }
        else
            html += '<span class="mid-margin-left icon-size2 icon-minus-round icon-red remCF" ></span>';
        html += '</div>'
        html += '</div>'
        $("#div_" + RateType + "Date").append(html);
        $(".remCF").on('click', function () {
            $(this).parent().parent().remove();
        });
        var elem_FromDate = $("#div_" + RateType + "Date").find("." + RateType + "From");
        var elem_ToDate = $("#div_" + RateType + "Date").find("." + RateType + "To");
        for (var i = 0; i < elem_FromDate.length; i++) {
            var previousFrom = 0;
            var previousTo = 0;
            if (i != 0)
                previousTo = moment($(elem_ToDate[i - 1]).val(), "DD-MM-YYYY");
            var minDate = 0;
            if (previousTo != 0)
                minDate = previousTo._i;
            $(elem_FromDate[i]).datepicker($.extend({
                minDate: minDate,
                onSelect: function () {
                    var minDate = $(this).datepicker('getDate');
                    minDate.setDate(minDate.getDate() + 1); //add One days
                    $(elem_ToDate[i - 1]).datepicker("option", "minDate", minDate);
                }, beforeShow: function () {
                    if (RateType != "GN") {
                        var minDate = moment($($("#div_GNDate").find(".GNFrom")[i - 1]).val(), "DD-MM-YYYY")._i;
                        var maxDate = moment($($("#div_GNDate").find(".GNTo")[i - 1]).val(), "DD-MM-YYYY")._i;
                        $(this).datepicker("option", "minDate", minDate);
                        $(this).datepicker("option", "maxDate", maxDate);
                    }
                },
            }, datepickersOpt));

            $(elem_ToDate[i]).datepicker($.extend({
                onSelect: function () {
                    var maxDate = $(this).datepicker('getDate');
                    maxDate.setDate(maxDate.getDate());
                    UpdateFlag = false;
                }, beforeShow: function () {
                    if (RateType != "GN") {
                        var minDate = moment($($("#div_GNDate").find(".GNFrom")[i - 1]).val(), "DD-MM-YYYY")._i;
                        var maxDate = moment($($("#div_GNDate").find(".GNTo")[i - 1]).val(), "DD-MM-YYYY")._i;
                        $(this).datepicker("option", "minDate", minDate);
                        $(this).datepicker("option", "maxDate", maxDate);
                    }
                },
            }, datepickersOpt));
        }
    } catch (e) { }
}

//function GenrateSlot() {
//    debugger
//    var html = "";
//    try {
//        var elem = $(".div_slot");
//        html += '<div class="columns div_slot">'
//        //html += '<div class="three-columns ten-columns-mobile five-columns-tablet">'
//        //html += '                        <p class="no-margin-bottom">Name</p>'
//        //html += '                        <input type="text" class="input SlotName" value="" size="17">'
//        //html += '                    </div>'
//        html += '                    <div class="four-columns twelve-columns-mobile seven-columns-tablet">'
//        html += '                        <p class="align-center no-margin-bottom">Starting Time</p>'
//        html += '                        <div class="columns">'
//        html += '                            <div class="six-columns">'
//        html += '                                <span class="number input margin-right">'
//        html += '                                    <label style="width: 20px" for="roomtariff" class="button field-drop-input grey-gradient">Hrs</label>'
//        html += '                                    <button type="button" class="button number-down">-</button>'
//        html += '                                    <input type="text" value="1" size="3" class="input-unstyled SlotStartHours" data-number-options="{"min":0,"max":23,"increment":1.0,"shiftIncrement":5,"precision":0.25}">'
//        html += '                                    <button type="button" class="button number-up">+</button>'
//        html += '                                </span>'
//        html += '                            </div>'
//        html += '                            <div class="six-columns">'
//        html += '                                <span class="number input margin-right">'
//        html += '                                    <label style="width: 20px" for="roomtariff" class="button field-drop-input grey-gradient">Min</label>'
//        html += '                                    <button type="button" class="button number-down">-</button>'
//        html += '                                    <input type="text" value="00" size="3" class="input-unstyled SlotStartMin" data-number-options="{"min":0,"max":59,"increment":10,"shiftIncrement":5,"precision":1}">'
//        html += '                                    <button type="button" class="button number-up">+</button>'
//        html += '                                </span>'
//        html += '                            </div>'
//        html += '                        </div>'
//        html += '                    </div>'
//        html += '                    <div class="four-columns twelve-columns-mobile seven-columns-tablet">'
//        html += '                        <p class="align-center no-margin-bottom">Ending Time</p>'
//        html += '                        <div class="columns">                                   '
//        html += '                            <div class="six-columns">                           '
//        html += '                                <span class="number input margin-right">        '
//        html += '                                    <label style="width: 20px" for="roomtariff" class="button field-drop-input grey-gradient">Hrs</label>'
//        html += '                                    <button type="button" class="button number-down">-</button>'
//        html += '                                    <input type="text" value="1" size="3" class="input-unstyled SlotEndHours" data-number-options="{"min":0,"max":23,"increment":1.0,"shiftIncrement":5,"precision":0.25}">'
//        html += '                                    <button type="button" class="button number-up">+</button>'
//        html += '                                </span>'
//        html += '                            </div>     '
//        html += '                            <div class="six-columns">'
//        html += '                                <span class="number input margin-right">'
//        html += '                                    <label style="width: 20px" for="roomtariff" class="button field-drop-input grey-gradient">Min</label>'
//        html += '                                    <button type="button" class="button number-down">-</button>'
//        html += '                                    <input type="text" value="00" size="3" class="input-unstyled SlotEndMin" data-number-options="{"min":0,"max":59,"increment":10,"shiftIncrement":5,"precision":1}">'
//        html += '                                    <button type="button" class="button number-up">+</button>'
//        html += '                                </span>'
//        html += '                            </div>'
//        html += '                        </div>'
//        html += '                    </div>'
//        if (elem.length == 0) {

//            html += '<div class="one-column">'
//            html += '                    <br />'
//            html += '                    <span class="mid-margin-left icon-size2 icon-plus-round icon-black" onclick="GenrateSlot()" id=""></span>'
//            html += '                </div>'
//            //html += '<span class="mid-margin-left icon-size2 icon-plus-round icon-black" onclick="GenrateDates(\'' + RateType + '\')" id="btn_Add' + RateType + 'Date"></span>'
//        }
//        else {
//            html += '<div class="one-column">'
//            html += '                    <br />'
//            html += '                    <span class="mid-margin-left icon-size2 icon-minus-round icon-red remCF" ></span>'
//            html += '                </div>'
//        }

//        html += '</div>'
//        html += '</div>'
//        html += '</div>'
//        $("#SlotUI").append(html);
//        $(".remCF").on('click', function () {
//            $(this).parent().parent().remove();
//        });

//    } catch (e) { }
//}

function GenrateSlot() {
    debugger
    var html = "";
    try {
        var elem = $(".div_slot");
        html += '<div class="columns div_slot">'

        html += ' <div class="four-columns twelve-columns-mobile seven-columns-tablet">'
        html += ' <div class="four-columns four-columns-mobile">'
        if (elem.length == 0)
            html += ' <label class="uk-form-label" for="">Starting Time</label>'
        html += ' <input id="SlotStartTime' + elem.length + '" class="SlotStartTime validate[required]" onchange="SetEndTime(\'' + elem.length + '\')" style="background:#e3e5ea"/>'
        html += ' </div>'
        html += ' </div>'

        html += ' <div class="four-columns twelve-columns-mobile seven-columns-tablet">'
        html += ' <div class="four-columns four-columns-mobile">'
        if (elem.length == 0)
            html += ' <label class="uk-form-label" for="">Ending Time</label>'
        html += ' <input id="SlotEndTime' + elem.length + '" class="SlotEndTime validate[required]" style="background:#e3e5ea"/>'
        html += ' </div>'
        html += ' </div>'

        if (elem.length == 0) {

            html += '<div class="one-column">'
            html += '                    <br />'
            html += '                    <span class="mid-margin-left icon-size2 icon-plus-round icon-black" onclick="GenrateSlot()" id=""></span>'
            html += '                </div>'
        }
        else {
            html += '<div class="one-column">'
            html += '                    <br />'
            html += '                    <span class="mid-margin-left icon-size2 icon-minus-round icon-red remCF" ></span>'
            html += '                </div>'
        }

        html += '</div>'
        html += '</div>'
        html += '</div>'
        $("#SlotUI").append(html);

        $("#SlotStartTime" + elem.length + "").kendoTimePicker({
            interval: 10,
            format: "HH:mm"
        }); /*Date Time Picker*/
        //$("#SlotEndTime" + elem.length + "").kendoTimePicker({
        //    interval: 10,
        //    format: "HH:mm",
        //    min: new Date(2000, 0, 1, 8, 0, 0)
        //});
        /*Date Time Picker*/
        $('.k-widget').removeClass('validate[required]')
        $(".remCF").on('click', function () {
            $(this).parent().parent().remove();
        });

    } catch (e) { }
}

function SetEndTime(length) {
    var starttime = new Date();
    starttime = $("#SlotStartTime" + length + "").val();
    newtime = moment.utc(starttime, "HH:mm").add(10, "minutes").format("HH:mm");
    var spltime = newtime.split(":");

    $("#SlotEndTime" + length + "").kendoTimePicker({
        interval: 10,
        format: "HH:mm",
        min: new Date(2000, 0, 1, spltime[0], spltime[1], 0)
    });
    $('.k-widget').removeClass('validate[required]')
}

var AgeOfChild1 = "";
var AgeOfChild2 = "";
var AgeOfInfant = "";
function AddTarrifRate(ActType, Priority) {
    try {
        debugger
        index = 0;

        index = $("#li_" + ActType).length;
        TarrifPlan(ActType, index);
        GetTab(index, ActType, Priority);
        if (ChildBit == true) {
            var x = document.getElementsByClassName("div_child");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "block";
            }
            AgeOfChild1 = ChildPolicy[0].Child_Age_Upto + " " + "to" + ChildPolicy[0].Child_Age_From + " yrs";
            AgeOfChild2 = ChildPolicy[0].Small_Child_Age_Upto + " " + "to" + ChildPolicy[0].Child_Age_Upto + " yrs";
            $(".sChild1").text("Child" + "(" + AgeOfChild1 + " )");
            $(".sChild2").text("Child" + "(" + AgeOfChild2 + " )");

            if (InfantBit == true) {
                var z = document.getElementsByClassName("div_infant");
                for (i = 0; i < z.length; i++) {
                    z[i].style.display = "block";
                }
                AgeOfInfant = "Below " + ChildPolicy[0].Small_Child_Age_Upto + " yrs";
                $(".sInfant").text("Infant(" + AgeOfInfant + ")");
            }
        }

        setTimeout(function () {
            debugger
            $("#div_Main").refreshInnerTabs();
        }, 500)
    } catch (e) {
        AlertDanger(e.message)
    }
}

function TarrifPlan(ActType, index) {
    var html = '';
    var css = '';
    if (index != 0)
        css = 'class="with-padding"';
    else
        css = 'style="height: auto !important;"';
    html += '<div id="div_' + ActType + '" class="tab-active">' +
    '<div class="with-padding" style="padding-bottom: 0px !important;">' +
    '    <small class="input-info">Cost Price is to be entered here</small>' +
    '    <div class="columns">' +
     /*Adult Rate*/
    '        <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">' +
    '            <label class="">Adult Rate </label>' +
    '            <span class="input tariff-textbox">' +
    '                <label style="width: 30px" for="adultrate" class="button field-drop-input green-gradient sCurrency"></label>' +
    '                <input style="width: 60px" type="text" name="AdultRate" id="txt_adultRate_' + ActType + '" onchange="AutoSetRate(this.value,\'' + ActType + '\',true,\'' + "adultRate" + '\')" class="input-unstyled align-right validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '            </span>' +
    '        </div>' +


    /*Child (4 to 10 yrs) Rate*/
    '        <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom div_child" style="display:none">' +
    '            <label class="sChild1"></label>' +
    '            <span class="input tariff-textbox">' +
    '                <label style="width: 30px" for="child1rate" class="button field-drop-input green-gradient sCurrency"></label>' +
    '                <input style="width: 60px" type="text" name="Child1Rate" id="txt_child1Rate_' + ActType + '" onchange="AutoSetRate(this.value,\'' + ActType + '\',true,\'' + "child1Rate" + '\')" class="input-unstyled align-right validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '            </span>' +
    '        </div>' +


    /*Child (2 to 4 yrs) Rate*/
    '        <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom div_child" style="display:none">' +
    '            <label class="sChild2"></label>' +
    '            <span class="input tariff-textbox">' +
    '                <label style="width: 30px" for="child2rate" class="button field-drop-input green-gradient sCurrency"></label>' +
    '                <input style="width: 60px" type="text" name="Child2Rate" id="txt_child2Rate_' + ActType + '" onchange="AutoSetRate(this.value,\'' + ActType + '\',true,\'' + "child2Rate" + '\')" class="input-unstyled align-right validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '            </span>                                                                                                                         ' +
    '        </div>   ' +


    /*Infant Rate*/
    '        <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom div_infant" id="div_infant_' + ActType + '" style="display:none">                                           ' +
    '            <label class="sInfant"></label>                                                                                   ' +
    '            <span class="input tariff-textbox">                                                                                             ' +
    '                <label style="width: 30px" for="infantrate" class="button field-drop-input green-gradient sCurrency"></label>                   ' +
    '                <input style="width: 60px" type="text" name="InfantRate" id="txt_infantRate_' + ActType + '" onchange="AutoSetRate(this.value,\'' + ActType + '\',true,\'' + "infantRate" + '\')" class="input-unstyled align-right validate[required,custom[onlyNumberSp]]" placeholder="0.00">     ' +
    '            </span>' +
    '        </div>     ' +


    /*Day Wise Rates*/
    //'<br />' +


    '           </div>' +
    '            <small> <a class="pointer gray strong" onclick="showdaywise(\'' + ActType + '\')">Daywise Tariff<input type="checkbox" id="chk_day' + ActType + '" style="display: none"></a></small>' +
    '       </div>' +
    '       <div class="silver-bg field-drop-tariff" id="ro-daywise' + ActType + '" style="display: none;">' +
    '           <div class="standard-tabs at-bottom">               ' +

    /*Day Wise Tabs*/
    '               <ul class="tabs">' +
    '                   <li class="active"><a href="#cost-' + ActType + '-adult">Adult</a></li>' +
    '                   <li class="div_child" style="display:none"><a href="#cost-' + ActType + '-child1" class="sChild1"></a></li>' +
    '                   <li class="div_child" style="display:none"><a href="#cost-' + ActType + '-child2" class="sChild2"></a></li> ' +
    '                   <li class="div_infant"  style="display:none"><a href="#cost-' + ActType + '-infant" class="sInfant"></a></li>             ' +
    '               </ul>' +


     /*Day Wise Tabs content*/
    '               <div class="tabs-content">' +


    /*Adult Tabs content*/
    '                   <div class="columns with-mid-padding" id="cost-' + ActType + '-adult">' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">     ' +
    '                           <span class="input">                                                                   ' +
    '                               <label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>' +
    '                               <input type="text" name="Mon" id="txt_mon_adultRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                       ' +
    '                       </div>                                                                                                                                            ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                            ' +
    '                           <span class="input">                                                                                                                          ' +
    '                               <label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label><input type="text" name="Tue" id="txt_tue_adultRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label><input type="text" name="Wed" id="txt_wed_adultRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label><input type="text" name="Thu" id="txt_thu_adultRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label><input type="text" name="Fri" id="txt_fri_adultRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label><input type="text" name="Sat" id="txt_sat_adultRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label><input type="text" name="Sun" id="txt_sun_adultRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>' +
    '                       </div>' +

    '                   </div>' +


     /*child1 Tabs content*/
    '                   <div class="columns with-mid-padding" id="cost-' + ActType + '-child1">' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
    '                           <span class="input">' +
    '                               <label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>' +
    '                               <input type="text" name="Mon" id="txt_mon_child1Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                       ' +
    '                       </div>                                                                                                                                            ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                            ' +
    '                           <span class="input">                                                                                                                          ' +
    '                               <label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label><input type="text" name="Tue" id="txt_tue_child1Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>' +
    '                       </div>' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
    '                           <span class="input">' +
    '                               <label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label><input type="text" name="Wed" id="txt_wed_child1Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>' +
    '                       </div>' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
    '                           <span class="input">' +
    '                               <label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label><input type="text" name="Thu" id="txt_thu_child1Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label><input type="text" name="Fri" id="txt_fri_child1Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label><input type="text" name="Sat" id="txt_sat_child1Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label><input type="text" name="Sun" id="txt_sun_child1Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>' +
    '                       </div>' +
    '                    </div>' +


     /*child2 Tabs content*/
    '                    <div class="columns with-mid-padding" id="cost-' + ActType + '-child2">' +
    '                        <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">     ' +
    '                            <span class="input">                                                                   ' +
    '                                <label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>' +
    '                                <input type="text" name="Mon" id="txt_mon_child2Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                            </span>' +
    '                        </div>' +
    '                        <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
    '                            <span class="input">' +
    '                                <label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label><input type="text" name="Tue" id="txt_tue_child2Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                            </span>                                                                                                                                                                                                                          ' +
    '                        </div>                                                                                                                                                                                                                               ' +
    '                        <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                            <span class="input">                                                                                                                                                                                                             ' +
    '                                <label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label><input type="text" name="Wed" id="txt_wed_child2Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                            </span>' +
    '                        </div>' +
    '                        <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
    '                            <span class="input">' +
    '                                <label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label><input type="text" name="Thu" id="txt_thu_child2Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                            </span>' +
    '                        </div>' +
    '                        <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
    '                            <span class="input">' +
    '                                <label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label><input type="text" name="Fri" id="txt_fri_child2Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                            </span>                                                                                                                                                                                                                          ' +
    '                        </div>                                                                                                                                                                                                                               ' +
    '                        <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                            <span class="input">                                                                                                                                                                                                             ' +
    '                                <label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label><input type="text" name="Sat" id="txt_sat_child2Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                            </span>                                                                                                                                                                                                                          ' +
    '                        </div>                                                                                                                                                                                                                               ' +
    '                        <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                            <span class="input">                                                                                                                                                                                                             ' +
    '                                <label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label><input type="text" name="Sun" id="txt_sun_child2Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                            </span>                                                                                                                                                                                                                          ' +
    '                        </div>                                                                                                                                                                                                                               ' +
    '                   </div>     ' +


    /*infant Tabs content*/
    '                   <div class="columns with-mid-padding" id="cost-' + ActType + '-infant">                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                                ' +
    '                           <span class="input">                                                                                                                                                                                                              ' +
    '                               <label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>                                                                                                                                           ' +
    '                               <input type="text" name="Mon" id="txt_mon_infantRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">                                                                                    ' +
    '                           </span>                                                                                                                                                                                                                           ' +
    '                       </div>                                                                                                                                                                                                                                ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                                ' +
    '                           <span class="input">                                                                                                                                                                                                              ' +
    '                               <label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label><input type="text" name="Tue" id="txt_tue_infantRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00"> ' +
    '                           </span>                                                                                                                                                                                                                           ' +
    '                       </div>                                                                                                                                                                                                                                ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                                ' +
    '                           <span class="input">                                                                                                                                                                                                              ' +
    '                               <label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label><input type="text" name="Wed" id="txt_wed_infantRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00"> ' +
    '                           </span>                                                                                                                                                                                                                           ' +
    '                       </div>                                                                                                                                                                                                                                ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                                ' +
    '                           <span class="input">                                                                                                                                                                                                              ' +
    '                               <label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label><input type="text" name="Thu" id="txt_thu_infantRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00"> ' +
    '                           </span>                                                                                                                                                                                                                           ' +
    '                       </div>                                                                                                                                                                                                                                ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                                ' +
    '                           <span class="input">                                                                                                                                                                                                              ' +
    '                               <label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label><input type="text" name="Fri" id="txt_fri_infantRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00"> ' +
    '                           </span>                                                                                                                                                                                                                           ' +
    '                       </div>                                                                                                                                                                                                                                ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                                ' +
    '                           <span class="input">                                                                                                                                                                                                              ' +
    '                               <label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label><input type="text" name="Sat" id="txt_sat_infantRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00"> ' +
    '                           </span>                                                                                                                                                                                                                           ' +
    '                       </div>                                                                                                                                                                                                                                ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                                ' +
    '                           <span class="input">                                                                                                                                                                                                              ' +
    '                               <label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label><input type="text" name="Sun" id="txt_sun_infantRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00"> ' +
    '                           </span>  ' +
    '                       </div>  ' +
    '                   </div>  ' +
    '                   <div class="clearfix"></div>  ' +
    '               </div>  ' +
    '           </div>  ' +
    '       </div>  ' +



    /*min selling price content*/
    '       <div class="with-padding" style="padding-bottom: 0px !important;">  ' +
    //'           <small class="input-info">You may enter min selling price if you want to keep it different then costing</small>  ' +
    '<small class="input-info"><a class="pointer" onclick="genminsell(\'' + ActType + '\')">You may enter min selling price if you want to keep it different then costing<input type="checkbox" id="chk_Min' + ActType + '" style="display:none"></a></small>' +
    '           <div class="columns" id="genminsell' + ActType + '" style="display:none ">  ' +


    /*min selling Adult Rate*/
    '               <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">  ' +
    '                   <label class="">Adult Rate </label>  ' +
    '                   <span class="input tariff-textbox">  ' +
    '                       <label style="width: 30px" for="adultrate" class="button field-drop-input green-gradient sCurrency"></label>  ' +
    '                       <input style="width: 60px" type="text" name="adultRate" id="txt_min_adultRate_' + ActType + '" onchange="AutoSetMinRate(this.value,\'' + ActType + '\',true,\'' + "adultRate" + '\')" class="input-unstyled validate[required,custom[onlyNumberSp]] align-right" placeholder="0.00"  ' +
    '                   </span>                                                                                                                                                                   ' +
    '               </div>         ' +


    /*min selling Child1 Rate*/
    '               <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom div_child" style="display:none">                                                                                     ' +
    '                   <label class="sChild1"></label>                                                                                                                              ' +
    '                   <span class="input tariff-textbox">                                                                                                                                       ' +
    '                       <label style="width: 30px" for="child1rate" class="button field-drop-input green-gradient sCurrency"></label>                                                        ' +
    '                       <input style="width: 60px" type="text" name="Child1Rate" id="txt_min_child1Rate_' + ActType + '" onchange="AutoSetMinRate(this.value,\'' + ActType + '\',true,\'' + "child1Rate" + '\')" class="input-unstyled validate[required,custom[onlyNumberSp]] align-right" placeholder="0.00">                                                   ' +
    '                   </span>                                                                                                                                                                   ' +
    '               </div>         ' +


    /*min selling Child2 Rate*/
    '               <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom div_child" style="display:none"">                                                                                     ' +
    '                   <label class="sChild2"></label>                                                                                                                                ' +
    '                   <span class="input tariff-textbox">                                                                                                                                       ' +
    '                       <label style="width: 30px" for="child2rate" class="button field-drop-input green-gradient sCurrency"></label>                                                             ' +
    '                       <input style="width: 60px" type="text" name="Child2Rate" id="txt_min_child2Rate_' + ActType + '" onchange="AutoSetMinRate(this.value,\'' + ActType + '\',true,\'' + "child2Rate" + '\')" class="input-unstyled validate[required,custom[onlyNumberSp]] align-right" placeholder="0.00">                                             ' +
    '                   </span>                                                                                                                                                                   ' +
    '               </div>             ' +


    /*min selling Infant Rate*/
    '               <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom div_infant" style="display:none"">                                                                                     ' +
    '                   <label class="sInfant"></label>                                                                                                                             ' +
    '                   <span class="input tariff-textbox">                                                                                                                                       ' +
    '                       <label style="width: 30px" for="infantrate" class="button field-drop-input green-gradient sCurrency"></label>                                                             ' +
    '                       <input style="width: 60px" type="text" name="InfantRate" id="txt_min_infantRate_' + ActType + '" onchange="AutoSetMinRate(this.value,\'' + ActType + '\',true,\'' + "infantRate" + '\')" class="input-unstyled validate[required,custom[onlyNumberSp]] align-right" placeholder="0.00">                                               ' +
    '                   </span>                                                                                                                                                                   ' +
    '               </div>  ' +


     /*min selling Daywise Tariff*/

    ' <div class="new-row"> ' +
    '<small>' +
    '<a class="pointer gray strong" onclick="showdaywiseMin(\'' + ActType + '\')">Daywise Tariff                                                                                               ' +
    '<input type="checkbox" id="chk_dayMin' + ActType + '" style="display: none">                                                                                                                   ' +
    '</a>                                                                                                                                                                      ' +
    '</small>       ' +
    '</div>     ' +

    '</div>     ' +

    '</div>                                                                                                                                                                                ' +
    '<div class="silver-bg field-drop-tariff sMinCell" id="ro-daywiseMin' + ActType + '" style="display: none;">                                                                                                       ' +
    '<div class="standard-tabs at-bottom">' +

    /*min selling Day Wise Tabs*/
    '               <ul class="tabs">' +
    '                   <li class="active"><a href="#min-' + ActType + '-adult">Adult</a></li>' +
    '                   <li class="div_child" style="display:none"><a href="#min-' + ActType + '-child1" class="sChild1"></a></li>' +
    '                   <li class="div_child" style="display:none"><a href="#min-' + ActType + '-child2" class="sChild2"></a></li> ' +
    '                   <li class="div_infant" style="display:none"><a href="#min-' + ActType + '-infant" class="sInfant"></a></li>             ' +
    '               </ul>' +


     /*min selling Day Wise Tabs content*/
    '               <div class="tabs-content">' +


    /*min selling Adult Tabs content*/
    '                   <div class="columns with-mid-padding" id="min-' + ActType + '-adult">' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">     ' +
    '                           <span class="input">                                                                   ' +
    '                               <label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>' +
    '                               <input type="text" name="Mon" id="txt_mon_min_adultRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                       ' +
    '                       </div>                                                                                                                                            ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                            ' +
    '                           <span class="input">                                                                                                                          ' +
    '                               <label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label><input type="text" name="Tue" id="txt_tue_min_adultRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label><input type="text" name="Wed" id="txt_wed_min_adultRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label><input type="text" name="Thu" id="txt_thu_min_adultRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label><input type="text" name="Fri" id="txt_fri_min_adultRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label><input type="text" name="Sat" id="txt_sat_min_adultRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label><input type="text" name="Sun" id="txt_sun_min_adultRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>' +
    '                       </div>' +

    '                   </div>' +


     /*min selling child1 Tabs content*/
    '                   <div class="columns with-mid-padding" id="min-' + ActType + '-child1">' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
    '                           <span class="input">' +
    '                               <label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>' +
    '                               <input type="text" name="Mon" id="txt_mon_min_child1Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                       ' +
    '                       </div>                                                                                                                                            ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                            ' +
    '                           <span class="input">                                                                                                                          ' +
    '                               <label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label><input type="text" name="Tue" id="txt_tue_min_child1Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>' +
    '                       </div>' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
    '                           <span class="input">' +
    '                               <label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label><input type="text" name="Wed" id="txt_wed_min_child1Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>' +
    '                       </div>' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
    '                           <span class="input">' +
    '                               <label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label><input type="text" name="Thu" id="txt_thu_min_child1Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label><input type="text" name="Fri" id="txt_fri_min_child1Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label><input type="text" name="Sat" id="txt_sat_min_child1Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>                                                                                                                                                                                                                          ' +
    '                       </div>                                                                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                           <span class="input">                                                                                                                                                                                                             ' +
    '                               <label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label><input type="text" name="Sun" id="txt_sun_min_child1Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                           </span>' +
    '                       </div>' +
    '                    </div>' +


     /*min selling price child2 Tabs content*/
    '                    <div class="columns with-mid-padding" id="min-' + ActType + '-child2">' +
    '                        <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">     ' +
    '                            <span class="input">                                                                   ' +
    '                                <label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>' +
    '                                <input type="text" name="Mon" id="txt_mon_min_child2Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                            </span>' +
    '                        </div>' +
    '                        <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
    '                            <span class="input">' +
    '                                <label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label><input type="text" name="Tue" id="txt_tue_min_child2Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                            </span>                                                                                                                                                                                                                          ' +
    '                        </div>                                                                                                                                                                                                                               ' +
    '                        <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                            <span class="input">                                                                                                                                                                                                             ' +
    '                                <label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label><input type="text" name="Wed" id="txt_wed_min_child2Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                            </span>' +
    '                        </div>' +
    '                        <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
    '                            <span class="input">' +
    '                                <label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label><input type="text" name="Thu" id="txt_thu_min_child2Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                            </span>' +
    '                        </div>' +
    '                        <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
    '                            <span class="input">' +
    '                                <label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label><input type="text" name="Fri" id="txt_fri_min_child2Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                            </span>                                                                                                                                                                                                                          ' +
    '                        </div>                                                                                                                                                                                                                               ' +
    '                        <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                            <span class="input">                                                                                                                                                                                                             ' +
    '                                <label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label><input type="text" name="Sat" id="txt_sat_min_child2Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                            </span>                                                                                                                                                                                                                          ' +
    '                        </div>                                                                                                                                                                                                                               ' +
    '                        <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                               ' +
    '                            <span class="input">                                                                                                                                                                                                             ' +
    '                                <label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label><input type="text" name="Sun" id="txt_sun_min_child2Rate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
    '                            </span>                                                                                                                                                                                                                          ' +
    '                        </div>                                                                                                                                                                                                                               ' +
    '                   </div>     ' +


    /*min selling price infant Tabs content*/
    '                   <div class="columns with-mid-padding" id="min-' + ActType + '-infant">                                                                                                                                                                               ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                                ' +
    '                           <span class="input">                                                                                                                                                                                                              ' +
    '                               <label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>                                                                                                                                           ' +
    '                               <input type="text" name="Mon" id="txt_mon_min_infantRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">                                                                                    ' +
    '                           </span>                                                                                                                                                                                                                           ' +
    '                       </div>                                                                                                                                                                                                                                ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                                ' +
    '                           <span class="input">                                                                                                                                                                                                              ' +
    '                               <label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label><input type="text" name="Tue" id="txt_tue_min_infantRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00"> ' +
    '                           </span>                                                                                                                                                                                                                           ' +
    '                       </div>                                                                                                                                                                                                                                ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                                ' +
    '                           <span class="input">                                                                                                                                                                                                              ' +
    '                               <label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label><input type="text" name="Wed" id="txt_wed_min_infantRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00"> ' +
    '                           </span>                                                                                                                                                                                                                           ' +
    '                       </div>                                                                                                                                                                                                                                ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                                ' +
    '                           <span class="input">                                                                                                                                                                                                              ' +
    '                               <label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label><input type="text" name="Thu" id="txt_thu_min_infantRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00"> ' +
    '                           </span>                                                                                                                                                                                                                           ' +
    '                       </div>                                                                                                                                                                                                                                ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                                ' +
    '                           <span class="input">                                                                                                                                                                                                              ' +
    '                               <label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label><input type="text" name="Fri" id="txt_fri_min_infantRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00"> ' +
    '                           </span>                                                                                                                                                                                                                           ' +
    '                       </div>                                                                                                                                                                                                                                ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                                ' +
    '                           <span class="input">                                                                                                                                                                                                              ' +
    '                               <label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label><input type="text" name="Sat" id="txt_sat_min_infantRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00"> ' +
    '                           </span>                                                                                                                                                                                                                           ' +
    '                       </div>                                                                                                                                                                                                                                ' +
    '                       <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">                                                                                                                                                ' +
    '                           <span class="input">                                                                                                                                                                                                              ' +
    '                               <label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label><input type="text" name="Sun" id="txt_sun_min_infantRate_' + ActType + '" class="field-drop-input input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00"> ' +
    '                           </span>  ' +
    '                       </div>  ' +
    '                   </div>  ' +
    '                   <div class="clearfix"></div>  ' +
    '               </div>  ' +
    '           </div>  ' +
    '       </div>  ' +
    '       </div> ' +
    '   </div> '
    $('#div_tab').append(html);
    /*Set Rate Currency*/
    SetCurrency($("#SelCurrency").val());
}/*Tarrif Rate */

function GetTab(index, ActType, Priority) {
    try {
        var CSS = '"active"';
        if (index)
            CSS = ''
        $("#Ul").append('<li class="' + CSS + ' li_' + ActType + '" id="li_' + ActType + '"><a class="active strong align-center" href="#div_' + ActType + '">' + Priority + '</a></li>')

    } catch (e) {

    }
}/*Tarrif Tab */
var SelectedCurrency = "";
function SetCurrency(val) {
    try {
        SelectedCurrency = val;
        $(".sCurrency").text(val);
    } catch (e) { }
}/*Set Currency*/

//function GenerateTransfer() {
//    var x = document.getElementById("div_pick_drop");
//    if (x.style.display === "none") {
//        x.style.display = "block";
//    } else {
//        x.style.display = "none";
//        //$("#chk_dayMin" + ActType).prop("checked", false);
//    }
//}

function SetTransfer() {

    touroption = $("#touroptions option:selected").val();

    if (touroption == "SIC") {
        Transfer = true;
        var x = document.getElementById("div_pick_drop");
        if (x.style.display === "none") {
            x.style.display = "block";
        }
        GenerateTransfer()
    }
    else {
        var x = document.getElementById("div_pick_drop");
        if (x.style.display === "block") {
            x.style.display = "none";
        }
    }
}

function GenerateTransfer() {
    var html = "";
    try {
        var elem = $(".div_transfers");

        html += ' <div class="div_transfers">'
        html += '<div class="columns">'
        html += '<div class="four-columns twelve-columns-mobile seven-columns-tablet small-margin-bottom">'
        html += '<input type="text" class="input PickupFrom" id="txt_pickup" value="" size="22" placeholder="Pickup from">'
        html += '</div>'
        html += '<div class="four-columns twelve-columns-mobile seven-columns-tablet small-margin-bottom">'
        html += '<div class="columns">'


        html += ' <div class="six-columns twelve-columns-mobile seven-columns-tablet">'
        html += ' <div class="six-columns four-columns-mobile">'
        html += ' <input id="txt_pickuptime' + elem.length + '" placeholder="Pickup Time" class="pickuptime validate[required]"/>'
        html += ' </div>'
        html += ' </div>'

        //html += '<div class="six-columns small-margin-bottom">'
        //html += '<span class="number input margin-right">'
        //html += '<label style="width: 20px" for="roomtariff" class="button field-drop-input grey-gradient">Hrs</label>'
        //html += '<button type="button" class="button number-down">-</button>'
        //html += '<input type="text" value="1" id="txt_pickup_hrs" size="3" class="input-unstyled" data-number-options="{"min":1,"max":24,"increment":1.0,"shiftIncrement":5,"precision":0.25}">'
        //html += '<button type="button" class="button number-up">+</button>'
        //html += '</span>'
        //html += '</div>'
        //html += '<div class="six-columns small-margin-bottom">'
        //html += '<span class="number input margin-right">'
        //html += '<label style="width: 20px" for="roomtariff" class="button field-drop-input grey-gradient">Min</label>'
        //html += '<button type="button" class="button number-down">-</button>'
        //html += '<input type="text" value="00" id="txt_pickup_min" size="3" class="input-unstyled" data-number-options="{"min":0,"max":50,"increment":10,"shiftIncrement":5,"precision":1}">'
        //html += '<button type="button" class="button number-up">+</button>'
        //html += '</span>'
        //html += '</div>'

        html += '</div>'
        html += '</div>'
        html += '</div>'

        html += '<div class="columns">'
        html += '<div class="four-columns twelve-columns-mobile seven-columns-tablet">'
        html += '<input type="text" class="input DropTo" id="txt_drop" value="" size="22" placeholder="Drop to">'
        html += '</div>'
        html += '<div class="four-columns twelve-columns-mobile seven-columns-tablet">'
        html += '<div class="columns">'


        html += ' <div class="six-columns twelve-columns-mobile seven-columns-tablet">'
        html += ' <div class="six-columns four-columns-mobile">'
        html += ' <input id="txt_droptime' + elem.length + '" placeholder="Drop Time" class="droptime validate[required]"/>'
        html += ' </div>'
        html += ' </div>'

        //html += '<div class="six-columns small-margin-bottom">'
        //html += '<span class="number input margin-right">'
        //html += '<label style="width: 20px" for="roomtariff" class="button field-drop-input grey-gradient">Hrs</label>'
        //html += '<button type="button" class="button number-down">-</button>'
        //html += '<input type="text" value="1" id="txt_drop_hrs" size="3" class="input-unstyled" data-number-options="{"min":1,"max":24,"increment":1.0,"shiftIncrement":5,"precision":0.25}">'
        //html += '<button type="button" class="button number-up">+</button>'
        //html += '</span>'
        //html += '</div>'
        //html += '<div class="six-columns small-margin-bottom">'
        //html += '<span class="number input margin-right">'
        //html += '<label style="width: 20px" for="roomtariff" class="button field-drop-input grey-gradient">Min</label>'
        //html += '<button type="button" class="button number-down">-</button>'
        //html += '<input type="text" value="00" id="txt_drop_min" size="3" class="input-unstyled" data-number-options="{"min":0,"max":50,"increment":10,"shiftIncrement":5,"precision":1}">'
        //html += '<button type="button" class="button number-up">+</button>'
        //html += '</span>'
        //html += '</div> '

        html += '</div> '
        html += '</div> '
        if (elem.length == 0) {
            html += '<div class="one-column">'
            html += '<span class="mid-margin-left icon-size2 icon-plus-round icon-black" onclick="GenerateTransfer()" id=""></span>'
            html += '</div>'
        }
        else {
            html += '<span class="mid-margin-left icon-size2 icon-minus-round icon-red remCF" ></span>';
        }
        html += '</div>'
        html += '</div>'
        html += '<br />'
        $("#div_transferdetails").append(html);

        $("#txt_pickuptime" + elem.length + "").kendoTimePicker({
            interval: 10,
            format: "HH:mm"
        }); /*Date Time Picker*/
        $("#txt_droptime" + elem.length + "").kendoTimePicker({
            interval: 10,
            format: "HH:mm"
        }); /*Date Time Picker*/
        $('.k-widget').removeClass('validate[required]')

        $(".remCF").on('click', function () {
            $(this).parent().parent().remove();
        });
    }
    catch (e) { }
}


var arr_Priorities = new Array();
function GetTourType() {
    try {
        post("../Handler/ActivityHandller.asmx/Getpriority", {}, function (data) {
            arr_Priorities = data.dtresult;
            var PriorityName = $.grep(arr_Priorities, function (p) { return p.Status == "True" })
          .map(function (p) { return p });
            $(PriorityName).each(function (index, arrTourType) {
                if (index == 0)
                    $('#sel_TourType').append($('<option selected="selected"></option>').val("").html("Select Tour/Activity Type"));
                else
                    $('#sel_TourType').append($('<option></option>').val(arrTourType.Sid).html(arrTourType.PriorityType));
                $('#sel_TourType').change();
            });
        }, function (errordata) {
            alertDanger(errordata.ex)
        })
    } catch (e) { }
}


var ChildPolicy = new Array();
var ChildBit = false;
var InfantBit = false;
function GetChildPolicy() {
    var data = {
        id: id
    }
    post("../Handler/ActivityHandller.asmx/GetChildPolicy", data, function (data) {
        ChildPolicy = data.ArrChildPolicy
        if (ChildPolicy.length != 0) {
            ChildBit = true;
            if (ChildPolicy[0].Infant_Allow == true) {
                InfantBit = true;
            }
        }
    }, function (error) {
        AlertDanger("server not responds..")
    })
}
var arrActivityType = [];
function SetTarrifRate() {
    var acttype = $('#sel_TourType').val();
    arrActivityType = [];
    $('#Ul').empty();
    $('#div_tab').empty();
    for (var i = 0; i < acttype.length; i++) {
        if (i != 0) {
            arrActivityType.push(acttype[i]);
            var PriorityName = $.grep(arr_Priorities, function (p) { return p.Sid == acttype[i] })
          .map(function (p) { return p });
            AddTarrifRate(acttype[i], PriorityName[0].PriorityType)
        }
    }

}
var Slotmode = "";
function SetSlot() {
    var chkk = document.getElementById('btn_slot').checked
    if (chkk == true) {
        $('#SlotUI').show();
        //GenrateSlot();
        Slotmode = "";
    }
    else {
        $('#SlotUI').hide();
        Slotmode = "Full day";
    }
}

function ShowCancel(elem) {

    try {
        if ($(elem).is(":checked")) {
            $("#cnlpolicy").css("display", "")
            $("#percentage").prop("checked", true);
            $("#percentageinput").css("display", "");
            $("#amountinput").css("display", "none")
        }
        else {
            $("#cnlpolicy").css("display", "none")
            $("#percentageinput").css("display", "none");
            $("#amountinput").css("display", "none")
        }

    } catch (e) { }

}/*Cancel Show*/


function GetChargeCond(elem, Input) {
    try {
        if (Input == "percentageinput") {
            $("#percentageinput").css("display", "");
            $("#amountinput").css("display", "none")
        }
        else if (Input == "amountinput") {
            $("#percentageinput").css("display", "none");
            $("#amountinput").css("display", "")
        }
    } catch (e) {

    }
}/* Charge Condition*/

/* Add Inclusion*/
var Label;
function InclusionsAdd() {
    if ($('#txt_Inclusions').val() != "") {
        var noInclusion = $(".lbl_Inclusions").length + 1;
        var id = 'lbl_Inclusions' + noInclusion;
        Label = '<p class="selected-inclusion lbl_Inclusions" id="lbl_Inclusions' + noInclusion + '">' + $('#txt_Inclusions').val() + '<i class="icon-cross pointer" aria-hidden="true" style="padding-left: 5px" onclick="DeleteInclusions(' + id + ')"></i></p>'
        $("#div_inclusions").append(Label);
        $("#txt_Inclusions").val("");
        $("#div_inclusions").removeClass("validate[required]");
    }
    else {
        $('#txt_Inclusions').focus()
        Success("Please Insert Inclusions Name")
    }
}

/* Delete Inclusion*/
function DeleteInclusions(lblthis) {
    $(lblthis).remove()
}


/* Add Exclusion*/
function ExclusionsAdd() {
    if ($('#txt_Exclusions').val() != "") {
        var noExclusion = $(".lbl_Exclusions").length + 1;
        var id = 'lbl_Exclusions' + noExclusion;
        Label = '<p class="selected-inclusion lbl_Exclusions" id="lbl_Exclusions' + noExclusion + '">' + $('#txt_Exclusions').val() + '<i class="icon-cross pointer" aria-hidden="true" style="padding-left: 5px" onclick="DeleteExclusions(' + id + ')"></i></p>'
        $("#div_exclusions").append(Label);
        $("#txt_Exclusions").val("");
        $("#div_exclusions").removeClass("validate[required]");
    }
    else {
        $('#txt_Exclusions').focus()
        Success("Please Insert Inclusions Name")
    }
}

/* Delete Exclusion*/
function DeleteExclusions(lblthis) {
    $(lblthis).remove()

}

var arrDates = new Array();
function SetUIDates(RateType) {
    arrDates = new Array();
    var Dates = $(".dte" + RateType);
    $(Dates).each(function (index, Dates) {
        var ndFromDates = $(Dates).find("." + RateType + "From")[0];
        var ndToDates = $(Dates).find("." + RateType + "To")[0];
        arrDates.push({
            sFromDate: $(ndFromDates).val(),
            sToDate: $(ndToDates).val(),
        });
    });

    $(".div_DatesValidity").empty();
    var html = "";
    for (var i = 0; i < arrDates.length; i++) {
        html += '<p class="black selected-inclusion">' + arrDates[i].sFromDate + ' to ' + arrDates[i].sToDate + '</p>';
    }
    $(".div_DatesValidity").append(html);
}

var arrSlottime = new Array();
var Slots = "";
function SetSlotTime() {
    arrSlottime = new Array();
    Slots = $(".div_slot");
    var chkk = document.getElementById('btn_slot').checked
    if (chkk == true) {
        $(Slots).each(function (index, Slots) {
            var ndStartTime = $(Slots).find(".SlotStartTime")[1];
            var ndEndTime = $(Slots).find(".SlotEndTime")[1];
            arrSlottime.push({
                Act_Id: id,
                Start_Time: $(ndStartTime).val(),
                End_Time: $(ndEndTime).val(),
                Transfer: Transfer
            });
        });
    }
    else {
        arrSlottime.push({
            Act_Id: id,
            Start_Time: "00:00",
            End_Time: "23:50",
            Transfer: Transfer
        });
    }

    $(".div_SlotMode").empty();
    var html = "";
    if (Slotmode == "") {
        for (var i = 0; i < arrSlottime.length; i++) {
            html += '<p class="black selected-inclusion">' + arrSlottime[i].Start_Time + ' to ' + arrSlottime[i].End_Time + '</p>';
        }
    }
    else {
        html += '<p class="black">' + Slotmode + '</p>';
    }
    $(".div_SlotMode").append(html);
}

function Review() {
    $("#div_review").empty();
    var acttype = $('#sel_TourType').val();
    for (var i = 0; i < acttype.length; i++) {
        if (i != 0) {
            var PriorityName = $.grep(arr_Priorities, function (p) { return p.Sid == acttype[i] })
          .map(function (p) { return p });
            GenerateReview(acttype[i], PriorityName[0].PriorityType, i)
        }
    }
}

function CancellationPreview() {
    var html = '';
    $("#div_cancellation").empty();
    try {
        //html += '<div class="field-block button-height wizard-control">' +
        html += '<label class="label"><b>Cancellation Policy</b></label>'
        '<div class="wrapped">';
        if ($("#chk_Cancel").is(":checked")) {
            if ($("#percentage").is(":checked"))
                html += 'In the event of cancellation before <b>' + $("#sel_DayPer").val() + '</b> from the day of check-in <b>' + $("#sel_ChargePer").val() + ' charge</b> will apply. <b>' + $("#sel_NoShowPer").val() + ' charge</b> will apply in the event of <b>No Show</b>.'
            else if ($("#amount").is(":checked"))
                html += 'In the event of cancellation before <b>' + $("#sel_DayAmt").val() + '</b> from the day of check-in <b>' + $("#SelCurrency option:selected").val() + ' ' + $("#cancellationamount").val() + ' charge</b> will apply. <b>' + $("#sel_NoShowAmt").val() + ' charge</b> will apply in the event of <b>No Show</b>.'
        }
        else {
            html += 'This non-Refunable rate , no charge will be refundable once cancelled.'
        }
        html += '</div>'
        //html += '</div> '
        return html;

    } catch (e) { }
}/*Charge Preview*/

function GenerateReview(acttype, PriorityType, cnt) {
    var html = "";

    var x = document.getElementById("ro-daywise" + acttype);
    if (x.style.display === "block") {

        /* Adult daywise rate review*/
        html += ' <div class="columns" style="display: ">'
        if (cnt > 1) {
            html += ' <hr />'
        }
        html += '<div class="two-columns twelve-columns-mobile align-center">'
        html += '<label class="red strong">Adult</label>'
        html += '<p class="black strong selected-inclusion">Cost Rate</p>'
        html += '<p class="blue strong selected-inclusion">Min Selling</p>'
        html += '<br />'
        html += '<label class="green strong font16">' + PriorityType + '</label>'
        html += '</div>'
        html += '<div class="ten-columns twelve-columns-mobile">'
        html += '<div class="columns">'
        html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
        html += '<label class="green strong">Monday</label>'
        html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_mon_adultRate_" + acttype).val() + '</p>'
        html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_mon_min_adultRate_" + acttype).val() + '</p>'
        html += '</div>'
        html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
        html += '<label class="green strong">Tuesday</label>'
        html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_tue_adultRate_" + acttype).val() + '</p>'
        html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_tue_min_adultRate_" + acttype).val() + '</p>'
        html += '</div>'
        html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
        html += '<label class="green strong">Wednesday</label>'
        html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_wed_adultRate_" + acttype).val() + '</p>'
        html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_wed_min_adultRate_" + acttype).val() + '</p>'
        html += '</div>'
        html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
        html += '<label class="green strong">Thursday</label>'
        html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_thu_adultRate_" + acttype).val() + '</p>'
        html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_thu_min_adultRate_" + acttype).val() + '</p>'
        html += '</div>'
        html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
        html += '<label class="green strong">Friday</label>'
        html += ' <p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_fri_adultRate_" + acttype).val() + '</p>'
        html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_fri_min_adultRate_" + acttype).val() + '</p>'
        html += ' </div>'
        html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
        html += '  <label class="green strong">Saturday</label>'
        html += ' <p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sat_adultRate_" + acttype).val() + '</p>'
        html += ' <p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sat_min_adultRate_" + acttype).val() + '</p>'
        html += ' </div>'
        html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
        html += '   <label class="green strong">Sunday</label>'
        html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sun_adultRate_" + acttype).val() + '</p>'
        html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sun_min_adultRate_" + acttype).val() + '</p>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '</div>'

        if (ChildBit == true) {
            /* child1 daywise rate review*/
            html += ' <div class="columns" style="display: ">'
            html += ' <hr />'
            html += '<div class="two-columns twelve-columns-mobile align-center">'
            html += '<label class="red strong">Child (' + AgeOfChild1 + ')</label>'
            html += '<p class="black strong selected-inclusion">Cost Rate</p>'
            html += '<p class="blue strong selected-inclusion">Min Selling</p>'
            html += '<br />'
            html += '<label class="green strong font16">' + PriorityType + '</label>'
            html += '</div>'
            html += '<div class="ten-columns twelve-columns-mobile">'
            html += '<div class="columns">'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Monday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_mon_child1Rate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_mon_min_child1Rate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Tuesday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_tue_child1Rate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_tue_min_child1Rate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Wednesday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_wed_child1Rate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_wed_min_child1Rate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Thursday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_thu_child1Rate_" + acttype).val() + '</p>'
            html += ' <p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_thu_min_child1Rate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Friday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_fri_child1Rate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_fri_min_child1Rate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Saturday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sat_child1Rate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sat_min_child1Rate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Sunday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sun_child1Rate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sun_min_child1Rate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '</div>'

            /* child2 daywise rate review*/

            html += ' <div class="columns" style="display: ">'
            html += ' <hr />'
            html += '<div class="two-columns twelve-columns-mobile align-center">'
            html += '<label class="red strong">Child (' + AgeOfChild2 + ')</label>'
            html += '<p class="black strong selected-inclusion">Cost Rate</p>'
            html += '<p class="blue strong selected-inclusion">Min Selling</p>'
            html += '<br />'
            html += '<label class="green strong font16">' + PriorityType + '</label>'
            html += '</div>'
            html += '<div class="ten-columns twelve-columns-mobile">'
            html += '<div class="columns">'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Monday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_mon_child2Rate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_mon_min_child2Rate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Tuesday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_tue_child2Rate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_tue_min_child2Rate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Wednesday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_wed_child2Rate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_wed_min_child2Rate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Thursday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_thu_child2Rate_" + acttype).val() + '</p>'
            html += ' <p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_thu_min_child2Rate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Friday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_fri_child2Rate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_fri_min_child2Rate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Saturday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sat_child2Rate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sat_min_child2Rate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Sunday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sun_child2Rate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sun_min_child2Rate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '</div>'
        }
        if (InfantBit == true) {
            /* Infant daywise rate review*/

            html += ' <div class="columns" style="display: ">'
            html += ' <hr />'
            html += '<div class="two-columns twelve-columns-mobile align-center">'
            html += '<label class="red strong">Child (' + AgeOfInfant + ')</label>'
            html += '<p class="black strong selected-inclusion">Cost Rate</p>'
            html += '<p class="blue strong selected-inclusion">Min Selling</p>'
            html += '<br />'
            html += '<label class="green strong font16">' + PriorityType + '</label>'
            html += '</div>'
            html += '<div class="ten-columns twelve-columns-mobile">'
            html += '<div class="columns">'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Monday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_mon_infantRate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_mon_min_infantRate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Tuesday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_tue_infantRate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_tue_min_infantRate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Wednesday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_wed_infantRate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_wed_min_infantRate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Thursday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_thu_infantRate_" + acttype).val() + '</p>'
            html += ' <p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_thu_min_infantRate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Friday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_fri_infantRate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_fri_min_infantRate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Saturday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sat_infantRate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sat_min_infantRate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '<div class="three-columns four-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Sunday</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sun_infantRate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_sun_min_infantRate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '</div>'
        }

    }
    else {
        /* adult rate review*/
        html += '<div class="columns" style="display: ">'
        if (cnt > 1) {
            html += ' <hr />'
        }
        html += '<div class="two-columns twelve-columns-mobile mid-margin-bottom">'
        html += '<label class="red strong">' + PriorityType + '</label>'
        html += '<p class="black selected-inclusion"><b>Cost</b></p>'
        html += ' <p class="blue selected-inclusion"><b>Selling</b></p>'
        html += '</div>'
        html += '<div class="two-columns twelve-columns-mobile mid-margin-bottom align-center">'
        html += '<label class="green strong">Adult Rate</label>'
        html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_adultRate_" + acttype).val() + '</p>'
        html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_min_adultRate_" + acttype).val() + '</p>'
        html += '</div>'
        if (ChildBit == true) {
            /* child1 rate review*/
            html += '<div class="two-columns twelve-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Child (' + AgeOfChild1 + ')</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_child1Rate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_min_child1Rate_" + acttype).val() + '</p>'
            html += '</div>'
            /* child2 rate review*/
            html += '<div class="two-columns twelve-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Child (' + AgeOfChild2 + ')</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_child2Rate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_min_child2Rate_" + acttype).val() + '</p>'
            html += '</div>'
        }
        if (InfantBit == true) {
            /* Infant rate review*/
            html += '<div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">'
            html += '<label class="green strong">Infant (' + AgeOfInfant + ')</label>'
            html += '<p class="black selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_infantRate_" + acttype).val() + '</p>'
            html += '<p class="blue selected-inclusion">' + SelectedCurrency + ' ' + $("#txt_min_infantRate_" + acttype).val() + '</p>'
            html += '</div>'
            html += '</div>'

        }
    }

    $("#div_review").append(html);
    $("#div_cancellation").append(CancellationPreview());
}
var arrDays = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
function AutoSetRate(Rate, ActType, sell, Pax) {
    if (sell) {
        if (Pax == "adultRate") {
            for (var i = 0; i < arrDays.length; i++) {

                $("#txt_" + arrDays[i] + "_" + Pax + "_" + ActType).val(Rate);
                $("#txt_" + arrDays[i] + "_min_" + Pax + "_" + ActType).val(Rate);
            }
            $("#txt_min_" + Pax + "_" + ActType).val(Rate);
        }
        if (Pax == "child1Rate") {
            for (var i = 0; i < arrDays.length; i++) {

                $("#txt_" + arrDays[i] + "_" + Pax + "_" + ActType).val(Rate);
                $("#txt_" + arrDays[i] + "_min_" + Pax + "_" + ActType).val(Rate);
            }
            $("#txt_min_" + Pax + "_" + ActType).val(Rate);
        }
        if (Pax == "child2Rate") {
            for (var i = 0; i < arrDays.length; i++) {

                $("#txt_" + arrDays[i] + "_" + Pax + "_" + ActType).val(Rate);
                $("#txt_" + arrDays[i] + "_min_" + Pax + "_" + ActType).val(Rate);
            }
            $("#txt_min_" + Pax + "_" + ActType).val(Rate);
        }
        if (Pax == "infantRate") {
            for (var i = 0; i < arrDays.length; i++) {

                $("#txt_" + arrDays[i] + "_" + Pax + "_" + ActType).val(Rate);
                $("#txt_" + arrDays[i] + "_min_" + Pax + "_" + ActType).val(Rate);
            }
            $("#txt_min_" + Pax + "_" + ActType).val(Rate);
        }

    }
    else {

    }
}

function AutoSetMinRate(Rate, ActType, sell, Pax) {
    if (sell) {
        if (Pax == "adultRate") {
            for (var i = 0; i < arrDays.length; i++) {
                $("#txt_" + arrDays[i] + "_min_" + Pax + "_" + ActType).val(Rate);
            }
        }
        if (Pax == "child1Rate") {
            for (var i = 0; i < arrDays.length; i++) {
                $("#txt_" + arrDays[i] + "_min_" + Pax + "_" + ActType).val(Rate);
            }
        }
        if (Pax == "child2Rate") {
            for (var i = 0; i < arrDays.length; i++) {
                $("#txt_" + arrDays[i] + "_min_" + Pax + "_" + ActType).val(Rate);
            }
        }
        if (Pax == "infantRate") {
            for (var i = 0; i < arrDays.length; i++) {
                $("#txt_" + arrDays[i] + "_min_" + Pax + "_" + ActType).val(Rate);
            }
        }

    }
}


function SaveSightseeingRates() {
    try {

        var arrRatePlan = new Array();
        var arrSlotDetails = new Array();
        var arrRates = new Array();
        var arrCancellation = new Array();
        var arrLanguages = new Array();

        var Languages = $("#sel_Language").val();
        for (var i = 0; i < Languages.length; i++) {
            if (Languages[i] == "All Language") {
                arrLanguages.push(Languages[i]);
                break;
            }
            else {
                arrLanguages.push(Languages[i]);
            }
        }

        InclusionsLength = $(".lbl_Inclusions");
        ExclusionsLength = $(".lbl_Exclusions");
        var Inclusions = "";
        var Exclusions = "";
        for (var i = 0; i < InclusionsLength.length ; i++) {
            Inclusions += InclusionsLength[i].innerText + ';';
        }
        for (var i = 0; i < ExclusionsLength.length ; i++) {
            Exclusions += ExclusionsLength[i].innerText + ';';
        }

        /*array Main detail*/

        arrRatePlan = {
            Activity_Id: id,
            SupplierID: $("#Sel_Supplier option:selected").val(),
            Market: Market,
            RateType: $("#touroptions option:selected").val(),
            //InventoryType:InventoryType,
            sCurrency: $("#SelCurrency option:selected").val(),
            Inclusions: Inclusions,
            Exclusions: Exclusions,
            MinPax: $("#txt_MinPax").val(),
            MaxPax: $("#txt_Max_Pax").val(),
            TariffNote: $("#txt_TourNote").val(),
        };

        /*array transfer/slot detail*/
        if (Transfer == true) {

            var TransDetail = $(".div_transfers");
            $(TransDetail).each(function (index, TransDetail) {
                var ndPickupFrom = $(TransDetail).find(".PickupFrom")[0];
                var ndDropTo = $(TransDetail).find(".DropTo")[0];
                var ndPickupTime = $(TransDetail).find(".pickuptime")[1];
                var ndDropTime = $(TransDetail).find(".droptime")[1];
                arrSlotDetails.push({
                    Pickup_From: $(ndPickupFrom).val(),
                    Pickup_Time: $(ndPickupTime).val(),
                    Drop_To: $(ndDropTo).val(),
                    Drop_Time: $(ndDropTime).val(),
                });
            });
        }

        /*array rates detail*/

        var PaxType = ["adult"];
        if (ChildBit == true) {
            PaxType = ["adult", "child1", "child2"];
        }
        if (InfantBit == true) {
            PaxType = ["adult", "child1", "child2", "infant"];
        }

        $(arrActivityType).each(function (a, ActivityType) {
            $(arrSlottime).each(function (s, Slots) {
                $(arrDates).each(function (d, Dates) {
                    $(PaxType).each(function (p, Pax) {
                        var arrDayRate = new Array();
                        var arrSelling = new Array();
                        /*Sell Rates*/
                        $(arrDays).each(function (p, Day) {
                            arrDayRate.push({
                                Day: Day,
                                Rate: $("#txt_" + Day + "_min_" + Pax + "Rate_" + ActivityType).val(),
                                PlanID: ActivityType               /*using class variable PlanID for ActivityType*/
                            });
                        });
                        arrSelling = {
                            FromDate: Dates.sFromDate,
                            ToDate: Dates.sToDate,
                            sPaxType: Pax,
                            Rate: $("#txt_min_" + Pax + "Rate_" + ActivityType).val(),
                            arrDays: arrDayRate,
                        };
                        arrDayRate = new Array();
                        /*Purchase Rates*/
                        $(arrDays).each(function (p, Day) {
                            arrDayRate.push({
                                Day: Day,
                                Rate: $("#txt_" + Day + "_" + Pax + "Rate_" + ActivityType).val(),
                                PlanID: ActivityType
                            });
                        });
                        arrRates.push({
                            FromDate: Dates.sFromDate,
                            ToDate: Dates.sToDate,
                            sPaxType: Pax,
                            Rate: $("#txt_" + Pax + "Rate_" + ActivityType).val(),
                            arrDays: arrDayRate,
                            MinSelling: arrSelling

                        });
                    });
                });
            });
        });

        /*array of cancellation*/
        var arrCancellation = GetCancellationPolicy();

        post("../Handler/ActivityHandller.asmx/SaveSightseeingRates",
    {
        arrRatePlan: arrRatePlan,
        arrSlottime: arrSlottime,
        arrSlotDetails: arrSlotDetails,
        arrRates: arrRates,
        arrCancellation: arrCancellation,
        arrInventory: arrInventory,
        arrLanguages: arrLanguages

    }, function (data) {
        Success("Rate Saved Successfully");
        window.location.href = "activitylist.aspx";
    }, function (error) {
        AlertDanger(error.ex);
    })

    } catch (e) {
        AlertDanger("Unable to saved Rates.")
    }
}


function GetCancellationPolicy() {
    var arrCancellation = new Array();
    try {
        var CancelationPolicy = "REF/FD";
        var NoShowPolicy = "No Show/";
        if ($("#chk_Cancel").is(":checked")) {
            if ($("#percentage").is(":checked")) {
                CancelationPolicy += "(" + $("#sel_DayPer").val().replace("days", "") + ")/(" + $("#sel_ChargePer").val() + ")";
                arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "Refundable", DaysPrior: $("#sel_DayPer").val().replace("days", ""), IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_ChargePer").val() });
                if ($("#sel_NoShowPer").val() != "Same") {
                    NoShowPolicy += $("#sel_NoShowPer").val();
                    arrCancellation.push({ CancelationPolicy: NoShowPolicy, RefundType: "NoShow", DaysPrior: 0, IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: 0, PercentageToCharge: $("#sel_NoShowPer").val() });/*No Show*/
                }
                else {
                    NoShowPolicy += $("#sel_ChargePer").val();
                    arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "NoShow", DaysPrior: $("#sel_DayPer").val().replace("days", ""), IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_ChargePer").val() });
                }
            }
            else if ($("#amount").is(":checked")) {
                CancelationPolicy += "(" + $("#sel_DayAmt").val().replace("days", "") + ")/(" + $("#cancellationamount").val() + ")";
                arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "Refundable", DaysPrior: $("#sel_DayAmt").val().replace("days", ""), AmountToCharge: $("#cancellationamount").val(), ChargesType: "Amount", SupplierID: HotelAdminID });
                if ($("#sel_NoShowAmt").val() != "Same") {
                    NoShowPolicy += $("#sel_NoShowAmt").val();
                    arrCancellation.push({ CancelationPolicy: NoShowPolicy, RefundType: "NoShow", DaysPrior: 0, IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_NoShowAmt").val() });;/*No Show*/
                }
                else {
                    NoShowPolicy += $("#cancellationamount").val();
                    arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "NoShow", DaysPrior: $("#sel_DayAmt").val().replace("days", ""), AmountToCharge: $("#cancellationamount").val(), ChargesType: "Amount", SupplierID: HotelAdminID });
                }
            }
        }
        return arrCancellation;
    } catch (e) { return new Array() }
}