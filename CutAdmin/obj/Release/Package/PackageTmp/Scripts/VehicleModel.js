﻿$(function () {
    GetVehicleModel();
    var arrVehicleType = new Array();
})
var HiddenId = 0;
function GetVehicleType() {
    $.ajax({
        type: "POST",
        url: "../Handler/VehicleMasterHandller.asmx/GetVehicleType",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrVehicleType = result.tbl_VehicleType;
                if (arrVehicleType.length > 0) {
                    $("#selVehicleType").empty();
                    $("#selVehicleType2").empty();

                    var ddlRequest = '<option selected="selected" value="-">Select Any Vehicle Type</option>';
                    for (i = 0; i < arrVehicleType.length; i++) {
                        ddlRequest += '<option value="' + arrVehicleType[i].ID + '">' + arrVehicleType[i].VehicleBrand + '</option>';
                    }
                    $("#selVehicleType").append(ddlRequest);
                    $("#selVehicleType2").append(ddlRequest);
                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });
}
function GetVehicleModel() {
    $("#tbl_VehicleModel").dataTable().fnClearTable();
    $("#tbl_VehicleModel").dataTable().fnDestroy();
    //$("#tbl_VehicleModel tbody tr").remove()

    $.ajax({
        url: "../Handler/VehicleMasterHandller.asmx/GetVehicleModel",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var arrVehicleModel = result.tbl_VehicleModel;
                var tr = '';
                for (var i = 0; i < arrVehicleModel.length; i++) {

                    tr += '<tr> '
                    tr += '<td class="align-center">' + (i + 1) + '</td>                                                                                                                          '
                    tr += '<td class="align-center">' + arrVehicleModel[i].VehicleBrand + '</td>'
                    tr += '<td class="align-center">' + arrVehicleModel[i].VehicleModel + '</td>'
                    tr += '<td class="align-center">' + arrVehicleModel[i].SeattingCapacity + '</td>'
                    tr += '<td class="align-center">' + arrVehicleModel[i].BaggageCapacity + '</td>'

                    tr += '<td class="align-center" style="vertical-align:middle;">';
                    tr += '<div class="button-group compact">';
                    tr += '<a class="button icon-pencil with-tooltip" title="Edit Details"  onclick="LoadVehicleModel(\'' + arrVehicleModel[i].ID + '\',\'' + arrVehicleModel[i].VehicleType + '\',\'' + arrVehicleModel[i].VehicleBrand + '\',\'' + arrVehicleModel[i].VehicleModel + '\',\'' + arrVehicleModel[i].SeattingCapacity + '\',\'' + arrVehicleModel[i].BaggageCapacity + '\')"></a>'
                    tr += '<a href="#" class="button icon-trash with-tooltip confirm" title="Delete" onclick="DeleteVehicleModel(\'' + arrVehicleModel[i].ID + '\',\'' + arrVehicleModel[i].VehicleBrand + '\')"></a>'
                    tr += '</div ></td>';

                    //tr += '<a class="button" onclick="LoadVehicleModel(\'' + arrVehicleModel[i].ID + '\',\'' + arrVehicleModel[i].VehicleType + '\',\'' + arrVehicleModel[i].VehicleBrand + '\',\'' + arrVehicleModel[i].VehicleModel + '\',\'' + arrVehicleModel[i].SeattingCapacity + '\',\'' + arrVehicleModel[i].BaggageCapacity + '\')"><span class="icon-pencil"></span></a>'
                    //tr += '<a href="#" class="button" title="trash" onclick="DeleteVehicleModel(\'' + arrVehicleModel[i].ID + '\',\'' + arrVehicleModel[i].VehicleBrand + '\')"><span class="icon-trash"></span></a></span></td>'
                    tr += '</tr>'

                }
                $("#tbl_VehicleModel tbody").append(tr);
                //$("#tbl_VehicleModel tbody").html(tr);
                $("#tbl_VehicleModel").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_VehicleModel").removeAttribute("style")
            }
        }
    })
}

function DeleteVehicleModel(ID, VehicleBrand) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete<br/> <span class=\"orange\">' + VehicleBrand + '</span>?</span>?</p>',
        function () {
            $.ajax({
                url: "../Handler/VehicleMasterHandller.asmx/DeleteVehicleModel",
                type: "post",
                data: '{"ID":"' + ID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Vehicle Model has been deleted successfully!");
                        window.location.reload();
                    } else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}

//********************Add Edit StaticPackage*******************************
function AddVehicleModel() {
    var VehicleType = $("#selVehicleType").val();
    var VehicleModel = $("#txt_VehicleModel").val();
    var SeatingCapacity = $("#txt_SeatingCapacity").val();
    var BaggageCapacity = $("#txt_BaggageCapacity").val();
    var bValid = true;

    if (VehicleType == "") {
        Success("Please select Vehicle Type")
        bValid = false;
        $("#selVehicleType").focus()

    }
    if (VehicleModel == "") {
        Success("Insert Model Name")
        bValid = false;
        $("#txt_VehicleModel").focus()

    }
    else if ($("#txt_SeatingCapacity").val() == "") {
        Success("Please Insert Seating Capacity")
        bValid = false;
        $("#txt_SeatingCapacity").focus();

    }
    else if ($("#txt_BaggageCapacity").val() == "") {
        Success("Please Insert Baggage Capacity")
        bValid = false;
        $("#txt_BaggageCapacity").focus();
    }

    if (bValid == true) {
        var DATA = {

            VehicleType: VehicleType,
            VehicleModel: VehicleModel,
            SeatingCapacity: SeatingCapacity,
            BaggageCapacity: BaggageCapacity
        }
        var jason = JSON.stringify(DATA);
        $.ajax({
            url: "../Handler/VehicleMasterHandller.asmx/AddVehicleModel",
            type: "post",
            data: jason,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    $("#VehicleModelModal").modal("hide")
                    Success("Vehicle Type Added Successfully");
                    window.location.reload();
                }

                Clear()
                GetVehicleModel()
            }
        })
    }

}
function LoadVehicleModel(ID, VehicleType, VehicleBrand, VehicleModel, SeattingCapacity, BaggageCapacity) {


    $.modal({
        content: '<div class="modal-body" id="VehicleModelModal2">' +
            '<div class="columns">' +
            '<div class="six-columns twelve-columns-mobile"><label>Vehicle Brand:</label><div class="full-width button-height" id="DivVehicleType2">' +
            '<select id="selVehicleType2" class="select OfferType">' +
            '</select> ' +
            '</div></div>' +
            '<div class="six-columns twelve-columns-mobile"><label>Vehicle Model<span class="red">*</span>:</label> <div class="input full-width">' +
            '<input name="prompt-value" id="txt_VehicleModel2" placeholder="Model Name" value="' + VehicleModel + '" class="input-unstyled full-width"  type="text">' +
            '</div></div>' +
            '<div class="columns">' +
            '<div class="six-columns twelve-columns-mobile"><label>Seating Capacity:</label> <div class="input full-width">' +
            '<input name="prompt-value" id="txt_SeatingCapacity2" placeholder="Seating Capacity" value="' + SeattingCapacity + '" class="input-unstyled full-width"  type="text">' +
            '</div></div>' +
            '<div class="six-columns twelve-columns-mobile"><label>Baggage Capacity: </label> <div class="input full-width">' +
            '<input name="prompt-value" id="txt_BaggageCapacity2" placeholder="Baggage Capacity"  value="' + BaggageCapacity + '" class="input-unstyled full-width"  type="text">' +
            '</div></div></div>' +
            '</div>' +
            '<p class="text-alignright"><input type="button" value="Update" onclick="UpdateVehicleModel(\'' + ID + '\')" title="Update Details" class="button anthracite-gradient"/></p>' +
            '</div>',
        title: 'Update Vehicle Model',
        width: 500,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
    GetVehicleType();
    setTimeout(function () {
        AppendVehicleType(VehicleType)
    }, 500);

    //setTimeout(function () {

    //    AppendVehicleType(VehicleType, VehicleBrand)
    //}, 2000);


}
function AppendVehicleType(VehicleType) {
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        var VehicleBrand = $.grep(arrVehicleType, function (p) { return p.ID == VehicleType; })
            .map(function (p) { return p.VehicleBrand; });


        $("#DivVehicleType2 .select span")[0].textContent = VehicleBrand;
        for (var i = 0; i < arrVehicleType.length; i++) {
            if (arrVehicleType[i].ID == VehicleType) {
                $("#DivVehicleType2 .select span")[0].textContent = VehicleBrand;
                $('input[value="' + VehicleType + '"][class="OfferType"]').prop("selected", true);

                $("#selVehicleType2").val(VehicleType);
                var selected = [];
            }


        }
    }
    catch (ex) { }
}
//function AppendVehicleType(VehicleType, VehicleBrand) {
//    try {
//        var checkclass = document.getElementsByClassName('check');
//        $("#DivVehicleType2 .select span")[0].textContent = VehicleBrand;
//        //for (var i = 0; i <= VehicleBrand.length; i++) {
//            $('input[value="' + VehicleType + '"][class="Vehicle"]').prop("selected", true);
//            $("#selVehicleType2").val(VehicleType);
//        //}
//    }
//    catch (ex) { }
//}

//function AppendVehicleType(VehicleType) {
//    debugger;
//    try {
//        var checkclass = document.getelementsbyclassname('check');
//        var VehicleBrand = $.grep(arrVehicleType, function (p) { return p.ID == VehicleType; })
//           .map(function (p) { return p.VehicleBrand; });
//        $("#DivVehicleType2 .select span")[0].textcontent = VehicleBrand;
//        for (var i = 0; i < arrVehicleType.length; i++) {
//            if (arrVehicleType[i].ID == VehicleType) {
//                $("#DivVehicleType2 .select span")[0].textcontent = VehicleBrand;
//                $('input[value="' + VehicleType + '"][class="Vehicle"]').prop("selected", true);
//                $("#selVehicleType2").val(VehicleType);
//                //  pgta += gta[i] + ",";
//                var selected = [];
//            }


//        }
//    }
//    catch (ex)
//    { }
//}




function UpdateVehicleModel(ID) {

    var ID = ID;
    var VehicleType = $("#selVehicleType2").val();
    var VehicleModel = $("#txt_VehicleModel2").val();
    var SeatingCapacity = $("#txt_SeatingCapacity2").val();
    var BaggageCapacity = $("#txt_BaggageCapacity2").val();
    var bValid = true;

    if (VehicleType == "") {
        Success("Please select Vehicle Type")
        bValid = false;
        $("#selVehicleType2").focus()
    }
    if (VehicleModel == "") {
        Success("Please Insert Model Name")
        bValid = false;
        $("#txt_VehicleModel2").focus()
    }
    else if ($("#txt_SeatingCapacity2").val() == "") {
        Success("Please Insert Seating Capacity")
        bValid = false;
        $("#txt_SeatingCapacity2").focus();

    }
    else if ($("#txt_BaggageCapacity2").val() == "") {
        Success("Please Insert Baggage Capacity")
        bValid = false;
        $("#txt_BaggageCapacity2").focus();
    }

    if (bValid == true) {
        var DATA = {

            VehicleType: VehicleType,
            VehicleModel: VehicleModel,
            SeatingCapacity: SeatingCapacity,
            BaggageCapacity: BaggageCapacity,
            ID: ID
        }
        var jason = JSON.stringify(DATA);
        $.ajax({
            url: "../Handler/VehicleMasterHandller.asmx/UpdateVehicleModel",
            type: "post",
            data: jason,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    $("#VehicleModelModal2").modal("hide")
                    Success("Vehicle Model Updated Successfully");
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }

                Clear()
                GetVehicleModel()
            }
        })
    }

}
function Clear() {
    $("#selVehicleType").val("");
    $("#txt_SeatingCapacity").val("");
    $("#txt_BaggageCapacity").val("");

    HiddenId = 0;
}
