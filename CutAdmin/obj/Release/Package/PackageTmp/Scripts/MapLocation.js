﻿$(function () {
    LoadAreaGroup();
    var tbl = document.getElementById("tbl_AreaGroup");
    tbl.style.width = "100%";
});
var contryname;
var CountryCode;
var AreaGroup = new Array();
var bLoaddrop = false;
function GetLocation(Country, Countryname) {
    CountryCode = Country;
    $.ajax({
        type: "POST",
        url: "MapLocationHandler.asmx/GetLocation",
        data: '{"Country":"' + Country + '","Countryname":"' + Countryname + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                bLoaddrop = true;
                $(".div_SP").removeClass("hidden");
                arrGTAlocation = result.GTAlocation;
                arrMGHAlocation = result.MGHAlocation;
                arrDOTWAlocation = result.DOTWAlocation;
                arrTBOAlocation = result.TBOAlocation;
                arrGRNAlocation = result.GRNAlocation;
                arrListHB = result.ListHB;
                arrGetAllMapCity = result.GetAllMapCity;
                var arrCity = [];
                var arrCity2 = [];
                if (arrGTAlocation.length > 0) {
                    $("#selGTA1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option value="-" disabled>Select Any City</option>';
                    for (var i = 0; i < arrGetAllMapCity.length; i++) {
                        var arrCity = [];
                        var arrCity2 = [];
                        for (j = 0; j < arrGTAlocation.length; j++) {
                            var MapCity = arrGetAllMapCity[i].Name.toUpperCase();
                            var City = arrGTAlocation[j].City_Name.toUpperCase();
                            if (MapCity.includes(City)) {

                                arrCity.push(arrGTAlocation[j])
                                //ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '" onchange="GetLocation(this.value)">' + arrGTAlocation[i].City_Name + ' ' + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                                //contryname = arrCountry[i].Countryname;
                            }
                            else {
                                arrCity2.push(arrGTAlocation[j])
                            }


                        }
                        arrGTAlocation = arrCity2;
                    }
                    for (var i = 0; i < arrGTAlocation.length; i++) {
                        ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '">' + arrGTAlocation[i].City_Name + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                    }

                    $("#selGTA1").append(ddlRequest);
                    $("#selGTA1").kendoMultiSelect({ filterable: true });/*kendoMultiSelect*/
                    // $("#selCountryy").append(ddlRequest);

                }

                if (arrMGHAlocation.length > 0) {
                    $("#selMGH1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option value="-" disabled>Select Any City</option>';
                    for (var i = 0; i < arrGetAllMapCity.length; i++) {
                        var arrCity = [];
                        var arrCity2 = [];
                        for (j = 0; j < arrMGHAlocation.length; j++) {
                            var MapCity = arrGetAllMapCity[i].Name.toUpperCase();
                            var City = arrMGHAlocation[j].Destination.toUpperCase();
                            if (MapCity.includes(City)) {

                                arrCity.push(arrMGHAlocation[j])
                                //ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '" onchange="GetLocation(this.value)">' + arrGTAlocation[i].City_Name + ' ' + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                                //contryname = arrCountry[i].Countryname;
                            }
                            else {
                                arrCity2.push(arrMGHAlocation[j])
                            }


                        }
                        arrMGHAlocation = arrCity2;
                    }
                    for (var i = 0; i < arrMGHAlocation.length; i++) {
                        ddlRequest += '<option value="' + arrMGHAlocation[i].DestinationID + '">' + arrMGHAlocation[i].Destination + '(' + arrMGHAlocation[i].DestinationID + ')' + '</option>';
                    }

                    $("#selMGH1").append(ddlRequest);
                    $("#selMGH1").kendoMultiSelect({ filterable: true });/*kendoMultiSelect*/
                    // $("#selCountryy").append(ddlRequest);

                }

                if (arrDOTWAlocation.length > 0) {
                    $("#selDOT1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option value="-" disabled>Select Any City</option>';
                    for (var i = 0; i < arrGetAllMapCity.length; i++) {
                        var arrCity = [];
                        var arrCity2 = [];
                        for (j = 0; j < arrDOTWAlocation.length; j++) {
                            var MapCity = arrGetAllMapCity[i].Name.toUpperCase();
                            var City = arrDOTWAlocation[j].name.toUpperCase();
                            if (MapCity.includes(City)) {

                                arrCity.push(arrDOTWAlocation[j])
                                //ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '" onchange="GetLocation(this.value)">' + arrGTAlocation[i].City_Name + ' ' + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                                //contryname = arrCountry[i].Countryname;
                            }
                            else {
                                arrCity2.push(arrDOTWAlocation[j])
                            }


                        }
                        arrDOTWAlocation = arrCity2;
                    }
                    for (var i = 0; i < arrDOTWAlocation.length; i++) {
                        ddlRequest += '<option value="' + arrDOTWAlocation[i].code + '">' + arrDOTWAlocation[i].name + '(' + arrDOTWAlocation[i].code + ')' + '</option>';
                    }

                    $("#selDOT1").append(ddlRequest);
                    $("#selDOT1").kendoMultiSelect({ filterable: true });/*kendoMultiSelect*/
                    // $("#selCountryy").append(ddlRequest);

                }

                if (arrTBOAlocation.length > 0) {
                    $("#selTBO1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option value="-" disabled>Select Any City</option>';
                    for (var i = 0; i < arrGetAllMapCity.length; i++) {
                        var arrCity = [];
                        var arrCity2 = [];
                        for (j = 0; j < arrTBOAlocation.length; j++) {
                            var MapCity = arrGetAllMapCity[i].Name.toUpperCase();
                            var City = arrTBOAlocation[j].Destination.toUpperCase();
                            if (City.includes(MapCity)) {

                                arrCity.push(arrTBOAlocation[j])
                                //ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '" onchange="GetLocation(this.value)">' + arrGTAlocation[i].City_Name + ' ' + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                                //contryname = arrCountry[i].Countryname;
                            }
                            else {
                                arrCity2.push(arrTBOAlocation[j])
                            }


                        }
                        arrTBOAlocation = arrCity2;
                    }
                    for (var i = 0; i < arrTBOAlocation.length; i++) {
                        ddlRequest += '<option value="' + arrTBOAlocation[i].cityid + '">' + arrTBOAlocation[i].Destination + '(' + arrTBOAlocation[i].cityid + ')' + '</option>';
                    }

                    $("#selTBO1").append(ddlRequest);
                    $("#selTBO1").kendoMultiSelect({ filterable: true });/*kendoMultiSelect*/

                }

                if (arrListHB.length > 0) {
                    $("#selHB1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option value="-" disabled>Select Any City</option>';
                    for (var i = 0; i < arrGetAllMapCity.length; i++) {
                        var arrCity = [];
                        var arrCity2 = [];
                        for (j = 0; j < arrListHB.length; j++) {
                            var name = arrListHB[j].Destination.split(',');
                            var MapCity = arrGetAllMapCity[i].Name.toUpperCase();
                            var City = name[0].toUpperCase();
                            if (MapCity.includes(City)) {

                                arrCity.push(arrListHB[j])
                                //ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '" onchange="GetLocation(this.value)">' + arrGTAlocation[i].City_Name + ' ' + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                                //contryname = arrCountry[i].Countryname;
                            }
                            else {
                                arrCity2.push(arrListHB[j])
                            }


                        }
                        arrListHB = arrCity2;

                    }
                    for (var i = 0; i < arrListHB.length; i++) {
                        var name = arrListHB[i].Destination.split(',');
                        ddlRequest += '<option value="' + arrListHB[i].DestinationCode + '" >' + name[0] + '(' + arrListHB[i].DestinationCode + ')' + '</option>';
                    }

                    $("#selHB1").append(ddlRequest);
                    $("#selHB1").kendoMultiSelect({ filterable: true });/*kendoMultiSelect*/

                }

                if (arrGRNAlocation.length > 0) {
                    $("#selGRN1").empty();
                    // $("#selCountryy").empty();
                    var ddlRequest = '<option value="-" disabled>Select Any City</option>';
                    for (var i = 0; i < arrGetAllMapCity.length; i++) {
                        var arrCity = [];
                        var arrCity2 = [];
                        for (j = 0; j < arrGRNAlocation.length; j++) {
                            var MapCity = arrGetAllMapCity[i].Name.toUpperCase();
                            var City = arrGRNAlocation[j].Cityname.toUpperCase();
                            if (MapCity.includes(City)) {

                                arrCity.push(arrGRNAlocation[j])
                                //ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '" onchange="GetLocation(this.value)">' + arrGTAlocation[i].City_Name + ' ' + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                                //contryname = arrCountry[i].Countryname;
                            }
                            else {
                                arrCity2.push(arrGRNAlocation[j])
                            }


                        }
                        arrGRNAlocation = arrCity2;
                    }
                    for (var i = 0; i < arrGRNAlocation.length; i++) {
                        ddlRequest += '<option value="' + arrGRNAlocation[i].Citycode + '">' + arrGRNAlocation[i].Cityname + '(' + arrGRNAlocation[i].Citycode + ')' + '</option>';
                    }

                    $("#selGRN1").append(ddlRequest);
                    $("#selGRN1").kendoMultiSelect({ filterable: true });/*kendoMultiSelect*/

                }


            }
        },
        error: function () {
        }
    });
}
function SaveCityLocation() {
    var GTA = "", HB = "", DOTW = "", MGH = "", TBO = "", Expedia = "", GRN="";
    if($('#selGTA1').val()!="")
        GTA = $('#selGTA1').val().toString();
    if ($('#selHB1').val() != "")
        HB = $('#selHB1').val().toString();
    if ($('#selDOT1').val() != "")
        DOTW = $('#selDOT1').val().toString();
    if ($('#selMGH1').val() != "")
        MGH = $('#selMGH1').val().toString();
    if ($('#selTBO1').val() != "")
        TBO = $('#selTBO1').val().toString();
    if ($('#selMGH1').val() != "")
        Expedia = $('#selMGH1').val().toString();
    if ($('#selGRN1').val() != "")
        GRN = $('#selGRN1').val().toString();
    /*array Location detail for tbl_araegroup*/
    arrCity = {
        Destination_ID: $("#txtCountryCode").val(),
        Name: $("#Area_Name").val(),
        CountryName: $("#hdnCountryName").val(),
        Country_Code: $("#hdCountryCode").val(),
        latitude: $("#hdnlatitude").val(),
        longitude: $("#hdnlongitude").val(),
        Placeid: $("#hdnPlaceid").val(),
        GTA: GTA,
        HB: HB,
        DOTW: DOTW,
        MGH: MGH,
        TBO: TBO,
        Expedia: Expedia,
        GRN: GRN,
    };
    post("MapLocationHandler.asmx/SaveCityLocation", { arrCity: arrCity }, function () {
        Success("Mapped Sucessfully")
        setTimeout(function () {
            window.location.reload();
        }, 500);
    }, function (error) {
        if (error.reCode != undefined && error.reCode == 2)
            AlertDanger("Destination Code Already Exist. Please Enter Another Code");
        else
            AlertDanger("Something went wrong");
    })
}
function LoadAreaGroup() {
    $("#tbl_AreaGroup").dataTable().fnClearTable();
    $("#tbl_AreaGroup").dataTable().fnDestroy();
    post("MapLocationHandler.asmx/LoadAreaGroup", {}, function (result) {
        AreaGroup = result.areaGroup;
        var tr = '';
        for (var i = 0; i < AreaGroup.length; i++) {
            var SrNo = i + 1;
            tr += '<tr>';
            tr += '<td class="align-center">' + SrNo + '</td>';
            tr += '<td class="align-center">' + AreaGroup[i].Destination_ID + '</td>';
            tr += '<td class="align-center">' + AreaGroup[i].Name + '</td>';
            tr += '<td class="align-center">' + AreaGroup[i].CountryName + '</td>';
            tr += '<td class="align-center">' + ToolTipData(AreaGroup[i].GTA) + '</td>';
            tr += '<td class="align-center">' + ToolTipData(AreaGroup[i].HB) + '</td>';
            tr += '<td class="align-center">' + ToolTipData(AreaGroup[i].DOTW) + '</td>';
            tr += '<td class="align-center">' + ToolTipData(AreaGroup[i].Expedia) + '</td>';
            tr += '<td class="align-center">' + ToolTipData(AreaGroup[i].TBO) + '</td>';
            tr += '<td class="align-center">' + ToolTipData(AreaGroup[i].GRN) + '</td>';
            tr += '<td align="center"><a style="cursor:pointer"  onclick="UpdateCode(\'' + AreaGroup[i].Name + '\',\'' + AreaGroup[i].Country_Code + '\',\'' + AreaGroup[i].CountryName + '\',\'' + AreaGroup[i].Destination_ID + '\',\'' + AreaGroup[i].latitude + '\',\'' + AreaGroup[i].longitude + '\',\'' + AreaGroup[i].Placeid + '\',\'' + AreaGroup[i].Sid + '\')"><span class="icon-pencil icon-size2" title="Edit"></span></a></td>'
            tr += '</tr>';
        }
        $("#tbl_AreaGroup tbody").append(tr);
        $("#tbl_AreaGroup").dataTable({
            bSort: false, sPaginationType: 'full_numbers',
        });
    }, function(error) {

    })
}
function BindCode(id) {
    $("#txtCountryCode").val(id);
    if(id != "")
    GetCode(2);
}
var DestCodeU;
function UpdateCode(Name,CountryCode,CountryName, DestCode,latitude,logitude,placeid, sid) {
    $("#txtSource").prop('disabled', true);
    DestCodeU = sid;
    $("#txtSource").val(Name + "," + CountryName);
    $("#hdnCode").val(CountryCode);
    
    $("#hdnplace").val(placeid);
  
    post("MapLocationHandler.asmx/GetLocation", { Country: CountryCode, Countryname: CountryName }, function (result) {
        if (result.retCode == 1) {
            var arrData = $.grep(AreaGroup, function (H) { return H.Sid == sid })
              .map(function (H) { return H; })[0];
            $("#Area_Name").val(arrData.Name);
            $("#txtCountryCode").val(arrData.Destination_ID)
            $("#hdCountryCode").val(arrData.Country_Code)
            $("#hdnCountryName").val(arrData.CountryName)
            $("#hdnPlaceid").val(arrData.Placeid)
            $("#hdnlatitude").val(arrData.latitude);
            $("#hdnlongitude").val(arrData.longitude);
            $("#filter").show();
            $(".div_SP").removeClass("hidden");
            arrGTAlocation = result.GTAlocation;
            arrMGHAlocation = result.MGHAlocation;
            arrDOTWAlocation = result.DOTWAlocation;
            arrTBOAlocation = result.TBOAlocation;
            arrGRNAlocation = result.GRNAlocation;
            arrListHB = result.ListHB;
            arrGetAllMapCity = result.GetAllMapCity;
            var arrCity = [];
            var arrCity2 = [];
            if (arrGTAlocation.length > 0) {
                $("#selGTA1").empty();
                // $("#selCountryy").empty();
                var ddlRequest = '<option value="-">Select Any City</option>';

                for (var i = 0; i < arrGTAlocation.length; i++) {
                    ddlRequest += '<option value="' + arrGTAlocation[i].City_Code + '">' + arrGTAlocation[i].City_Name + '(' + arrGTAlocation[i].City_Code + ')' + '</option>';
                }
                $("#selGTA1").append(ddlRequest);
            }

            if (arrMGHAlocation.length > 0) {
                $("#selMGH1").empty();
                // $("#selCountryy").empty();
                var ddlRequest = '<option value="-">Select Any City</option>';
                for (i = 0; i < arrMGHAlocation.length; i++) {
                    ddlRequest += '<option value="' + arrMGHAlocation[i].DestinationID + '" onchange="GetLocation(this.value)">' + arrMGHAlocation[i].Destination + ' ' + '(' + arrMGHAlocation[i].DestinationID + ')' + '</option>';
                    //contryname = arrCountry[i].Countryname;
                }
                $("#selMGH1").append(ddlRequest);
            }

            if (arrDOTWAlocation.length > 0) {
                $("#selDOT1").empty();
                // $("#selCountryy").empty();
                var ddlRequest = '<option value="-">Select Any City</option>';
                for (i = 0; i < arrDOTWAlocation.length; i++) {
                    ddlRequest += '<option value="' + arrDOTWAlocation[i].code + '" onchange="GetLocation(this.value)">' + arrDOTWAlocation[i].name + ' ' + '(' + arrDOTWAlocation[i].code + ')' + '</option>';
                    //contryname = arrCountry[i].Countryname;
                }
                $("#selDOT1").append(ddlRequest);
            }

            if (arrTBOAlocation.length > 0) {
                $("#selTBO1").empty();
                // $("#selCountryy").empty();
                var ddlRequest = '<option value="-">Select Any City</option>';
                for (i = 0; i < arrTBOAlocation.length; i++) {
                    ddlRequest += '<option value="' + arrTBOAlocation[i].cityid + '" onchange="GetLocation(this.value)">' + arrTBOAlocation[i].Destination + ' ' + '(' + arrTBOAlocation[i].cityid + ')' + '</option>';
                    //contryname = arrCountry[i].Countryname;
                }
                $("#selTBO1").append(ddlRequest);
            }


            if (arrListHB.length > 0) {
                $("#selHB1").empty();
                // $("#selCountryy").empty();
                var ddlRequest = '<option value="-">Select Any City</option>';
                for (i = 0; i < arrListHB.length; i++) {
                    var name = arrListHB[i].Destination.split(',');
                    ddlRequest += '<option value="' + arrListHB[i].DestinationCode + '" onchange="GetLocation(this.value)">' + name[0] + ' ' + '(' + arrListHB[i].DestinationCode + ')' + '</option>';
                    //contryname = arrCountry[i].Countryname;
                }
                $("#selHB1").append(ddlRequest);
            }

            if (arrGRNAlocation.length > 0) {
                $("#selGRN1").empty();
                // $("#selCountryy").empty();
                var ddlRequest = '<option value="-">Select Any City</option>';
                for (i = 0; i < arrGRNAlocation.length; i++) {
                    //var name = arrListHB[i].Destination.split(',');
                    ddlRequest += '<option value="' + arrGRNAlocation[i].Citycode + '" onchange="GetLocation(this.value)">' + arrGRNAlocation[i].Cityname + ' ' + '(' + arrGRNAlocation[i].Citycode + ')' + '</option>';
                    //contryname = arrCountry[i].Countryname;
                }
                $("#selGRN1").append(ddlRequest);
            }
            SetMappedLocation(arrData.GTA, "selGTA1");
            SetMappedLocation(arrData.Expedia, "selMGH1");
            SetMappedLocation(arrData.DOTW, "selDOT1");
            SetMappedLocation(arrData.TBO, "selTBO1");
            SetMappedLocation(arrData.HB, "selHB1");
            SetMappedLocation(arrData.GRN, "selGRN1");
            $("#txtCountryCode").val(DestCode);
        }
    }
    , function (error) {
    })
}
function SetMappedLocation(sCategory,ID) {
    try {
        var sID = sCategory.split(',');
        if (bLoaddrop==false)
            $("#" + ID).kendoMultiSelect({ filterable: true });/*kendoMultiSelect*/
        $('#' + ID).data("kendoMultiSelect").value(sID);
    } catch (e) { }
}
function ToolTipData(arrData) {
    var slength = 0;
    if (arrData != null)
        slength = arrData.split(',').length;
    var html = '';
    html += '<span class="info-spot pointer"><br/><span class="count green-gradient" style="margin-top: 5px;">' + (slength) + '</span></span>'
    return html;
}
function NewMapped() {
    try {
        $("#filter").slideToggle();
    } catch (e) { }
}