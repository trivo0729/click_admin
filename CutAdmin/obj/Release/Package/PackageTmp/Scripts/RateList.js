﻿var HotelCode = 0;
var AddCount = 0;
$(function () {
    HotelCode = getParameterByName('sHotelID');
    var HotelName = getParameterByName('HName');
    $("#lbl_Hotel").text(HotelName);
    GetRatelist(HotelCode, HotelName)
    GetButton(HotelCode, HotelName)
    GetHotelAddress(HotelCode);
})

function GetButton(HotelCode, HotelName) {
    var html = '';
    html += '<a  onclick="AddNewRate()" class="button anthracite-gradient pointer"><i class="fa fa-plus"></i> &nbsp;Add New</a>                                                                                                  ';
    $("#div_btn").append(html);
}

function AddNewRate() {
    var html = '';
    try {
        var html = '<div class="with-padding">';
        html += '<select id="sel_RoomsType" class="select  full-width">'
        for (var i = 0; i < arrRoom.length; i++) {
            html += '<option value="' + arrRoom[i].RoomTypeID + '">' + arrRoom[i].RoomType + '</option>'
        }
        html += '</select>'
        html += '</div>'
        Modal(html, "Please select room to Add Rates", function () {
            window.location.href = href = 'roomrate.aspx?sHotelID=' + HotelCode + '&HotelName=' + getParameterByName('HName') + '&RoomId=' + $("#sel_RoomsType").val() + '&RoomType=' + $("#sel_RoomsType option:selected").text();
        });
        GetRoomDropDown(HotelCode)
    } catch (e) {
        alert(e.message);
    }
}

function GetRatelist(HotelCode, HotelName) {
    $("#tbl_Ratelist").dataTable().fnClearTable();
    $("#tbl_Ratelist").dataTable().fnDestroy();
    post("handler/RoomHandler.asmx/GetRatelist", { HotelCode: HotelCode }, function (data) {
        arrRateList = data.RateList;
        $(arrRateList).each(function (i, RatePlan) { // GETTING Sucees HERE
             var html = '';
            html += '<tr><td style="width:3%" id="' + i + '" align="center">' + (i + 1) + '</td>'
            var Checkindt = GetDate(RatePlan.Checkin);
            var Checkoutdt = GetDate(RatePlan.Checkout);
            RatePlan.dFromDate = "";
            RatePlan.dToDate = "";
            html += '<td style="width:13%" id="' + i + '" align="center">' + Checkindt + " to " + Checkoutdt + ' </td>';
            html += '<td style="width:10%" id="' + i + '" align="center">' + RatePlan.MealPlan + ' </td>';
            html += '<td style="width:10%" id="' + i + '" align="center">' + RatePlan.CurrencyCode + ' </td>';
            html += '<td style="width:10%" id="' + i + '" align="center" title="\'' + RatePlan.ValidNationality + '\'">' + trancate_per(RatePlan.ValidNationality) + ' </td>'
            html += '<td style="width:10%" id="' + i + '" align="center">' + RatePlan.MinStay + ' </td>';
            html += '<td style="width:10%" id="' + i + '" align="center">' + RatePlan.MaxStay + ' </td>';
            html += '<td style="width:10%" id="' + i + '" align="center">' + RatePlan.MinRoom + ' </td>';
            $("#tbl_Ratelist").append(html);
        });
        $("#tbl_Ratelist").dataTable({
            bSort: false,
            sPaginationType: 'full_numbers',
            sSearch: false
        });
        $("#tbl_Ratelist").addClass("respTable");
        GenrateTable();
    },function(error) {
        $("#tbl_Ratelist").dataTable({
            bSort: false,
            sPaginationType: 'full_numbers',
            sSearch: false
        });
    })
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function trancate_per(title) {
    var length = 20;
    if (title.length > length) {
        title = title.substring(0, length) + '...';
    }
    return title;
}

function GenrateTable() {
    // Call template init (optional, but faster if called manually)
    var arrRate;
    var Id;
    $.template.init();
    $('#tbl_Ratelist').tablesorter({
        headers: {
            0: { sorter: false },
            6: { sorter: false }
        }
    }).on('click', 'tbody td', function (event) {
        Id = this.id;
        // Do not process if something else has been clicked
        if (event.target !== this) {
            return;
        }
        var tr = $(this).parent(),
            row = tr.next('.row-drop'),
            rows;
        // If click on a special row
        if (tr.hasClass('row-drop')) {
            return;
        }
        // If there is already a special row
        if (row.length > 0) {
            // Un-style row
            tr.children().removeClass('anthracite-gradient glossy');
            // Remove row
            row.remove();
            return;
        }
        // Remove existing special rows
        rows = tr.siblings('.row-drop');
        if (rows.length > 0) {
            // Un-style previous rows
            rows.prev().children().removeClass('anthracite-gradient glossy');
            // Remove rows
            rows.remove();

        }
        // Style row
        if (Id != "") {
            tr.children().addClass('anthracite-gradient glossy');

            $('<tr class="row-drop"><td id="div_Rate' + Id + '" colspan="9"></td></tr>').insertAfter(tr);
            var arrRate = arrRateList[Id];

            var data = {
                arrRate: arrRate
            }

            $.ajax({
                url: "handler/RoomHandler.asmx/GetRateData",
                type: "post",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        var RateList = result.RateData;
                        var html = '';
                        html += '<table class="table responsive-table responsive-table-on dataTable" id="tbl_RatesDetails' + Id + '"><th style="text-align:center">Room Type</th>';
                        html += '<th style="text-align:center" >Rate Type</th>';
                        html += '<th style="text-align:center" >Period</th>';
                        html += '<th style="text-align:center" >Room Rate</th>';
                        html += '<th style="text-align:center" >EB Rate</th>';
                        html += '<th style="text-align:center" >CWB Rate</th>';
                        html += '<th style="text-align:center" >CNB Rate</th>';
                        html += '<th style="text-align:center" >Offer</th>';
                        html += '<th style="text-align:center" >Cancellation Policy</th>';
                        html += '<th style="text-align:center" >Meal Rate</th>';
                        html += '<th style="text-align:center" >AddOns</th>';
                        html += '<th style="text-align:center" >Status</th>';
                        html += '<tbody>'
                        for (var i = 0; i < RateList.length; i++) {
                            html += '<tr><td align="center">' + RateList[i].RoomType + ' </td>'
                            html += '<td align="center">' + RateList[i].Type + ' </td>'
                            var from = GetDate(RateList[i].Checkin);
                            var To = GetDate(RateList[i].Checkout);
                            html += '<td align="center">' + from + ' to ' + To + ' </td>'
                            html += '<td align="center">'
                            if (RateList[i].DayWise == "NO") {
                                html += RateList[i].RR
                            }
                            else {
                                html += '<lable >DayWise</lable>'
                                html += '    <span class="info-spot">'
                                html += '		<span class="icon-info-round"></span>'
                                html += '		<span class="info-bubble">'
                                for (var d = 0; d < RateList[i].arrDayRate.length; d++) {
                                    html += '<small>' + RateList[i].arrDayRate[d].Day + ':</small> ' + RateList[i].arrDayRate[d].Rate + '<br/>'
                                }
                                html += '		</span>'
                                html += '	</span>'
                            }
                            html += '</td>'
                            if (RateList[i].EB != null)
                                html += '<td align="center">' + RateList[i].EB + ' </td>'
                            else
                                html += '<td align="center">0</td>'

                            if (RateList[i].CWB != null)
                                html += '<td align="center">' + RateList[i].CWB + ' </td>'
                            else
                                html += '<td align="center">0</td>'

                            if (RateList[i].CNB != null)
                                html += '<td align="center">' + RateList[i].CNB + ' </td>'
                            else
                                html += '<td align="center">0</td>'

                            if (RateList[i].OfferId == "")
                                html += '<td align="center">-</td>'
                            else
                                html += '<td align="center" onclick="GetOffer(\'' + RateList[i].HotelRateID + '\')" id="lbl_Fillter' + Id + '" class="icon-lightning"></td>'

                            if (RateList[i].CancellationPolicyId == "1")
                                html += '<td align="center" style="color:red">Non Refundable</td>'
                            else
                                html += '<td align="center" onclick="GetCancellation(\'' + RateList[i].CancellationPolicyId + '\')"  class="icon-info-round"></td>'   //id="lbl_FillterCancle' + Id + '"

                            html += '<td align="center" onclick="GetMeal(\'' + RateList[i].HotelRateID + '\')" class="icon-mixi"></td>'
                            html += '<td align="center" onclick="GetAddOn(\'' + RateList[i].HotelRateID + '\')" class="icon-cup"></td>'
                            if (RateList[i].Status == 'Active') {
                                html += '<td class="align-center"><input type="checkbox" id="chk' + RateList[i].HotelRateID + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"  checked  onclick="UpdateRateStatus(' + RateList[i].HotelRateID + ',\'Deactive\')"></td></tr>';
                            }
                            else {
                                html += '<td class="align-center"><input type="checkbox" id="chk' + RateList[i].HotelRateID + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"   onclick="UpdateRateStatus(' + RateList[i].HotelRateID + ',\'Active\')"></td></tr>';
                            }

                        }
                        html += '</tbody>'
                    }
                    html += '</table>'
                    $('#div_Rate' + Id).append(html);
                    $('#lbl_Fillter' + Id).menuTooltip($('#filter').hide(), {
                        classes: ['with-small-padding', 'full-width']
                    });
                    $('#lbl_FillterCancle' + Id).menuTooltip($('#filter').hide(), {
                        classes: ['with-small-padding', 'full-width']
                    });
                    $(".tiny").click(function () {
                        $(this).find("input:checkbox").click();
                    })
                    //$("#tbl_RatesDetails"+ Id).dataTable({
                    //    bSort: false,
                    //    sPaginationType: 'full_numbers',
                    //    sSearch: false
                    //});
                    //$("#tbl_RatesDetails" + Id).addClass("respTable");

                }
            })
        }


    })
    //.on('sortStart', function () {
    //var rows = $(this).find('.row-drop');
    //if (rows.length > 0) {
    //    rows.prev().children().removeClass('anthracite-gradient glossy');
    //    rows.removeAll();
    //}
    //  });
}

function GetOffer(HotelRateId) {
    var data = {
        RateID: HotelRateId
    }

    $.modal({
        content:
                 '<div class="with-padding" id="Div_Offer"></div>',

        title: 'Offers',
        width: 600,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetOffer",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#filter").empty();
            if (result.retCode == 1) {
                arrOfferList = result.ListOffer;
                var html = '';

                html += '<table class="table responsive-table responsive-table-on dataTable">';
                html += ' <th style="text-align:center">Offer Name</th>';
                html += '<th style="text-align:center">Offer Type</th>';
                html += '<th style="text-align:center">Season</th>';
                html += '<tbody>'
                for (var i = 0; i < arrOfferList.length; i++) {
                    html += '<tr><td align="center">' + arrOfferList[i].OfferName + ' </td>'
                    html += '<td align="center">' + arrOfferList[i].OfferType + ' </td>'
                    html += '<td align="center">' + arrOfferList[i].From + ' to ' + arrOfferList[i].To + ' </td></tr>'
                }
                html += '</tbody>'
                html += '</table>'
                $("#Div_Offer").append(html);
            }
            else {
                Success("Something went wrong");
            }


        },
        error: function () {
        }
    });
}

function GetCancellation(CancellationPolicyId) {
    var data = {
        CancellationPolicyId: CancellationPolicyId
    }

    $.modal({
        content:
                 '<div class="with-padding" id="Div_Cancle"></div>',

        title: 'Cancellation',
        width: 850,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetCancellation",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#filter").empty();
            if (result.retCode == 1) {
                arrListCancle = result.ListCancle;
                var html = '';

                html += '<table class="table responsive-table responsive-table-on dataTable" id="tbl_CancleList"><th style="text-align:center">Cancelation Policy</th>';
                html += '<th style="text-align:center">Refund Type</th>';
                html += '<th style="text-align:center">Days Prior/Date</th>';
                html += '<th style="text-align:center">Rate Type</th>';
                html += '<th style="text-align:center">Rate</th>';
                html += '<th style="text-align:center">Note</th>';
                html += '<tbody>'
                for (var i = 0; i < arrListCancle.length; i++) {
                    html += '<tr><td style="text-align:center">' + arrListCancle[i].CancellationPolicy + '</td>';
                    html += '<td style="text-align:center">' + arrListCancle[i].RefundType + '</td>';
                    if (arrListCancle[i].IsDaysPrior == "true") {
                        html += '<td style="text-align:center">' + arrListCancle[i].DaysPrior + '</td>';
                    }
                    else {
                        html += '<td style="text-align:center">' + arrListCancle[i].Date + '</td>';
                    }
                    html += '<td style="text-align:center">' + arrListCancle[i].ChargeTypes + '</td>';
                    if (arrListCancle[i].ChargeTypes == "Percentile") {
                        html += '<td style="text-align:center">' + arrListCancle[i].PerToCharge + '%</td>';
                    }
                    else if (arrListCancle[i].ChargeTypes == "Nights") {
                        html += '<td style="text-align:center">' + arrListCancle[i].NightToCharge + ' Night</td>';
                    }
                    else {
                        html += '<td style="text-align:center">' + arrListCancle[i].AmntToCharge + '</td>';
                    }
                    html += '<td style="text-align:center">' + arrListCancle[i].Note + '</td>';
                    html += '</tr>';
                }
                html += '</tbody>'
                html += '</table>'
                $("#Div_Cancle").append(html);
            }
            else {
                Success("Something went wrong");
            }

        },
        error: function () {
        }
    });
}

function GetAddOn(HotelRateId) {
    var data = {
        RateID: HotelRateId
    }

    $.modal({
        content:
                 '<div class="with-padding" id="Div_AddOn"></div>',

        title: 'AddOns',
        width: 450,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetAddOn",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#filter").empty();
            if (result.retCode == 1) {
                arrListAddOn = result.AddOnNew;
                arrAddOns = result.arrAddOns;
                var html = '';
                if (arrListAddOn.length != 0) {
                    html += '<table class="table responsive-table responsive-table-on dataTable" id="tbl_CancleList">';
                    html += '<th style="text-align:center">AddOns Type</th>';
                    html += '<th style="text-align:center">Rate</th>';
                   // html += '<th style="text-align:center" >CWB</th>';
                    //html += '<th style="text-align:center" >CNB</th>';
                    // html += '<th style="text-align:center" >Hi-Tea</th>';
                    html += '<tbody>'
                    for (var i = 0; i < arrListAddOn.length; i++) {
                        html += '<tr><td style="text-align:center">' + arrListAddOn[i].MealType + '</td>';
                        html += '<td style="text-align:center">' + arrListAddOn[i].Rate + '</td>'
                        //html += '<td style="text-align:center">' + arrListAddOn[i].CWB + '</td>';
                       // html += '<td style="text-align:center">' + arrListAddOn[i].CNB + '</td>';
                        html += '</tr>';
                    }
                    html += '</tbody>'
                    html += '</table>'

                    html += '<br>For Update AddOns Rate kindly <a style="cursor:pointer" onclick="generateaddons(\'' + HotelRateId + '\',\'' + AddCount + '\');">Click here</a>'
                    $("#Div_AddOn").append(html);
                }
                else {
                    html += '<table class="table responsive-table responsive-table-on dataTable" id="tbl_CancleList">';
                    html += '<th style="text-align:center">AddOns Type</th>';
                    html += '<th style="text-align:center">Rate</th>';
                    //html += '<th style="text-align:center" >CWB</th>';
                   // html += '<th style="text-align:center" >CNB</th>';
                    html += '<tr><td style="text-align:center">-</td>';
                    html += '<td style="text-align:center">-</td>';
                    //html += '<td style="text-align:center">-</td>';
                   // html += '<td style="text-align:center">-</td>';
                    html += '</tr>';

                    html += '</tbody>'
                    html += '</table>'

                    html += '<br>For Update AddOns Rate kindly <a style="cursor:pointer" onclick="generateaddons(\'' + HotelRateId + '\',\'' + AddCount + '\');">Click here</a>'
                    $("#Div_AddOn").append(html);
                }

            }
            else {
                Success("Something went wrong");
            }

        },
        error: function () {
        }
    });
}

function GetMeal(HotelRateId) {
    var data = {
        RateID: HotelRateId
    }

    $.modal({
        content:
                 '<div class="with-padding" id="Div_Meal"></div>',

        title: 'Meal',
        width: 850,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetMeal",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#filter").empty();
            if (result.retCode == 1) {
                arrAddOnMeal = result.AddOnMeal;
                var html = '';
                if (arrAddOnMeal.length != 0) {

                    html += '<table class="table responsive-table responsive-table-on dataTable" id="tbl_CancleList">';
                    html += '<th style="text-align:center">Meal Type</th>';
                    html += '<th style="text-align:center">Adult Rate</th>';
                    html += '<th style="text-align:center" >CWB</th>';
                    html += '<th style="text-align:center" >CNB</th>';
                    // html += '<th style="text-align:center" >Hi-Tea</th>';
                    html += '<tbody>'
                    for (var i = 0; i < arrAddOnMeal.length; i++) {
                        html += '<tr><td style="text-align:center">' + arrAddOnMeal[i].MealType + '</td>';
                        html += ' <td style="text-align:center">' + arrAddOnMeal[i].Rate + '</td>';
                        html += '<td style="text-align:center">' + arrAddOnMeal[i].CWB + '</td>';
                        html += '<td style="text-align:center">' + arrAddOnMeal[i].CNB + '</td>';
                        html += '</tr>';
                    }
                    html += '</tbody>'
                    html += '</table>'

                    html += '<br>For Update Meal Rate kindly <a style="cursor:pointer" onclick="GetAddDetails(\'' + HotelRateId + '\');">Click here</a>'
                    $("#Div_Meal").append(html);
                }

                else {
                    html += '<table class="table responsive-table responsive-table-on dataTable" id="tbl_CancleList">';
                    html += '<th style="text-align:center">Meal Type</th>';
                    html += '<th style="text-align:center">Adult Rate</th>';
                    html += '<th style="text-align:center" >CWB</th>';
                    html += '<th style="text-align:center" >CNB</th>';
                    html += '<tr><td style="text-align:center">-</td>';
                    html += ' <td style="text-align:center">-</td>';
                    html += '<td style="text-align:center">-</td>';
                    html += '<td style="text-align:center">-</td>';
                    html += '</tr>';
                    html += '</tbody>'
                    html += '</table>'

                    html += '<br>For Update Meal Rate kindly <a style="cursor:pointer" onclick="GetAddDetails(\'' + HotelRateId + '\');">Click here</a>'
                    $("#Div_Meal").append(html);
                }


            }
            else {
                Success("Something went wrong");
            }

        },
        error: function () {
        }
    });
}

function UpdateRateStatus(HotelRateId, status) {

    var data = {
        RateID: HotelRateId,
        status: status,
    }

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/UpdateRateStatus",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#filter").empty();
            if (result.retCode == 1) {
                Success("Status Updated Successfully");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
            else {
                Success("Something went wrong");
            }


        },
        error: function () {
        }
    });
}

function GetDate(dt) {
    var dts = moment(dt, "DD.MM.YYYY").format("DD-MM-YY");
    return dts;
}

function GetHotelAddress(HotelCode) {

    $.ajax({
        type: "POST",
        url: "handler/RoomHandler.asmx/GetHotelAddress",
        data: JSON.stringify({ HotelCode: HotelCode }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotelAddress = result.HotelAddress;
                $("#lbl_address").text("Address: " + arrHotelAddress.HotelAddress + ',' + arrHotelAddress.CityId + ',' + arrHotelAddress.CountryId)
            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })
}  

function GetAddDetails(RateID) {
    debugger
    var HotelID = HotelCode;
    var RateId = RateID;
    $.ajax({
        url: "../handler/TaxHandler.asmx/GetAddOnsMapping",
        type: "post",
        data: '{"HotelID":"' + HotelID + '","RoomId":"' + RateID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                arrAddOns = result.arrAddOns
                arrTax = result.arrTax
                arrAddOnsTaxces = result.arrAddOnsRate;
                addonsAdd(RateID)
                arrMeals = result.arrMeals;
                GenrateExtraMeal(RateID)

            }
            else {
                alert(result.ErrorMsg);
                setTimeout(function () {
                    window.location.href = "default.aspx", 2000
                }, 2000);
            }

        }
    })
}

function GenrateExtraMeal(RateID) {
    $.modal({
        content:
            '<div class="with-padding" id="AddMealRate"></div>',

        title: 'Add Meal Rate',
        width: 850,
        height: 200,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },

        buttonsLowPadding: true
    });
    var trRoomsRV = '';

    var Meal = $("#sel_MealPlan").val();
    try {
        for (var i = 0; i < arrMeals.length; i++) {
            var icon = '';
            trRoomsRV += '<div  class="columns MealRate">';
            trRoomsRV += '<div class="two-columns"> <a href="javascript:void(0)" class="button full-width txt_AddOns">'
            if (arrMeals[i].MealType == null)
                arrMeals[i].MealType = "";
            var COMPULSORY = "";
            var ListMP = arrMeals[i].MealType.split(',');

            var sMeal = $.grep(ListMP, function (p) { return p == Meal; })
                .map(function (p) { return p });

            if (sMeal.length != 0) {

                COMPULSORY = "checked disabled"
            }
            //else
            //    trRoomsRV += '<span class="button-icon red-gradient"><span class="icon-cross  txt_AddOns"></span>'
            trRoomsRV += arrMeals[i].AddOnName + '</a></div>';

            trRoomsRV += '<div class="two-columns">'
            trRoomsRV += '<input type="number" min="0"  value="0.00"     class="input full-width txtMeal">'
            trRoomsRV += '</div>';

            trRoomsRV += '<div class="two-columns">'
            trRoomsRV += '<input type="number" min="0"  value="0.00"     class="input full-width txtCWB">'
            trRoomsRV += '</div>';

            trRoomsRV += '<div class="two-columns">'
            trRoomsRV += '<input type="number" min="0"  value="0.00"     class="input full-width txtCNB">'
            trRoomsRV += '</div>';


            trRoomsRV += '<div class="two-columns">'
            trRoomsRV += '<select  name="validation-select" class="select full-width sel_Type">'
            trRoomsRV += '<option value="PN" selected="selected">Per Person</option>'
            //trRoomsRV += '<option value="PR">Per Room</option>'
            trRoomsRV += '    </select>'
            trRoomsRV += '</div>';

            trRoomsRV += '<div class="two-columns"><input type="checkbox" id="chk' + arrMeals[i].ID + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1" ' + COMPULSORY + '></div>'
            trRoomsRV += ' </div>';
        }
        trRoomsRV += '<div class="two-columns" style="float:right"> <button type="button" class="button black-gradient glossy" id="btn_MealRate" onclick="GenrateAddOnsRates(\'' + RateID + '\')">Update</button>'
        $("#AddMealRate").append(trRoomsRV);
    }
    catch (ex) {
        Success(ex)
    }

}

function GenrateAddOnsRates(RateID) {
    var ListAddOnsRates = new Array();
    var arrAddOnsRates = new Array();
    /*Meal Rates*/
    var arrMeal = $(".MealRate");
    for (var i = 0; i < arrMeal.length; i++) {
        var Meals = $($(arrMeal[i]).find(".txt_AddOns")[0]).text()
        var ID = $.grep(arrMeals, function (p) { return p.AddOnName == Meals; })
            .map(function (p) { return p.ID; })[0];
        var IsCumpulsury = false;
        if ($(arrMeal[i]).find(".checked").length != 0)
            IsCumpulsury = true;
        if (ID != undefined) {
            arrAddOnsRates.push({
                AddOnsID: ID,
                IsCumpulsury: IsCumpulsury,
                HotelID: GetQueryStringParams("sHotelID"),
                RateID: RateID,
                RateType: $(arrMeal[i]).find(".sel_Type")[0].outerText,
                Rate: $(arrMeal[i]).find(".txtMeal").val(),
                CWB: $(arrMeal[i]).find(".txtCWB").val(),
                CNB: $(arrMeal[i]).find(".txtCNB").val(),
            });
        }
    }
    ListAddOnsRates.push(arrAddOnsRates);

    var data = {
        ListAddOnsRates: ListAddOnsRates,
    }
    $.ajax({
        url: "handler/RoomHandler.asmx/UpdateMealRate",
        type: "post",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                Success("Meal Rate Updated");
                setTimeout(function () {
                    window.location.href = "Ratelist.aspx?sHotelID=" + HotelCode + "&HName=" + getParameterByName('HName');
                }, 100);
            }
            else {
                alert(result.ErrorMsg);
            }

        }
    })
}

function generateaddons(rateid,count) {
    try {
        
        $.modal({
            content:
                '<div class="with-padding" id="AddOnsRate"></div>',

            title: 'Add AddOns Rate',
            width: 600  ,
            height: 300,
            scrolling: true,
            actions: {
                'Close': {
                    color: 'red',
                    click: function (win) { win.closeModal(); }
                }
            },

            buttonsLowPadding: true
        });
        addonsAdd(rateid, count)
    } catch (ex) {

    }
}
var cnt = 0;
function addonsAdd(RateID,ID) {
    
    try {

        var id = ID + RateID;
        var html = '';
        
        html += '<div class="new-row columns TaxDetails' + RateID + '  divRow_' + id + '" id="divRow_' + id + '" style="height:100px">'
        html += '<div class="three-columns DivAddons">'
        html += '<input type="hidden" class="addonID' + ID + '" value="' + RateID + '"/>'
        html += '<select id="sel_Addons' + id + '" name="validation-select" class=" full-width sel_Addons">'
        html += '</select>'
        html += '</div>'
        html += ''

        html += '<div class="three-columns">'
        html += '<input class="input full-width txt_AddOnsRate" id="txtTaxDetails' + id + '" type="number" min="0" value="0.00">'
        html += '</div>'


        html += '<div class="three-columns">'
        html += '<select id="" name="validation-select" class="select full-width sel_Type">'
        html += '<option value="PN" selected="selected">Per Night</option>'
        // html += '<option value="PR" selected="selected">Per Room</option>'
        html += '    </select>'
        html += '</div>';


        html += '<div class="one-columns" id="Action' + id + '">'
        html += '<input type="checkbox" id="chk' + ID + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1" > '
        html += '<a id="lnk_' + id + '" onclick="addonsAdd(\'' + RateID + '\',\'' + (parseInt(ID) + 1) + '\');" class="button">'
        html += '<span class="icon-plus-round blue"></span>'
        html += '</a>'
        html += ' <a id="lnkR_' + id + '" onclick="addonsDelete(' + parseInt(ID) + ',\'' + id + '\',\'' + RateID + '\');" class="button"><span class="icon-minus-round red"></span></a>'
        html += '<br>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        $("#lnk_" + parseInt(ID - 1) + RateID + "").remove()
        //$("div_Add_" + parseInt(ID)).append()
        if (cnt==0) {
            html += '<div id="div_' + id + '" class="two-columns" style="float:right"> <button type="button" class="button black-gradient glossy" id="btn_MealRate" onclick="SaveAddOnsUpdate(\'' + RateID + '\',\'' + id + '\')">Update</button>'
        }
        else if (cnt > 0) {
           // var ncnt = parseFloat(id - 1);
            //$("#div_" + id + "").remove();
            $("#btn_MealRate").remove(); 
            html += '<div id="div_' + id + '" class="two-columns" style="float:right"> <button type="button" class="button black-gradient glossy" id="btn_MealRate" onclick="SaveAddOnsUpdate(\'' + RateID + '\',\'' + id + '\')">Update</button>'
        }
        
        $("#AddOnsRate").append(html);
        GenrateAddOnsType('sel_Addons' + id)
        AddCount++;
        cnt++;

        //if (RoomID != 0) {
        //    GenrateAddOns();
        //}
    }
    catch (ex) {
        alert(ex.message)
    }
}
function addonsDelete(ID, ROW, RateID) {
    $("#divRow_" + ROW).remove();
    if ($(".TaxDetails" + RateID).length == 1) {
        var html = '';
        var id = parseInt(ID - 1);
        $("#Action" + id + RateID).empty();
        html += '<a id="lnk_' + id + '" onclick="addonsAdd(\'' + RateID + '\',\'' + (parseInt(ID) + 1) + '\');" class="button">'
        html += '<span class="icon-plus-round blue"></span>'
        html += '</a>'
        html += ' <a id="lnkR_' + id + '" onclick="addonsDelete(' + parseInt(ID) + ',\'' + id + '\',\'' + RateID + '\');" class="button"><span class="icon-minus-round red"></span></a>'
        html += '<br>'
        $("#Action" + id + RateID).append(html);
    }
    AddCount--;
}
function GenrateAddOnsType(ID) {
    var Opt = '';
    Opt += '<option value="0" selected=selected style="color:red">Non Select</option>'
    for (var i = 0; i < arrAddOns.length; i++) {
        Opt += '<option value="' + arrAddOns[i].ID + '">' + arrAddOns[i].AddOnName + '</option>'
    }
    $('#' + ID).append(Opt)
    $('#' + ID).change(function () {
        console.log($(this).val());
    }).multipleSelect({
        selectAll: true,
        single: true,
        multiple: false,
        width: '100%',
        delimiter: '+ ',
        filter: true,
        keepOpen: false,
    })
    return Opt;
}

function SaveAddOnsUpdate(RateID, RowDivID) {
    var arrAddOnsRates = new Array();
    var ListAddOnsRates = new Array();
    /*Meal Rates*/
    var arrTaxDetails = $(".TaxDetails" + RateID);
    for (var i = 0; i < arrTaxDetails.length; i++) {
        var Addons = $($(arrTaxDetails[i]).find(".sel_Addons")[0]).val();
        var IsCumpulsury = false;
        if ($(arrTaxDetails[i]).find(".checked").length != 0)
            IsCumpulsury = true;

        if (Addons != null && Addons != 0) {
            arrAddOnsRates.push({
                AddOnsID: Addons,
                IsCumpulsury: IsCumpulsury,
                HotelID: GetQueryStringParams("sHotelID"),
                RateID: RateID,
                RateType: $($(arrTaxDetails[i]).find(".select-value")[0]).text(),
                Rate: $(arrTaxDetails[i]).find(".txt_AddOnsRate").val(),
            });
        }
    }
    ListAddOnsRates.push(arrAddOnsRates);

    var data = {
        ListAddOnsRates: ListAddOnsRates,
    }
    $.ajax({
        url: "handler/RoomHandler.asmx/UpdateMealRate",
        type: "post",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                Success("AddOns Rate Updated");
                setTimeout(function () {
                    window.location.href = "Ratelist.aspx?sHotelID=" + HotelCode + "&HName=" + getParameterByName('HName');
                }, 100);
            }
            else {
                alert(result.ErrorMsg);
            }

        }
    })
}
