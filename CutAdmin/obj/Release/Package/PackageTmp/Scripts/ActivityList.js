﻿$(function () {
    GetActivitylist();
})

//var arrActivity = new Array();
//function GetActivitylist() {
//    $("#tbl_Actlist").dataTable().fnClearTable();
//    $("#tbl_Actlist").dataTable().fnDestroy();

//    $.ajax({
//        url: "../Handler/ActivityHandller.asmx/ActList",
//        type: "post",
//        data: '{}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (data) {
//            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
//            if (result.retCode == 1) {
//                arrActivity = result.arrActivity
//                for (var i = 0; i < arrActivity.length; i++) {
//                    var html = '';
//                    html += '<tr><td style="width:3%" align="center">' + (i + 1) + '</td>'
//                    html += '<td style="width:20%"  align="center">' + arrActivity[i].Name + ' </td>'
//                    html += '<td style="width:10%"  align="center">' + arrActivity[i].City + ' ,' + arrActivity[i].Country + ' </td>'
//                    html += '<td style="width:30%"  align="center">'
//                    for (var j = 0; j < arrActivity[i].TourType.length; j++) {
//                        html += arrActivity[i].TourType[j] + " ";
//                    }
//                    if (arrActivity[i].Mode.length == 0)
//                        html += '-'
//                    html += '</td>'

//                    html += '<td style="width:10%" align="center">'
//                    for (var j = 0; j < arrActivity[i].Mode.length; j++) {
//                        html += arrActivity[i].Mode[j].Type.replace('TKT', '<img src="../fonts/glyphicons_free/glyphicons/png/glyphicons-67-tags.png" style="height: 15px;" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="Ticket Only"> ').replace('SIC', '<img src="../fonts/glyphicons_free/glyphicons/png/glyphicons-44-group.png" style="height: 15px;" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="Sharing"> ').replace('PVT', '<img src="../fonts/glyphicons_free/glyphicons/png/glyphicons-6-car.png" alt="" style="height: 15px;" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Private"> ');
//                    }
//                    if (arrActivity[i].Mode.length == 0)
//                        html += '-'
//                    html += '</td>'
//                    var checked = '';
//                    if (arrActivity[i].Status == "True")
//                        checked = 'checked';
//                    html += '<td class="align-center" id="' + arrActivity[i].Aid + '"><input type="checkbox" id="chk' + arrActivity[i].Aid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"  ' + checked + '   onclick="RedirectStatus(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Status + '\')"></td>';

//                    html += '<td class="align-center" style="vertical-align:middle;">';
//                    html += '<div class="button-group compact">';
//                    html += '<a class="button icon-pencil with-tooltip tracked" style="cursor:pointer" href="sightseeing.aspx?id=' + arrActivity[i].Aid + '" title="Edit Details "></a>';
//                    html += '<a href="#" class="button icon-read"  onclick="Rates(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Name + '\',\'' + arrActivity[i].Location + '\',\'' + arrActivity[i].City + '\',\'' + arrActivity[i].Country + '\')" title="Edit Rates"></a>';
//                    html += '<a  class="button icon-cart with-tooltip" title="Inventory" href=""></a>';
//                    html += '<a href="#" class="button icon-trash" title="Delete" onclick="Delete(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Name + '\')"></a>';

//                    html += '</div>';
//                    html += '</td>'

//                    html += '</tr>'
//                    $("#tbl_Actlist tbody").append(html);
//                }

//                $(".tiny").click(function () {
//                    $(this).find("input:checkbox").click();
//                })

//                $('[data-toggle="tooltip"]').tooltip()
//                $("#tbl_Actlist").dataTable({
//                    bSort: false, sPaginationType: 'full_numbers',
//                });
//                $("#tbl_Actlist").css("width", "100%")

//            }
//        }
//    })
//}

/*New Work 04/04/2019*/
var arrActivity = new Array();
var arrSlots = new Array();
function GetActivitylist() {
    $("#tbl_Actlist").dataTable().fnClearTable();
    $("#tbl_Actlist").dataTable().fnDestroy();

    $.ajax({
        url: "../Handler/ActivityHandller.asmx/GetActivityList",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                arrActivity = result.arrActivity;
                arrSlots = result.arrSlot;
                for (var i = 0; i < arrActivity.length; i++) {
                    var html = '';
                    html += '<tr id="' + arrActivity[i].Activity_Id + '"><td style="width:3%" align="center">' + (i + 1) + '</td>'
                    html += '<td style="width:20%"  align="center">' + arrActivity[i].Act_Name + ' </td>'
                    html += '<td style="width:10%"  align="center">' + arrActivity[i].City + ' </td>'
                    html += '<td style="width:10%"  align="center">' + arrActivity[i].Country + ' </td>'
                    html += '<td style="width:30%"  align="center">'
                    var TourType = arrActivity[i].Tour_Type.split(";");
                    for (var j = 0; j < TourType.length - 1; j++) {
                        html += '<small class="tag orange-bg">' + TourType[j] + ' </small> '
                        //html += '<img src="../images/activities_icons/' + TourType[j].trim() + '.png" style="width: 30px;margin-right: 10px;"  data-placement="bottom" title="' + TourType[j].trim() + '" alt="" data-original-title="' + TourType[j].trim() + '">'
                    }
                    html += '</td>'
                    var checked = '';
                    if (arrActivity[i].Status == true)
                        checked = 'checked';
                    html += '<td class="align-center" id="' + arrActivity[i].Activity_Id + '"><input type="checkbox" id="chk' + arrActivity[i].Activity_Id + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"  ' + checked + '   onclick="RedirectStatus(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Status + '\')"></td>';

                    html += '<td class="align-center" style="vertical-align:middle;">';
                    html += '<div class="button-group compact">';
                    html += '<a class="button icon-pencil with-tooltip tracked" style="cursor:pointer" href="sightseeing.aspx?id=' + arrActivity[i].Activity_Id + '" title="Edit Details "></a>';
                    //html += '<a href="#" class="button icon-read"  onclick="Rates(\'' + arrActivity[i].Activity_Id + '\',\'' + arrActivity[i].Act_Name + '\',\'' + arrActivity[i].City + '\',\'' + arrActivity[i].Country + '\')" title="Edit Rates"></a>';
                    html += '<a href="sightseeingrates.aspx?sSightseeingID=' + arrActivity[i].Activity_Id + '&SightseeingName=' + arrActivity[i].Act_Name + '&Location=' + "" + '&City=' + arrActivity[i].City + '&Country=' + arrActivity[i].Country + '" class="button icon-read" title="Edit Rates"></a>';
                    html += '<a  class="button icon-cart with-tooltip" title="Inventory" href="sightseeingavailability_pricing.aspx?sSightseeingID=' + arrActivity[i].Activity_Id + '&SightseeingName=' + arrActivity[i].Act_Name + '&Location=' + "" + '&City=' + arrActivity[i].City + '&Country=' + arrActivity[i].Country + '"></a>';
                    html += '<a href="#" class="button icon-trash" title="Delete" onclick="Delete(\'' + arrActivity[i].Activity_Id + '\',\'' + arrActivity[i].Act_Name + '\')"></a>';

                    html += '</div>';
                    html += '</td>'

                    html += '</tr>'
                    $("#tbl_Actlist tbody").append(html);
                }
                $(".tiny").click(function () {
                    $(this).find("input:checkbox").click();
                })
                GenrateTable()
                $("#tbl_Actlist").dataTable(
                   {
                       sSort: true, sPaginationType: 'full_numbers',
                   });
                $("#tbl_Actlist").removeAttr("style");
            }
            else if (result.retCode == 0) {
                $("#tbl_Actlist").dataTable(
                    {
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                $("#tbl_Actlist").removeAttr("style");
            }
        }
    })
}

function GenrateTable() {
    $.template.init();
    $('#tbl_Actlist').tablesorter({
        headers: {
            0: { sorter: false },
            6: { sorter: false }
        }
    }).on('click', 'tbody td', function (event) {
        debugger
        var ID = this.parentNode.id;
        if (ID == "")
            return true;
        if (event.target !== this) {
            return;
        }
        var tr = $(this).parent(),
            row = tr.next('.row-drop'),
            rows;
        if (tr.hasClass('row-drop')) {
            return;
        }
        if (row.length > 0) {
            tr.children().removeClass('anthracite-gradient glossy');
            row.remove();
            return;
        }
        rows = tr.siblings('.row-drop');
        if (rows.length > 0) {
            rows.prev().children().removeClass('anthracite-gradient glossy');
            rows.remove();
        }
        // Add fake row
        $('<tr  class="row-drop">' +
            '<td  colspan="' + tr.children().length + '">' +
            '<div class="columns">' +
            '<div id="DropLeft" class="four-columns"></div>' +
            '<div id="Dropmiddle" class="three-columns align-center"> </div>' +
            '<div id="DropRight" class="five-columns" style=display:none> </div>' +
            '</div>' +
            '</td>' +
            '</tr>').insertAfter(tr);
        DropRowwithId(ID)
    }).on('sortStart', function () {
        var rows = $(this).find('.row-drop');
        if (rows.length > 0) {
            rows.prev().children().removeClass('anthracite-gradient glossy');
            rows.remove();
        }
    });
}

function DropRowwithId(ID) {
    var trDropleft = "";
    var trDropRight = "";

    var ActivityAddress = $.grep(arrActivity, function (p) { return p.Activity_Id == ID; })
        .map(function (p) { return p; });

    trDropRight = "";
    //Left
    trDropleft += '<label class="font11 silver">' + "" + '<br>' + ActivityAddress[0].City + ',' + ActivityAddress[0].Country + '&nbsp;&nbsp;&nbsp;<a onclick="GetMap(\'' + ID + '\')" class="icon-marker icon-size2 font11 silver" title="Map"></a></label>';
    //Right
    trDropRight += '<a style="float: right;" class="mid-margin-left font11 white pointer Inventory" onclick="SetInventory(\'' + ID + '\',\'' + ActivityAddress[0].Act_Name + '\',\'' + "" + '\',\'' + ActivityAddress[0].City + '\', \'' + ActivityAddress[0].Country + '\')">Add Inventory</a>';
    $('#DropLeft').append(trDropleft);
    $('#DropRight').append(trDropRight);
    $('.Inventory').menuTooltip($('#Inventoryfilter').hide(), {
        classes: ['with-small-padding', 'full-width', 'anthracite-gradient']
    });

    $(".mini").click(function () {
        $(this).find("input:checkbox").click();
    })
}

//function SetInventory(ActivityID, ActivityName, ActivityLocation, ActivityCity, ActivityCountry) {
//    try {
//        $("#txt_ActivityID").val(ActivityID);
//        $("#txt_ActivityName").val(ActivityName);
//        $("#txt_ActivityLocation").val(ActivityLocation);
//        $("#txt_ActivityCity").val(ActivityCity);
//        $("#txt_ActivityCountry").val(ActivityCountry);
//    } catch (e) { }
//}

//function AddInventory() {
//    try {
//        if ($("#frm_Inventory").validationEngine('validate')) {
//            window.location.href = $("#sel_Inventory").val() + "?AID=" + $("#txt_ActivityID").val() + "&AName=" + $("#txt_ActivityName").val() + "&Location=" + $("#txt_ActivityLocation").val() + "&City=" + $("#txt_ActivityCity").val() + "&Country=" + $("#txt_ActivityCountry").val();
//        }
//    }
//    catch (e) {
//    }
//}

/*New Work */

function RedirectStatus(aid, value) {
    if (value == 'True')
        value = "False";
    else
        value = 'True'

    var data = {
        Actid: aid,
        Status: value
    }
    var Name = $.grep(arrActivity, function (p) { return p.Aid == aid; }).map(function (p) { return p.Name; })[0];
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to ' + value.replace("False", "de-active").replace("True", "active") + " inventory for<br/><span class=\"orange\"> " + Name + '</span>?</p>', function () {
        $.ajax({
            url: "../Handler/ActivityHandller.asmx/RedirectStatus",
            type: "post",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    Success("Status Updated");
                    GetActivitylist()
                }
                else
                    Success("Something Went Wrong");
            }
        })
    }, function () {
        $('#modals').remove();
    });
}

function Delete(Id) {

    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are You want to delete this activity?</p>', function () {
        Deletee(Id);
    }, function () {
        $('#modals').remove();
    });

}

function Deletee(Id) {
    debugger;
    $.ajax({
        url: "../Handler/ActivityHandller.asmx/DeleteActivity",
        type: "post",
        data: '{"id":"' + Id + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                Success("Activity deleted.");
                GetActivitylist();
                //$("#" + sid + '_' + No).remove();
                //$('[data-toggle="tooltip"]').tooltip()
            }
        }
    })
}