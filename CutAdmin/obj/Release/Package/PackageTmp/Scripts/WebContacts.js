﻿var HotelList = "";
var HotelCountryList = "";
var Div = '';
var OfferList = "";
var sid = 0;
var HotelName = "";
$(document).ready(function () {
    debugger

    GetAllCPContactList();
    AddContactDetailUI();
    sid = GetQueryStringParams('sid');
    if (sid != undefined) {
        $("#btn_update").show();
        $("#btn").hide();
        GetContactDetailByContactType(sid);
    }

});
var arrCountry = "";
function GetCountry(Id) {
    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.CountryList;
                Arr_Country = result.CountryList;
                if (arrCountry.length > 0) {
                    $("#selCountry").empty();
                    $("#selTerCityCountry").empty();

                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Country + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    //if (Id == 1) {
                    $("#selCountry" + Id).append(ddlRequest);

                    //}
                    //else {
                    //$("#selCountry" + Id).append(ddlRequest);
                    //}
                    //$("#selCountry").append(ddlRequest);
                    //$("#selTerCityCountry").append(ddlRequest);
                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });
}

var Count = 1;
//var Count = 1;
function AddContactDetailUI() {
    var Div = "";
    Div += '<div class="columns" id="MyDiv' + Count + '">'
    Div += '<div class="three-columns">'
    Div += '<label class="text-left" id="label_SupportType' + Count + '">Support Type<label class="red">*</label></label>'
    Div += '<div class="full-width button-height typeboth" id ="DivSupportType' + Count + '" >'
    Div += '<select id="selSupportType' + Count + '" class="select SupportType" name="SupportType" style="width:230px">'
    Div += '<option selected="selected" value="-">Support Type</option>'
    Div += '<option value="Sales">Sales</option>'
    Div += '<option value="Customer">Customer</option>'
    Div += '<option value="Account">Account</option>'
    Div += '</select>'
    Div += '</div >'
    //Div += '<span class="text-left" id="label_CoustomerSupportType' + Count + '">Coustomer Support Type<span class="red">*</span></span>'
    //Div += '<input class="input full-width" type="text" id="txt_CoustomerSupportType' + Count + '" placeholder="Support Type" />'
    Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_selSupportType' + Count + '"><b>* This field is required</b></label>'
    Div += '</div >'
    Div += '<div class="three-columns">'
    Div += '<label class="text-left" id="label_selCountry' + Count + '">Support Country <label class="red">*</label></label>'
    Div += '<div class="full-width button-height" id = "DivselCountry' + Count + '" >'
    Div += '<select id="selCountry' + Count + '" class="select OfferType">'
    Div += '</select>'
    Div += '</div >'
    //Div += '<input type="text" class="input full-width name" id="txtSeasonName' + Count + '"/>'
    Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_selCountry' + Count + '">'
    Div += '<b>* This field is required</b></label>'
    Div += '</div>'

    Div += '<div class="two-columns">'
    Div += '<label class="text-left" id="label_CPNo' + Count + '">Support No <label class="red">*</label></label>'
    Div += '<input class="input full-width CPNo" type="text" id="CPNo' + Count + '"  placeholder="Coustomer Support No"/>'
    Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_CPNo' + Count + '">'
    Div += '<b>* This field is required</b></label>'
    Div += '</div>'

    Div += '<div class="two-columns">'
    Div += '<label class="text-left" id="label_CPEmail' + Count + '">Support Email <label class="red">*</label></label>'
    Div += '<input class="input full-width CPEmail" type="text" id="CPEmail' + Count + '"  placeholder="Coustomer Support Email" />'
    Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_CPEmail' + Count + '">'
    Div += '<b>* This field is required</b></label>'
    Div += '</div>'

    Div += '<div class="one-columns">'
    Div += '<div id="btnAddContactDetail' + Count + '" class="one-columns" title="Add Support Detail" style="padding-top: 80%">'
    Div += '<i Onclick="CheckEmpty(\'' + Count + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button anthracite-gradient" ><span class="icon-plus"></span></label></i>'
    Div += '</div>'



    Div += '</div>'
    if (Count != 1) {
        $("#btnAddContactDetail" + parseInt(Count - 1)).hide();
        Div += '<div class="one-columns" title="Remove Contact Detail" id="div_rembtn' + Count + '" style="margin-top: 23px">'
        Div += '<i Onclick="RemoveContactDetailUI(\'' + Count + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button anthracite-gradient" ><span class="icon-minus"></span></label></i>'
        Div += '</div>'
        var remcount = parseFloat(Count - 1);
        if (Count > 2) {
            $("#div_rembtn" + remcount).hide();
        }
    }

    Div += '</div>'
    //if (Count > 1) {
    //    $("#label_SupportType" + Count).css("display", "none");
    //    $("#label_selCountry" + Count).css("display", "none");
    //    $("#label_CPNo" + Count).css("display", "none");
    //    $("#label_CPEmail" + Count).css("display", "none");
    //}
    $("#ContactDetailUI").append(Div);
    GetCountry(Count);
    //AppendControls(Count);
    Count += 1;
    //Count++;
}
function CheckEmpty(Count) {
    var chk = true;
    //var Ndt = parseInt(parseInt(dt) + parseInt(1));
    //var Ntxt = parseInt(parseInt(txt) + parseInt(1));
    //var Ndt = parseInt(parseInt(dt) + parseInt(1));
    if (Count == 1) {

        if ($("#CPNo" + Count).val() == "") {
            $("#lbl_CPNo" + Count).css("display", "");
            chk = false;
        }
        else {
            $("#lbl_CPNo" + Count).css("display", "none");
        }

        if ($("#CPEmail" + Count).val() == "") {
            $("#lbl_CPEmail" + Count).css("display", "");
            chk = false;
        }
        else {
            $("#lbl_CPEmail" + Count).css("display", "none");
        }
    }
    else {
        if ($("#CPNo" + Count).val() == "") {
            $("#lbl_CPNo" + Count).css("display", "");
            chk = false;
        }
        else {
            $("#lbl_CPNo" + Count).css("display", "none");
        }

        if ($("#CPEmail" + Count).val() == "") {
            $("#lbl_CPEmail" + Count).css("display", "");
            chk = false;
        }
        else {
            $("#lbl_CPEmail" + Count).css("display", "none");
        }

    }
    if ($("#selCountry" + Count).val() == "-") {
        $("#lbl_selCountry" + Count).css("display", "");
        chk = false;
    }
    else {
        $("#lbl_selCountry" + Count).css("display", "none");
    }
    if ($("#selSupportType" + Count).val() == "-") {
        $("#lbl_selSupportType" + Count).css("display", "");
        chk = false;
    }
    else {
        $("#lbl_selSupportType" + Count).css("display", "none");
    }
    //GetCountry(Count);
    if (chk) {
        AddContactDetailUI();
    }
}
function RemoveContactDetailUI(Cnt) {
    var c = parseInt(Cnt) - 1;

    $("#MyDiv" + Cnt).remove();
    $("#btnAddContactDetail" + c).show();
    Count = Count - 1;
    $("#div_rembtn" + c).show();
}
function Validate() {
    bValid = true;
    arrListCustumer = new Array();
    for (var i = 0; i < $(".SupportType").length; i++) {

        var ContactType = $("#selSupportType" + (parseInt(i + 1))).val();
        if (ContactType == "-" || ContactType == undefined) {
            return false;
        }
        var MobileNo = $(".CPNo")[i].value;
        if (MobileNo == "") {
            $('#lbl_' + $(".CPNo")[i].id).css("display", "");
            return false;
        }
        else {
            $('#lbl_' + $(".CPNo")[i].id).css("display", "none");
        }
        var sEmails = $(".CPEmail")[i].value;
        if (sEmails == "") {
            $('#lbl_' + $(".CPEmail")[i].id).css("display", "");
            return false;
        }
        else {
            $('#lbl_' + $(".CPEmail")[i].id).css("display", "none");
        }
        var sCountry = $("#selCountry" + (parseInt(i + 1))).val();
        if (sCountry == "" || sCountry == undefined) {
            return false;
        }
        arrListCustumer.push({
            ContactNationality: sCountry,
            ContactType: ContactType,
            ContactEmail: sEmails,
            ContactNo: MobileNo,
        });
    }
    return bValid;
}
function Validate2() {
    bValid = true;
    arrListCustumer = new Array();
    for (var i = 0; i < document.getElementsByName("SupportType").length; i++) {
        //var sid = $(".sid")[i].value;
        var ContactType = $("#selSupportType" + (parseInt(i))).val();
        if (ContactType == "-" || ContactType == undefined) {
            return false;
        }
        var MobileNo = $(".CPNo")[i].value;
        if (MobileNo == "") {
            $('#lbl_' + $(".CPNo")[i].id).css("display", "");
            return false;
        }
        else {
            $('#lbl_' + $(".CPNo")[i].id).css("display", "none");
        }
        var sEmails = $(".CPEmail")[i].value;
        if (sEmails == "") {
            $('#lbl_' + $(".CPEmail")[i].id).css("display", "");
            return false;
        }
        else {
            $('#lbl_' + $(".CPEmail")[i].id).css("display", "none");
        }
        var sCountry = $("#selCountry" + (parseInt(i))).val();
        if (sCountry == "" || sCountry == undefined) {
            return false;
        }
        arrListUpdate.push({
            ContactNationality: sCountry,
            ContactType: ContactType,
            ContactEmail: sEmails,
            ContactNo: MobileNo,
            //sid: sid,
        });
    }
    return bValid;
}
var arrListCustumer = new Array();
var arrListUpdate = new Array();
function AddDetail() {
    bValid = Validate();
    if (bValid == true) {
        var Data = {
            arrContacts: arrListCustumer
        };
        $.ajax({
            type: "POST",
            url: "../Handler/VehicleMasterHandller.asmx/AddCustomerSupportDetails",
            data: JSON.stringify(Data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {

                var obj = JSON.parse(response.d)
                if (obj.retCode == 1) {
                    // var Offer_Id = obj.Offer_Id;
                    Success("Details Save Successfully.");
                    window.location.reload();
                    ClearAll();
                }
            },
        });
    }
    else
        Success("Please fill All Compulsary Details");
}
var OfferId;
function UpdateDetail() {
    bValid = Validate2();
    //arrLisCustumer = new Array();
    if (bValid == true) {
        var Data = {
            arrContacts: arrListUpdate
        };
        //var Data = {
        //    sid: sid, ContactType: ContactType, Country: Country, CPNo: CPNo, CPEmail: CPEmail, ContactType: ContactType
        //};

        $.ajax({
            type: "POST",
            url: "../handler/VehicleMasterHandller.asmx/UpdateCustomerSupportDetails",
            data: JSON.stringify(Data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {

                var obj = JSON.parse(response.d)
                if (obj.retCode == 1) {
                    // var Offer_Id = obj.Offer_Id;
                    Success("Contact Detail Updated.");
                    setTimeout(function () {
                        window.location.href = "AddUpdateWebContacts.aspx";
                    }, 500);
                }
            },
        });
    }


}

function AppendsupportType(Typename) {
    debugger;
    try {
        //var checkclass = document.getelementsbyclassname('check');
        $("#DivSupportType0 .select span")[0].textcontent = Typename;
        $('input[value="' + Typename + '"][class="SupportType"]').prop("selected", true);
        $("#selSupportType0").val(Typename);
    }
    catch (ex) { }
}
//function AppendCountry(id) {
//    debugger;
//    try {
//        var checkclass = document.getelementsbyclassname('check');
//        //var VehicleBrand = $.grep(arrVehicleType, function (p) { return p.ID == VehicleType; })
//        //    .map(function (p) { return p.VehicleBrand; });
//        //$("#DivVehicleType2 .select span")[0].textcontent = VehicleBrand;
//        //for (var i = 0; i < arrVehicleType.length; i++) {
//        //    if (arrVehicleType[i].ID == VehicleType) {
//        $("#DivselCountry .select span")[id].textcontent = VehicleBrand;
//                $('input[value="' + VehicleType + '"][class="Vehicle"]').prop("selected", true);
//                $("#selVehicleType2").val(VehicleType);
//            //    //  pgta += gta[i] + ",";
//            //    var selected = [];
//            //}


//        }
//    }
//    catch (ex) { }
//}
function EditContact(ContactType) {
    $("#btn_update").show();
    $("#btn").hide();
    $("#ContactDetailUI").empty();
    var arrContact = $.grep(List_Contact, function (H) { return H.ContactType == ContactType })
        .map(function (H) { return H.objContacts; });
    $("#ContactDetailUI").empty();
    var Div = "";
    for (var i = 0; i < arrContact[0].length; i++) {

        Div += '<div class="columns" id="MyDiv' + i + '">'

        //Div += '<div class="one-columns" style="display:none;">'
        //Div += '<input class="input full-width sid" type="hidden"  value="' + arrContact[0][i].sid + '" id="sid' + i + '" />'
        //Div += '</div>'

        Div += '<div class="three-columns">'
        Div += '<label class="text-left" id="label_SupportType' + i + '">Support Type<label class="red">*</label></label>'
        Div += '<div class="full-width button-height typeboth" id ="DivSupportType' + i + '" >'
        Div += '<select id="selSupportType' + i + '" class="select SupportType' + i + '" name="SupportType" style="width:230px" disabled>'
        Div += '<option selected="selected" value="-">Support Type</option>'
        Div += '<option value="Sales">Sales</option>'
        Div += '<option value="Customer">Customer</option>'
        Div += '<option value="Account">Account</option>'
        Div += '</select>'
        Div += '</div >'
        Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_selSupportType' + i + '"><b>* This field is required</b></label>'
        Div += '</div >'

        Div += '<div class="three-columns">'
        Div += '<label class="text-left" id="label_selCountry' + i + '">Support Country <label class="red">*</label></label>'
        Div += '<div class="full-width button-height" id = "DivselCountry' + i + '" >'
        Div += '<select id="selCountry' + i + '" class="select OfferType' + i + '" name="OfferType">'
        Div += '</select>'
        Div += '</div >'


        Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_selCountry' + i + '">'
        Div += '<b>* This field is required</b></label>'
        Div += '</div>'

        GetCountry(i);

        Div += '<div class="two-columns">'
        Div += '<label class="text-left" id="label_CPNo' + i + '">Support No <label class="red">*</label></label>'
        Div += '<input class="input full-width CPNo" type="text" value="' + arrContact[0][i].ContactNo + '" id="CPNo' + i + '"  placeholder="Coustomer Support No"/>'
        Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_CPNo' + i + '">'
        Div += '<b>* This field is required</b></label>'
        Div += '</div>'

        Div += '<div class="two-columns">'
        Div += '<label class="text-left" id="label_CPEmail' + i + '">Support Email <label class="red">*</label></label>'
        Div += '<input class="input full-width CPEmail" type="text" value="' + arrContact[0][i].ContactEmail + '" id="CPEmail' + i + '"  placeholder="Coustomer Support Email" />'
        Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_CPEmail' + i + '">'
        Div += '<b>* This field is required</b></label>'
        Div += '</div>'


        if (i == parseFloat(arrContact[0].length - 1)) {
            Div += '<div class="one-columns">'
            Div += '<div id="btnAddContactDetail' + i + '" class="one-columns" title="Add Support Detail">'
            Div += '<i Onclick="CheckEmpty(\'' + i + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button anthracite-gradient" ><span class="icon-plus"></span></label></i>'
            Div += '</div>'
        }

        Div += '</div>'
        if (i >= 1) {
            $("#btnAddContactDetail" + parseInt(i - 1)).hide();
            Div += '<div class="one-columns" title="Remove Contact Detail">'
            Div += '<i Onclick="RemoveContactDetailUI(\'' + i + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button anthracite-gradient" ><span class="icon-minus"></span></label></i>'
            Div += '</div>'
        }
        Div += '</div>'
    }
    $("#ContactDetailUI").append(Div);
    SetTourType(arrContact);
    setTimeout(function () { SetSupCountry(arrContact) }, 5000);
}

function SetTourType(arrContact) {
    for (var i = 0; i < arrContact[0].length; i++) {
        $("#DivSupportType" + i + " .select span")[0].textContent = arrContact[0][i].ContactType;
        $('input[value="' + arrContact[0][i].ContactType + '"][class="SupportType' + i + '"]').prop("selected", true);
        $("#selSupportType" + i + "").val(arrContact[0][i].ContactType);
    }
}

function SetSupCountry(arrContact) {
    for (var i = 0; i < arrContact[0].length; i++) {

        var CountryName = $.grep(arrCountry, function (p) { return p.Country == arrContact[0][i].ContactNationality; })
            .map(function (p) { return p.Countryname; });
        $("#DivselCountry" + i + " .select span")[0].textContent = CountryName;
        for (var j = 0; j < arrCountry.length; j++) {
            if (arrCountry[j].Country == arrContact[0][i].ContactNationality) {
                $("#DivselCountry" + i + " .select span")[0].textContent = arrCountry[j].Countryname;
                $('input[value="' + arrCountry[j].Country + '"][class="OfferType' + i + '"]').prop("selected", true);
                $("#selCountry" + i + "").val(arrCountry[j].Country);
            }
        }


    }
}

function UpdateContactDetailUI(arrContact) {
    //if (result.retCode == 1) {
    var Div = "";
    //$("#txt_CoustomerSupportType").val(list_ContactList[0].ContactType);
    $("#ContactDetailUI").empty();
    //var length = list_ContactList.length;
    for (var i = 0; i < list_ContactList.length; i++) {


    }
}
var List_Contact;
function GetAllCPContactList() {
    var Divs = '';
    $("#tbl_CPDetails").dataTable().fnClearTable();
    $("#tbl_CPDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../handler/VehicleMasterHandller.asmx/GetAllCPContactList",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            List_Contact = obj.tbl_ContactList;
            if (obj.retCode == 1) {
                for (var i = 0; i < List_Contact.length; i++) {
                    Divs += '<tr>'
                    Divs += '<td class="align-center">' + parseInt(parseInt(i) + parseInt(1)) + '</td>'
                    Divs += '<td class="align-center">' + List_Contact[i].ContactType + '</td>'
                    Divs += '<td class="align-center">'
                    for (var c = 0; c < List_Contact[i].objContacts.length; c++) {
                        Divs += List_Contact[i].objContacts[c].ContactNationality + '<br/>'
                    }
                    Divs += '</td>'

                    Divs += '<td class="align-center">'
                    for (var c = 0; c < List_Contact[i].objContacts.length; c++) {
                        Divs += List_Contact[i].objContacts[c].ContactNo + '<br/>'
                    }
                    Divs += '</td>'

                    Divs += '<td class="align-center">'
                    for (var c = 0; c < List_Contact[i].objContacts.length; c++) {
                        Divs += List_Contact[i].objContacts[c].ContactEmail + '<br/>'
                    }
                    Divs += '</td>'
                    //Divs += '<td class="align-center">'
                    //Divs += '<a href="#" title="Edit" class="button" style="" onclick="EditContact(\'' + List_Contact[i].ContactType + '\')"><span class="icon-pencil"></span></a>'
                    //Divs += '<a  href="#" class="button" title="Delete" onclick="DeleteCustomerSupportDetails(\'' + List_Contact[i].ContactType + '\')"><span class="icon-trash"></span></a>'
                    //Divs += '</td>'

                    Divs += '<td class="align-center" style="vertical-align:middle;">';
                    Divs += '<div class="button-group compact">';
                    Divs += '<a class="button icon-pencil with-tooltip" title="Edit Details"  onclick="EditContact(\'' + List_Contact[i].ContactType + '\')"></a>'
                    Divs += '<a href="#" class="button icon-trash with-tooltip confirm" title="Delete" onclick="DeleteCustomerSupportDetails(\'' + List_Contact[i].ContactType + '\')"></a>'
                    Divs += '</div ></td>';

                    Divs += '</tr>'
                }
            }
            $("#tbl_CPDetails tbody").append(Divs);
            $("#tbl_CPDetails").dataTable(
                {
                    bSort: true, sPaginationType: 'full_numbers',
                });

        },
    });
}
function SearchByContactType() {
    var ContactType = $("#txt_CoustomerSupportType2").val();
    var Divs = '';
    $("#tbl_CPDetails").dataTable().fnClearTable();
    $("#tbl_CPDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../handler/VehicleMasterHandller.asmx/GetContactDetailByContactType",
        data: '{"ContactType":"' + ContactType + '"}',
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            var List_Contact = obj.tbl_ContactList;
            if (obj.retCode == 1) {
                for (var i = 0; i < List_Contact.length; i++) {
                    Divs += '<tr>'
                    Divs += '<td>' + parseInt(parseInt(i) + parseInt(1)) + '</td>'
                    Divs += '<td>' + List_Contact[i].ContactType + '</td>'
                    Divs += '<td>' + List_Contact[i].Countryname + '</td>'
                    Divs += '<td>' + List_Contact[i].ContactNo + '</td>'
                    Divs += '<td>' + List_Contact[i].ContactEmail + '</td>'

                    //Divs += '<td align="center" style="width:10%"><i style="cursor:pointer" aria-hidden="true" title="Update Details"><label class="icon-pencil icon-size2" onclick="EditOffer(\'' + List_Contact[i].OfferID + '\')" ></label></i> | <i style="cursor:pointer" onclick="Delete(\'' + List_Contact[i].OfferID + '\',\'' + List_Contact[i].sid + '\')" aria-hidden="true" title="Delete"><label class="icon-trash" ></label></i></td>'
                    Divs += '<td class="align-center">'
                    Divs += '<a href="#" title="Edit" class="button" style="" onclick="EditContact(\'' + List_Contact[i].sid + '\')"><span class="icon-pencil"></span></a>'
                    Divs += '<a  href="#" class="button" title="Delete" onclick="DeleteCustomerSupportDetails(\'' + List_Contact[i].ContactType + '\')"><span class="icon-trash"></span></a>'
                    Divs += '</td>'
                    Divs += '</tr>'
                }
            }
            $("#tbl_CPDetails tbody").append(Divs);
            $("#tbl_CPDetails").dataTable(
                {

                    bSort: true, sPaginationType: 'full_numbers',

                });

        },
    });
}

//function AppendControls(Id) {
//    GetCountry()

//}

function DeleteCustomerSupportDetails(ContactType) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete<br/> <span class=\"orange\">' + ContactType + '</span>?</span>?</p>',
        function () {
            $.ajax({
                url: "../Handler/VehicleMasterHandller.asmx/DeleteCustomerSupportDetails",
                type: "post",
                data: '{"ContactType":"' + ContactType + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Contact has been deleted successfully!");
                        window.location.reload();
                    } else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}