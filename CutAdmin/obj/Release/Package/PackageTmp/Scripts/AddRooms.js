﻿var HotelCode = 0; var HotelName; var RoomId = 0; var AmenityList = []; var code1;
var ID = "";

$(function () {
    debugger
    HotelCode = GetQueryStringParams('sHotelID');
    HotelName = GetQueryStringParams('HotelName');
    RoomId = GetQueryStringParams('RoomId');
    if (GetQueryStringParams('RoomId') == undefined) {
        RoomId = "0"
        GetRoomType();
        GetRoomAmenities();
    }
    else {
        ID = RoomId;
        GetRoomType();
        getRoomwithId();
    }
});

function getRoomwithId() {
    try {
        var data =
         {
            RoomId: RoomId,
            HotelCode: HotelCode
         }
        post("../handler/RoomHandler.asmx/getRoomwithId", data, function (result) {
            RoomList = result.RoomList;
            RoomType = result.RoomType;
            AmenityList = result.AmenityList;
            HRoomTypeID =RoomList.RoomTypeId;
            $('#txtRoomType').val(RoomType.RoomType);
            $('#QtyOccupacy').val(RoomList.RoomOccupancy);
            $('#RoomSize').val(RoomList.RoomSize);
            $('#AdultsWithoutChilds').val(RoomList.NoOfChildWithoutBed);
            $('#RoomDescription').val(RoomList.RoomDescription);
            $('#MaxExtrabedAllowed').val(RoomList.MaxExtrabedAllowed);
            $("#BeddingType").val(RoomList.BeddingType)
            $("#BeddingType").change();
            $("#SmokingAllowed").val(RoomList.SmokingAllowed)
            $("#SmokingAllowed").change();
            GetRoomAmenities();
            $('#RoomNotes').val(RoomList.RoomNote);
            if (RoomList.SubImages != null) {
                var SubImg = [];
                SubImg = RoomList.SubImages.split('^');
                if (SubImg.length != 0) {
                    var tdRequest = "";
                    var ImgRequest = '';
                    for (var j = 0; j < SubImg.length; j++) {
                        if (SubImg[j] != 'undefined' && SubImg[j] != '') {
                            ImgRequest += '<li><img src="RoomImages/' + RoomList.RoomId + "/" + SubImg[j] + '" width="185px">' +
                                           '<span class="input">' +
                                           '<input type="text" name="pseudo-input-1" id="pseudo-input-1" class="input-unstyled ImageFileName" value="' + SubImg[j] + '" size="12">' +
                                           '<span class="button-group center">' +
                                           '<label for="sbutton-radio-' + j + '" class="button green-active icon-monitor with-tooltip" title="Default Image">' +
                                           '<input type="radio" name="button-radio" id="sbutton-radio-' + j + '" value="1">' +
                                           '</label>' +
                                           '<label for="pseudo-input-1" class="button icon-trash with-tooltip" title="Delete" onclick="deletePreview(this, \'' + j + '\',\'' + SubImg[j] + '\')"></label>' +
                                           '</span>' +
                                           '</span>'
                            arrImages[j] = SubImg[j];
                        }
                    }
                    $('#image_preview').append(ImgRequest);
                    ImageProgress('image_preview')
                }
            }
        }, function error(result) {

        });
    } catch (e) {
        AlertDanger(e.message)
    }
}

function GetRoomType() {
    post("HotelHandler.asmx/GetRoomType", {}, function (result) {
        if (result.retCode == 1) {
            arrRoomType = result.RoomType;
            Div = '';
            for (var i = 0; i < arrRoomType.length; i++) {

                Div += '<option data-id=' + arrRoomType[i].RoomTypeID + '>' + arrRoomType[i].RoomType + '</option>'
            }
            $("#RoomType").append(Div);
            $("#RoomType1").append(Div);
        }
    }, function (error) {

    });
}

function GetRoomAmenities() {
    $("#divAddAmenities").empty();
    $("#divAddAmenities1").empty();
    post("HotelHandler.asmx/GetRoomAmenities", {}, function(result) {
        arrRoomAmenities = result.arrRoomAmenities;
        var chkRequest = '<div class="columns">';
        for (i = 0; i < arrRoomAmenities.length; i++) {
            chkRequest += '<div class="four-columns">'
            chkRequest += '<input type="checkbox" name="chkAmenities" onchange="AddAmenities(id)" id="HtlAmenities' + i + '" class="HotelAmenities validate[required]"   data-errormessage-value-missing="at least One Amenity is required!"  value="' + arrRoomAmenities[i].RoomAmunityID + '" title="' + arrRoomAmenities[i].RoomAmunityName + '"><label for="HtlAmenities' + i + '">' + arrRoomAmenities[i].RoomAmunityName + '</label></span>';
            chkRequest += '</div>'
        }
        $("#divAddAmenities").append(chkRequest);
        if (AmenityList.length > 0) {
            for (i = 0; i < arrRoomAmenities.length; i++) {
                for (var j = 0; j < AmenityList.length; j++) {
                    if (arrRoomAmenities[i].RoomAmunityName == AmenityList[j]) {
                        $(':checkbox[value="' + arrRoomAmenities[i].RoomAmunityID + '"]').prop('checked', true)
                    }
                }
            }
        }
    }, function (error) {
        AlertDanger(error.ex)
    })
}

$('#wizAmenities .wizard-next').filter(function () {
    return $(this).removeClass('.button').length == 1;
})

var AmenitiesCode;
function AddAmenities() {

    var addedAmenitiesValues = "";
    var rAmenities = document.getElementsByClassName('HotelAmenities')
    for (var i = 0; i < rAmenities.length; i++) {
        if (rAmenities[i].checked) {
            addedAmenitiesValues = addedAmenitiesValues + ',' + rAmenities[i].value;
        }
    }
    addedAmenitiesValues = addedAmenitiesValues.replace(/^,|,$/g, '');
    AmenitiesCode = addedAmenitiesValues;
}

function SaveAmenities() {
    var roomAmenityID = '';
    var RoomAmenityText = $('#txtAmenities').val();
    var RoomAmenityText1 = $('#txtAmenities1').val();
    if (RoomAmenityText == "" && RoomAmenityText1 == "") {
        Success("Please enter amenities name.");
        return false;
    }
    if (RoomAmenityText1 != "" && RoomAmenityText == "") {
        RoomAmenityText = RoomAmenityText1;
    }
    var HotelId = HotelCode;
    var data =
  {
      RoomAmenityText: RoomAmenityText,
      HotelId: HotelId
  }
    post("HotelHandler.asmx/AddRoomAmenity", data, function (result) {
        Success("Room Amenity Saved")
        GetRoomAmenities();
    }, function(error) {
        AlertDanger('Something went wrong')
    })
}


function getHotelDetails(HotelCode) {
  var data =
  {
      HotelCode: HotelCode
  }

    $.ajax({
        type: "POST",
        url: "HotelHandler.asmx/GetHotelByHotelId",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrHotelDetail = result.HotelListbyId[0];
                $('#HotelNameTitle').val(arrHotelDetail.HotelName);
                $('#HtlName1').val(arrHotelDetail.HotelName);
                $('#HtlRatings1').val(arrHotelDetail.HotelCategory);
                $('#HtlDescription1').val(arrHotelDetail.HotelDescription);
                $('#HtlAddress1').val(arrHotelDetail.HotelAddress);
                $('#htlCity1').val(arrHotelDetail.CityId);
                $('#htlCountry1').val(arrHotelDetail.CountryId);
                $('#htlZipcode1').val(arrHotelDetail.HotelZipCode);
                $('#htlLangitude1').val(arrHotelDetail.HotelLangitude);
                $('#htlLatitude1').val(arrHotelDetail.HotelLatitude);
                $('#txtChildAgeFrom1').val(arrHotelDetail.ChildAgeFrom);
                $('#txtChildAgeTo1').val(arrHotelDetail.ChildAgeTo);
                $('#txtTripAdviserLink1').val(arrHotelDetail.TripAdviserLink);
                $('#txtHotelGroup1').val(arrHotelDetail.HotelGroup);
                $('#txtCheckinTime1').val(arrHotelDetail.CheckinTime);
                $('#txtCheckoutTime1').val(arrHotelDetail.CheckoutTime);
                $('#selPatesAllowed1').val(arrHotelDetail.PatesAllowed);
                $('#selLiquorPolicy1').val(arrHotelDetail.LiquorPolicy);
                $('#TxtContactPerson1').val(arrHotelDetail.ContactPerson);
                $('#txtMobileNo1').val(arrHotelDetail.MobileNo);
                $('#txtEmail1').val(arrHotelDetail.EmailAddress);

                nFacilities = arrHotelDetail.HotelFacilities.split(',');
                if (arrHotelDetail.HotelImage != "") {
                    $('.selMainImage').attr('src', arrHotelDetail.HotelImage);
                    MainImage = arrHotelDetail.HotelImage;
                }
                if (arrHotelDetail.SubImages != "") {
                    var SubImg = [];
                    SubImg = arrHotelDetail.SubImages.split(',');
                    var tdRequest = "";
                    for (var i = 0; i < SubImg.length; i++) {
                        tdRequest += '<img src="' + SubImg[i] + '"  class="SubImage" width="30%" style="padding:1px;">';
                        SubImages += SubImg[i] + ',';
                    }
                    $('#image-holder2').append(tdRequest);

                }
                sid = arrHotelDetail.sid;
                HotelBedsCode = arrHotelDetail.HotelBedsCode;
                DotwCode = arrHotelDetail.DotwCode;
                MGHCode = arrHotelDetail.MGHCode;
                ExpediaCode = arrHotelDetail.ExpediaCode;
                GRNCode = arrHotelDetail.GRNCode;
            }
        },
    });
}
var HRoomTypeID="0";
var arrRoom = new Array();
var arrRoomType = new Array();
function AddRoomDetails() {
    try {
        debugger
        AddAmenities();
        SetImage();
        var val = $('#txtRoomType').val();
        var RoomTypeID = $('#RoomType option').filter(function () { return this.value == val; }).data('id');
        if (RoomTypeID == "" || RoomTypeID != undefined) {
            HRoomTypeID = RoomTypeID
        }
        arrRoom = {
            RoomId:RoomId,
            HotelId: HotelCode,
            RoomTypeId: HRoomTypeID,
            RoomAmenitiesId: GetAssignedAminity(),
            RoomOccupancy: $('#QtyOccupacy').val(),
            MaxExtrabedAllowed: $('#MaxExtrabedAllowed').val(),
            NoOfChildWithoutBed: $('#AdultsWithoutChilds').val(),
            RoomSize: $('#RoomSize').val(),
            BeddingType: $('#BeddingType').val(),
            SmokingAllowed: $('#SmokingAllowed').val(),
            RoomImage: MainImage,
            SubImages: SubImage,
            RoomDescription: $("#RoomDescription").val()
        } /*Room Type*/
        var arrRoomType ={
            RoomType: val,
            RoomTypeId: HRoomTypeID
            }
        post("HotelHandler.asmx/SaveRoom", { arrRoom: arrRoom, arrRoomType: arrRoomType }, function (result) {
            SaveDoc('RoomImages', "RoomImages/" + result.RoomID, function (done) {
                Success("Room Details Saved.")
                window.location.href = "hotellist.aspx";
            });
        }, function(error) {
            AlertDanger('Unable to save details please  try again.')
        })
    } catch (e) {
    
    }
    

}
function GetAssignedAminity() {
    var aminity = "";
    try {
        var arrnd = $('#divAddAmenities').find('input:checkbox');
        $(arrnd).each(function (index, nd) {
            if($(nd).is(":checked"))
            {
                aminity += $(nd).val() + ",";
            }
        });
    } catch (e) { }
    return aminity;
}