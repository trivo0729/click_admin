﻿$(function () {
    GetRateTypes();
});
var content = '<form id="frm_Rates"><div class="Exchange" >' +
    '<input type="hidden" id="Hdn_Plan" value="0" />' +
    '<div class="columns">' +
    '<div class="six-columns twelve-columns-mobile">' +
    '<label>Rate type:</label>' +
    '<input class="input full-width validate[required]" data-prompt-position="bottomLeft" id="txt_RateType" type="text" autocomplete="off">' +
    '</div>' +

    '<div class="six-columns twelve-columns-mobile">' +
    '<label>Valid Inventory:</label>' +
    '<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>' +
    '<select id="sel_Invt" name="validation-select" data-prompt-position="bottomLeft" class="select multiple-as-single easy-multiple-selection check-list expandable-list full-width validate[required]" multiple></select>' +
    '</div>' +
    '</div>' +
    '<div class="clear"></div>' +
    '</div></form>';

var arrTypes = new Array();
var arrInventory = [{ type: 'FR', InventoryType: 'FreeSale' , color:'green'},
                    { type: 'AL', InventoryType: 'Allocation', color:'blue'},
                    { type: 'AT', InventoryType: 'Allotment', color: 'red' }]
function GetRateTypes() {
    try {
        setTimeout(function () {
            $("#tbl_RateType").dataTable().fnClearTable();
            $("#tbl_RateType").dataTable().fnDestroy();
            post("../GenralHandler.asmx/GetRateType", { AdminID: HotelAdminID }, function (result) {
                var html = '';
                arrTypes = result.arrRateType;
                $(arrTypes).each(function (index, arrType) {
                    html += '<tr>';
                    html += '<td>' + (index + 1) + '</td>'
                    html += '<td style="text-align:center">' + arrType.RateType + '</td>';
                    html += '<td style="text-align:center">'
                    var arrInvt = arrType.InventoryType.split(',');
                    for (var i = 0; i < arrInvt.length; i++) {
                        if (arrInvt[i] == "")
                            continue;
                        var type = $.grep(arrInventory, function (H) { return H.type == arrInvt[i] })
                            .map(function (H) { return H; })[0]
                        html += '<small class="tag ' + type.color + '-bg bold">' + type.InventoryType + ' </small> <br/>'
                    }
                    html += '</td>'
                    html += '<td>'
                    html += '<div class="button-group compact">';
                    html += '<a  class="button icon-pencil confirm" onclick="UpdateRateType(this,\'' + arrType.ID + '\',\'' + arrType.RateType + '\');"></a>';
                    //html += '<a   class="button icon-trash confirm" onclick="Delete(this,\'' + arrType.ID + '\',\'' + arrType.RateType + '\')"></a>';
                    html += '</div>'
                    html += '</td >'
                    html += '</tr>';
                });
                $("#tbl_RateType").append(html)
                $("#tbl_RateType").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_RateType").removeAttribute("style");
            }, function (error) {
                $("#tbl_RateType").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_RateType").removeAttribute("style");
            })
        },1000)
        
    } catch (e) {
        $("#tbl_RateType").dataTable({
            bSort: false, sPaginationType: 'full_numbers',
        });
        document.getElementById("tbl_RateType").removeAttribute("style");
    }
}

function SaveRateType() {
    if ($("#frm_Rates").validationEngine('validate')) {
    try {
        
            var arInvt = $("#sel_Invt").val();
            var InventoryType="";
            $(arInvt).each(function (index, Invt) {
                InventoryType +=Invt +"," ;
            });
            var arrPlan = {
                ID: $("#Hdn_Plan").val(),
                RateType: $("#txt_RateType").val(),
                InventoryType: InventoryType,
                ParentID: HotelAdminID
            };
            post("../GenralHandler.asmx/SavetRateType", { arrRateType: arrPlan }, function (data) {
                Success("Ratetype Saved.");
                $("#modals").remove();
                GetRateTypes()
            }, function (data) {// GETTING Error HERE
                AlertDanger(data.ex)
            });
        } catch (e) {
            AlertDanger(e.message)
        }
    }
}


function UpdateRateType(ID,PlanID, Name) {
    $(ID).confirm({
        message: 'Are you really Update ' + Name + ' Ratetype?',
        onConfirm: function () {
            openModals(content, 'Update Ratet ype', function Save() {
                SaveRateType();
            });
            GetInventory();
            $("#frm_Rates").validationEngine();
            $("#Hdn_Plan").val(PlanID);
            $("#txt_RateType").val(Name);
            setTimeout(function () {
                var type = $.grep(arrTypes, function (H) { return H.ID == PlanID })
              .map(function (H) { return H.InventoryType; })[0];
                $("#sel_Invt").val(type.split(','));
                $('#sel_Invt').change();
            }, 500);
            return false;
        }
    });
}

function Delete(ID, PlanID, Name) {
    $(ID).confirm({
        message: 'Are you really Want to <br/> Delete ' + Name + ' Plan?',
        onConfirm: function () {
            post("../handler/GenralHandler.asmx/DeletePlan", { PlanID: PlanID }, function (data) {
                Success("Plan Deleted Successfully");
                GetInventory();
            }, function (error) {
                AlertDanger(error.ex);
            });
            return false;
        }
    });
}
function AddRateType() {
    openModals(content, 'Add Rate Type', function Save() {
        SaveRateType();
    });
    $("#frm_Rates").validationEngine();
    GetInventory();
}

function GetInventory() {
    $(arrInventory).each(function (index, Inventory) {
        $('#sel_Invt').append($('<option></option>').val(Inventory.type).html(Inventory.InventoryType));
    });
    $('#sel_Invt').change();
}