﻿$(function () {
    LoadAddFee();
});
function AddFee() {
    var AmountPrice = $('#txtAmountPrice').val();
    var AmountPercent = $('#txtAmountInPercent').val();
    if (AmountPrice == "") {
        bValid = false;
        Success("Please Enter Amount Price");
        return;
    }
    if (AmountPercent == "") {
        bValid = false;
        Success("Please Enter Amount Percent");
        return;
    }
    var data = {
        AmountPrice: AmountPrice,
        AmountPercent: AmountPercent
    }
    $.ajax({
        type: "POST",
        url: "../handler/Convenience.asmx/AddFee",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);
            }
            else if (result.retCode == 2) {
                Success("Convenience fee saved successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
            else if (result.retCode == 1) {
                Success("Convenience fee Updated successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
            else {
                Success("Something Went's Wrong.");
            }
        },
        error: function () {
            Success("An error occured while saving details.");
        }
    });
}

function LoadAddFee() {
    $("#tbl_Convenience").dataTable().fnClearTable();
    $("#tbl_Convenience").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../handler/Convenience.asmx/GetAddFee",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            
             if (result.retCode == 1) {
                var Convenience = result.Convenience;

                console.log(Convenience);
                var row = '';
                for (var i = 0; i < Convenience.length; i++) {
                    row += '<tr>'
                    row += '<td class="align-center">' + (i + 1) + '</td>'
                    row += '<td class="align-center">' + Convenience[i].GstTax + '</td>'
                    row += '<td class="align-center">' + Convenience[i].MarkupGstTax + '</td>'

                    //row += '<td class="align-center" style="vertical-align:middle;">';
                    //row += '<div class="button-group compact">';
                    ////row += '<a class="button icon-pencil with-tooltip" title="Edit Details"  onclick="EditBankDetail(\'' + BankDetails[i].sid + '\',\'' + BankDetails[i].BankName + '\',\'' + BankDetails[i].AccountNo + '\',\'' + BankDetails[i].Branch + '\',\'' + BankDetails[i].SwiftCode + '\' ,\'' + BankDetails[i].Country + '\')"></a>'
                    //row += '<a href="#" class="button icon-trash with-tooltip confirm" title="Delete" onclick="DeleteAddFee(\'' + Convenience[i].AmountPrice + '\',\'' + Convenience[i].AmountPercent + '\')"></a>'
                    //row += '</div ></td>';

                    row += '</tr>'
                }
                 $('#tbl_Convenience tbody').html(row);
                 $("#tbl_Convenience").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
            else {
                 $("#tbl_Convenience").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }

        },
        error: function () {
            Success("An error occured while loading details.");
        }
    });

}