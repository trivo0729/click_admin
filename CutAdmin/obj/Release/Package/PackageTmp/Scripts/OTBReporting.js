﻿var arrOtb = new Array();
var hiddenId;
//$(document).ready(function () {
//    GetAllOtbDetails();
//});

function GetAllOtbDetails() {
    $("#tbl_OtbDetails").dataTable().fnClearTable();
    $("#tbl_OtbDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../Handler/OTBHandler.asmx/GetAllOTBDetails",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrOtb = result.tbl_OTB;
                var tRow = '';
                if (arrOtb.length > 0) {
                    var Service = (arrOtb[0].VisaType).split(' ')
                    var OtbStatus = arrOtb[0].OTBStatus;
                    for (i = 0; i < arrOtb.length; i++) {
                        if (arrOtb[i].ParentID == "1") {
                            Service = (arrOtb[i].VisaType).split(' ')
                            OtbStatus = arrOtb[i].OTBStatus;
                            if (OtbStatus == "Processing")
                                OtbStatus = "Recived"
                            else if (OtbStatus == "Processing-Applied")
                                OtbStatus = "Applied"
                            else if (OtbStatus == "Otb Updated")
                                OtbStatus = "Approved"
                            else if (OtbStatus == "Otb Not Required")
                                OtbStatus = "Not Required"
                            else if (OtbStatus == "Rejected")
                                OtbStatus = "Cancelled By Admin"
                            else if (OtbStatus == "Cancelled")
                                OtbStatus = "Cancelled By Agent"

                            tRow += '<tr>                                                                          ';
                            tRow += '<td class="align-center" style="width:15%;">' + arrOtb[i].LastUpdateDate + '</td>                                      ';
                            tRow += '<td class="align-center">' + arrOtb[i].OTBNo + '</td>                                   ';
                            tRow += '<td class="align-center">' + arrOtb[i].AgencyName + '</td>                          ';
                            tRow += '<td class="align-center">' + arrOtb[i].Name + " " + arrOtb[i].LastName + ' x (' + arrOtb[i].num_Pax + ' Pax)</td>                          ';
                            tRow += '<td class="align-center">' + arrOtb[i].VisaNo + '</td>                       ';
                            tRow += '<td class="align-center">' + Service[0] + " " + Service[1] + '</td>                                         ';
                            tRow += '<td class="align-center">' + arrOtb[i].In_AirLine + '</td>                                      ';
                            tRow += '<td class="align-center">' + arrOtb[i].In_DepartureDt + '</td>                                      ';
                            if (OtbStatus == "Applied Directly") {
                                tRow += '<td class="align-center">' + OtbStatus + '</td>';

                            }
                            tRow += '<td class="align-center"><a style="cursor:pointer" title="Update Staus" onclick="UpdateStatus(\'' + arrOtb[i].OTBNo + '\',\'' + arrOtb[i].uid + '\',\'' + arrOtb[i].OTBStatus + '\')">' + OtbStatus + '</a> | <a style="cursor:pointer" ><span class="fa fa-mail-forward" title="Mail" aria-hidden="true" style="cursor:pointer"  onclick="OtbMail(\'' + arrOtb[i].OTBNo + '\',\'' + arrOtb[i].uid + '\')"></span></a></td>';
                            tRow += '<td class="align-center"><a style="cursor:pointer" title="Print Invoice" onclick="GetOTBPrintInvoice(\'' + arrOtb[i].OTBNo + '\',\'' + arrOtb[i].uid + '\')">View</a></td>';
                            tRow += '<td class="align-center">' + arrOtb[i].In_AirLine + '</td>';
                            tRow += '</tr>                                                                         ';





                            //tRow += '<tr style="100%">';
                            //tRow += '<td style="width:15%;">' + arrOtb[i].LastUpdateDate + '</td>';
                            //tRow += '<td style="width:10%">' + arrOtb[i].OTBNo + '</td>';
                            //tRow += '<td style="width:10%">' + arrOtb[i].AgencyName + '</td>';
                            //tRow += '<td style="width:30%">' + arrOtb[i].Name + " " + arrOtb[i].LastName + ' x (' + arrOtb[i].num_Pax + ' Pax)</td>';
                            //tRow += '<td style="width:10%"> ' + arrOtb[i].VisaNo + '</td>';
                            //tRow += '<td style="width:30%">' + Service[0] + " " + Service[1] + '</td>';
                            //tRow += '<td style="width:15%">' + arrOtb[i].In_AirLine + '</td>';
                            //tRow += '<td style="width:30%">' + arrOtb[i].In_DepartureDt + '</td>';
                            //if (OtbStatus == "Applied Directly") {
                            //    tRow += '<td style="width:5%">' + OtbStatus + '</td>';

                            //}
                            //tRow += '<td style="width:5%"><a style="cursor:pointer" title="Update Staus" onclick="UpdateStatus(\'' + arrOtb[i].OTBNo + '\',\'' + arrOtb[i].uid + '\',\'' + arrOtb[i].OTBStatus + '\')">' + OtbStatus + '</a> | <a style="cursor:pointer" ><span class="fa fa-mail-forward" title="Mail" aria-hidden="true" style="cursor:pointer"  onclick="OtbMail(\'' + arrOtb[i].OTBNo + '\',\'' + arrOtb[i].uid + '\')"></span></a></td>';
                            //tRow += '<td style="width:5%"><a style="cursor:pointer" title="Print Invoice" onclick="GetPrintInvoice(\'' + arrOtb[i].OTBNo + '\',\'' + arrOtb[i].uid + '\')">View</a></td>';
                            //tRow += '<td style="width:5%">' + arrOtb[i].In_AirLine + '</td>';
                        }
                    }
                    $("#tbl_OtbDetails tbody").html(tRow);

                }
            }
            else {
                $("#tbl_OtbDetails tbody").append('<tr style="100%"><td style="width:15%;" colspan="11"> No Record Found </td></tr>');
            }
            $("#tbl_OtbDetails").dataTable({
                bSort: false, sPaginationType: 'full_numbers',
            });
            document.getElementById("tbl_OtbDetails").removeAttribute("style")
        },
        error: function () {
            Success('Error Ocurred while Getting Otb Application!')
        }
    });
}

function GetOTBPrintInvoice(OTBNo, uid) {
    //window.open('../OTBInvoice.html?RefNo=' + OTBNo + '&Uid=' + uid, 'PrintInvoice', 'left=5000,top=5000,width=800,height=600');
    window.location.href = "OTBInvoice.html?RefNo='" + OTBNo + '&Uid=' + uid;
}

function GetOTB() {
    var columns = [
           {
               "mData": "Date",
               
           },
  {
      "mData": "ReferNo",

  },
  {
      "mData": "OTBAgency",

  },
  {
      "mData": "Applicant",
      //"mRender": function (data, type, row, meta) {
      //    var tRow = '';
      //    if (row.Name != null) {
      //        tRow += '<td class="align-center">' + row.Name +" " +row.LastName+ '</td> ';
      //        //' x (' + row.num_Pax + ' Pax);
      //    }
      //    else {
      //        return '';
      //    }

      //},
  },

  {
      "mData": "DocNo",

  },
 {
     "mData": "VisaType",
     "mRender": function (data, type, row, meta) {
         var tRow = '';
         if (row.VisaType != null) {
             var Service = (row.VisaType).split(' ');
             tRow += '<td class="align-center">'  +Service[0]+ " " +Service[1] +'</td>';
             // trow += '<a title = "Click for Voucher" class="voucher-btn align-center"  onclick="GetActPrintVoucher(\'' + row.Request_Id + '\',\'' + row.User_Id + '\',\'' + row.Status + '\')" >Voucher</a >';
         }
         else {
             return tRow;
         }
         return tRow;
     },
 },
  {
      "mData": "Airline",

  },
  {
      "mData": "TravelDate",
  },
  {
      "mData": "status",
      "mRender": function (data, type, row, meta) {
          var tRow = '';
          if (row.status != null) {
              OtbStatus = row.status;
              if (OtbStatus == "Processing")
                  OtbStatus = "Received"
              else if (OtbStatus == "Processing-Applied")
                  OtbStatus = "Applied"
              else if (OtbStatus == "Otb Updated")
                  OtbStatus = "Approved"
              else if (OtbStatus == "Otb Not Required")
                  OtbStatus = "Not Required"
              else if (OtbStatus == "Rejected")
                  OtbStatus = "Cancelled By Admin"
              else if (OtbStatus == "Cancelled")
                  OtbStatus = "Cancelled By Agent"
              else if (status == "Applied Directly") {
                  tRow += '<td class="align-center">' + status + '</td>';

              }
              tRow += '<td class="align-center"><a style="cursor:pointer" title="Update Staus" onclick="UpdateStatus(\'' + row.ReferNo + '\',\'' + row.uid + '\',\'' + row.status + '\')">' + OtbStatus + '</a> | <a style="cursor:pointer" ><span class="fa fa-mail-forward" title="Mail" aria-hidden="true" style="cursor:pointer"  onclick="OtbMail(\'' + row.ReferNo + '\',\'' + row.uid + '\')"></span></a></td>';
          }
          else {
              return tRow;
          }
          return tRow;
      },
  },
  {
      "mData": "uid",
      "mRender": function (data, type, row, meta) {
          var tRow = '';
          if (row.uid != null) {
              tRow += '<td class="align-center"><a style="cursor:pointer" title="Print Invoice" onclick="GetOTBPrintInvoice(\'' + row.ReferNo + '\',\'' + row.uid + '\')">View</a></td>';
              // trow += '<a title = "Click for Voucher" class="voucher-btn align-center"  onclick="GetActPrintVoucher(\'' + row.Request_Id + '\',\'' + row.User_Id + '\',\'' + row.Status + '\')" >Voucher</a >';
          }
          else {
              return tRow;
          }
          return tRow;
      },
  },
   {
       "mData": "Airline",

   },

    ];
    GetData("tbl_OtbDetails", [], '../handler/OTBHandler.asmx/GetAllOTBList', columns);
    $("#tbl_OtbDetails").css("width", "100%");

}

function SearchOTB() {

    $("#tbl_OtbDetails").dataTable().fnClearTable();
    $("#tbl_OtbDetails").dataTable().fnDestroy();
    var Date = $("#txt_Date").val();
    // var StartDate = $("#datepicker3").val();
    //var EndDate = $("#datepicker4").val();
    var ReferNo = $("#txt_ReferNo").val();
    var OTBAgency = $("#txt_otbAgency").val();
    var VisaType = $("#selvisa").val();
    if (VisaType == "-") {
         VisaType = "";
    }

    var TravelDate = $("#txt_Travel").val();
    var data =
    {
        Date: Date,
        ReferNo: ReferNo,
        OTBAgency: OTBAgency,
        VisaType: VisaType,
        TravelDate: TravelDate
    }
    var columns = [
            {
                "mData": "Date",

            },
   {
       "mData": "ReferNo",

   },
   {
       "mData": "OTBAgency",

   },
   {
       "mData": "Applicant",

   },

   {
       "mData": "DocNo",

   },
  {
      "mData": "VisaType",
      //"mRender": function (data, type, row, meta) {
      //    var tRow = '';
      //    if (row.VisaType != null) {
      //        tRow += '<td class="align-center">'  +(row.VisaType).split(' ')+ '</td>';
      //        // trow += '<a title = "Click for Voucher" class="voucher-btn align-center"  onclick="GetActPrintVoucher(\'' + row.Request_Id + '\',\'' + row.User_Id + '\',\'' + row.Status + '\')" >Voucher</a >';
      //    }
      //    else {
      //        return tRow;
      //    }
      //    return tRow;
      //},
  },
   {
       "mData": "Airline",

   },
   {
       "mData": "TravelDate",
   },
   {
       "mData": "status",
       "mRender": function (data, type, row, meta) {
           var tRow = '';
           if (row.status != null) {
               OtbStatus = row.status;
               if (OtbStatus == "Processing")
                   OtbStatus = "Received"
               else if (OtbStatus == "Processing-Applied")
                   OtbStatus = "Applied"
               else if (OtbStatus == "Otb Updated")
                   OtbStatus = "Approved"
               else if (OtbStatus == "Otb Not Required")
                   OtbStatus = "Not Required"
               else if (OtbStatus == "Rejected")
                   OtbStatus = "Cancelled By Admin"
               else if (OtbStatus == "Cancelled")
                   OtbStatus = "Cancelled By Agent"
              else if (status == "Applied Directly") {
                   tRow += '<td class="align-center">' + status + '</td>';

               }
               tRow += '<td class="align-center"><a style="cursor:pointer" title="Update Staus" onclick="UpdateStatus(\'' + row.ReferNo + '\',\'' + row.uid + '\',\'' + row.status + '\')">' + OtbStatus + '</a> | <a style="cursor:pointer" ><span class="fa fa-mail-forward" title="Mail" aria-hidden="true" style="cursor:pointer"  onclick="OtbMail(\'' + row.ReferNo + '\',\'' + row.uid + '\')"></span></a></td>';
           }
           else {
               return tRow;
           }
           return tRow;
       },
   },
   {
       "mData": "uid",
       "mRender": function (data, type, row, meta) {
           var tRow = '';
           if (row.uid != null) {
               tRow += '<td class="align-center"><a style="cursor:pointer" title="Print Invoice" onclick="GetOTBPrintInvoice(\'' + row.ReferNo + '\',\'' + row.uid + '\')">View</a></td>';
               // trow += '<a title = "Click for Voucher" class="voucher-btn align-center"  onclick="GetActPrintVoucher(\'' + row.Request_Id + '\',\'' + row.User_Id + '\',\'' + row.Status + '\')" >Voucher</a >';
           }
           else {
               return tRow;
           }
           return tRow;
       },
   },
    {
        "mData": "Airline",

    },

    ];
    GetData("tbl_OtbDetails", data, '../handler/OTBHandler.asmx/SearchByOTBList', columns);
    $("#tbl_OtbDetails").css("width", "100%");
}
function Search() {
    //$("#tbl_OtbDetails tbody tr").remove();
    $("#tbl_OtbDetails").dataTable().fnClearTable();
    $("#tbl_OtbDetails").dataTable().fnDestroy();
    var dteTo = $("#dteFrom").val()
    var Date = $("#txt_Date").val();
    // var StartDate = $("#datepicker3").val();
    //var EndDate = $("#datepicker4").val();
    var ReferNo = $("#txt_ReferNo").val();
    var OTBAgency = $("#txt_otbAgency").val();
    var VisaType = $("#selvisa option:selected").val();

    var dteTravel = $("#txt_Travel").val();

    //var dteFrom = $("#dteTo").val()
    //var dteTravel = $("#dteTravel").val()
    //var FirstName = $("#txtFirst").val()
    //var LastName = $("#txtLast").val()
    //var VisaType = $("#selService option:selected").text();
    //var AirLine = $("#Sel_AirLineIn option:selected").text();
    //var PnrNo = $("#txtPnr").val();
    //var uid = $("#hdn_uid").val();
    if ($("#selOTBStatus option:selected").text() == "All" || $("#selvisa option:selected").text() == "All") {
        GetAllOtbDetails();
    }
    else {
        $.ajax({
            type: "POST",
            url: "../Handler/OTBHandler.asmx/SearchOTB",
            data: '{"dteTo":"' + dteTo + '","dteFrom":"' + dteFrom + '","dteTravel":"' + dteTravel + '","FirstName":"' + FirstName + '","LastName":"' + LastName + '","VisaType":"' + VisaType + '","AirLine":"' + AirLine + '","PnrNo":"' + PnrNo + '","uid":"' + uid + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    arrOtb = result.tbl_OTB;
                    var tRow = '';

                    if (arrOtb.length > 0) {
                        for (i = 0; i < arrOtb.length; i++) {
                            var Service = (arrOtb[i].VisaType).split(' ')
                            var OtbStatus = arrOtb[i].OTBStatus;
                            if (arrOtb[i].ParentID == "1") {
                                if (OtbStatus == "Processing")
                                    OtbStatus = "Recived"
                                else if (OtbStatus == "Processing-Applied")
                                    OtbStatus = "Applied"
                                else if (OtbStatus == "Otb Updated")
                                    OtbStatus = "Approved"
                                else if (OtbStatus == "Otb Not Required")
                                    OtbStatus = "Not Required"
                                else if (OtbStatus == "Rejected")
                                    OtbStatus = "Cancelled By Admin"
                                else if (OtbStatus == "Cancelled")
                                    OtbStatus = "Cancelled By Agent"
                                else if (OtbStatus == "Applied Directly")
                                    OtbStatus = "Diverted to EK"
                                tRow += '<tr style="100%">';
                                tRow += '<td style="width:15%;">' + arrOtb[i].LastUpdateDate + '</td>';
                                tRow += '<td style="width:10%">' + arrOtb[i].OTBNo + '</td>';
                                tRow += '<td style="width:10%">' + arrOtb[i].AgencyName + '</td>';
                                tRow += '<td style="width:15%">' + arrOtb[i].Name + " " + arrOtb[i].LastName + '</td>';
                                tRow += '<td style="width:10%"> ' + arrOtb[i].VisaNo + '</td>';
                                tRow += '<td style="width:15%">' + Service[0] + " " + Service[1] + '</td>';
                                tRow += '<td style="width:15%">' + arrOtb[i].In_AirLine + '</td>';
                                tRow += '<td style="width:15%">' + arrOtb[i].In_DepartureDt + '</td>';
                                tRow += '<td style="width:5%">' + OtbStatus + '</td>';
                                // tRow += '<td style="width:5%"><a style="cursor:pointer" title="Print Invoice" onclick="GetOTBPrintInvoice(\'' + arrOtb[i].OTBNo + '\',\'' + arrOtb[i].uid + '\')">View</a></td>';
                                tRow += '<td style="width:15%">' + arrOtb[i].In_AirLine + '</td>';
                            }


                        }
                        $("#tbl_OtbDetails tbody").append(tRow);

                        $("#tbl_OtbDetails").dataTable({
                            "bSort": false,

                        });

                        document.getElementById("tbl_OtbDetails").removeAttribute("style")
                    }
                }
                else {
                    $("#tbl_OtbDetails tbody").append('<tr style="100%"><td style="width:15%;" colspan="5"> No Record Found </td></tr>');
                }
            },
            error: function () {
                Success('Error Ocurred while Getting Otb Application!')
            }
        });


    }

}


//function GetAgentName(uid) {
//    $.ajax({
//        type: "POST",
//        contentType: "application/json; charset=utf-8",
//        url: "../Handler/OTBHandler.asmx/GetAgentName",
//        data: "{'uid':'" + uid + "'}",
//        dataType: "json",
//        success: function (data) {
//            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
//            if (result.retCode == 1) {
//                var AgentDetails = result.tbl_OTB;
//                AgencyName = AgentDetails[0].AgencyName;

//            }
//            //response(result);
//        },
//        error: function (result) {
//            alert("Something went wrong!");
//        }
//    });
//    //return AgencyName;
//}