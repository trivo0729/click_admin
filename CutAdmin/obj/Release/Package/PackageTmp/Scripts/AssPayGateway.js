﻿var GatewayName = new Array;
function AssignPayGateway(Id) {
    $.ajax({
        type: "POST",
        url: "../Handler/GatewayHandler.asmx/GetPaymtGateway",
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                GatewayName = result.Name;
                GenerateGateway(Id);
                GetMapped(Id);
            }
        },
        error: function () {
            Success("An error occured while getting detail");
        }
    });
}

function GetMapped(Id) {
    $.ajax({
        type: "POST",
        url: "../Handler/GatewayHandler.asmx/GetMappedPaymentGateway",
        data: '{"Id":"' + Id + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Name = result.Name;
                if (Name.length > 0) {
                    for (var i = 0; i < Name.length; i++) {
                        $('#chk' + Name[i].GatewayId).click();
                    }
                }

            }
        },
        error: function () {
            Success("An error occured while getting detail");
        }
    });
}

function GenerateGateway(Id) {
    $("#tblGetway").empty();
    if (GatewayName.length > 0) {
        var trForms = '<tbody>';
        for (i = 0; i < GatewayName.length; i = i + 4) {
            if (i < GatewayName.length) {
                trForms += '<tr>';
                trForms += '<td><input id="chk' + GatewayName[i].Id + '" class="checkbox mid-margin-left chkGTW" type="checkbox" value="' + GatewayName[i].Id + '"/> ' + GatewayName[i].PaymentGateway + '</td>';
                if ((i + 1) < GatewayName.length)
                    trForms += '<td><input id="chk' + GatewayName[i + 1].Id + '" type="checkbox" class="checkbox mid-margin-left chkGTW" value="' + GatewayName[i + 1].Id + '"/> ' + GatewayName[i + 1].PaymentGateway + '</td>';
                if ((i + 2) < GatewayName.length)
                    trForms += '<td><input id="chk' + GatewayName[i + 2].Id + '" type="checkbox" class="checkbox mid-margin-left chkGTW" value="' + GatewayName[i + 2].Id + '"/> ' + GatewayName[i + 2].PaymentGateway + '</td>';
                if ((i + 3) < GatewayName.length)
                    trForms += '<td><input id="chk' + GatewayName[i + 3].Id + '" type="checkbox" class="checkbox mid-margin-left chkGTW" value="' + GatewayName[i + 3].Id + '"/> ' + GatewayName[i + 3].PaymentGateway + '</td>';
                trForms += '</tr>';
            }
        }
        trForms += '</tbody>';
        $.modal({
            content: '<table class="table table-striped table-bordered" id="tblGetway" cellspacing="0" cellpadding="0" border="0">' + trForms + '' +
                '</table><br>' +
                '<button style="float:right" type="submit" class="button anthracite-gradient" onclick="SaveGateway(\'' + Id + '\')">Save</button>',

            title: 'Assign Payment Gateway',
            width: 600,
            scrolling: true,
            actions: {
                'Close': {
                    color: 'red',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttons: {
                'Close': {
                    classes: 'anthracite-gradient displayNone',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttonsLowPadding: true
        });
    }
}
var ArrGatewayName = new Array();
function SaveGateway(Id) {
    var GatewayName = $(".chkGTW");
    for (var i = 0; i < GatewayName.length; i++) {
        if (GatewayName[i].classList.length == 5) {
            GatewayId = GatewayName[i].childNodes[1].value;
            ArrGatewayName.push({
                GatewayId: GatewayId,
                ParentId: Id,
            });
        }
    }

    if (ArrGatewayName == 0) {
        Success('Please Select Payment Gateway.');
        return false;
    }
    var data = {
        ArrGateway: ArrGatewayName,
    }
    $.ajax({
        type: "POST",
        url: "../Handler/GatewayHandler.asmx/MapPaymentGateway",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);

            }
            else if (result.retCode == 1) {
                Success("saved successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
        },
        error: function () {
            Success("An error occured while saving");
        }
    });
}