﻿$(document).ready(function () {
    GetPackageDetails();
});

function GetCategoryName(categoryID) {
    if (categoryID == 1) {
        return "Standard";
    }
    else if (categoryID == 2) {
        return "Deluxe";
    }
    else if (categoryID == 3) {
        return "Premium";
    }
    else if (categoryID == 4) {
        return "Luxury";
    }
}

function GetPackageType(id) {
    if (id == 1) {
        return "Holidays";
    }
    else if (id == 2) {
        return "Umrah";
    }
    else if (id == 3) {
        return "Hajj";
    }
    else if (id == 4) {
        return "Honeymoon";
    }
    else if (id == 5) {
        return "Summer";
    }
    else if (id == 6) {
        return "Adventure";
    }
    else if (id == 7) {
        return "Deluxe";
    }
    else if (id == 8) {
        return "Business";
    }
    else if (id == 9) {
        return "Premium";
    }
    else if (id == 10) {
        return "Wildlife";
    }
    else if (id == 11) {
        return "Weekend";

    }
    else if (id == 12) {
        return "New Year";
    }
    else {
        return "No Theme";
    }
}
function GetPackageDetails() {
    debugger;
    $("#tbl_PackageDetails").dataTable().fnClearTable();
    $("#tbl_PackageDetails").dataTable().fnDestroy();
    $.ajax({
        url: "../Handler/ViewPackageHandler.asmx/GetPackageDetails",
        type: "Post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.Session == 0) {
                window.location.href = "index.htm";
            }
            if (result.retCode == 0) {
                alert("No packages found");
            }
            else if (result.retCode == 1) {
                List_Packages = result.List_Packages;
                var tRow = '';
                for (var i = 0; i < List_Packages.length; i++) {
                    var sCategory = List_Packages[i].sPackageCategory.split(',');
                    var sCategoryName = "";
                    //if (sCategory.length == 1) {
                    for (var k = 0; k < sCategory.length; k++) {
                        if (sCategory[k] != "") {
                            sCategoryName = sCategoryName + "," + GetCategoryName(sCategory[k]);
                        }
                    }
                    //}
                    //else {
                    //    for (var k = 0; k < sCategory.length - 1; k++) {
                    //        sCategoryName = sCategoryName + "," + GetCategoryName(sCategory[k]);
                    //    }
                    //}


                    var dfrom = (List_Packages[i].dValidityFrom).split(' ');

                    var dTO = (List_Packages[i].dValidityTo).split(' ');

                    var city = (List_Packages[i].sPackageDestination).split('|');
                    tRow += '<tr>';
                    tRow += '<td class="align-center">' + List_Packages[i].sPackageName + '</td>';
                    tRow += '<td class="align-center">' + city[0] + '</td>';
                    tRow += '<td class="align-center">' + List_Packages[i].nDuration + ' Days</td>';
                    tRow += '<td class="align-center">' + sCategoryName.replace(/^,|,$/g, ''); + '</td>';
                    tRow += '<td class="align-center">' + dfrom[0] + '</td>';
                    tRow += '<td class="align-center">' + dTO[0] + '</td>';
                    if (List_Packages[i].Status == true)
                        tRow += '<td class="align-center" id="' + List_Packages[i].nID + '"><input type="checkbox" id="chk' + List_Packages[i].nID + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"  checked  onchange="PackageStatus(' + List_Packages[i].nID + ',\'Deactive\')"></td>';
                    else
                        tRow += '<td class="align-center" id="' + List_Packages[i].nID + '"><input type="checkbox" id="chk' + List_Packages[i].nID + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"   onchange="PackageStatus(' + List_Packages[i].nID + ',\'Active\')"></td>';
                 
                  
                    //tRow += '<td class="align-center"><a class="button" title="Update"><span class="icon-pencil " style="cursor:pointer" title="Click here to View package detail" onclick="PackageDetails(' + List_Packages[i].nID + ')"></span></a></td>';
                    //tRow += '<td data-title="Click here to delete the package" style="text-align:center;"><a style="cursor:pointer" href="#"><span class="icon-trash" title="Click here to delete the package" style="cursor:pointer" onclick="DeletePackage(' + List_Packages[i].nID + ')"></span></a></td>';
                    tRow += '<td class="align-center" style="vertical-align:middle;">';
                    tRow += '<div class="button-group compact">';
                    tRow += '<a class="button icon-pencil with-tooltip" title="Edit Details" onclick="PackageDetails(' + List_Packages[i].nID + ')"></a>'
                    tRow += '<a href="#" class="button icon-trash with-tooltip confirm" title="Delete" onclick="DeletePackage(' + List_Packages[i].nID + ')"></a>'
                    tRow += '</div ></td>';
                    tRow += '</tr>';
                }

                $("#tbl_PackageDetails tbody").html(tRow);
                $("#tbl_PackageDetails").dataTable({
                    bSort: false,
                    sPaginationType: 'full_numbers',
                    sSearch: false
                });
                document.getElementById("tbl_PackageDetails").removeAttribute("style")
            }
        },
        error: function () {
            alert('Error in getting package!');
        }
    });
}
////// PackageDetails.aspx linked required here //////
function PackageDetails(ID) {

    window.location.href = "AddPackage.aspx?ID=" + ID;

}

function DeletePackage(nID) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure want to delete the package<br/> <span class=\"orange\">' + nID + '</span>?</span>?</p>',
        function () {

            $.ajax({
                url: "../Handler/PackageDetailHandler.asmx/DeletePackage",
                type: "post",
                data: '{"nID":"' + nID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        GetPackageDetails();
                        Success("Package deleted successfully");
                    } else if (result.retCode == 0) {
                        Success("Error in deleting package");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}
function PackageStatus(nID, Status) {
    var StatusNew = "";
    if (Status == "Active")
        StatusNew = "Activate"
    else
        StatusNew = "Deactivate"
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure want to ' + StatusNew + ' the package</p>',
        function () {
            $.ajax({
                url: "../Handler/PackageDetailHandler.asmx/PackageStatus",
                type: "post",
                data: '{"nID":"' + nID + '","Status":"' + Status + '"}',
                contentType: "application/json; charset=utf-8 ",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Status Updated successfully");
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);

                        return false;
                    }
                    else {
                        alert("Error in Update package");
                        return false;
                    }
                },
                error: function () {

                }
            });
        },
        function () {
            $('#modals').remove();
        }
    );
}