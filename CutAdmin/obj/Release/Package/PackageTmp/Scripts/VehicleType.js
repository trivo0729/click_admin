﻿$(function () {
    GetVehicleType();

    $("#txt_ValidUpto").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
})
var HiddenId = 0;
function GetVehicleType() {
    $("#tbl_VehicleType").dataTable().fnClearTable();
    $("#tbl_VehicleType").dataTable().fnDestroy();
    //$("#tbl_VehicleType tbody tr").remove()
    
    $.ajax({
        url: "../Handler/VehicleMasterHandller.asmx/GetVehicleType",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var arrVehicleType = result.tbl_VehicleType;
                var tr = '';
                for (var i = 0; i < arrVehicleType.length; i++) {
                   
                    tr += '<tr> '
                    tr += '<td class="align-center" >' + (i + 1) + '</td> '                                                                                                                         
                    tr += '<td class="align-center">' + arrVehicleType[i].VehicleBrand + '</td>'
                    //tr += '<span class="button-group">'
                    //tr += '<a class="button" onclick="LoadVehicleType(\'' + arrVehicleType[i].ID + '\',\'' + arrVehicleType[i].VehicleBrand + '\')"><span class="icon-pencil"></span></a>'
                    //tr += ' <a href="#" class="button" title="trash" onclick="DeleteVehicleType(\'' + arrVehicleType[i].ID + '\',\'' + arrVehicleType[i].VehicleBrand + '\')"><span class="icon-trash"></span></a></span></td>'
                    tr += '<td class="align-center" style="vertical-align:middle;">';
                    tr += '<div class="button-group compact">';
                    tr += '<a class="button icon-pencil with-tooltip" title="Edit Details"  onclick="LoadVehicleType(\'' + arrVehicleType[i].ID + '\',\'' + arrVehicleType[i].VehicleBrand + '\')"></a>'
                    tr += '<a href="#" class="button icon-trash with-tooltip confirm" title="Delete" onclick="DeleteVehicleType(\'' + arrVehicleType[i].ID + '\',\'' + arrVehicleType[i].VehicleBrand + '\')"></a>'
                    tr += '</div ></td>';

                    tr += '</tr>'

                }
                $("#tbl_VehicleType tbody").append(tr);
                //$("#tbl_VehicleType tbody").html(tr);
                $("#tbl_VehicleType").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_VehicleType").removeAttribute("style")
            }
        }
    })
}

function DeleteVehicleType(ID, VehicleBrand) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete<br/> <span class=\"orange\">' + VehicleBrand + '</span>?</span>?</p>',
        function () {
            $.ajax({
                url: "../Handler/VehicleMasterHandller.asmx/DeleteVehicleType",
                type: "post",
                data: '{"ID":"' + ID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Vehicle Type has been deleted successfully!");
                        window.location.reload();
                    } else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}

//********************Add Edit StaticPackage*******************************
function AddVehicleType() {
    var VehicleTypename = $("#txt_Name").val();
  
    var bValid = true;
    if (VehicleTypename == "") {
        Success("Please Insert Vehicle Type Name")
        bValid = false;
        $("#txt_Name").focus()

    }
   
    if (bValid == true) {
        var DATA = {

            VehicleTypename: VehicleTypename
           
        }
        var jason = JSON.stringify(DATA);
        $.ajax({
            url: "../Handler/VehicleMasterHandller.asmx/AddVehicleType",
            type: "post",
            data: jason,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    $("#VehicleTypeModal").modal("hide")
                    Success("Vehicle Type Added Successfully");
                    window.location.reload();
                }

                Clear()
                GetVehicleType()
            }
        })
    }

}
function LoadVehicleType(ID, VehicleBrand) {


    $.modal({
        content: '<div class="modal-body" id="VehicleTypeModal2">' +
				'<div class="columns">' +
'<div class="Twelve-columns twelve-columns-mobile"><label>Vehicle Type Name:</label> <div class="input full-width">' +
'<input name="prompt-value" id="txt_Name2" placeholder="Vehicle Type Name" value="' + VehicleBrand + '" class="input-unstyled full-width"  type="text">' +
'</div></div>' +
'</div>' +
'<p class="text-alignright"><input type="button" value="Update" onclick="UpdateVehicleTypes(\'' + ID + '\');" title="Update Details" class="button anthracite-gradient"/></p>' +
'</div>',
        title: 'Update Vehicle  Type',
        width: 500,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}


function UpdateVehicleTypes(ID) {

    var ID = ID;
    var VehicleTypename = $("#txt_Name2").val();

    var bValid = true;
    if (VehicleTypename == "") {
        Success("Please Insert Vehicle Type Name")
        bValid = false;
        $("#txt_Name2").focus()

    }

    if (bValid == true) {
        var DATA = {

            VehicleTypename: VehicleTypename,
            ID: ID

        }
        var jason = JSON.stringify(DATA);
        $.ajax({
            url: "../Handler/VehicleMasterHandller.asmx/UpdateVehicleType",
            type: "post",
            data: jason,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    $("#VehicleTypeModal2").modal("hide")
                    Success("Vehicle Type Updated Successfully");
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }

                Clear()
                GetVehicleType()
            }
        })
    }

}
function Clear() {
    $("#txt_Name").val("");
  
    HiddenId = 0;
}
