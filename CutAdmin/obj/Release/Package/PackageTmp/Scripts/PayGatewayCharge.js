﻿$(function () {
    LoadGatewayDetails();
});

var PaymentGatewayDetails = new Array();
var PaymentGatewayCharge = new Array();
function LoadGatewayDetails() {
    $.ajax({
        type: "POST",
        url: "../Handler/GatewayHandler.asmx/GetPaymentGatewayDetails",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);
            }
            else if (result.retCode == 1) {
                PaymentGatewayDetails = result.Arr;
                GenerateHtmlTab();
            }
            else if (result.retCode == 0) {

            }
        },
        error: function () {
            Success("An error occured while loading details.");
        }
    });
}

function GenerateHtmlTab() {
    $("#Pmtgtw").empty();
    var html = "";
    html += '<div class="side-tabs same-height margin-bottom">'
    html += '<ul class="tabs">'
    for (var i = 0; i < PaymentGatewayDetails.length; i++) {
        if (i == 0)
            html += '<li class="active"><a href="#' + PaymentGatewayDetails[i].GatewayId + '">' + PaymentGatewayDetails[i].PaymentGateway + '</a></li>'
        else
            html += '<li><a href="#' + PaymentGatewayDetails[i].GatewayId + '">' + PaymentGatewayDetails[i].PaymentGateway + '</a></li>'
    }
    html += '</ul>'
    html += '<div class="tabs-content">'
    for (var i = 0; i < PaymentGatewayDetails.length; i++) {
        html += '<div id="' + PaymentGatewayDetails[i].GatewayId + '" class="with-padding">'
        html += '<div class="columns">'
        html += '<div class="four-columns twelve-columns-mobile six-columns-tablet">'
        html += '<span>Amount Percent</span>'
        html += '<div class="input full-width">'
        html += '<input name="prompt-value" id="txt_AmountPercent' + i + '" value="' + PaymentGatewayDetails[i].AmountPer + '" class="input-unstyled full-width" placeholder="" type="text">'
        html += '</div>'
        html += '</div>'
        html += '<div class="four-columns twelve-columns-mobile six-columns-tablet">'
        html += '<span>Amount </span>'
        html += '<div class="input full-width">'
        html += '<input name="prompt-value" id="txt_Amount' + i + '" value="' + PaymentGatewayDetails[i].Amount + '" class="input-unstyled full-width" placeholder="" type="text">'
        html += '</div>'
        html += '</div>'
        html += ' <div class="four-columns twelve-columns-mobile six-columns-tablet">'
        html += '</br>'
        html += '<input type="button" class="button anthracite-gradient" onclick="Submit(\'' + i + '\',\'' + PaymentGatewayDetails[i].GatewayId + '\',\'' + PaymentGatewayDetails[i].ParentId + '\')" value="Submit">'
        html += '</div>'
        html += '</div>'
        html += '</div>'
    }
    html += '</div>'
    html += '</div>'
    $("#Pmtgtw").append(html);
}

function Submit(i, GatewayId, ParentId) {
    var GatewayId = GatewayId;
    var AmountPer = $("#txt_AmountPercent" + i).val();
    var Amount = $("#txt_Amount" + i).val();
    var ParentId = ParentId;
    if (AmountPer == "") {
        Success("Please Enter Amount Percent")
        return false
    }
    if (Amount == "") {
        Success("Please Enter Amount")
        return false
    }
    var data = {
        GatewayId: GatewayId,
        AmountPer: AmountPer,
        Amount: Amount,
        ParentId: ParentId,
    }
    $.ajax({
        type: "POST",
        url: "../Handler/GatewayHandler.asmx/SavePaymentGatewayDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);

            }
            else if (result.retCode == 1) {
                Success("saved successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
        },
        error: function () {
            Success("An error occured while saving details.");
        }
    });
}
