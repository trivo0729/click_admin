﻿$(document).ready(function () {
    GetCountry();
    $('#selCountry').change(function () {
        var sndcountry = $('#selCountry').val();
        GetCity(sndcountry);
    });
});
var arrCountry = new Array();
var arrCity = new Array();
var arrCityCode = new Array();
function GetCountry() {
    post("GenralHandler.asmx/GetCountry", {}, function(data) {
        $("#selCountry").empty();
        $(data.Country).each(function (index, item) { // GETTING Sucees HERE
            $('#selCountry').append($('<option></option>').val(item.Country).html(item.Countryname));
        });
    }, function(responsedata) {
        AlertDanger(responsedata.ex)
    })
}

function GetCity(reccountry) {
    post(
        "GenralHandler.asmx/GetCity",
        { country: reccountry },
        function Success(data) {
            $("#selCity").empty();
            $(data.City).each(function (index, item) { // GETTING Sucees HERE
                $('#selCity').append($('<option></option>').val(item.Code).html(item.Description));
            });
        }, function Error(ex) { AlertDanger(ex) }
    );
}

function GetCityCode() {
    if ($('#selCountry').val() == '-') {
        Success('Please select Country.');
    }
    else if ($('#selCountry').val() != '-' && $('#selCity').val() == '-') {
        Success('Please select City.');
    }
    else {
        $.ajax({
            type: "POST",
            url: "GenralHandler.asmx/GetCityCode",
            data: '{"Description":"' + $('#selCity option:selected').text() + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    arrCityCode = result.CityCode;
                    if (arrCityCode.length > 0) {
                        //....................//
                        $("#tbl_GetCityCode tbody").remove();
                        var trRequest = '<tbody><tr style="border-top:hidden">';
                        trRequest += '<td><span class="text-left">Country Name</span></td>';
                        trRequest += '<td><span class="text-left">Code</span></td>';
                        trRequest += '<td><span class="text-left">City Name</span></td>';
                        trRequest += '<td><span class="text-left">City Code</span></td>';
                        trRequest += '</tr>';
                        for (i = 0; i < arrCityCode.length; i++) {
                            trRequest += '<tr><td align="left">';
                            trRequest += '<span class="text-left">' + arrCityCode[i].Countryname + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrCityCode[i].Country + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrCityCode[i].Description + '</span>';
                            trRequest += '</td><td align="left">';
                            trRequest += '<span class="text-left">' + arrCityCode[i].Code + '</span>';
                            trRequest += '</td></tr>';
                        }
                        trRequest += '</tbody>';
                        $("#tbl_GetCityCode").append(trRequest);
                        //.......................//
                    }
                }
            },
            error: function () {
            }
        });
    }
}