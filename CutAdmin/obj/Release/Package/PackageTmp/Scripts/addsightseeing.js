﻿//Global Variables
var AllowChild = false;
var AllowHeight = false;
var AllowCapacity = false;
var AllowFullYear = true;
var AllowInfant = false;
var sid = "";
var id = 0;
$(function () {
    loadtourtype();

    if (location.href.indexOf('?') != -1) {
        id = GetQueryStringParams('id');
        if (id != undefined) {
            setTimeout(function () { GetSightseeing(id) }, 3000);
        }
    }

    $('.wizard #wiz_operation').on('wizardleave', function () {
        Review();
    })
});

function SetValidation() {
    $('.k-widget').removeClass('validate[required]')
}

function loadtourtype() {

    $.ajax({
        type: "POST",
        url: "../Handler/ActivityHandller.asmx/GetMode",
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arTypeList = result.dtresult;
                if (arTypeList.length > 0) {
                    var html = "";
                    for (var i = 0; i < arTypeList.length; i++) {
                        html += '<option  value="' + arTypeList[i].TourType + '">' + arTypeList[i].TourType + '</option>'
                    }
                    $("#sel_tourtype").append(html);

                    $('#kUI_cmb').kendoComboBox({ /*kendoComboBox*/
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: [
                          { text: "Cotton", value: "1" },
                          { text: "Polyester", value: "2" },
                          { text: "Cotton/Polyester", value: "3" },
                          { text: "Rib Knit", value: "4" }
                        ],
                        filter: "contains",
                        suggest: true,
                        index: 3
                    });
                    $("#sel_tourtype").kendoMultiSelect();/*kendoMultiSelect*/
                    //$('#sel_tourtype').removeClass('validate[required]')
                }
            }
        },
    });
}
function SetChildPolicy(elem) {

    try {
        if ($(elem).is(":checked")) {
            $("#div_childpolicy").css("display", "")
            $("#div_childdetails").css("display", "")
            AllowChild = true;
            ChangeAge();
        }
        else {
            $("#div_childpolicy").css("display", "none")
            $("#div_childdetails").css("display", "none")
            AllowChild = false;
        }
    } catch (e) {

    }
}
function SetheightPolicy(elem) {

    try {
        if ($(elem).is(":checked")) {
            $("#div_heightpolicy").css("display", "")
            AllowHeight = true;
        }
        else {
            $("#div_heightpolicy").css("display", "none")
            AllowHeight = false;
        }
    } catch (e) {
    }
}
function SetDuration(elem) {
    try {
        if ($(elem).is(":checked")) {
            $("#div_duration").css("display", "none")
            $("#div_operationdates").empty();
            AllowFullYear = true;
        }
        else {
            $("#div_duration").css("display", "")
            AllowFullYear = false;
            SetDurationDates()
        }
    } catch (e) {

    }
}
function SetCapacity(elem) {
    try {
        if ($(elem).is(":checked")) {
            $("#div_capacity").css("display", "")
            AllowCapacity = true;
        }
        else {
            $("#div_capacity").css("display", "none")
            AllowCapacity = false;
        }
    } catch (e) {

    }
}

var datepickersOpt = {
    dateFormat: 'dd-mm-yy'
}
function SetDurationDates() {
    var html = "";
    try {
        var elem = $(".dte_durationrange");
        html += '<div class="columns dte_durationrange">'
        html += '<div class="three-columns ten-columns-mobile five-columns-tablet mid-margin-bottom">'
        html += '<p class="white font12 strong no-margin-bottom">From</p>'
        html += '<span class="input margin-right">'
        html += '<span class="icon-calendar"></span>'
        html += '<input type="text" id="dtp_from' + elem.length + '" class="input-unstyled datepicker dtp_from validate[required]" size="14" placeholder="Starting from">'
        html += '</span>'
        html += '</div>'
        html += '<div class="three-columns twelve-columns-mobile seven-columns-tablet mid-margin-bottom">'
        html += '<p class="white font12 strong no-margin-bottom">To</p>'
        html += '<span class="input margin-right">'
        html += '<span class="icon-calendar"></span>'
        html += '<input type="text" id="dtp_to' + elem.length + '" class="input-unstyled datepicker dtp_to validate[required]" size="14" placeholder="Ending on">'
        html += '</span>'
        html += '</div>'
        html += '<div class="two-columns twelve-columns-mobile seven-columns-tablet mid-margin-bottom no-margin-left">'
        html += ' <p class="white font12 strong no-margin-bottom">Opening Time</p>'
        html += ' <input id="StartTime' + elem.length + '" style="width: 70px" class="StartingTime validate[required]"/>'
        html += '</div>'
        html += '<div class="three-columns twelve-columns-mobile seven-columns-tablet mid-margin-bottom">'
        html += '   <p class="white font12 strong no-margin-left no-margin-bottom">Closing Time</p>'
        html += ' <input id="EndTime' + elem.length + '" style="width: 70px" class="EndingTime validate[required]"/>'
        if (elem.length == 0) {
            html += '     <span class="mid-margin-left icon-size2 icon-plus-round icon-white" onclick="SetDurationDates()" id=""></span>'
        }
        else
            html += '<span class="mid-margin-left icon-size2 icon-minus-round icon-red remCF" ></span>';
        html += ' </div>'
        html += ' </div>'
        $("#div_operationdates").append(html);

        /*Time Picker*/
        $("#StartTime" + elem.length + "").kendoTimePicker({
            interval: 10,
            format: "HH:mm"
        }); /*Time Picker*/
        $("#EndTime" + elem.length + "").kendoTimePicker({
            interval: 10,
            format: "HH:mm"
        }); /*Time Picker*/
        $('.k-widget').removeClass('validate[required]')
        $('.k-widget').removeClass('StartingTime')
        $('.k-widget').removeClass('EndingTime')

        /*Remove Button*/
        $(".remCF").on('click', function () {
            $(this).parent().parent().remove();
        });

        /*Date Picker*/
        var elem_FromDate = $("#div_operationdates").find(".dtp_from");
        var elem_ToDate = $("#div_operationdates").find(".dtp_to");
        for (var i = 0; i < elem_FromDate.length; i++) {
            var previousFrom = 0;
            var previousTo = 0;
            if (i != 0)
                previousTo = moment($(elem_ToDate[i - 1]).val(), "DD-MM-YYYY");
            var minDate = 0;
            if (previousTo != 0)
                minDate = previousTo._i;
            $(elem_FromDate[i]).datepicker($.extend({
                minDate: minDate,
                onSelect: function () {
                    var minDate = $(this).datepicker('getDate');
                    minDate.setDate(minDate.getDate() + 1); //add One days
                    $(elem_ToDate[i - 1]).datepicker("option", "minDate", minDate);
                }, beforeShow: function () {

                    var minDate = moment($($("#div_operationdates").find(".dtp_from")[i - 1]).val(), "DD-MM-YYYY")._i;
                    var maxDate = moment($($("#div_operationdates").find(".dtp_to")[i - 1]).val(), "DD-MM-YYYY")._i;
                    $(this).datepicker("option", "minDate", minDate);
                    $(this).datepicker("option", "maxDate", maxDate);
                },
            }, datepickersOpt));

            $(elem_ToDate[i]).datepicker($.extend({
                onSelect: function () {
                    var maxDate = $(this).datepicker('getDate');
                    maxDate.setDate(maxDate.getDate());
                    UpdateFlag = false;
                }, beforeShow: function () {

                    var minDate = moment($($("#div_operationdates").find(".dtp_from")[i - 1]).val(), "DD-MM-YYYY")._i;
                    var maxDate = moment($($("#div_operationdates").find(".dtp_to")[i - 1]).val(), "DD-MM-YYYY")._i;
                    $(this).datepicker("option", "minDate", minDate);
                    $(this).datepicker("option", "maxDate", maxDate);
                },
            }, datepickersOpt));
        }

    } catch (e) {

    }
}
function DaysAllChecked() {
    if ($('#Chk_Daily').prop("checked")) {
        $('.chk_OperationDays').removeClass('validate[required]')
        Chk_mon.checked = true;
        Chk_tue.checked = true;
        Chk_wed.checked = true;
        Chk_thu.checked = true;
        Chk_fri.checked = true;
        Chk_sat.checked = true;
        Chk_sun.checked = true;
    }
    else if ($('#Chk_Daily').prop("checked", false)) {
        $('.chk_OperationDays').addClass('validate[required]')
        Chk_mon.checked = false;
        Chk_tue.checked = false;
        Chk_wed.checked = false;
        Chk_thu.checked = false;
        Chk_fri.checked = false;
        Chk_sat.checked = false;
        Chk_sun.checked = false;
    }
}
function DaysChecked() {
    $('.chk_OperationDays').removeClass('validate[required]')
    if ($('#Chk_mon').prop("checked") || $('#Chk_tue').prop("checked") || $('#Chk_wed').prop("checked") || $('#Chk_thu').prop("checked") || $('#Chk_fri').prop("checked") || $('#Chk_sat').prop("checked") || $('#Chk_sun').prop("checked")) {
        Chk_Daily.checked = false;
    }
    if ($('#Chk_mon').prop("checked") && $('#Chk_tue').prop("checked") && $('#Chk_wed').prop("checked") && $('#Chk_thu').prop("checked") && $('#Chk_fri').prop("checked") && $('#Chk_sat').prop("checked") && $('#Chk_sun').prop("checked")) {
        Chk_Daily.checked = true;
    }
    if (($('#Chk_mon').prop("checked") == false) && ($('#Chk_tue').prop("checked") == false) && ($('#Chk_wed').prop("checked") == false) && ($('#Chk_thu').prop("checked") == false) && ($('#Chk_fri').prop("checked") == false) && ($('#Chk_sat').prop("checked") == false) && ($('#Chk_sun').prop("checked") == false)) {
        $('.chk_OperationDays').addClass('validate[required]')
    }
}
function ChangeAge() {
    $("#div_childageabove").empty();
    $("#div_smallchildageabove").empty();
    var id = $("#childagefrom").val();
    var tRow = '';
    var st = parseFloat(id - 1);
    var stt = parseFloat(st - 1);
    var en = 2;
    var html = "";
    var htm = "";
    var NN = '{"min":2,"max":' + st + ',"increment":1}';
    var TT = '{"min":2,"max":' + stt + ',"increment":1}';

    html += '<button type="button" class="button number-down">-</button>';
    html += "<input id='childageto' onchange='AgeUpTo()' type='text'  size='3' value='" + st + "' class='input-unstyled' data-number-options=" + NN + ">"
    html += '<button type="button" class="button number-up">+</button>';
    $("#div_childageabove").append(html);

    htm += '<button type="button" class="button number-down">-</button>';
    htm += "<input id='child2ageto' onchange='AgeUpTo()' type='text'  size='3' value='" + stt + "' class='input-unstyled' data-number-options=" + TT + ">"
    htm += '<button type="button" class="button number-up">+</button>';
    $("#div_smallchildageabove").append(htm);

    $("#child2agefrom").val(st);
}
function AgeUpTo() {
    $("#div_smallchildageabove").empty();
    var id = $("#childageto").val();
    var st = parseFloat(id - 1);
    var NN = '{"min":2,"max":' + st + ',"increment":1}';
    htm = "";
    htm += '<button type="button" class="button number-down">-</button>';
    htm += "<input id='child2ageto' type='text'  size='3' value='" + st + "' class='input-unstyled' data-number-options=" + NN + ">"
    htm += '<button type="button" class="button number-up">+</button>';
    $("#div_smallchildageabove").append(htm);
    $("#child2agefrom").val(id);
}
function Review() {
    $(".lbl_Name").text($("#txt_name").val());
    $(".lbl_Location").text($("#txt_location").val());
    $(".lbl_CountryCity").text($("#lbl_country").text() + '/' + $("#lbl_city").text());
    SetReviewDates();
    if (AllowChild == true) {
        $("#div_reviewchildpolicy").show()
        $("#div_ChildAges").empty();
        var html = "";
        html += '<p class="black selected-inclusion">Kids( <b>' + $("#childagefrom").val() + '</b> to <b>' + $("#childageto").val() + '</b> yrs) old, are considered as<b> big child</b></p>';
        html += '<p class="black selected-inclusion">Kids( <b>' + $("#child2agefrom").val() + '</b> to <b>' + $("#child2ageto").val() + '</b> yrs) old, are considered as<b> small child</b></p>';
        $("#div_ChildAges").append(html);
    }
    if (AllowHeight == true) {
        $("#div_ChildHeight").show()
        $("#div_ChildHeightdetail").empty();
        var html = "";
        html += '<p class="black selected-inclusion">Kids above <b>' + $("#childfeet").val() + '</b> feet and <b>' + $("#childinch").val() + ' inch</b> are considered as adult</p>';
        $("#div_ChildHeightdetail").append(html);
    }
    if ($("#btn_infantpolicy").is(":checked")) {
        $(".lbl_Infant").text("Allow");
    }
    else {
        $(".lbl_Infant").text("Not allow");
    }

    if (AllowCapacity == true) {
        $("#div_Paxcapacity").empty();
        var html = "";
        html += '<p class="black selected-inclusion">Minimum <b>' + $("#minpax").val() + '</b> and Maximum <b>' + $("#maxpax").val() + '</b>persons are allowed per booking</p>';
        $("#div_Paxcapacity").append(html);
    }
    else {
        html += '<p class="black selected-inclusion">-</p>';
    }
}

var OperationDays = "";
function SetReviewDates() {
    var arrDuration = new Array();
    var arrOpenCloseTime = new Array();
    var Duration = $(".dte_durationrange");
    $(Duration).each(function (index, Duration) {
        var ndValidtyFrom = $(Duration).find(".dtp_from")[0];
        var ndValidtyTill = $(Duration).find(".dtp_to")[0];
        var ndOpeningTime = $(Duration).find(".StartingTime")[0];
        var ndClosingTime = $(Duration).find(".EndingTime")[0];

        arrDuration.push({
            ValidtyFrom: $(ndValidtyFrom).val(),
            ValidtyTill: $(ndValidtyTill).val(),
        });
        arrOpenCloseTime.push({
            OpeningTime: $(ndOpeningTime).val(),
            ClosingTime: $(ndClosingTime).val(),
        });
    });

    $("#div_Validity").empty();
    $("#div_Opening_Closing_Time").empty();
    var html = "";
    var htmll = "";
    if (AllowFullYear == false) {
        for (var i = 0; i < arrDuration.length; i++) {
            html += '<p class="black selected-inclusion">' + arrDuration[i].ValidtyFrom + ' to ' + arrDuration[i].ValidtyTill + '</p>';
        }
        for (var i = 0; i < arrOpenCloseTime.length; i++) {
            htmll += '<p class="black selected-inclusion">' + arrOpenCloseTime[i].OpeningTime + ' to ' + arrOpenCloseTime[i].ClosingTime + '</p>';
        }

        for (var j = 0; j < $(".chk_OperationDays").length; j++) {
            if ($(".chk_OperationDays")[j].checked)
                OperationDays += $(".chk_OperationDays")[j].defaultValue + ";";
        }
        if (OperationDays.includes("Daily")) {
            $(".lbl_OperationDays").text("Daily");
        }
        else {
            $(".lbl_OperationDays").text(OperationDays);
        }

    }
    else {
        html += '<p class="black">Full Year</p>';
        htmll += '<p class="black">-</p>';
    }
    $("#div_Validity").append(html);
    $("#div_Opening_Closing_Time").append(htmll);
}
function SaveSightseeing() {
    try {
        var arrLocation = new Array();
        var arrSightseeing = new Array();
        var arrChildPolicy = new Array();
        var arrOperatingTime = new Array();
        var arrAreaLocation = new Array();
        var Tourtypelength = $(".k-button");
        var TourType = "";
        var MinHeight = "";

        /*TourType*/
        for (var i = 0; i < Tourtypelength.length; i++) {
            TourType += Tourtypelength[i].innerText + ';';
        }

        /*array Child Poilicy*/
        if (AllowChild == true) {
            if ($("#btn_infantpolicy").is(":checked")) {
                AllowInfant = true;
            }
            if (AllowHeight == true) {
                MinHeight = $("#childfeet").val() + '.' + $("#childinch").val();
            }
            arrChildPolicy.push({
                Child_Age_From: $("#childagefrom").val(),
                Child_Age_Upto: $("#childageto").val(),
                Small_Child_Age_Upto: $("#child2ageto").val(),
                Allow_Height: AllowHeight,
                Child_Min_Height: MinHeight,
                Infant_Allow: AllowInfant
            });
        }


        /*array Location detail for tbl_araegroup*/
        arrLocation = {
            Name: $("#lbl_city").text(),
            CountryName: $("#lbl_country").text(),
            Country_Code: $("#hdCountryCode").val(),
            latitude: $("#hdnCitylati").val(),
            longitude: $("#hdnCitylongi").val(),
            Placeid: $("#hdnPlaceid").val(),
        };
        /*array Location detail for tbl_commomlocation*/
        arrAreaLocation = {
            LocationName: $("#hdnName").val(),
            City: $('#lbl_city').text(),
            CityCode: $('#hdCityCode').val(),
            Country: $("#lbl_country").text(),
            CountryCode: $('#hdCountryCode').val(),
            Latitude: $("#hdnlatitude").val(),
            Longitutde: $("#hdnlatitude").val(),
            Placeid: $("#hdnPlaceid").val(),
        };


        var mincapacity = "";
        var maxcapacity = "";
        if (AllowCapacity == true) {
            mincapacity = $("#minpax").val();
            maxcapacity = $("#maxpax").val();
        }

        /*array Operating Date detail*/
        var datefrom = [];
        for (var i = 0; i < $(".dtp_from").length; i++) {
            if ($(".dtp_from")[i].value != "")
                datefrom.push($(".dtp_from")[i].value);
        }
        var dateTill = [];
        for (var i = 0; i < $(".dtp_to").length; i++) {
            if ($(".dtp_to")[i].value != "")
                dateTill.push($(".dtp_to")[i].value);
        }
        var openingTime = [];
        for (var i = 0; i < $(".StartingTime").length ; i++) {
            if ($(".StartingTime")[i].value != "")
                openingTime.push($(".StartingTime")[i].value);
        }
        var closingTime = [];
        for (var i = 0; i < $(".EndingTime").length ; i++) {
            if ($(".EndingTime")[i].value != "")
                closingTime.push($(".EndingTime")[i].value);
        }

        for (var i = 0; i < datefrom.length; i++) {
            arrOperatingTime.push({
                Operating_From: datefrom[i],
                Operating_Till: dateTill[i],
                Opening_Time: openingTime[i],
                Closing_Time: closingTime[i],
                Operating_Days: OperationDays,
            });
        }

        /*array Sightseeing detail*/
        arrSightseeing = {
            Country: $("#hdCountryCode").val(),
            City: $("#lbl_city").text(),
            Act_Name: $("#txt_name").val(),
            Description: $("#txt_description").val(),
            Tour_Type: TourType,
            Imp_Note: $("#txt_impnote").val(),
            Allow_Child: AllowChild,
            Allow_Infant: AllowInfant,
            Act_Images: arrImages.toString(),
            Min_Capacity: mincapacity,
            Max_Capacity: maxcapacity,
            Full_Year: AllowFullYear,
            Status: true,
        };

        post("../Handler/ActivityHandller.asmx/SaveSightseeing",
            {
                arrLocation: arrLocation,
                arrSightseeing: arrSightseeing,
                arrChildPolicy: arrChildPolicy,
                arrOperatingTime: arrOperatingTime,
                arrAreaLocation: arrAreaLocation,
                id: id,

            }, function (data) {
                sid = data.ActivityID;
                //$("#Button1").click();
                Success("Sightseeing Added");
                SaveDoc("Imges", "activityImages/" + sid, function () {

                });
            }, function (error) {
                AlertDanger(error.ex);
            })
    } catch (e) { AlertDanger("Unable to saved.") }
}
function ClickImges() {
    document.getElementById("Imges").click()
}

function GetSightseeing(id) {
    var data = {
        ActivityID: id
    }
    post("../Handler/ActivityHandller.asmx/GetSightseeing", data, function (data) {
        arrSightseeingDetail = data.arrSightseeingDetail
        if (arrSightseeingDetail.length != 0) {
            arrBasicDetail = arrSightseeingDetail[0].SightstseeingDetail[0];
            arrGetLocation = arrSightseeingDetail[0].Location[0];

            /*Set Basic Details*/
            if (arrSightseeingDetail[0].Location.length != 0) {
                $("#txt_location").val(arrGetLocation.City);
                $("#hdnPlaceid").val(arrGetLocation.Placeid);
                $("#hdnName").val(arrGetLocation.LocationName);
                $("#lbl_city").text(arrGetLocation.City);
                $("#lbl_country").text(arrGetLocation.Country);
            }
            
            $("#txt_name").val(arrBasicDetail.Act_Name);
            var TourType = arrBasicDetail.Tour_Type.split(";");
            if (TourType.length != 0)
                SetValidation();
            $('#sel_tourtype').data("kendoMultiSelect").value(TourType);
            $("#txt_description").val(arrBasicDetail.Description);
            $("#txt_impnote").val(arrBasicDetail.Imp_Note);
            if (arrBasicDetail.Min_Capacity != null) {
                $("#btn_capacity").click();
                $("#minpax").val(arrBasicDetail.Min_Capacity);
                $("#maxpax").val(arrBasicDetail.Max_Capacity);
            }

            /*Set Child Policy*/
            if (arrSightseeingDetail[0].ChildPolicy.length != 0) {
                $("#btn_childpolicy").click();
                arrGetChild = arrSightseeingDetail[0].ChildPolicy[0];
                AllowChild = true;
                ChangeAge()
                $("#childagefrom").val(arrGetChild.Child_Age_From);
                $("#childageto").val(arrGetChild.Child_Age_Upto);
                $("#child2agefrom").val(arrGetChild.Child_Age_Upto);
                $("#child2ageto").val(arrGetChild.Small_Child_Age_Upto);
                if (arrGetChild.Allow_Height == true) {
                    $("#btn_heightpolicy").click();
                    var n = arrGetChild.Child_Min_Height;
                    var m = parseFloat(n).toFixed(2);
                    var strNumber = m.toString();
                    var Height = strNumber.split(".");
                    $("#childfeet").val(Height[0]);
                    $("#childinch").val(Height[1]);
                }
                if (arrGetChild.Infant_Allow) {
                    $("#btn_infantpolicy").click();
                }
            }

            if (arrSightseeingDetail[0].OperatingTime.length != 0) {
                arrGetTime = arrSightseeingDetail[0].OperatingTime;
                $("#btn_duration").click();
                for (var i = 0; i < arrGetTime.length; i++) {
                    if (i != 0) {
                        SetDurationDates();
                    }
                    $("#dtp_from" + i + "").val(arrGetTime[i].Operating_From);
                    $("#dtp_to" + i + "").val(arrGetTime[i].Operating_Till);
                    $("#StartTime" + i + "").val(arrGetTime[i].Opening_Time);
                    $("#EndTime" + i + "").val(arrGetTime[i].Closing_Time);
                }
                var OpDays = arrGetTime[0].Operating_Days.split(";")
                for (var j = 0; j <= OpDays.length - 1; j++) {
                    $('input[value="' + OpDays[j].trim() + '"][class="chk_OperationDays"]').prop("checked", true);
                }
                $('.chk_OperationDays').removeClass('validate[required]')
            }


            if (arrBasicDetail.Act_Images != "") {
                LoadPreview(arrBasicDetail.Act_Images.split(","), 'activityImages/' + arrBasicDetail.Activity_Id + '', 'result');
            }
            Review();
        }

    }, function (error) {
        AlertDanger("server not responds..")
    })
}