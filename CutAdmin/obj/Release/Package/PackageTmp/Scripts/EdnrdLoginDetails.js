﻿var Path;
$(function () {
    var href = document.location.href;
    Path = getPageName(href);
    GetUserList();
    //addList('test', '1', 2);
});

function GetUserList() {
    $("#tbl_EdnrdLogin tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "../Handler/VisaHandler.asmx/GetUserList",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrUsers = result.tbl_EdnrdUser;
                var tr = '';
                $("#selSupplier").empty()
                for (var i = 0; i < arrUsers.length; i++) {
                    //if (Path != "AddVisaDetails" || Path != "adVisaDetails") {
                    tr += '<tr><td style="width: 5%">' + (i + 1) + '</td>';
                    tr += "<td>" + arrUsers[i].SupplierName + "</td>";
                    tr += "<td>" + arrUsers[i].Username + "</td>";
                    tr += "<td>" + arrUsers[i].Password + "</td>";
                    /*tr += "<td>" + arrUsers[i].Hurs96.replace("True", '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>').replace("False", '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>') + "</td>";
                    tr += "<td>" + arrUsers[i].Service14Days.replace("True", '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>').replace("False", '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>') + "</td>";
                    tr += "<td>" + arrUsers[i].Single30Days.replace("True", '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>').replace("False", '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>') + "</td>";
                    tr += "<td>" + arrUsers[i].Multipule30Days.replace("True", '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>').replace("False", '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>') + "</td>";
                    tr += "<td>" + arrUsers[i].Single90Days.replace("True", '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>').replace("False", '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>') + "</td>";
                    tr += "<td>" + arrUsers[i].Multipule90Days.replace("True", '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>').replace("False", '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>') + "</td>";
                    tr += "<td>" + arrUsers[i].Covertable90Days.replace("True", '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>').replace("False", '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>') + "</td>";*/
                    tr += "<td>" + arrUsers[i].Active.replace("True", '<span class="glyphicon glyphicon-ok" title="Activated"  aria-hidden="true"></span>').replace("False", '<span class="glyphicon glyphicon-ban-circle" title="Deactivated" aria-hidden="true"></span>') + "</td>";
                    tr += '<td><a style="cursor:pointer" target="_Blank" href="AddPostingCharges.aspx?Add=false&Sid=' + arrUsers[i].sid + '&Supplier=' + arrUsers[i].SupplierId + '"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a></td></tr>';
                    //   }
                    // else {
                    var opt = '';
                    opt = '<option value="' + arrUsers[i].Username + '|' + arrUsers[i].Password + '">' + arrUsers[i].SupplierName + '  {username: ' + arrUsers[i].Username + '}</option>'
                    $("#selSupplier").append(opt)
                    //  }
                }

                $("#tbl_EdnrdLogin tbody").append(tr);
            }
        },
        error: function () {
            alert('An error occured while Getting Username Details')
        }
    });
}
var SupplierName, Hurs96, Service14Days, Single30Days, Multipule30Days, Single90Days, Multipule90Days, Covertable90Days, Username, Password, sActive, HiddenId;
function CheckBtn() {

    if ($("#btn_User").val() == "Add") {
        AddUsername();
    }
    else if ($("#btn_User").val() == "Update") {
        UpdateUser();
    }
}
function AddUsername() {
    SupplierName = $("#txt_Supplier").val();
    Username = $("#txt_Username").val();
    Password = $("#txt_Password").val();

    if (Username == "") {
        alert("Please Enter User Name");
        return false;
    }
    if (Password == "") {
        alert("Please Enter Password");
        return false;
    }
    if (SupplierName == "") {
        Success("Please Insert Supplier Name");
        //$("#txt_Supplier").focus()
        return false;
    }
    if (chk96hr.checked == true) {
        Hurs96 = "true"
        Hurs96Price = $("#96hrPrice").val();
        if (Hurs96Price == "") {
            alert("Please Enter 96 Hour Price");
            return false;
        }
        Hurs96Price = Hurs96Price + " " + $("#96hrCr option:selected").val();
    }
    else {
        Hurs96 = "false"
    }

    if (chk14Days.checked == true) {
        Service14Days = "True"
        Service14DaysPrice = $("#14DaysPrice").val();
        if (Service14DaysPrice == "") {
            alert("Please Enter 14 Days Price");
            return false;
        }
        Service14DaysPrice = Service14DaysPrice + " " + $("#14DaysCr option:selected").val();
    }
    else {
        Service14Days = "false"
    }

    if (chk30Single.checked == true) {
        Single30Days = "true"
        Single30DaysPrice = $("#30SinglePrice").val();
        if (Single30DaysPrice == "") {
            alert("Please Enter 30 Days Single Price");
            return false;
        }
        Single30DaysPrice = Single30DaysPrice + " " + $("#30SingleCr option:selected").val();
    }
    else {
        Single30Days = "false"
    }

    if (chk30Multipule.checked == true) {
        Multipule30Days = "true"
        Multipule30DaysPrice = $("#30MultipulePrice").val();
        if (Multipule30DaysPrice == "") {
            alert("Please Enter 30 Days Multiple Price");
            return false;
        }
        Multipule30DaysPrice = Multipule30DaysPrice + " " + $("#30MultipuleCr option:selected").val();
    }

    else {
        Multipule30Days = "false"
    }

    if (chk90Single.checked == true) {
        Single90Days = "true"
        Single90DaysPrice = $("#90SinglePrice").val();
        if (Single90DaysPrice == "") {
            alert("Please Enter 90 Days Single Price");
            return false;
        }
        Single90DaysPrice = Single90DaysPrice + " " + $("#90SingleCr option:selected").val();
    }
    else {
        Single90Days = "false"
    }
    if (chk90Multipule.checked == true) {
        Multipule90Days = "true"
        Multipule90DaysPrice = $("#90MultipulePrice").val();
        if (Multipule90DaysPrice == "") {
            alert("Please Enter 90 Days Multiple Price");
            return false;
        }
        Multipule90DaysPrice = Multipule90DaysPrice + " " + $("#90MultipuleCr option:selected").val();
    }
    else {
        Multipule90Days = "false"
    }

    if (chk90Covertable.checked == true) {
        Covertable90Days = "true"
        Covertable90DaysPrice = $("#90ConvetablePrice").val();
        if (Covertable90DaysPrice == "") {
            alert("Please Enter 90 Days Convetable Price");
            return false;
        }
        Covertable90DaysPrice = Covertable90DaysPrice + " " + $("#90ConvetableCr option:selected").val();
    }
    else {
        Covertable90Days = "false"
    }
    if (chkActive.checked == true)
        sActive = "true"
    else
        sActive = "false"
    var Data = {
        SupplierName: SupplierName,
        Username: Username,
        Password: Password,
        Hours96: Hurs96,
        Sevice14Day: Service14Days,
        Single30Days: Single30Days,
        Multipule30Days: Multipule30Days,
        Single90Days: Single90Days,
        Multipule90Days: Multipule90Days,
        Covertable90Days: Covertable90Days,
        Active: sActive,
        Hurs96Price: Hurs96Price,
        Service14DaysPrice: Service14DaysPrice,
        Single30DaysPrice: Single30DaysPrice,
        Multipule30DaysPrice: Multipule30DaysPrice,
        Single90DaysPrice: Single90DaysPrice,
        Multipule90DaysPrice: Multipule90DaysPrice,
        Covertable90DaysPrice: Covertable90DaysPrice
    }
    var json = JSON.stringify(Data)
    $.ajax({
        type: "POST",
        url: "VisaHandler.asmx/AddUser",
        data: json,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                $("#UpdateUserModal").modal("hide")
                Success("Username Details Added")
                GetUserList()
            }
        },
        error: function () {
            alert('An error occured while Adding Username Details')
        }
    });
}
var Hurs96Price = ""; Service14DaysPrice = ""; Single30DaysPrice = ""; Multipule30DaysPrice = ""; Single90DaysPrice = ""; Multipule90DaysPrice = ""; Covertable90DaysPrice = "";

function UpdateUser() {
    Username = $("#txt_Username").val();
    Password = $("#txt_Password").val();
    if (chk96hr.checked == true) {
        Hurs96 = "true"
        Hurs96Price = $("#96hrPrice").val();
        if (Hurs96Price == "") {
            alert("Please Enter 96 Hour Price");
            return false;
        }
        Hurs96Price = Hurs96Price + " " + $("#96hrCr option:selected").val();
    }
    else {
        Hurs96 = "false"
    }

    if (chk14Days.checked == true) {
        Service14Days = "True"
        Service14DaysPrice = $("#14DaysPrice").val();
        if (Service14DaysPrice == "") {
            alert("Please Enter 14 Days Price");
            return false;
        }
        Service14DaysPrice = Service14DaysPrice + " " + $("#14DaysCr option:selected").val();
    }
    else {
        Service14Days = "false"
    }

    if (chk30Single.checked == true) {
        Single30Days = "true"
        Single30DaysPrice = $("#30SinglePrice").val();
        if (Single30DaysPrice == "") {
            alert("Please Enter 30 Days Single Price");
            return false;
        }
        Single30DaysPrice = Single30DaysPrice + " " + $("#30SingleCr option:selected").val();
    }
    else {
        Single30Days = "false"
    }

    if (chk30Multipule.checked == true) {
        Multipule30Days = "true"
        Multipule30DaysPrice = $("#30MultipulePrice").val();
        if (Multipule30DaysPrice == "") {
            alert("Please Enter 30 Days Multiple Price");
            return false;
        }
        Multipule30DaysPrice = Multipule30DaysPrice + " " + $("#30MultipuleCr option:selected").val();
    }

    else {
        Multipule30Days = "false"
    }

    if (chk90Single.checked == true) {
        Single90Days = "true"
        Single90DaysPrice = $("#90SinglePrice").val();
        if (Single90DaysPrice == "") {
            alert("Please Enter 90 Days Single Price");
            return false;
        }
        Single90DaysPrice = Single90DaysPrice + " " + $("#90SingleCr option:selected").val();
    }
    else {
        Single90Days = "false"
    }
    if (chk90Multipule.checked == true) {
        Multipule90Days = "true"
        Multipule90DaysPrice = $("#90MultipulePrice").val();
        if (Multipule90DaysPrice == "") {
            alert("Please Enter 90 Days Multiple Price");
            return false;
        }
        Multipule90DaysPrice = Multipule90DaysPrice + " " + $("#90MultipuleCr option:selected").val();
    }
    else {
        Multipule90Days = "false"
    }

    if (chk90Covertable.checked == true) {
        Covertable90Days = "true"
        Covertable90DaysPrice = $("#90ConvetablePrice").val();
        if (Covertable90DaysPrice == "") {
            alert("Please Enter 90 Days Convetable Price");
            return false;
        }
        Covertable90DaysPrice = Covertable90DaysPrice + " " + $("#90ConvetableCr option:selected").val();
    }
    else {
        Covertable90Days = "false"
    }
    if (chkActive.checked == true)
        sActive = "true"
    else
        sActive = "false"
    var Data = {
        Username: Username,
        Password: Password,
        Hours96: Hurs96,
        Sevice14Day: Service14Days,
        Single30Days: Single30Days,
        Multipule30Days: Multipule30Days,
        Single90Days: Single90Days,
        Multipule90Days: Multipule90Days,
        Covertable90Days: Covertable90Days,
        Active: sActive,
        Hours96Price: Hurs96Price,
        Sevice14DayPrice: Service14DaysPrice,
        Single30DaysPrice: Single30DaysPrice,
        Multipule30DaysPrice: Multipule30DaysPrice,
        Single90DaysPrice: Single90DaysPrice,
        Multipule90DaysPrice: Multipule90DaysPrice,
        Covertable90DaysPrice: Covertable90DaysPrice
    }
    var json = JSON.stringify(Data)
    if ($("#btn_User").val() == "Update") {
        $.ajax({
            type: "POST",
            url: "VisaHandler.asmx/UpdateUsername",
            data: json,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                debugger;
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    alert("Username Details Updated")
                    $("#UpdateUsernameModal").modal("hide")
                    GetUserList()
                }
            },
            error: function () {
                alert('An error occured while Updating Username Details')
            }
        });
    }
    else {
        $.ajax({
            type: "POST",
            url: "VisaHandler.asmx/AddUser",
            data: json,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                debugger;
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    alert("Username Details Added")
                    $("#AddUsernameModal").modal("hide")
                    GetUserList()
                }
            },
            error: function () {
                alert('An error occured while Adding Username Details')
            }
        });
    }

}

var Price = "";
function Update(Username, Password, Hurs96, Service14Days, Single30Days, Multipule30Days, sid, Active, Single90Days, Multipule90Days, Covertable90Days, SupplierName, Hurs96Price, Service14DaysPrice, Single30DaysPrice, Multi30DaysPrice, Single90DaysPrice, Multi90DaysPrice, Covert90DaysPrice) {
    $("#UpdateUserModal").modal("show")
    HiddenId = sid;
    ClearModalBoxField();
    $("#txt_Username").val(Username);
    $("#txt_Password").val(Password);
    $("#txt_Supplier").val(SupplierName);
    $("#btn_User").val("Update")
    if (Hurs96 == "True") {
        $("#chk96hr").prop('checked', true)
        //$('input[id="chk96hr"][type="checkbox"][value="96hr"]').attr("checked", true)
        //$('#chk96hr').attr('checked', true);
        Price = Hurs96Price.split(' ');
        $("#96hrPrice").val(Price[0]);
        $("#96hrCr").val(Price[1]);
        //$("#96hrCr").each(function () {
        //if ($(this).val() == Price[1]) {
        //    $("#96hrCr").val(Price[1]);
        //    //return;
        //}
        //});
    }
    else
        $('input[id="chk96hr"][type="checkbox"][value="96hr"]').attr("checked", false)
    if (Service14Days == "True") {
        $("#chk14Days").prop('checked', true)
        //$('input[id="chk14Days"][type="checkbox"][value="14Days"]').attr("checked", true)
        Price = Service14DaysPrice.split(' ');
        $("#14DaysPrice").val(Price[0]);
        $("#14DaysCr").val(Price[1]);
    }

    else
        $('input[id="chkService14Days"][type="checkbox"][value="14Days"]').attr("checked", false)
    if (Single30Days == "True") {
        $("#chk30Single").prop('checked', true)
        //$('input[id="chk30Single"][type="checkbox"][value="30Single"]').attr("checked", true)
        Price = Single30DaysPrice.split(' ');
        $("#30SinglePrice").val(Price[0]);
        $("#30SingleCr").val(Price[1]);
    }
    else
        $('input[id="chk30Single"][type="checkbox"][value="30Single"]').attr("checked", false)
    if (Multipule30Days == "True") {
        $("#chk30Multipule").prop('checked', true)
        //$('input[id="chk30Multipule"][type="checkbox"][value="30Multipule"]').attr("checked", true)
        Price = Multi30DaysPrice.split(' ');
        $("#30MultipulePrice").val(Price[0]);
        $("#30MultipuleCr").val(Price[1]);
    }
    else
        $('input[id="chk30Multipule"][type="checkbox"][value="30Multipule"]').attr("checked", false)
    if (Single90Days == "True") {
        $("#chk90Single").prop('checked', true)
        //$('input[id="chk90Single"][type="checkbox"][value="90Single"]').attr("checked", true)
        Price = Single90DaysPrice.split(' ');
        $("#90SinglePrice").val(Price[0]);
        $("#90SingleCr").val(Price[1]);
    }
    else
        $('input[id="chk90Single"][type="checkbox"][value="90Single"]').attr("checked", false)

    if (Multipule90Days == "True") {
        $("#chk90Multipule").prop('checked', true)
        //$('input[id="chk90Multipule"][type="checkbox"][value="90Multipule"]').attr("checked", true)
        Price = Multi90DaysPrice.split(' ');
        $("#90MultipulePrice").val(Price[0]);
        $("#90MultipuleCr").val(Price[1]);
    }
    else
        $('input[id="chk90Multipule"][type="checkbox"][value="90Multipule"]').attr("checked", false)

    if (Covertable90Days == "True") {
        $("#chk90Covertable").prop('checked', true)
        // $('input[id="chk90Covertable"][type="checkbox"][value="90Covertable"]').attr("checked", true)
        Price = Covert90DaysPrice.split(' ');
        $("#90ConvetablePrice").val(Price[0]);
        $("#90ConvetableCr").val(Price[1]);
    }
    else
        $('input[id="chk90Covertable"][type="checkbox"][value="90Covertable"]').attr("checked", false)

    if (Active == "True") {
        $("#chkActive").prop('checked', true)
        //$('input[id="chkActive"][type="checkbox"][value="Active"]').attr("checked", true)
    }

    else
        $('input[id="chkActive"][type="checkbox"][value="Active"]').attr("checked", false)
}

function ClearModalBoxField() {
    $("#txt_Username").val('');
    $("#txt_Password").val('');
    $("#txt_Supplier").val('');
    $('input[type="checkbox"]').attr("checked", false)
    $("#96hrPrice").val('');
    $("#14DaysPrice").val('');
    $("#30SinglePrice").val('');
    $("#30MultipulePrice").val('');
    $("#90SinglePrice").val('');
    $("#90MultipulePrice").val('');
    $("#90ConvetablePrice").val('');
    //$(".form-control option").val('INR')
    for (var i = 0; i < $(".form-control option").length; i++) {
        if ($(".form-control option")[i].innerText == "INR") {
            $(".form-control option")[i].selected = true;
        }
    }
    //$("#96hrCr option").each(function () {
    //    if ($(this).html() == 'INR') {
    //        $(this).attr("selected", "selected");
    //        return;
    //    }
    //});
    //$('[name=Cr] option').filter(function () {
    //    return ($(this).text() == 'INR'); //To select Blue
    //}).prop('selected', true);
}
function OpenDilogBox() {
    ClearModalBoxField()
    $("#btn_User").val("Add")
    $("#UpdateUserModal").modal("show")
}
function getPageName(url) {
    var index = url.lastIndexOf("/") + 1;
    var filenameWithExtension = url.substr(index);
    var filename = filenameWithExtension.split(".")[0]; // <-- added this line
    return filename;                                    // <-- added this line
}

function addList(Name, Text, Value) {
    try {
        var select = document.getElementsByName(Name)[0];
        var option = document.createElement('option');
        option.text = Text;
        option.value = Value;
        select.add(option);
    }
    catch (ex) {
        alert(ex);
    }
}

