﻿var AgeOfChild1 = "";
var AgeOfChild2 = "";
var AgeOfInfant = "";
var RateofAdult = "";
var RateforChild1 = "";
var RateforChild2 = "";
var RateforInfant = "";
var arrBooking = new Array();
$(function () {
    GetAgency();
    GetDetails();
})

function GetDetails() {
    debugger
    arrBooking = window.parent.arrBooking;
    if (arrBooking.length != 0) {
        /*Set Currency*/
        $(".sCurrency").text(arrBooking.Currency);

        /*set adult details*/
        RateofAdult = window.parent.$("#" + arrBooking.RateTypes + "_" + arrBooking.SlotID + "_adult").val();
        $("#lbl_rateofadult").text(RateofAdult);

        /*set childs details*/
        if (arrBooking.arrChildPolicy.length != 0) {

            AgeOfChild1 = "Child(" + arrBooking.arrChildPolicy[0].Child_Age_Upto + " " + "to " + arrBooking.arrChildPolicy[0].Child_Age_From + " yrs) ";
            AgeOfChild2 = "Child(" + arrBooking.arrChildPolicy[0].Small_Child_Age_Upto + " " + "to " + arrBooking.arrChildPolicy[0].Child_Age_Upto + " yrs)";
            $("#spn_childage1").text(AgeOfChild1);
            $("#spn_childage2").text(AgeOfChild2);

            RateforChild1 = window.parent.$("#" + arrBooking.RateTypes + "_" + arrBooking.SlotID + "_child1").val();
            RateforChild2 = window.parent.$("#" + arrBooking.RateTypes + "_" + arrBooking.SlotID + "_child2").val();
            $("#lbl_rateofchild1").text(RateforChild1);
            $("#lbl_rateofchild2").text(RateforChild2);

            var x = document.getElementById("div_child");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }

            /*set infant details*/
            if (arrBooking.arrChildPolicy[0].Infant_Allow == true) {
                AgeOfInfant = "Infant(Below " + arrBooking.arrChildPolicy[0].Small_Child_Age_Upto + " yrs)";
                $("#spn_infantage").text(AgeOfInfant);

                RateforInfant = window.parent.$("#" + arrBooking.RateTypes + "_" + arrBooking.SlotID + "_infant").val();
                $("#lbl_rateofinfant").text(RateforInfant);

                var x = document.getElementById("div_infant");
                if (x.style.display === "none") {
                    x.style.display = "block";
                } else {
                    x.style.display = "none";
                }
            }
        }
        CalculateRates();
    }
}
var adulttotal = "";
var child1total = "";
var child2total = "";
var infanttotal = "";
var TotalAmount = "";
var TotalPax = "";

function CalculateRates() {
    adulttotal = parseFloat(RateofAdult) * parseFloat($("#txt_adult").val());
    child1total = parseFloat(RateforChild1) * parseFloat($("#txt_child1").val());
    child2total = parseFloat(RateforChild2) * parseFloat($("#txt_child2").val());
    infanttotal = parseFloat(RateforInfant) * parseFloat($("#txt_infant").val());
    TotalAmount = parseFloat(adulttotal) + parseFloat(child1total) + parseFloat(child2total) + parseFloat(infanttotal);
    TotalPax = parseFloat($("#txt_adult").val()) + parseFloat($("#txt_child1").val()) + parseFloat($("#txt_child2").val()) + parseFloat($("#txt_infant").val());

    $("#lbl_rateofadult").text(adulttotal);
    $("#lbl_rateofchild1").text(child1total);
    $("#lbl_rateofchild2").text(child2total);
    $("#lbl_rateofinfant").text(infanttotal);
    $("#lbl_totalamount").text(TotalAmount);
}

function GetAgency() {
    try {
        post("../GenralHandler.asmx/Getagency", {}, function (data) {
            arrAgencyDetail = data.arrAgencyDetail;
            $(arrAgencyDetail).each(function (index, Agency) {
                $('#Sel_Agency').append($('<option></option>').val(Agency.AgencyID).html(Agency.AgencyName));
                $('#Sel_Agency').change();
            });
        }, function (errordata) {
            alertDanger(errordata.ex)
        })
    } catch (e) { }
}

var arrBookingDetail = new Array();
function BookSightseeing() {
    debugger
    try {
        if ($("#frm_booking").validationEngine('validate')) {
            var PassengerName = $("#txt_name").val() + ' ' + $("#txt_lname").val();

            arrBookingDetail = {
                Activity_Id: arrBooking.ActivityID,
                Passenger_Name: PassengerName,
                Email: $("#txt_email").val(),
                Contact_No: $("#txt_contact").val(),
                Adults: $("#txt_adult").val(),
                Child1: $("#txt_child1").val(),
                Child2: $("#txt_child2").val(),
                Infant: $("#txt_infant").val(),
                Adult_Cost: $("#lbl_rateofadult").text(),
                Child1_Cost: $("#lbl_rateofchild1").text(),
                Child2_Cost: $("#lbl_rateofchild2").text(),
                Infant_Cost: $("#lbl_rateofinfant").text(),
                TotalPax:TotalPax,
                TotalAmount: $("#lbl_totalamount").text(),
                User_Id: $("#Sel_Agency").val(),
                SupplierId:arrBooking.Supplier,
                Sightseeing_Date: arrBooking.Sightseeing_Date,
                SlotID: arrBooking.SlotID,
                Ticket_Type: arrBooking.TicketType,
                Activity_Type: arrBooking.RateTypes,
                BookingType: "B2B",
                InventoryType: arrBooking.InventoryType
            };

            post("../Handler/ActivityHandller.asmx/BookSightseeing",
        {
            arrBookingDetail: arrBookingDetail,
        }, function (data) {
            Success("Sightseeing Booked Successfully");
        }, function (error) {
            AlertDanger(error.ex);
        })
        }
    }
    catch (e) {
    }
}

