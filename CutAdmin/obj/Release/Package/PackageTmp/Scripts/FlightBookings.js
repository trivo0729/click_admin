﻿$(function () {
    $("#Arrival").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#Departure").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#Bookingdate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    // GetFlightsBookings()
});
var arrFlightsName = new Array();
arrFlightsName.push(
    { ID: "SG", Value: "Spice Jet", Type: ["N", "L"] },
    { ID: "6E", Value: "Indigo", Type: ["N", "L"] },
    { ID: "G8", Value: "Go Air", Type: ["N", "L"] },
    { ID: "G9", Value: "Air Arabia", Type: ["N"] },
    { ID: "FZ", Value: "Fly Dubai", Type: ["N"] },
    { ID: "IX", Value: "Air India Express", Type: ["N"] },
    { ID: "AK", Value: "Air Asia", Type: ["N"] },
    { ID: "LB", Value: "Air Costa", Type: ["N"] },
    { ID: "UK", Value: "Air Vistara", Type: ["N", "G"] },
    { ID: "AI", Value: "Air India", Type: ["G"] },
    { ID: "9W", Value: "Jet Airways", Type: ["G"] },
    { ID: "S2", Value: "JetLite", Type: ["G"] }
);
//function GetFlightsBookings() {
//    $.ajax({
//        type: "POST",
//        url: "../handler/FlightHandler.asmx/GetBookingList",
//        data: {},
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//            if (result.retCode == 1) {
//                arrAirTicket = result.arrAirTicket;
//                GenrateFlightHtml();
//                $("#tbl_AirTicket").dataTable({
//                    bSort: false, sPaginationType: 'full_numbers',
//                });
//            }
//            else {
//                $("#tbl_AirTicket").dataTable({
//                    bSort: false, sPaginationType: 'full_numbers',
//                });
//            }
//        },
//        error: function () {
//            $('#SpnMessege').text("An error occured while loading countries")
//            $('#ModelMessege').modal('show')
//            $("#tbl_AirTicket").dataTable({
//                bSort: false, sPaginationType: 'full_numbers',
//            });
//        }
//    });
//}

function GetFlightsBookings() {
    var columns = [
        {
            "mData": "Date",
            "mRender": function (data, type, row, meta) {
                if (row.Date != null) {
                    var InvoiceCreatedOn = row.Date.split('+');
                    InvoiceCreatedOn = InvoiceCreatedOn[0];
                    InvoiceCreatedOn = InvoiceCreatedOn.split(' ');
                    InvoiceCreatedOn = InvoiceCreatedOn[0];
                    return '' + InvoiceCreatedOn + '';
                }
                else {
                    return '';
                }
            }
        },
        {
            "mData": "InvoiceNo",
            //"mRender": function (data, type, row, meta) {
            //    return '' + row.InvoiceNo + '';
            //}
        },
        {
            "mData": "AgencyName",
            //"mRender": function (data, type, row, meta) {
            //    return '' + row.AgencyName + '';
            //}
        },
        {
            "mData": "PNR",
            "mRender": function (data, type, row, meta) {
                return '' + row.AirlineName + '-' + row.PNR + '';
            }
        },
        {
            "mData": "Paxces",
            "mRender": function (data, type, row, meta) {
                var trow = '';
                if (row.Paxces != 1) {
                    trow += '<span>' + row.LeadingPaxName + '-' + row.Paxces + 'Pax<b class="info-spot" style="font-size:11px;cursor: pointer;" id="pax_Details' + row.BookingID + '"></b></span>';
                }
                else {
                    trow += '<span>' + row.LeadingPaxName + '</span>';
                }
                GetPaxDetails(row.BookingID);
                return trow;
               
            }
        },

        {
            "mData": "AirlineName",
            "mRender": function (data, type, row, meta) {
                var trow = '';
                if (row.DepartureDate.includes('T'))
                {
                    var DepartureDate = row.DepartureDate.split('T');
                }
                else
                {
                    var DepartureDate = row.DepartureDate.split(' ');
                }
                DepartureDate = DepartureDate[0];
                GetSegment(row.BookingID)
                trow += '<div style="text-align:center">' + row.AirlineName + '</br>' + DepartureDate + ' </div>';
                trow += '<div style="text-align:center"><span id="Segment' + row.BookingID + '"></span><br></div>';
                return trow;
                
            }
        },
        {
            "mData": "TicketStatus",
            "mRender": function (data, type, row, meta) {
                var trow = '<div style="max-width:75px; text-align: center;">';
                if (row.TicketStatus != null) {
                    if (row.TicketStatus == 'Cancelled') {
                        trow += '<span class="status-cancelled" style="width: 53px;">Cancelled</span>';
                    } else if (row.TicketStatus == '1') {
                        trow += '<span class="status-vouchered" style="width: 53px;">Ticketed</span>';
                    } else {
                        trow += '<span class="status-onrequest" style="width: 53px;">Hold</span>';
                    }
                }

                return trow;
            }
        },
        {
            "mData": "PublishedFare",  
            "mRender": function (data, type, row, meta) {
                return '<b>S </b> - ' + parseFloat(row.PublishedFare).toFixed(2) + '<br> <b> P </b> - ' + parseFloat(row.InvoiceAmount).toFixed(2) + '';
            }
        },
        {
            "mData": "uid",
            "sWidth":"10%",
            "mRender": function (data, type, row, meta) {
                var trow = '';
                trow += '<a href="#" class="invoice-btn" title="Click for Invoice" onclick="GetPrintFlightInvoice(\'' + row.BookingID + '\',\'' + row.uid + '\',\'' + row.TicketStatus + '\')">INVOICE</a>'
                if (row.TicketStatus == '1') {
                    trow += '<a href="#" class="voucher-btn"  title="Click for Ticket" onclick="GetPrintTicket(\'' + row.BookingID + '\',\'' + row.uid + '\',\'' + row.TicketStatus + '\')">Ticket</a>'
                    trow += '<a href="#" class="cancel-btn" title="Click to Cancel" onclick="CancelBooking(\'' + row.BookingID + '\',\'' + row.TicketStatus + '\')">Cancel</a>';
                }
                return trow;
            }
        },

    ];
    GetData("tbl_AirTicket", [], 'handler/FlightHandler.asmx/GetBookingList', columns);
    $("#tbl_AirTicket").css("width", "100%");
}



function SearchByFlight() {
    $("#tbl_AirTicket").dataTable().fnClearTable();
    $("#tbl_AirTicket").dataTable().fnDestroy();
    var TransactionNo = $("#txt_transaction").val();
    var AgencyName = $("#txt_Agency").val();
    var Traveller = $("#txt_Ticket").val();
    var TicketStatus = $("#selectAirStatus").val();
    var data =
      {
          TransactionNo: TransactionNo,
          //StartDate: StartDate,
          // EndDate: EndDate,
          AgencyName: AgencyName,
          Paxces: Traveller,
          TicketStatus: TicketStatus
      }
    var columns = [
       {
           "mData": "Date",
           "mRender": function (data, type, row, meta) {
               if (row.Date != null) {
                   var InvoiceCreatedOn = row.Date.split('+');
                   InvoiceCreatedOn = InvoiceCreatedOn[0];
                   InvoiceCreatedOn = InvoiceCreatedOn.split(' ');
                   InvoiceCreatedOn = InvoiceCreatedOn[0];
                   return '' + InvoiceCreatedOn + '';
               }
               else {
                   return '';
               }
           }
       },
       {
           "mData": "InvoiceNo",
           //"mRender": function (data, type, row, meta) {
           //    return '' + row.InvoiceNo + '';
           //}
       },
       {
           "mData": "AgencyName",
           //"mRender": function (data, type, row, meta) {
           //    return '' + row.AgencyName + '';
           //}
       },
       {
           "mData": "PNR",
           "mRender": function (data, type, row, meta) {
               return '' + row.AirlineName + '-' + row.PNR + '';
           }
       },
       {
           "mData": "Paxces",
           "mRender": function (data, type, row, meta) {
               var trow = '';
               if (row.Paxces != 1) {
                   trow += '<span>' + row.LeadingPaxName + '-' + row.Paxces + 'Pax<b class="info-spot" style="font-size:11px;cursor: pointer;" id="pax_Details' + row.BookingID + '"></b></span>';
               }
               else {
                   trow += '<span>' + row.LeadingPaxName + '</span>';
               }
               GetPaxDetails(row.BookingID);
               return trow;

           }
       },

       {
           "mData": "AirlineName",
           "mRender": function (data, type, row, meta) {
               var trow = '';
               if (row.DepartureDate.includes('T')) {
                   var DepartureDate = row.DepartureDate.split('T');
               }
               else {
                   var DepartureDate = row.DepartureDate.split(' ');
               }
               DepartureDate = DepartureDate[0];
               GetSegment(row.BookingID)
               trow += '<div style="text-align:center">' + row.AirlineName + '</br>' + DepartureDate + ' </div>';
               trow += '<div style="text-align:center"><span id="Segment' + row.BookingID + '"></span><br></div>';
               return trow;

           }
       },
       {
           "mData": "TicketStatus",
           "mRender": function (data, type, row, meta) {
               var trow = '<div style="max-width:75px; text-align: center;">';
               if (row.TicketStatus != null) {
                   if (row.TicketStatus == 'Cancelled') {
                       trow += '<span class="status-cancelled" style="width: 53px;">Cancelled</span>';
                   } else if (row.TicketStatus == '1') {
                       trow += '<span class="status-vouchered" style="width: 53px;">Ticketed</span>';
                   } else {
                       trow += '<span class="status-onrequest" style="width: 53px;">Hold</span>';
                   }
               }

               return trow;
           }
       },
       {
           "mData": "PublishedFare",
           "mRender": function (data, type, row, meta) {
               return '<b>S </b> - ' + parseFloat(row.PublishedFare).toFixed(2) + '<br> <b> P </b> - ' + parseFloat(row.InvoiceAmount).toFixed(2) + '';
           }
       },
       {
           "mData": "uid",
           "sWidth": "10%",
           "mRender": function (data, type, row, meta) {
               var trow = '';
               trow += '<a href="#" class="invoice-btn" title="Click for Invoice" onclick="GetPrintFlightInvoice(\'' + row.BookingID + '\',\'' + row.uid + '\',\'' + row.TicketStatus + '\')">INVOICE</a>'
               if (row.TicketStatus == '1') {
                   trow += '<a href="#" class="voucher-btn"  title="Click for Ticket" onclick="GetPrintTicket(\'' + row.BookingID + '\',\'' + row.uid + '\',\'' + row.TicketStatus + '\')">Ticket</a>'
                   trow += '<a href="#" class="cancel-btn" title="Click to Cancel" onclick="CancelBooking(\'' + row.BookingID + '\',\'' + row.TicketStatus + '\')">Cancel</a>';
               }
               return trow;
           }
       },

    ];
    GetData("tbl_AirTicket", data, '../handler/FlightHandler.asmx/SearchAirBookingList', columns);
    $("#tbl_AirTicket").css("width", "100%");
}

function GetPaxDetails(BookingID) {
    var data = {
        BookingID: BookingID,

    }
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/GetPaxDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $('#pax_Details' + BookingID + '').empty();
                var PaxList = result.PaxList;
                var trow = '';
                var html = '';
                var ol = "";
                ol += '<ol>'
                for (var Pax = 0; Pax < PaxList.length; Pax++) {
                    ol += '<li>' + PaxList[Pax] + '</li>'
                }
                ol += '</ol>'
                var PaxCount = PaxList.length - 1;
                if (PaxCount != 0) {
                    html += '<span class="info-bubble" style="width: 200px;" >' + ol + '</span>';
                }
                $('#pax_Details' + BookingID + '').addClass('info-spot on-top')
                $('#pax_Details' + BookingID + '').append(html);

                //    return html;
            }
        },
        error: function () {

        }
    });



}

function GetSegment(BookingID) {
    var data = {
        BookingID: BookingID,
    }
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/GetSegment",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $('#Segment' + BookingID + '').empty();
                var SegmentList = result.Segmnets;
                var html = '';
                var html1 = '';
                var html2 = '';
                for (var Seg = 0; Seg < SegmentList.length; Seg++) {
                    var Origin = SegmentList[Seg].OriginAirport.split('(');
                    Origin = Origin[1].split(')');
                    Origin = Origin[0];
                    var Destination = SegmentList[Seg].DestinationAirport.split('(');
                    Destination = Destination[1].split(')');
                    Destination = Destination[0];
                  
                    html += '<span class="info-spot" style="font-size:11px;margin-right: 6px;cursor: pointer"">' + Origin + '<span class="info-bubble" style="width: 200px;">' + SegmentList[Seg].OriginAirport + '</span></span>&nbsp;-&nbsp;';
                    html += '<span class="info-spot" style="font-size:11px;margin-left: 4px;cursor: pointer"">' + Destination + '<span class="info-bubble" style="width: 200px;">' + SegmentList[Seg].DestinationAirport + '</span></span></br>';
                   
                }
                $('#Segment' + BookingID + '').append(html);  
            }
        },
        error: function () {

        }
    });
}

function CancelBooking(BookingID, Type) {
    var data = {
        BookingID: BookingID,
        Type: Type
    }

    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/CancelBooking",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

            }
        },
        error: function () {
            $('#SpnMessege').text("An error occured while cancel booking")
            $('#ModelMessege').modal('show')
        }
    });
}

function GetPrintFlightInvoice(ReservationID, Uid, Status) {

    if (Status == '1' || Status == 'Cancelled') {

        var win = window.open('ViewFlightInvoice.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status, '_blank');
    }
    else {
        Success('Please Confirm Your Booking to get Invoice !!')
       // $('#ModelMessege').modal('show')
    }
}

function GetPrintTicket(ReservationID, Uid, Status) {
    ;
    if (Status == '1') {
        var win = window.open('ViewFlightTicket.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status, '_blank');
    }
    else {
        Success('Cancelled  !!')
      //  $('#ModelMessege').modal('show')
        //  alert('Please Confirm Your Booking!')
    }
}

function ExportToExcelFlight(Document) {
    var InvoiceNo = $("#txt_transaction").val();
    var AgencyName = $("#txt_Agency").val();
    var Ticket = $("#txt_Ticket").val();
    var TicketStatus = $("#selectAirStatus").val();
    //var RequestParams = "?RefNo=" + RefNo + "&FirstName=" + ApplicationName + "&Doc=" + Doc + "&Status=" + Status
    //  +"&VisaType=" + VisaType + "&Agency=" + Agency;
    //  window.location.href = "../Handler/ExportToExcelHandler.ashx?datatable=VisaBookingDetails" + RequestParams;
    window.location.href = "../Handler/ExportToExcelHandler.ashx?datatable=FlightBookingDetails&Document=" + Document + "&InvoiceNo=" + InvoiceNo + "&AgencyName=" + AgencyName + "&Ticket=" + Ticket + "&TicketStatus=" + TicketStatus;
   
}


function SearchFlights() {
    var BooingStatus = $("#selReservation").val()
    if (BooingStatus == "Ticketed") {
        BooingStatus = 1;
    }
    else if (BooingStatus == "Hold") {
        BooingStatus = "";
    }
    else {
        BooingStatus = $("#selReservation").val()
    }
    var data = {
        BooingStatus: BooingStatus,
        sArrival: $("#Arrival").val(),
        sDeparture: $("#Departure").val(),
        Passenger: $("#txt_Passenger").val(),
        Reference: $("#txt_Reference").val(),
        Location: $("#txt_Hotel").val(),
        Airlines: [],
        Agency: 0,
        InvoiceDate: $("#Bookingdate").val()
    };
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/SearchBookingList",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrAirTicket = result.arrAirTicket;
                GenrateFlightHtml();
                $("#tbl_AirTicket").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });

            }
        },
        error: function () {
            $('#SpnMessege').text("An error occured while Searching")
            $('#ModelMessege').modal('show')
        }
    });
}

function GenrateFlightHtml() {
    $("#tbl_AirTicket").dataTable().fnClearTable();
    $("#tbl_AirTicket").dataTable().fnDestroy();
    var html = '';
    for (var i = 0; i < arrAirTicket.length; i++) {
        if (arrAirTicket[i].Reservation.TicketStatus == "1")
            arrAirTicket[i].Reservation.TicketStatus = "Ticketed"
        else if (arrAirTicket[i].Reservation.TicketStatus == "")
            arrAirTicket[i].Reservation.TicketStatus = "Hold"
        var FlightName = $.grep(arrFlightsName, function (p) { return p.ID == arrAirTicket[i].Reservation.AirlineName; })
            .map(function (p) { return p.Value; });
        html += '<tr>';
        html += '<td style ="max-width:20px; vertical-align: middle;" class="center">' + parseInt(i + 1) + '</td > ';
        if (arrAirTicket[i].Reservation.InvoiceCreatedOn != "" && arrAirTicket[i].Reservation.InvoiceCreatedOn != null) {
            var InvoiceCreatedOn = arrAirTicket[i].Reservation.InvoiceCreatedOn.split('+');
            InvoiceCreatedOn = InvoiceCreatedOn[0];
            InvoiceCreatedOn = InvoiceCreatedOn.split(' ');
            InvoiceCreatedOn = InvoiceCreatedOn[0];
            //html += '<td style="max-width:60px; vertical-align: middle;" class="center">' + moment(InvoiceCreatedOn).format("DD-MM-YY") + '</td>';
            html += '<td style="max-width:60px; vertical-align: middle;" class="center">' + InvoiceCreatedOn + '</td>'
        }
        else {
            html += '<td style="max-width:60px; vertical-align: middle;" class="center"></td>';
        }
        html += '<td style="max-width: 115px; vertical-align: middle;" class="center">' + arrAirTicket[i].Reservation.InvoiceNo + '</td>';
        html += '<td style="max-width: ; vertical-align: middle;" class="align-left">' + arrAirTicket[i].Agency.AgencyName + '</td>';
        html += '<td style="max-width: ; vertical-align: middle;" class="center">' + arrAirTicket[i].Reservation.AirlineName + ' - ' + arrAirTicket[i].Reservation.PNR + '</td>';
        html += '<td style="min-width: 175px; vertical-align: middle;" class="align-left">';
        var ol = "";
        ol += '<ol>'
        for (var Pax = 0; Pax < arrAirTicket[i].Paxces.length; Pax++) {
            ol += '<li>' + arrAirTicket[i].Paxces[Pax] + '</li>'
        }
        ol += '</ol>'
        var PaxCount = arrAirTicket[i].Paxces.length - 1;
        if (PaxCount != 0) {
            html += '' + arrAirTicket[i].Paxces[0] + '\' x ';
            //html += '<span class="with-tooltip" title="' + ol + '" style="width: 120px;cursor:pointer"><b> ' + PaxCount + ' Pax</b> </span>';
            html += '<b class="info-spot" style="font-size:11px;cursor: pointer;width: 35px;">' + PaxCount + ' Pax<span class="info-bubble" style="width: 200px;">' + ol + '</span></b>';
        }
        else {
            html += '' + arrAirTicket[i].Paxces[0] + '';
        }

        html += '</td>';
        html += '<td style="max-width: ; vertical-align: middle;" class="center">' + arrAirTicket[i].Reservation.AirlineName + '<br>';
        //html += '' + moment(arrAirTicket[i].Reservation.DepartureDate).format("DD-MM-YY") + '<br>';
        var DepartureDate = arrAirTicket[i].Reservation.DepartureDate.split(' ');
        DepartureDate = DepartureDate[0];
        html += '' + DepartureDate + '<br>';
        for (var Seg = 0; Seg < arrAirTicket[i].Segments.length; Seg++) {
            var Origin = arrAirTicket[i].Segments[Seg].OriginAirport.split('(');
            Origin = Origin[1].split(')');
            Origin = Origin[0];
            var Destination = arrAirTicket[i].Segments[Seg].DestinationAirport.split('(');
            Destination = Destination[1].split(')');
            Destination = Destination[0];
            //html += '<span class="with-tooltip" title="' + arrAirTicket[i].Segments[Seg].OriginAirport + '" id="' + i + '_' + Seg + '" onmouseover="RemoveTitle(\'' + i + '_' + Seg + '\', \'' + arrAirTicket[i].Segments[Seg].OriginAirport + '\') " onmouseout="AddTitle(\'' + i + '_' + Seg + '\', \'' + arrAirTicket[i].Segments[Seg].OriginAirport + '\')" style="width: 120px; cursor: pointer">' + Origin + '</span> - ';
            html += '<span class="info-spot" style="font-size:11px;margin-right: 6px;cursor: pointer"">' + Origin + '<span class="info-bubble" style="width: 200px;">' + arrAirTicket[i].Segments[Seg].OriginAirport + '</span></span>&nbsp;-&nbsp;';
            html += '<span class="info-spot" style="font-size:11px;margin-left: 4px;cursor: pointer"">' + Destination + '<span class="info-bubble" style="width: 200px;">' + arrAirTicket[i].Segments[Seg].DestinationAirport + '</span></span><br>';
            //html += '<span class="with-tooltip" title="' + arrAirTicket[i].Segments[Seg].DestinationAirport + '" id="' + i + '_' + Seg + '" onmouseover="RemoveTitle(\'' + i + '_' + Seg + '\', \'' + arrAirTicket[i].Segments[Seg].OriginAirport + '\') " onmouseout="AddTitle(\'' + i + '_' + Seg + '\', \'' + arrAirTicket[i].Segments[Seg].OriginAirport + '\')" style="width: 120px; cursor: pointer">' + Destination + '</span><br>';
        }
        html += '</td > ';
        if (arrAirTicket[i].Reservation.TicketStatus == 'Cancelled') {
            html += '<td style="max-width: 75px; vertical-align: middle;" class="center"><span class="status-cancelled">Cancelled</span></td>';
        } else if (arrAirTicket[i].Reservation.TicketStatus == 'Ticketed') {
            html += '<td style="max-width: 75px; vertical-align: middle;" class="center"><span class="status-vouchered">Ticketed</span></td>';
        } else {
            html += '<td style="max-width: 75px; vertical-align: middle;" class="center"><span class="status-sup-confpending">' + arrAirTicket[i].Reservation.TicketStatus + '</span ></td >';
        }
        html += '<td style="min-width:100px; vertical-align: middle;" class="align-right"><b>S </b> - ' + arrAirTicket[i].Reservation.PublishedFare + '<br> <b>P </b> -' + arrAirTicket[i].Reservation.InvoiceAmount + '</td>';
        //html += '<td >' + arrAirTicket[i].InvoiceAmount + '</td>';
        //html += '<td></td>';
        html += '<td style="max-width: 55px; vertical-align: middle;" class="align-center action-button">'
        html += '<a href="#" class="invoice-btn" title="Click for Invoice" onclick="GetPrintFlightInvoice(\'' + arrAirTicket[i].Reservation.BookingID + '\',\'' + arrAirTicket[i].Reservation.uid + '\',\'' + arrAirTicket[i].Reservation.TicketStatus + '\')">INVOICE</a>'
        if (arrAirTicket[i].Reservation.TicketStatus == "Ticketed") {
            html += '<a href="#" class="voucher-btn"  title="Click for Ticket" onclick="GetPrintTicket(\'' + arrAirTicket[i].Reservation.BookingID + '\',\'' + arrAirTicket[i].Reservation.uid + '\',\'' + arrAirTicket[i].Reservation.TicketStatus + '\')">Ticket</a>'
            html += '<a href="#" class="cancel-btn" title="Click to Cancel" onclick="CancelBooking(\'' + arrAirTicket[i].Reservation.BookingID + '\',\'' + arrAirTicket[i].Reservation.TicketStatus + '\')">Cancel</a>';
        }
        /* else if (arrAirTicket[i].TicketStatus == "Hold" || arrAirTicket[i].TicketStatus == "Cancelled") {
             html += '<a href="#" class="button disabled" title="Cancelled"><span class="icon-pages disabled"></span></a>'
         }
         if (arrAirTicket[i].TicketStatus == "Ticketed") {
             
         }
         else if (arrAirTicket[i].TicketStatus == "Hold") {
             html += '<a href="#" class="button" title="Cancel" onclick="CancelBooking(\'' + arrAirTicket[i].BookingID + '\',\'' + arrAirTicket[i].TicketStatus + '\')"><span class="icon-cross"></span></a>'
         }
         else if (arrAirTicket[i].TicketStatus == "Cancelled") {
             html += '<a href="#" class="button disabled" title="Cancelled"><span class="icon-cross disabled"></span></a>'
         }*/
        html += '</td>';
        //html += '<td>' + arrAirTicket[i].InvoiceAmount + '</td>';
        //html += '<td><a style="cursor:pointer" onclick="GetPrintInvoice(\'' + arrAirTicket[i].BookingID + '\',\'' + arrAirTicket[i].uid + '\',\'' + arrAirTicket[i].TicketStatus + '\')">View</a></td>';

        //if (arrAirTicket[i].TicketStatus == "Ticketed") {
        //    html += '<td><a style="cursor:pointer" onclick="GetPrintTicket(\'' + arrAirTicket[i].BookingID + '\',\'' + arrAirTicket[i].uid + '\',\'' + arrAirTicket[i].TicketStatus + '\')">View</a></td>';

        //}
        //else if (arrAirTicket[i].TicketStatus == "Hold" || arrAirTicket[i].TicketStatus == "Cancelled") {
        //    html += '<td> -  </td>';
        //}

        //if (arrAirTicket[i].TicketStatus == "Ticketed") {
        //    html += '<td><a style="cursor:pointer" onclick="CancelBooking(\'' + arrAirTicket[i].BookingID + '\',\'' + arrAirTicket[i].TicketStatus + '\')" ><span class="glyphicon glyphicon-remove-sign"></span></a></td></tr>';
        //}
        //else if (arrAirTicket[i].TicketStatus == "Hold") {
        //    html += '<td><a style="cursor:pointer" onclick="CancelBooking(\'' + arrAirTicket[i].BookingID + '\',\'' + arrAirTicket[i].TicketStatus + '\')" ><span class="glyphicon glyphicon-remove-sign"></span></a></td></tr>';
        //}
    }
    $("#tbl_AirTicket").append(html)
}

function RemoveTitle(Id, Name) {
    setTimeout(function () {
        RmvTitle(Id, Name);
    }, 100);

}
function AddTitle(Id, Name) {
    $("#" + Id + "").attr("title", "" + Name + "");
}

function RmvTitle(Id, Name) {
    $("#" + Id + "").attr({
        "title": ""
    });
}