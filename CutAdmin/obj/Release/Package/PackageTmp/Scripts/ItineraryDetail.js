﻿var globe_ItineraryCount = 0;
var globe_urlParamDecoded = 0;
var global_CategoryCount = 0;
var global_Itenerary = [];
var hcd;
$(document).ready(function () {
    var tpj = jQuery;
    var HotelCode
    var chkinMonth;
    var chkoutDate;
    var chkoutMonth;
    var id;
    function AutoComplete(id) {
        ;
        var hdncc = tpj('#hdnHCode').val()
        tpj('#' + id).autocomplete({
            source: function (request, response) {
                tpj.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "../Handler/Genralhandler.asmx/GetHotel",
                    data: "{'name':'" + tpj('#' + id).val() + "','destination':'" + tpj('#hdnHCode').val() + "'}",
                    dataType: "json",
                    success: function (data) {
                        var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                        response(result);

                    },
                    error: function (result) {
                        alert("No Match");
                        //alert(tpj('#hdnHCode').val());
                    }
                });
            },
            minLength: 4,
            select: function (event, ui) {
                ;
                HotelCode = ui.item.id;
                GetDescription(id);

            }
        });

    }


    function GetDescription(id) {
        $.ajax({
            type: "POST",
            url: "../Handler/Genralhandler.asmx/GetHotelDescription",
            data: '{"HCode":"' + HotelCode + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                ;
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {

                    var arrAgentList = result.ReservationDetails;

                    //var txtid = id.replace("txt_ItineraryHotelName_", "txt_Itinerary_");

                    //$('#' + txtid).val(arrAgentList[0].HotelFacilities)

                    var txtidplace = id.replace("txt_ItineraryHotelName_", "txt_ItineraryPlace_");

                    $('#' + txtidplace).val(arrAgentList[0].City)
                }


            },
            error: function () {
            }
        });
    };
    $('#hdnHCode').val(hcd);

});
function GetItineraryDetail(globe_urlParamDecoded) {
    globe_urlParamDecoded =$("#hdpackageId").val();
    $.ajax({
        url: "../Handler/PackageDetailHandler.asmx/GetItineraryDetail",
        type: "post",
        data: '{"nID":"' + globe_urlParamDecoded + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.Session == 0) {
                window.location.href = "Default.aspx";
            }
            if (result.retCode == 0) {
                Success("No packages found");
            }
            else if (result.retCode == 1) {
                globe_ItineraryCount = result.List_ItineraryDetail[0].nDuration;
                global_Itenerary = result.List_ItineraryDetail;
                global_CategoryCount = result.nCount;
                CreateItineraryEditors(global_Itenerary);
            }
        },
        error: function () {
        }
    });
}

function GetCategoryName(categoryID) {
    if (categoryID == 1) {
        return "Standard";
    }
    else if (categoryID == 2) {
        return "Deluxe";
    }
    else if (categoryID == 3) {
        return "Premium";
    }
    else if (categoryID == 4) {
        return "Luxury";
    }
}
function GetItineraryVal(Itinerary, tabindex, index) {
    if (index == 1) {
        return Itinerary[tabindex].sItinerary_1.split('^-^');
    }
    else if (index == 2) {
        return Itinerary[tabindex].sItinerary_2.split('^-^');
    }
    else if (index == 3) {
        return Itinerary[tabindex].sItinerary_3.split('^-^');
    }
    else if (index == 4) {
        return Itinerary[tabindex].sItinerary_4.split('^-^');
    }
    else if (index == 5) {
        return Itinerary[tabindex].sItinerary_5.split('^-^');
    }
    else if (index == 6) {
        return Itinerary[tabindex].sItinerary_6.split('^-^');
    }
    else if (index == 7) {
        return Itinerary[tabindex].sItinerary_7.split('^-^');
    }
    else if (index == 8) {
        return Itinerary[tabindex].sItinerary_8.split('^-^');
    }
    else if (index == 9) {
        return Itinerary[tabindex].sItinerary_9.split('^-^');
    }
    else if (index == 10) {
        return Itinerary[tabindex].sItinerary_10.split('^-^');
    }
    else if (index == 11) {
        return Itinerary[tabindex].sItinerary_11.split('^-^');
    }
    else if (index == 12) {
        return Itinerary[tabindex].sItinerary_12.split('^-^');
    }
    else if (index == 13) {
        return Itinerary[tabindex].sItinerary_13.split('^-^');
    }
    else if (index == 14) {
        return Itinerary[tabindex].sItinerary_14.split('^-^');
    }
    else if (index == 15) {
        return Itinerary[tabindex].sItinerary_15.split('^-^');
    }
}

function CreateItineraryEditors(Itinerary) {
    var sTabWidth = 100 / global_CategoryCount;
    var sTabRow = '';
    var sTabRowContent = '';
    var sclass = '';
    var sDisplay = '';
    var count = Itinerary[0].nDuration;
    var ItCount = Itinerary.length - 1;
    //for (var k = 0; k < global_CategoryCount; k++) {
    var sTabID = GetCategoryName(Itinerary[ItCount].nCategoryID);
    //if (k == 0) {
    sTabRowContent += '<div id="tbl_AddPackagePrice' + sTabID + '">';
    for (var i = 0; i < count; i++) {
        var sItineraryArray = GetItineraryVal(Itinerary, ItCount, i + 1);
        if (sItineraryArray[0] == undefined) {
            sItineraryArray[0] = "";
        }
        if (sItineraryArray[1] == undefined) {
            sItineraryArray[1] = "";
        }
        if (sItineraryArray[2] == undefined) {
            sItineraryArray[2] = "";
        }
        sTabRowContent += '<div class="columns">';
        sTabRowContent += '<div class="two-columns twelve-columns-mobile">Day ' + (i + 1) + '</div>';
        sTabRowContent += '<div class="ten-columns twelve-columns-mobile">';
        sTabRowContent += '<textarea name="ckeditor" id="txt_itinerary_' + i + '" >' + sItineraryArray[2] + '</textarea>';
        $("#div_IteneraryTabContent").html(sTabRowContent);
        sTabRowContent += '</div>';
        sTabRowContent += '</div>';
        sTabRowContent += '</div>';
        sTabRowContent += '<script type="text/javascript">' + 'CKEDITOR.replace("txt_itinerary_' + i + '", {height: 200});</script>';
    }
    sTabRowContent += '</div>';
    $("#div_IteneraryTabContent").html(sTabRowContent);
}
function SaveItinerary() {
    ;
    //var global_CategoryCount = 1;
    //for (var k = 0; k < global_CategoryCount; k++) {
    for (var i = 0; i < globe_ItineraryCount; i++) {
        var sHotelName = $("#txt_itineraryHotelName_" + i).val();
        var sPlace = $("#txt_ItineraryPlace_" + i).val();
        var sItinerary = CKEDITOR.instances['txt_itinerary_' + i].getData();
        if (sHotelName == "") {
            $("#txt_itineraryHotelName_" + i).focus();
            alert("Please fill hotel name");
            return false;
        }
        else if (sPlace == "") {
            $("#txt_itineraryPlace_" + i).focus();
            alert("Please fill place");
            return false;
        }
        else if (sItinerary == "") {
            $("#txt_itinerary_" + i).focus();
            alert("Please fill itinerary");
            return false;
        }
    }
    //}
    var singleItinerary;
    var nSuccessCount = 0;
    //for (var k = 0; k < global_CategoryCount; k++) {
    var listItinerary = [];
    for (var i = 0; i < globe_ItineraryCount; i++) {
        var sHotelName = $("#txt_ItineraryHotelName_" + i).val();
        var sPlace = $("#txt_ItineraryPlace_" + i).val();
        var sItinerary = CKEDITOR.instances['txt_itinerary_' + i].getData();
        value = sHotelName + '^-^' + sPlace + '^-^' + sItinerary;
        listItinerary.push(value);
    }
    var globe_urlParamDecoded = $("#hdpackageId").val();
    var jsonText = JSON.stringify({ nID: globe_urlParamDecoded, listItinerary: listItinerary, nCategoryID: global_Itenerary[0].nCategoryID, sCategoryName: GetCategoryName(global_Itenerary[0].nCategoryID) });
    $.ajax({
        url: "../handler/AddPackageHandler.asmx/SaveItinerary",
        type: "post",
        data: jsonText,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.Session == 0) {
                window.location.href = "Default.aspx";
            }
            if (result.retCode == 0) {
                alert("No packages found");
            }
            else if (result.retCode == 1) {
                nSuccessCount = nSuccessCount + 1;
                //if (nSuccessCount == global_CategoryCount) {
                alert("Itinerary Added Successfully, Please add Hotel details to products");
                globe_urlParamDecoded = $("#hdpackageId").val();
                HotelDetails(globe_urlParamDecoded);
                //}
            }
        },
        error: function () {
            alert('Error in saving, please try again!');
        }
    });
    //}
}

