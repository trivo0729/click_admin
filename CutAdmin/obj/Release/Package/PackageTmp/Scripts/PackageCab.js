﻿var r = 1;
function AddRow(Id) {
    var html = '';
    html += '<div class="columns" id="row' + Id + '">'
    html += '<div class="three-columns twelve-columns-mobile">'
    html += '<label>Vehicle</label>'
    html += '<br>'
    html += '<div class="full-width button-height" id="Divselect_Vehicle' + Id + '">'
    html += '    <select id="select_Vehicle' + Id + '" name="validation-select" style="width: 250px" class="select">'
    for (var i = 0; i < VehicleName.length; i++) {
        html += '<option value="' + VehicleName[i].ID + '">' + VehicleName[i].VehicleModel + '</option>';
    }
    html += '</select>'
    html += '</div>                                                                                                                                    '
    html += '</div>                                                                                                                                    '
    html += '<div class="three-columns">'
    html += '<label>Price :</label>'
    html += '<div class="input full-width">'
    html += '    <input value="" id="txt_Price' + Id + '" class="input-unstyled full-width" placeholder="0.00" type="text">'
    html += ' </div>'
    html += '</div>'
    html += '<div class="one-column">'
    html += '    <i aria-hidden="true" onclick="Validation(' + (Id + 1) + ')" id="ColPlus_' + Id + '">'
    html += '        <label for="pseudo-input-2" class="button anthracite-gradient" style="margin-top: 21px;"><span class="icon-plus"></span></label>'
    html += '    </i>'
    html += '    <i aria-hidden="true" onclick="RemoveRow(' + Id + ')"><label for="pseudo-input-2" class="button anthracite-gradient" style="margin-top: 21px;"><span class="icon-minus"></span></label></i>'
    html += '</div>'
    html += '</div>'
    $("#RowTransfer").append(html);
    $("#ColPlus_" + (Id - 1)).hide();
    r++;
}

function RemoveRow(Id) {
    $("#row" + Id).remove();
    $("#ColPlus_" + (Id - 1)).show();
    r = (r - 1);
}

function Validation(Id) {
    var bValid = true;
    var VehicleName = $("#select_Vehicle" + (Id - 1)).val();
    var Price = $("#txt_Price" + (Id - 1)).val();
    if (VehicleName == "") {
        Success("Please Select Vehicle")
        bValid = false;
    }
    if (Price == "") {
        Success("Please Insert Price")
        bValid = false;
    }
    if (bValid == true) {
        AddRow(Id);
    }
}

var ArrTransfer = new Array();

function AddPackageCab() {
    ArrTransfer = new Array()
    for (var i = 0; i < r; i++) {
        var ModelId = $("#select_Vehicle" + i).val();
        var VehicleName = $("#select_Vehicle" + i + " option:selected").text();
        var Price = $("#txt_Price" + i).val();
        var PackageId = $("#hdpackageId").val();
        if ($("#select_Vehicle" + i).val() == "") {
            Success("Please Select Vehicle");
            return false
        }
        if ($("#txt_Price" + i).val() == "") {
            Success("Please Insert Vehicle");
            return false
        }
        ArrTransfer.push({
            VehicleName: VehicleName,
            Price: Price,
            PackageId: PackageId,
            ModelId: ModelId,
        });
    }
    var data = { ArrTransfer: ArrTransfer };
    $.ajax({
        type: "POST",
        url: "../Handler/AddPackageHandler.asmx/AddTransferDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {
                window.location.href = "Default.aspx";
            }
            if (result.retCode == 1) {
                Success("Transfer details added successfully , Please add package images to products");
                Success("Transfer details added successfully");
                globe_urlParamDecoded = $("#hdpackageId").val();
                $('input[id$=hidden_Filed_ID]').val(globe_urlParamDecoded);
                GetCurrentImages(globe_urlParamDecoded);
            }
            else {

            }
        },
        error: function () {
            Success("errors")
        }
    });
}


function GetTransfer() {
    var Id = $("#hdpackageId").val();
    var data = { Id: Id };
    if (Id != "") {
        $.ajax({
            type: "POST",
            url: "../Handler/PackageDetailHandler.asmx/GetTransfer",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    var Arr = result.Arr;
                    for (var i = 0; i < Arr.length; i++) {
                        if (i == 0) {
                            $("#Divselect_Vehicle" + i + " .select span")[0].textContent = Arr[i].VehicleName;
                            $("#select_Vehicle" + i).val(Arr[i].ModelId);
                            $("#txt_Price" + i).val(Arr[i].Price);
                            $("#RowTransfer").empty();
                            r = 1;
                        }
                        else {
                            AddRow(i);
                            $("#Divselect_Vehicle" + i + " .select span")[0].textContent = Arr[i].VehicleName;
                            $("#select_Vehicle" + i).val(Arr[i].ModelId);
                            $("#txt_Price" + i).val(Arr[i].Price);
                        }
                    }
                }
            },
            error: function () {
                alert("An error occured !!!");
            }
        });
    }

}
