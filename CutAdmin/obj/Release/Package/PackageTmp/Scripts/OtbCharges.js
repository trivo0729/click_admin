﻿var bvalid;
function GetAirportsList() {
    var Airline = $("#Sel_AirLineIn").val();
    $("#Sel_ArivalFrom").empty();
    var Airports = $("#Sel_AirLineIn option:selected").text();
    $.ajax({
        type: "POST",
        url: "../Handler/OTBHandler.asmx/GetAirports",
        data: '{ "Airlines": "' + Airports + '" }',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrAirports = result.tbl_Airports;

                var DeprtureFromOption;
                DeprtureFromOption += ' <option selected="selected" value="-">---Select Airports----</option>'
                for (var i = 0; i < arrAirports.length; i++) {
                    DeprtureFromOption += '<option value="' + arrAirports[i].CityCode + '">' + arrAirports[i].Origin + '</option>'
                }
                $("#Sel_ArivalFrom").append(DeprtureFromOption);
            }
            if (result.retCode == 0) {

            }
        },
        error: function () {
            //Success("An error occured while loading Visa")
        }
    });
    //$('#selCity option:selected').val(tempcitycode);
}
function GetOtbCharges() {
    var Airline = $("#Sel_AirLineIn").val();
    var From = $("#Sel_ArivalFrom ").val();
    var Airports = $("#Sel_AirLineIn option:selected").text();
    $.ajax({
        type: "POST",
        url: "../Handler/OTBHandler.asmx/GetOTBCharges",
        data: '{ "Airlines": "' + Airline + '","From": "' + From + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrAirports = result.tbl_OTB;

                for (var i = 0; i < arrAirports.length; i++) {
                    //$("#selCurrencyType").val(arrAirports[0].Currency);
                    //$("#selCurrencyType").append(div);
                    $("#selCurrencyType").val(arrAirports[0].Currency);
                    $("#DivCurrency .select span")[0].textContent = arrAirports[0].Currency;
                    $("#txtOtbFee").val(arrAirports[0].Charge);
                    $("#txtOtherFee").val(arrAirports[0].UrgentChg);
                }
            }
            if (result.retCode == 0) {

            }
        },
        error: function () {
        }
    });
}
function UpdateCharges() {
    $("#alSuccess1").css("display", "none")
    bvalid = Validate_Charges();
    if (bvalid == true) {
        var Airline = $("#Sel_AirLineIn").val();
        var From = $("#Sel_ArivalFrom ").val();
        var Currency = $("#selCurrencyType").val();
        var OtbFee = $("#txtOtbFee ").val();
        var Urgent = $("#txtOtherFee").val();
        $.ajax({
            type: "POST",
            url: "../Handler/OTBHandler.asmx/UpdateOTBCharges",
            data: '{"Airlines":"' + Airline + '","From":"' + From + '","Currency":"' + Currency + '","OtbFee":"' + OtbFee + '","UrgentFee":"' + Urgent + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Otb Chargest Updated successfully!");
                    //GetCharges()
                }
                if (result.retCode == 0) {

                }
            },
            error: function () {
                Success("An error occured while loading Visa Charges")
            }
        });
    }
}
function Validate_Charges() {
    if ($("#Sel_AirLineIn").val() == "-") {
        $("#Sel_AirLineIn").focus();
        Success("Please Select AirLine");
        return false;
    }
    
    if ($("#Sel_ArivalFrom").val() == "-") {
        Success("Please Select Arrival From");
        return false;
    }
   
    if ($("#selCurrencyType").val() == "-") {
        Success("Please Select Currency Type");
        return false;
    }
    
    if ($("#txtOtbFee").val() == "") {
        Success("Please enter OTB Fee");
        return false;
    }
   
    return true;
}
function Closed() {
    $("#alSuccess1").css("display", "none")
}
//function MailDilog() {
//    $("#MailModal").modal("show")
//}
function GetOtbMails() {
    var Airline = $("#Sel_AirLineInMail").val();
    $(".multiple_emails-ul").empty();
    $.ajax({
        type: "POST",
        url: "../Handler/OTBHandler.asmx/GetOtbMails",
        data: '{ "Airlines": "' + Airline + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrAirports = result.tbl_Airports;
                $("#txt_AirlinesMailId").val(arrAirports[0].Email1)
                var CCmails = arrAirports[0].CCEmailId.split(';')
                var li = ''
                if (CCmails != "") {
                    //for (var i = 0; i < (CCmails.length) ; i++) {
                    //    li += '<li class="multiple_emails-email" onclick="RemoveLi(this)"><a style="cursor:pointer"   class="multiple_emails-close" title="Remove"><i class="glyphicon glyphicon-remove"></i></a><span class="email_name" data-email="' + CCmails[i] + '">' + CCmails[i] + '</span></li>'
                    //}

                    //$(".multiple_emails-ul").append(li)
                    for (var i = 0; i < CCmails.length ; i++) {
                        li += '<li class="multiple_emails" onclick="RemoveLi(this)" ><a style="cursor:pointer"  class="multiple_emails-close" title="Remove"><i class="icon-cross"></i></a><span class="email_name">' + CCmails[i] + '</span></li>'
                    }
                    $("#CcMailList").append(li)
                }

            }
            if (result.retCode == 0) {

            }
        },
        error: function () {
        }
    });
}
//var Mails = [];
function AddMails() {

    var Airline = $("#Sel_AirLineInMail").val();
    var AirlinesMailId = $("#txt_AirlinesMailId").val();
    //Mails = $("#example_emailSUI").val();
    //Mails = Mails.split('"');
    var CcMails = "";
    var Mails = document.getElementsByClassName('email_name'), i;
    for (i = 0; i < Mails.length; i += 1) {
        if (i != (Mails.length - 1)) {
            CcMails += Mails[i].textContent + ";";
        }
        else {
            CcMails += Mails[i].textContent;
        }

    }
    $.ajax({
        type: "POST",
        url: "../Handler/OTBHandler.asmx/UpdateOTBMails",
        data: '{ "Airlines": "' + Airline + '","Mails": "' + AirlinesMailId + '","CCMails": "' + CcMails + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Success("Airlines Mail Updated Successfully..")
                $("#Sel_AirLineInMail").val("-");
                $(".multiple_emails-ul").empty();
            }
            if (result.retCode == 0) {

            }
        },
        error: function () {
        }
    });
}
function RemoveLi(link) {
    link.remove();
}


function MailDilog() {
    $.modal({
        content: '<div class="columns" id="VisaModal">' +
                    '<div class="twelve-columns twelve-columns-mobile" id="divForVisa">' +
                       '<label>AirLines</label>' +
                    '<div class="full-width button-height Groupmark">' +
                        '<select id="Sel_AirLineInMail" class="select" onchange="GetOtbMails()">' +
                            '<option value="-" selected="selected">-Select AirLine-</option>' +
                                '<option value="G9">Air Arabia</option>' +
                                '<option value="AI">Air India</option>' +
                                '<option value="Ix">Air India Express</option>' +
                                '<option value="9w" id="OptJet">Jet Airways Airlines</option>' +
                                '<option value="6E">Indigo Airlines </option>' +
                               ' <option value="Wy">Oman Airways</option>' +
                                '<option value="SG">Spicejet Airlines</option>' +
                                '<option value="EK" id="OptEmirates">Emirates Airlines</option>' +
                                '<option value="EY">Etihad Airways</option>' +
                                '<option value="FZ">Fly Dubai</option>' +
                        '</select>' +
                    '</div>' +
                    '</div>' +
                    '<div class="twelve-columns twelve-columns-mobile">' +
                        '<label>Airline Mail Id: </label><br/> ' +
                        '<div class="input full-width">' +
                            '<input name="prompt-value" id="txt_AirlinesMailId" value="" class="input-unstyled full-width"  placeholder="Mail Id" type="text">' +
                        '</div>' +
                    '</div>' +
                    '<div class="updatemail">' +
                        '<label>  CC Mail Id : </label><br/> ' +
                        '<ul id="CcMailList">' +
                        '</ul>' +
                        '<input type="text" id="example_emailSUI" name="mail" style="border:none; margin-left:10px; margin-bottom:2px" onblur="AddCcMail()" placeholder="Enter Cc Mail here" >' +
                    '</div>' +
                '</div>' +
                '<p class="text-right" style="text-align:right;">' +
                    '<button id="btn_RegiterAgent" type="button" class="button anthracite-gradient" onclick="AddMails()">Add</button>' +
                '</p>',
        title: 'Update Airlines Mail ',
        width: 500,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
    
}

var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
function AddCcMail() {
    var NewCcMail = $('#example_emailSUI').val();
    if (!NewCcMail) {
        return false;
    }
    if (!emailReg.test(NewCcMail)) {
        bValid = false;
        $("example_emailSUI").focus();
        Success("Please Insert Valid Mail Id");
        $('#example_emailSUI').val('');
        return false;
    }
    var ccli = '<li class="multiple_emails" onclick="RemoveLi(this)"><a style="cursor:pointer" class="multiple_emails-close" title="Remove"><i class="icon-cross"></i></a><span class="email_name">' + NewCcMail + '</span></li>'
    $('#CcMailList').append(ccli);
    $('#example_emailSUI').val('');
}

function ResetControls() {
    $("#Sel_AirLineIn").val('');
    $("#Sel_ArivalFrom").val('');
    $("#selCurrencyType").val('');
    $("#txtOtbFee").val(0.00);
    $("#txtOtherFee").val(0.00);
}