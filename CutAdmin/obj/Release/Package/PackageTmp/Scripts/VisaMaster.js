﻿//var $ = jQuery;
//$.noConflict();

$(document).ready(function () {

    $("#all").change(function () {
        $('.Spec:enabled').prop('checked', $(this).prop("checked"));
        if (all.checked == true) {
            $("#MultiPuleStatusModal").modal("show")
        }
    });
    $("#txt_Fdate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#txt_Todate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
   
    $("#txt_RefNo").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../Handler/VisaDetails.asmx/GetVisaCode",
                data: "{'Vcode':'" + document.getElementById('txt_RefNo').value + "'}",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    response(result);
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        },
        minLength: 3,
    });
    $("#txt_AppName").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../Handler/VisaDetails.asmx/GetVisaApplicationName",
                data: "{'Name':'" + document.getElementById('txt_AppName').value + "'}",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    response(result);
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        },
        minLength: 3,
    });
    $("#txt_DocNo").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../Handler/VisaDetails.asmx/GetVisaDocumentNumber",
                data: "{'Number':'" + document.getElementById('txt_DocNo').value + "'}",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    response(result);
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        },
        minLength: 3,
    });
    $("#txt_AgentName").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../Handler/VisaDetails.asmx/GetAgencyName",
                data: "{'AgencyName':'" + document.getElementById('txt_AgentName').value + "'}",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    response(result);
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            $('#agentid').text(ui.item.id);
        }
    });
});
function GetDocuments(VisaCode) {
    var tRow = '';
    var tRow2 = '';
    tRow += '<tr>';
    for (var i = 0; i < 3; i++) {
        tRow += '<td style="width:33.33%"><img src="../VisaImages/' + VisaCode + "_" + (i + 1) + '.jpg" class="wrapper" width="25%" height="190px" style="box-shadow: 0 0 5px"  onerror="imgError(this);" /></td>';
    }
    tRow += '</tr>';
    tRow2 += '<tr>'
    for (var i = 3; i < 6; i++) {
        tRow2 += '<td style="width:33.33%"><img src="../VisaImages/' + VisaCode + "_" + (i + 1) + '.jpg" class="wrapper" width="25%" height="190px" style="box-shadow: 0 0 5px"  onerror="imgError(this);" /></td>';
    }
    tRow2 += '</tr>';
    $("#tbl_CurrentImages1 tbody").html(tRow);
    $("#tbl_CurrentImages2 tbody").html(tRow2);
    $('#DocumentsModal').modal("show")
}
function GetCurrentImages(VisaCode) {
    debugger;
    var tRow = '';
    var tRow2 = '';
    tRow += '<tr>';
    for (var i = 0; i < 3; i++) {
        tRow += '<td style="width:33.33%"><img src="../VisaImages/' + VisaCode + "_" + (i + 1) + '.jpg" class="wrapper" width="25%" height="190px" style="border:1px solid #000000"  onerror="imgError(this);" /></td>';
    }
    tRow += '</tr>';
    tRow2 += '<tr>'
    for (var i = 3; i < 6; i++) {
        tRow2 += '<td style="width:33.33%"><img src="../VisaImages/' + VisaCode + "_" + (i + 1) + '.jpg" class="wrapper" width="25%" height="190px" style="border:1px solid #000000"  onerror="imgError(this);" /></td>';
    }
    tRow2 += '</tr>';
    $("#tbl_CurrentImages3 tbody").html(tRow);
    $("#tbl_CurrentImages4 tbody").html(tRow2);
    AddVisaDetails(VisaCode)
}
function DownloadVisa(VisaCode) {

    var imgVisa;
    var url = "../VisaImages/" + VisaCode + "_Visa.pdf";
    var tRow;
    $('#Visa tbody').empty();
    var tRow;
    tRow = '<iframe src="../VisaImages/' + VisaCode + '_Visa.pdf" width="100%" height="500px"  />';
    $('#preview').append(tRow)
    $("#Visa tbody").append(tRow);
    $('#VisaAttacmentModal').modal("show");
    //$.get(url, function () {
    //    $('#Visa tbody').empty();
    //    var tRow;
    //    tRow = '<iframe src="../VisaImages/' + VisaCode + '_Visa.pdf" width="100%" height="500px"  />';
    //    $('#preview').append(tRow)
    //    $("#Visa tbody").append(tRow);
    //    $('#VisaAttacmentModal').modal("show");
    //}).error(function () { alert('Visa Not Found!!'); });
    //imgVisa +='<tr>'
    //imgVisa = '<td style="width:33.33%"><img src="../VisaImages/pdf.jpg" class="wrapper" width="25%" height="190px" style="border:1px solid #000000"  onerror="imgError(this);" /><a id="Download' + (i + 1) + '" style="padding-left: 40%;" href="../VisaImages/' + VisaCode + '_Visa.pdf" download>Download</a></td>';
    //imgVisa += '</tr>'

}
function imgError(image) {
    image.onerror = "";
    $("#tbl_CurrentImages3 thead").html('')
    $("#tbl_CurrentImages4 tbody").html('<tr><td align="center">Documents not Available </td></tr>');
    $("#tbl_CurrentImages1 thead").html('');
    $("#tbl_CurrentImages2 thead").html('');
    $("#tbl_CurrentImages4 thead").html('')
    $("#tbl_CurrentImages2 tbody").html('<tr><td align="center">Documents not Available </td></tr>');
    $("#tbl_CurrentImages1 tbody").html('<tr><td align="center">Documents not Available </td></tr>');
    $("#tbl_CurrentImages3 tbody").html('<tr><td align="center">Documents not Available </td></tr>')
    return true;
}
function ViewVisaApplication(Vcode, Eno) {
    //window.open('../Admin/VisaApplicationDetails.html?RefNo=' + Vcode)

    window.open('VisaApplicationView.aspx?RefNo=' + Vcode)
}
function EditVisa(Vcode) {
    window.open('AddVisaDetails.aspx?Vcode=' + Vcode)
}
