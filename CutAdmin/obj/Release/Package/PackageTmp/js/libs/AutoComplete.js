﻿var arrData = new Array();
function AutoSelect(ID, data, URL, successfunction) {
    debugger
    $("#" + ID).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: URL,
                data: JSON.stringify({ name: $("#" + ID).val() }),
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    response(result);
                },
                error: function (result) {
                    alert("No Match");
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            successfunction(ui.item.id)
        }
    });
}