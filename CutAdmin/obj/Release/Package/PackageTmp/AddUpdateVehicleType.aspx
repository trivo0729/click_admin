﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddUpdateVehicleType.aspx.cs" Inherits="CutAdmin.AddUpdateVehicleType" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/VehicleType.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>Vehicle Type</h1>
            <hr />
        </hgroup>

        <div class="with-padding">
            <div class="columns">
                <div class="twelve-columns six-columns-mobile" style="text-align:right">
                    <button type="button"  class="button compact anthracite-gradient" onclick="NewVehicleType()">New Vehicle Type</button></div>
                <%--<div class="six-columns  six-columns-mobile text-alignright selCurrency">
                    <div class="full-width button-height">
                        <select id="selCurrency" class="select" onchange="GetPackages()">
                            <option selected="selected" value="USD">USD</option>
                            <option value="INR">INR</option>
                            <option value="AED">AED</option>
                        </select>
                    </div>
                </div>--%>
            </div>

            <div class="respTable">
                <table class="table responsive-table font11" id="tbl_VehicleType">

                    <thead>
                        <tr>
                            <th class="align-center" scope="col">Sr.No</th>
                            <th class="align-center" scope="col">Vehicle Type Name</th>
                            <th class="align-center" scope="col">Action</th>
                            <%--<th scope="col">Edit</th>
                            <th scope="col">Delete</th>--%>
                        </tr>
                    </thead>


                    <tbody>
                       

                    </tbody>

                </table>

            </div>
        </div>

    </section>
    <script>

     
        // New Pakages modal
        function NewVehicleType() {
            $.modal({
                content: '<div class="modal-body" id="VehicleTypeModal">' +
				'<div class="columns">' +
'<div class="Twelve-columns twelve-columns-mobile"><label>Vehicle Type Name<span class="red">*</span>:</label><div class="input full-width">' +
'<input name="prompt-value" id="txt_Name" placeholder="Vehicle Type Name" value="" class="input-unstyled full-width"  type="text">' +
'</div></div>' +
'</div>' +
'<p class="text-alignright"><input type="button" value="Add" onclick="AddVehicleType();" title="Submit Details" class="button anthracite-gradient"/></p>' +
'</div>',

                title: 'Add Vehicle Type',
                width: 500,
                scrolling: true,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'huge anthracite-gradient displayNone',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: true
            });
        };


        // deletTrush modal
        function deletTrush() {
            $.modal({
                content: '<p class="avtiveDea">Are you sure you want to Delete this package</strong></p>' +
'<p class="text-alignright text-popBtn"><button type="submit" class="button anthracite-gradient">OK</button></p>',


                width: 300,
                scrolling: false,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'anthracite-gradient glossy',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: false
            });
        };
       
    </script>
</asp:Content>

