﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="HotelsMapping.aspx.cs" Inherits="CutAdmin.HotelsMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/libs/jquery-1.10.2.min.js"></script>
  
    <script src="Scripts/HotelMapping.js?V=1.1"></script>

    <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <!-- DataTables -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <%--<style type="text/css">
        .pageNumber {
            padding: 2px;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: auto;
        }
    </style>--%>



    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <hgroup id="main-title" class="thin">
            <h3>Hotels Mapping</h3>
            <hr />
        </hgroup>

        <div class="with-padding ">
            <div class="table-header white-gradient">
                <div class="columns">
                    <div class="three-columns">
                        <input type="text" id="txtCity" placeholder="Search with City" class="input  ui-autocomplete-input full-width">
                    </div>
                    <div class="three-columns">
                        <input type="text" id="txtHotelName" placeholder="Search with Hotel" class="input  ui-autocomplete-input full-width">
                    </div>
                    <div class="three-columns">
                        <input type="text" id="txtfdate" placeholder="From Date" class="input  ui-autocomplete-input full-width">
                    </div>
                    <div class="three-columns">
                        <input type="text" id="txtldate" placeholder="To Date" size="30" class="input ui-autocomplete-input full-width">
                    </div>
                    <hr />
                    <div class="new-row three-columns" style="float: right">
                        <input id="hdnDCode" hidden="hidden" value="DXB" />
                        <button type="button" class="button  glossy" id="btn_GetCityCode" onclick="GetHotelDetails();" style="float: right">Search</button>
                    </div>
                </div>

                <%--Nationality:
				<select name="select90"  id="Select_Nationality" class="select blue-gradient glossy mid-margin-left">
                    <option selected="selected" value="-" >India</option>
				</select>--%>
            </div>

            <table class="table responsive-table" id="tbl_GetHotelMapping" width="100%">
                <thead>
                    <tr>

                        <th align="center" scope="col">Supplier </th>
                        <th align="center" scope="col">Hotel Name</th>
                        <th align="center" scope="col">Ratings</th>
                        <th align="center" scope="col">Address</th>
                        <th align="center" scope="col">Select</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>

            </table>
            <hr />
            <%--<img src="loader.gif" style="-webkit-user-select: none; display: none; padding-left: 20%; padding-right: 20%" id="img_file_attach" alt="" />--%>

            <div class="table-footer white-gradient">
                <button type="button" class="button glossy mid-margin-right anthracite-gradient float-right" id="btn_Map" onclick="MapHotel();">Map Selected Hotel</button>
                <br />
                <br />
            </div>


        </div>

        <input id="MhotelID" hidden="hidden" />

    </section>
    <!-- End main content -->

    <script>
                          $(function myfunction() {
                              $("#txtCity").autocomplete({
                                  source: function (request, response) {
                                      jQuery.ajax({
                                          type: "POST",
                                          contentType: "application/json; charset=utf-8",
                                          url: "HotelHandler.asmx/GetDestinationList",
                                          data: "{'name':'" + document.getElementById('txtCity').value + "'}",
                                          dataType: "json",
                                          success: function (data) {
                                              var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                                              response(result);
                                          },
                                          error: function (result) {
                                              alert("No Match");
                                          }
                                      });
                                  },
                                  minLength: 3,
                                  select: function (event, ui) {
                                      jQuery('#hdnDCode').val(ui.item.id);
                                  }
                              });
                          })
    </script>
</asp:Content>
