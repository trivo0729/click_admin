﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddTourMode.aspx.cs" Inherits="CutAdmin.AddTourMode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/TourMode.js?v=1.0"></script>

    <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
     <link href="css/styles/switches.css?v=1" rel="stylesheet" />
  
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <!-- Main content -->
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <hgroup id="main-title" class="thin">
                <h3>Add Tour Mode</h3>
                <hr />
            </hgroup>

            <div class="with-padding">
            <%--<h4 class="wrapped white" style="background-color: #006699;">Add Tour Mode </h4>--%>

            <div class="columns">

                <%-- <div class="new-row twelve-columns" style="margin-bottom:-2px">
                                            <h3 class="thin underline green">Add Tour Mode</h3>
                                        </div>--%>

                <div class="new-row four-columns">
                    <input type="text" name="Text[]" id="Fname" placeholder="Tour Mode Name" class="input full-width margin-left">
                </div>

                <div class="four-columns">
                    <input id="Imges" class="input full-width margin-left" type="file" multiple />
                </div>


                <div class="four-columns">
                    <input type="button" class="button anthracite-gradient glossy margin-left" id="btn_Supplier" value="Save" onclick="SaveMode()" />
                </div>

            </div>


            <table class="table responsive-table font11" id="tbl_Getmode">

                <thead>
                    <tr>

                        <th align="center" scope="col">S.N </th>
                        <th align="center" scope="col">Tour Mode </th>
                        <th align="center" scope="col">Status</th>
                        <th align="center" scope="col">Action</th>

                    </tr>
                </thead>

            </table>
            <%--<div  class="table-footer ">
                 <button type="button" class="button glossy mid-margin-right float-right" id="btn_Map" onclick="MapCities();">Map Selected Cities</button>
			<br /><br />
			</div>--%>
        </div>

    </section>
    <!-- End main content -->


</asp:Content>
