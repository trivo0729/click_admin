﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="mapcity.aspx.cs" Inherits="CutAdmin.mapcity" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyDLvhAriXSRStAxraxjCp1GtClM4slLh-k"></script>
    <script src="Scripts/GoogleMap.js"></script>
     <script src="Scripts/MapLocation.js?v=2.4"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Map City</h1>
             <hr />
            <h2><a style="cursor:pointer" class="addnew with-tooltip" title="Map City"><i class="fa fa-plus-circle"></i></a></h2>
              <br />
            <div class="with-padding anthracite-gradient"  id="filter" style="display:none">
                 <form class="form-horizontal">
             <div class="columns">
                <input type="hidden" id="hdnlatitude" />
                <input type="hidden" id="hdnlongitude" />
                <input type="hidden" id="hdnPlaceid" />
                <input type="hidden" id="hdCountryCode" />
                <input type="hidden" id="hdnCountryName" />
                <input type="hidden" id="hdnCode" />
                <input type="hidden" id="Area_Name" />
                 
                <div class="four-columns">
                    <span class="text-left">Location:</span>
                    <br>
                    <input type="text" id="txtSource" class="input full-width pointer" placeholder="Location" autocomplete="off" />
                </div>
                <div class="three-columns">
                    <span class="text-left">Code:</span>
                    <br>
                    <input id="txtCountryCode" class="input full-width" type="text">
                </div>
            </div>
             <div class="columns hidden div_SP">
                        <div class="four-columns" id="div_DOT1">
                            <span class="text-left">DOTW:</span>
                       <%--     <input type="radio" id="DOTWradio" name="radio" onclick="GetCode(4)" />--%>
                            <br>
                            <select id="selDOT1" class="full-width OfferType" multiple="multiple">
                            </select>
                        </div>

                        <div class="four-columns" id="div_MGH1">
                            <span class="text-left">Expedia:</span>
                          <%--  <input type="radio" id="MGHWradio" name="radio" onclick="GetCode(5)" />--%>
                            <br>
                            <select id="selMGH1" class="full-width OfferType" multiple="multiple">
                            </select>
                        </div>

                        <div class="four-columns" id="div_GRN1">
                            <span class="text-left">GRN:</span>
                            <%--<input type="radio" id="GRNradio" name="radio" onclick="GetCode(6)" />--%>
                            <br>
                            <select id="selGRN1" class="full-width OfferType" multiple="multiple">
                            </select>
                        </div>


                        


                    </div>
             <div class="columns  hidden div_SP">


                        <div class="four-columns" id="div_GTA1">
                            <span class="text-left">GTA:</span>
                         <%--   <input type="radio" id="GTAradio" name="radio" onclick="GetCode(1)" />--%>
                            <br>
                            <select id="selGTA1" class="full-width OfferType" multiple="multiple">
                            </select>
                        </div>

                        <div class="four-columns" id="div_HB1">
                            <span class="text-left">HB:</span>
                         <%--   <input type="radio" id="HBradio" name="radio" onclick="GetCode(2)" checked />--%>
                            <br>
                            <select id="selHB1" class="full-width OfferType" multiple="multiple">
                            </select>  <%--//onchange="BindCode(this.value)"--%>
                        </div>

                        <div class="four-columns" id="div_TBO1">
                            <span class="text-left">TBO:</span>
                          <%--  <input type="radio" id="TBOradio" name="radio" onclick="GetCode(3)" />--%>
                            <br>
                            <select id="selTBO1" class="full-width OfferType" multiple="multiple">
                            </select>
                        </div>


                    </div>
             <a class="button  glossy blue-gradient float-right" id="btn_save" onclick="SaveCityLocation()">Save</a>
             <input type="reset" class="button  glossy red-gradient float-right" value="Reset"/>
                     </form>
             <div class="clearfix"></div>
        </div>
        </hgroup>
         
        <div class="with-padding">

            <div class="respTable">

                    <table class="table responsive-table font11" id="tbl_AreaGroup">
                        <thead>
                            <tr>
                                <th scope="col" class="align-center">Sr.No.</th>
                                <th scope="col" class="align-center">Destination Code</th>
                                <th scope="col" class="align-center">City Name</th>
                                <th scope="col" class="align-center">Country</th>

                                <th scope="col" class="align-center">GTA</th>
                                <th scope="col" class="align-center">HB</th>
                                <th scope="col" class="align-center">DOTW</th>
                                <th scope="col" class="align-center">Expedia</th>
                                <th scope="col" class="align-center">TBO</th>
                                <th scope="col" class="align-center">GRN</th>
                                <th scope="col" class="align-center">Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>
        </div>


    </section>
    <script>
        jQuery(document).ready(function () {
            jQuery('.fa-plus-circle').click(function () {
                jQuery(' #filter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #filter').slideUp();
                });
            });
        });
    </script>
</asp:Content>
