﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ViewPackage.aspx.cs" Inherits="CutAdmin.ViewPackage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/ViewPackage.js?v=1.2"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>Package Details</h1>
            <hr />
        </hgroup>

        <div class="with-padding">
            <div class="respTable">
                <table class="table responsive-table font11" id="tbl_PackageDetails">

                    <thead>
                        <tr>
                            <th scope="col" class="align-center">Package Name</th>
                            <th scope="col" class="align-center hide-on-mobile">City </th>
                            <th scope="col" class="align-center">Duration</th>
                            <th scope="col" class="align-center">Category </th>
                            <th scope="col" class="align-center">Validity From</th>
                            <th scope="col" class="align-center">Validity Upto</th>
                            <th scope="col" class="align-center">Status</th>
                           <%-- <th scope="col" class="align-center">Detail</th>
                            <th scope="col" class="align-center">Delete</th>--%>
                            <th class="align-center" scope="col">Action</th>
                        </tr>
                    </thead>


                    <tbody>
                       <%-- <tr>
                            <td>Srilanka</td>
                            <td>Kandy, Sri Lanka </td>
                            <td>8 Days</td>
                            <td>Standard</td>
                            <td>31-07-2018</td>
                            <td>30-11-2018</td>
                            <td class="align-center"><a href="AddPackage.html" class="button" title="Update"><span class="icon-publish "></span></a></td>
                            <td class="align-center"><span class="button-group children-tooltip">
                                <a href="#" class="button" title="trash" onclick="deletTrush();"><span class="icon-trash"></span></a></span></td>
                        </tr>--%>
                       
                    </tbody>

                </table>
            </div>

        </div>

    </section>
    <!-- End main content -->
</asp:Content>
