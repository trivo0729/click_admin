﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="offermanager.aspx.cs" Inherits="CutAdmin.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script src="js/libs/AutoComplete.js?v=1.5"></script>
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
    <!-- inut Js -->
    <script src="js/developr.input.js"></script>
    <!-- JavaScript at bottom except for Modernizr -->
    <script src="js/libs/modernizr.custom.js"></script>
    
    <!-- Date Picker -->
    <link href="js/libs/glDatePicker/developr.css" rel="stylesheet" />
    <link href="js/libs/glDatePicker/developr.fixed.css" rel="stylesheet" />
    <script src="js/libs/fullcalendar/moment.js"></script>
     <script src="Scripts/Rooms.js?v=2"></script>
    <script src="Scripts/offerManager.js?v=1.4"></script>
    <script src="Scripts/Exchange.js?v=1.3"></script>
</asp:Content>
   
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
       <hgroup id="main-title" class="thin">
            <h3 class="font24"><b>Promotion Manager</b></h3>
            <hr />
        </hgroup>

        <div class="with-padding">            
            <!-- Form element with wizard class -->
            <form method="post" action="#" class="block margin-bottom wizard same-height" id="">
                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Offers</legend>                   
                    <div class="field-block button-height">
                       <%-- <small class="input-info">Select one of the offer type you wish to apply</small>--%>
                        <label for="" class="label"><b>Offer Type</b></label>
                        <div class="columns">
                            <%-- Early Booking --%>
                            <div class="three-columns twelve-columns-mobile offercard strong-box-shadow offerDIV pointer" id="div_Early" onclick="fnChangeBorder(this)">
                                <div class="with-padding green-bg" style="border-bottom: 5px solid #6e8a00">
                                    <p></p>
                                    <p class="icon-like align-center icon-size4"></p>
                                    <br />
                                    <p class="align-center font18 strong">Early Booking</p>
                                </div>
                                <div class="with-mid-padding align-center" <%--style="min-height: 60px"--%>>
                                    <p>Here you can give discount or change entire rate if the booking is done in advance</p>
                                </div>
                                 <input type="radio" name="offer" id="rdb_Early" class="hidden" />
                            </div>
                            <%-- Free Nights --%>
                            <div class="three-columns twelve-columns-mobile offercard strong-box-shadow offerDIV pointer" id="" onclick="fnChangeBorder(this)">
                                <div class="with-padding blue-bg" style="border-bottom: 5px solid #00438d">
                                    <p></p>
                                    <p class="icon-new align-center icon-size4"></p>
                                    <br />
                                    <p class="align-center font18 strong">Free Nights</p>
                                </div>
                                <div class="with-mid-padding align-center" <%--style="min-height: 60px"--%>>
                                    <p>Free night is offered to those booking min No of nights</p>
                                </div>
                                <input type="radio" name="offer" id="rdb_Nights" class="hidden" />
                            </div>
                            <%-- Last Min Booking --%>
                            <div class="three-columns twelve-columns-mobile offercard strong-box-shadow offerDIV pointer" id="" onclick="fnChangeBorder(this)">
                                <div class="with-padding red-bg" style="border-bottom: 5px solid #931a03">
                                    <p></p>
                                    <p class="icon-hourglass align-center icon-size4"></p>
                                    <br />
                                    <p class="align-center font18 strong">Last Minute</p>
                                </div>
                                <div class="with-mid-padding align-center" <%--style="min-height: 60px"--%>>
                                    <p>Special discount is offered for last available rooms</p>
                                </div>
                                  <input type="radio" name="offer" id="rdb_Minute" class="hidden" />
                            </div>
                            <%-- Free AddOns --%>
                            <div class="three-columns twelve-columns-mobile offercard strong-box-shadow offerDIV pointer" id="" onclick="fnChangeBorder(this)">
                                <div class="with-padding orange-bg" style="border-bottom: 5px solid #ff9800">
                                    <p></p>
                                    <p class="icon-ticket align-center icon-size4"></p>
                                    <br />
                                    <p class="align-center font18 strong">Free AddOns</p>
                                </div>
                                <div class="with-mid-padding align-center" <%--style="min-height: 60px"--%>>
                                    <p>Give away AddOn's as per the booking conditions</p>
                                </div>
                                 <input type="radio" name="offer" id="rdb_AddOns"  class="hidden"/>
                            </div>

                        </div>
                    </div>
                     <div class="field-block button-height" id="div_OfferDate">
                        <small class="input-info">You may select one or more range of dates where this offer is applicable</small>
                        <label for="" class="label"><b>Validity</b></label>
                        
                    </div>
                     <div class="field-drop black-inputs button-height">
                        <small class="input-info">Here define dates or days on which these offers are not applicable</small>
                        <label for="" class="label"><b>Blockout</b></label>
                        <div class="columns">
                            <div class="three-columns twelve-columns-mobile mid-margin-bottom">
                                <p>
                                   <input type="radio" class="radio" name="blockout" id="rdb_blockout">
                                    Offer is not valid between
                                </p>
                            </div>
                            <div class="nine-columns twelve-columns-mobile mid-margin-bottom" id="div_BlockDate">
                               
                            </div>

                        </div>
                        <div class="columns">
                            <div class="three-columns twelve-columns-mobile mid-margin-bottom">
                                <p>
                                    <input type="radio" class="radio" name="blockout" id="rdb_blockoutDay">
                                    Offer is not valid on
                                </p>
                            </div>
                            <div class="nine-columns twelve-columns-mobile" id="div_blockout">
                                <input type="checkbox" class="checkbox" id="chk_Mon" value="Mon">
                                <label class="font12" for="chk_Mon">Mon</label>
                                <input type="checkbox" class="checkbox" id="chk_Tue" value="Tue">
                                <label class="font12" for="chk_Tue">Tue</label>
                                <input type="checkbox" class="checkbox" id="chk_Wed" value="Wed">
                                <label class="font12" for="chk_Wed">Wed</label>
                                <input type="checkbox" class="checkbox" id="chk_Thu" value="Thu">
                                <label class="font12" for="chk_Thu">Thu</label>
                                <input type="checkbox" class="checkbox" id="chk_Fri" value="Fri">
                                <label class="font12" for="chk_Fri">Fri</label>
                                <input type="checkbox" class="checkbox" id="chk_Sat" value="Sat">
                                <label class="font12" for="chk_Sat">Sat</label>
                                <input type="checkbox" class="checkbox" id="chk_Sun" value="Sun">
                                <label class="font12" for="chk_Sun">Sun</label>
                            </div>
                        </div>
                    </div>
                    
                </fieldset>

                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Hotel</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Details</b></label>
                        <div class="columns">
                            <div class="four-columns six-columns-mobile mid-margin-bottom">
                                <label class="green strong">Offer Type</label>
                                <label class="black lbl_offerType" ></label>
                            </div>
                            <div class="eight-columns six-columns-mobile mid-margin-bottom div_Validity" id="">
                                <label class="green strong">Validity</label>
                               
                            </div>
                        </div>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">Select the hotel for which this offer / promo is defined</small>
                        <label for="" class="label"><b>Hotel</b></label>
                        <p>
                            <span class="input">
                                <input type="text" name="input-2" id="txt_HotelName" size="22" class="input-unstyled ui-autocomplete-input" placeholder="Eg. Atlantis The Palms">
                                <input type="hidden" id="hdn_HotelCode" value="0" />
                                <span class="icon-search"></span>
                            </span>
                            <small class="input-info red">Only hotels already in your contracted list will be shown</small>
                        </p>

                    </div>
                    <div class="field-block button-height ">
                        <small class="input-info"></small>
                        <label for="" class="label"><b>Offer provider</b></label>
                        <p>
                            This offer is provided by
                            <select name="supplier" class="select expandable-list mid-margin-right" style="width: 150px" id="Sel_OfferBy">
                                <option value="hotel">hotel</option>
                                <option value="supplier">supplier</option>
                                <option value="us">us...</option>
                            </select>
                            <%-- Show only when supplier is selected --%>
                            <%-- Filter & display only suppliers who have rates for selected hotel --%>
                            <span class="input hidden" id="div_Suppier">
                                <input type="text" name="input-2" id="" size="18" class="input-unstyled" placeholder="Eg. xyz supplier">
                                <span class="info-spot">
                                    <span class="icon-info-round"></span>
                                    <span class="info-bubble">Only supplier having valid rates will be shown here
                                    </span>
                                </span>
                            </span>
                        </p>
                    </div>
                    <div class="field-block button-height" id="div_Room">
                        <small class="input-info">Select the rooms for which this offer / promo is aplicable</small>
                        <label for="" class="label"><b>Rooms</b></label>
                        <small id="validateRoom" class="red"><b>Please Select Hotel</b></small>
                    </div>
                    <div class="field-block button-height" id="div_Rates">
                        <small class="input-info">Select the rates for which this offer / promo is applicable</small>
                        <label for="" class="label"><b>Rates</b></label>

                        <input type="checkbox" class="checkbox validate[required]" id="chk_Contracted" value="Contracted" name="ratetype">
                        <label class="font12" for="chk_Contracted">Contracted Rates</label>
                        <input type="checkbox" class="checkbox validate[required]" id="chk_Promotion" value="Promotion" name="ratetype">
                        <label class="font12" for="chk_Promotion">Promotion Rates</label>
                        <input type="checkbox" class="checkbox validate[required]" id="chk_Tectical" value="Tectical" name="ratetype">
                        <label class="font12" for="chk_Tectical">Tectical Rates</label>
                    </div>
                    <div class="field-block button-height">
                        <label class="label"><b>Details</b></label>
                        <small class="input-info">Give your promotion a name</small>
                        <input type="text" name="input-2" id="txt_offerName" size="22" class="input small-margin-top validate[required]" placeholder="30 Days Advance Booking">
                        <small class="input-info red">This will be for your internal referance only</small>
                    </div>
                </fieldset>
                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Define</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Details</b></label>
                        <div class="columns">
                            <div class="four-columns six-columns-mobile mid-margin-bottom">
                                <label class="green strong">Offer Type</label>
                                <label class="black lbl_offerType"></label>
                            </div>
                            <div class="eight-columns six-columns-mobile mid-margin-bottom div_Validity">
                               
                            </div>
                        </div>
                    </div>
                    <div class="field-block button-height">
                        <label class="label"><b>Offer for</b></label>
                        <div class="columns">
                            <div class="four-columns six-columns-mobile mid-margin-bottom">
                                <label class="green strong">Provided by</label>
                                <label class="black Provider"></label>
                            </div>
                            <div class="four-columns six-columns-mobile mid-margin-bottom">
                                <label class="green strong">Offer Name</label>
                                <label class="black OfferName"></label>
                            </div>
                            <div class="four-columns six-columns-mobile mid-margin-bottom">
                                <label class="green strong">Hotel</label>
                                <label class="black hotelname"></label>
                            </div>
                            
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Rooms</label>
                                <label class="black Rooms"></label>
                            </div>
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Rates Type</label>
                                <label class="black rateType"></label>
                            </div>
                            
                        </div>
                    </div>

                    <div class="field-block button-height">
                        <small class="input-info">Promorions are valid for which market / nationalities</small>
                        <label for="market" class="label"><b>Market</b></label>
                        <select name="market" class="select multiple-as-single easy-multiple-selection check-list" id="sel_Country" multiple style="width: 180px">
                        </select>
                    </div>
                    <%-- Show when Early Booking is selected --%>
                    <div class="field-block button-height  div_earlybookingcond" >
                        <small class="input-info">Select any one condition which will attract this promotion</small>
                        <label for="supplier" class="label"><b>Condition</b></label>
                        <p>
                            <input class="radio validate[required]" type="radio" id="earlybookingcond1" name="earlybookingcond" checked>
                            <label class="font13" for="earlybookingcond1">Offer will apply if booked </label>                            
                             <%-- Enable if this radio is selected --%>
                            <span class="number input small-margin-right">
                                <button type="button" class="button number-down">-</button>
                                <input type="text" value="1" size="4" class="input-unstyled" id="txt_DayPriorEarly" data-number-options='{"min":1,"max":90}'>
                                <button type="button" class="button number-up">+</button>
                            </span>
                            days prior (before) to check in date
                        </p>
                        <p>
                            <input class="radio validate[required]" type="radio" id="earlybookingcond2" name="earlybookingcond">
                            <label class="font13" for="earlybookingcond2">Offer will apply if the booking is done before </label> 
                            
                            <%-- Enable if this radio is selected --%>
                            <span class="input">
                                <input type="text" id="dte_DayPrior" class="input-unstyled datepicker" value="">
                                <span class="icon-calendar"></span>
                            </span>
                        </p>
                          <p>
                            <input type="radio" class="radio" id="offerdiscount" name="offerdisorfix" onclick="OfferRateType('discount')" checked>
                            <label class="font13" for="offerdiscount">Offer discount</label>
                            <input type="radio" class="radio" id="changerate" name="offerdisorfix" onclick="OfferRateType('rate')">
                            <label class="font13" for="changerate">Change entire room rate</label>
                        </p>
                    </div>
                    <%-- Show when Free Night is selected --%>
                    <div class="field-block button-height" id="div_FreeNightCond">
                        <small class="input-info">How many paid nights are required to book to get free nights?</small>
                        <label for="" class="label"><b>Condition</b></label>
                        <p>                            
                            If the booking is done for min 
                             
                            <span class="number input small-margin-right">
                                <button type="button" class="button number-down">-</button>
                                <input type="text" value="1" size="4" class="input-unstyled" id="txt_MinNight" data-number-options='{"min":1,"max":30}'>
                                <button type="button" class="button number-up">+</button>
                            </span>
                            nights, 
                            <span class="number input small-margin-right">
                                <button type="button" class="button number-down">-</button>
                                <input type="text" value="1" size="4" class="input-unstyled" id="txt_FreeNight" data-number-options='{"min":1,"max":30}'>
                                <button type="button" class="button number-up">+</button>
                            </span> free nights will be granted
                        </p>
                    </div>

                     <%-- Show when Last Minute is selected --%>
                    <div class="field-block button-height" id="div_LastMinCond">
                        <small class="input-info">When will this offer activated</small>
                        <label for="" class="label"><b>Activation Condition</b></label>
                        <p>
                            <input class="radio" type="radio" id="lastminstartdays" name="lastminstart" checked>
                            <label class="font13" for="lastminstartdays">Start this offer </label>                            
                             <%-- Enable if this radio is selected --%>
                            <span class="number input small-margin-right">
                                <button type="button" class="button number-down">-</button>
                                <input type="text" value="1" size="4" class="input-unstyled" id="txt_LastDay" data-number-options='{"min":1,"max":10}'>
                                <button type="button" class="button number-up">+</button>
                            </span>
                            days before check-in day
                        </p>
                        <p>
                            <input class="radio" type="radio" id="lastminstarthr" name="lastminstart">
                            <label class="font13" for="lastminstarthr">Start this offer </label>                            
                             <%-- Enable if this radio is selected --%>
                            <span class="number input small-margin-right">
                                <button type="button" class="button number-down">-</button>
                                <input type="text" value="1" size="4" class="input-unstyled" id="txt_LastHours" data-number-options='{"min":1,"max":23}'>
                                <button type="button" class="button number-up">+</button>
                            </span>
                            hours before check-in day
                        </p> 
                        <p>
                            <input type="radio" class="radio" id="lasmindiscount" name="lasmin" onclick="OfferRateType('discount')" checked>
                            <label class="font13" for="lasmindiscount">Offer discount</label>
                            <input type="radio" class="radio" id="lasminratechange" name="lasmin" onclick="OfferRateType('')">
                            <label class="font13" for="lasminratechange">Change entire room rate</label>
                        </p>
                    </div>
                    <%-- When new rate is selected --%>
                    <div class="field-block button-height div_rate hidden">
                        <small class="input-info">Define new rate tariff</small>
                        <label for="earlybookingnewrate" class="label"><b>New Rate</b></label>
                        <div class="columns"  id="RoomsRate">
                         
                        </div>
                        <small class="input-info red">These rates will surpass all the rates available in tariff</small>
                    </div>
                    <%-- When Discount is selected --%>
                    <div class="field-block button-height div_discount" style="display:">
                        <small class="input-info">Define discount value in % or amount</small>
                        <label for="" class="label"><b>Discount value</b></label>
                        <span class="input">
                            <input type="text" name="pseudo-input-1" id="txt_Discount" class="input-unstyled validate[required]" style="width: 100px" placeholder="12,345">
                            <select name="DV" class="select green-gradient  check-list" id="SelCurrency" >
                                <option value="%">%</option>
                            </select>
                        </span>
                        <small class="input-info red">Discount will be calculated on per room per night</small>
                    </div>


                    <%-- Not compulsory --%>
                    <div class="field-block button-height">
                        <small class="input-info">Will there be any promo / offer code</small>
                        <label for="" class="label"><b>Promo Code</b></label>
                        <span class="input">
                            <input type="text" class="input-unstyled" id="txt_PromoCode" placeholder="SUMMER2019">
                        </span>
                    </div>
                    <%-- Not compulsory --%>
                    <div class="field-block button-height ">
                        <small class="input-info">Booking note to be desplayed to guest</small>
                        <label for="" class="label"><b>Important Note</b></label>
                        <textarea class="input full-width autoexpanding" id="txt_Note"></textarea>
                    </div>

                </fieldset>
                <fieldset class="wizard-fieldset fields-list" id="div_RatePreview">
                    <legend class="legend">Review</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Details</b></label>
                        <div class="columns">
                            <div class="four-columns six-columns-mobile mid-margin-bottom">
                                <label class="green strong">Offer Type</label>
                                <label class="lbl_offerType"></label>
                            </div>
                            <div class="eight-columns six-columns-mobile mid-margin-bottom div_Validity">
                            </div>
                        </div>
                    </div>
                    <div class="field-block button-height">
                        <label class="label"><b>Offer for</b></label>
                        <div class="columns">
                            <div class="four-columns six-columns-mobile mid-margin-bottom">
                                <label class="green strong ">Provided by</label>
                                <label class="Provider"></label>
                            </div>
                            <div class="four-columns six-columns-mobile mid-margin-bottom">
                                <label class="green strong">Offer Name</label>
                                <label class="OfferName"></label>
                            </div>
                            <div class="four-columns six-columns-mobile mid-margin-bottom">
                                <label class="green strong">Hotel</label>
                                <label class="hotelname"></label>
                            </div>
                            
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Rooms</label>
                                <label class="Rooms"></label>
                            </div>
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Rate type</label>
                                <label class="rateType"></label>
                            </div>
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Market</label>
                                <label class="market"></label>
                            </div>                            
                        </div>
                    </div>
                    <div class="field-block button-height">
                        <label class="label"><b>Offer conditions</b></label>
                        <%--any one will be displayed as per selection--%>
                        <p style="display: ">Offer will apply if booked <b>1 day(s)</b> prior (before) to check in date</p>
                        <p style="display: none">Offer will apply if the booking is done before <b>01/01/2019</b></p>
                    </div>
                    <div class="field-block button-height">
                        <label class="label"><b>New Rates</b></label>
                        <%--any one will be displayed as per selection--%>
                        <div class="columns RatePreview">
                        
                        </div>
                    </div>
                     <div class="field-block button-height" style="display:none">
                         <label class="label"><b>Discount</b></label>
                         <%--any one will be displayed as per selection--%>
                         <p style="display:none"><b>AED 123,456</b> will be discounted per room per night</p>
                         <p><b>100%</b> is discounted on base rates</p>
                     </div>
                    <div class="field-block button-height">
                        <label class="label"><b>Promo Code</b></label>
                        <label class="strong PromoCode"></label>
                    </div>
                    <div class="field-block button-height">
                        <label class="label"><b>Important Note</b></label>
                        <label class="strong Note"></label>
                    </div>

                    <div class="field-block button-height wizard-controls align-right">
    <div class="wizard-spacer" style="height: 19px"></div>
     <a style="cursor:pointer" class="button glossy mid-margin-right" onclick="SaveOffer()">
     <span class="button-icon"><span class="icon-tick"></span></span>
     Save
   </a>
    </div>
                </fieldset>
            </form>
        </div>

    </section>
    

    <script>

        $(document).ready(function () {
            debugger
            //setTimeout(function() {
               
            //},1000)
            // Elements
            var form = $('.wizard'),

                // If layout is centered
                centered;

            // Handle resizing (mostly for debugging)
            function handleWizardResize() {
                centerWizard(false);
            };

            // Register and first call
            $(window).on('normalized-resize', handleWizardResize);

            /*
             * Center function
             * @param boolean animate whether or not to animate the position change
             * @return void
             */
            function centerWizard(animate) {
                form[animate ? 'animate' : 'css']({ marginTop: Math.max(0, Math.round(($.template.viewportHeight - 30 - form.outerHeight()) / 2)) + 'px' });
            };

            // Initial vertical adjust
            centerWizard(false);

            // Refresh position on change step
            form.on('wizardchange', function () { centerWizard(true); });
            // Validation

            if ($.validationEngine) {
                debugger
                form.validationEngine();
            }
            form.showWizardPrevStep(true)
        });
    </script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    
    <script src="js/libs/glDatePicker/glDatePicker.min.js"></script>
    <script src="js/libs/glDatePicker/glDatePicker.js"></script>

    <script type="text/javascript">
        $(window).load(function()
        {
            $('#example').glDatePicker();
        });
    </script>

    <script src="js/developr.collapsible.js"></script>
    <script src="js/developr.auto-resizing.js"></script>
</asp:Content>
