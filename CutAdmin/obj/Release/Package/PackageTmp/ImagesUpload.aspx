﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ImagesUpload.aspx.cs" Inherits="CutAdmin.ImagesUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/Upload_Image.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Add Images</h1>

        </hgroup>
        <div class="with-padding">
            <%--<form action="#" class="frmSupplier">--%>
            <form id="form3" runat="server" class="frmSupplier">
                <%-- <h3>Company Information</h3>--%>
                <asp:HiddenField ID="HiddenField2" runat="server" ClientIDMode="Static" Value="" />
                <asp:HiddenField ID="HiddenField1" runat="server" ClientIDMode="Static" Value="false" />
                <asp:HiddenField ID="hidden_Filed_ID" runat="server" ClientIDMode="Static" Value="" />
                <hr />
                <div class="columns">
                    <div class="eight-columns bold" style="text-align: center;">
                        <button type="button" class="collapsebtn alert-warning">
                            Upload Size More than (32 Kb) <span class=""></span>
                        </button>

                    </div>
                    <div class="four-columns bold">
                        <button type="button" class="collapsebtn alert-warning">
                            Crop File Size (32 Kb) <span class=""></span>
                        </button>

                    </div>
                </div>
                <div class="columns">
                    <div class="four-columns bold">
                        <label>1. Color Passport Size Photograph*	 :</label>

                    </div>
                    <div class="four-columns bold">
                        <asp:FileUpload ID="FileUpload0" ClientIDMode="Static" CssClass="form-control" onchange="this.form.submit()" runat="server" accept="image/*" EnableTheming="False" />
                        <asp:Label ID="lblMessage" runat="server" Text="Color Passport Photo uploaded " ForeColor="Green" Visible="False" />
                        <asp:Button ID="btnUpload" Text="Upload" runat="server" OnClick="Upload" Style="display: none" />
                        <script type="text/javascript">
                            function UploadFile(fileUpload) {
                                if (fileUpload.value != '') {
                                    document.getElementById("<%=btnUpload.ClientID %>").click();
                                }
                            }
                            function OpenDilog(id) {
                                debugger;
                                if (confirm("Are you want to Croped " + id.replace("CropedPass", "Color Passport Size Photograph").replace("FistPage", "Passport Fist Page Coloured Scan Copy").replace("LastPage", "Passport Fist Page Coloured Scan Copy").replace("Supporting1", "Supporting Document").replace("Supporting2", "Supporting Document") + "??")) {
                                    $("#" + id).click()
                                }

                            }
                                                            </script>
                    </div>
                    <div class="four-columns bold">
                        <input type="button" class="button anthracite-gradient UpdateMarkup" value="Crop Photograph" id="CropedPass" onclick="CropImage('1')" />
                    </div>
                </div>
                <div class="columns">
                    <div class="four-columns bold">
                        <label>2. Passport Fist Page Coloured Scan Copy*:</label>

                    </div>
                    <div class="four-columns bold">
                        <asp:FileUpload ID="FileUpload1" CssClass="form-control" runat="server" accept="image/*" ClientIDMode="Static" onchange="this.form.submit()" />
                        <asp:Label ID="Label1" runat="server" Text="Passport Fist Page uploaded ." ForeColor="Green" Visible="False" />
                        <asp:Button ID="Button1" Text="Upload" runat="server" OnClick="UploadPassport" Style="display: none" />
                        <script type="text/javascript">
                            function UploadPassport(fileUpload) {
                                if (fileUpload.value != '') {
                                    document.getElementById("<%=Button1.ClientID %>").click();
                                }
                            }
                                                            </script>
                    </div>
                    <div class="four-columns bold">
                        <input type="button" class="button anthracite-gradient UpdateMarkup" value="Fist Page" id="FistPage" onclick="CropImage('2')" />
                    </div>
                </div>
                <div class="columns">
                    <div class="four-columns bold">
                        <label>3. Passport Last Page Coloured Scan Copy* :</label>

                    </div>
                    <div class="four-columns bold">
                        <asp:FileUpload ID="FileUpload2" CssClass="form-control" ClientIDMode="Static" onchange="this.form.submit()" runat="server" accept="image/*" />
                        <asp:Label ID="Label2" runat="server" Text=" Passport Last Page uploaded ." ForeColor="Green" Visible="False" />
                        <asp:Button ID="Button2" Text="Upload" runat="server" OnClick="UploadPassportLast" Style="display: none" />
                        <script type="text/javascript">
                            function UploadPassportLast(fileUpload) {
                                if (fileUpload.value != '') {
                                    document.getElementById("<%=Button2.ClientID %>").click();
                                }
                            }
                                                            </script>
                    </div>
                    <div class="four-columns bold">
                        <input type="button" class="button anthracite-gradient UpdateMarkup" value="Crop Last Page" id="LastPage" onclick="CropImage('3')" />
                    </div>
                </div>
                <div class="columns">
                    <div class="four-columns bold">
                        <label>4. Supporting Document *:</label>

                    </div>
                    <div class="four-columns bold">
                        <asp:FileUpload ID="FileUpload3" CssClass="form-control" ClientIDMode="Static" onchange="this.form.submit()" runat="server" accept="image/*" />
                        <asp:Label ID="Label3" runat="server" Text=" Supporting Document uploaded." ForeColor="Green" Visible="False" />
                        <asp:Button ID="Button3" Text="Upload" runat="server" OnClick="UploadSupportingDocument1" Style="display: none" />
                        <script type="text/javascript">
                            function UploadSupportingDocument1(fileUpload) {
                                if (fileUpload.value != '') {
                                    document.getElementById("<%=Button3.ClientID %>").click();
                                }
                            }
                                                            </script>
                    </div>
                    <div class="four-columns bold">
                        <input type="button" class="button anthracite-gradient UpdateMarkup" value="Crop Document First" id="Supporting1" onclick="CropImage('4')" />
                    </div>
                </div>

                <div class="columns">
                    <div class="four-columns bold">
                        <label>5. Supporting Document *: </label>

                    </div>
                    <div class="four-columns bold">
                        <asp:FileUpload ID="FileUpload5" CssClass="form-control" ClientIDMode="Static" onchange="this.form.submit()" runat="server" accept="image/*" />
                        <asp:Label ID="Label4" runat="server" Text=" Supporting Document uploaded." ForeColor="Green" Visible="False" />
                        <asp:Button ID="Button4" Text="Upload" runat="server" OnClick="UploadSupportingDocument2" Style="display: none" />
                        <script type="text/javascript">
                            function UploadSupportingDocument2(fileUpload) {
                                if (fileUpload.value != '') {
                                    document.getElementById("<%=Button4.ClientID %>").click();
                                }
                            }
                                                            </script>
                    </div>
                    <div class="four-columns bold">
                        <input type="button" class="button anthracite-gradient UpdateMarkup" value="Crop Document Second" id="Supporting2" onclick="CropImage('5')" />
                    </div>
                </div>
                <div class="columns" id="Confirm1">
                    <div class="four-columns bold">
                        <label>6. Confirm Ticket Copy for 96 Hrs Visa*:</label>

                    </div>
                    <div class="four-columns bold">
                        <asp:FileUpload ID="FileUpload4" CssClass="form-control" ClientIDMode="Static" onchange="this.form.submit()" runat="server" accept="application/pdf" />
                        <asp:Label ID="Label5" runat="server" Text="Ticket Copy uploaded successfully." ForeColor="Green" Visible="false" />
                        <asp:Button ID="Button5" Text="Upload" runat="server" OnClick="UploadConfirmTicketCopy" Style="display: none" />
                        <script type="text/javascript">
                            function UploadConfirmTicketCopy(fileUpload) {
                                if (fileUpload.value != '') {
                                    document.getElementById("<%=Button5.ClientID %>").click();
                                }
                            }
                                                            </script>
                    </div>
                    <div class="four-columns bold">
                        <asp:FileUpload ID="FileCropSuppTicket" CssClass="form-control" runat="server" accept="image/*" ClientIDMode="Static" />
                    </div>
                </div>
                <p class="text-right" style="text-align: right;">
                    <button type="button" class="button anthracite-gradient UpdateMarkup" onclick="GetVisaByCode()">Save & Continue</button>
                </p>
            </form>
        </div>
    </section>
    <!-- End main content -->
</asp:Content>
