﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="sightseeinglist.aspx.cs" Inherits="CutAdmin.sightseeinglist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="css/styles/switches.css?v=1" rel="stylesheet" />
    <script src="js/setup.js"></script>
    <script src="js/libs/formValidator/jquery.validationEngine.js?v=1"></script>
    <script src="js/libs/formValidator/languages/jquery.validationEngine-en.js?v=1"></script>
     <script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBnIPCXTY_ul30N9GMmcSmJPLjPEYzGI7c" type="text/javascript"></script>
    <!-- Webfonts --> 
    <script src="js/libs/jquery.tablesorter.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="with-padding">

    <table class="table responsive-table" id="sorting-example1">

				<thead>
					<tr>
						<th scope="col"><input type="checkbox" name="check-all" id="check-all" value="1"></th>
						<th scope="col">Text</th>
						<th scope="col" width="15%" class="align-center hide-on-mobile">Date</th>
						<th scope="col" width="15%" class="align-center hide-on-mobile-portrait">Status</th>
						<th scope="col" width="15%" class="hide-on-tablet">Tags</th>
						<th scope="col" width="100" class="align-right">Actions</th>
					</tr>
				</thead>


				<tbody>
					<tr>
						<th scope="row" class="checkbox-cell"><input type="checkbox" name="checked[]" id="check-1" value="1"></th>
						<td>John Doe</td>
						<td>Jul 5, 2011</td>
						<td>Enabled</td>
						<td><small class="tag">User</small> <small class="tag">Client</small> <small class="tag green-bg">Valid</small></td>
						<td class="low-padding">
							<span class="select compact full-width" tabindex="0">
								<a href="#" class="select-value">Edit</a>
								<span class="select-arrow"></span>
								<span class="drop-down">
									<a href="#">Put offline</a>
									<a href="#">Review</a>
									<a href="#">Put to trash</a>
									<a href="#">Delete</a>
								</span>
							</span>
						</td>
					</tr>
					<tr>
						<th scope="row" class="checkbox-cell"><input type="checkbox" name="checked[]" id="check-2" value="2"></th>
						<td>John Appleseed</td>
						<td>Jul 5, 2011</td>
						<td>Enabled</td>
						<td><small class="tag orange-bg">Non-verified</small></td>
						<td class="low-padding">
							<span class="select compact full-width" tabindex="0">
								<a href="#" class="select-value">Edit</a>
								<span class="select-arrow"></span>
								<span class="drop-down">
									<a href="#">Put offline</a>
									<a href="#">Review</a>
									<a href="#">Put to trash</a>
									<a href="#">Delete</a>
								</span>
							</span>
						</td>
					</tr>
					<tr>
						<th scope="row" class="checkbox-cell"><input type="checkbox" name="checked[]" id="check-3" value="3"></th>
						<td>Sheldon Cooper</td>
						<td>Jul 4, 2011</td>
						<td>Enabled</td>
						<td><small class="tag">User</small> <small class="tag green-bg">Valid</small></td>
						<td class="low-padding">
							<span class="select compact full-width" tabindex="0">
								<a href="#" class="select-value">Edit</a>
								<span class="select-arrow"></span>
								<span class="drop-down">
									<a href="#">Put offline</a>
									<a href="#">Review</a>
									<a href="#">Put to trash</a>
									<a href="#">Delete</a>
								</span>
							</span>
						</td>
					</tr>
					<tr>
						<th scope="row" class="checkbox-cell"><input type="checkbox" name="checked[]" id="check-4" value="4"></th>
						<td>Rage Guy</td>
						<td>Jun 25, 2011</td>
						<td>Enabled</td>
						<td><small class="tag red-bg">Fake</small></td>
						<td class="low-padding">
							<span class="select compact full-width" tabindex="0">
								<a href="#" class="select-value">Edit</a>
								<span class="select-arrow"></span>
								<span class="drop-down">
									<a href="#">Put offline</a>
									<a href="#">Review</a>
									<a href="#">Put to trash</a>
									<a href="#">Delete</a>
								</span>
							</span>
						</td>
					</tr>
					<tr>
						<th scope="row" class="checkbox-cell"><input type="checkbox" name="checked[]" id="check-5" value="5"></th>
						<td>Thomas A. Anderson</td>
						<td>Jun 16, 2011</td>
						<td>Enabled</td>
						<td><small class="tag">User</small> <small class="tag">Client</small> <small class="tag green-bg">Valid</small></td>
						<td class="low-padding">
							<span class="select compact full-width" tabindex="0">
								<a href="#" class="select-value">Edit</a>
								<span class="select-arrow"></span>
								<span class="drop-down">
									<a href="#">Put offline</a>
									<a href="#">Review</a>
									<a href="#">Put to trash</a>
									<a href="#">Delete</a>
								</span>
							</span>
						</td>
					</tr>
					<tr>
						<th scope="row" class="checkbox-cell"><input type="checkbox" name="checked[]" id="check-6" value="6"></th>
						<td>Jane Doe</td>
						<td>May 19, 2011</td>
						<td>Enabled</td>
						<td><small class="tag">User</small> <small class="tag">Client</small></td>
						<td class="low-padding">
							<span class="select compact full-width" tabindex="0">
								<a href="#" class="select-value">Edit</a>
								<span class="select-arrow"></span>
								<span class="drop-down">
									<a href="#">Put offline</a>
									<a href="#">Review</a>
									<a href="#">Put to trash</a>
									<a href="#">Delete</a>
								</span>
							</span>
						</td>
					</tr>
				</tbody>

			</table>

        </div>
    <script>

		// Call template init (optional, but faster if called manually)
		$.template.init();

		// Table sort - DataTables
		var table = $('#sorting-advanced');
		table.dataTable({
			'aoColumnDefs': [
				{ 'bSortable': false, 'aTargets': [ 0, 5 ] }
			],
			'sPaginationType': 'full_numbers',
			'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
			'fnInitComplete': function( oSettings )
			{
				// Style length select
				table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
				tableStyled = true;
			}
		});

		// Table sort - styled
		$('#sorting-example1').tablesorter({
			headers: {
				0: { sorter: false },
				5: { sorter: false }
			}
		}).on('click', 'tbody td', function(event)
		{
			// Do not process if something else has been clicked
			if (event.target !== this)
			{
				return;
			}

			var tr = $(this).parent(),
				row = tr.next('.row-drop'),
				rows;

			// If click on a special row
			if (tr.hasClass('row-drop'))
			{
				return;
			}

			// If there is already a special row
			if (row.length > 0)
			{
				// Un-style row
				tr.children().removeClass('anthracite-gradient glossy');

				// Remove row
				row.remove();

				return;
			}

			// Remove existing special rows
			rows = tr.siblings('.row-drop');
			if (rows.length > 0)
			{
				// Un-style previous rows
				rows.prev().children().removeClass('anthracite-gradient glossy');

				// Remove rows
				rows.remove();
			}

			// Style row
			tr.children().addClass('anthracite-gradient glossy');

			// Add fake row
			$('<tr class="row-drop">'+
				'<td colspan="'+tr.children().length+'">'+
					'<div class="float-right">'+
						'<button type="submit" class="button glossy mid-margin-right">'+
							'<span class="button-icon"><span class="icon-mail"></span></span>'+
							'Send mail'+
						'</button>'+
						'<button type="submit" class="button glossy">'+
							'<span class="button-icon red-gradient"><span class="icon-cross"></span></span>'+
							'Remove'+
						'</button>'+
					'</div>'+
					'<strong>Name:</strong> John Doe<br>'+
					'<strong>Account:</strong> admin<br>'+
					'<strong>Last connect:</strong> 05-07-2011<br>'+
					'<strong>Email:</strong> john@doe.com'+
				'</td>'+
			'</tr>').insertAfter(tr);

		}).on('sortStart', function()
		{
			var rows = $(this).find('.row-drop');
			if (rows.length > 0)
			{
				// Un-style previous rows
				rows.prev().children().removeClass('anthracite-gradient glossy');

				// Remove rows
				rows.remove();
			}
		});

		// Table sort - simple
	    $('#sorting-example2').tablesorter({
			headers: {
				5: { sorter: false }
			}
		});

	</script>
</asp:Content>
