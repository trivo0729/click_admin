﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="HotelList.aspx.cs" Inherits="CutAdmin.MappedHotelsList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/HotelList.js?v=3.35"></script>
    <script src="Scripts/GetMap.js"></script>
    <link href="css/styles/switches.css?v=1" rel="stylesheet" />
    <script src="js/setup.js"></script>
    <script src="js/libs/formValidator/jquery.validationEngine.js?v=1"></script>
    <script src="js/libs/formValidator/languages/jquery.validationEngine-en.js?v=1"></script>
     <script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBnIPCXTY_ul30N9GMmcSmJPLjPEYzGI7c" type="text/javascript"></script>
    <!-- Webfonts --> 
    <script src="js/libs/jquery.tablesorter.min.js"></script>
    <meta http-equiv="cleartype" content="on">
<%--    <script src="Scripts/ManageInventory.js?v=3.8"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Hotels List</h1>
            <h2><a href="#" class="addnew" style=""><i class="fa fa-filter"></i></a></h2>
            <hr/>
        </hgroup>
        <div class="with-padding">
          
            <table class="table responsive-table font11" id="tbl_HotelList">
                <thead>
                    <tr>
                        <th scope="col" style="max-width:20px;" class="align-center">#</th>
                        <th scope="col" style="max-width:200px;" class="align-center">Hotel Name</th>
                        <th scope="col" style="max-width:70px;" class="align-center">Ratings</th>
                        <th scope="col" style="max-width:100px;" class="align-center">City</th>
                        <th scope="col" style="max-width:150px;" class="align-center">Country</th>
                        <th scope="col" style="max-width:150px;" class="align-center">Supplier Mapped</th>
                        <th scope="col" style="max-width:30px;"class="align-center">Activate</th>
                        <th scope="col" class="align-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>


       <%-- Inventory Filter--%>
       <div id="Inventoryfilter" class="<%--green-gradient--%>" style="width:200px;display: none;">
           <form id="frm_Inventory">
                  <input type="hidden" id="txt_RoomID" />
           <input type="hidden" id="txt_RoomName" />
           <input type="hidden" id="HotelCode" />
            <input type="hidden" id="HotelName" />
            <input type="hidden" id="HotelAddress" />
           <select class="select  auto-open  full-width validate[required]" id="sel_Inventory" onchange="AddInventory()">
               <option value="">-Select Inventory Type-</option>
               <option value="Inventory_freesale.aspx">Free Sale</option>
               <option value="Inventory_allocation.aspx">Allocation</option>
               <option value="Inventory_allotment.aspx">Allotment</option>
           </select>
           </form>
        
       </div>
    </section>
</asp:Content>
