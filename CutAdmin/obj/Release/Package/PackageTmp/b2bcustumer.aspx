﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="b2bcustumer.aspx.cs" Inherits="CutAdmin.b2bcustumer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>B2b User</h1>
            <h2><a href="AddStaff.aspx" class="button anthracite-gradient" style="float: right"><span class="icon-user"></span>Add New</a> 
                 <a onclick="ExportStaffDetailsToExcel()">
                        <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35">
                    </a>
            </h2>
            <hr />
        </hgroup>
        <div class="with-padding">
            
            <div class="respTable">
                <table class="table responsive-table font11" id="dte_B2bUser">
                    <thead>
                        <tr>
                            <th scope="col" class="align-center hide-on-mobile">Name</th>
                            <th scope="col" class="align-center hide-on-mobile">Email | Password Manage</th>
                            <th scope="col" class="align-center hide-on-mobile-portrait">Unique Code</th>
                            <th scope="col" class="align-center hide-on-mobile-portrait">IsActive</th>
                            <th scope="col" width="150" class="align-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                
            </div>
        </div>
    </section>
</asp:Content>
