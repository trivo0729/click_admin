﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="PackageBookingReport.aspx.cs" Inherits="CutAdmin.PackageBookingReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/PackageBookingReport.js"></script>
    <script>
        $(function () {

            $("#datepicker3").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "m/d/yy"
            });
            $("#datepicker4").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "m/d/yy"
            });


            $("#datepicker5").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "m/d/yy"
            });

        });

    </script>

 <%--   <script type="text/javascript">
       
        //var tpj = jQuery;
        //tpj(document).ready(function () {
        //    debugger;
        //    tpj("#txt_AgencyList").autocomplete({
        //        source: function (request, response) {
        //            tpj.ajax({
        //                type: "POST",
        //                contentType: "application/json; charset=utf-8",
        //                url: "../Handler/RoleManagementHandler.asmx/GetAgentRoleList",
        //                data: "{'name':'" + document.getElementById('txt_AgencyList').value + "'}",
        //                dataType: "json",
        //                success: function (data) {
        //                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
        //                    response(result);
        //                },
        //                error: function (result) {
        //                    alert("No Match");
        //                }
        //            });
        //        },
        //        minLength: 1,
        //        select: function (event, ui) {
        //            debugger;
        //            tpj('#hdnDCode').val(ui.item.id);

        //        }
        //    });
        //});
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Package Booking Report</h1>
            <hr />
            <h2><a href="#" class="addnew" id="btn_Packagefilter"><i class="fa fa-filter"></i></a></h2>
            <div id="filter" class="with-padding anthracite-gradient" style="display: none;">
                <form class="form-horizontal">
                    <div class="columns">
                        <div class="three-columns twelve-columns-mobile bold">
                            <label>Passenger Name </label>
                            <div class="input full-width">
                                <input type="text" id="txt_PassengerName" class="input-unstyled full-width">
                            </div>
                        </div>
                        <div class="three-columns twelve-columns-mobile bold">
                            <label>Agency </label>
                            <div class="input full-width">
                                <input type="text" id="txt_AgencyList" class="input-unstyled full-width">
                            </div>
                             <input type="hidden" id="hdnDCode">
                        </div>
                        <div class="three-columns twelve-columns-mobile">
                            <label>Start Date</label>
                            <span class="input full-width">
                                <span class="icon-calendar"></span>
                                <input type="text" name="datepicker" id="datepicker3" class="input-unstyled" value="">
                            </span>
                        </div>
                        <div class="three-columns twelve-columns-mobile">
                            <label>End Date</label>
                            <span class="input full-width">
                                <span class="icon-calendar"></span>
                                <input type="text" name="datepicker" id="datepicker4" class="input-unstyled" value="">
                            </span>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="three-columns twelve-columns-mobile">
                            <label>Booking Date</label>
                            <span class="input full-width">
                                <span class="icon-calendar"></span>
                                <input type="text" name="datepicker" id="datepicker5" class="input-unstyled" value="">
                            </span>
                        </div>
                        <div class="three-columns twelve-columns-mobile bold">
                            <label>Package Name </label>
                            <div class="input full-width">
                                <input type="text" id="txt_Package" class="input-unstyled full-width">
                            </div>
                        </div>
                        <div class="three-columns twelve-columns-mobile bold">
                            <label>Package Type</label>
                            <div class="full-width button-height typeboth">
                                <select id="Sel_PackageType" class="select">
                                    <option value="All" selected="selected">All</option>
                                    <option value="Standard">Standard</option>
                                    <option value="Deluxe">Deluxe</option>
                                    <option value="Premium">Premium</option>
                                    <option value="Luxury">Luxury</option>
                                </select>
                            </div>
                        </div>
                        <div class="three-columns twelve-columns-mobile bold">
                            <label>Location</label>
                            <div class="input full-width">
                                <input type="text" id="txt_Destination" class="input-unstyled full-width">
                            </div>
                        </div>
                       
                    </div>
                    <div class="columns">
                        <div class="eight-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                        </div>
                        <div class="two-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                            <button type="button" class="button anthracite-gradient" onclick="SearchByList();return false;">Search</button>
                            <button type="button" class="button anthracite-gradient" onclick="BookingList();return false;">Reset</button>
                        </div>
                        <div class="two-columns twelve-columns-mobile bold text-alignright">
                            <span class="icon-pdf right" onclick="ExportBooking('PDF');">
                                <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" id="btnPDF" title="Export To Pdf" height="35" width="35">
                            </span>
                            <span class="icon-excel right" onclick="ExportBooking('Excel');" >
                                <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnExcel" height="35" width="35"></span>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>

        </hgroup>


        <div class="with-mid-padding">
            <div class="respTable">
                <table class="table responsive-table font11" id="tbl_PACKAGE">
                    <thead>
                        <tr>
                            <th scope="col">S.N</th>
                            <th scope="col" class="align-center">Agency       </th>
                            <th scope="col" class="align-center">Travel Date  </th>
                            <th scope="col" class="align-center">Passenger    </th>
                            <th scope="col" class="align-center">Package Name </th>
                            <th scope="col" class="align-center">Package Type </th>
                            <th scope="col" class="align-center">Location     </th>
                            <th scope="col" class="align-center">Start Date   </th>
                            <th scope="col" class="align-center">End Date     </th>
                            <%--<th scope="col" class="align-center">Status       </th>--%>
                        </tr>
                    </thead>
                     <tbody></tbody>
                </table>
            </div>
        </div>

    </section>
    <script>
        jQuery(document).ready(function () {
            jQuery('#btn_Packagefilter').click(function () {
                jQuery(' #filter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #filter').slideUp();
                });
            });
        });
    </script>
</asp:Content>
