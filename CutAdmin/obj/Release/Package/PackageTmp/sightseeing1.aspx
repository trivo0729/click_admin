﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="sightseeing1.aspx.cs" Inherits="CutAdmin.sightseeing1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link rel="stylesheet" href="css/styles/form.css?v=3">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
    <!-- DatePicker-->
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.css">    
    
    <script src="js/developr.auto-resizing.js"></script>
    <script>
       $(function() {
           $("#kUI_timepicker").kendoTimePicker(); /*Date Time Picker*/
           //$("#kUI_calendar").kendoCalendar();    /*kendoCalendar*/
           $("#kUI_Datepicker").kendoDatePicker({   /*kendoCalendar*/
               format: "d-MM-yyyy"
           });
       
           $('#kUI_cmb').kendoComboBox({ /*kendoComboBox*/
               dataTextField: "text",
               dataValueField: "value",
               dataSource: [
                 { text: "Cotton", value: "1" },
                 { text: "Polyester", value: "2" },
                 { text: "Cotton/Polyester", value: "3" },
                 { text: "Rib Knit", value: "4" }
               ],
               filter: "contains",
               suggest: true,
               index: 3
           });
           $("#required").kendoMultiSelect();/*kendoMultiSelect*/
       })
    
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h3 class="font24"><b>Add rates for</b>
                <label id="">Dhow Cruise</label>
                </h3>
            <hr />
        </hgroup>
        <div class="with-padding" id="div_Main">
             <form method="post" action="#" class="block margin-bottom wizard same-height" id="">
        <fieldset class="wizard-fieldset fields-list">
            <legend class="legend">Sightseeing</legend>
            <div class="field-block button-height">
                <small class="input-info">Where is this activity / sightseeing / tour operating</small>
                <label for="supplier" class="label"><b>Location</b></label>
                <div class="columns">
                    <div class="four-columns twelve-columns-mobile six-columns-mobile-landscape">
                        <input class="input" type="text" placeholder="Location">
                    </div>
                    <!-- replace with city once location is selected-->
                    <div class="three-columns twelve-columns-mobile six-columns-mobile-landscape">
                        <label class="small-margin-right strong green">City</label>
                    </div>
                    <!-- replace with city once location is selected-->
                    <div class="four-columns twelve-columns-mobile six-columns-mobile-landscape">
                        <label class="small-margin-right strong green">Country</label>
                    </div>
                </div>
            </div>
            <div class="field-block button-height">
                <small class="input-info">What is the name of this activity / sightseeing / tour</small>
                <label for="supplier" class="label"><b>Name</b></label>
                <input class="input" type="text" />
            </div>
            <div class="field-block button-height">
                <small class="input-info">Tag the activity / sightseeing / tour with tour type for whom is this suitable for</small>
                <label for="supplier" class="label"><b>Tour Type</b></label>
                <select id="required" class="" multiple="multiple" data-placeholder="attraction...">
                    <option>Family</option>
                    <option>Honeymoon</option>
                    <option>Adventure</option>
                    <option>Youth</option>                    
                </select>
            </div>
            <div class="field-block button-height">
                <small class="input-info">Give details about this activity / sightseeing / tour</small>
                <label for="supplier" class="label"><b>Description</b></label>
                <textarea class="input full-width autoexpanding" type="text"></textarea>
            </div>
        </fieldset>
        
        <fieldset class="wizard-fieldset fields-list">
            <legend class="legend">Policy</legend>
            <div class="field-block button-height">
                <small class="input-info">There min age restrictions to participate in this tour</small>
                <label for="childpolicy" class="label"><b>Child Policy</b></label>
                <p>
                    <label>Is there any age restriction for children on this tour?</label>
                    <input type="checkbox" class="switch medium" data-checkable-options='{"textOn":"YES","textOff":"NO"}'>
                </p>
            </div>
            <div class="field-drop black-inputs button-height">
                <label for="childagefrom" class="label"><b>Age Restrictions:</b></label>
                <p>
                    <label>Kids below</label>
                    <span class="number input small-margin-right">                       
                        <button type="button" class="button number-down">-</button>
                        <input id="childagefrom" type="text" value="12" size="3" class="input-unstyled" data-number-options='{"min":2,"max":17,"increment":1}'>
                        <button type="button" class="button number-up">+</button>                        
                    </span>
                     <label>years old and above </label>
                     <span class="number input small-margin-right">                       
                        <button type="button" class="button number-down">-</button>
                        <input id="childageto" type="text" value="5" size="3" class="input-unstyled" data-number-options='{"min":2,"max":17,"increment":1.0}'>
                        <button type="button" class="button number-up">+</button>                        
                    </span>
                     <label for="childageto">year old, are considered as <b>big child</b></label>
                </p>
                <p class="no-margin-bottom">
                    <label>Kids below</label>
                    <span class="number input small-margin-right">                       
                        <button type="button" class="button number-down">-</button>
                        <input id="child2agefrom" disabled type="text" value="5" size="3" class="input-unstyled" data-number-options='{"min":2,"max":17,"increment":1.0}'>
                        <button type="button" class="button number-up">+</button>                        
                    </span>
                     <label>years old and above </label>
                     <span class="number input small-margin-right">                       
                        <button type="button" class="button number-down">-</button>
                        <input id="child2ageto" type="text" value="2" size="3" class="input-unstyled" data-number-options='{"min":2,"max":17,"increment":1.0}'>
                        <button type="button" class="button number-up">+</button>                        
                    </span>
                     <label for="childageto">years old, are considered as <b>small child</b></label>
                </p>
                <small class="input-info white">You can change this policy as per rates at the time of rates feeding</small>
            </div>

            <div class="field-block button-height">
                <small class="input-info">Many tour / attractions has max height restrictions</small>
                <label for="height polich" class="label"><b>Height Policy</b></label>
                <p>
                    <label>Is there any height restriction for children on this tour?</label>
                    <input type="checkbox" class="switch medium" data-checkable-options='{"textOn":"YES","textOff":"NO"}'>
                </p>                
            </div>
            <div class="field-drop black-inputs button-height">
                <label for="childagefrom" class="label"><b>Height Restrictions:</b></label>
                <p class="no-margin-bottom">
                    <label>Kids above</label>
                    <span class="number input small-margin-right">                       
                        <button type="button" class="button number-down">-</button>
                        <input id="childfeet" type="text" value="1" size="3" class="input-unstyled" data-number-options='{"min":0,"max":7,"increment":1}'>
                        <button type="button" class="button number-up">+</button>                        
                    </span>
                     <label><b>feet</b> and </label>
                    <span class="number input small-margin-right">                       
                        <button type="button" class="button number-down">-</button>
                        <input id="childinch" type="text" value="0" size="3" class="input-unstyled" data-number-options='{"min":0,"max":7,"increment":1}'>
                        <button type="button" class="button number-up">+</button>                        
                    </span>
                     <label><b>inch</b> are cosidered as adult</label>                     
                </p>
                <small class="input-info white">You can change this policy as per rates at the time of rates feeding</small>
            </div>            
            <div class="field-block button-height">
                <small class="input-info">Some tour / attractions has Infant restrictions</small>
                <label for="height polich" class="label"><b>Infant Policy</b></label>
                <p>
                    <label>Is <b>Infant </b>allowed on this tour?</label>
                    <input type="checkbox" class="switch medium" checked data-checkable-options='{"textOn":"YES","textOff":"NO"}'>
                </p>
            </div>  
            <div class="field-block button-height">
                <small class="input-info">Important information which you want your customers to know</small>
                <label for="height polich" class="label"><b>Important Note</b></label>
                <textarea class="input full-width autoexpanding" type="text"></textarea>
            </div>  
            
        </fieldset>

        <fieldset class="wizard-fieldset fields-list">
            <legend class="legend">Operations</legend>
            <div class="field-block button-height">
                <small class="input-info">Sightseeing / Activity / Tour may be operating entire year or for specific period, kindly define the same</small>
                <label for="childpolicy" class="label"><b>Duration</b></label>
                <p>
                    <label>Is this sightseeing / Tour / Activity is operational full year?</label>
                    <input type="checkbox" class="switch medium" checked data-checkable-options='{"textOn":"YES","textOff":"NO"}'>
                </p>
            </div>
            <%-- if no --%>
            <div class="field-drop black-inputs button-height">
                <small class="input-info">This Tour / Sightseeing / Activity is only operational between below dates</small>
                <label for="childagefrom" class="label"><b>Operation Dates</b></label>
                <div class="columns">
                    <div class="three-columns ten-columns-mobile five-columns-tablet mid-margin-bottom">
                         <p class="white font12 strong no-margin-bottom">From</p>
                        <span class="input margin-right">
                            <span class="icon-calendar"></span>
                            <input type="text" class="input-unstyled datepicker" size="14" placeholder="Starting from">
                        </span>
                    </div>
                    <div class="three-columns twelve-columns-mobile seven-columns-tablet mid-margin-bottom">
                         <p class="white font12 strong no-margin-bottom">To</p>
                        <span class="input margin-right">
                            <span class="icon-calendar"></span>
                            <input type="text" class="input-unstyled datepicker" size="14" placeholder="Ending on">
                        </span>
                    </div>
                    <div class="two-columns twelve-columns-mobile seven-columns-tablet mid-margin-bottom no-margin-left">
                         <p class="white font12 strong no-margin-bottom">Opening Time</p>
                        <span class="input margin-right">
                            <span class="icon-clock"></span>
                            <input type="time" style="width:63px" class="input-unstyled datepicker">
                        </span>
                    </div>
                    <div class="three-columns twelve-columns-mobile seven-columns-tablet mid-margin-bottom">
                        <p class="white font12 strong no-margin-left no-margin-bottom">Closing Time</p>
                        <span class="input margin-right">
                            <span class="icon-clock"></span>
                            <input type="time" style="width:63px" class="input-unstyled datepicker">
                        </span>
                        <span class="mid-margin-left icon-size2 icon-plus-round icon-white" onclick="" id=""></span>
                    </div>                   
                                     
                </div>
                <p class="small-margin-bottom">
                    <input type="checkbox" class="checkbox" id="mon">
                    <label for="mon">Mon</label>
                    <input type="checkbox" class="checkbox" id="tue">
                    <label for="tue">Tue</label>
                    <input type="checkbox" class="checkbox" id="wed">
                    <label for="wed">Wed</label>
                    <input type="checkbox" class="checkbox" id="thu">
                    <label for="thu">Thu</label>
                    <input type="checkbox" class="checkbox" id="fri">
                    <label for="fri">Fri</label>
                    <input type="checkbox" class="checkbox" id="sat">
                    <label for="sat">Sat</label>
                    <input type="checkbox" class="checkbox" id="sun">
                    <label for="sun">Sun</label>
                </p>
            </div>
            <div class="field-block button-height">
                <small class="input-info">Mention if there is any minimum or maximum capacity of this tour</small>
                <label for="supplier" class="label"><b>Capacity</b></label>
                <p>
                    <label>Is there any min or max condition for this sightseeing / Tour / Activity?</label>
                    <input type="checkbox" class="switch medium" checked data-checkable-options='{"textOn":"YES","textOff":"NO"}'>
                </p>
            </div>
             <div class="field-drop black-inputs button-height">
                 <small class="input-info">How many person are allowed per booking?</small>
                 <%--<label for="supplier" class="label"><b>Allowed</b></label>--%>
                <p>
                    <label for="minpax"><b>Minimum</b></label>
                    <span class="number input small-margin-right">                       
                        <button type="button" class="button number-down">-</button>
                        <input id="minpax" type="text" value="12" size="3" class="input-unstyled" data-number-options='{"min":2,"max":17,"increment":1}'>
                        <button type="button" class="button number-up">+</button>                        
                    </span>
                     <label for="maxpax"><b> and Maximum</b></label>
                     <span class="number input small-margin-right">                       
                        <button type="button" class="button number-down">-</button>
                        <input id="maxpax" type="text" value="5" size="3" class="input-unstyled" data-number-options='{"min":2,"max":17,"increment":1.0}'>
                        <button type="button" class="button number-up">+</button>                        
                    </span>
                     <label for="maxpax"><b>are allowed per booking</b></label>
                </p>
             </div>
        </fieldset>
        <fieldset class="wizard-fieldset fields-list">
            <legend class="legend">Main</legend>
            <div class="field-block button-height">
                <small class="input-info">Select supplier for which these rates are being added / updated</small>
                <label for="supplier" class="label"><b>Supplier</b></label>
                <select name="supplier" class="select expandable-list" style="width: 180px" id="">
                    <option value="0">Rayna</option>
                    <option value="1">ClickUrTrip</option>
                </select>
            </div>
        </fieldset>

    </form>
        </div>
    </section>
</asp:Content>
