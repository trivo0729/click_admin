﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Franchisee/AgentMaster.Master" AutoEventWireup="true" CodeBehind="AddPriorityType.aspx.cs" Inherits="CutAdmin.Franchisee.AddPriorityType" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script src="Scripts/AddPriorityType.js"></script>
      <!-- Additional styles -->
    <link rel="stylesheet" href="../css/styles/form.css?v=1">
    <link rel="stylesheet" href="../css/styles/switches.css?v=1">
    <link rel="stylesheet" href="../css/styles/table.css?v=1">
    <!-- DataTables -->
    <link rel="stylesheet" href="../js/libs/DataTables/jquery.dataTables.css?v=1">

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <!-- Main content -->
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <hgroup id="main-title" class="thin">
                <h3>Add Priority Type</h3>
                <hr />
            </hgroup>


        <div class="with-padding">

         
         
				 <div class="columns">

                    <%-- <div class="new-row twelve-columns" style="margin-bottom:-2px">
                                            <h3 class="thin underline green">Add Tour Mode</h3>
                                        </div>--%>

                      <div class="new-row four-columns">
				 <input type="text" name="Text[]" id="fname" class="input full-width margin-left" >
                          </div>
                    
              
                <div class="four-columns">
				<input type="button" class="button anthracite-gradient glossy margin-left" id="btn_Supplier2" value="Save"  onclick="Save()" />
			</div>
               
             </div>
            

            <table class="table responsive-table" id="tbl_GetPriority">
               
                    <thead>
                    <tr>

                        <th  scope="col"> S.N </th>
                        <th align="center" scope="col"> Priority Type </th>
                        <th align="center" scope="col"> Status </th>
                        <th align="center" scope="col"> Edit </th>                           

                    </tr>
                        </thead>
               
            </table>
            <%--<div  class="table-footer ">
                 <button type="button" class="button glossy mid-margin-right float-right" id="btn_Map" onclick="MapCities();">Map Selected Cities</button>
			<br /><br />
			</div>--%>
        </div>

    </section>
    <!-- End main content -->



  </asp:Content>
