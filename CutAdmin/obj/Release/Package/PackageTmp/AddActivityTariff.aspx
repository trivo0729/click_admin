﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddActivityTariff.aspx.cs" Inherits="CutAdmin.AddActivityTariff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/AddActivityTariff.js?V=1.5"></script>
    <script type="text/javascript" src="Scripts/ActMaster.js"></script>
    <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">

    <script type="text/javascript">
        $(document).ready(function () {
            $("#datepicker_From").datepicker({
                dateFormat: "dd-mm-yy",
                //minDate: "dateToday",
                autoclose: true,
            });
            $("#datepicker_To").datepicker({
                // minDate: $("#datepicker_To").text(),
                dateFormat: "dd-mm-yy",
                autoclose: true,
            });
        });
    </script>

    <style type="text/css">
        div.Inclusion {
            height: 150px;
            overflow-y: auto;
            margin-top: 10px;
            background-color: #e6e6e6;
        }

        label.lblAtraction {
            cursor: pointer;
        }

        ::-webkit-scrollbar {
            width: 2px; /* for vertical scrollbars */
            height: 8px; /* for horizontal scrollbars */
        }

        ::-webkit-scrollbar-track {
            background: rgb(255, 255, 255);
        }

        ::-webkit-scrollbar-thumb {
            /*background: #ff9900;*/
            position: relative;
            top: 17px;
            float: right;
            width: 5px;
            height: 32px;
            background-color: rgb(204, 204, 204);
            border: 0px solid rgb(255, 255, 255);
            background-clip: padding-box;
            border-radius: 0px;
        }

        @media only screen and (max-width: 767px) {

            span {
                font-size: .8em;
            }
        }
    </style>

    <!-- glDatePicker -->
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.fixed.css?v=1" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Main content -->
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h1>Add Tariff Details</h1>
            <h2><b class="grey"><strong>
                <label id="ActivityName"></label>
            </strong><small class="" style="float: left; vertical-align: middle"><b id="LocationName" style="font-size: 13px"></b></small></b></h2>
            <hr />
        </hgroup>
        <div class="with-padding">

            <div class="standard-tabs margin-bottom" id="addd-tabs">

                <ul class="tabs">
                    <li id="11" style="display: none"><a href="#tab-11" style="cursor: pointer" onclick="">Ticket Only</a></li>
                    <li id="22" style="display: none"><a href="#tab-22" style="cursor: pointer" onclick="">Sharing</a></li>
                    <li id="33" style="display: none"><a href="#tab-33" style="cursor: pointer" onclick="">Private</a></li>
                </ul>

                <div class="tabs-content">

                    <%-- Start  Tab 1--%>
                    <div id="tab-11" class="with-padding">
                        <div class="columns">
                            <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h6 class="underline margin-bottom">Supplier & Validity</h6>
                            </div>

                            <div class="new-row three-columns">
                                <small class="input-info">Supplier*:</small>
                                <select id="ddlSupplier" name="ddlSupplier" class="full-width select validate[required]">
                                    <option value="" selected="selected" disabled>Please select</option>
                                </select>
                            </div>
                            <div class="three-columns" id="TimeSlot" style="">

                                <small class="input-info">Select Time Slots*:</small>
                                <select id="ddlTimeSlots" class="full-width glossy select multiple-as-single easy-multiple-selection PSLot check-list" multiple onchange="fnSlot()">
                                </select>
                            </div>
                            <div id="DatesUI" class="six-columns">
                            </div>

                            <div class="four-columns" id="tktSlot" style="display: none">
                                <h6>Activity Time Slots </h6>
                                <select id="ddlSlot" name="ddlSlot" class="full-width select validate[required]" onchange="SlotChange()">
                                    <option selected="" value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>

                            <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h6 class="underline margin-bottom">Rates</h6>
                            </div>

                            <div class="new-row twelve-columns" id="UIDates" style="margin-bottom: 5px">
                                <label style="font-size: 13px; font: bolder; color: orange">Dates:</label>
                            </div>
                            <div id="RatesDetails" style="width: 98%">
                            </div>

                            <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h6 class="underline margin-bottom">Special Dates</h6>
                            </div>

                            <div class="new-row three-columns">

                                <span class="button-group">
                                    <label for="chksplDateYes" class="button green-active">
                                        <input type="radio" name="button-radio chksplDate" id="chksplDateYes" value="Yes" onclick="SpecialDate(this.value);">
                                        YES
                                    </label>
                                    <label for="chksplDateNo" class="button green-active">
                                        <input type="radio" checked name="button-radio chksplDate" id="chksplDateNo" value="No" onclick="SpecialDate(this.value);">
                                        NO
                                    </label>

                                </span>
                            </div>

                            <div class="three-columns" id="SpecialTimeSlot" style="display: none">
                                <small class="input-info">Select Time Slots*: </small>
                                <select id="ddlSpecialTimeSlots" class="full-width glossy select multiple-as-single easy-multiple-selection SplPSLot check-list" multiple onchange="fnSpecialSlot()">
                                    <%-- <option class="PPCity" value="Abu Dhabi">Abu Dhabi</option>
                                                <option class="PPCity" value="Dubai" selected="selected">Dubai</option>--%>
                                </select>
                            </div>

                            <div id="SpecialDates" class="six-columns">
                            </div>

                            <div id="SdateSlot" class="four-columns" style="display: none">

                                <h6>Activity By Slots </h6>
                                <select id="ddlDateSlot" name="ddlDateSlot" class="full-width select validate[required]" onchange="SDateSlotChange()">
                                    <option selected="" value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>

                            </div>

                            <div class="new-row twelve-columns" id="SpecialDateTKTDiv" style="margin-bottom: -2px; display: none">
                                <h6 class="underline margin-bottom">Rates For Special Dates</h6>
                            </div>
                            <div id="UISplDatesTKT" class="new-row twelve-columns" style="margin-bottom: 7px; display: none">

                                <%--<div class="" id="UISplDatesTKT" style="display: none">--%>
                                <label style="font-size: 13px; font: bolder; color: orange">Special Dates:</label>
                                <%--</div>--%>
                                <%--<label style="font-size:15px" id="lbl_Date"></label>--%>
                            </div>

                            <div id="SpecialRatesDetails" style="width: 98%">
                            </div>

                            <div class="new-row twelve-columns" id="ChildPolicy" style="margin-bottom: -2px">
                                <h6 class="underline margin-bottom">Child Policy</h6>
                            </div>

                            <div class="columns" id="ChildPolicyAllow">

                                <div class="new-row three-columns">
                                    <small class="input-info">Child Age From:</small>

                                    <input type="text" name="Text[]" id="txt_ChildAgeFrom" class="input full-width" value="" readonly="readonly">
                                </div>
                                <div class="three-columns">
                                    <small class="input-info">Child Age Upto:</small>
                                    <input type="text" name="Text[]" id="txt_ChildAgeUpto" class="input full-width" value="" readonly="readonly">
                                </div>
                                <div class="three-columns">
                                    <small class="input-info">Small Child Age Upto:</small>
                                    <input type="text" name="Text[]" id="txt_SmallChildAgeupto" class="input full-width" value="" readonly="readonly">
                                </div>
                                <div class="three-columns">
                                    <small class="input-info">Child Min Hight:</small>
                                    <input type="text" name="Text[]" id="txt_ChildMinHeight" class="input full-width" value="" readonly="readonly">
                                </div>
                            </div>

                            <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h6 class="underline margin-bottom">Conditions</h6>
                            </div>
                            <div style="width: 100%">
                                <div class="columns" id="" style="width: 100%">
                                    <div class="new-row two-columns">
                                        <%--<p>Is this activity bookable live?</p>--%>
                                        <small>
                                            <label>this activity bookable live?</label>
                                        </small>
                                        <span class="button-group">
                                            <label for="chkbookcndyes" class="button green-active">
                                                <input type="radio" name="button-radio chkbookcnd" id="chkbookcndyes" value="Yes" onclick="Showcondition(this.value)">
                                                YES
                                            </label>
                                            <label for="chkbookcndno" class="button green-active">
                                                <input type="radio" checked name="button-radio chkbookcnd" id="chkbookcndno" value="No" onclick="Showcondition(this.value)">
                                                NO
                                            </label>

                                        </span>
                                    </div>

                                    <div class="new-row two-columns" style="display: none">
                                        <small class="input-info">Booking Condition:</small>
                                        <select id="sel_BookinCondition" name="sel_BookinCondition" class="full-width select validate[required]" onchange="">
                                            <option value="Yes">Yes</option>
                                            <option value="No" selected="selected">No</option>
                                        </select>
                                    </div>


                                    <div class="four-columns" id="div_Before" style="display: none">
                                        <small>
                                            <label>Until what time before tour start time you want to accept booking</label>
                                        </small>
                                        <%-- <input type="text" name="Text[]" id="txt_Before" class="input full-width" value="">--%>
                                        <select id="sel_Before" name="sel_Before" class="full-width select validate[required]">
                                            <option selected="" value="">Select booking time</option>
                                           <%-- <option value="01">01 Hours</option>
                                            <option value="06">06 Hours</option>
                                            <option value="12">12 Hours</option>
                                            <option value="18">18 Hours</option>
                                            <option value="24">24 Hours</option>
                                            <option value="2-5Days">2-5 Days</option>--%>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="new-row ten-columns">
                            </div>
                            <div class="two-columns">
                                <button type="button" class="button glossy mid-margin-right" onclick="Save2();" title="Submit Details">
                                    <span class="button-icon"><span class="icon-tick"></span></span>
                                    Add
                                </button>
                            </div>

                        </div>
                        <input type="hidden" id="hdn_Tkt" value="0" />
                    </div>
                    <%-- End  Tab 1--%>



                    <%-- Start  Tab 2--%>
                    <div id="tab-22" class="with-padding">
                        <div class="columns">

                            <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h6 class="underline margin-bottom">Supplier & Validity</h6>
                            </div>

                            <div class="new-row three-columns">
                                <small class="input-info">Supplier*:</small>
                                <select id="ddlSupplierSIC" name="ddlSupplier" class="full-width select validate[required]">
                                    <option value="" selected="selected" disabled>Please select</option>

                                </select>
                            </div>
                            <div class="three-columns" id="TimeSlotSIC" style="">
                                <small class="input-info">Select Time Slots </small>
                                <select id="ddlTimeSlotsSIC" class="full-width glossy select multiple-as-single easy-multiple-selection Tslots  check-list" multiple onchange="fnSlotSIC()">
                                    <%-- <option class="PPCity" value="Abu Dhabi">Abu Dhabi</option>
                                                <option class="PPCity" value="Dubai" selected="selected">Dubai</option>--%>
                                </select>
                            </div>
                            <div id="DatesUISIC" class="six-columns">
                            </div>


                            <div class="four-columns" id="sicslot" style="display: none">
                                <h6>Activity Time Slots </h6>
                                <select id="ddlSlotSIC" name="ddlSlotSIC" class="full-width select validate[required]" onchange="SlotChangeSIC()">
                                    <option selected="" value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>

                            <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h6 class="underline margin-bottom">Rates</h6>
                            </div>

                            <div class="new twelve-columns" id="UIDatesSIC" style="margin-bottom: 5px">
                                <label style="font-size: 13px; color: orange">Dates:</label>
                            </div>

                            <%--<div class="new-row twelve-columns" id="SlotTimingSIC">
                                            <label style="font-size: 18px; font: bold;color:orange">Slot Timing:</label>
                                        </div>--%>


                            <div id="RatesDetailsSIC" style="width: 98%">
                            </div>

                            <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h6 class="underline margin-bottom">Special Dates</h6>
                            </div>
                            <div class="new-row three-columns">
                                <%--<h6>Special Dates :</h6>
                                        <select id="ddlSpecialDatesSIC" name="ddlSpecialDatesSIC" class="full-width select validate[required]" onchange="SpecialDateSIC()">
                                            <option selected="" value="">Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>--%>
                                <span class="button-group">
                                    <label for="sicchksplDateYes" class="button green-active">
                                        <input type="radio" name="button-radio sicchksplDate" id="sicchksplDateYes" value="Yes" onclick="SpecialDateSIC(this.value);">
                                        YES
                                    </label>
                                    <label for="sicchksplDateNo" class="button green-active">
                                        <input type="radio" checked name="button-radio sicchksplDate" id="sicchksplDateNo" value="No" onclick="SpecialDateSIC(this.value);">
                                        NO
                                    </label>

                                </span>

                            </div>

                            <div class="three-columns" id="SpecialTimeSlotSIC" style="display: none">
                                <small class="input-info">Select Time Slots*: </small>
                                <select id="ddlSpecialTimeSlotsSIC" class="full-width glossy select multiple-as-single easy-multiple-selection  check-list" multiple onchange="fnSpecialSlotSIC()">
                                    <%-- <option class="PPCity" value="Abu Dhabi">Abu Dhabi</option>
                                                <option class="PPCity" value="Dubai" selected="selected">Dubai</option>--%>
                                </select>
                            </div>

                            <div id="SpecialDatesSIC" class="six-columns">
                            </div>

                            <div id="SdateSlotSIC" class="four-columns" style="display: none">

                                <h6>Activity By Slots </h6>
                                <select id="ddlDateSlotSIC" name="ddlDateSlotSIC" class="full-width select validate[required]" onchange="SDateSlotChangeSIC()">
                                    <option selected="" value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>

                            </div>
                            <div class="new-row twelve-columns" id="SpecialDateSICDiv" style="margin-bottom: -2px; display: none">
                                <h6 class="underline margin-bottom">Rates For Special Dates</h6>
                            </div>
                            <div class="new-row twelve-columns">

                                <div class="" id="UISplDatesSIC" style="display: none; margin-bottom: -15px">
                                    <label style="font-size: 13px; font: bolder; color: orange">Special Dates:</label>
                                </div>
                                <%--<label style="font-size:15px" id="lbl_Date"></label>--%>
                            </div>
                            <div id="SpecialRatesDetailsSIC" style="width: 98%">
                            </div>


                            <div id="sicchildpolicyy" class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h6 class="underline margin-bottom">Child Policy</h6>
                            </div>
                            <div class="columns" id="sicchildpolicy">

                                <div class="new-row three-columns">
                                    <small class="input-info">Child Age From:</small>

                                    <input type="text" name="Text[]" id="txt_ChildAgeFromSIC" class="input full-width" value="" readonly="readonly">
                                </div>
                                <div class="three-columns">
                                    <small class="input-info">Child Age Upto 	 :</small>
                                    <input type="text" name="Text[]" id="txt_ChildAgeUptoSIC" class="input full-width" value="" readonly="readonly">
                                </div>
                                <div class="three-columns">
                                    <small class="input-info">Small Child Age Upto :</small>
                                    <input type="text" name="Text[]" id="txt_SmallChildAgeuptoSIC" class="input full-width" value="" readonly="readonly">
                                </div>
                                <div class="three-columns">
                                    <small class="input-info">Child Min Hight:</small>
                                    <input type="text" name="Text[]" id="txt_ChildMinHeightSIC" class="input full-width" value="" readonly="readonly">
                                </div>
                            </div>

                            <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h6 class="underline margin-bottom">Conditions</h6>
                            </div>

                            <div style="width: 100%">
                                <div class="columns" id="" style="width: 100%">

                                    <div class="new-row two-columns">
                                        <small>
                                            <label>this activity bookable live?</label>
                                        </small>
                                        <span class="button-group">
                                            <label for="SICchkbookcndyes" class="button green-active">
                                                <input type="radio" name="button-radio SICchkbookcnd" id="SICchkbookcndyes" value="Yes" onclick="ShowconditionSIC(this.value)">
                                                YES
                                            </label>
                                            <label for="SICchkbookcndno" class="button green-active">
                                                <input type="radio" checked name="button-radio SICchkbookcnd" id="SICchkbookcndno" value="No" onclick="ShowconditionSIC(this.value)">
                                                NO
                                            </label>
                                        </span>
                                    </div>

                                    <div class="new-row two-columns" style="display: none">
                                        <small class="input-info">Booking Condition:</small>
                                        <select id="sel_SICBookinCondition" name="sel_SICBookinCondition" class="full-width select validate[required]" onchange="ShowconditionSIC(this.value)">
                                            <option value="Yes">Yes</option>
                                            <option value="No" selected="selected">No</option>
                                        </select>
                                    </div>

                                    <div class="four-columns" id="div_SICBefore" style="display: none">
                                        <small>
                                            <label>Until what time before tour start time you want to accept booking</label>
                                        </small>
                                        <select id="sel_SICBefore" name="sel_SICBefore" class="full-width select validate[required]">
                                            <option selected="" value="">Select booking time</option>
                                            <%--<option value="00">01 Hours</option>
                                            <option value="06">06 Hours</option>
                                            <option value="12">12 Hours</option>
                                            <option value="18">18 Hours</option>
                                            <option value="24">24 Hours</option>
                                            <option value="2-5Days">2-5 Days</option>--%>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="new-row ten-columns">
                            </div>
                            <div class="two-columns">
                                <button type="button" id="btn_RegiterAgent" class="button glossy mid-margin-right" onclick="Save();" title="Submit Details">
                                    <span class="button-icon"><span class="icon-tick"></span></span>
                                    Add
                                </button>
                            </div>
                            <input type="hidden" id="hdn_Sic" value="0" />
                        </div>
                    </div>


                    <%-- End  Tab 2--%>

                    <%-- Start  Tab 3--%>
                    <div id="tab-33" class="with-padding">
                        <div class="columns">

                            <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h3 class="thin underline green">Supplier & Validity</h3>
                            </div>

                            <div class="new-row four-columns">
                                <h6>Supplier*:</h6>
                                <select id="ddlSupplierPVT" name="ddlSupplier" class="full-width select validate[required]">
                                    <option value="" selected="selected" disabled>Please select</option>

                                </select>
                            </div>

                            <div id="DatesUIPVT" class="eight-columns">
                                <%--<div class="four-columns">
                                                <h6>Pick up from :</h6>
                                                <input type="text" name="Text[]" id="txt_pickupfrom" class="input full-width" value="">
                                            </div>

                                            <div class="four-columns">
                                                <h6>Drop time :</h6>
                                                <input type="time" id="txt_DropTime" class="input full-width" value="">
                                            </div>--%>
                            </div>

                            <div class="new-row four-columns" id="pvtslot">
                                <h6>Activity Time Slots </h6>
                                <select id="ddlSlotPVT" name="ddlSlotPVT" class="full-width select validate[required]" onchange="SlotChangePVT()">
                                    <option selected="" value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="four-columns" id="TimeSlotPVT" style="display: none">
                                <h6>Select Time Slots </h6>
                                <select id="ddlTimeSlotsPVT" class="full-width glossy select multiple-as-single easy-multiple-selection  check-list" multiple onchange="fnSlotPVT()">
                                    <%-- <option class="PPCity" value="Abu Dhabi">Abu Dhabi</option>
                                                <option class="PPCity" value="Dubai" selected="selected">Dubai</option>--%>
                                </select>
                            </div>
                            <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h3 class="thin underline green">Special Dates</h3>
                            </div>
                            <div class="new-row four-columns">
                                <h6>Special Dates :</h6>
                                <select id="ddlSpecialDatesPVT" name="ddlSpecialDatesPVT" class="full-width select validate[required]" onchange="SpecialDatePVT()">
                                    <option selected="" value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>

                            <div id="SpecialDatesPVT" class="eight-columns">
                            </div>

                            <div id="SdateSlotPVT" class="new-row four-columns" style="display: none">

                                <h6>Activity By Slots </h6>
                                <select id="ddlDateSlotPVT" name="ddlDateSlotPVT" class="full-width select validate[required]" onchange="SDateSlotChangePVT()">
                                    <option selected="" value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>

                            </div>

                            <div class="four-columns" id="SpecialTimeSlotPVT" style="display: none">
                                <h6>Select Time Slots </h6>
                                <select id="ddlSpecialTimeSlotsPVT" class="full-width glossy select multiple-as-single easy-multiple-selection  check-list" multiple onchange="fnSpecialSlotPVT()">
                                    <%-- <option class="PPCity" value="Abu Dhabi">Abu Dhabi</option>
                                                <option class="PPCity" value="Dubai" selected="selected">Dubai</option>--%>
                                </select>
                            </div>
                            <div id="SpecialRatesDetailsPVT" style="width: 100%">
                            </div>


                            <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h3 class="thin underline green">Pick Up & Drop Timing</h3>
                            </div>
                            <div class="new-row three-columns">
                                <h6>Pick-Up From:</h6>

                                <input type="text" name="Text[]" id="txt_PickupfromPVT" class="input full-width" value="">
                            </div>
                            <div class="three-columns">
                                <h6>Pick-Up Time:</h6>
                                <input type="text" name="Text[]" id="txt_PickuptoPVT" class="input full-width" value="">
                            </div>
                            <div class="three-columns">
                                <h6>Drop-Off At :</h6>
                                <input type="text" name="Text[]" id="txt_DropofatPVT" class="input full-width" value="">
                            </div>
                            <div class="three-columns">
                                <h6>Drop-Off Time:</h6>
                                <input type="text" name="Text[]" id="txt_DropoftimePVT" class="input full-width" value="">
                            </div>



                            <div class="columns" id="pvtchildpolicy">
                                <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                    <h3 class="thin underline green">Child Policy</h3>
                                </div>
                                <div class="new-row three-columns">
                                    <h6>Child Age From 	 :</h6>

                                    <input type="text" name="Text[]" id="txt_ChildAgeFromPVT" class="input full-width" value="" readonly="readonly">
                                </div>
                                <div class="three-columns">
                                    <h6>Child Age Upto 	 :</h6>
                                    <input type="text" name="Text[]" id="txt_ChildAgeUptoPVT" class="input full-width" value="" readonly="readonly">
                                </div>
                                <div class="three-columns">
                                    <h6>Small Child Age Upto :</h6>
                                    <input type="text" name="Text[]" id="txt_SmallChildAgeuptoPVT" class="input full-width" value="" readonly="readonly">
                                </div>
                                <div class="three-columns">
                                    <h6>Child Min Hight:</h6>
                                    <input type="text" name="Text[]" id="txt_ChildMinHeightPVT" class="input full-width" value="" readonly="readonly">
                                </div>
                            </div>






                            <%--                                        <div class="new-row five-columns">
                                            <h6>Inclusions :</h6>
                                            <input type="text" name="Text[]" id="txt_InclusionsSic" class="input full-width" value="">
                                        </div>

                                        <div class="one-column">
                                            <br />
                                            <button type="button" onclick="InclusionsAdd('Sic')" id="btnSerchName2" style="cursor: pointer">
                                                <span class="icon-plus" title="Click Here To Add"></span>
                                            </button>
                                        </div>

                                        <div class="five-columns">
                                            <h6>Exclusions  :</h6>
                                            <input type="text" name="Text[]" id="txt_ExclusionsSic" class="input full-width" value="">
                                        </div>

                                        <div class="one-column">
                                            <br />
                                            <button type="button" onclick="ExclusionsAdd('Sic')" id="btnSerchName3" style="cursor: pointer">
                                                <span class="icon-plus" title="Click Here To Add"></span>
                                            </button>
                                        </div>


                                        <div class="six-columns">
                                            <div id="idinclusionSic" class="Inclusion"></div>
                                        </div>

                                        <div class="six-columns">
                                            <div id="idexclusionSic" class="Inclusion"></div>
                                        </div>--%>

                            <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h3 class="thin underline green">Rates</h3>
                            </div>

                            <%--<div class="new-row twelve-columns">
                                            
                                         
                                        </div>--%>


                            <div class="new twelve-columns" id="UIDatesPVT">
                                <label style="font-size: 18px; font: bold; color: orange">Dates:</label>
                            </div>

                            <%--<div class="new-row twelve-columns" id="SlotTimingPVT">
                                            <label style="font-size: 18px; font: bold;color:orange">Slot Timing:</label>
                                        </div>--%>


                            <div id="RatesDetailsPVT" style="width: 100%">
                            </div>

                            <div class="new-row twelve-columns">
                                <h6></h6>
                            </div>


                            <div class="new-row ten-columns">
                            </div>
                            <div class="two-columns">
                                <button type="button" <%--id="btn_RegiterAgent"--%> class="button glossy mid-margin-right" onclick="Save3();" title="Submit Details">
                                    <span class="button-icon"><span class="icon-tick"></span></span>
                                    Add
                                </button>
                                <%-- <button type="button" class="button glossy" onclick="window.location.href='AdminDashBoard.aspx'">
                                            <span class="button-icon red-gradient"><span class="icon-cross-round"></span></span>
                                            Cancel
                                        </button>
                                        <button type="button"  class="button glossy" onclick="DeleteActivityTariff('Pvt');">
                                            <span class="button-icon red-gradient"><span class="icon-minus-round"></span></span>
                                            Delete
                                        </button>--%>
                            </div>


                        </div>
                        <input type="hidden" id="hdn_Pvt" value="0" />
                    </div>
                    <%-- End  Tab 3--%>
                </div>
            </div>
        </div>
    </section>
</asp:Content>


