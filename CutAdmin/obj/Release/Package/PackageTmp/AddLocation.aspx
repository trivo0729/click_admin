﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddLocation.aspx.cs" Inherits="CutAdmin.AddLocation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyDLvhAriXSRStAxraxjCp1GtClM4slLh-k"></script>
    <script src="Scripts/AddLocation.js?v=1.9"></script>
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <!-- DataTables -->
    <link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">
    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
    <style>
      .pac-container {
    background-color: #FFF;
    z-index: 20;
    position: fixed;
    display: inline-block;
    float: left;
}
#modal{
    z-index: 20;   
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h1>Location Details</h1>
            <h2><a style="cursor:pointer" onclick="NewLocation()"  class="addnew with-tooltip" title="Add Location"><i class="fa fa-plus-circle"></i></a></h2>

            <hr />
        </hgroup>
        <div class="with-padding">
            <div class="respTable">
                <input type="hidden" id="LocationPlaceID" />
                <input type="hidden" id="Location_Name" />
                <input type="hidden" id="lat_Location" />
                <input type="hidden" id="log_Location" />

                <input type="hidden" id="AreaPlaceID" />
                <input type="hidden" id="Area_Name" />
                <input type="hidden" id="lat_Area" />
                <input type="hidden" id="log_Area" />

                <table class="table responsive-table font11" id="tbl_GetLocation">
                    <thead>
                        <tr>
                            <th scope="col" class="align-center"><span>Sr.No</span></th>
                            <th scope="col" class="align-center"><span>Location</span></th>
                            <th scope="col" class="align-center"><span>City</span></th>
                            <th scope="col" class="align-center"><span>Country</span></th>
                            <th scope="col" class="align-center"><span>Actions</span></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- End main content -->
</asp:Content>
