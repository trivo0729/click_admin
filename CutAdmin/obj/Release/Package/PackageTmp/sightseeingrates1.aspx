﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="sightseeingrates1.aspx.cs" Inherits="CutAdmin.sightseeingrates1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/styles/form.css?v=3">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
    <!-- DatePicker-->
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.css">    
    <script src="js/developr.auto-resizing.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h3 class="font24"><b>Add rates for</b>
                <label id="">Dhow Cruise</label>
                </h3>
            <hr />
        </hgroup>
        <div class="with-padding" id="div_Main">
            <form method="post" action="#" class="block margin-bottom wizard same-height" id="">
                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Main</legend>
                    <div class="field-block button-height">
                        <small class="input-info">Select supplier for which these rates are being added / updated</small>
                        <label for="supplier" class="label"><b>Supplier</b></label>
                        <select name="supplier" class="select expandable-list" style="width: 180px" id="">
                            <option value="0">Rayna</option>
                            <option value="1">ClickUrTrip</option>                            
                        </select>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">Select in which currency you wish to add / update rates</small>
                        <label for="currency" class="label"><b>Currency</b></label>
                        <%--Use dropdown--%>
                        <select name="currency" class="select expandable-list <%--validate[required]--%>" style="width: 180px" id="SelCurrency" onchange="SetCurrency(this.value)">
                            <option value="AED">AED</option>
                            <option value="USD">USD</option>
                            <option value="INR">INR</option>
                        </select>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">Select one or more nationality / market for which rates are valid</small>
                        <label for="market" class="label "><b>Valid for market</b></label>
                        <%--Use dynamic dropdown--%>
                        <select name="country" class="select multiple-as-single easy-multiple-selection check-list expandable-list input <%--validate[required]--%>" id="sel_Country" multiple style="width: 180px">
                        </select>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">Select any one tour type for which rates are being feeded</small>
                        <label for="touroption" class="label "><b>Tour Options</b></label>
                        <%--Use dynamic dropdown--%>
                        <select name="touroptions" class="select expandable-list" style="width: 180px" id="touroptions">
                            <option value="tkt">Entry Ticket</option>
                            <option value="sic">Sharing (SIC)</option>
                            <option value="pvt">Private (PVT)</option>                            
                        </select>
                       
                    </div>

                    <div class="field-block button-height">
                        <small class="input-info">Select for which type of sightseeing / activity you want to feed rates</small>
                        <label for="market" class="label "><b>Tour / Activity Type</b></label>
                        <span id="tourtype1" class="margin-right">
                            <input type="checkbox" class="checkbox" id="tourtype1-checkbox">
                            <label for="tourtype1-checkbox">Bronze</label>
                        </span>
                        <span id="tourtype2" class="margin-right">
                            <input type="checkbox" class="checkbox" id="tourtype2-checkbox">
                            <label for="tourtype2-checkbox">Silver</label>
                        </span>
                        <span id="tourtype3" class="margin-right">
                            <input type="checkbox" class="checkbox" id="tourtype3-checkbox">
                            <label for="tourtype3-checkbox">Golden</label>
                        </span>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">If this tour had timing restiction enable slot time or keep it No</small>
                        <label for="market" class="label ">
                            <b>Slot</b>
                            <p class="reversed-switches">
                                <input type="checkbox" class="switch medium wide" checked data-text-on="Enable" data-text-off="Disable">
                            </p>
                        </label>
                        <!-- -->
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile seven-columns-tablet">
                                <p class="align-center no-margin-bottom">Starting Time</p>
                                <div class="columns">
                                    <div class="six-columns">
                                        <span class="number input margin-right">
                                            <label style="width: 20px" for="" class="button field-drop-input grey-gradient">Hrs</label>
                                            <button type="button" class="button number-down">-</button>
                                            <input type="text" value="1" size="3" class="input-unstyled" data-number-options='{"min":1,"max":24,"increment":1.0,"shiftIncrement":5,"precision":0.25}'>
                                            <button type="button" class="button number-up">+</button>
                                        </span>
                                    </div>
                                    <div class="six-columns">
                                        <span class="number input margin-right">
                                            <label style="width: 20px" for="" class="button field-drop-input grey-gradient">Min</label>
                                            <button type="button" class="button number-down">-</button>
                                            <input type="text" value="00" size="3" class="input-unstyled" data-number-options='{"min":0,"max":50,"increment":10,"shiftIncrement":5,"precision":1}'>
                                            <button type="button" class="button number-up">+</button>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="four-columns twelve-columns-mobile seven-columns-tablet">
                                <p class="align-center no-margin-bottom">Ending Time</p>
                                <div class="columns">
                                    <div class="six-columns">
                                        <span class="number input margin-right">
                                            <label style="width: 20px" for="" class="button field-drop-input grey-gradient">Hrs</label>
                                            <button type="button" class="button number-down">-</button>
                                            <input type="text" value="1" size="3" class="input-unstyled" data-number-options='{"min":1,"max":24,"increment":1.0,"shiftIncrement":5,"precision":0.25}'>
                                            <button type="button" class="button number-up">+</button>
                                        </span>
                                    </div>
                                    <div class="six-columns">
                                        <span class="number input margin-right">
                                            <label style="width: 20px" for="" class="button field-drop-input grey-gradient">Min</label>
                                            <button type="button" class="button number-down">-</button>
                                            <input type="text" value="00" size="3" class="input-unstyled" data-number-options='{"min":0,"max":50,"increment":10,"shiftIncrement":5,"precision":1}'>
                                            <button type="button" class="button number-up">+</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="one-column">
                                <br />
                                <span class="mid-margin-left icon-size2 icon-plus-round icon-black" onclick="" id=""></span>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Rates</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Rates for</b></label>
                        <div class="columns">
                            <div class="six-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Supplier</label>
                                <label class="black">Rayna</label>
                            </div>
                            <div class="six-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Tour Option</label>
                                <label class="black">Entry Ticket</label>
                            </div>                           
                        </div>
                        <div class="columns">
                            <div class="six-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Slot</label>
                                <label class="black">Full day</label>
                            </div>
                             <div class="six-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Market</label>
                                <label class="black">All markets</label>
                            </div>
                        </div>

                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">You may select more than one date range to feed same rates in the different dates</small>
                        <label for="market" class="label "><b>Validity</b></label>
                        <div class="columns">
                            <div class="seven-columns">
                                <div class="columns">
                                    <div class="five-columns ten-columns-mobile five-columns-tablet mid-margin-bottom">
                                        <span class="input margin-right">
                                            <span class="icon-calendar"></span>
                                            <input type="text" class="input-unstyled datepicker" onchange="SetUIDates()" size="14" placeholder="Valid from">
                                        </span>
                                    </div>
                                    <div class="five-columns twelve-columns-mobile seven-columns-tablet mid-margin-bottom">
                                        <span class="input margin-right">
                                            <span class="icon-calendar"></span>
                                            <input type="text" class="input-unstyled datepicker" size="14" placeholder="Valid till">
                                        </span>
                                    </div>
                                    <div class="two-columns mid-margin-bottom no-margin-left">
                                        <span class="mid-margin-left icon-size2 icon-plus-round icon-black" onclick="" id=""></span>
                                    </div>
                                </div>
                                <div class="columns" style="display: none">
                                    <div class="five-columns ten-columns-mobile five-columns-tablet mid-margin-bottom">
                                        <span class="input margin-right">
                                            <span class="icon-calendar"></span>
                                            <input type="text" class="input-unstyled datepicker" size="14" placeholder="Valid from">
                                        </span>
                                    </div>
                                    <div class="five-columns twelve-columns-mobile seven-columns-tablet mid-margin-bottom">
                                        <span class="input margin-right">
                                            <span class="icon-calendar"></span>
                                            <input type="text" class="input-unstyled datepicker" size="14" placeholder="Valid till">
                                        </span>
                                    </div>
                                    <div class="two-columns mid-margin-bottom no-margin-left">
                                        <span class="mid-margin-left icon-size2 icon-plus-round icon-black" onclick="" id=""></span>
                                        <span class="mid-margin-left icon-size2 icon-minus-round icon-black" onclick="" id=""></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                                    
                    <div class="field-block button-height" style="padding-left: inherit;">
                        <!-- Activity Type wise rates -->
                        <div class="side-tabs same-height tabs-active" style="height: 57px;">
                            <!-- Tabs -->
                            <ul class="tabs" id="">
                                <li class="active" active="li_gn" id="">
                                    <a class="active strong align-center" href="#tab1">Bronze</a>
                                </li>
                                <li class="active" id="">
                                    <a class="strong align-center" href="#tab2">Silver</a>
                                </li>
                                <li class="active" id="">
                                    <a class="strong align-center" href="#tab3">Golden</a>
                                </li>
                            </ul>
                            <div class="tabs-content" style="height: auto !important; min-height: 56px;" id="div_GNtab">
                                <span class="tabs-back with-left-arrow top-bevel-on-light dark-text-bevel">Back</span>
                                <div id="tab1" class="tab-active" style="height: 283px;">
                                    <div class="with-padding" style="padding-bottom: 0px !important;">
                                        <small class="input-info">Cost Price is to be entered here</small>
                                        <div class="columns" style="display: ">
                                            <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">
                                                <label class="">Adult Rate </label>
                                                <span class="input tariff-textbox">
                                                    <label style="width: 30px" for="adultrate" class="button field-drop-input green-gradient sCurrency">AED</label>
                                                    <input style="width: 60px" type="text" name="adultrate" id="" class="input-unstyled <%--validate[required,custom[onlyNumberSp]]--%> align-right" placeholder="0.00">
                                                </span>
                                            </div>
                                            <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">
                                                <label class="">Child (4 to 10 yrs) </label>
                                                <span class="input tariff-textbox">
                                                    <label style="width: 30px" for="child1" class="button field-drop-input green-gradient sCurrency">AED</label>
                                                    <input style="width: 60px" type="text" name="child1" id="" class="input-unstyled <%--validate[required,custom[onlyNumberSp]]--%> align-right" placeholder="0.00">
                                                </span>
                                            </div>
                                            <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">
                                                <label class="">Child (2 to 4 yrs)</label>
                                                <span class="input tariff-textbox">
                                                    <label style="width: 30px" for="child2" class="button field-drop-input green-gradient sCurrency">AED</label>
                                                    <input style="width: 60px" type="text" name="child2" id="" class="input-unstyled <%--validate[required,custom[onlyNumberSp]]--%> align-right" placeholder="0.00">
                                                </span>
                                            </div>
                                            <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">
                                                <label class="">Infant (Below 2 yrs) </label>
                                                <span class="input tariff-textbox">
                                                    <label style="width: 30px" for="infant" class="button field-drop-input green-gradient sCurrency">AED</label>
                                                    <input style="width: 60px" type="text" name="infant" id="" class="input-unstyled <%--validate[required,custom[onlyNumberSp]]--%> align-right" placeholder="0.00">
                                                </span>
                                            </div>
                                            <small>
                                                <a class="pointer gray strong" onclick="showdaywise('GN_1')">Daywise Tariff
                                                    <input type="checkbox" id="" style="display: none">
                                                </a>
                                            </small>
                                        </div>
                                    </div>
                                    <div class="field-drop-tariff" id="" style="display: ;">
                                        <div class="standard-tabs at-bottom">
                                            <!-- Tabs -->
                                            <ul class="tabs">
                                                <li class="active"><a href="#cost-tkt-adult">Adult</a></li>
                                                <li><a href="#cost-tkt-child1">Child (4 to 10 yrs)</a></li>
                                                <li><a href="#cost-tkt-child2">Child (2 to 4 yrs)</a></li>
                                                <li><a href="#cost-tkt-infant">Infant</a></li>
                                            </ul>
                                            <!-- Content -->
                                            <div class="tabs-content">
                                                <div class="columns with-mid-padding" id="cost-tkt-adult">
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>
                                                            <input type="text" name="Mon" id="" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label><input type="text" name="Tue" id="txt_the_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label><input type="text" name="Wed" id="txt_Wed_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label><input type="text" name="Thu" id="txt_thu_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label><input type="text" name="Fri" id="txt_fri_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label><input type="text" name="Sat" id="txt_sat_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label><input type="text" name="Sun" id="txt_sun_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>

                                                </div>
                                                <div class="columns with-mid-padding" id="cost-tkt-child1">
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>
                                                            <input type="text" name="Mon" id="" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label><input type="text" name="Tue" id="txt_the_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label><input type="text" name="Wed" id="txt_Wed_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label><input type="text" name="Thu" id="txt_thu_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label><input type="text" name="Fri" id="txt_fri_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label><input type="text" name="Sat" id="txt_sat_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label><input type="text" name="Sun" id="txt_sun_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>

                                                </div>
                                                <div class="columns with-mid-padding" id="cost-tkt-child2">
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>
                                                            <input type="text" name="Mon" id="" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label><input type="text" name="Tue" id="txt_the_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label><input type="text" name="Wed" id="txt_Wed_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label><input type="text" name="Thu" id="txt_thu_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label><input type="text" name="Fri" id="txt_fri_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label><input type="text" name="Sat" id="txt_sat_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label><input type="text" name="Sun" id="txt_sun_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>

                                                </div>
                                                <div class="columns with-mid-padding" id="cost-tkt-infant">
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>
                                                            <input type="text" name="Mon" id="" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label><input type="text" name="Tue" id="txt_the_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label><input type="text" name="Wed" id="txt_Wed_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label><input type="text" name="Thu" id="txt_thu_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label><input type="text" name="Fri" id="txt_fri_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label><input type="text" name="Sat" id="txt_sat_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                    <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                        <span class="input">
                                                            <label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label><input type="text" name="Sun" id="txt_sun_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="with-padding" style="padding-bottom: 0px !important;">
                                        <small class="input-info">You may enter min selling price if you want to keep it different then costing</small>
                                        <div class="columns" style="display: ">
                                            <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">
                                                <label class="">Adult Rate </label>
                                                <span class="input tariff-textbox">
                                                    <label style="width: 30px" for="adultselling" class="button field-drop-input green-gradient sCurrency">AED</label>
                                                    <input style="width: 60px" type="text" name="adultselling" id="" class="input-unstyled <%--validate[required,custom[onlyNumberSp]]--%> align-right" placeholder="0.00">
                                                </span>
                                            </div>
                                            <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">
                                                <label class="">Child (4 to 10 yrs) </label>
                                                <span class="input tariff-textbox">
                                                    <label style="width: 30px" for="child1selling" class="button field-drop-input green-gradient sCurrency">AED</label>
                                                    <input style="width: 60px" type="text" name="child1selling" id="" class="input-unstyled <%--validate[required,custom[onlyNumberSp]]--%> align-right" placeholder="0.00">
                                                </span>
                                            </div>
                                            <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">
                                                <label class="">Child (2 to 4 yrs)</label>
                                                <span class="input tariff-textbox">
                                                    <label style="width: 30px" for="child2selling" class="button field-drop-input green-gradient sCurrency">AED</label>
                                                    <input style="width: 60px" type="text" name="child2selling" id="" class="input-unstyled <%--validate[required,custom[onlyNumberSp]]--%> align-right" placeholder="0.00">
                                                </span>
                                            </div>
                                            <div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">
                                                <label class="">Infant (Below 2 yrs) </label>
                                                <span class="input tariff-textbox">
                                                    <label style="width: 30px" for="infantselling" class="button field-drop-input green-gradient sCurrency">AED</label>
                                                    <input style="width: 60px" type="text" name="infantselling" id="" class="input-unstyled <%--validate[required,custom[onlyNumberSp]]--%> align-right" placeholder="0.00">
                                                </span>
                                            </div>
                                            <small>
                                                <a class="pointer gray strong" onclick="showdaywise('GN_1')">Daywise Tariff
                                                    <input type="checkbox" id="" style="display: none">
                                                </a>
                                            </small>
                                        </div>
                                    </div>
                                    <div class="silver-bg field-drop-tariff sMinCell" id="" style="display: none;">
                                        <div class="columns">
                                            <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                <span class="input">
                                                    <label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>
                                                    <input type="text" name="Mon" id="" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                </span>
                                            </div>
                                            <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                <span class="input">
                                                    <label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label><input type="text" name="Tue" id="txt_the_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                </span>
                                            </div>
                                            <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                <span class="input">
                                                    <label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label><input type="text" name="Wed" id="txt_Wed_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                </span>
                                            </div>
                                            <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                <span class="input">
                                                    <label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label><input type="text" name="Thu" id="txt_thu_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                </span>
                                            </div>
                                            <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                <span class="input">
                                                    <label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label><input type="text" name="Fri" id="txt_fri_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                </span>
                                            </div>
                                            <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                <span class="input">
                                                    <label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label><input type="text" name="Sat" id="txt_sat_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                </span>
                                            </div>
                                            <div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">
                                                <span class="input">
                                                    <label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label><input type="text" name="Sun" id="txt_sun_GN_1" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">
                                                </span>
                                            </div>
                                            <div class="twelve-columns twelve-columns-mobile"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Show only when SIC Tab is active -->
                    <div class="field-block button-height" style="display: block">
                        <small class="input-info">You may define more than one pickup & drop areas / location & timing </small>
                        <label for="market" class="label "><b>Transfer Details</b></label>
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile seven-columns-tablet small-margin-bottom">
                                <input type="text" class="input" value="" size="22" placeholder="Pickup from">
                            </div>
                            <div class="four-columns twelve-columns-mobile seven-columns-tablet small-margin-bottom">
                                <div class="columns">
                                    <div class="six-columns small-margin-bottom">
                                        <span class="number input margin-right">
                                            <label style="width: 20px" for="sic-from-hr" class="button field-drop-input grey-gradient">Hrs</label>
                                            <button type="button" class="button number-down">-</button>
                                            <input type="text" value="1" size="3" class="input-unstyled" data-number-options='{"min":1,"max":24,"increment":1.0,"shiftIncrement":5,"precision":0.25}'>
                                            <button type="button" class="button number-up">+</button>
                                        </span>
                                    </div>
                                    <div class="six-columns small-margin-bottom">
                                        <span class="number input margin-right">
                                            <label style="width: 20px" for="sic-from-min" class="button field-drop-input grey-gradient">Min</label>
                                            <button type="button" class="button number-down">-</button>
                                            <input type="text" value="00" size="3" class="input-unstyled" data-number-options='{"min":0,"max":50,"increment":10,"shiftIncrement":5,"precision":1}'>
                                            <button type="button" class="button number-up">+</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--<small class="red strong">You may select more than one pickup location for same rate</small>--%>
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile seven-columns-tablet">
                                <input type="text" class="input" value="" size="22" placeholder="Drop to">
                            </div>
                            <div class="four-columns twelve-columns-mobile seven-columns-tablet">
                                <div class="columns">
                                    <div class="six-columns small-margin-bottom">
                                        <span class="number input margin-right">
                                            <label style="width: 20px" for="sic-to-hr" class="button field-drop-input grey-gradient">Hrs</label>
                                            <button type="button" class="button number-down">-</button>
                                            <input type="text" value="1" size="3" class="input-unstyled" data-number-options='{"min":1,"max":24,"increment":1.0,"shiftIncrement":5,"precision":0.25}'>
                                            <button type="button" class="button number-up">+</button>
                                        </span>
                                    </div>
                                    <div class="six-columns small-margin-bottom">
                                        <span class="number input margin-right">
                                            <label style="width: 20px" for="sic-to-min" class="button field-drop-input grey-gradient">Min</label>
                                            <button type="button" class="button number-down">-</button>
                                            <input type="text" value="00" size="3" class="input-unstyled" data-number-options='{"min":0,"max":50,"increment":10,"shiftIncrement":5,"precision":1}'>
                                            <button type="button" class="button number-up">+</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="one-column">
                                <span class="mid-margin-left icon-size2 icon-plus-round icon-black" onclick="" id=""></span>
                            </div>
                        </div>
                    </div>

                </fieldset>
                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Conditions</legend>
                     <div class="field-block button-height">
                        <label class="label"><b>Rates for</b></label>
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Supplier</label>
                                <label class="black">Rayna</label>
                            </div>                            
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Market</label>
                                <label class="black">All markets</label>
                            </div>
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Validity</label>
                                <label class="black">01-06-2019 to 31-08-2019</label>
                            </div>
                        </div>
                    </div>

                    <div class="field-block button-height">
                        <label class="label"><b>Canellation Policy</b></label>
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile no-margin-bottom">
                                <label class="">Is this tariff refundable? </label>
                                <input type="checkbox" class="switch medium" checked data-text-on="YES" data-text-off="NO">
                            </div>
                            <div class="eight-columns twelve-columns-mobile" id="cnlpolicy" style="display: block">
                                <input type="radio" class="radio " id="percentage" value="1" name="chk_Charge" checked="checked" onclick="GetChargeCond(this, 'percentageinput')">
                                <label for="percentage">Charges in percentage</label>

                                <input type="radio" class="radio mid-margin-left" id="amount" value="2" name="chk_Charge" onclick="GetChargeCond(this, 'amountinput')">
                                <label for="amount">Charges in amount</label>
                            </div>
                        </div>
                    </div>
                    <%-- Charges in percentage --%>
                    <div class="field-drop button-height black-inputs" id="percentageinput" style="display:none ">
                        <small class="input-info silver">Specify the charges in percentage in the event of cancellation</small>
                        <label for="" class="label"><b>Charges in percentage (%)</b></label>
                        <div class="columns">
                            <div class="three-columns twelve-columns-mobile six-columns-tablet">
                                <select class="validate[required] select expandable-list anthracite-gradient" id="sel_ChargePer">
                                    <option value="">Applicable Charges</option>
                                    <option value="0%">No charges will apply</option>
                                    <option value="10%">10% will be charged</option>
                                    <option value="20%">20% will be charged</option>
                                    <option value="30%">30% will be charged</option>
                                    <option value="40%">40% will be charged</option>
                                    <option value="50%">50% will be charged</option>
                                    <option value="60%">60% will be charged</option>
                                    <option value="70%">70% will be charged</option>
                                    <option value="80%">80% will be charged</option>
                                    <option value="90%">90% will be charged</option>
                                    <option value="100%">100% will be charged</option>
                                </select>
                            </div>
                            <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                <select class="select expandable-list anthracite-gradient validate[required]" tabindex="0" id="sel_DayPer">
                                    <option value="">how long before service? </option>
                                    <option value="4hrs">until 12 Hrs before service time</option>
                                    <option value="6hrs">until 12 Hrs before service time</option>
                                    <option value="12hrs">until 12 Hrs before service time</option>
                                    <option value="18hrs">until 18 Hrs before service time</option>
                                    <option value="1days">until 1 day before service date</option>
                                    <option value="1days">until 1 day before service date</option>
                                    <option value="2days">until 2 days before service date</option>
                                    <option value="3days">until 3 days before service date</option>
                                    <option value="5days">until 5 days before service date</option>
                                    <option value="10days">until 10 days before service date</option>
                                    <option value="15days">until 15 days before service date</option>
                                    <option value="20days">until 20 days before service date</option>
                                    <option value="25days">until 25 days before service date</option>
                                    <option value="30days">until 30 days before service date</option>
                                    <option value="45days">until 45 days before service date</option>
                                    <option value="60days">until 60 days before service date</option>                                               
                                </select>
                            </div>
                            <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                <select class="select expandable-list anthracite-gradient validate[required]" tabindex="0" id="sel_NoShowPer">
                                    <option value="" class="select-value">in case of No Show </option>
                                    <option value="100%">100% charged in case of No Show</option>
                                    <option value="Same">Same as cancellation charge</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <%-- Charges in amount --%>
                    <div class="field-drop button-height black-inputs" id="amountinput" style="display: ;">
                        <small class="input-info silver">Specify the charges in amount, in the event of cancellation</small>
                        <label for="" class="label"><b>Charges in amount</b></label>
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile four-columns-tablet">
                                <span class="input">
                                    <label style="width: 30px" for="cancellationamount" class="button green-gradient sCurrency">AED</label>
                                    <input style="width: 60px" type="text" name="cancellationamount" id="cancellationamount" class="input-unstyled  <%--validate[required]--%>"  placeholder="12,345.00">
                                </span>
                                <label class="white font13 strong">will be charged</label>
                            </div>
                            <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                <select class="select expandable-list anthracite-gradient  validate[required]" tabindex="0" id="sel_DayAmt">
                                    <option value="">how long before service? </option>
                                    <option value="4hrs">until 12 Hrs before service time</option>
                                    <option value="6hrs">until 12 Hrs before service time</option>
                                    <option value="12hrs">until 12 Hrs before service time</option>
                                    <option value="18hrs">until 18 Hrs before service time</option>
                                    <option value="1days">until 1 day before service date</option>
                                    <option value="1days">until 1 day before service date</option>
                                    <option value="2days">until 2 days before service date</option>
                                    <option value="3days">until 3 days before service date</option>
                                    <option value="5days">until 5 days before service date</option>
                                    <option value="10days">until 10 days before service date</option>
                                    <option value="15days">until 15 days before service date</option>
                                    <option value="20days">until 20 days before service date</option>
                                    <option value="25days">until 25 days before service date</option>
                                    <option value="30days">until 30 days before service date</option>
                                    <option value="45days">until 45 days before service date</option>
                                    <option value="60days">until 60 days before service date</option>
                                </select>
                            </div>
                            <div class="three-columns twelve-columns-mobile six-columns-tablet">
                                <select class="select expandable-list anthracite-gradient  validate[required]" id="sel_NoShowAmt" >
                                    <option value="" class="select-value">in case of No Show </option>
                                    <option value="100%">100% charged in case of No Show</option>
                                    <option value="same">Same as cancellation charge</option>
                                </select>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="field-block button-height">                        
                        <label class="label"><b>Inclusion & Exclusion</b></label>
                        <div class="columns">
                            <div class="five-columns">
                                <small class="input-info">Mention what are the <i class="red">inclusions</i> in the rate</small>
                                <input type="text" class="input full-width mid-margin-bottom" placeholder="e.g: Entry Ticket">                                
                                    <p class="selected-inclusion">
                                       Dinner Dhow Cruise at Cruise
                                        <i class="icon-cross pointer" aria-hidden="true" style="padding-left: 5px" onclick=""></i>
                                    </p>
                                    <p class="selected-inclusion">
                                        Cruising Traditional Dhow Cruise At Creek
                                        <i class="icon-cross pointer" aria-hidden="true" style="padding-left: 5px" onclick=""></i>
                                    </p>
                                    <p class="selected-inclusion">
                                        Cruising Traditional Dhow Cruise At Creek
                                        <i class="icon-cross pointer" aria-hidden="true" style="padding-left: 5px" onclick=""></i>
                                    </p> 
                                <p class="selected-inclusion">
                                        Cruising Traditional Dhow Cruise At Creek
                                        <i class="icon-cross pointer" aria-hidden="true" style="padding-left: 5px" onclick=""></i>
                                    </p>                               
                            </div>
                            <div class="five-columns">
                                <small class="input-info">Mention what are the <i class="red">Exclusions</i> in the rate</small>
                                <input type="text" class="input full-width mid-margin-bottom" placeholder="e.g: Entry Ticket">
                                <p class="selected-inclusion">
                                    Dinner Dhow Cruise at Cruise
                                        <i class="icon-cross pointer" aria-hidden="true" style="padding-left: 5px" onclick=""></i>
                                </p>
                                <p class="selected-inclusion">
                                    Cruising Traditional Dhow Cruise At Creek
                                        <i class="icon-cross pointer" aria-hidden="true" style="padding-left: 5px" onclick=""></i>
                                </p>
                                <p class="selected-inclusion">
                                    Cruising Traditional Dhow Cruise At Creek
                                        <i class="icon-cross pointer" aria-hidden="true" style="padding-left: 5px" onclick=""></i>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">If there are any conditions to book this Tour / Sightseeing</small>
                        <label class="label"><b>Min Pax Conditions</b></label>
                        <%--<div class="six-columns small-margin-bottom">--%>
                            <label>To book this tour min </label>
                            <span class="number input small-margin-right">                               
                                <button type="button" class="button number-down">-</button>
                                <input type="text" value="0" size="3" class="input-unstyled" data-number-options='{"min":1,"shiftIncrement":5}'>
                                <button type="button" class="button number-up">+</button>
                            </span>
                            <label>pax & max </label>
                            <span class="number input small-margin-right">                               
                                <button type="button" class="button number-down">-</button>
                                <input type="text" value="0" size="3" class="input-unstyled" data-number-options='{"min":1,"shiftIncrement":5}'>
                                <button type="button" class="button number-up">+</button>
                            </span>
                            <label>pax are required </label>
                        <%--</div>--%>
                    </div>

                    <div class="field-block button-height">
                        <small class="input-info">Any important note regarding to this rates should mention here</small>
                        <label class="label"><b>Tariff Note</b></label>
                       <p>
                            <textarea class="input full-width autoexpanding"></textarea>
                        </p>
                    </div>

                </fieldset>
                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Review</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Main Details</b></label>
                        <div class="columns">
                            <div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Supplier</label>
                                <p class="black selected-inclusion">Rayna</p>
                            </div>
                            <div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Market</label>
                                <p class="black selected-inclusion">All markets</p>
                            </div>
                            <div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Validity</label>
                                <p class="black selected-inclusion">01-06-2019 to 31-08-2019</p>
                                <p class="black selected-inclusion">01-06-2019 to 31-08-2019</p>
                            </div>
                            <div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Slot Timing</label>
                                <p class="black selected-inclusion">10:30 to 11:30</p>
                                <p class="black selected-inclusion">10:30 to 11:30</p>
                            </div>
                        </div>
                    </div>
                    <div class="field-block button-height">
                        <label class="label"><b>Entry Ticket</b></label>
                        <div class="columns" style="display:">
                            <div class="one-column twelve-columns-mobile mid-margin-bottom">
                                <label class="red strong">Prime Time</label>
                                <p class="black selected-inclusion"><b>Cost</b></p>
                                <p class="blue selected-inclusion"><b>Selling</b></p>
                            </div>
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Adult Rate</label>
                                <p class="black selected-inclusion">AED 1000.00</p>
                                <p class="blue selected-inclusion">AED 1000.00</p>
                            </div>

                            <div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Child (4 to 10 Yrs)</label>
                                <p class="black selected-inclusion">AED 1000.00</p>
                                <p class="blue selected-inclusion">AED 1000.00</p>
                            </div>
                             <div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Child (2 to 4 Yrs)</label>
                                <p class="black selected-inclusion">AED 1000.00</p>
                                 <p class="blue selected-inclusion">AED 1000.00</p>
                            </div>
                            <div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Infant (Below 2 Yrs)</label>
                                <p class="black selected-inclusion">AED 1000.00</p>
                                <p class="blue selected-inclusion">AED 1000.00</p>
                            </div>
                        </div>
                        <div class="columns" style="display:none">
                            <hr />
                            <div class="one-column twelve-columns-mobile mid-margin-bottom">
                                <label class="red strong">Silver</label>
                                <p class="black selected-inclusion"><b>Cost</b></p>
                                <p class="blue selected-inclusion"><b>Selling</b></p>
                            </div>
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Adult Rate</label>
                                <p class="black selected-inclusion">AED 1000.00</p>
                                <p class="blue selected-inclusion">AED 1000.00</p>
                            </div>

                            <div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Child (4 to 10 Yrs)</label>
                                <p class="black selected-inclusion">AED 1000.00</p>
                                <p class="blue selected-inclusion">AED 1000.00</p>
                            </div>
                             <div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Child (2 to 4 Yrs)</label>
                                <p class="black selected-inclusion">AED 1000.00</p>
                                 <p class="blue selected-inclusion">AED 1000.00</p>
                            </div>
                            <div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Infant (Below 2 Yrs)</label>
                                <p class="black selected-inclusion">AED 1000.00</p>
                                <p class="blue selected-inclusion">AED 1000.00</p>
                            </div>
                        </div>

                        <div class="columns" style="display:">
                            <div class="two-columns twelve-columns-mobile align-center">
                                <label class="red strong">Adult</label>
                                <p class="black strong selected-inclusion">Cost Rate</p>
                                <p class="blue strong selected-inclusion">Min Selling</p>
                                <br />
                                <label class="green strong font16">Bronze</label>
                            </div>
                            <div class="ten-columns twelve-columns-mobile">
                                <div class="columns">
                                    <div class="three-columns four-columns-mobile mid-margin-bottom align-center">
                                        <label class="green strong">Monday</label>
                                        <p class="black selected-inclusion">AED 1000.00</p>
                                        <p class="blue selected-inclusion">AED 1000.00</p>
                                    </div>
                                    <div class="three-columns four-columns-mobile mid-margin-bottom align-center">
                                        <label class="green strong">Tuesday</label>
                                        <p class="black selected-inclusion">AED 1000.00</p>
                                        <p class="blue selected-inclusion">AED 1000.00</p>
                                    </div>
                                    <div class="three-columns four-columns-mobile mid-margin-bottom align-center">
                                        <label class="green strong">Wednesday</label>
                                        <p class="black selected-inclusion">AED 1000.00</p>
                                        <p class="blue selected-inclusion">AED 1000.00</p>
                                    </div>
                                    <div class="three-columns four-columns-mobile mid-margin-bottom align-center">
                                        <label class="green strong">Thursday</label>
                                        <p class="black selected-inclusion">AED 1000.00</p>
                                        <p class="blue selected-inclusion">AED 1000.00</p>
                                    </div>
                                    <div class="three-columns four-columns-mobile mid-margin-bottom align-center">
                                        <label class="green strong">Friday</label>
                                        <p class="black selected-inclusion">AED 1000.00</p>
                                        <p class="blue selected-inclusion">AED 1000.00</p>
                                    </div>
                                    <div class="three-columns four-columns-mobile mid-margin-bottom align-center">
                                        <label class="green strong">Saturday</label>
                                        <p class="black selected-inclusion">AED 1000.00</p>
                                        <p class="blue selected-inclusion">AED 1000.00</p>
                                    </div>
                                    <div class="three-columns four-columns-mobile mid-margin-bottom align-center">
                                        <label class="green strong">Sunday</label>
                                        <p class="black selected-inclusion">AED 1000.00</p>
                                        <p class="blue selected-inclusion">AED 1000.00</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="columns" style="display:">
                            <hr />
                            <div class="two-columns twelve-columns-mobile align-center">
                                <label class="red strong">Child (4 to 10 Yrs)</label>
                                <p class="black strong selected-inclusion">Cost Rate</p>
                                <p class="blue strong selected-inclusion">Min Selling</p>
                                <br />
                                <label class="green strong font16">Bronze</label>
                            </div>
                            <div class="ten-columns twelve-columns-mobile">
                                <div class="columns">
                                    <div class="three-columns four-columns-mobile mid-margin-bottom align-center">
                                        <label class="green strong">Monday</label>
                                        <p class="black selected-inclusion">AED 1000.00</p>
                                        <p class="blue selected-inclusion">AED 1000.00</p>
                                    </div>
                                    <div class="three-columns four-columns-mobile mid-margin-bottom align-center">
                                        <label class="green strong">Tuesday</label>
                                        <p class="black selected-inclusion">AED 1000.00</p>
                                        <p class="blue selected-inclusion">AED 1000.00</p>
                                    </div>
                                    <div class="three-columns four-columns-mobile mid-margin-bottom align-center">
                                        <label class="green strong">Wednesday</label>
                                        <p class="black selected-inclusion">AED 1000.00</p>
                                        <p class="blue selected-inclusion">AED 1000.00</p>
                                    </div>
                                    <div class="three-columns four-columns-mobile mid-margin-bottom align-center">
                                        <label class="green strong">Thursday</label>
                                        <p class="black selected-inclusion">AED 1000.00</p>
                                        <p class="blue selected-inclusion">AED 1000.00</p>
                                    </div>
                                    <div class="three-columns four-columns-mobile mid-margin-bottom align-center">
                                        <label class="green strong">Friday</label>
                                        <p class="black selected-inclusion">AED 1000.00</p>
                                        <p class="blue selected-inclusion">AED 1000.00</p>
                                    </div>
                                    <div class="three-columns four-columns-mobile mid-margin-bottom align-center">
                                        <label class="green strong">Saturday</label>
                                        <p class="black selected-inclusion">AED 1000.00</p>
                                        <p class="blue selected-inclusion">AED 1000.00</p>
                                    </div>
                                    <div class="three-columns four-columns-mobile mid-margin-bottom align-center">
                                        <label class="green strong">Sunday</label>
                                        <p class="black selected-inclusion">AED 1000.00</p>
                                        <p class="blue selected-inclusion">AED 1000.00</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="field-block button-height">
                        <label class="label"><b>Rates for</b></label>
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Supplier</label>
                                <label class="black">Rayna</label>
                            </div>
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Market</label>
                                <label class="black">All markets</label>
                            </div>
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Validity</label>
                                <label class="black">01-06-2019 to 31-08-2019</label>
                            </div>
                        </div>
                    </div>
                    <div class="field-block button-height">
                        <label class="label"><b>Rates for</b></label>
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Supplier</label>
                                <label class="black">Rayna</label>
                            </div>
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Market</label>
                                <label class="black">All markets</label>
                            </div>
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Validity</label>
                                <label class="black">01-06-2019 to 31-08-2019</label>
                            </div>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </section>
</asp:Content>

