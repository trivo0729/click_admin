﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="TravelAgent.aspx.cs" Inherits="CutAdmin.TravelAgent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <script>
       $(function () {
           $("#timepicker").kendoTimePicker({
               interval: 10,
               format: "HH:mm"
           }); /*Date Time Picker*/

           //$("#kUI_calendar").kendoCalendar();    /*kendoCalendar*/
           $("#kUI_Datepicker").kendoDatePicker({   /*kendoCalendar*/
               format: "d-MM-yyyy"
           });

           $('#kUI_cmb').kendoComboBox({ /*kendoComboBox*/
               dataTextField: "text",
               dataValueField: "value",
               dataSource: [
                 { text: "Cotton", value: "1" },
                 { text: "Polyester", value: "2" },
                 { text: "Cotton/Polyester", value: "3" },
                 { text: "Rib Knit", value: "4" }
               ],
               filter: "contains",
               suggest: true,
               index: 3
           });
           //$("#required").kendoMultiSelect();/*kendoMultiSelect*/


           $("#required").kendoMultiSelect({
               dataSource: searchDataSource,
               autoBind: false,
               minLength: 3,
               placeholder: 'Search Timekeepers...',
               dataTextField: 'label',
               dataTextValue: 'value',
               delay: 200
           }).data("kendoMultiSelect");
           var searchDataSource = new kendo.data.DataSource({
               transport: {
                   read: function (options) {
                       $.ajax({
                           type: 'POST',
                           url: "GenralHandler.asmx/GetCity",
                           contentType: 'application/json; charset=utf-8',
                           data: {
                               //searchValue: function () {
                               //    // better: use a model property instead of this
                               //    return $("#required").data('kendoMaskedTextBox').value();
                               //}
                               country: function () {
                                   // better: use a model property instead of this
                           return $("#required").val();
                       }
                           },
                           success: function (data) {
                               options.success(data.RESULT);
                           }
                       });
                   }
               },
               group: { field: 'category' },
               serverFiltering: true
           });
       });

   
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         <section role="main" id="main">
                 <div class="with-padding ">
                     <div class="columns">

                    
                     <div class="two-columns four-columns-mobile">
                                      <label class="uk-form-label" for="kUI_timepicker">Basic Example</label>
                                <input id="timepicker"  />
                     </div>
                     <div class="three-columns">
                         <br />
                          <input id="kUI_Datepicker" type="text"  />
                     </div>

                     <div class="three-columns">
                          <input id="kUI_cmb" type="text"  />
                     </div>



                   <div class="three-columns">
                        <br />
                      <select id="required" class="" multiple="multiple" data-placeholder="Select attendees...">
            <option>Steven White</option>
            <option>Nancy King</option>
            <option>Nancy Davolio</option>
            <option>Robert Davolio</option>
            <option>Michael Leverling</option>
            <option>Andrew Callahan</option>
            <option>Michael Suyama</option>
            <option selected>Anne King</option>
            <option>Laura Peacock</option>
            <option>Robert Fuller</option>
            <option>Janet White</option>
            <option>Nancy Leverling</option>
            <option>Robert Buchanan</option>
            <option>Margaret Buchanan</option>
            <option selected>Andrew Fuller</option>
            <option>Anne Davolio</option>
            <option>Andrew Suyama</option>
            <option>Nige Buchanan</option>
            <option>Laura Fuller</option>
        </select>
                     
                   </div>
                     </div>
                      </div>

                        </section>
</asp:Content>
