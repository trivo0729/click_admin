﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="VehicleRegistrationDetails.aspx.cs" Inherits="CutAdmin.VehicleRegistrationDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/VehicleRegistrationDetails.js?v=1.0"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Vehicle Registration
 Details</h1>
            <hr />
            <%--<h2><a href="VehicleRegistration.aspx" class="addnew"><i class="fa fa-user-plus"></i></a><a href="#" class="addnew"><i class="fa fa-filter"></i></a></h2>--%>
            <h2><a href="VehicleRegistration.aspx" class="addnew"><i class="fa fa-user-plus"></i></a>
                <a href="#" class="addnew" id="btn_filter"><i class="fa fa-filter"></i></a></h2>
            <br />
            <div id="filter" class="with-padding anthracite-gradient" style="display: none;">
                <form class="form-horizontal">
                    <div class="columns">
                        <div class="four-columns twelve-columns-mobile four-columns-tablet bold">
                            <label>Supplier</label><br />
                            <div class="full-width button-height" id="DivSupplier">
                                <select id="selSupplier" name="validation-select" class="select OfferType" >
                                    <option value="-">-Select-</option>
                                    <option value="Own">Own </option>
                                    <option value="Attached">Attached </option>
                                    <option value="Supplier">Supplier</option>
                                </select>
                            </div>
                        </div>
                        <div class="four-columns twelve-columns-mobile four-columns-tablet">
                            <label>Vehicle Modal </label>
                            <br />
                            <div class="full-width button-height" id="DivVehicleModal">
                                <select id="selVehicleModal" name="validation-select" class="select OfferType">
                                    <%--  <option value="-">-Select-</option>
                                <option value="Own">Own </option>
                                <option value="Attached">Attached </option>
                                <option value="Supplier">Supplier</option>--%>
                                </select>
                            </div>
                        </div>
                        <div class="three-columns four-columns-tablet twelve-columns-mobile">
                            <label>Registration Date </label><br />
                            <div class="input full-width">
                                <input name="datepicker" id="txt_RegistrationDate" placeholder="Date" value="" class="input-unstyled full-width" type="text">
                            </div>
                        </div>
                         <div class="two-columns twelve-columns-mobile formBTn">
                            <br />
                            <button type="button" class="button anthracite-gradient" onclick="Search()">Search</button>
                            <button type="button" class="button anthracite-gradient" onclick="reset();">Reset</button>

                        </div>
                        <div class="two-columns four-columns-tablet twelve-columns-mobile">
                            <br />
                            <span class="icon-pdf right" onclick="ExportVehicleDetailsToExcel('PDF')">
                                <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                            </span>
                            <span class="icon-excel right" onclick="ExportVehicleDetailsToExcel('excel')">
                                <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </hgroup>
        <div class="with-padding">
            <div class="respTable">
                <table class="table responsive-table font11" id="tbl_VehicleDetails">
                    <thead>
                        <tr>
                            <th scope="col" align="center">S.No</th>
                            <th scope="col" align="center">Supplier</th>
                            <th scope="col" class="align-center hide-on-mobile">Vehicle Model</th>
                            <th scope="col" class="align-center hide-on-mobile-portrait">Registration Validity</th>
                            <th scope="col" class="hide-on-tablet" align="center">INSURANCE DUE DATE</th>
                            
                            <%--<th scope="col" class="hide-on-tablet">Edit</th>--%>
                            <%--<th scope="col" class="align-center">AMENITIES</th>--%>
                            <th scope="col" class="align-center hide-on-mobile">Action</th>
                            <%--<th scope="col" class="align-center" style="width: 30px">Delete</th>--%>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <script>
        jQuery(document).ready(function () {
            jQuery('#btn_filter').click(function () {
                jQuery(' #filter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #filter').slideUp();
                });
            });
        });
    </script>
</asp:Content>

