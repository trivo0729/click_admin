﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="BookingList.aspx.cs" Inherits="CutAdmin.BookingList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/libs/DataTables/AjaxDataTable.js?v=1.13"></script>
    <script src="Scripts/Invoice.js?v=1.5"></script>
    <script src="Scripts/ActivityBookingList.js?v=1.6"></script>
    <script src="Scripts/BookingList.js?v=1.10"></script>
    <script src="Scripts/Booking.js?v=1.20"></script>
    <script src="Scripts/FlightBookings.js?v=1.9"></script>
    <script src="Scripts/VisaSearch.js?v=1.9"></script>
    <script src="Scripts/PackageBookingReport.js?v=1.6"></script>
    <script src="Scripts/OTBReporting.js"></script>
    <script src="Scripts/CancellationPolicy.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment-with-locales.min.js"></script>
    <script src="js/moment.js"></script>
    <%--<link href="css/jquery-ui.css" rel="stylesheet" />--%>
    <style>
        #ConfirmDate {
            z-index: 99999999;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <div class="standard-tabs">
            <ul class="tabs" id="TAB" runat="server">
                <li runat="server" class="active" id="li_Hotel"><a href="#hotel">Hotel</a></li>
                <li runat="server" class="" id="li_Airline" onclick='GetFlightsBookings()'><a href="#Airline">Airline</a></li>
                <li runat="server" class="" id="li_Sightseeing" onclick='GetActBookingList()'><a href="#Sightseeing">Sightseeing</a></li>
                <li runat="server" class="" id="li_Visa" onclick='GetVisa()'><a href="#Visa">Visa</a></li>
            <%-- <li runat="server" class="" id="li_OTB" onclick='GetAllOtbDetails()'><a href="#OTB">OTB</a></li>--%>
                <li runat="server" class="" id="li_OTB" onclick='GetOTB()'><a href="#OTB">OTB</a></li>
                <li runat="server" class="" id="li_Package" onclick='GetPackageBookings()'><a href="#Package">Package</a></li>
            </ul>

            <div class="tabs-content">
                <!-- Hotel Tab -->
                <div id="hotel" class="with-small-padding">
                    <div style="text-align: right">
                        <h3 style="margin: 0px"><a href="#" class="addnew"><i class="fa fa-filter"></i></a></h3>
                    </div>
                    <div id="filter" class="with-padding anthracite-gradient" style="display: none;">
                        <form class="form-horizontal">
                            <div class="columns">
                                <div class="three-columns twelve-columns-mobile">
                                    <label>Check-In</label>
                                    <span class="input full-width">
                                        <span class="icon-calendar"></span>
                                        <input type="text" name="datepicker" id="Check-In" class="input-unstyled" value="">
                                    </span>
                                </div>
                                <div class="three-columns twelve-columns-mobile">
                                    <label>Check-Out</label>
                                    <span class="input full-width">
                                        <span class="icon-calendar"></span>
                                        <input type="text" name="datepicker" id="Check-Out" class="input-unstyled" value="">
                                    </span>
                                </div>
                                <div class="three-columns twelve-columns-mobile">
                                    <label>Booking Date</label>
                                    <span class="input full-width">
                                        <span class="icon-calendar"></span>
                                        <input type="text" name="datepicker" id="Bookingdate" class="input-unstyled" value="">
                                    </span>
                                </div>
                                <div class="three-columns twelve-columns-mobile bold">
                                    <label>Passenger Name </label>
                                    <div class="input full-width">
                                        <input type="text" id="txt_Passenger" class="input-unstyled full-width">
                                    </div>
                                </div>
                            </div>
                            <div class="columns">
                                <div class="three-columns twelve-columns-mobile bold">
                                    <label>Reference No.</label>
                                    <div class="input full-width">
                                        <input type="text" id="txt_Reference" class="input-unstyled full-width">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile bold">
                                    <label>Hotel Name</label>
                                    <div class="input full-width">
                                        <input type="text" id="txt_Hotel" class="input-unstyled full-width">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile bold">
                                    <label>Location</label>
                                    <div class="input full-width">
                                        <input type="text" id="txt_Location" class="input-unstyled full-width">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile bold">
                                    <label>Reservation Status</label>
                                    <div class="full-width button-height typeboth">
                                        <select id="selReservation" class="select">
                                            <option selected="selected">All</option>
                                            <option>Vouchered</option>
                                            <option>Cancelled</option>
                                            <option>OnRequest</option>
                                            <option>Reconfirmed</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="columns">
                                <div class="eight-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                </div>
                                <div class="two-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                    <button type="button" class="button anthracite-gradient" onclick="SearchBooking()">Search</button>
                                    <button type="button" class="button anthracite-gradient" onclick="Reset()">Reset</button>
                                </div>
                                <div class="two-columns twelve-columns-mobile bold text-alignright">
                                    <span class="icon-pdf right" onclick="ExportBookingDetailsToExcel('PDF')">
                                        <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                                    </span>
                                    <span class="icon-excel right" onclick="ExportBookingDetailsToExcel('excel')">
                                        <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                                </div>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                    <div class="respTable">
                        <table class="table responsive-table font11" id="tbl_BookingList">
                            <thead>
                                <tr>
                                    <%-- <th scope="col">S.N</th>--%>
                                    <th scope="col" class="align-center">Date</th>
                                    <th scope="col" class="align-center">Ref No.</th>
                                    <th scope="col" class="align-center">Agency</th>
                                    <th scope="col" class="align-center">Passenger</th>
                                    <th scope="col" class="align-center">Hotel &amp; Location</th>
                                    <th scope="col" class="align-center">Stay</th>
                                    <th scope="col" class="align-center">Room</th>
                                    <th scope="col" class="align-center">Status</th>
                                    <th scope="col" class="align-center">Amount</th>
                                    <th scope="col" class="align-center">Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div id="Sightseeing" class="with-small-padding">

                    <div style="text-align: right">
                        <h3 style="margin: 0px"><a href="#" class="addnew"><i class="fa fa-filter Sightfilter"></i></a></h3>
                    </div>


                    <div id="Sightseeingfilter" class="with-padding anthracite-gradient" style="display: none;">
                        <form class="form-horizontal">
                            <div class="columns">
                                <div class="three-columns twelve-columns-mobile">
                                    <label>Booking Date</label>
                                    <span class="input full-width">
                                        <span class="icon-calendar"></span>
                                        <input type="text" name="datepicker" id="actbookdate" class="input-unstyled" value="">
                                    </span>
                                </div>
                                <div class="three-columns twelve-columns-mobile">
                                    <label>Activity Date</label>
                                    <span class="input full-width">
                                        <span class="icon-calendar"></span>
                                        <input type="text" name="datepicker" id="actdate" class="input-unstyled" value="">
                                    </span>
                                </div>

                                <div class="three-columns twelve-columns-mobile bold">
                                    <label>Agency/Passenger Name </label>
                                    <div class="input full-width">
                                        <input type="text" id="txt_actpassenger" class="input-unstyled full-width">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile bold">
                                    <label>Activity Name</label>
                                    <div class="input full-width">
                                        <input type="text" id="txt_actname" class="input-unstyled full-width">
                                    </div>
                                </div>
                            </div>
                            <div class="columns">

                                <div class="three-columns twelve-columns-mobile bold">
                                    <label>Reservation Status</label>
                                    <div class="full-width button-height typeboth">
                                        <select id="selactStatus" class="select">
                                            <option selected="selected" value="All">All</option>
                                            <option value="Vouchered">Vouchered</option>
                                            <option value="Cancelled">Cancelled</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="five-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                </div>
                                <div class="two-columns twelve-columns-mobile formBTn searhbtn text-alignright">

                                    <button type="button" class="button anthracite-gradient" onclick="SearchAct()">Search</button>
                                    <button type="button" class="button anthracite-gradient" onclick="ResetAct()">Reset</button>
                                </div>
                                <div class="two-columns twelve-columns-mobile bold text-alignright">

                                    <span class="icon-pdf right" onclick="ExportActBookingDetailsToExcel('PDF')">
                                        <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                                    </span>
                                    <span class="icon-excel right" onclick="ExportActBookingDetailsToExcel('excel')">
                                        <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                                </div>
                            </div>
                            <%--<div class="columns">
                                <div class="eight-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                </div>
                                
                            </div>--%>
                        </form>
                        <div class="clearfix"></div>
                    </div>

                    <div class="respTable">
                        <table class="table responsive-table font11" id="tbl_Sightseeing">
                            <thead>
                                <tr>
                                    <%--<th scope="col" style="max-width: 20px; vertical-align: middle;" class="align-center">S.N</th>--%>
                                    <th scope="col" class="align-center" style="min-width: 60px; vertical-align: middle;" class="center">Booking Date</th>
                                    <th scope="col" class="align-center">Activity Date</th>
                                    <th scope="col" class="align-center">Agency / Customer</th>
                                    <th scope="col" class="align-center">Activity Name & Location </th>
                                    <%--<th scope="col"  class="align-center">Travllers Name</th>--%>
                                    <th scope="col" class="align-center">Status</th>
                                    <th scope="col" class="align-center">Total Fare</th>
                                    <!--<th scope="col" style="max-width: 115px; vertical-align: middle;" class="align-center">Sell</th>-->
                                    <%--<th scope="col" class="align-center">Payment Mode</th>--%>
                                    <th scope="col" class="align-center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>


                <div id="Airline" class="with-small-padding">
                    <div style="text-align: right">
                        <h3 style="margin: 0px"><a href="#" class="addnew"><i class="fa fa-filter Airline"></i></a></h3>
                    </div>
                    <div id="AirlineFilter" class="with-padding anthracite-gradient" style="display: none;">
                        <form class="form-horizontal">
                            <div class="columns">
                                <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                    <label>Transaction No:</label>
                                    <div class="input full-width">
                                        <input type="text" class="input-unstyled full-width" id="txt_transaction">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                    <label>Agency Name :</label>
                                    <div class="input full-width">
                                        <input type="text" class="input-unstyled full-width" id="txt_Agency">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                    <label>Travellers Name:</label>
                                    <div class="input full-width">
                                        <input type="text" class="input-unstyled full-width" id="txt_Ticket">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                    <label>Status:</label>
                                    <div class="full-width button-height">
                                        <select id="selectAirStatus" class="select">
                                            <option value="" selected="selected">Select Status</option>
                                            <option value="Cancelled">Cancelled</option>
                                            <option value="Ticketed">Ticketed</option>
                                            <option value="Hold">Hold</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="columns">
                                   <div class="eight-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                </div>
                                <div class="two-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                    <button type="button" class="button anthracite-gradient" onclick="SearchByFlight()">Search</button>
                                    &nbsp;
                            <button type="button" id="btnBack" class="button anthracite-gradient" onclick="Getall()">Back</button>
                                </div>
                                 <div class="two-columns twelve-columns-mobile bold text-alignright">
                                    <span class="icon-pdf right" onclick="ExportToExcelFlight('PDF')">
                                        <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                                    </span>
                                    <span class="icon-excel right" onclick="ExportToExcelFlight('excel')">
                                        <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                                </div>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>

                    <div class="respTable">
                        <table class="table responsive-table font11" id="tbl_AirTicket">
                            <thead>
                                <tr>
                                    <%-- <th scope="col" style="max-width: 20px; vertical-align: middle;" class="align-center">S.N</th>--%>
                                    <th scope="col" class="align-center">Date</th>
                                    <th scope="col" class="align-center">Transaction No.</th>
                                    <th scope="col" class="align-center">Agency / Customer</th>
                                    <th scope="col" class="align-center">Ticket # / PNR </th>
                                    <th scope="col" class="align-center">Travllers Name</th>
                                    <th scope="col" class="align-center">Travelling Details</th>
                                    <th scope="col" style="max-width: 75px;" class="align-center">Status</th>
                                    <th scope="col" class="align-center">Total Fare</th>
                                    <!--<th scope="col" style="max-width: 115px; vertical-align: middle;" class="align-center">Sell</th>-->
                                    <%--<th scope="col" class="align-center">Payment Mode</th>--%>
                                    <th scope="col" style="width: 94px;" class="align-center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>


                <div id="Visa" class="with-small-padding">
                    <div style="text-align: right">
                        <h3 style="margin: 0px"><a href="#" class="addnew"><i class="fa fa-filter Visa"></i></a></h3>
                    </div>
                    <%--<hgroup id="main-title" class="thin">
                        <h2><a href="#" class="addnew"><i class="formTab fa fa-angle-left"></i></a></h2>
                        </hgroup>--%>
                    <div id="VisaFilter" class="with-padding anthracite-gradient" style="display: none;">
                        <form class="form-horizontal">
                            <div class="columns">
                                <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                    <label>Ref No.:</label>
                                    <div class="input full-width">
                                        <input type="text" class="input-unstyled full-width" id="txt_RefNo">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                    <label>Application Name :</label>
                                    <div class="input full-width">
                                        <input type="text" class="input-unstyled full-width" id="txt_AppName">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                    <label>Document No.:</label>
                                    <div class="input full-width">
                                        <input type="text" class="input-unstyled full-width" id="txt_DocNo">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                    <label>Status:</label>
                                    <div class="full-width button-height">
                                        <select id="selStatus" class="select">
                                            <option value="" selected="selected">Select Status</option>
                                            <option value="Under Process">Not Posted</option>
                                            <option value="Posted">Posted</option>
                                            <option value="Approved">Application Approved</option>
                                            <option value="Rejected">Application Rejected</option>
                                            <option value="Entered the in country">Visa Holder entered in country</option>
                                            <option value="Left the country">Visa Holder left the country</option>
                                            <option value="Documents Required">Documents Required for further processing</option>
                                            <option value="Processing error">Application processing error</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="columns">

                                <%--  <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                        <label>From Date:</label>
                                        <span class="input full-width">
                                            <span class="icon-calendar"></span>
                                            <input type="text" name="datepicker" id="txt_Fdate" class="input-unstyled datepicker" value="">
                                        </span>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                        <label>To date:</label>
                                        <span class="input full-width">
                                            <span class="icon-calendar"></span>
                                            <input type="text" name="datepicker" id="txt_Todate" class="input-unstyled datepicker" value="">
                                        </span>
                                    </div>--%>
                                <div class="six-columns four-columns-tablet twelve-columns-mobile">
                                    <label>Visa Type:</label>
                                    <div class="full-width button-height">
                                        <select id="selService" class="select" <%--onchange="Notification()"--%>>
                                            <option value="-" selected="selected">--Salect Visa Type--</option>
                                            <option value="96hrs VISA (Compulsory Confirm Ticket Copy)">96 hours Visa transit Single Entery (Compulsory Confirm Ticket Copy)</option>
                                            <option value="14 days Service VISA">14 days Service VISA</option>
                                            <option value="30 days Tourist Single Entry">30 days Tourist Single Entry</option>
                                            <option value="30 days Tourist Multiple Entry">30 days Tourist Multiple Entry</option>
                                            <option value="90 days Tourist Single Entry">90 days Tourist Single Entry</option>
                                            <option value="90 days Tourist Multiple Entry">90 days Tourist Multiple Entry</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="three-columns four-columns-tablet twelve-columns-mobile">
                                    <label>Agency Name:</label>
                                    <div class="input full-width">
                                        <input type="text" class="input-unstyled full-width" id="txt_AgentName">
                                        <%-- <input type="hidden" id="hdn_uid" />--%>
                                    </div>
                                </div>
                            </div>
                            <div class="columns">
                                <div class="eight-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                </div>
                                <div class="two-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                    <button type="button" id="btn_Search" class="button anthracite-gradient" onclick="SearchbyVisa()">Search</button>
                                    &nbsp;
                            <button type="button" id="btn_Back" class="button anthracite-gradient" onclick="Getall()">Back</button>
                                </div>
                                <div class="two-columns twelve-columns-mobile bold text-alignright">
                                    <span class="icon-pdf right" onclick="ExportToExcel('PDF')">
                                        <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                                    </span>
                                    <span class="icon-excel right" onclick="ExportToExcel('excel')">
                                        <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                                </div>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>

                    <div class="respTable">
                        <table class="table responsive-table font11" id="tbl_VisaDetails">

                            <thead>
                                <tr>
                                    <%--  <th scope="col" class="align-center">Sr. No</th>--%>
                                    <th scope="col" class="align-center">ref. no</th>
                                    <th scope="col" class="align-center">Agent Name</th>
                                    <th scope="col" class="align-center">Applicant</th>
                                    <th scope="col" class="align-center">Doc.No </th>
                                    <th scope="col" class="align-center">Visa Country</th>
                                    <th scope="col" class="align-center">Visa Type</th>
                                    <th scope="col" class="align-center">Status</th>
                                    <th scope="col" class="align-center">Genrated</th>
                                    <th scope="col" class="align-center">Supplier</th>
                                    <th scope="col" class="align-center">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>

                        </table>
                    </div>

                </div>
                <div id="OTB" class="with-small-padding">
                    <div style="text-align: right">
                        <h3 style="margin: 0px"><a href="#" class="addnew"><i class="fa fa-filter otb"></i></a></h3>
                    </div>
                     <div id="OTBFilter" class="with-padding anthracite-gradient" style="display: none;">
                        <form class="form-horizontal">
                            <div class="columns">
                                <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                    <label>Date:</label>
                                    <div class="input full-width">
                                        <input type="text" class="input-unstyled full-width" id="txt_Date">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                    <label>Ref. No.:</label>
                                    <div class="input full-width">
                                        <input type="text" class="input-unstyled full-width" id="txt_ReferNo">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                    <label>Agent Name:</label>
                                    <div class="input full-width">
                                        <input type="text" class="input-unstyled full-width" id="txt_otbAgency">
                                    </div>
                                </div>
                            
                            </div>
                            <div class="columns">

                              
                                <div class="three-columns four-columns-tablet twelve-columns-mobile">
                                    <label>Visa Type:</label>
                                    <div class="full-width button-height">
                                        <select id="selvisa" class="select" <%--onchange="Notification()"--%>>
                                            <option value="-" selected="selected">--Select Visa Type--</option>
                                            <option value="All">All</option>
                                            <option value="96hrs VISA (Compulsory Confirm Ticket Copy)">96 hours Visa transit Single Entery</option>
                                            <option value="14 days Service VISA">14 days Service VISA</option>
                                            <option value="30 days Tourist Single Entry">30 days Tourist Single Entry</option>
                                            <option value="30 days Tourist Multiple Entry">30 days Tourist Multiple Entry</option>
                                            <option value="90 days Tourist Single Entry">90 days Tourist Single Entry</option>
                                            <option value="90 days Tourist Multiple Entry">90 days Tourist Multiple Entry</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="three-columns four-columns-tablet twelve-columns-mobile">
                                    <label>Travel Date:</label>
                                    <div class="input full-width">
                                        <input type="text" class="input-unstyled full-width" id="txt_Travel">
                                        <%-- <input type="hidden" id="hdn_uid" />--%>
                                   
                                         </div>

                                </div>
                                  <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                    <label>Status:</label>
                                    <div class="full-width button-height">
                                        <select id="selOTBStatus" class="select">
                                            <option value="" selected="selected">Select Status</option>
                                            <option value="All">All</option>
                                            <option value="Recived">Processing</option>
                                            <option value="Applied">Processing-Applied</option>
                                            <option value="Approved">Otb Updated</option>
                                            <option value="Not Required">Otb Not Required</option>
                                            <option value="Cancelled By Admin">Rejected</option>
                                            <option value="Cancelled By Agent">Cancelled</option>
                                            <option value="Diverted to EK">Applied Directly</option>
                                          
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="columns">
                                <div class="eight-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                </div>
                                <div class="two-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                    <button type="button" id="btn_OTBSearch" class="button anthracite-gradient" onclick="SearchOTB()">Search</button>
                                    &nbsp;
                                </div>
                                <div class="two-columns twelve-columns-mobile bold text-alignright">
                                    <span class="icon-pdf right" onclick="ExportToExcel('PDF')">
                                        <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                                    </span>
                                    <span class="icon-excel right" onclick="ExportToExcel('excel')">
                                        <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                                </div>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                    <div class="respTable">
                        <table class="table responsive-table font11" id="tbl_OtbDetails">

                            <thead>
                                <tr>
                                    <th scope="col" class="align-center">Date</th>
                                    <th scope="col" class="align-center">Ref. No.</th>
                                    <th scope="col" class="align-center">Agent Name </th>
                                    <th scope="col" class="align-center">Applicant</th>
                                    <th scope="col" class="align-center">Doc.No</th>
                                    <th scope="col" class="align-center">Visa Type</th>
                                    <th scope="col" class="align-center">Airlines</th>
                                    <th scope="col" class="align-center">Travel Date</th>
                                    <th scope="col" class="align-center">Status</th>
                                    <th scope="col" class="align-center">Invoice</th>
                                    <th scope="col" class="align-center">Supplier</th>

                                </tr>

                            </thead>

                            <tbody>
                            </tbody>

                        </table>
                    </div>
                </div>
                <div id="Package" class="with-small-padding">
                    <div style="text-align: right">
                        <h3 style="margin: 0px"><a href="#" class="addnew"><i class="fa fa-filter Package"></i></a></h3>
                    </div>
                    <div id="Packagefilter" class="with-padding anthracite-gradient" style="display: none;">
                        <form class="form-horizontal">
                            <div class="columns">
                                <div class="three-columns twelve-columns-mobile">
                                    <label>Agency</label>
                                    <span class="input full-width">
                                        <span class="icon-calendar"></span>
                                        <input type="text" name="Agency" id="txt_AgencyList" class="input-unstyled" value="">
                                    </span>
                                </div>
                                <%--<div class="three-columns twelve-columns-mobile">
                                    <label>Start Date</label>
                                    <span class="input full-width">
                                        <span class="icon-calendar"></span>
                                        <input type="text" name="datepicker" id="datepicker3" class="input-unstyled" value="">
                                    </span>
                                </div>--%>
                                <%--<div class="three-columns twelve-columns-mobile">
                                    <label>End Date:</label>
                                    <span class="input full-width">
                                        <span class="icon-calendar"></span>
                                        <input type="text" name="datepicker" id="datepicker4" class="input-unstyled" value="">
                                    </span>
                                </div>--%>
                                <div class="three-columns twelve-columns-mobile bold">
                                    <label>Passenger Name </label>
                                    <div class="input full-width">
                                        <input type="text" id="txt_PassengerName" class="input-unstyled full-width">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile bold">
                                    <label>Booking Date</label>
                                    <div class="input full-width">
                                        <input type="text" id="datepicker5" class="input-unstyled full-width">
                                    </div>
                                </div>
                            </div>
                            <div class="columns">
                                <div class="three-columns twelve-columns-mobile bold">
                                    <label>Package Name</label>
                                    <div class="input full-width">
                                        <input type="text" id="txt_Package" class="input-unstyled full-width">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile bold">
                                    <label>Location:</label>
                                    <div class="input full-width">
                                        <input type="text" id="txt_Destination" class="input-unstyled full-width">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile bold">
                                    <label>Package Type</label>
                                    <div class="full-width button-height typeboth">
                                        <select id="Sel_PackageType" class="select">
                                            <option selected="selected">All</option>
                                            <option>Standard</option>
                                            <option>Deluxe</option>
                                            <option>Premium</option>
                                            <option>Luxury</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="columns">
                                <div class="eight-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                </div>
                                <div class="two-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                    <button type="button" class="button anthracite-gradient" onclick="SearchByList()">Search</button>
                                    <button type="button" class="button anthracite-gradient" onclick="Reset()">Reset</button>
                                </div>
                                <div class="two-columns twelve-columns-mobile bold text-alignright">
                                    <span class="icon-pdf right" onclick="ExportBooking('PDF')">
                                        <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                                    </span>
                                    <span class="icon-excel right" onclick="ExportBooking('excel')">
                                        <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                                </div>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                    <div class="with-mid-padding">
                        <div class="respTable">
                            <table class="table responsive-table font11" id="tbl_PACKAGE">
                                <thead>
                                    <tr>
                                        <%--<th scope="col">S.N</th>--%>
                                        <th scope="col" class="align-center">Agency       </th>
                                        <th scope="col" class="align-center">Travel Date  </th>
                                        <th scope="col" class="align-center">Passenger    </th>
                                        <th scope="col" class="align-center">Package Name </th>
                                        <th scope="col" class="align-center">Package Type </th>
                                        <th scope="col" class="align-center">Location     </th>
                                        <th scope="col" class="align-center">Start Date   </th>
                                        <th scope="col" class="align-center">End Date     </th>
                                        <%--<th scope="col" class="align-center">Status       </th>--%>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <script>
        jQuery(document).ready(function () {
            jQuery('.Sightfilter').click(function () {
                jQuery(' #Sightseeingfilter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #Sightseeingfilter').slideUp();
                });
            });
        });
        jQuery(document).ready(function () {
            jQuery('.Package').click(function () {
                jQuery(' #Packagefilter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #Packagefilter').slideUp();
                });
            });
        });
        jQuery(document).ready(function () {
            jQuery('.Visa').click(function () {
                jQuery(' #VisaFilter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #VisaFilter').slideUp();
                });
            });
        });
        jQuery(document).ready(function () {
            jQuery('.otb').click(function () {
                jQuery(' #OTBFilter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #OTBFilter').slideUp();
                });
            });
        });
        jQuery(document).ready(function () {
         jQuery('.Airline').click(function () {
             jQuery(' #AirlineFilter').slideToggle();
               jQuery('.searchBox li a ').on('click', function () {
                   jQuery('.filterBox #AirlineFilter').slideUp();
               });
            });
        });
    </script>
    <script>
        jQuery(document).ready(function () {
            jQuery('.fa-filter').click(function () {
                jQuery(' #filter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #filter').slideUp();
                });
            });
        });
    </script>

</asp:Content>
