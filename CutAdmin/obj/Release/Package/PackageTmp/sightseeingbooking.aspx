﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="sightseeingbooking.aspx.cs" Inherits="CutAdmin.sightseeingbooking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <script src="Scripts/moments.js"></script>
    <script src="Scripts/sightseeingbooking.js?v=1"></script>
    <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section role="main" id="main">
        <hgroup id="main-title" class="thin" style="padding-bottom: 0">
            <%--<h1>
                <span class="float-right font18 align-right " style="line-height: 1.5;" id="spn_Activity"></span>
            </h1>--%>
        </hgroup>
        <hr>
        <div class=" with-padding">
            <form id="frm_booking">
                <fieldset class="">

                    <div class="field-block button-height">
                        <label for="validity" class="label"><b>Agency</b></label>
                       <select name="country" class="select expandable-list loading validate[required]" style="width: 180px" id="Sel_Agency">
                          <option value="" selected="selected">Select Agent</option>
                        </select>
                    </div>

                    <div class="field-block button-height">
                        <label for="validity" class="label"><b>Pax Details</b></label>
                        <div class="columns">
                            <div class="two-columns">
                                <div class="twelve-columns">
                                    <small class="">Adult</small>
                                    <span class="number input small-margin-right">
                                        <button type="button" class="button number-down">-</button>
                                        <input type="text" value="1" size="3" class="input-unstyled" onchange="CalculateRates()" id="txt_adult" data-number-options='{"min":1,"shiftIncrement":5}'>
                                        <button type="button" class="button number-up">+</button>
                                    </span>
                                    <br />
                                    <small class="sCurrency"></small>
                                    <label><b id="lbl_rateofadult"></b></label>
                                </div>
                            </div>

                            <div class="four-columns" id="div_child" style="display: none">
                                <div class="columns">
                                    <div class="six-columns">
                                        <small class="" id="spn_childage1"></small>
                                        <span class="number input small-margin-right">
                                            <button type="button" class="button number-down">-</button>
                                            <input type="text" value="0" size="3" class="input-unstyled" onchange="CalculateRates()" id="txt_child1" data-number-options='{"min":0,"shiftIncrement":5}'>
                                            <button type="button" class="button number-up">+</button>
                                        </span>
                                        <br />
                                        <small class="sCurrency"></small>
                                        <label><b id="lbl_rateofchild1"></b></label>
                                    </div>

                                    <div class="six-columns">
                                        <small class="" id="spn_childage2"></small>
                                        <span class="number input small-margin-right">
                                            <button type="button" class="button number-down">-</button>
                                            <input type="text" value="0" size="3" class="input-unstyled" onchange="CalculateRates()" id="txt_child2" data-number-options='{"min":0,"shiftIncrement":5}'>
                                            <button type="button" class="button number-up">+</button>
                                        </span>
                                        <br />
                                        <small class="sCurrency"></small>
                                        <label><b id="lbl_rateofchild2"></b></label>
                                    </div>
                                </div>

                            </div>

                            <div class="three-columns" id="div_infant" style="display: none">
                                <div class="twelve-columns">
                                    <small class="" id="spn_infantage"></small>
                                    <span class="number input small-margin-right">
                                        <button type="button" class="button number-down">-</button>
                                        <input type="text" value="0" size="3" class="input-unstyled" onchange="CalculateRates()" id="txt_infant" data-number-options='{"min":0,"shiftIncrement":5}'>
                                        <button type="button" class="button number-up">+</button>
                                    </span>
                                    <br />
                                    <small class="sCurrency"></small>
                                    <label><b id="lbl_rateofinfant"></b></label>
                                </div>
                            </div>

                            <div class="three-columns">
                                <span class=""><b>Total Amount</b></span>
                                <br />
                                <small class="sCurrency"></small>
                                <label><b id="lbl_totalamount"></b></label>
                            </div>
                        </div>
                    </div>

                    <div class="field-block button-height">

                        <label for="validity" class="label"><b>Leading Pax Detail</b></label>
                        <div class="columns">
                            <div class="five-columns">
                                <small class="input-info">First Name</small>
                                <input class="input validate[required]" type="text" id="txt_name">
                            </div>
                            <div class="five-columns">
                                <small class="input-info">Last Name</small>
                                <input class="input validate[required]" type="text" id="txt_lname">
                            </div>
                        </div>
                        <div class="columns">
                            <div class="five-columns">
                                <small class="input-info">Email ID</small>
                                <input class="input validate[required,custom[email]]" type="text" id="txt_email">
                            </div>
                            <div class="five-columns">
                                <small class="input-info">Contact No</small>
                                <input class="input validate[required,custom[onlyNumberSp]]" type="text" id="txt_contact">
                            </div>
                        </div>
                    </div>


                    <div class="field-block button-height">
                        <a href="javascript:void(0)" class="button small-margin-right" onclick="BookSightseeing()">
                            <span class="button-icon green-gradient"><span class="icon-download"></span></span>
                            Book
                        </a>
                        <a href="javascript:void(0)" class="button" onclick="CancelInventory()">
                            <span class="button-icon red-gradient"><span class="icon-cross"></span></span>
                            Cancel
                        </a>
                    </div>

                </fieldset>
            </form>
        </div>

    </section>

</asp:Content>
