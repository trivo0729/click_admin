﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ConvenienceFee.aspx.cs" Inherits="CutAdmin.ConvenienceFee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/ConvenienceFee.js?v=1.1"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Convenience Fees</h1>
            <hr />
        </hgroup>
        <div class="with-padding">
            <div class="columns">
                <div class="two-columns four-columns-mobile">
                    <label>Amount Price:<span class="red">*</span></label>
                </div>
                <div class="four-columns eight-columns-mobile">
                    <div class="input full-width">
                        <input class="input-unstyled full-width" placeholder="Amount Price" type="text" id="txtAmountPrice">
                    </div>
                </div>
                <div class="two-columns four-columns-mobile">
                    <label>Amount in Percent:</label>
                </div>
                <div class="four-columns eight-columns-mobile">
                    <div class="input full-width">
                        <input class="input-unstyled full-width" placeholder="Amount in Percent" type="text" id="txtAmountInPercent">
                    </div>
                </div>
                <div style="float: right">
                    <input type="button" class="button anthracite-gradient" id="btnAddConvenience" onclick="AddFee()" value="Add" title="Submit Details">
                    <a id="btn_Cancel" class="button anthracite-gradient" onclick="Cancel()" style="cursor: pointer; display: none">Cancel</a>
                </div>
                <br />
            </div>
            <div class="with-padding">
                <div class="respTable">
                    <table class="table responsive-table font11" id="tbl_Convenience">
                        <thead>
                            <tr>
                                <th scope="col" class="align-center">Sr.No</th>
                                <th scope="col" class="align-center">Amount Price</th>
                                <th scope="col" class="align-center">Amount in Percent</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
