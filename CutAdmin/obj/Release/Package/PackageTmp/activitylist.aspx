﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="activitylist.aspx.cs" Inherits="CutAdmin.activitylist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/ActivityList.js?v=1.5"></script>

    <link href="css/styles/switches.css?v=1" rel="stylesheet" />
    <script src="js/setup.js"></script>
    <script src="js/libs/formValidator/jquery.validationEngine.js?v=1"></script>
    <script src="js/libs/formValidator/languages/jquery.validationEngine-en.js?v=1"></script>
    <script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBnIPCXTY_ul30N9GMmcSmJPLjPEYzGI7c" type="text/javascript"></script>
    <!-- Webfonts -->
    <script src="js/libs/jquery.tablesorter.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#datepicker_From").datepicker({
                dateFormat: "dd-mm-yy",
                //minDate: "dateToday",
                autoclose: true,
            });
            $("#datepicker_To").datepicker({
                // minDate: $("#datepicker_To").text(),
                dateFormat: "dd-mm-yy",
                autoclose: true,
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Activities Details</h1>
            <hr />
            <h2><a href="sightseeing.aspx" class="addnew children-tooltip" title="Add Activity"><i class="fa fa-plus-circle"></i></a></h2>
        </hgroup>

        <div class="with-padding">
            <table class="table responsive-table font11" id="tbl_Actlist">
                <thead>
                    <tr>
                        <th class="align-center" scope="col">S.N </th>
                        <th class="align-center" scope="col">Name </th>
                        <th class="align-center" scope="col">City</th>
                        <th class="align-center" scope="col">Country</th>
                        <th class="align-center" scope="col">Tour Type</th>
                        <th class="align-center" scope="col">Status</th>
                        <th class="align-center" scope="col">Action</th>
                        <%--<th class="align-center" scope="col">Rates</th>--%>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <%-- Inventory Filter--%>
        <div id="Inventoryfilter" class="<%--green-gradient--%>" style="width: 200px; display: none;">
            <form id="frm_Inventory">
                <input type="hidden" id="txt_ActivityID" />
                <input type="hidden" id="txt_ActivityName" />
                <input type="hidden" id="txt_ActivityLocation" />
                <input type="hidden" id="txt_ActivityCity" />
                <input type="hidden" id="txt_ActivityCountry" />

                <select class="select  auto-open  full-width validate[required]" id="sel_Inventory" onchange="AddInventory()">
                    <option value="">-Select Inventory Type-</option>
                    <option value="SightseeingInventory_freesale.aspx">Free Sale</option>
                    <option value="SightseeingInventory_allocation.aspx">Allocation</option>
                    <option value="SightseeingInventory_allotment.aspx">Allotment</option>
                </select>
            </form>

        </div>

    </section>
    <!-- End main content -->





</asp:Content>
