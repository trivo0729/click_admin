﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="OfflineBooking.aspx.cs" Inherits="CutAdmin.OfflineBooking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/RoomRates.js?v=1.3"></script>
    <script src="../Scripts/OfflineHotelSearch.js?v=1.3"></script>
    <script src="../Scripts/OfflineBooking.js?v=1.0"></script>
    <style>
        .mySlides {
            display: none;
        }

        .w3-display-left {
            position: absolute;
            top: 50%;
            left: 0%;
            transform: translate(0%,-50%);
        }

        .w3-display-right {
            position: absolute;
            top: 50%;
            right: 0%;
            transform: translate(0%,-50%);
        }

        .w3-content {
            max-width: 980px;
            margin: auto;
        }

        .w3-tooltip, .w3-display-container {
            position: relative;
            width: 300px;
            height: 250px
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section role="main" id="main">

        <hgroup id="main-title" class="bold grey">
            <h1 id="SPN_HotelName">Offline Booking</h1>
            <h2 style="float: right">
                <a class="addnew with-tooltip tooltip-bottom" id="div_Image" style="cursor: pointer" title="Hotel Details" onclick="GetHotelInfo()"></a>
                <a href="#" class="addnew  with-tooltip tooltip-bottom" id="lbl_Fillter" title="Filter" style="display: none; cursor: pointer" onclick="ShowFilter()"><i class="fa fa-filter"></i></a>
                <input type="button" class="button anthracite-gradient buttonmrgTop" id="btn_mdfSearch" style="display: none" onclick="ShowSearch();" value="Modify Search">
            </h2>
            <hr>
        </hgroup>

        <div class="with-padding">

            <div id="div_Search">
                <div class="columns">
                    <div class="three-columns six-columns-mobile">
                        <label for="txt_SearchType" class="label">Search Type</label><br />
                        <span class="button-group">
                            <label for="SearchType1" class="button blue-active">
                                <input type="radio" name="button-radio SearchType" id="SearchType1" value="Online" onclick="SearchSelect(this.value);">
                                Online Booking
                            </label>
                            <label for="SearchType2" class="button blue-active">
                                <input type="radio" checked name="button-radio SearchType" id="SearchType2" value="Offline" onclick="SearchSelect(this.value);">
                                Offline Booking
                            </label>

                        </span>
                    </div>
                </div>

                <div class="columns">

                    <div class="three-columns six-columns-mobile">
                        <label for="txt_Agents" class="label">Agent Name</label>
                        <input type="text" id="txt_Agents" list="listAgents" placeholder="Agent name" class="input ui-autocomplete-input full-width">
                        <input type="hidden" id="hdnAgentCode" />
                    </div>


                    <div class="three-columns six-columns-mobile">
                        <span>Name:<span class="orange" id="spAgent"></span>,<br />
                            Agent Code:<span class="orange" id="spUniqueCode"></span>
                        </span>
                    </div>

                    <div class="six-columns six-columns-mobile">
                        Available limit:<span class="orange" id="spAvailableLimit"></span>,
                     Credit Limit:[<span class="orange" id="spCreditLimit"></span>/<span class="orange" id="spCreditLimit_Cmp">0</span>]
                     One Time Limit:[<span class="orange" id="spOneTimeCreditLimit"></span>/<span class="orange" id="spOneTimeCreditLimit_Cmp"></span>]
                    </div>


                </div>
                <div class="columns">
                    <div class="new-row four-columns six-columns-mobile">
                        <label for="lbl_txtCity" class="label">Destination</label><span class="red">*</span>
                        <%-- onchange="GetHotel()--%>
                        <input type="text" id="txtCity" placeholder="City name" class="input  ui-autocomplete-input full-width">
                    </div>
                    <div class="four-columns six-columns-mobile">
                        <label for="txt_HotelName" class="label">Hotel Name</label>
                        <input type="text" id="txt_HotelName" placeholder="Hotel name" class="input ui-autocomplete-input full-width">
                    </div>

                    <input type="hidden" id="hdnDCode" />
                    <input type="hidden" id="hdnHCode" />

                    <div class="four-columns twelve-columns-mobile" id="drp_Nationality">
                        <label for="sel_Nationality" class="label">Nationality</label>
                        <select id="sel_Nationality" onchange="CheckNationality(this.value);" class="select multiple-as-single easy-multiple-selection allow-empty check-list chkNationality full-width">
                            <option value="All">All&nbsp;/&nbsp;Unselect</option>
                        </select>
                    </div>
                </div>



                <div class="columns">
                    <div class="three-columns six-columns-mobile">
                        <label for="txt_Checkin" class="label">Check In</label><br />
                        <%--  <span class="input full-width datePick">
                        <input type="text" id="txt_Checkin" class="input-unstyled  gldp-el" value="">
                    </span>--%>
                        <span class="input full-width datePick">
                            <span class="icon-calendar"></span>
                            <input type="text" id="txt_Checkin" class="input-unstyled  gldp-el" value="">
                        </span>

                    </div>

                    <div class="three-columns six-columns-mobile">
                        <label for="txt_Checkout" class="label">Check Out</label><br />
                        <span class="input full-width datePick">
                            <span class="icon-calendar"></span>

                            <input type="text" id="txt_Checkout" class="input-unstyled" value="" />
                        </span>
                    </div>

                    <div class="three-columns six-columns-mobile" id="drp_Nights">
                        <label for="sel_Nights" class="label">Nights</label><br />
                        <select id="sel_Nights" class="select full-width" onchange="drpChangeNights();">
                            <option selected="selected" value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                        </select>
                    </div>


                    <%-- <div class="new-row  three-columns">
                    <label for="sel_Nationality" class="label">Nationality</label>
                    <select id="sel_Nationality" class="select  full-width">
                        <option value="" selected="selected" disabled>Please Select</option>
                    </select>
                </div>--%>


                    <div class="three-columns six-columns-mobile">
                        <label for="sel_Rooms" class="label">Rooms</label><br />

                        <select id="Select_Rooms" class="select full-width" onchange="GetTabs(this.value)">
                            <option selected="selected" value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>
                    </div>

                </div>


                <div class="columns">

                    <div class="three-columns six-columns-mobile" id="drp_Supplier">
                        <label for="sel_Supplier" class="label">Supplier</label><br />
                        <select id="sel_Supplier" class="select full-width" onchange="drpChangeNights();">
                        </select>
                    </div>
                     <div class="three-columns six-columns-mobile">
                        <label for="txt_BookingDate" class="label">Booking Refrence No</label><br />
                        <input type="text" id="txt_Refrence" class="input full-width" value="">
                         
                         </div>
                    <div class="three-columns six-columns-mobile">
                        <label for="txt_BookingDate" class="label">Booking Date</label><br />
                        <span class="input full-width datePick">
                            <span class="icon-calendar"></span>
                            <input type="text" id="txt_BookingDate" class="input-unstyled  gldp-el" value="">
                        </span>

                    </div>
                    <div class="three-columns six-columns-mobile">
                        <label class="input-info">Booking Time</label>
                        <input type="time" class="input full-width" value="" id="txtBookingTime">
                    </div>

                </div>
            </div>

            <div id="filter" class="with-padding anthracite-gradient" style="display: none;">
                <form class="form-horizontal">
                    <div class="columns">

                        <div class="three-columns twelve-columns-mobile four-columns-tablet">
                            <label>Room Type</label>
                            <div class="full-width button-height">
                                <select id="RoomType" onchange="FilterRate(this.value)" class="select multiple-as-single easy-multiple-selection  check-list" multiple>
                                </select>
                            </div>
                        </div>

                        <div class="three-columns twelve-columns-mobile four-columns-tablet">
                            <label>Meal Type</label>
                            <div class="full-width button-height">
                                <select id="MealType" onchange="FilterRate(this.value)" class="select multiple-as-single easy-multiple-selection  check-list" multiple>
                                </select>
                            </div>
                        </div>

                        <div class="three-columns twelve-columns-mobile four-columns-tablet">
                            <label>Price Range</label>
                            <div class="full-width button-height" id="Price_Filter">
                                <span class="demo-slider" data-slider-options='{"size":false,"values":[0,100],"tooltip":["top","top"],"tooltipOnHover":false,"topLabelAlign":"right","barClasses":["red-gradient","glossy"]}'></span>
                            </div>
                        </div>

                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
            <div>
            </div>

            <div id="div_Rooms">
            </div>

            <div class="columns">
                <div style="float: right">

                    <input type="button" class="button anthracite-gradient" id="btn_Save" value="Submit" onclick="SaveBooking()" />
                    <input type="button" class="button anthracite-gradient" id="btn_CancelSubmit" value="Back" onclick="window.location.href = 'VisaDashBoard.aspx'" title="Back" />
                </div>
            </div>

        </div>
    </section>
    <script>
        $(function myfunction() {
            $("#txt_Agents").autocomplete({
                source: function (request, response) {
                    jQuery.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetAgents",
                        data: "{'AgentName':'" + document.getElementById('txt_Agents').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    jQuery('#hdnAgentCode').val(ui.item.id);
                    var test = $('#hdnAgentCode').val();
                    GetAgentDetails($('#hdnAgentCode').val())
                }
            });

        })



    </script>

    <script>
        $(function myfunction() {
            $("#txtCity").autocomplete({
                source: function (request, response) {
                    jQuery.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../Genralhandler.asmx/GetDestinationCode",
                        data: "{'name':'" + document.getElementById('txtCity').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    jQuery('#hdnDCode').val(ui.item.id);
                }
            });
            $("#txt_HotelName").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetHotel",
                        data: "{'name':'" + $('#txt_HotelName').val() + "','destination':'" + $('#hdnDCode').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    $('#hdnHCode').val(ui.item.id);
                    //alert($('#hdnHCode').val());
                }
            });
        })



    </script>

    <%--<script>
        $(function myfunction() {
            $("#txt_HotelName").autocomplete({
                source: function (request, response) {
                    var City1 = $("#txt_Destination").val();
                    var City = City1.split(',')[0];
                    var Country = City1.split(',')[1].trim();
                    var data = {
                        Country: Country,
                        City: City
                    }
                    jQuery.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../HotelHandler.asmx/GetMappedHotelsList",
                        // data: "{'name':'" + document.getElementById('txt_Destination').value + "','City':'" + document.getElementById('txt_Destination').value + "'}",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function (data) {
                            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            response(result);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    jQuery('#HotelCode').val(ui.item.id);
                }
            });

        })



    </script>--%>
</asp:Content>
