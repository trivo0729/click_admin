﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddAgent.aspx.cs" Inherits="CutAdmin.AddAgent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/Exchange.js"></script>
    <script src="Scripts/CountryCityCode.js?v=1.4"></script>
    <script src="Scripts/AddAgent.js?v=1.6"></script>
    <!-- jQuery Form Validation -->
    <script src="js/libs/formValidator/jquery.validationEngine.js?v=1"></script>
    <script src="js/libs/formValidator/languages/jquery.validationEngine-en.js?v=1"></script>
    <script>
        $(function () {
            $("#selCountry").change(function () {
                if ($('#selCountry option:selected').val() == 'IN') {
                    $("#Gst").removeClass("disabled");
                    $("#li_GST").removeClass("disabled");
                }
                else {  
                    $("#Gst").addClass("disabled");
                    $("#li_GST").addClass("disabled");
                }
            });
        })
        $(".addagent").wizard()
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Agent Details</h1>
            <hr />
        </hgroup>
        <div class="with-padding">

            <form action="#" class="addagent">
                <div class="standard-tabs margin-bottom" id="add-tabs">
                    <ul class="tabs">
                        <li class="active"><a href="#Details">Basic Details</a></li>
                        <li class="disabled" id="li_GST"><a href="#Gst">Gst Details</a></li>
                    </ul>
                    <div class="tabs-content">
                        <div id="Details" class="with-mid-padding">
                            <h4>Personal details</h4>
                            <hr>
                            <div class="columns">
                                <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                    <label>First Name </label>
                                    <span class="red">*</span><br>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_FirstName" value="" class="input-unstyled full-width" placeholder="First Name" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                    <label>Last Name </label>
                                    <span class="red">*</span><br>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_LastName" value="" class="input-unstyled full-width" placeholder="Last Name" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                    <label>Designation </label>
                                    <span class="red">*</span><br>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_Designation" value="" class="input-unstyled full-width" placeholder="Designation" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile six-columns-tablet bold">
                                    <label>Company Name </label>
                                    <span class="red">*</span>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_AgencyName" value="" class="input-unstyled full-width" placeholder="Agency Name" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile six-columns-tablet	 bold">
                                    <label>Company mail (Agency Mail) </label>
                                    <span class="red">*</span>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_Email" value="" class="input-unstyled full-width" placeholder="Email" type="text">
                                    </div>
                                </div>
                                <div class="four-columns twelve-columns-mobile six-columns-tablet">
                                    <label>Address </label>
                                    <span class="red">*</span><div class="input full-width">
                                        <input name="prompt-value" id="txt_Address" value="" class="input-unstyled full-width" placeholder="Address" type="text">
                                    </div>
                                </div>
                            </div>

                            <h4>Other Details</h4>
                            <hr>
                            <div class="columns">
                                <div class="three-columns  twelve-columns-mobile six-columns-tablet">
                                    <label>Country </label>
                                    <span class="red">*</span>
                                    <br>
                                    <div class="full-width button-height" id="DivCountry">
                                        <select id="selCountry" class="select country">
                                            <option selected="selected" value="-">Select Any Country</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="three-columns  twelve-columns-mobile six-columns-tablet">
                                    <label>City </label>
                                    <span class="red">*</span>
                                    <br>
                                    <div class="full-width button-height" id="City">
                                        <select id="selCity" class="select City">
                                            <option value="-">-Select Any City-</option>
                                        </select>
                                        <%--<span class="select-value">-Select Any City-</span>
                                <span class="select-arrow"></span>
                                <span class="drop-down"></span>
                            </span>--%>
                                    </div>
                                </div>

                                <div class="two-columns  twelve-columns-mobile six-columns-tablet">
                                    <label>IATA STATUS </label>
                                    <span class="red">*</span>
                                    <br>
                                    <div class="full-width button-height" id="Div_IATA">
                                        <select id="Select_IATA" name="validation-select" class="select" tabindex="-1">
                                            <option selected="selected" value="0">Select Status</option>
                                            <option value="1">Approved</option>
                                            <option value="2">UnApproved</option>
                                        </select>
                                    </div>
                                </div>

                               
                                <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                                    <label>IATA No </label>
                                    <br>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_IATA" value="" class="input-unstyled full-width" placeholder="IATA No" type="text">
                                    </div>
                                </div>

                                <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                                    <label>Pin Code</label><br>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_PinCode" value="" class="input-unstyled full-width" placeholder="Pin Code" type="text">
                                    </div>
                                </div>

                            </div>

                            <div class="columns">
                                <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                                    <label>Phone </label>
                                    <br>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_Phone" value="" class="input-unstyled full-width" placeholder="Phone" type="text">
                                    </div>
                                </div>
                                <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                                    <label>Mobile </label>
                                    <span class="red">*</span><br>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_Mobile" value="" class="input-unstyled full-width" placeholder="Mobile" type="text">
                                    </div>
                                </div>
                                <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                                    <label>Fax </label>
                                    <br>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_Fax" value="" class="input-unstyled full-width" placeholder="Fax" type="text">
                                    </div>
                                </div>

                                <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                                    <label>PAN No</label><br>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_PAN" value="" class="input-unstyled full-width" placeholder="PAN No" type="text">
                                    </div>
                                </div>
                                <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                                    <label>Service Tax Number</label><br>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_ReferenceNumber" value="" class="input-unstyled full-width" placeholder="Service Tax Number " type="text">
                                    </div>
                                </div>
                                <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                                    <label>Website URL</label><br>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_Website" value="" class="input-unstyled full-width" placeholder="Website URL" type="text">
                                    </div>
                                </div>

                            </div>

                            <div class="columns">
                                <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                                    <label>Group Markup </label>
                                    <span class="red">*</span><br>
                                    <div class="full-width button-height" id="Divgroup">
                                        <select id="selGroup" name="validation-select" class="select Divgroup" tabindex="-1">
                                            <option selected="selected" value="-">Select Any Group</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                                    <label>Remarks</label><br>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_Remarks" value="" class="input-unstyled full-width" placeholder="Remarks" type="text">
                                    </div>
                                </div>
                               <%-- <div class="two-columns  twelve-columns-mobile four-columns-tablet" style="display: none">
                                    <label>GST NO</label>
                                    <br>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_GSTNO" value="" class="input-unstyled full-width" placeholder="GST NO" type="text">
                                    </div>
                                </div>--%>
                                 <div class="two-columns  twelve-columns-mobile six-columns-tablet" >
                                    <label>User type</label>
                                    <span class="red">*</span>
                                    <br>
                                    <div class="full-width button-height" id="UserType">
                                        <select id="Select_Usertype" class="select UsrTyp">
                                            <option selected="selected" value="0">Select User Type</option>
                                            <option value="Agent">Agent</option>
                                            <option value="Corporate" id="Corporate">Corporate</option>
                                            <option value="Franchisee" id="Franchisee">Franchisee</option>
                                            <option value="Supplier" id="Supplier">Supplier</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="two-columns  twelve-columns-mobile four-columns-tablet">
                                    <label>Preferred Currency</label>
                                    <br>
                                    <div class="full-width button-height" id="Currency">
                                        <select id="SelCurrency" name="validation-select currency" class="select" tabindex="-1">
                                           
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="columns">
                                <div style="float: right" class="two-columns  twelve-columns-mobile four-columns-tablet">
                                    <%--<button type="button" class="button anthracite-gradient UpdateMarkup">Register</button>--%>
                                    <input id="btn_RegisterSupplier" type="button" value="Register" class="button anthracite-gradient UpdateMarkup" onclick="ValidateLogin();" title="Submit Details" />
                                    <a class="button" href="AgentDetails.aspx">Back</a>
                                </div>
                            </div>
                        </div>

                        <div id="Gst" class="with-padding disabled">
                            <h4>Gst Details</h4>
                            <hr>
                            <div class="columns">
                                <div class="three-columns twelve-columns-mobile six-columns-tablet">
                                    <label>Agency Name</label>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_AgencyName" value="" class="input-unstyled full-width" placeholder="Agency Name" type="text">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile six-columns-tablet">
                                    <label>Agency GSTIN </label>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_GSTNO" value="" class="input-unstyled full-width" placeholder="Agency GSTIN " type="text">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile six-columns-tablet">
                                    <label>Agency Code</label>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_AgencyCode" value="" class="input-unstyled full-width" placeholder="Agency Code" type="text">
                                    </div>
                                </div>
                                 <div class="three-columns twelve-columns-mobile six-columns-tablet">
                                    <label>GST Registration Status </label>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_GSTRegistrationStatus " value="" class="input-unstyled full-width" placeholder="GST Registration Status " type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="columns">
                                <div class="three-columns twelve-columns-mobile six-columns-tablet bold">
                                    <label>Contact Person:</label>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_ContactPerson" value="" class="input-unstyled full-width" placeholder="Contact Person:" type="text">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile six-columns-tablet	 bold">
                                    <label>Contact Person Email :</label>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_ContactPersonEmail" value="" class="input-unstyled full-width" placeholder="Contact Person Email :" type="text">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile six-columns-tablet	 bold">
                                    <label>Account Mail ID:</label>
                                    <div class="input full-width">
                                        <input name="prompt-value" id="txt_ContactPersonMail" value="" class="input-unstyled full-width" placeholder="Account Mail ID:" type="text">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile six-columns-tablet">
                                    <label>Contact Person Mobile:</label>
                                   <div class="input full-width">
                                        <input name="prompt-value" id="txt_ContactPersonMobile" value="" class="input-unstyled full-width" placeholder="Contact Person Mobile:" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="columns">
                                <div class="three-columns  twelve-columns-mobile six-columns-tablet">
                                    <label>Agency State </label>
                                    <div class="full-width button-height" id="DivState">

                                        <select id="Sel_AgencyState" onchange="GetStateCode()" class="select State" tabindex="-1">
                                            <option value="-" selected="selected">-Select Agency State-</option>
                                            <option value="1">Andaman and Nicobar Islands</option>
                                            <option value="2">Andhra Pradesh</option>
                                            <option value="3">Arunachal Pradesh</option>
                                            <option value="4">Assam</option>
                                            <option value="5">Bihar</option>
                                            <option value="6">Chandigarh</option>
                                            <option value="7">Chhattisgarh</option>
                                            <option value="8">Dadra and Nagar Haveli</option>
                                            <option value="9">Daman and Diu</option>
                                            <option value="10">Delhi</option>
                                            <option value="11">Goa</option>
                                            <option value="12">Gujarat</option>
                                            <option value="13">Haryana</option>
                                            <option value="14">Himachal Pradesh</option>
                                            <option value="15">Jammu and Kashmir</option>
                                            <option value="16">Jharkhand</option>
                                            <option value="17">Karnataka</option>
                                            <option value="18">Kerala</option>
                                            <option value="19">Lakshadweep Islands</option>
                                            <option value="20">Madhya Pradesh</option>
                                            <option value="21">Maharashtra</option>
                                            <option value="22">Manipur</option>
                                            <option value="23">Meghalaya</option>
                                            <option value="24">Mizoram</option>
                                            <option value="25">Nagaland</option>
                                            <option value="26">Odisha</option>
                                            <option value="27">Puducherry</option>
                                            <option value="28">Punjab</option>
                                            <option value="29">Rajasthan</option>
                                            <option value="30">Sikkim</option>
                                            <option value="31">Tamil Nadu</option>
                                            <option value="32">Telangana</option>
                                            <option value="33">Tripura</option>
                                            <option value="34">Uttar Pradesh</option>
                                            <option value="35">Uttarakhand</option>
                                            <option value="36">West Bengal</option>
                                            <option value="37">Andhra Pradesh (New)</option>
                                            <option value="38">Other Territory</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile six-columns-tablet">
                                    <label>Agency State Code :</label>
                                    <div class="input full-width" id="DivAgencyStateCode">
                                        <input name="prompt-value" id="txt_AgencyStateCode" value="" class="input-unstyled full-width" placeholder="Agency State Code :"  readonly="readonly" type="text">
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile six-columns-tablet">
                                    <label>Agency Classification :</label>
                                    <div class="full-width button-height" id="DivAgencyClassification">
                                        <select id="Sel_AgencyClassification" class="select" tabindex="-1">
                                            <option value="-" selected="selected">-Select Agency Classification-</option>
                                            <option value="Registered Dealers">Registered Dealers</option>
                                            <option value="United Nations">United Nations</option>
                                            <option value="Government Bodies">Government Bodies</option>
                                            <option value="Composite Dealers">Composite Dealers</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="three-columns twelve-columns-mobile six-columns-tablet">
                                    <label style="font-size:9px">Composition Levy as per Section 10 of CGST </label>
                                    <div class="full-width button-height" id="DivComposition">
                                        <select id="Sel_Composition" class="select  Composition" tabindex="-1">   
                                             <option value="-" selected="selected">-Select -</option> 
                                            <option value="Yes">Yes</option> 
                                            <option value="No">No</option>                                         
                                        </select>
                                    </div>
                                </div>
                            </div>
                             <div class="columns">
                                <div style="float: right" class="two-columns  twelve-columns-mobile four-columns-tablet">
                                    <input id="btn_Submit" type="button" value="SUBMIT" class="button anthracite-gradient UpdateMarkup" onclick="SaveDetails()" title="Submit Details" />
                                     <a class="button" style="cursor:pointer" onclick="Reload()" >RESET</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
       
        </form>
        </div>
    </section>
    <script>
        function Reload() {
            window.location.reload();
        }
    </script>
</asp:Content>
