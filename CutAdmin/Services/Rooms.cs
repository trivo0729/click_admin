﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.EntityModal;
namespace CutAdmin.Services
{

    public class Rooms
    {
        public long RoomID { get; set; }
        public String RoomName { get; set; }

        public static List<Rooms> GetRooms(string HotelCode)
        {
            List<Rooms> arrRooms = new List<Rooms>();
            try
            {
                using (var db = new Click_Hotel())
                {
                    IQueryable<Rooms> arrList = (from obj in db.tbl_commonRoomDetails
                                                 join objRoom in db.tbl_commonRoomType on obj.RoomTypeId.ToString() equals
                                                 objRoom.RoomTypeID.ToString() where obj.HotelId.ToString() == HotelCode
                                                 select new Rooms { RoomID = objRoom.RoomTypeID, RoomName = objRoom.RoomType });
                    if (arrList.ToList().Count != 0)
                    {
                        arrRooms = arrList.ToList();
                    }
                    else
                        throw new Exception("No Room Found.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return arrRooms;
        }
    }
}