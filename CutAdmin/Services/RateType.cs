﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.EntityModal;
namespace CutAdmin.Services
{
    public class RateType
    {
        public string sRateType { get; set; }
        public static List<RateType> _ByInventory(string InventoryType,string HotelAdminID)
        {
            List<RateType> arrRateType = new List<RateType>();
            try
            {
                using (var db = new Click_Hotel())
                {
                    IQueryable<RateType> arrObj = (from obj in db.tbl_RateType
                                                   where obj.InventoryType.Contains(InventoryType) &&
                                                   obj.ParentID.ToString() == HotelAdminID
                                                   select new RateType
                                                   {
                                                       sRateType = obj.RateType,
                                                   });
                    if (arrObj.ToList().Count == 0)
                        throw new Exception("Please  Add Rate Type");
                    arrRateType = arrObj.ToList().Distinct().ToList();
                }
            }
            catch
            {
                throw new Exception("No Rate Type Available");
            }
            return arrRateType;
        }

        public static List<RateType> _ByRateType(string HotelAdminID)
        {
            List<RateType> arrRateType = new List<RateType>();
            try
            {
                using (var db = new Click_Hotel())
                {
                    IQueryable<RateType> arrObj = (from obj in db.tbl_RateType
                                                 where obj.ParentID.ToString() == HotelAdminID
                                                   select new RateType
                                                   {
                                                       sRateType = obj.RateType,
                                                   });
                    if (arrObj.ToList().Count == 0)
                        throw new Exception("Please  Add Rate Type");
                    arrRateType = arrObj.ToList().Distinct().ToList();

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return arrRateType;
        }


        public static List<RateType> _ByRate(string HotelAdminID)
        {
            List<RateType> arrRateType = new List<RateType>();
            try
            {
                using (var db = new Click_Hotel())
                {
                    IQueryable<RateType> arrObj = (from obj in db.tbl_RateType
                                                   join
                       objRate in db.tbl_RatePlan on obj.RateType equals objRate.RateType
                       where obj.ParentID.ToString() == HotelAdminID
                                                   select new RateType
                                                   {
                                                       sRateType = obj.RateType,
                                                   });
                    if (arrObj.ToList().Count == 0)
                        throw new Exception("No Rates Found");
                    arrRateType = arrObj.ToList().Distinct().ToList();
                }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            return arrRateType;
        }

        public static List<RateType> _ByRateInventory(string Inventory, string HotelCode, string RoomCode)
        {
            List<RateType> arrRateType = new List<RateType>();
            try
            {
                using (var db = new Click_Hotel())
                {
                    IQueryable<RateType> arrObj = (from obj in db.tbl_RateType
                                                   join
                      objRate in db.tbl_RatePlan on  obj.RateType equals objRate.RateType
                      where obj.InventoryType.Contains(Inventory) && objRate.HotelID.ToString() == HotelCode &&
                      objRate.RoomTypeID.ToString() == RoomCode
                      select new RateType
                      {
                          sRateType = obj.RateType,
                      });
                    if (arrObj.ToList().Count == 0)
                        throw new Exception("No Rates Found <br/> <a class='white underline bold' onclick='AddRate()' > Click Here to Add Rate</a>");
                    arrRateType = arrObj.ToList().Distinct().ToList().GroupBy(d => d.sRateType).Select(d => d.FirstOrDefault()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return arrRateType;
        }

        public static List<RateType> _ByHotelInventory(string Inventory, string HotelCode)
        {
            List<RateType> arrRateType = new List<RateType>();
            try
            {
                using (var db = new Click_Hotel())
                {
                    IQueryable<RateType> arrObj = (from obj in db.tbl_RateType
                                                   join
                      objRate in db.tbl_RatePlan on obj.RateType equals objRate.RateType
                                                   where obj.InventoryType.Contains(Inventory) && objRate.HotelID.ToString() == HotelCode
                                                   select new RateType
                                                   {
                                                       sRateType = obj.RateType,
                                                   });
                    if (arrObj.ToList().Count == 0)
                        throw new Exception("No Tarrif Type Found");
                    arrRateType = arrObj.ToList().Distinct().ToList().GroupBy(d => d.sRateType).Select(d => d.FirstOrDefault()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return arrRateType;
        }

        public static List<RateType> _ByHotel(string HotelCode)
        {
            List<RateType> arrRateType = new List<RateType>();
            try
            {
                using (var db = new Click_Hotel())
                {
                    IQueryable<RateType> arrObj = (from obj in db.tbl_RateType
                                                   join
                      objRate in db.tbl_RatePlan on obj.RateType equals objRate.RateType
                                                   where objRate.HotelID.ToString() == HotelCode
                                                   select new RateType
                                                   {
                                                       sRateType = obj.RateType,
                                                   });
                    if (arrObj.ToList().Count == 0)
                        throw new Exception("No Tarrif Type Found");
                    arrRateType = arrObj.ToList().Distinct().ToList().GroupBy(d => d.sRateType).Select(d => d.FirstOrDefault()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return arrRateType;
        }
    }
}