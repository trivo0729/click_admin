﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.Services
{
    public class GroupService
    {
        public static List<Group> GetGroup()
        {
            List<Group> arrGroup = new List<Group>();
            using (var db = new helperDataContext())
            {
                IQueryable<Group> GroupList = (from obj in db.tbl_GroupMarkups
                                               where obj.ParentID == AccountManager.GetAdminByLogin()
                                               select new Group
                                               {
                                                   sid = obj.sid,
                                                   GroupName = obj.GroupName
                                               });
                arrGroup = GroupList.Distinct().ToList();
            }
            return arrGroup;
        }
    }

    public class Group
    {
        public Int64 sid { get; set; }
        public string GroupName { get; set; }
    }
}