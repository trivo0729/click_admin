﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.dbml;
using CutAdmin.DataLayer;

namespace CutAdmin.Services
{
    public class ActivityMails
    {
        #region Activity Mails
        public object GetActivityMails(string Type, string Activity)
        {
            using (helperDataContext db = new helperDataContext())
            {
                var arrActivityMails = (from a in db.tbl_ActivityMails
                                        where a.Type == Type && a.Activity == Activity && a.ParentID == AccountManager.GetAdminByLogin()
                                        select a).FirstOrDefault();
                return arrActivityMails;
            }
        }
        #endregion
    }
}