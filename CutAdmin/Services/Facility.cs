﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.BL;
namespace CutAdmin.Services
{
    public class Facility
    {
        public long ID { get; set; }
        public string Name { get; set; }

        public static List<Facility> _ByAll()
        {
            List<Facility> arrFacility = new List<Facility>();

            try
            {
                using (var db =  DBHelper.db_Hotel)
                {
                    IQueryable<Facility> arrData = (from obj in db.tbl_commonFacility
                                                    select new Facility
                                                    {
                                                        ID= obj.HotelFacilityID,
                                                        Name = obj.HotelFacilityName,
                                                    });
                    if(arrData.ToList().Count==0)
                        throw new Exception("No Facility Found");
                    arrFacility = arrData.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return arrFacility;
        }

        public static List<Facility> _ByHotel(string HotelCode)
        {
            List<Facility> arrFacility = new List<Facility>();

            try
            {
                using (var db = DBHelper.db_Hotel)
                {
                    string sID = (from obj in db.tbl_CommonHotelMaster where obj.sid.ToString() == HotelCode select obj.HotelFacilities).FirstOrDefault();
                    if (sID == null)
                        throw new Exception("No Facility Found");
                    List<string> arrID = sID.Split(',').ToList();
                    IQueryable<Facility> arrData = (from obj in db.tbl_commonFacility
                                                    where arrID.Contains(obj.HotelFacilityID.ToString())
                                                    select new Facility
                                                    {
                                                        ID = obj.HotelFacilityID,
                                                        Name = obj.HotelFacilityName,
                                                    });
                    if (arrData.ToList().Count != 0)
                        arrFacility = arrData.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return arrFacility;
        }
    }
}