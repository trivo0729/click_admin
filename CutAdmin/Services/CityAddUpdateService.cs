﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace CutAdmin.Services
{
    public static class CityAddUpdateService
    {
        public static List<CountryCityList> GetCountryCity()
        {
            List<CountryCityList> listCountryCityList = new List<CountryCityList>();

            using (var DB = new helperDataContext())
            {

                IQueryable<CountryCityList> CountryCityList = (from obj in DB.tbl_HCities
                                                               select new CountryCityList
                                                               {
                                                                   Countryname = obj.Countryname,
                                                                   Country = obj.Country,
                                                               });
                listCountryCityList = CountryCityList.Distinct().ToList();
            }
            return listCountryCityList;
        }

        public static List<CityList> GetCityByCode(string Code)
        {
            List<CityList> listCityList = new List<CityList>();
            using (var DB = new helperDataContext())
            {
                IQueryable<CityList> CityList = (from obj in DB.tbl_HCities
                                                 where obj.Country == Code
                                                 select new CityList
                                                 {
                                                     Description = obj.Description,
                                                     Code = obj.Code,
                                                 });
                listCityList = CityList.Distinct().ToList();
            }
            return listCityList;
        }

        public static List<CityList> GetCityByName(string Name)
        {
            List<CityList> listCityList = new List<CityList>();
            using (var DB = new helperDataContext())
            {

                IQueryable<CityList> CityList = (from obj in DB.tbl_HCities
                                                 where obj.Countryname == Name
                                                 select new CityList
                                                 {
                                                     Description = obj.Description,
                                                     Code = obj.Code,
                                                 });
                listCityList = CityList.Distinct().ToList();
            }
            return listCityList;
        }
    }

    public class CityList
    {
        public string Description { get; set; }
        public string Code { get; set; }
    }

    public class CountryCityList
    {
        public string Countryname { get; set; }
        public string Country { get; set; }
    }
}