﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.EntityModal;
namespace CutAdmin.Services
{
    public class Hotels
    {
        public long HotelID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

        public static List<Hotels> GetHotels(string sAdminKey)
        {
            List<Hotels> arrHotels = new List<Hotels>();
            try
            {
                using (var db = new Click_Hotel())
                {
                    IQueryable<Hotels> IQueryableHotel = (from obj in db.tbl_CommonHotelMaster where obj.ParentID.ToString() == sAdminKey
                                                          select new Hotels {
                                                              HotelID = obj.sid,
                                                              Name = obj.HotelName,
                                                              Address = obj.HotelAddress
                                                          });
                    if (IQueryableHotel.ToList().Count == 0)
                        throw new Exception("No Hotels Found");
                    arrHotels = IQueryableHotel.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception (ex.Message);
            }
            return arrHotels;
        }
    }
}