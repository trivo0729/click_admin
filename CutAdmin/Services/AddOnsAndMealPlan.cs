﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.EntityModal;
using CutAdmin.DataLayer;
namespace CutAdmin.Services
{
    public class AddOnsAndMealPlan
    {
        public static CUT_LIVE_UATSTEntities db { get { return new CUT_LIVE_UATSTEntities(); } }
        public static Click_Hotel db_Hotel { get { return new Click_Hotel(); } }
        public static List<CutAdmin.EntityModal.Comm_AddOns> GetMeals()
        {
            Int64 Uid = AccountManager.GetSuperAdminID();
            List<CutAdmin.EntityModal.Comm_AddOns> arrMeals = new List<CutAdmin.EntityModal.Comm_AddOns>();
            try
            {
                arrMeals = (from obj in db_Hotel.Comm_AddOns
                            where obj.UserId == Uid
                            select obj).ToList();
            }
            catch (Exception ex)
            {

            }
            return arrMeals;
        }

        public static object GetMealPlans()
        {
            object arrMealPlan = new List<object>();
            try
            {
                Int64 Uid = AccountManager.GetAdminByLogin();
                using (var db_Hotel = new Click_Hotel())
                {
                    arrMealPlan = (from obj in db_Hotel.tbl_MealPlan
                                   where obj.ParentID == Uid
                                   select new
                                   {
                                       obj.ID,
                                       obj.Meal_Plan,
                                       arrMealID = (from objMealMap in db_Hotel.tbl_MealPlanMapping
                                                    where objMealMap.PlanID == obj.ID
                                                    select objMealMap.MealID).ToList()
                                   }).ToList();
                }

            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
            }
            return arrMealPlan;
        }
    }
}