﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using CutAdmin.BL;
using CutAdmin.EntityModal;
namespace CutAdmin.Services
{
    public class LocationService : DBHelper
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Loc_PlaceID { get; set; }
        public string Loc_Lat { get; set; }
        public string Loc_Long { get; set; }
        public string CityName { get; set; }
        public string CityCode { get; set; }

        public string Country_Code { get; set; }

        public string Country_Name { get; set; }
        public string City_PlaceID { get; set; }
        public string City_Lat { get; set; }
        public string City_Long { get; set; }

        public static List<Models.AutoComplete> GetDestination(string name)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
            try
            {
                using (var db = db_Admin)
                {
                    IQueryable<tbl_MappedArea> Hotels = (from obj in db.tbl_MappedArea where (obj.Name).ToLower().Contains(name.ToLower()) select obj);
                    var Hotel = Hotels.ToList().Distinct();
                    list_autocomplete = Hotel.Select(data => new Models.AutoComplete
                    {
                        id = data.Destination_ID,
                        value = data.Name + "," + data.CountryName,
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
            }
            return list_autocomplete;
        }

        public static LocationService _ByID(long? ID) {
            LocationService arrLocation = new LocationService();
            try
            {
                using (var db = db_Admin)
                {
                    IQueryable<LocationService> arrData = (from obj in db.tbl_MappedArea
                             join objLoc in db.tbl_Location on obj.Destination_ID
                             equals objLoc.CityCode
                             where objLoc.Lid == ID
                             select new LocationService
                             {
                                 ID = objLoc.Lid,
                                 Name = objLoc.LocationName,
                                 Loc_Lat = objLoc.Latitude,
                                 Loc_Long = objLoc.Longitutde,
                                 Loc_PlaceID = objLoc.Placeid,

                                 CityCode = objLoc.CityCode,
                                 CityName = obj.Name,
                                 City_Lat = obj.latitude,
                                 City_Long = obj.longitude,
                                 City_PlaceID = obj.Placeid,
                                 Country_Code = obj.Country_Code,
                                 Country_Name = obj.CountryName,

                             });
                    if(arrData.ToList().Count!=0)
                    {
                        arrLocation = arrData.ToList().FirstOrDefault();
                    }

                    db_Admin.Dispose();
                }
                
            }
            catch (Exception)
            {

                throw;
            }
            return arrLocation;
        }
    }
}