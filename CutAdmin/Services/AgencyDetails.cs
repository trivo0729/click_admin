﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.EntityModal;
using CutAdmin.dbml;
using CutAdmin.DataLayer;

namespace CutAdmin.Services
{
    public class AgencyDetails
    {
        public string AgencyName { get; set; }
        public int? AgencyID { get; set; }
        public string Currency { get; set; }
        public static List<AgencyDetails> GetAgencyList()
        {
            List<AgencyDetails> arrAgency = new List<AgencyDetails>();
            try
            {
                Int64 AdminID = AccountManager.GetAdminByLogin();
                using (var db = new helperDataContext())
                {
                    IQueryable<AgencyDetails> arrObj = (from obj in db.tbl_AdminLogins
                                                        where obj.ParentID == AdminID && obj.UserType== "Agent"
                                                        select new AgencyDetails
                                                        {
                                                            AgencyID = obj.sid,
                                                            AgencyName = obj.AgencyName,
                                                            Currency=obj.CurrencyCode
                                                        });
                    if (arrObj.ToList().Count != 0)
                    {
                        arrAgency = arrObj.ToList().Distinct().ToList();
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return arrAgency;
        }
    }
}