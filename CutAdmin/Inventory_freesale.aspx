﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Inventory_freesale.aspx.cs" Inherits="CutAdmin.Inventory_freesale" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <script src="Scripts/moments.js"></script>
    <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
    <script src="Scripts/Inventory.js?v=1.25"></script>
    <script src="Scripts/Hotels/GetRateType.js?v=1.0"></script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
<section role="main" id="main">
        <hgroup id="main-title" class="thin" style="padding-bottom:0">
            <h1>Freesale
                <span class="float-right font18 align-right " style="line-height:1.5;" id="spn_Hotel"> </span>
            </h1>
        </hgroup>
        <hr>
        <div class=" with-padding">
             <form id="frm_Inventory">
            <fieldset class="">            
            <div class="field-block button-height">
                <small class="input-info">You may select more than one date range for which you want to open / close this hotel room</small>
                    <label for="validity" class="label"><b>Validity</b></label>
                    <div id="div_InventoryDate">
                                      
                    </div>
                     <br />
                <div class="columns">
                    <div class="two-columns three-columns-mobile">
                            <label for="market" class=" ">
                            <p class="reversed-switches">
                                <input type="checkbox" class="switch medium wide" name="btn_slot"  id="chk_Days" checked data-text-on="All Day" data-text-off="Valid Day" onchange="SelectDays(this)">
                            </p>
                        </label>
                    </div>
                    <div class="eight-columns six-columns-mobile divDays" style="display:none">
                        <p>
                        <input type="checkbox" class="checkbox" id="mon" name="days" checked/>
                        <label class="" for="mon">Mon </label>
                        <input type="checkbox" class="checkbox" id="tue" name="days" checked/>
                        <label class="" for="tue">Tue </label>
                        <input type="checkbox" class="checkbox" id="wed" name="days" checked/>
                        <label class="" for="wed">Wed </label>
                        <input type="checkbox" class="checkbox" id="Thu" name="days" checked/>
                        <label class="" for="Thu">Thu </label>
                        <input type="checkbox" class="checkbox" id="fri" name="days" checked/>
                        <label class="" for="fri">Fri </label>
                        <input type="checkbox" class="checkbox" id="sat" name="days" checked/>
                        <label class="" for="sat">Sat </label>
                        <input type="checkbox" class="checkbox" id="sun" name="days" checked/>
                        <label class="" for="sun">Sun </label>

                    </p>
                    </div>

                </div>
                </div>    
                <div class="field-block button-height">
                    <small class="input-info">Select rate type for which you want to open / close this inventory</small>
                    <label for="validity" class="label"><b>Rate type</b></label>
                    <div class="three-columns">                        
                        <select class="select full-width" id="sel_RateType">
                         
                        </select>                       
                    </div>
                </div>
                <div class="field-block button-height">
                    <small class="input-info">Select open if you want to start sale or select close if you want to stop sale</small>
                    <label for="validity" class="label"><b>Action</b></label>
                    <p>
                        <input type="radio" class="radio" id="open" name="freesale" checked/>
                        <label class="" for="open">Open </label>
                        <input type="radio" class="radio" id="close" name="freesale" />
                        <label class="" for="close">Close</label>
                    </p>
                </div>

                <div class="field-block button-height">
                    <small class="input-info">Select here if you have more conditions to define for this inventory</small>
                    <label for="conditions" class="label"><b>Conditions</b></label>
                    <p>
                        Is there any additional conditions applied for this inventory?
                        <input id="conditions" type="checkbox" class="switch medium" data-checkable-options='{"textOn":"YES","textOff":"NO"}'  onchange="Showconditions()">
                    </p>
                </div>
                <div class="field-drop button-height black-inputs  conditions" style="display:none">

                    <label for="validity" class="label"><b>Restrictions</b></label>
                    <p class="white">
                        Accept booking request even after inventory is closed?
                        <input id="chk_Request" type="checkbox" class="switch medium" checked data-checkable-options='{"textOn":"YES","textOff":"NO"}'>
                    </p>
                    <p class="white">
                        <span>This free sale will be autometically closed</span>
                        <select class="select anthracite-gradient" id="txt_Till">
                            <option>0 day</option>
                            <option>1 day</option>
                            <option>2 days</option>
                            <option>3 days</option>
                            <option>5 days</option>
                            <option>7 days</option>
                            <option>10 days</option>
                            <option>15 days</option>
                            <option>20 days</option>
                            <option>30 days</option>
                        </select>
                        <span>prior to checkin date</span>
                    </p>
                    <p class="white">
                        <span>Maximum </span>
                        <span class="number input small-margin-right">
                            <button type="button" class="button number-down">-</button>
                            <input type="text" value="0" size="3" class="input-unstyled  validate[required,min[0],max[30]]" id="txt_MaxRoomperbook"  >
                            <button type="button" class="button number-up">+</button>
                        </span>
                        <span>room(s) can be booked per booking & maximum </span>
                        <span class="number input small-margin-right">
                            <button type="button" class="button number-down">-</button>
                            <input type="text" value="0" size="3" class="input-unstyled  validate[required,min[0],max[30]]" id="txt_MaxRoomperDate">
                            <button type="button" class="button number-up">+</button>
                        </span>
                        <span>room(s) can be booked per day</span>
                    </p>
                    <small class="input-info white">Keep it '0' (Zero) if you don't have any restrictions</small>
                </div>

                <div class="field-block button-height">
                    <a href="javascript:void(0)" class="button small-margin-right" onclick="SaveInventory('FreeSale')">
                        <span class="button-icon green-gradient"><span class="icon-download"></span></span>
                        Save
                    </a>
                    <a href="javascript:void(0)" class="button" onclick="CancelInventory()">
                        <span class="button-icon red-gradient"><span class="icon-cross"></span></span>
                        Cancel
                    </a>
                </div>

</fieldset>
                    </form>
        </div>
        
    </section>

</asp:Content>
