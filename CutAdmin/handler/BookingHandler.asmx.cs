﻿using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.Common;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.Script.Services;
using CutAdmin.EntityModal;
using CutAdmin.Models;
using Newtonsoft.Json;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for BookingHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class BookingHandler : System.Web.Services.WebService
    {
        string json = ""; string Texts = "";
        string jsonString = "";
        DataTable dtResult;
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //DBHandlerDataContext DB = new DBHandlerDataContext();
        CutAdmin.dbml.helperDataContext db = new CutAdmin.dbml.helperDataContext();
        DBHelper.DBReturnCode retCode;
        List<RecordInv> Record = new List<RecordInv>();
        public class Addons
        {
            public string Date { get; set; }
            public string Name { get; set; }
            public string Type { get; set; }
            public string TotalRate { get; set; }
            public string Quantity { get; set; }

            public Int64 Id { get; set; }
            public Int64 AddOnsID { get; set; }
            public bool IsCumpulsary { get; set; }
            public Int64 HotelID { get; set; }
            public Int64 RateID { get; set; }
            public string RateType { get; set; }
            public Decimal Rate { get; set; }
            public Int64 UserID { get; set; }


        }
        public class RecordInv
        {
            public string BookingId { get; set; }
            public string InvSid { get; set; }
            public string SupplierId { get; set; }
            public string HotelCode { get; set; }
            public string RoomType { get; set; }
            public string RateType { get; set; }
            public string InvType { get; set; }
            public string Date { get; set; }
            public string Month { get; set; }
            public string Year { get; set; }
            public string TotalAvlRoom { get; set; }
            public string OldAvlRoom { get; set; }
            public string NoOfBookedRoom { get; set; }
            public string NoOfCancleRoom { get; set; }
            public DateTime UpdateDate { get; set; }
            public string UpdateOn { get; set; }
        }

        public class RateGroup
        {
            public string RoomTypeID { get; set; }
            public string RoomDescriptionId { get; set; }
            public string Total { get; set; }
            public int noRooms { get; set; }
            public int AdultCount { get; set; }
            public int ChildCount { get; set; }
            public string ChildAges { get; set; }
        }

        [WebMethod(EnableSession = true)]
        public string GetAvailibility(string Serach, string RoomID, string RoomDescID, int RoomNo)
        {
            try
            {
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Serach];
                List<date> arrDate = RatesManager.GetAvailibility(Serach, RoomID, RoomDescID, RoomNo);
                List<Image> arrImage = RatesManager.GetRoomImage(Convert.ToInt64(RoomID), Convert.ToInt64(arrHotelDetails.HotelId));
                return jsSerializer.Serialize(new { retCode = 1, arrDates = arrDate, arrImage = arrImage });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, errMsg = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GenrateBookingDetails(List<RateGroup> ListRates, string Serach)
        {
            try
            {
                Session["BookingRates" + Serach] = ListRates;
                List<RoomType> Rooms = new List<RoomType>();
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)Session["AvailDetails" + Serach];
                if (arrHotelDetails == null)
                    throw new Exception("Please Search again ,Rates are Changged.");
                List<CommonLib.Response.RateGroup> objAvailRate = arrHotelDetails.List_RateGroup;
                arrHotelDetails.List_RateGroup[0].Charge = new ServiceCharge();
                float RoomTotal = 0;
                List<TaxRate> other = new List<TaxRate>();
                float Other = 0;
                float Markup = 0;
                List<bool> bList = new List<bool>();
                List<Int64> noInventory = new List<Int64>();
                foreach (var objRate in ListRates)
                {
                    var arrRates = objAvailRate[0].RoomOccupancy.Where(d => d.AdultCount == objRate.AdultCount && d.ChildCount == objRate.ChildCount &&
                                                    d.ChildAges == objRate.ChildAges).FirstOrDefault();
                    if (arrRates != null)
                    {
                        //#region Other Rates
                        //var arrRate = arrRates.Rooms.Where(d => d.RoomTypeId == objRate.RoomTypeID
                        //            && d.RoomDescription == objRate.RoomDescriptionId
                        //            && d.Total == Convert.ToSingle(objRate.Total)).FirstOrDefault();
                        //arrRate.OtherRates.ForEach(d => { d.TotalRate = (d.BaseRate * d.Per / 100); }); //  Take Other Rate
                        //Other += arrRate.OtherRates.Select(d => d.TotalRate).ToList().Sum();
                        //foreach (var objOther in arrRate.OtherRates)
                        //{
                        //    if (other.Where(d => d.RateName == objOther.RateName).ToList().Count == 0)
                        //    {
                        //        other.Add(objOther);
                        //    }
                        //    else
                        //    {
                        //        other.Where(d => d.RateName == objOther.RateName).FirstOrDefault().TotalRate =
                        //            other.Where(d => d.RateName == objOther.RateName).FirstOrDefault().TotalRate + objOther.TotalRate;
                        //    }
                        //}
                        #region Other Rates
                        var arrRate = arrRates.Rooms.Where(d => d.RoomTypeId == objRate.RoomTypeID
                                    && d.RoomDescription == objRate.RoomDescriptionId
                                    && d.Total == Convert.ToSingle(objRate.Total)).FirstOrDefault();
                        arrRate.HotelTaxRates.ForEach(d => { d.TotalRate = (d.BaseRate * d.Per / 100) * arrRate.dates.Count; }); //  Take Other Rate
                        Other += arrRate.HotelTaxRates.Select(d => d.TotalRate).ToList().Sum();
                        foreach (var objOther in arrRate.HotelTaxRates)
                        {
                            if (other.Where(d => d.RateName == objOther.RateName).ToList().Count == 0)
                            {
                                other.Add(objOther);
                            }
                            else
                            {
                                other.Where(d => d.RateName == objOther.RateName).FirstOrDefault().TotalRate =
                                    other.Where(d => d.RateName == objOther.RateName).FirstOrDefault().TotalRate + objOther.TotalRate;
                            }
                        }
                        Markup += arrRate.dates.Select(d => d.AdminMarkup).Sum() + arrRate.dates.Select(d => d.S2SMarkup).Sum();
                        RoomTotal += arrRate.dates.Select(d => d.RoomRate).Sum();//+ arrRate.Dates.Select(d => d.AdminMarkup).Sum() + arrRate.Dates.Select(d => d.S2SMarkup).Sum();
                        bList.Add(arrRate.CancellationPolicy.Any(d => d.CancelRestricted));
                        Rooms.Add(arrRate);
                        Rooms.Last().AdultCount = objRate.AdultCount;
                        Rooms.Last().ChildCount = objRate.ChildCount;
                        Rooms.Last().ChildAges = objRate.ChildAges;
                        #endregion

                        //RoomTotal += arrRate.Total;
                        //bList.Add(arrRate.CancellationPolicy.Any(d => d.CancelRestricted));
                        //Rooms.Add(arrRate);
                        //Rooms.Last().AdultCount = objRate.AdultCount;
                        //Rooms.Last().ChildCount = objRate.ChildCount;
                        //Rooms.Last().ChildAges = objRate.ChildAges;
                        //#endregion

                        #region Check Inventory
                        foreach (var objDate in arrRate.dates)
                        {
                            if (objDate.NoOfCount == 0)
                                objDate.NoOfCount = InventoryManager.GetInventoryCount(arrHotelDetails.HotelId, objDate.RateTypeId, objDate.RoomTypeId.ToString(), objDate.datetime, AccountManager.GetSupplierByUser());
                            noInventory.Add(objDate.NoOfCount);

                        }
                        #endregion
                    }

                }



                //if (noInventory.Any(d => d == 0) && bList.Any(d => d == true))
                //    throw new Exception("Inventory is not available ,Please Contact Administrator or Change checking Date.");
                bool OnHold = false, OnRequest = false;
                DateTime ComapreDate = RatesManager.OnHoldDate(Rooms);
                if (noInventory.All(d => d == 0) && bList.Any(d => d != true))
                    OnRequest = true;
                else if (noInventory.All(d => d != 0) && bList.Any(d => d != true) && ComapreDate >= DateTime.Now)
                    OnHold = true;
                Session["RateList" + Serach] = Rooms;
                var arrHotelCharge = new ServiceCharge { RoomRate = RoomTotal, HotelTaxes = other, TotalPrice = RoomTotal + other.Select(d => d.TotalRate).ToList().Sum(), CUTMarkup = Markup };
                arrHotelDetails.List_RateGroup[0].Charge = arrHotelCharge;
                Session["AvailDetails" + Serach] = arrHotelDetails;
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, OnHold = OnHold, OnRequest = OnRequest });

            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ex = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetBookingDetails(string Serach)
        {
            try
            {
                List<RoomType> Rooms = (List<RoomType>)Session["RateList" + Serach];
                List<RateGroup> ListRates = (List<RateGroup>)Session["BookingRates" + Serach];
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)Session["AvailDetails" + Serach];
                return jsSerializer.Serialize(new { retCode = 1, ListRates = Rooms, arrHotel = arrHotelDetails, arrHotelDetails.List_RateGroup[0].Charge });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
        }


        [WebMethod(EnableSession = true)]
        public string ValidateTransaction(List<Addons> arrAdons, string Serach)
        {
            jsSerializer = new JavaScriptSerializer();
            bool IsValid = false;
            try
            {
                CommonHotelDetails arrHotelDetails = (CommonHotelDetails)Session["AvailDetails" + Serach];
                List<CommonLib.Response.RateGroup> objAvailRate = arrHotelDetails.List_RateGroup;
                float AddOnsPrice = 0;
                if (arrAdons.Count != 0)
                {
                    arrAdons.ForEach(d => d.TotalRate = (Convert.ToSingle(d.TotalRate) * Convert.ToSingle(d.Quantity)).ToString());
                    AddOnsPrice = arrAdons.Select(d => Convert.ToSingle(d.TotalRate)).ToList().Sum();
                }
                if (Session["AvailDetails" + Serach] == null)
                    throw new Exception("Not Valid Booking Please Search again.");
                List<RatesManager.Supplier> Supplier = (List<RatesManager.Supplier>)Session["RatesDetails"];
                //float BaseRate = Supplier[0].Details.Select(d => d.Rate.TotalCharge.TotalRate).ToList().Sum() + AddOnsPrice;
                float BaseRate = objAvailRate[0].Charge.TotalPrice + AddOnsPrice;
                #region checking AvailCredit with Booking Amount
                float AvailableCredit = 0;
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again");
                Int64 Uid = AccountManager.GetUserByLogin();
                CutAdmin.dbml.helperDataContext dbTax = new CutAdmin.dbml.helperDataContext();
                var dtAvailableCredit = (from obj in dbTax.tbl_AdminCreditLimits where obj.uid == Uid select obj).FirstOrDefault();
                if (dtAvailableCredit == null)
                    throw new Exception("Please Contact  Administrator and try Again");
                AvailableCredit = Convert.ToSingle(dtAvailableCredit.AvailableCredit);
                float @Creditlimit = Convert.ToSingle(dtAvailableCredit.CreditAmount);
                bool Credit_Flag = (bool)dtAvailableCredit.Credit_Flag;
                bool OTC = (bool)dtAvailableCredit.OTC;
                float @MAXCreditlimit = Convert.ToSingle(dtAvailableCredit.MaxCreditLimit);
                if (AvailableCredit >= 0 && AvailableCredit >= BaseRate)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit <= 0 && @Creditlimit <= 0 && @MAXCreditlimit >= BaseRate && @OTC == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @MAXCreditlimit + @Creditlimit) >= BaseRate && @OTC == true && @Credit_Flag == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @MAXCreditlimit) >= BaseRate && @OTC == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit >= 0 && (@AvailableCredit + @Creditlimit) >= BaseRate && @Credit_Flag == true)
                {
                    IsValid = true;
                }
                else if (@AvailableCredit < 0 && @Creditlimit > 0 && @Creditlimit >= BaseRate && @Credit_Flag == true)
                {
                    IsValid = true;
                }

                #endregion
                CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                if (objGlobalDefault.UserType == "Agent")
                {
                    if (IsValid)
                        return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
                    else
                        throw new Exception("Your Balance is insufficient to Make this booking");
                }
                else
                {
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
                }

            }

            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ErrorMsg = ex.Message });
            }

        }

        //[WebMethod(EnableSession = true)]
        //public string BookHotel(List<Addons> arrAddOns, string Serach, List<CommonLib.Request.Customer> LisCustumer)
        //{
        //    try
        //    {
        //        helperDataContext DB = new helperDataContext();
        //        Click_Hotel db = new Click_Hotel();
        //        List<RateGroup> ListRates = (List<RateGroup>)Session["BookingRates" + Serach];
        //        CommonHotelDetails arrHotelDetails = (CommonHotelDetails)Session["AvailDetails" + Serach];
        //        Int64 Uid = 0; string sTo = "";
        //        if (HttpContext.Current.Session["LoginUser"] == null)
        //            throw new Exception("Session expired.");
        //       CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //        Uid = objGlobalDefault.sid;

        //        if (objGlobalDefault.UserType != "Supplier")
        //        {
        //            Uid = objGlobalDefault.ParentId;
        //            sTo = objGlobalDefault.uid;
        //            sTo += "," + (from obj in DB.tbl_AdminLogins where obj.sid == Uid select obj).FirstOrDefault().uid;
        //        }
        //        else
        //        {
        //            sTo = objGlobalDefault.uid;
        //        }
        //        var arrLastReservationID = (from obj in db.tbl_CommonHotelReservations select obj).ToList().Count;
        //        String ReservationID = (arrLastReservationID + 1).ToString("D" + 6);
        //        bool response = false;
        //        #region Inventory Update & Validate
        //        List<RoomType> ListRate = (List<RoomType>)Session["RateList" + Serach];
        //        bool valid = InventoryManager.CheckInventory(arrHotelDetails.HotelId, ListRate, AccountManager.GetSupplierByUser().ToString());
        //        if (valid)
        //        {

        //            response = InventoryManager.UpdateInventory(ListRate, ReservationID, arrHotelDetails.HotelId);
        //            List<tbl_CommonInventoryRecord> AddRecord = new List<tbl_CommonInventoryRecord>();
        //            List<InventoryManager.RecordInv> Record = InventoryManager.Record;
        //            for (int i = 0; i < Record.Count; i++)
        //            {
        //                tbl_CommonInventoryRecord Recordnew = new tbl_CommonInventoryRecord();
        //                Recordnew.BookingId = Record[i].BookingId;
        //                Recordnew.InvSid = Record[i].InvSid;
        //                Recordnew.SupplierId = Record[i].SupplierId;
        //                Recordnew.HotelCode = Record[i].HotelCode;
        //                Recordnew.RoomType = Record[i].RoomType;
        //                Recordnew.RateType = Record[i].RateType;
        //                Recordnew.InvType = Record[i].InvType;
        //                Recordnew.Date = Record[i].Date;
        //                Recordnew.Month = Record[i].Month;
        //                Recordnew.Year = Record[i].Year;
        //                Recordnew.TotalAvlRoom = Record[i].TotalAvlRoom;
        //                Recordnew.OldAvlRoom = Record[i].OldAvlRoom;
        //                Recordnew.NoOfBookedRoom = Record[i].NoOfBookedRoom;
        //                Recordnew.NoOfCancleRoom = Record[i].NoOfCancleRoom;
        //                Recordnew.UpdateDate = Record[i].UpdateDate.ToString();
        //                Recordnew.UpdateOn = Record[i].UpdateOn;
        //                AddRecord.Add(Recordnew);
        //            }
        //            db.tbl_CommonInventoryRecords.InsertAllOnSubmit(AddRecord);
        //            db.SubmitChanges();
        //        }
        //        #endregion

        //        /*AB Qudus*/
        //        #region AddOns  Save
        //        List<tbl_CommonBookingHotelAddon> Addons = new List<tbl_CommonBookingHotelAddon>();
        //        for (int l = 0; l < arrAddOns.Count; l++)
        //        {

        //            string[] Date = arrAddOns[l].Date.Split('_');
        //            Addons.Add(new tbl_CommonBookingHotelAddon
        //            {
        //                BookingId = ReservationID,
        //                Date = Date[0],
        //                RoomNo = Date[1],
        //                Name = arrAddOns[l].Name,
        //                Type = arrAddOns[l].Type,
        //                Quantity = arrAddOns[l].Quantity,
        //                Rate = arrAddOns[l].TotalRate,
        //            });
        //        }
        //        db.tbl_CommonBookingHotelAddons.InsertAllOnSubmit(Addons);
        //        db.SubmitChanges();
        //        Addons.ForEach(r => r.Rate = (Convert.ToDecimal(r.Rate) * Convert.ToDecimal(r.Quantity)).ToString());
        //        #endregion

        //        #region Booking Transactions
        //        CutAdmin.DataLayer.ReservationDetails objReservation = new ReservationDetails();
        //        objReservation.TotalAmount = arrHotelDetails.RateList[0].Charge.TotalPrice;
        //        objReservation.AdminCommission = 0;
        //        objReservation.HotelCharges = new List<TaxRate>();
        //        foreach (var objRate in arrHotelDetails.RateList[0].Charge.HotelTaxes)
        //        {
        //            objReservation.HotelCharges.Add(objRate);
        //        }
        //        Int64 noNights = Convert.ToInt64(ListRate[0].Dates.Count);
        //        Int64 uid = AccountManager.GetSupplierByUser();
        //        objReservation.AdminCommission = BookingManager.AdminCommission(Convert.ToInt64(arrHotelDetails.HotelId), uid, noNights);
        //        objReservation.AddOnsCharge = Addons.Select(D => Convert.ToSingle(D.Rate)).ToList().Sum();
        //        CutAdmin.DataLayer.BookingManager.objReservation = objReservation;
        //        objReservation.TotalPayable = BookingManager.TransactionAmount("AG");
        //        #region Hotel Booking Details
        //        var arrHotel = (from obj in db.tbl_CommonHotelMaster where obj.sid == Convert.ToInt64(arrHotelDetails.HotelId) select obj).FirstOrDefault();
        //        tbl_CommonHotelReservation HotelReserv = new tbl_CommonHotelReservation();
        //        HotelReserv.ReservationID = Convert.ToString(ReservationID);
        //        HotelReserv.HotelCode = arrHotelDetails.HotelId;
        //        HotelReserv.HotelName = arrHotelDetails.HotelName;
        //        HotelReserv.mealplan = ListRate[0].RoomDescription;
        //        HotelReserv.TotalRooms = Convert.ToInt16(ListRate.Count);
        //        //HotelReserv.TotalRooms = Convert.ToInt16(Supplier[0].Details.Select(d => d.noRooms));
        //        HotelReserv.RoomCode = "";
        //        HotelReserv.sightseeing = 0;
        //        HotelReserv.Source = "From Hotel";
        //        if (response)
        //            HotelReserv.Status = "Vouchered";
        //        else
        //            HotelReserv.Status = "OnRequest";
        //        HotelReserv.SupplierCurrency = "INR";
        //        HotelReserv.terms = arrHotel.HotelNote;
        //        HotelReserv.ParentID = Uid;
        //        HotelReserv.Uid = AccountManager.GetUserByLogin();
        //        HotelReserv.Updatedate = DateTime.Now.ToString("dd-MM-yyyy");
        //        HotelReserv.Updateid = objGlobalDefault.sid.ToString();
        //        HotelReserv.VoucherID = "VCH-" + ReservationID;
        //        HotelReserv.DeadLine = "";
        //        HotelReserv.AgencyName = "";
        //        HotelReserv.AgentContactNumber = "";
        //        HotelReserv.AgentEmail = "";
        //        HotelReserv.AgentMarkUp_Per = 0;
        //        HotelReserv.AgentRef = "";
        //        HotelReserv.AgentRemark = "";
        //        HotelReserv.AutoCancelDate = "";
        //        HotelReserv.AutoCancelTime = "";
        //        HotelReserv.bookingname = LisCustumer[0].Title + ". " + LisCustumer[0].Name + " " + LisCustumer[0].LastName;
        //        HotelReserv.BookingStatus = "";
        //        HotelReserv.CancelDate = "";
        //        HotelReserv.CancelFlag = false;
        //        HotelReserv.CheckIn = arrHotelDetails.DateFrom;
        //        HotelReserv.CheckOut = arrHotelDetails.DateTo;
        //        HotelReserv.ChildAges = "";
        //        HotelReserv.Children = Convert.ToInt16(arrHotelDetails.RateList[0].RoomOccupancy.Select(d => d.ChildCount).ToList().Sum());
        //        HotelReserv.City = arrHotel.CityId;
        //        HotelReserv.deadlineemail = false;
        //        HotelReserv.Discount = 0;
        //        HotelReserv.ExchangeValue = 0;
        //        HotelReserv.ExtraBed = 0;
        //        HotelReserv.GTAId = "";
        //        HotelReserv.holdbooking = 0;
        //        HotelReserv.HoldTime = "";
        //        HotelReserv.HotelBookingData = "";
        //        HotelReserv.HotelDetails = "";
        //        HotelReserv.Infants = 0;
        //        HotelReserv.InvoiceID = "-";
        //        HotelReserv.IsAutoCancel = false;
        //        HotelReserv.IsConfirm = false;
        //        HotelReserv.LatitudeMGH = arrHotel.HotelLatitude;
        //        HotelReserv.LongitudeMGH = arrHotel.HotelLatitude;
        //        HotelReserv.LuxuryTax = 0;
        //        HotelReserv.mealplan_Amt = 0;
        //        HotelReserv.NoOfAdults = Convert.ToInt16(arrHotelDetails.RateList[0].RoomOccupancy.Select(d => d.AdultCount).ToList().Sum());
        //        // HotelReserv.NoOfDays = Convert.ToInt16(Supplier[0].Details[0].Rate.ListDates.Select(d => d.Date).ToList().Count());
        //        HotelReserv.NoOfDays = Convert.ToInt16(noNights);
        //        HotelReserv.OfferAmount = 0;
        //        HotelReserv.OfferId = 0;
        //        HotelReserv.RefAgency = "";
        //        HotelReserv.ReferenceCode = "";
        //        HotelReserv.Remarks = "";
        //        HotelReserv.ReservationDate = DateTime.Now.ToString("dd-MM-yyyy");
        //        HotelReserv.ReservationTime = "";
        //        HotelReserv.RoomRate = 0;
        //        HotelReserv.SalesTax = 0;
        //        HotelReserv.Servicecharge = 0;
        //        HotelReserv.ComparedFare = Convert.ToDecimal(objReservation.TotalAmount + objReservation.HotelCharges.Select(d => d.TotalRate).ToList().Sum()) + Addons.Select(D => Convert.ToDecimal(D.Rate)).ToList().Sum();
        //        HotelReserv.ComparedCurrency = "";
        //        // HotelReserv.TotalFare = Convert.ToDecimal(Supplier[0].Details[0].Rate.ListDates.Select(d => d.TotalPrice).ToList().Sum());
        //        // HotelReserv.TotalFare = Total;
        //        HotelReserv.TotalFare = Convert.ToDecimal(BookingManager.TransactionAmount("AG")); //- objReservation.AdminCommission);
        //        db.tbl_CommonHotelReservations.InsertOnSubmit(HotelReserv);
        //        DB.SubmitChanges();
        //        #endregion
        //        objReservation.objHotelDetails = new tbl_CommonHotelReservation();
        //        objReservation.objHotelDetails = HotelReserv;
        //        BookingManager.objReservation.objHotelDetails = new tbl_CommonHotelReservation();
        //        BookingManager.objReservation.objHotelDetails = HotelReserv;
        //        if (valid)
        //            retCode = CutAdmin.DataLayer.BookingManager.BookingTransact("Hotel");
        //        #endregion

        //        //if (HotelReserv.Status == "OnRequest")
        //        //{
        //        //    OnRequestMail(Convert.ToInt64(HotelReserv.Uid), HotelReserv.HotelName, HotelReserv.ReservationID);
        //        //}

        //        #region Booked Room
        //        int b = 0;
        //        Click_Hotel dbRoom = new Click_Hotel();
        //        List<tbl_CommonBookedRoom> arrBookedRoom = new List<tbl_CommonBookedRoom>();
        //        List<tbl_CommonBookedPassenger> arrPax = new List<tbl_CommonBookedPassenger>();
        //        foreach (var objRoom in ListRate)
        //        {


        //            string Ages = "";
        //            string CancellationAmountMultiple = "";
        //            string CancellationDateTime = "";
        //            Ages = objRoom.ChildAges;
        //            tbl_CommonBookedRoom BookedRoom = new tbl_CommonBookedRoom();
        //            BookedRoom.Adults = objRoom.AdultCount;
        //            BookedRoom.BoardText = Convert.ToString(objRoom.RoomDescription);
        //            BookedRoom.CanAmtWithTax = "";
        //            BookedRoom.RoomAmount = Convert.ToDecimal(objRoom.Total) + 0;
        //            foreach (var Canclellation in objRoom.CancellationPolicy)
        //            {
        //                CancellationAmountMultiple += Canclellation.objCharges.TotalRate + "|";
        //                CancellationDateTime += Canclellation.CancellationDateTime + "|";
        //            }
        //            BookedRoom.CancellationAmount = CancellationAmountMultiple;
        //            BookedRoom.Child = objRoom.ChildCount;
        //            BookedRoom.ChildAge = Ages;
        //            BookedRoom.Commision = 0;
        //            BookedRoom.CutCancellationDate = CancellationDateTime;
        //            BookedRoom.Discount = 0;
        //            BookedRoom.EssentialInfo = "";
        //            BookedRoom.ExchangeRate = 0;
        //            BookedRoom.FranchiseeMarkup = 0;
        //            BookedRoom.FranchiseeTaxDetails = "";
        //            BookedRoom.LeadingGuest = "";
        //            BookedRoom.MealPlanID = Convert.ToString(objRoom.RoomTypeId);
        //            BookedRoom.Remark = "";
        //            BookedRoom.ReservationID = Convert.ToString(ReservationID);
        //            BookedRoom.TaxDetails = "";
        //            foreach (var objCharge in objRoom.HotelTaxRates)
        //            {
        //                BookedRoom.TaxDetails += objCharge.RateName + ":" + objCharge.TotalRate;
        //            }
        //            BookedRoom.RoomAmtWithTax = 0;
        //            BookedRoom.RoomCode = Convert.ToString(objRoom.RoomDescriptionId);
        //            //BookedRoom.RoomNumber = Convert.ToString(ListRate.IndexOf(objRoom) + 1);
        //            BookedRoom.RoomNumber = Convert.ToString(b + 1);
        //            BookedRoom.RoomServiceTax = 0;
        //            BookedRoom.RoomType = objRoom.RoomTypeName;
        //            BookedRoom.SupplierAmount = 0;
        //            BookedRoom.SupplierCurrency = objRoom.Dates[0].Currency;
        //            BookedRoom.SupplierNoChargeDate = "";
        //            BookedRoom.TDS = 0;
        //            BookedRoom.TotalRooms = Convert.ToInt16(ListRate.Where(d => d.RoomTypeId == objRoom.RoomTypeId && d.RoomDescription == objRoom.RoomDescription).ToList().Count);
        //            arrBookedRoom.Add(BookedRoom);
        //            // var arrRoomPax = LisCustumer.Where(d => d.RoomNo == ListRate.IndexOf(objRoom) + 1).ToList();
        //            var arrRoomPax = LisCustumer.Where(d => d.RoomNo == b + 1).ToList();
        //            foreach (var objPax in arrRoomPax)
        //            {
        //                tbl_CommonBookedPassenger BookedPassenger = new tbl_CommonBookedPassenger();
        //                BookedPassenger.Age = objPax.Age;
        //                if (ListRate.IndexOf(objRoom) == 0)
        //                    BookedPassenger.IsLeading = true;
        //                else
        //                    BookedPassenger.IsLeading = false;
        //                BookedPassenger.LastName = objPax.LastName;
        //                BookedPassenger.Name = objPax.Title + " " + objPax.Name;
        //                BookedPassenger.PassengerType = objPax.type;
        //                BookedPassenger.ReservationID = ReservationID;
        //                BookedPassenger.RoomCode = "";
        //                //BookedPassenger.RoomNumber = Convert.ToString(ListRate.IndexOf(objRoom) + 1);
        //                BookedPassenger.RoomNumber = Convert.ToString(b + 1);
        //                arrPax.Add(BookedPassenger);
        //            }

        //            b++;
        //        }
        //        dbRoom.tbl_CommonBookedRooms.InsertAllOnSubmit(arrBookedRoom);
        //        dbRoom.tbl_CommonBookedPassengers.InsertAllOnSubmit(arrPax);
        //        dbRoom.SubmitChanges();
        //        #endregion
        //        MailManager.SendInvoice(ReservationID, Uid, sTo);
        //        return jsSerializer.Serialize(new { retCode = 1, BookingID = ReservationID });
        //    }
        //    catch (Exception ex)
        //    {
        //        return jsSerializer.Serialize(new { });
        //    }
        //}

        //[WebMethod(EnableSession = true)]
        //public string Booking(List<RatesManager.Supplier> Supplier, Int16 Total, List<string> Amount, List<Addons> arrAddOns)
        //{
        //    try
        //    {
        //        /*Checking User Details*/
        //        Int64 Uid = 0; string sTo = "";
        //        if (HttpContext.Current.Session["LoginUser"] == null)
        //            throw new Exception("Session expired.");
        //       CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //        Uid = objGlobalDefault.sid;
        //        if (objGlobalDefault.UserType != "Supplier")
        //        {
        //            Uid = objGlobalDefault.ParentId;
        //            sTo = objGlobalDefault.uid;
        //            using (var DB = new helperDataContext())
        //            {
        //                sTo += "," + (from obj in DB.tbl_AdminLogins where obj.sid == Uid select obj).FirstOrDefault().uid;
        //            }
        //        }
        //        else
        //        {
        //            sTo = objGlobalDefault.uid;
        //        }

        //        String ReservationID = "";
        //        using (var DB = new Click_Hotel())
        //        {
        //            var arrLastReservationID = (from obj in DB.tbl_CommonHotelReservations select obj).ToList().Count;
        //            if (arrLastReservationID == 0)
        //                arrLastReservationID = 0;
        //            ReservationID = (arrLastReservationID + 1).ToString("D" + 6);
        //        }

        //        bool response = false;

        //        #region Inventory Update & Validate
        //        bool valid = InventoryManager.CheckInventory(Supplier, AccountManager.GetSupplierByUser().ToString());
        //        if (valid)
        //        {
        //            response = InventoryManager.UpdateInventory(Supplier, ReservationID);
        //            using (var DB = new Click_Hotel())
        //            {
        //                List<tbl_CommonInventoryRecord> AddRecord = new List<tbl_CommonInventoryRecord>();
        //                List<InventoryManager.RecordInv> Record = InventoryManager.Record;
        //                for (int i = 0; i < Record.Count; i++)
        //                {
        //                    tbl_CommonInventoryRecord Recordnew = new tbl_CommonInventoryRecord();
        //                    Recordnew.BookingId = Record[i].BookingId;
        //                    Recordnew.InvSid = Record[i].InvSid;
        //                    Recordnew.SupplierId = Record[i].SupplierId;
        //                    Recordnew.HotelCode = Record[i].HotelCode;
        //                    Recordnew.RoomType = Record[i].RoomType;
        //                    Recordnew.RateType = Record[i].RateType;
        //                    Recordnew.InvType = Record[i].InvType;
        //                    Recordnew.Date = Record[i].Date;
        //                    Recordnew.Month = Record[i].Month;
        //                    Recordnew.Year = Record[i].Year;
        //                    Recordnew.TotalAvlRoom = Record[i].TotalAvlRoom;
        //                    Recordnew.OldAvlRoom = Record[i].OldAvlRoom;
        //                    Recordnew.NoOfBookedRoom = Record[i].NoOfBookedRoom;
        //                    Recordnew.NoOfCancleRoom = Record[i].NoOfCancleRoom;
        //                    Recordnew.UpdateDate = Record[i].UpdateDate.ToString();
        //                    Recordnew.UpdateOn = Record[i].UpdateOn;
        //                    AddRecord.Add(Recordnew);
        //                }
        //                DB.tbl_CommonInventoryRecords.InsertAllOnSubmit(AddRecord);
        //                DB.SubmitChanges();
        //            }
        //        }
        //        #endregion

        //        /*AB Qudus*/
        //        #region AddOns Save
        //        List<tbl_CommonBookingHotelAddon> Addons = new List<tbl_CommonBookingHotelAddon>();
        //        using (var DB = new Click_Hotel())
        //        {
        //            for (int l = 0; l < arrAddOns.Count; l++)
        //            {
        //                string[] Date = arrAddOns[l].Date.Split('_');
        //                Addons.Add(new tbl_CommonBookingHotelAddon
        //                {
        //                    BookingId = ReservationID,
        //                    Date = Date[0],
        //                    RoomNo = Date[1],
        //                    Name = arrAddOns[l].Name,
        //                    Type = arrAddOns[l].Type,
        //                    Quantity = arrAddOns[l].Quantity,
        //                    Rate = arrAddOns[l].TotalRate,
        //                });
        //            }
        //            DB.tbl_CommonBookingHotelAddons.InsertAllOnSubmit(Addons);
        //            DB.SubmitChanges();
        //            Addons.ForEach(r => r.Rate = (Convert.ToDecimal(r.Rate) * Convert.ToDecimal(r.Quantity)).ToString());
        //        }
        //        #endregion



        //        #region Booking Transactions
        //        CutAdmin.DataLayer.ReservationDetails objReservation = new ReservationDetails();
        //        objReservation.TotalAmount = Supplier[0].Details.Select(r => r.Rate.TotalCharge.BaseRate).ToList().Sum();
        //        objReservation.AdminCommission = 0;
        //        objReservation.HotelCharges = new List<TaxRate>();
        //        foreach (var objRoom in Supplier[0].Details)
        //        {
        //            foreach (var objRate in objRoom.Rate.TotalCharge.Charge)
        //            {
        //                if (objReservation.HotelCharges.Where(d => d.RateName == objRate.RateName).ToList().Count == 0)
        //                {
        //                    objReservation.HotelCharges.Add(objRate);
        //                }
        //                else
        //                {
        //                    objReservation.HotelCharges.Single(d => d.RateName == objRate.RateName).TotalRate += objRate.TotalRate;
        //                }
        //            }


        //        }
        //        Int64 noNights = Convert.ToInt64(Supplier[0].Details[0].Rate.ListDates.Count - 1);
        //        Int64 uid = AccountManager.GetSupplierByUser();
        //        objReservation.AdminCommission = BookingManager.AdminCommission(Convert.ToInt64(Supplier[0].HotelCode), uid, noNights);
        //        objReservation.AddOnsCharge = Addons.Select(D => Convert.ToSingle(D.Rate)).ToList().Sum();
        //        CutAdmin.DataLayer.BookingManager.objReservation = objReservation;
        //        objReservation.TotalPayable = BookingManager.TransactionAmount("AG");
        //        #region Hotel Booking Details
        //        using (var DB = new Click_Hotel())
        //        {
        //            var arrHotel = (from obj in DB.tbl_CommonHotelMaster where obj.sid == Convert.ToInt64(Supplier[0].HotelCode) select obj).FirstOrDefault();

        //            tbl_CommonHotelReservation HotelReserv = new tbl_CommonHotelReservation();
        //            HotelReserv.ReservationID = Convert.ToString(ReservationID);
        //            HotelReserv.HotelCode = Supplier[0].HotelCode;
        //            HotelReserv.HotelName = arrHotel.HotelName;
        //            HotelReserv.mealplan = Supplier[0].Details[0].MealName;
        //            HotelReserv.TotalRooms = Convert.ToInt16(Supplier[0].Details.Select(d => d.noRooms).ToList().Sum());
        //            //HotelReserv.TotalRooms = Convert.ToInt16(Supplier[0].Details.Select(d => d.noRooms));
        //            HotelReserv.RoomCode = "";
        //            HotelReserv.sightseeing = 0;
        //            HotelReserv.Source = Supplier[0].Name;
        //            if (response)
        //                HotelReserv.Status = "Vouchered";
        //            else
        //                HotelReserv.Status = "OnRequest";
        //            HotelReserv.SupplierCurrency = "INR";
        //            HotelReserv.terms = arrHotel.HotelNote;
        //            HotelReserv.Uid = Uid;
        //            HotelReserv.Updatedate = DateTime.Now.ToString("dd-MM-yyyy");
        //            HotelReserv.Updateid = objGlobalDefault.sid.ToString();
        //            HotelReserv.VoucherID = "VCH-" + ReservationID;
        //            HotelReserv.DeadLine = "";
        //            HotelReserv.AgencyName = "";
        //            HotelReserv.AgentContactNumber = "";
        //            HotelReserv.AgentEmail = "";
        //            HotelReserv.AgentMarkUp_Per = 0;
        //            HotelReserv.AgentRef = "";
        //            HotelReserv.AgentRemark = "";
        //            HotelReserv.AutoCancelDate = "";
        //            HotelReserv.AutoCancelTime = "";
        //            HotelReserv.bookingname = "";
        //            HotelReserv.BookingStatus = "";
        //            HotelReserv.CancelDate = "";
        //            HotelReserv.CancelFlag = false;
        //            HotelReserv.CheckIn = Convert.ToString(Supplier[0].Details[0].Rate.ListDates.Select(d => d.Date).ToList().First());
        //            HotelReserv.CheckOut = Convert.ToString(Supplier[0].Details[0].Rate.ListDates.Select(d => d.Date).ToList().Last());
        //            HotelReserv.ChildAges = "";
        //            HotelReserv.Children = Supplier[0].Details[0].Rate.LisCustumer.Where(d => d.type == "CH").ToList().Count();
        //            HotelReserv.City = arrHotel.CityId;
        //            HotelReserv.deadlineemail = false;
        //            HotelReserv.Discount = 0;
        //            HotelReserv.ExchangeValue = 0;
        //            HotelReserv.ExtraBed = 0;
        //            HotelReserv.GTAId = "";
        //            HotelReserv.holdbooking = 0;
        //            HotelReserv.HoldTime = "";
        //            HotelReserv.HotelBookingData = "";
        //            HotelReserv.HotelDetails = "";
        //            HotelReserv.Infants = 0;
        //            HotelReserv.InvoiceID = "-";
        //            HotelReserv.IsAutoCancel = false;
        //            HotelReserv.IsConfirm = false;
        //            HotelReserv.LatitudeMGH = arrHotel.HotelLatitude;
        //            HotelReserv.LongitudeMGH = arrHotel.HotelLatitude;
        //            HotelReserv.LuxuryTax = 0;
        //            HotelReserv.mealplan_Amt = 0;
        //            HotelReserv.NoOfAdults = Supplier[0].Details[0].Rate.LisCustumer.Where(d => d.type == "AD").ToList().Count(); ;
        //            Int32 night = Convert.ToInt16(Supplier[0].Details[0].Rate.ListDates.Select(d => d.Date).ToList().Count());
        //            night = night - 1;
        //            // HotelReserv.NoOfDays = Convert.ToInt16(Supplier[0].Details[0].Rate.ListDates.Select(d => d.Date).ToList().Count());
        //            HotelReserv.NoOfDays = Convert.ToInt16(night);
        //            HotelReserv.OfferAmount = 0;
        //            HotelReserv.OfferId = 0;
        //            HotelReserv.RefAgency = "";
        //            HotelReserv.ReferenceCode = "";
        //            HotelReserv.Remarks = "";
        //            HotelReserv.ReservationDate = DateTime.Now.ToString("dd-MM-yyyy");
        //            HotelReserv.ReservationTime = "";
        //            HotelReserv.RoomRate = 0;
        //            HotelReserv.SalesTax = 0;
        //            HotelReserv.Servicecharge = 0;
        //            HotelReserv.ComparedFare = Convert.ToDecimal(objReservation.TotalAmount + objReservation.HotelCharges.Select(d => d.TotalRate).ToList().Sum()) + Addons.Select(D => Convert.ToDecimal(D.Rate)).ToList().Sum();
        //            HotelReserv.ComparedCurrency = "";
        //            // HotelReserv.TotalFare = Convert.ToDecimal(Supplier[0].Details[0].Rate.ListDates.Select(d => d.TotalPrice).ToList().Sum());
        //            // HotelReserv.TotalFare = Total;
        //            HotelReserv.TotalFare = Convert.ToDecimal(BookingManager.TransactionAmount("AG") - objReservation.AdminCommission);
        //            DB.tbl_CommonHotelReservations.InsertOnSubmit(HotelReserv);
        //            DB.SubmitChanges();
        //            #endregion

        //            if (HotelReserv.Status == "OnRequest")
        //            {
        //                OnRequestMail(Convert.ToInt64(HotelReserv.Uid), HotelReserv.HotelName, HotelReserv.ReservationID);
        //            }

        //            CUT.DataLayer.AgentDetailsOnAdmin objAgentDetailsOnAdmin = new CUT.DataLayer.AgentDetailsOnAdmin();
        //            objReservation.objHotelDetails = new tbl_CommonHotelReservation();
        //            objReservation.objHotelDetails = HotelReserv;
        //            BookingManager.objReservation.objHotelDetails = new tbl_CommonHotelReservation();
        //            BookingManager.objReservation.objHotelDetails = HotelReserv;
        //            DBHelper.DBReturnCode retCode = CutAdmin.DataLayer.BookingManager.BookingTransact("Hotel");
        //        }
        //        #endregion



        //        #region Booked Room

        //        for (var i = 0; i < Supplier[0].Details.Count; i++)
        //        {

        //            for (int j = 0; j < Supplier[0].Details[i].noRooms; j++)
        //            {

        //                string Ages = "";
        //                string CancellationAmountMultiple = "";
        //                string CancellationDateTime = "";
        //                foreach (var ages in Supplier[0].Details[i].Rate.LisCustumer.Where(d => d.RoomNo == j + 1 && d.type == "CH").ToList().Select(c => c.Age).ToList())
        //                {
        //                    Ages += ages + "|";
        //                }
        //                using (var DB = new Click_Hotel())
        //                {
        //                    tbl_CommonBookedRoom BookedRoom = new tbl_CommonBookedRoom();
        //                    BookedRoom.Adults = Supplier[0].Details[i].Rate.LisCustumer.Where(d => d.RoomNo == j + 1 && d.type == "AD").ToList().Count;
        //                    BookedRoom.BoardText = Convert.ToString(Supplier[0].Details[i].MealName);
        //                    BookedRoom.CanAmtWithTax = "";
        //                    // BookedRoom.CancellationAmount = Convert.ToString(Supplier[0].Details[i].Rate.ListCancellation[0].CancellationAmount);

        //                    //for (int k = 0; k < Amount.Count; k++)
        //                    //{
        //                    //    if (Amount[k] == Amount[j])
        //                    //    {
        //                    //        BookedRoom.RoomAmount = Convert.ToDecimal(Amount[k]);
        //                    //    }

        //                    //}

        //                    //var ChargeAddOns= Addons.Where(d=>d.RoomNo == (i+1).ToString()).Select(r=> Convert.ToDecimal( r.Rate)).ToList().Sum();
        //                    BookedRoom.RoomAmount = Convert.ToDecimal(Supplier[0].Details[i].Rate.TotalCharge.BaseRate) + 0;
        //                    foreach (var Canclellation in Supplier[0].Details[i].Rate.ListCancellation.ToList())
        //                    {
        //                        if (Canclellation.nonRefundable == true)
        //                        {
        //                            CancellationAmountMultiple += BookedRoom.RoomAmount + "|";
        //                            CancellationDateTime += Canclellation.CancellationDateTime + "|";
        //                        }
        //                        else
        //                        {
        //                            CancellationAmountMultiple += Canclellation.objCharges.TotalRate + "|";
        //                            CancellationDateTime += Canclellation.CancellationDateTime + "|";
        //                        }


        //                    }

        //                    BookedRoom.CancellationAmount = CancellationAmountMultiple;
        //                    BookedRoom.Child = Supplier[0].Details[i].Rate.LisCustumer.Where(d => d.RoomNo == j + 1 && d.type == "CH").ToList().Count;
        //                    BookedRoom.ChildAge = Ages;
        //                    BookedRoom.Commision = 0;
        //                    BookedRoom.CutCancellationDate = CancellationDateTime;
        //                    BookedRoom.Discount = 0;
        //                    BookedRoom.EssentialInfo = "";
        //                    BookedRoom.ExchangeRate = 0;
        //                    BookedRoom.FranchiseeMarkup = 0;
        //                    BookedRoom.FranchiseeTaxDetails = "";
        //                    BookedRoom.LeadingGuest = "";
        //                    BookedRoom.MealPlanID = Convert.ToString(Supplier[0].Details[i].MealID);
        //                    BookedRoom.Remark = "";
        //                    BookedRoom.ReservationID = Convert.ToString(ReservationID);
        //                    //BookedRoom.RoomAmount = Convert.ToInt64(Supplier[0].Details[i].Rate.ListDates[i].TotalPrice);
        //                    BookedRoom.TaxDetails = "";
        //                    foreach (var objCharge in Supplier[0].Details[i].Rate.TotalCharge.Charge)
        //                    {
        //                        BookedRoom.TaxDetails += objCharge.RateName + ":" + objCharge.TotalRate;
        //                    }

        //                    BookedRoom.RoomAmtWithTax = 0;
        //                    BookedRoom.RoomCode = Convert.ToString(Supplier[0].Details[i].RateTypeID);
        //                    BookedRoom.RoomNumber = Convert.ToString(i + 1);
        //                    BookedRoom.RoomServiceTax = 0;
        //                    BookedRoom.RoomType = Supplier[0].Details[i].RateType;
        //                    BookedRoom.SupplierAmount = 0;
        //                    BookedRoom.SupplierCurrency = "INR";
        //                    BookedRoom.SupplierNoChargeDate = "";


        //                    BookedRoom.TDS = 0;
        //                    BookedRoom.TotalRooms = Convert.ToInt16(Supplier[0].Details.Select(d => d.noRooms).ToList().Sum());
        //                    //   BookedRoom.TotalRooms = Convert.ToInt16(Supplier[0].Details.Select(d => d.noRooms)); 

        //                    DB.tbl_CommonBookedRooms.InsertOnSubmit(BookedRoom);
        //                }
        //            }

        //            // DB.SubmitChanges();
        //            using (var DB = new Click_Hotel())
        //            {
        //                for (int k = 0; k < Supplier[0].Details[i].Rate.LisCustumer.Count; k++)
        //                {
        //                    tbl_CommonBookedPassenger BookedPassenger = new tbl_CommonBookedPassenger();
        //                    BookedPassenger.Age = Supplier[0].Details[i].Rate.LisCustumer[k].Age;
        //                    if (i == 0)
        //                        BookedPassenger.IsLeading = true;
        //                    else
        //                        BookedPassenger.IsLeading = false;
        //                    BookedPassenger.LastName = Supplier[0].Details[i].Rate.LisCustumer[k].LastName;
        //                    BookedPassenger.Name = Supplier[0].Details[i].Rate.LisCustumer[k].Title + " " + Supplier[0].Details[i].Rate.LisCustumer[k].Name;
        //                    BookedPassenger.PassengerType = Supplier[0].Details[i].Rate.LisCustumer[k].type;
        //                    BookedPassenger.ReservationID = ReservationID;
        //                    BookedPassenger.RoomCode = "";
        //                    BookedPassenger.RoomNumber = Convert.ToString(i + 1);

        //                    DB.tbl_CommonBookedPassengers.InsertOnSubmit(BookedPassenger);
        //                }

        //                DB.SubmitChanges();
        //            }
        //        }
        //        #endregion

        //        /*Tausif Work*/
        //        MailManager.SendInvoice(ReservationID, Uid, sTo);
        //        return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
        //    }
        //    catch
        //    {
        //        string Message = EmailManager.GetErrorMessage("Booking Error");
        //        return jsSerializer.Serialize(new { retCode = 0, Session = 1, Message = Message });
        //    }
        //    //   }
        //    //   else
        //    //{

        //    //    return jsSerializer.Serialize(new { retCode = 0, Session = 1, Message = "" });
        //    //}

        //}

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void BookingList()
        {
            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {

                object BookingList = new object { };
                List<CutAdmin.Models.MenuItem> arrMenu = (List<CutAdmin.Models.MenuItem>)Session["Menu"];
                if (arrMenu.Where(d => d.Name == "Service").FirstOrDefault().ChildItem.ToList().Exists(d => d.Name == "Hotel"))
                    BookingList = Reservations.GetBookings();
                Context.Response.Write(jsSerializer.Serialize(BookingList));
            }
            catch
            {
                Context.Response.Write(jsSerializer.Serialize(new { retCode = 0, Session = 1 }));
            }
        }

        [WebMethod(EnableSession = true)]
        public string GroupBookingList()
        {
            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                DataTable dtResult = new DataTable();
                Session["SupplierBookingListGroup"] = null;
                List<Reservations> BookingList = Reservations.GroupBookingList();
                GenralHandler.ListtoDataTable lsttodt = new GenralHandler.ListtoDataTable();
                dtResult = lsttodt.ToDataTable(BookingList);
                Session["SupplierBookingListGroup"] = dtResult;
                dtResult.Dispose();
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = BookingList.OrderByDescending(d => d.Sid).ToList() });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string BookingListFilter(string status, string type)
        {
            string json = "";
            object arrData = new { };
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;
            try
            {
                List<Reservations> BookingListnew = Reservations.BookingList();
                List<Reservations> GroupBookingList = Reservations.GroupBookingList();
                if (status == "Vouchered" && type == "BookingPending")
                {
                    var BookingPending = BookingListnew.Where(d => d.Status == "Vouchered" && d.IsConfirm == false).ToList().OrderByDescending(d => d.Sid).ToList();
                    int TotalCount = BookingPending.Count;
                    BookingPending = BookingPending.Skip(start).Take(length).ToList();
                    arrData = DTResult.GetDataTable(BookingPending, TotalCount);
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = arrData });
                }
                else if (status == "Vouchered" && type == "BookingConfirmed")
                {
                    var BookingConfirmed = BookingListnew.Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    int TotalCount = BookingConfirmed.Count;
                    BookingConfirmed = BookingConfirmed.Skip(start).Take(length).ToList();
                    arrData = DTResult.GetDataTable(BookingConfirmed, TotalCount);
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = arrData });
                }
                else if (status == "Vouchered" && type == "BookingRejected")
                {
                    var BookingRejected = BookingListnew.Where(d => d.Status == "Cancelled").ToList();
                    int TotalCount = BookingRejected.Count;
                    BookingRejected = BookingRejected.Skip(start).Take(length).ToList();
                    arrData = DTResult.GetDataTable(BookingRejected, TotalCount);
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = arrData });
                }
                else if (status == "OnRequest" && type == "BookingPending")
                {
                    var OnHoldBookings = BookingListnew.Where(d => d.Status == "OnRequest" && d.IsConfirm == false).ToList().OrderByDescending(d => d.Sid).ToList();
                    int TotalCount = OnHoldBookings.Count;
                    OnHoldBookings = OnHoldBookings.Skip(start).Take(length).ToList();
                    arrData = DTResult.GetDataTable(OnHoldBookings, TotalCount);
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = arrData });
                }
                else if (status == "OnRequest" && type == "BookingConfirmed")
                {
                    var OnHoldRequested = BookingListnew.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    int TotalCount = OnHoldRequested.Count;
                    OnHoldRequested = OnHoldRequested.Skip(start).Take(length).ToList();
                    arrData = DTResult.GetDataTable(OnHoldRequested, TotalCount);
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = arrData });
                }
                else if (status == "OnRequest" && type == "BookingRejected")
                {
                    var OnHoldConfirmed = BookingListnew.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    int TotalCount = OnHoldConfirmed.Count;
                    OnHoldConfirmed = OnHoldConfirmed.Skip(start).Take(length).ToList();
                    arrData = DTResult.GetDataTable(OnHoldConfirmed, TotalCount);
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = arrData });
                }



                else if (status == "Vouchered" && type == "ReconfirmPending")
                {
                    var ReconfirmPending = BookingListnew.Where(d => d.Status != "Cancelled" && d.IsConfirm == false).ToList().OrderByDescending(d => d.Sid).ToList();
                    int TotalCount = ReconfirmPending.Count;
                    ReconfirmPending = ReconfirmPending.Skip(start).Take(length).ToList();
                    arrData = DTResult.GetDataTable(ReconfirmPending, TotalCount);
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = arrData });
                }
                else if (status == "Vouchered" && type == "ReconfirmRequested")
                {
                    var ReconfirmRequested = BookingListnew.Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    int TotalCount = ReconfirmRequested.Count;
                    ReconfirmRequested = ReconfirmRequested.Skip(start).Take(length).ToList();
                    arrData = DTResult.GetDataTable(ReconfirmRequested, TotalCount);
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = arrData });
                }
                else if (status == "Vouchered" && type == "ReconfirmReject")
                {
                    var ReconfirmReject = BookingListnew.Where(d => d.Status == "Cancelled" && d.IsConfirm == true).ToList().OrderByDescending(d => d.Sid).ToList();
                    int TotalCount = ReconfirmReject.Count;
                    ReconfirmReject = ReconfirmReject.Skip(start).Take(length).ToList();
                    arrData = DTResult.GetDataTable(ReconfirmReject, TotalCount);
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = arrData });
                }

                else if (status == "GroupRequest" && type == "GroupRequestPending")
                {
                    var GroupRequestPending = GroupBookingList.Where(d => d.Status == "GroupRequest" && d.BookingStatus == "GroupRequest").ToList().OrderByDescending(d => d.Sid).ToList();
                    int TotalCount = GroupRequestPending.Count;
                    GroupRequestPending = GroupRequestPending.Skip(start).Take(length).ToList();
                    arrData = DTResult.GetDataTable(GroupRequestPending, TotalCount);
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = arrData });
                }
                else if (status == "GroupRequest" && type == "GroupRequestRequested")
                {
                    var GroupRequestRequested = GroupBookingList.Where(d => d.Status == "Vouchered" && d.BookingStatus == "GroupRequest").ToList().OrderByDescending(d => d.Sid).ToList();
                    int TotalCount = GroupRequestRequested.Count;
                    GroupRequestRequested = GroupRequestRequested.Skip(start).Take(length).ToList();
                    arrData = DTResult.GetDataTable(GroupRequestRequested, TotalCount);
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = arrData });
                }
                else if (status == "GroupRequest" && type == "GroupRequestReject")
                {
                    var GroupRequestReject = GroupBookingList.Where(d => d.Status == "Cancelled" && d.BookingStatus == "GroupRequest").ToList().OrderByDescending(d => d.Sid).ToList();
                    int TotalCount = GroupRequestReject.Count;
                    GroupRequestReject = GroupRequestReject.Skip(start).Take(length).ToList();
                    arrData = DTResult.GetDataTable(GroupRequestReject, TotalCount);
                    json = jsSerializer.Serialize(new { retCode = 1, Session = 1, BookingList = arrData });
                }

            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string Search(string CheckIn, string CheckOut, string Passenger, string BookingDate, string Reference, string HotelName, string Location, string ReservationStatus)
        {
            try
            {
                int TotalCount = 0;
                var SearchBooking = Reservations.Search(CheckIn, CheckOut, Passenger, BookingDate, Reference, HotelName, Location, ReservationStatus, out TotalCount);
                object finalresult = DTResult.GetDataTable(SearchBooking, TotalCount);
            }
            catch (Exception ex)
            {
            }
            return json;

        }

        [WebMethod(EnableSession = true)]
        public string SearchGroupBooking(string CheckIn, string CheckOut, string Passenger, string BookingDate, string Reference, string HotelName, string Location, string ReservationStatus)
        {
            DataTable BookingList = (DataTable)Session["SupplierBookingListGroup"];
            DataRow[] row = null;
            try
            {
                if (CheckIn != "")
                {
                    row = BookingList.Select("CheckIn like '%" + CheckIn + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (CheckOut != "")
                {
                    row = BookingList.Select("CheckOut like '%" + CheckOut + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (Passenger != "")
                {
                    row = BookingList.Select("bookingname like '%" + Passenger + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (BookingDate != "")
                {
                    row = BookingList.Select("ReservationDate like '%" + BookingDate + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (Reference != "")
                {
                    row = BookingList.Select("ReservationID like '%" + Reference + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                if (HotelName != "")
                {
                    row = BookingList.Select("HotelName like '%" + HotelName + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }


                if (Location != "")
                {
                    row = BookingList.Select("City like '%" + Location + "%'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }

                if (ReservationStatus != "" && ReservationStatus != "All")
                {
                    row = BookingList.Select("Status = '" + ReservationStatus + "'");
                    if (row.Length != 0)
                    {
                        BookingList = row.CopyToDataTable();
                        List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                        BookingList.Dispose();
                        json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }

                Session["SearchSupplierBookingList"] = BookingList;
                //List<Dictionary<string, object>> HotelBookingList = JsonStringManager.ConvertDataTable(BookingList);
                //BookingList.Dispose();
                //json = jsSerializer.Serialize(new { Session = 1, retCode = 1, BookingList = HotelBookingList });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;

        }

        [WebMethod(EnableSession = true)]
        public string BookingCancle(string ReservationID, string Status)
        {
            try
            {
                CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 Uid = AccountManager.GetUserByLogin();
                //if (objGlobalDefault.UserType != "Supplier")
                //    Uid = objGlobalDefault.ParentId;
                bool response = InventoryManager.UpdateInvOnCancellation(ReservationID);
                using (var DB = new Click_Hotel())
                {

                    if (response)
                    {
                        List<tbl_CommonInventoryRecord> AddRecord = new List<tbl_CommonInventoryRecord>();
                        List<InventoryManager.RecordInv> Record = InventoryManager.Record;
                        for (int i = 0; i < Record.Count; i++)
                        {
                            tbl_CommonInventoryRecord Recordnew = new tbl_CommonInventoryRecord();
                            Recordnew.BookingId = Record[i].BookingId;
                            Recordnew.InvSid = Record[i].InvSid;
                            Recordnew.SupplierId = Record[i].SupplierId;
                            Recordnew.HotelCode = Record[i].HotelCode;
                            Recordnew.RoomType = Record[i].RoomType;
                            Recordnew.RateType = Record[i].RateType;
                            Recordnew.InvType = Record[i].InvType;
                            Recordnew.Date = Record[i].Date;
                            Recordnew.Month = Record[i].Month;
                            Recordnew.Year = Record[i].Year;
                            Recordnew.TotalAvlRoom = Record[i].TotalAvlRoom;
                            Recordnew.OldAvlRoom = Record[i].OldAvlRoom;
                            Recordnew.NoOfBookedRoom = Record[i].NoOfBookedRoom;
                            Recordnew.NoOfCancleRoom = Record[i].NoOfCancleRoom;
                            Recordnew.UpdateDate = Record[i].UpdateDate.ToString();
                            Recordnew.UpdateOn = Record[i].UpdateOn;
                            AddRecord.Add(Recordnew);
                        }
                        DB.tbl_CommonInventoryRecord.AddRange(AddRecord);
                        DB.SaveChanges();
                    }

                    //    if (objGlobalDefault.UserType == "Supplier")
                    //   {
                    tbl_CommonHotelReservation Update = DB.tbl_CommonHotelReservation.Single(x => x.ReservationID == ReservationID);
                    Update.Status = "Cancelled";
                    //    }

                    //else
                    //{
                    //    tbl_CommonHotelReservation Update = DB.tbl_CommonHotelReservations.Single(x => x.ReservationID == ReservationID && x.Uid == Uid);
                    //    Update.Status = "Cancelled";
                    //}
                    DB.SaveChanges();

                    var CancleDetail = (from obj in DB.tbl_CommonBookedRoom where obj.ReservationID == ReservationID select obj).ToList();
                    var BookingDetail = (from obj in DB.tbl_CommonHotelReservation where obj.ReservationID == ReservationID select obj).ToList();
                    var Cancle = CancleDetail[0].CutCancellationDate.Split('|');
                    var Cancleamnt = CancleDetail[0].CancellationAmount.Split('|');
                    List<DateTime> CDate = new List<DateTime>();
                    DateTime CancleDate = DateTime.Now;

                    Int64 Parent = AccountManager.GetSupplierByUser();

                    string Res = "HQ-" + ReservationID;
                    var InvoiceDetails = (from obj in db.tbl_Invoices where obj.InvoiceNo == BookingDetail[0].InvoiceID && obj.AgentID == Uid && obj.ParentID == Parent select obj).ToList();
                    // var Commission = InvoiceDetails[0].Commission;
                    var Commission = "0";

                    decimal RefundAmnt = 0;
                    decimal CanclAmnt = 0;
                    decimal Total = 0;
                    //for (int i = 0; i < CancleDetail.Count; i++)
                    //{
                    //    CDate.Add(DateTime.ParseExact(CancleDetail[i].CutCancellationDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i));
                    //}

                    for (int i = 0; i < Cancle.Length - 1; i++)
                    {

                        if (CancleDate >= DateTime.ParseExact(Cancle[i], "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture))
                        {
                            CanclAmnt = Convert.ToDecimal(Cancleamnt[i]);
                            Total = Convert.ToDecimal(CancleDetail[0].RoomAmount);
                            RefundAmnt = Total - CanclAmnt;
                            break;
                        }
                    }

                    //Email Sending


                    Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"]);
                    //CUT.DataLayer.GlobalDefault objGlobalDefault = newCUT.DataLayer.GlobalDefault();
                    if (HttpContext.Current.Session["LoginUser"] != null)
                    {
                        objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                        // ParentID = objGlobalDefault.sid;
                    }

                    string BookingStatus = "Vouchered";
                    DBHelper.DBReturnCode retCode = HotelManager.CancelBooking(ReservationID, CanclAmnt.ToString(), BookingStatus, "", BookingDetail[0].TotalFare.ToString(), Commission.ToString(), "");
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        var sReservation = (from obj in DB.tbl_CommonHotelReservation
                                            join objAgent in db.tbl_AdminLogins on obj.Uid equals objAgent.sid
                                            where obj.ReservationID == ReservationID
                                            select new
                                            {
                                                customer_Email = objAgent.uid,
                                                customer_name = obj.AgencyName,
                                                VoucherNo = obj.VoucherID,
                                                Uid = obj.Uid,
                                                Email = objAgent.uid,
                                                ContactPerson = objAgent.ContactPerson,
                                                CurrencyCode = objAgent.CurrencyCode,
                                                HotelName = obj.HotelName,
                                                City = obj.City,
                                                checkin = obj.CheckIn,
                                                checkout = obj.CheckOut,
                                                Ammount = obj.TotalFare,
                                                Nights = obj.NoOfDays,
                                                Passenger = obj.bookingname,
                                                InvoiceID = obj.InvoiceID,
                                                VoucherID = obj.VoucherID,
                                                bookingdate = obj.ReservationDate,
                                                Status = obj.Status,
                                            }).FirstOrDefault();

                        //  var sMail = (from obj in DB.tbl_ActivityMails where obj.ParentID == objGlobalDefault.ParentId && obj.Activity == "Booking Cancelled" select obj).FirstOrDefault();



                        var sMail = new CutAdmin.dbml.tbl_ActivityMail();
                        ParentID = CutAdmin.DataLayer.AccountManager.GetSupplierByUser();
                        sMail = (from obj in db.tbl_ActivityMails where obj.ParentID == ParentID && obj.Activity == "Booking Cancelled" select obj).FirstOrDefault();
                        string BCcTeamMails = "";
                        string CcTeamMail = "";

                        if (sMail != null)
                        {
                            BCcTeamMails = sMail.Email;
                            CcTeamMail = sMail.CcMail;
                        }
                        Dictionary<string, string> Email1List = new Dictionary<string, string>();
                        Email1List.Add(BCcTeamMails, BCcTeamMails);
                        Email1List.Add(sReservation.customer_Email, sReservation.customer_Email);



                        string Logo = HttpContext.Current.Session["logo"].ToString();
                        if (objGlobalDefault.UserType == "SupplierStaff" || objGlobalDefault.UserType == "Agent")
                        {
                            Logo = (from obj in db.tbl_AdminLogins where obj.sid == objGlobalDefault.ParentId select obj.Agentuniquecode).FirstOrDefault();
                        }
                        string Url = Convert.ToString(ConfigurationManager.AppSettings["URL"]);
                        StringBuilder sb = new StringBuilder();

                        sb.Append("<!DOCTYPE html>");
                        sb.Append("<html lang=\"en\" id=\"InvoicePrint\" xmlns=\"http://www.w3.org/1999/xhtml\">");
                        sb.Append("<head>    ");
                        sb.Append("<meta charset=\"utf-8\" />");
                        sb.Append("</head>");
                        sb.Append("<body class=\"print-portrait-a4\" style=\"font-family: Segoe UI, Tahoma, sans-serif; margin:0px 40px 0px 40px;  width:auto;\">    ");
                        sb.Append("<div style=\"height:50px;width:auto\">    ");
                        sb.Append("<br />");
                        sb.Append("<img src=" + Url + "AgencyLogo/" + Logo + "\" alt=\"\" style=\"padding-left:10px;\" />    ");
                        sb.Append("</div>");
                        sb.Append("<br>");
                        sb.Append("<div style=\"margin-left:10%\">");
                        sb.Append("<p> <span style=\\\"margin-left:2%;font-weight:400\\\">Dear " + sReservation.ContactPerson + ",</span><br /></p>");
                        sb.Append("<p> <span style=\\\"margin-left:2%;font-weight:400\\\">We have received your request, your Booking Details.</span><br /></p>       ");
                        sb.Append("<style type=\"text/css\">");
                        sb.Append(".tg  {border-collapse:collapse;border-spacing:0;}");
                        sb.Append(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}    ");
                        sb.Append(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}    ");
                        sb.Append(".tg .tg-9hbo{font-weight:bold;vertical-align:top}");
                        sb.Append(".tg .tg-yw4l{vertical-align:top}");
                        sb.Append("@media screen and (max-width: 767px) {.tg {width: auto !important;}.tg col {width: auto !important;}.tg-wrap {overflow-x: auto;-webkit-overflow-scrolling: touch;}}</style>    ");
                        sb.Append("<div class=\"tg-wrap\"><table class=\"tg\">");
                        sb.Append("<tr>");
                        sb.Append("<td class=\"tg-9hbo\"><b>Transaction ID</b></td>");
                        sb.Append("<td class=\"tg-yw4l\">:  " + ReservationID + "</td>  ");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td class=\"tg-9hbo\"><b>Reservation Status</b></td>  ");
                        sb.Append("<td class=\"tg-yw4l\">:" + sReservation.Status + "</td>  ");
                        sb.Append("</tr>  ");
                        sb.Append("<tr>  ");
                        sb.Append("<td class=\"tg-9hbo\"><b>Reservation Date</b></td>  ");
                        sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.bookingdate + "</td>");
                        sb.Append("</tr>  ");
                        sb.Append("<tr>  ");
                        sb.Append("<td class=\"tg-9hbo\"><b>Hotel Name</b></td>  ");
                        sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.HotelName + "</td>");
                        sb.Append("</tr>  ");
                        sb.Append("<tr>  ");
                        sb.Append("<td class=\"tg-9hbo\"><b>Location</b></td>  ");
                        sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.City + "</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>  ");
                        sb.Append("<td class=\"tg-9hbo\"><b>Check-In</b></td>  ");
                        sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.checkin + "</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>  ");
                        sb.Append("<td class=\"tg-9hbo\"><b>Check-Out </b></td>");
                        sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.checkout + "</td>");
                        sb.Append("</tr>  ");
                        sb.Append("<tr>  ");
                        sb.Append("<td class=\"tg-9hbo\"><b>Rate</b></td>");
                        sb.Append("<td class=\"tg-yw4l\">:" + sReservation.CurrencyCode + " " + Convert.ToInt32(sReservation.Ammount) / Convert.ToInt32(sReservation.Nights) + "</td>  ");
                        sb.Append("</tr>  ");
                        sb.Append("<tr>  ");
                        sb.Append("<td class=\"tg-9hbo\"><b>Total</b></td>");
                        sb.Append("<td class=\"tg-yw4l\">:  " + sReservation.CurrencyCode + "" + sReservation.Ammount + "</td>  ");
                        sb.Append("</tr>");
                        sb.Append("</table></div>                            ");
                        sb.Append("<p><span style=\\\"margin-left:2%;font-weight:400\\\">For any further clarification & query kindly feel free to contact us</span><br /></p>    ");
                        sb.Append("<span style=\\\"margin-left:2%\\\">    ");
                        sb.Append("<b>Thank You,</b><br />    ");
                        sb.Append("</span>    ");
                        sb.Append("<span style=\\\"margin-left:2%\\\">    ");
                        sb.Append("Administrator    ");
                        sb.Append("</span>                 ");
                        sb.Append("        </div>       ");
                        sb.Append("     <div>    ");
                        sb.Append("        <table border=\"0\" style=\" height:30px; width: 100%; border-spacing: 0px; border-top-color: white; border-left: none; border-right: none\">    ");
                        sb.Append("            <tr style=\"border: none; border-bottom-color: gray; color: #57585A;\">    ");
                        sb.Append("                <td colspan=\"6\"><div align=\"center\" style=\"font-weight:600\">  Clickurhotel.com</div></td>    ");
                        sb.Append("            </tr>    ");
                        sb.Append("        </table>    ");
                        sb.Append("     </div>    ");
                        sb.Append("     </body>    ");
                        sb.Append("     </html>");

                        string Title = "Cancel Confirmation";
                        MailManager.SendMail(sReservation.customer_Email, Title, sb.ToString(), "", CcTeamMail);


                    }


                    string Message = EmailManager.GetErrorMessage("Booking Cancelled");
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, Message = Message });


                }
            }
            catch
            {
                string Message = EmailManager.GetErrorMessage("Booking Cancelled Error");
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, Message = Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetDetail(string ReservationID)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    var Detail = (from obj in DB.tbl_CommonHotelReservation
                                      //  join objPass in DB.tbl_CommonBookedPassengers on obj.ReservationID equals objPass.ReservationID
                                  join objroom in DB.tbl_CommonBookedRoom on obj.ReservationID equals objroom.ReservationID
                                  where obj.ReservationID == ReservationID
                                  select new
                                  {
                                      obj.Sid,
                                      obj.ReservationID,
                                      obj.ReservationDate,
                                      obj.Status,
                                      obj.TotalFare,
                                      obj.TotalRooms,
                                      obj.HotelName,
                                      obj.CheckIn,
                                      obj.CheckOut,
                                      obj.City,
                                      obj.Children,
                                      obj.BookingStatus,
                                      obj.NoOfAdults,
                                      obj.Source,
                                      obj.Uid,
                                      obj.NoOfDays,
                                      obj.bookingname,
                                      //  objPass.Name,
                                      //  objPass.LastName,
                                      //  objPass.RoomNumber,
                                      objroom.CancellationAmount,
                                      objroom.CutCancellationDate
                                  }).ToList();

                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, Detail = Detail });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetBookingDetail(string ReservationID)
        {
            try
            {
                CUT.HotelHandler obj = new CUT.HotelHandler();
                return jsSerializer.Serialize(obj.GetCancellationDetails(ReservationID));
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }


        [WebMethod(EnableSession = true)]
        public string HotelCancelBooking(string ReservationID, string ReferenceCode, string CancellationAmount, string BookingStatus, string Remark, string TotalFare, string ServiceCharge, string Total, string Type)
        {
            try
            {
                CUT.HotelHandler obj = new CUT.HotelHandler();
                string json = "";
                if (Type == "TBO")
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    js.MaxJsonLength = Int32.MaxValue;
                    TboHotelLib.Request.CancelHold.CancelRoom objCancelRoom = new TboHotelLib.Request.CancelHold.CancelRoom();

                    object GetLoginDetails = GetTokenID();
                    string TokenId = GetLoginDetails.GetType().GetProperty("TockenID").GetValue(GetLoginDetails, null).ToString();

                    List<tbl_ApiCredential> arrCredential = GetCredential("TBO");
                    string EndUserIp = arrCredential[0].IP;
                    objCancelRoom = new TboHotelLib.Request.CancelHold.CancelRoom
                    {
                        EndUserIp = EndUserIp,
                        TokenId = TokenId,
                        BookingId = Convert.ToInt32(ReservationID),
                        RequestType = 4,
                        Remarks = "",
                    };
                    string Request = JsonConvert.SerializeObject(objCancelRoom);
                    string url = "https://booking.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/SendChangeRequest/";

                    string Reponse = "";
                    string RequestHeader = "";
                    string ResponseHeader = ""; int Status = 0;

                    isReponse = TboHotelLib.Common.webClient.GetHotelRoomResponse(Request, url, out Reponse);
                    CUT.Agent.DataLayer.LogManager.AddAPILog(3, RequestHeader, Request, ResponseHeader, Reponse, arrCredential[0].ParentId, Status, "TBO");

                    var arrResponse = JsonConvert.DeserializeObject<dynamic>(Reponse);
                    TboHotelLib.Response.HotelChangeRequestResult objBookingResponse = new TboHotelLib.Response.HotelChangeRequestResult();
                    string sStatus = arrResponse.HotelChangeRequestResult.ResponseStatus.Value.ToString();
                    string ChangeRequestStatus = arrResponse.HotelChangeRequestResult.ChangeRequestStatus.Value.ToString();
                    string CancellationCharge = "";

                    if (isReponse == true && sStatus == "1" && ChangeRequestStatus != "4")
                    {
                        BookingStatus = "Vouchered";
                        if (arrResponse.HotelChangeRequestResult.CancellationCharge != null)
                        {
                            CancellationCharge = arrResponse.HotelChangeRequestResult.CancellationCharge.Value.ToString();

                            float ExchangeRate = ExchangeRateManager.GetExchange();

                         // float ExchangeRate = CUT.DataLayer.ExchangeRateManger.GetExchange();
                            CancellationCharge = (Convert.ToSingle(CancellationCharge) * ExchangeRate).ToString();

                            ServiceCharge = (Convert.ToDecimal(ServiceCharge) + Convert.ToDecimal(CancellationCharge)).ToString();
                            Remark = "Cancellation charge added with in Service Charge.";
                        }
                        CUT.BL.DBHelper.DBReturnCode retCode = CUT.DataLayer.HotelReservationManager.CancelBooking(ReservationID, CancellationAmount, BookingStatus, Remark, TotalFare, ServiceCharge, Total);
                        string Message = CUT.DataLayer.EmailManager.GetErrorMessage("Booking Cancelled");
                        // CUT.DataLayer.EmailManager.BookingCancellation(ReservationID, BookingStatus);
                        return js.Serialize(new { session = 1, retCode = 1, Message = Message });
                    }
                    else
                    {
                        //return js.Serialize(new { session = 1, retCode = 0, Message = "Error while cancelling booking" });
                        string Message = CUT.DataLayer.EmailManager.GetErrorMessage("Booking Cancelled Error");
                        return js.Serialize(new { session = 0, retCode = 0, Message = Message });
                    }
                }
                else
                {
                    json = obj.HotelCancelBooking(ReservationID, ReferenceCode, CancellationAmount, BookingStatus, Remark, TotalFare, ServiceCharge, Total, Type);
                    return json;
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, ex = ex.Message });
            }
        }

        static bool isReponse { get; set; }
        public static List<tbl_ApiCredential> GetCredential(string Type)
        {
            using (var db = new Trivo_AmsHelper())
            {
                List<tbl_ApiCredential> arrCredential = new List<tbl_ApiCredential>();
                Int64 ParentID = AccountManager.GetAdminByLogin();
                IQueryable<tbl_ApiCredential> Cred = from obj in db.tbl_ApiCredential where obj.ParentId == ParentID && obj.sSupplierCode == Type select obj;

                arrCredential.Add(new tbl_ApiCredential
                {
                    IP = Cred.FirstOrDefault().IP,
                    URL = Cred.FirstOrDefault().URL,
                    sUserName = Cred.FirstOrDefault().sUserName,
                    sPassword = Cred.FirstOrDefault().sPassword,
                    sAccessKey = Cred.FirstOrDefault().sAccessKey,
                    sSupplierCode = Cred.FirstOrDefault().sSupplierCode,
                    ParentId = Cred.FirstOrDefault().ParentId,
                });
                return arrCredential;
            }
        }

        public static object GetTokenID()
        {
             isReponse = false;
            JavaScriptSerializer objSerialize = new JavaScriptSerializer();
            JavaScriptSerializer objderialize = new JavaScriptSerializer();
            string TockenID = ""; string Reponse = "";
            objSerialize = new JavaScriptSerializer();
            object LoginTbo = new object();

            List<tbl_ApiCredential> arrCredential = GetCredential("TBO");

            string Request = objSerialize.Serialize(
               new
               {
                   ClientId = arrCredential[0].sAccessKey,
                   UserName = arrCredential[0].sUserName,
                   Password = arrCredential[0].sPassword,
                   EndUserIp = arrCredential[0].IP,
               });

            try
            {
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    string Date = DateTime.Now.ToString("dd-MM-yyyy");
                    string FirstName = arrCredential[0].sUserName;
                    var arrDepart = (from obj in db.tbl_TBOLoginDetail where obj.Date == Date && obj.FirstName == FirstName select obj).ToList();
                    if (arrDepart.Count == 0)
                    {
                        isReponse = TboFlightLib.Common.webClient.GetReponse(Request, "Authenticate", out Reponse);
                        var arrResponse = JsonConvert.DeserializeObject<dynamic>(Reponse);
                        TockenID = arrResponse.TokenId.Value.ToString();

                        LoginTbo = new
                        {
                            TockenID = TockenID,
                            UserName = arrResponse.Member.LoginName.Value.ToString(),
                            FirstName = arrResponse.Member.LoginName.Value.ToString(),
                            LastName = arrResponse.Member.LoginName.Value.ToString(),
                            LoginDetails = "",
                            MemberId = arrResponse.Member.MemberId.Value.ToString(),
                            Email = arrResponse.Member.Email.Value.ToString(),
                            AgencyId = arrResponse.Member.AgencyId.Value.ToString()
                        };

                        tbl_TBOLoginDetail Details = new tbl_TBOLoginDetail();
                        Details.TockenID = TockenID;
                        Details.UserName = arrResponse.Member.LoginName.Value.ToString();
                        Details.FirstName = arrResponse.Member.LoginName.Value.ToString();
                        Details.LastName = arrResponse.Member.LoginName.Value.ToString();
                        Details.LoginDetails = "";
                        Details.MemberId = arrResponse.Member.MemberId.Value.ToString();
                        Details.Email = arrResponse.Member.Email.Value.ToString();
                        Details.AgencyId = arrResponse.Member.AgencyId.Value.ToString();
                        Details.Date = Date;

                        db.tbl_TBOLoginDetail.Add(Details);
                        db.SaveChanges();
                    }
                    else
                        LoginTbo = new
                        {
                            TockenID = arrDepart[arrDepart.Count - 1].TockenID,
                            UserName = arrDepart[arrDepart.Count - 1].UserName,
                            FirstName = arrDepart[arrDepart.Count - 1].FirstName,
                            LastName = arrDepart[arrDepart.Count - 1].LastName,
                            LoginDetails = arrDepart[arrDepart.Count - 1].LoginDetails,
                            MemberId = arrDepart[arrDepart.Count - 1].MemberId,
                            Email = arrDepart[arrDepart.Count - 1].Email,
                            AgencyId = arrDepart[arrDepart.Count - 1].AgencyId
                        };
                }
            }
            catch (Exception ex)
            {
            }

            return LoginTbo;
        }

        [WebMethod(EnableSession = true)]
        public string GetAmendmentDetail(string ReservationID)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    var Detail = (from obj in DB.tbl_CommonBookedPassenger where obj.ReservationID == ReservationID select obj).ToList();

                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, Detail = Detail });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string SaveConfirmDetail(string HotelName, string ReservationId, string ConfirmDate, string StaffName, string ReconfirmThrough, string HotelConfirmationNo, string Comment)
        {
            try
            {

                CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 UserId = objGlobalDefault.sid;
                CutAdmin.dbml.tbl_HotelReconfirmationDetail Confirm = new CutAdmin.dbml.tbl_HotelReconfirmationDetail();
                Confirm.UserID = UserId;
                Confirm.ReservationId = ReservationId;
                Confirm.ReconfirmDate = ConfirmDate;
                Confirm.Staff_Name = StaffName;
                Confirm.ReconfirmThrough = ReconfirmThrough;
                Confirm.HotelConfirmationNo = HotelConfirmationNo;
                Confirm.Comment = Comment;
                db.tbl_HotelReconfirmationDetails.InsertOnSubmit(Confirm);
                db.SubmitChanges();
                CutAdmin.dbml.tbl_HotelReservation Bit = db.tbl_HotelReservations.Single(x => x.ReservationID == ReservationId);
                Bit.IsConfirm = true;
                Bit.Status = "Vouchered";
                Bit.BookingStatus = "Confirmrd";
                Bit.HotelConfirmationNo = HotelConfirmationNo;
                db.SubmitChanges();
                ReconfirmationMail(Convert.ToInt64(Bit.Uid), Confirm.sid, HotelName, ReservationId, StaffName, Comment);
                Int64 Uid = 0; string sTo = "";
                Uid = AccountManager.GetSupplierByUser();
                sTo = objGlobalDefault.uid;
                sTo += "," + (from obj in db.tbl_AdminLogins where obj.sid == Bit.Uid select obj).FirstOrDefault().uid;
                MailManager.SendReconfrmInvoice(ReservationId, Uid, sTo);

                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string AcceptGroupReq(Int64 Id, string Status, string Remark)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    string AgencyName = objGlobalDefault.AgencyName;
                    tbl_CommonHotelReservation Accept = DB.tbl_CommonHotelReservation.Single(x => x.Sid == Id);
                    Accept.Status = "Vouchered";
                    Accept.UpdatedBy = AgencyName;
                    Accept.Remark = Remark;
                    DB.SaveChanges();
                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string RejectGroupReq(Int64 Id, string Status, string Remark)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    string AgencyName = objGlobalDefault.AgencyName;
                    tbl_CommonHotelReservation Accept = DB.tbl_CommonHotelReservation.Single(x => x.Sid == Id);
                    Accept.Status = "Cancelled";
                    Accept.UpdatedBy = AgencyName;
                    Accept.Remark = Remark;
                    DB.SaveChanges();
                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }

        public static bool ReconfirmationMail(Int64 Uid, Int64 Sid, string HotelName, string ReservationId, string ReconfirmBy, string Comment)
        {

            try
            {
                using (var db = new CutAdmin.dbml.helperDataContext())
                {
                    var arrDetail = (from objRes in db.tbl_AdminLogins where objRes.sid == Uid select objRes).FirstOrDefault();
                    string CompanyName = (from objRes in db.tbl_AdminLogins where objRes.sid == arrDetail.ParentID select objRes).FirstOrDefault().AgencyName;
                    var sReservation = (from obj in db.tbl_HotelReservations
                                        join
    objAgent in db.tbl_AdminLogins on obj.Uid equals objAgent.sid
                                        where obj.ReservationID == ReservationId
                                        select new
                                        {
                                            //CompanyName = (from objRes in DB.tbl_AdminLogins where objRes.ParentID == obj.Uid select objRes).FirstOrDefault().uid,
                                            CompanyName = CompanyName,
                                            customer_Email = objAgent.uid,
                                            customer_name = objAgent.AgencyName,
                                            VoucherNo = obj.VoucherID,
                                            PareniId = objAgent.ParentID

                                        }).FirstOrDefault();

                    var sMail = (from obj in db.tbl_ActivityMails where obj.Activity == "Booking Reconfirm" && obj.ParentID == sReservation.PareniId select obj).FirstOrDefault();
                    string BCcTeamMails = sMail.BCcMail;
                    string CcTeamMail = sMail.CcMail;

                    StringBuilder sb = new StringBuilder();

                    sb.Append("<html>");
                    sb.Append("<head>");
                    sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
                    sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
                    sb.Append("<style>");
                    sb.Append("<!--");
                    sb.Append(" /* Font Definitions */");
                    sb.Append(" @font-face");
                    sb.Append("	{font-family:\"Cambria Math\";");
                    sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
                    sb.Append("@font-face");
                    sb.Append("	{font-family:Calibri;");
                    sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
                    sb.Append(" /* Style Definitions */");
                    sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
                    sb.Append("	{margin-top:0cm;");
                    sb.Append("	margin-right:0cm;");
                    sb.Append("	margin-bottom:10.0pt;");
                    sb.Append("	margin-left:0cm;");
                    sb.Append("	line-height:115%;");
                    sb.Append("	font-size:11.0pt;");
                    sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
                    sb.Append("a:link, span.MsoHyperlink");
                    sb.Append("	{color:blue;");
                    sb.Append("	text-decoration:underline;}");
                    sb.Append("a:visited, span.MsoHyperlinkFollowed");
                    sb.Append("	{color:purple;");
                    sb.Append("	text-decoration:underline;}");
                    sb.Append(".MsoPapDefault");
                    sb.Append("	{margin-bottom:10.0pt;");
                    sb.Append("	line-height:115%;}");
                    sb.Append("@page Section1");
                    sb.Append("	{size:595.3pt 841.9pt;");
                    sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
                    sb.Append("div.Section1");
                    sb.Append("	{page:Section1;}");
                    sb.Append("-->");
                    sb.Append("</style>");
                    sb.Append("</head>");
                    sb.Append("<body lang=EN-IN link=blue vlink=purple>");
                    sb.Append("<div class=Section1>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Dear ");
                    sb.Append("" + sReservation.customer_name + ",</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Greetings ");
                    //sb.Append("from&nbsp;<b>Click<span style='color:#ED7D31'>Ur</span><span style='color:#4472C4'>Trip</span></b></span></p>");
                    sb.Append("from&nbsp;<b>" + sReservation.CompanyName + "</b></span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Once ");
                    sb.Append("again thanks for choosing our service, we believe that smooth check-in will");
                    sb.Append("defiantly satisfy your guest, so we take this efforts to reconfirm your booking</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0");
                    sb.Append(" style='border-collapse:collapse'>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Transaction");
                    sb.Append("  No.</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + sReservation.customer_name + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hotel");
                    sb.Append("  Name</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'> " + HotelName + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reservation");
                    sb.Append("  ID</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReservationId + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reconfirmed");
                    sb.Append("  by</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReconfirmBy + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append(" <tr>");
                    sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Remark");
                    sb.Append("  / Note</span></p>");
                    sb.Append("  </td>");
                    sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                    sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + Comment + "</span></p>");
                    sb.Append("  </td>");
                    sb.Append(" </tr>");
                    sb.Append("</table>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span lang=EN-US style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hope ");
                    sb.Append("the above is correct &amp;&nbsp;</span><span style='font-size:12.0pt;");
                    sb.Append("font-family:\"Times New Roman\",\"serif\"'>your guest will enjoy the stay</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>In ");
                    sb.Append("case of any discrepancy kindly contact us immediately at&nbsp;<a");
                    sb.Append("href=\"mailto:" + AccountManager.GetUserMailByAdmin() + "\" target=\"_blank\"><span style='color:#1155CC'>" + AccountManager.GetUserMailByAdmin() + "</span></a></span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>&nbsp;</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Best Regards,</span></p>");
                    sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                    sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                    sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Online Reservation Team</span></p>");
                    sb.Append("<p class=MsoNormal>&nbsp;</p>");
                    sb.Append("</div>");
                    sb.Append("</body>");
                    sb.Append("</html>");

                    string Title = "Hotel reconfirmation - " + HotelName + " - " + sReservation.VoucherNo + "";
                    List<string> from = new List<string>();
                    from.Add(Convert.ToString(AccountManager.GetUserMailByAdmin()));
                    List<string> attachmentList = new List<string>();

                    Dictionary<string, string> Email1List = new Dictionary<string, string>();
                    Dictionary<string, string> Email2List = new Dictionary<string, string>();
                    string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                    Email1List.Add(sReservation.customer_Email, sReservation.customer_Email);
                    if (BCcTeamMails != "")
                        Email1List.Add(BCcTeamMails, BCcTeamMails);
                    if (CcTeamMail != "")
                        Email2List.Add(CcTeamMail, CcTeamMail);
                    MailManager.SendMail(accessKey, Email1List, Email2List, Title, sb.ToString(), from, attachmentList);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool OnRequestMail(Int64 Uid, string HotelName, string ReservationId)
        {

            try
            {
                //DBHandlerDataContext DB = new DBHandlerDataContext();
                using (var DB = new CutAdmin.dbml.helperDataContext())
                {
                    using (var db = new Click_Hotel())
                    {
                        var arrDetail = (from objRes in DB.tbl_AdminLogins where objRes.sid == Uid select objRes).FirstOrDefault();

                        string CompanyName = (from objRes in DB.tbl_AdminLogins where objRes.sid == arrDetail.ParentID select objRes).FirstOrDefault().AgencyName;
                        var sReservation = (from obj in db.tbl_CommonHotelReservation
                                            from objAgent in DB.tbl_AdminLogins
                                            where obj.ReservationID == ReservationId
                                            select new
                                            {
                                                //CompanyName = (from objRes in DB.tbl_AdminLogins where objRes.ParentID == obj.Uid select objRes).FirstOrDefault().uid,
                                                CompanyName = CompanyName,
                                                customer_Email = objAgent.uid,
                                                customer_name = objAgent.AgencyName,
                                                VoucherNo = obj.VoucherID,
                                                PareniId = objAgent.ParentID

                                            }).FirstOrDefault();

                        var arrSupplierDetails = (from objAgent in DB.tbl_AdminLogins
                                                  join objConct in DB.tbl_Contacts on objAgent.ContactID equals objConct.ContactID
                                                  where objAgent.sid == AccountManager.GetSupplierByUser()
                                                  select new
                                                  {
                                                      CompanyName = objAgent.AgencyName,
                                                      Uid = objAgent.uid,
                                                      website = objConct.Website
                                                  }).FirstOrDefault();

                        var sMail = (from obj in DB.tbl_ActivityMails where obj.Activity == "Booking OnRequest" && obj.ParentID == sReservation.PareniId select obj).FirstOrDefault();
                        string BCcTeamMails = sMail.BCcMail;
                        string CcTeamMail = sMail.CcMail;



                        StringBuilder sb = new StringBuilder();

                        sb.Append("<html>");
                        sb.Append("<head>");
                        sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">");
                        sb.Append("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">");
                        sb.Append("<style>");
                        sb.Append("<!--");
                        sb.Append(" /* Font Definitions */");
                        sb.Append(" @font-face");
                        sb.Append("	{font-family:\"Cambria Math\";");
                        sb.Append("	panose-1:2 4 5 3 5 4 6 3 2 4;}");
                        sb.Append("@font-face");
                        sb.Append("	{font-family:Calibri;");
                        sb.Append("	panose-1:2 15 5 2 2 2 4 3 2 4;}");
                        sb.Append(" /* Style Definitions */");
                        sb.Append(" p.MsoNormal, li.MsoNormal, div.MsoNormal");
                        sb.Append("	{margin-top:0cm;");
                        sb.Append("	margin-right:0cm;");
                        sb.Append("	margin-bottom:10.0pt;");
                        sb.Append("	margin-left:0cm;");
                        sb.Append("	line-height:115%;");
                        sb.Append("	font-size:11.0pt;");
                        sb.Append("	font-family:\"Calibri\",\"sans-serif\";}");
                        sb.Append("a:link, span.MsoHyperlink");
                        sb.Append("	{color:blue;");
                        sb.Append("	text-decoration:underline;}");
                        sb.Append("a:visited, span.MsoHyperlinkFollowed");
                        sb.Append("	{color:purple;");
                        sb.Append("	text-decoration:underline;}");
                        sb.Append(".MsoPapDefault");
                        sb.Append("	{margin-bottom:10.0pt;");
                        sb.Append("	line-height:115%;}");
                        sb.Append("@page Section1");
                        sb.Append("	{size:595.3pt 841.9pt;");
                        sb.Append("	margin:72.0pt 72.0pt 72.0pt 72.0pt;}");
                        sb.Append("div.Section1");
                        sb.Append("	{page:Section1;}");
                        sb.Append("-->");
                        sb.Append("</style>");
                        sb.Append("</head>");
                        sb.Append("<body lang=EN-IN link=blue vlink=purple>");
                        sb.Append("<div class=Section1>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Dear ");
                        sb.Append("" + arrSupplierDetails.CompanyName + ",</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Greetings ");
                        //sb.Append("from&nbsp;<b>Click<span style='color:#ED7D31'>Ur</span><span style='color:#4472C4'>Trip</span></b></span></p>");
                        sb.Append("from&nbsp;<b>" + sReservation.customer_name + "</b></span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>");
                        sb.Append("thanks for choosing our service,booking is subject to room availiblity and The confirmation of whether the room is available or not</span></p>");
                        sb.Append("will be communicated to you within within 24 business hours");
                        // sb.Append("we will confirm your booking when we have availability</span></p>");
                        // sb.Append("defiantly satisfy your guest, so we take this efforts to reconfirm your booking</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0");
                        sb.Append(" style='border-collapse:collapse'>");
                        //sb.Append(" <tr>");
                        //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Transaction");
                        //sb.Append("  No.</span></p>");
                        //sb.Append("  </td>");
                        //sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                        //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + sReservation.customer_name + "</span></p>");
                        //sb.Append("  </td>");
                        //sb.Append(" </tr>");
                        //sb.Append(" <tr>");
                        //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hotel");
                        //sb.Append("  Name</span></p>");
                        //sb.Append("  </td>");
                        //sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                        //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'> " + HotelName + "</span></p>");
                        //sb.Append("  </td>");
                        //sb.Append(" </tr>");
                        //sb.Append(" <tr>");
                        //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Reservation");
                        //sb.Append("  ID</span></p>");
                        //sb.Append("  </td>");
                        //sb.Append("  <td width=265 valign=top style='width:7.0cm;padding:0cm 5.4pt 0cm 5.4pt'>");
                        //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>" + ReservationId + "</span></p>");
                        //sb.Append("  </td>");
                        //sb.Append(" </tr>");
                        //sb.Append(" <tr>");
                        //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Onrequest");
                        //sb.Append("  by</span></p>");
                        //sb.Append("  </td>");
                        //sb.Append(" </tr>");
                        //sb.Append(" <tr>");
                        //sb.Append("  <td width=179 valign=top style='width:134.45pt;padding:0cm 5.4pt 0cm 5.4pt'>");
                        //sb.Append("  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        //sb.Append("  normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Remark");
                        //sb.Append("  / Note</span></p>");
                        //sb.Append("  </td>");
                        //sb.Append(" </tr>");
                        sb.Append("</table>");
                        //sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        //sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        //sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        //sb.Append("normal'><span lang=EN-US style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hope ");
                        //sb.Append("the above is correct &amp;&nbsp;</span><span style='font-size:12.0pt;");
                        //sb.Append("font-family:\"Times New Roman\",\"serif\"'>your guest will enjoy the stay</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span lang=EN-US style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>Hope ");
                        sb.Append("the above is correct &amp;&nbsp;</span><span style='font-size:12.0pt;");
                        sb.Append("font-family:\"Times New Roman\",\"serif\"'>your guest will enjoy the stay</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal'><span style='font-size:12.0pt;font-family:\"Times New Roman\",\"serif\"'>In ");
                        sb.Append("case of any discrepancy kindly contact us immediately at&nbsp;<a");
                        sb.Append("href=\"mailto:hotels@clickurtrip.com\" target=\"_blank\"><span style='color:#1155CC'>hotels@clickurtrip.com</span></a></span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                        sb.Append("\"Arial\",\"sans-serif\";color:#222222'>&nbsp;</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                        sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Best Regards,</span></p>");
                        sb.Append("<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:");
                        sb.Append("normal;background:white'><span lang=EN-US style='font-size:9.5pt;font-family:");
                        sb.Append("\"Arial\",\"sans-serif\";color:#222222'>Online Reservation Team</span></p>");
                        sb.Append("<p class=MsoNormal>&nbsp;</p>");
                        sb.Append("</div>");
                        sb.Append("</body>");
                        sb.Append("</html>");

                        string Title = "Booking On-Request - " + HotelName;

                        List<string> from = new List<string>();
                        from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));
                        List<string> attachmentList = new List<string>();

                        Dictionary<string, string> Email1List = new Dictionary<string, string>();
                        Dictionary<string, string> Email2List = new Dictionary<string, string>();
                        string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                        //Email1List.Add(sReservation.customer_Email, "");
                        Email1List.Add(arrSupplierDetails.Uid, "");
                        Email1List.Add(BCcTeamMails, "");
                        Email2List.Add(CcTeamMail, "");

                        MailManager.SendMail(accessKey, Email1List, Email2List, Title, sb.ToString(), from, attachmentList);
                        return true;
                    }

                }
            }
            catch
            {
                return false;
            }
        }

        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }

        [WebMethod(EnableSession = true)]
        public string SaveAmendemnt(string ReservationID, List<tbl_CommonBookedPassenger> ListPax)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {

                    var oldDetails = (from obj in DB.tbl_CommonBookedPassenger
                                      where obj.ReservationID == ReservationID
                                      select obj).ToList();
                    if (oldDetails.Count != 0)
                    {
                        DB.tbl_CommonBookedPassenger.RemoveRange(oldDetails);
                        DB.SaveChanges();
                    }
                    DB.tbl_CommonBookedPassenger.AddRange(ListPax);
                    DB.SaveChanges();

                    var Name = ListPax[0].Name + " " + ListPax[0].LastName;


                    tbl_CommonHotelReservation tbl = DB.tbl_CommonHotelReservation.Where(d => d.ReservationID == ReservationID).FirstOrDefault();
                    tbl.bookingname = Name;
                    DB.SaveChanges();


                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
        }


    }
}

