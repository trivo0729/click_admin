﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for Convenience
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Convenience : System.Web.Services.WebService
    {
        JavaScriptSerializer objSerialize = new JavaScriptSerializer();
        [WebMethod(EnableSession = true)]
        public string AddFee(decimal AmountPrice,decimal AmountPercent)
        {
            string json = "";
            using (var DB = new helperDataContext())
            {
                try
                {
                    var List = (from obj in DB.tbl_TAXes where obj.ParentID == AccountManager.GetAdminByLogin()
                                && obj.Service == "Payment Gateway Charge" select obj).ToList();
                    if (List.Any())
                    {
                        tbl_TAX Update = DB.tbl_TAXes.Single(x => x.tID == List[0].tID);
                        Update.GstTax = AmountPrice;
                        Update.MarkupGstTax = AmountPercent;
                        DB.SubmitChanges();
                        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                    }
                    else
                    {
                        tbl_TAX Add = new tbl_TAX();
                        Add.Service = "Payment Gateway Charge";
                        Add.VAT = 0;
                        Add.GstTax = AmountPrice;
                        Add.TDS = 0;
                        Add.MarkupGstTax = AmountPercent;
                        Add.VatTax = 0;
                        Add.OnMarkup = false;
                        Add.UpdateDate = DateTime.Now.ToString();
                        Add.UpdateBy = Convert.ToString(AccountManager.GetAdminByLogin());
                        Add.ParentID= AccountManager.GetAdminByLogin();
                        DB.tbl_TAXes.InsertOnSubmit(Add);
                        DB.SubmitChanges();
                        json = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                catch (Exception)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAddFee()
        {
            string json = "";
            try
            {
                using (var DB = new helperDataContext())
                {
                    
                    var List = (from obj in DB.tbl_TAXes where obj.Service == "Payment Gateway Charge" && obj.ParentID == AccountManager.GetAdminByLogin() select obj).ToList();
                    if (List.Any())
                        json = objSerialize.Serialize(new { Session = 1, retCode = 1, Convenience = List });
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\" 1\",\"retCode\":\"0\"}";
            }
            return json;
        }

    }
}
