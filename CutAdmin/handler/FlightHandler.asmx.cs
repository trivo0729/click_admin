﻿using CutAdmin.DataLayer;
using CutAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for FlightHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class FlightHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer objSerlizer = new JavaScriptSerializer();

        [WebMethod(EnableSession = true)]
        public void GetBookingList()
        {
            objSerlizer = new JavaScriptSerializer();
            objSerlizer.MaxJsonLength = Int32.MaxValue;
            try
            {
                //var arrResrvation = FlightManager.GetBookings();
                int TotalCount = 0;
                var arrResrvation = BookingListManager.GetBookings(out TotalCount);
                object finalresult = DTResult.GetDataTable(arrResrvation, TotalCount);
                // return objSerlizer.Serialize(new { retCode = 1, arrAirTicket = arrResrvation });
            }
            catch (Exception ex)
            {
                // return objSerlizer.Serialize(new { retCode = 0 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string SearchBookingList(string BooingStatus, string sArrival, string sDeparture, string[] Airlines, Int64 Agency, string InvoiceDate, string Reference, string Location, string Passenger)
        {
            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            objSerlizer = new JavaScriptSerializer();
            objSerlizer.MaxJsonLength = Int32.MaxValue;
            try
            {
                if (objGlobalDefault.UserType != "Admin" || objGlobalDefault.UserType != "AdminStaff")
                    Agency = 0;
                var arrResrvation = FlightManager.GetSearch(BooingStatus, sArrival, sDeparture, Agency, Airlines, InvoiceDate, Reference, Location, Passenger);
                return objSerlizer.Serialize(new { retCode = 1, arrAirTicket = arrResrvation });
            }
            catch
            {
                return objSerlizer.Serialize(new { retCode = 0 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string CancelBooking(Int64 BookingID, string Type)
        {
            CUT.handler.FlightHandler obj = new CUT.handler.FlightHandler();
            string json = "";
            return json = obj.CancelBooking(BookingID, Type);
        }

        [WebMethod(EnableSession = true)]
        public string GetAirSegment(string BookingID)
        {
            string json = "";
            using (Flightdbml.FlightHelperDataContext db = new Flightdbml.FlightHelperDataContext())
            {
                var Paxces = (from objPax in db.tbl_AirPaxDetails where objPax.BookingID == BookingID select objPax).ToList();
                json = objSerlizer.Serialize(new { retCode = 1, Session = 1, BookingList = Paxces });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetPaxDetails(string BookingID)
        {
            string json = "";
            using (Flightdbml.FlightHelperDataContext db = new Flightdbml.FlightHelperDataContext())
            {
                var Paxces = (from objPax in db.tbl_AirPaxDetails
                              where objPax.BookingID == BookingID
                              select objPax.PaxTitle + " " + objPax.FirstName + " " + objPax.LastName).ToList();
                json = objSerlizer.Serialize(new { retCode = 1, Session = 1, PaxList = Paxces });
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetSegment(string BookingID)
        {
            string json = "";
            using (Flightdbml.FlightHelperDataContext db = new Flightdbml.FlightHelperDataContext())
            {
                var Segmnets = (from objSeg in db.tbl_AirSegments
                              where objSeg.BookingID == BookingID
                              select objSeg).ToList();
                json = objSerlizer.Serialize(new { retCode = 1, Session = 1, Segmnets = Segmnets });
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public void SearchAirBookingList(string TransactionNo, string AgencyName, string Paxces, string TicketStatus)
        {
            try
            {
                int TotalCount = 0;
                var SearchFlight = BookingListManager.SearchFlight(TransactionNo, AgencyName, Paxces, TicketStatus, out TotalCount);

                object finalresult = DTResult.GetDataTable(SearchFlight, TotalCount);

            }
            catch (Exception ex)
            {


            }

        }



    }
}
