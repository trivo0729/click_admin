﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for BookingConditionHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class BookingConditionHandler : System.Web.Services.WebService
    {
        helperDataContext DB = new helperDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";

        public static List<string> ServiceName { get; set; }


        [WebMethod(EnableSession = true)]
        public string GetBookingCondition()
        {
            try
            {
                Int64 ParentID = AccountManager.GetAdminByLogin();
                List<Services> arrService = new List<Services>();
                var arrConditions = BookingConditionHandler.GetService();
                using (var db = new helperDataContext())
                {
                    foreach (var objService in arrConditions)
                    {
                        var arrSupplier = (from obj in db.tbl_APIDetails
                                           join meta in db.tbl_BookingConditions on obj.Supplier equals meta.Supplier
                                           where obj.ParentID == ParentID && obj.Activity == true && meta.Type == (arrConditions.IndexOf(objService) +       5).ToString()
                                           select new Conditions
                                           {
                                               SupplierName = meta.Supplier,
                                               TimeCondition = meta.ConditionalTime,
                                               ConditionID = meta.sid
                                           }).ToList();
                        arrService.Add(new Services { ServiceType = objService.ServiceType, ListSupplier = arrSupplier });
                    }
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = arrService });
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        public static List<Services> GetService()
        {
            ServiceName = new List<string> {"Activity"};
            List<Services> ListService = new List<Services>();
            Int64 ParentID = AccountManager.GetAdminByLogin();
            try
            {
                String UserType = AccountManager.GetUserType();
                using (var db = new helperDataContext())
                {


                    foreach (var ojService in ServiceName)
                    {
                        if (UserType == "Admin" || UserType == "AdminStaff")
                        {
                            var arrSupplier = (from obj in db.tbl_APIDetails
                                               join meta in db.tbl_BookingConditions on obj.Supplier equals meta.Supplier
                                               where obj.ParentID == ParentID && obj.Activity == true && meta.Type == (ServiceName.IndexOf                   (ojService) + 5).ToString()

                                               //from obj in db.tbl_BookingConditions
                                               //where obj.Type == (ServiceName.IndexOf(ojService) + 5).ToString()
                                               select new Conditions
                                               {
                                                   SupplierName = obj.Supplier,
                                               }).ToList();
                            ListService.Add(new Services { ServiceType = ojService, ListSupplier = arrSupplier });
                        }
                        else if (UserType == "Franchisee" || UserType == "FranchiseeStaff")
                        {
                            ListService.Add(new Services { ServiceType = ojService, ListSupplier = new List<Conditions>() { new Conditions { SupplierName = ojService, } } });
                        }

                    }
                }
            }
            catch (Exception ex)
            {
            }
            return ListService;
        }

        public class Services
        {
            public string ServiceType { get; set; }
            public List<Conditions> ListSupplier { get; set; }
        }

        public class Conditions
        {
            public Int64 ConditionID { get; set; }
            public string SupplierName { get; set; }
            public string TimeCondition { get; set; }
        }

        [WebMethod(EnableSession = true)]
        public string SaveBookingCondition(List<Services> arrService)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            try
            {
                using (var db = new helperDataContext())
                {
                    foreach (var objService in arrService)
                    {

                        foreach (var objSupplier in objService.ListSupplier)
                        {
                            var arrCondition = (from obj in db.tbl_BookingConditions
                                             where obj.Type == (arrService.IndexOf(objService) + 5).ToString()
                                                 && obj.Supplier == objSupplier.SupplierName
                                                select obj).FirstOrDefault();
                            arrCondition.ConditionalTime = objSupplier.TimeCondition;
                            db.SubmitChanges();
                        }

                    }
                }
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception)
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }
        }

    }
}
