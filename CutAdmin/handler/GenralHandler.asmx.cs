﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CommonLib.Response;
using System.Reflection;
using System.Configuration;
using CutAdmin.Models;
using Newtonsoft.Json;
using CutAdmin.EntityModal;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for GenralHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class GenralHandler : System.Web.Services.WebService
    {
        helperDataContext DB = new helperDataContext();
        //Click_Hotel db  = new Click_Hotel();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }
        #region SAVE Meals &n Plans
        [WebMethod(EnableSession = true)]
        public string SaveAddOn(Comm_AddOns objAddOn)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new Click_Hotel())
                {
                    if (objAddOn.ID == 0)
                    {
                        if (HttpContext.Current.Session["LoginUser"] == null)
                            throw new Exception("Session Expired ,Please Login and try Again");
                        CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                        Int64 Uid = AccountManager.GetSuperAdminID();
                        //if (objGlobalDefault.UserType != "Supplier")
                        //    Uid = objGlobalDefault.ParentId;
                        if (DB.Comm_AddOns.Where(d => d.AddOnName == objAddOn.AddOnName && d.UserId == Uid).ToList().Count != 0)
                            return jsSerializer.Serialize(new { retCode = 2, ErrorMsg = "Already Exist, You Can not add this." });
                        objAddOn.UserId = Uid;
                        DB.Comm_AddOns.Add(objAddOn);
                        DB.SaveChanges();
                        return jsSerializer.Serialize(new { retCode = 1 });
                    }
                    else
                    {
                        var arrTax = DB.Comm_AddOns.Where(d => d.ID == objAddOn.ID).FirstOrDefault();
                        arrTax.AddOnName = objAddOn.AddOnName;
                        arrTax.Details = objAddOn.Details;
                        arrTax.Type = objAddOn.Type;
                        arrTax.IsMeal = objAddOn.IsMeal;
                        arrTax.Activate = objAddOn.Activate;
                        DB.SaveChanges();
                        return jsSerializer.Serialize(new { retCode = 1 });
                    }
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }

        [WebMethod(EnableSession = true)]
        public string DeleteAddOns(Int64 ID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new Click_Hotel())
                {
                    var arrAddOns = DB.Comm_AddOns.Where(d => d.ID == ID).FirstOrDefault();
                    DB.Comm_AddOns.Remove(arrAddOns);
                    DB.SaveChanges();
                }
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string LoadAddOns()
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (AccountManager.CheckSession)
                {
                    Int64 Uid = AccountManager.GetSuperAdminID();
                    return jsSerializer.Serialize(new { retCode = 1, arrAddOns = GenralManager.GetMeals() });
                }
                else
                    throw new Exception("Session Expired.");
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }
        }
        [WebMethod(EnableSession = true)]
        public string GetMealPlans()
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (AccountManager.CheckSession)
                {
                    return jsSerializer.Serialize(new { retCode = 1, arrMealPlan = GenralManager.GetMealPlans(), arrMeals = GenralManager.GetMeals() });
                }
                else
                    throw new Exception("Session Expired.");
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        [WebMethod(EnableSession = true)]
        public string SaveMealPlans(tbl_MealPlan arrMealPlan, List<tbl_MealPlanMapping> arrMeals)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (AccountManager.CheckSession)
                {
                    GenralManager.SaveMealPlan(arrMealPlan, arrMeals);
                    return jsSerializer.Serialize(new { retCode = 1 });
                }
                else
                    throw new Exception("Session Expired.");
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        [WebMethod(EnableSession = true)]
        public string DeletePlan(Int64 PlanID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (AccountManager.CheckSession)
                {
                    GenralManager.DeletePlan(PlanID);
                    return jsSerializer.Serialize(new { retCode = 1 });
                }
                else
                    throw new Exception("Session Expired.");
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        #endregion

        #region Supplier Staff

        [WebMethod(EnableSession = true)]
        public void GetStaffDetails()
        {
            try
            {
                string Search = HttpContext.Current.Request.Params["sSearch"];
                int TotalCount = 0;
                var Stafflist = UserManager.GetStaff(out TotalCount);
                var OrderList = Stafflist.Select(o => new
                {
                    o.sid,
                    o.ContactPerson,
                    Iswitch = "<input type='checkbox' id='chk_On" + o.sid + "' name='switch-tiny' class='switch tiny' value='On'   onchange=\"Activate('" + o.sid + "','" + o.LoginFlag + "','" + o.ContactPerson + "','" + o.Last_Name + "','" + "" + "')\" value='' / >",
                    o.Last_Name,
                    o.Designation,
                    o.uid,
                    o.StaffUniqueCode,
                    o.LoginFlag, 
                    o.Validity,
                    o.password
                });

                object finalresult = DTResult.GetDataTable(OrderList, TotalCount);
            }
            catch (Exception ex)
            {
            }
        }

        #endregion

        #region ConvertToDatatable
        private static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
            TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
        #endregion


        #region Hotel Info
        [WebMethod(EnableSession = true)]
        public string GetHotelInfo(Int64 HotelCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var db = new Click_Hotel())
                {
                    var arrHotelInfo = (from obj in db.tbl_CommonHotelMaster
                                        where obj.sid == HotelCode
                                        select new
                                        {
                                            obj.HotelName,
                                            obj.HotelLatitude,
                                            obj.HotelLangitude,
                                            obj.HotelAddress,
                                            obj.HotelZipCode,
                                            obj.HotelImage,
                                            obj.HotelCategory,
                                            obj.HotelDescription
                                        }).FirstOrDefault();
                    return jsSerializer.Serialize(new { retCode = 1, arrHotel = arrHotelInfo });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetHotelImages(Int64 HotelCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var db = new Click_Hotel())
                {
                    var arrHotelInfo = (from obj in db.tbl_CommonHotelMaster
                                        where obj.sid == HotelCode
                                        select new
                                        {
                                            obj.HotelName,
                                            obj.HotelImage,
                                            obj.SubImages

                                        }).FirstOrDefault();
                    string Image = arrHotelInfo.HotelImage;
                    List<Image> arrImage = new List<Image>();
                    if (Image != null)
                    {
                        List<string> Url = Image.Split('^').ToList();

                        foreach (string Link in Url)
                        {
                            if (Link != "")
                                arrImage.Add(new Image { Url = "https://clickurhotel.com/HotelImages/" + Link, Count = Url.Count });
                        }
                    }
                    foreach (var sImage in arrHotelInfo.SubImages.Split('^'))
                    {
                        arrImage.Add(new Image { Url = "https://clickurhotel.com/HotelImages/" + sImage, Count = arrHotelInfo.SubImages.Split('^').ToList().Count });
                    }
                    return jsSerializer.Serialize(new { retCode = 1, arrImage = arrImage });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetHotelInfoBySearch(string Search)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                var arrHotelInfo = GenralManager.GetHotelInfo(Search);
                return jsSerializer.Serialize(new { retCode = 1, arrHotel = arrHotelInfo });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, });
            }
        }
        #endregion

        #region ListtoDataTable
        public class ListtoDataTable
        {
            public DataTable ToDataTable<T>(List<T> items)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }
        #endregion

        #region Hotel
        [WebMethod(EnableSession = true)]
        public string GetHotel(string name, string destination)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CutAdmin.DataLayer.HotelManager.Get(name, destination, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("HotelCode"),
                    value = data.Field<String>("Name")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetHotels(string AdminID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrHotels = CutAdmin.Services.Hotels.GetHotels(AdminID) }); 
            }
            catch (Exception ex)
            {

                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetCurrencyByHotel(string HotelCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrCurrency = CutAdmin.Services.HotelCurrency.GetCurrency(HotelCode) });
            }
            catch (Exception ex)
            {

                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        #region Get Rate Type
        [WebMethod(EnableSession = true)]
        public string _ByRateType(string HotelAdminID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrRateTypes = CutAdmin.Services.RateType._ByRateType(HotelAdminID) });
            }
            catch (Exception ex)
            {

                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string _ByRate(string HotelAdminID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrRateTypes = CutAdmin.Services.RateType._ByRate(HotelAdminID) });
            }
            catch (Exception ex)
            {

                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }


        [WebMethod(EnableSession = true)]
        public string _ByRateInventory(string Inventory,string HotelCode,string RoomCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrRateTypes = CutAdmin.Services.RateType._ByRateInventory(Inventory,  HotelCode,  RoomCode) });
            }
            catch (Exception ex)
            {

                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        [WebMethod(EnableSession = true)]
        public string _ByHotelInventory(string Inventory, string HotelCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrRateTypes = CutAdmin.Services.RateType._ByHotelInventory(Inventory, HotelCode) });
            }
            catch (Exception ex)
            {

                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string _ByHotel(string HotelCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, arrRateTypes = CutAdmin.Services.RateType._ByHotel(HotelCode) });
            }
            catch (Exception ex)
            {

                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        #endregion


        [WebMethod(EnableSession = true)]
        public string GetHotelDescription(string HCode)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CutAdmin.DataLayer.HotelManager.GetHotelDescription(HCode, out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetails\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();


            }
            else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }


        [WebMethod(EnableSession = true)]
        public string GetHotelCategory(string code)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CutAdmin.DataLayer.HotelManager.GetCategory(code, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("CategoryCode"),
                    value = data.Field<String>("CategoryName")
                }).ToList();
                list_autocomplete.Insert(0, new Models.AutoComplete { id = "", value = "SELECT TYPE OF HOTEL" });
                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                list_autocomplete.Insert(0, new Models.AutoComplete { id = "", value = "SELECT TYPE OF HOTEL" });
                jsonString = objSerlizer.Serialize(list_autocomplete);
            }
            return jsonString;
        }
        #endregion Hotel


        [WebMethod(EnableSession = true)]
        public bool SendFlightInvoice(string sEmail, string ReservationId)
        {
            bool sJsonString = false;

            sJsonString = SendFlightInvoiceMail(sEmail, ReservationId);
            if (sJsonString == true)
            {
                sJsonString = true;
            }
            return sJsonString;
        }
        [WebMethod(EnableSession = true)]
        public bool SendFlightInvoiceMail(string sEmail, string ReservationId)
        {

            CUT.DataLayer.GlobalDefault objGlobalDefaults = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string Mail = HttpContext.Current.Session["FlightInvoice"].ToString();
            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in sEmail.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = CutAdmin.DataLayer.MailManager.SendMail(accessKey, Email1List, "Your Booking Detail", Mail, from, DocLinksList);
                return reponse;
            }
            catch
            {
                return false;
            }

        }


        [WebMethod(EnableSession = true)]
        public bool SendFlightTicket(string sEmail, string ReservationId)
        {
            bool sJsonString = false;

            sJsonString = SendFlightTicketMail(sEmail, ReservationId);
            if (sJsonString == true)
            {
                sJsonString = true;
            }
            return sJsonString;
        }
        [WebMethod(EnableSession = true)]
        public bool SendFlightTicketMail(string sEmail, string ReservationId)
        {

            CUT.DataLayer.GlobalDefault objGlobalDefaults = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
            string Mail = HttpContext.Current.Session["Ticket"].ToString();
            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                foreach (string mail in sEmail.Split(','))
                {
                    if (mail != "")
                    {
                        Email1List.Add(mail, mail);
                    }
                }
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);

                bool reponse = CutAdmin.DataLayer.MailManager.SendMail(accessKey, Email1List, "Your Booking Detail", Mail, from, DocLinksList);
                return reponse;
            }
            catch
            {
                return false;
            }

        }


        [WebMethod(EnableSession = true)]
        public string GetLanguages()
        {
            try
            {
                using (var db=new CUT_LIVE_UATSTEntities())
                {
                    IQueryable<tbl_Languages> arrLang = (from obj in db.tbl_Languages select obj);
                    return jsSerializer.Serialize(new { Session = 1, retCode = 1, arrLang = arrLang });
                }
            }
            catch (Exception)
            {

                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }

        }
    }
}
