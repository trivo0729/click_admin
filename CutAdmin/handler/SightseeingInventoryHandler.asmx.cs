﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CutAdmin.DataLayer;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using System.Globalization;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for SightseeingInventoryHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class SightseeingInventoryHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer objserialize = new JavaScriptSerializer();
        string json = string.Empty;

        #region Save Inventory

        [WebMethod(EnableSession = true)]
        public string SaveInventory(Int32 ActivityID, Int64 Supplier, Int32[] Slot, Int32[] TicketType, string MaxTicket, string DtTill, string[] DateInvFr, string[] DateInvTo, string OptTicketperDate, string InvLiveOrReq, string InType, string InventoryState, List<string> arrDays)
        {
            try
            {
                SightseeingInventoryManager.SaveInventory(ActivityID, Supplier, Slot, TicketType, MaxTicket, DtTill, DateInvFr, DateInvTo, OptTicketperDate, InvLiveOrReq, InType, InventoryState, arrDays);
                return objserialize.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                return objserialize.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        #endregion#region Save Inventory

        public string Json { get; set; }
        [WebMethod(EnableSession = true)]
        public async Task<string> GetInventoryBySlot(string Startdt, string InventoryType, Int32 ActivityID,List<Int32> arrSlots, Int32 TicketType, Int32 SupplierID)
        {
            Json = string.Empty;
            try
            {
                DateTime dtFrom = DateTime.ParseExact(Startdt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                List<DateTime> calendar = new List<DateTime>();
                calendar.Add(dtFrom);
                //dtFrom = dtFrom.AddDays(1);
                await System.Threading.Tasks.Task.Run(() =>
                {
                    Json = objserialize.Serialize(new { retCode = 1, arrRates = SightseeingInventoryManager.GetSlotInventory(ActivityID, arrSlots, TicketType, InventoryType, SupplierID, calendar) });
                }).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Json = objserialize.Serialize(new { retCode = 0, ex = ex.Message });
            }
            return Json;
        }

        #region Get Market By Rates
        [WebMethod(EnableSession = true)]
        public string GetValidMarket(string FromDate, string ActivityID, string TourType, string TicketType, string InventoryType, string Supplier)
        {
            try
            {
                    json = objserialize.Serialize(new
                    {
                        retCode = 1,
                        arrMarket = SightseeingInventoryManager.GetMarket(FromDate, ActivityID, TourType, TicketType, InventoryType, Supplier)
                    });
            }
            catch (Exception ex)
            {

                json = objserialize.Serialize(new { retCode = 0, ex = ex.Message });
            }
            return json;
        }
        #endregion



    }
}
