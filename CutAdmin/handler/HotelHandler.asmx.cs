﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CutAdmin.EntityModal;
using System.Web.Script.Serialization;
using CutAdmin.DataLayer;
namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for HotelHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class HotelHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        #region Save Hotel
        [WebMethod(true)]
        public string SaveHotel(tbl_CommonHotelMaster arrHotels, tbl_MappedArea arrCity, tbl_Location arrLocation, List<Comm_HotelContacts> arrContacts)
        {
            try
            {
                LocationManager.SaveLocation(arrLocation, arrCity);
                arrHotels.LocationId = arrLocation.Lid;
                HotelManager.SaveHotel(arrHotels, arrContacts);
                return jsSerializer.Serialize(new { retCode = 1, HotelCode = arrHotels.sid });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        #endregion
    }
}
