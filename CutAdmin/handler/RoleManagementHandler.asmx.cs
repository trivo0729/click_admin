﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.BL;
using CutAdmin.DataLayer;


namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for RoleManagementHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class RoleManagementHandler : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public string GetAgentRoleList(string name)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = RoleManager.GetAgentRoleList(name, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("Sid"),
                    value = data.Field<String>("AgencyName")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
    }
}
