﻿using CommonLib.Response;
using CutAdmin.BL;
using CutAdmin.Common;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.IO;
using CutAdmin.EntityModal;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for RoomHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class RoomHandler : System.Web.Services.WebService
    {
        string json = ""; string Texts = "";
        string jsonString = "";
        DataTable dtResult;
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //Click_Hotel DB = new Click_Hotel();
        CutAdmin.dbml.helperDataContext db = new CutAdmin.dbml.helperDataContext();
        DBHelper.DBReturnCode retCode;

        [WebMethod(EnableSession = true)]
        public string GetRooms(Int64 sHotelId)
        {
            string jsonString = "";
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            //Click_Hotel DB = new Click_Hotel();

            if (sHotelId > 0)
            {
                using (var DB = new Click_Hotel())
                {
                    List<Dictionary<string, object>> objHotelList = new List<Dictionary<string, object>>();
                    //objHotelList = JsonStringManager.ConvertDataTable(dtResult);          
                    var RoomList = (from obj in DB.tbl_commonRoomDetails
                                    join meta in DB.tbl_commonRoomType on obj.RoomTypeId equals meta.RoomTypeID
                                    where obj.HotelId == sHotelId

                                    select new
                                    {
                                        meta.RoomType,
                                        meta.RoomTypeID,
                                        obj.RoomSize,
                                        obj.SmokingAllowed,
                                        obj.BeddingType,
                                        obj.MaxExtrabedAllowed,
                                        obj.RoomOccupancy,
                                        obj.RoomId,
                                        obj.RoomAmenitiesId,
                                        obj.RoomDescription,
                                        obj.Approved

                                    }

                                    ).ToList();

                    var RoomTypeList = (from obj in DB.tbl_commonRoomType select obj).ToList();
                    var RoomAmenityList = (from obj in DB.tbl_commonAmunity select obj).ToList();

                    return objserialize.Serialize(new { Session = 1, retCode = 1, RoomList = RoomList, RoomAmenityList = RoomAmenityList, RoomTypeList = RoomTypeList });
                }
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string getRoomwithId(Int64 RoomId, Int64 HotelCode)
        {
            string jsonString = "";
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            //Click_Hotel DB = new Click_Hotel();

            if (RoomId > 0)
            {
                using (var DB = new Click_Hotel())
                {
                    var RoomList = (from Room in DB.tbl_commonRoomDetails where Room.RoomId == RoomId && Room.HotelId == HotelCode select Room).FirstOrDefault();
                    var RoomType = (from RType in DB.tbl_commonRoomType where RType.RoomTypeID == RoomList.RoomTypeId select RType).FirstOrDefault();
                    List<string> AmenityList = new List<string>();
                    if(RoomList.RoomAmenitiesId != null)
                    {
                        foreach (string AmenityId in RoomList.RoomAmenitiesId.Split(','))
                        {
                            if (AmenityId == "")
                                continue;
                            Int64 amenityIds = Convert.ToInt64(AmenityId);
                            var Amenity = (from obj in DB.tbl_commonAmunity where obj.RoomAmunityID == amenityIds select obj.RoomAmunityName).FirstOrDefault();
                            AmenityList.Add(Amenity);
                        }
                    }
                    return objserialize.Serialize(new { Session = 1, retCode = 1, RoomList = RoomList, RoomType = RoomType, AmenityList = AmenityList });
                }
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        Int64 sid = 0;
        Int64 SidNew = 0;
        [WebMethod(EnableSession = true)]
        //public string AddRates(Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, List<string> CheckinR, List<string> CheckoutR, string RateInclude, string RateExclude, string MinStay, string MaxStay, string TaxIncluded, string TaxType, string TaxOn, string TaxValue, string TaxDetails, List<string> RoomTypeID, List<string> RateTypeCode, List<string> RoomRates, List<string> ExtraBeds, List<string> CWB, List<string> CWN, List<string> BookingCode, List<string> CancelPolicy, List<string> Offers, List<string> RateNote, List<string> Daywise, List<string> Sunday, List<string> Monday, List<string> Tuesday, List<string> Wednesday, List<string> Thursday, List<string> Friday, List<string> Saturday, List<string> InventoryType, List<string> NoOfInventoryRoom)
        public string AddRates(Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, List<string> CheckinR, List<string> CheckoutR, string RateInclude, string RateExclude, string MinStay, string MaxStay, Int64 MinRoom, string TaxIncluded, string TaxType, string TaxOn, string TaxValue, string TaxDetails, List<string> RoomTypeID, List<string> RateTypeCode, List<string> RoomRates, List<string> ExtraBeds, List<string> CWB, List<string> CWN, List<string> BookingCode, List<string> CancelPolicy, List<string> Offers, List<string> RateNote, List<string> RateType, List<string> Daywise, List<string> Sunday, List<string> Monday, List<string> Tuesday, List<string> Wednesday, List<string> Thursday, List<string> Friday, List<string> Saturday, List<List<Comm_AddOnsRate>> ListAddOnsRates, List<string> OfferNight, List<Int64> Nights, List<float> MinRate)
        {
            try
            {
                Int64 Uid = 0;
                Uid = AccountManager.GetSupplierByUser();

                //AddInventory(HotelCode,Supplier,MealPlan,CheckinR,CheckoutR,RoomTypeID,RateTypeCode)
                CUT.DataLayer.GlobalDefault objUser = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 sUserID = AccountManager.GetSupplierByUser();
                string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");
                //RoomTypeID, CheckinR/CheckoutR,      
                int[] arr = new int[5];
                string[] status = new string[2];
                status[0] = "Active";
                status[1] = "Deactive";
                int countRateId = 0, CountRoomTypes = 0;
                //string[]
                int k = 0;
                // int m = m + CheckinR.Count; ;
                for (int i = 0; i < RoomTypeID.Count(); i++)
                {

                    if (CancelPolicy[i] == "")
                    {
                        CancelPolicy[i] = "1";
                    }
                    List<tbl_commonRoomRate> ListRoomID = new List<tbl_commonRoomRate>();
                    using (var DB = new Click_Hotel())
                    {
                        Int64 Roomid = Convert.ToInt64(RoomTypeID[i]);
                        ListRoomID = (from obj in DB.tbl_commonRoomRate where (obj.RoomId == Roomid && obj.HotelID == HotelCode) select obj).ToList();
                    }
                    if (ListRoomID.Count == 0)
                    {
                        #region If No Entries present
                        for (int j = 0; j < CheckinR.Count; j++)
                        {
                            AddPrices(RoomTypeID[i], HotelCode, sUserID, Country, Currency, MealPlan, RateTypeCode[k], CheckinR[j], CheckoutR[j],
                                RateInclude, RateExclude, MinStay, MaxStay, MinRoom, TaxIncluded, TaxType, TaxOn, TaxValue,
                                TaxDetails, BookingCode[k], CancelPolicy[k], Offers[k], RateNote[k], RateType[k], Daywise[k],
                                Sunday[k], Monday[k], Tuesday[k], Wednesday[k], Thursday[k], Friday[k], Saturday[k],
                                RoomRates[k], ExtraBeds[k], CWB[k], CWN[k], OfferNight[k], Nights[k], MinRate[k]);
                            k++;
                        }

                        #endregion
                    }
                    else
                    {
                        bool AvlGenData, AvlNationality, AvlCancelation = false, AvlOffer = false, AvlDates = false;
                        //var dGeneralData = ListRoomID.Where(d => (d.MealPlan == MealPlan) && (d.CurrencyCode == Currency) && (d.SupplierId == Supplier) && d.MinStay == Convert.ToInt64(MinStay) && d.MaxStay == Convert.ToInt64(MaxStay)).ToList();
                        var dGeneralData = ListRoomID.Where(d => (d.MealPlan == MealPlan) && (d.CurrencyCode == Currency) && (d.SupplierId == Supplier) && (d.RateType == RateTypeCode[i])).ToList();

                        var dNationality = ListRoomID.Where(d => (d.ValidNationality == Country));

                        if (dGeneralData.Count() > 0) { AvlGenData = true; }
                        else { AvlGenData = false; }
                        if (dNationality.Count() > 0) { AvlNationality = true; }
                        else { AvlNationality = false; }

                        var RateTypes = dGeneralData.Select(d => d.RateType).ToList();
                        var RateID = dGeneralData.Select(d => d.HotelRateID).ToList();




                        if (AvlGenData == false)
                        {
                            #region if Supplier, MealPlan, Currency Not  Exists
                            for (int j = 0; j < CheckinR.Count; j++)
                            {
                                AddPrices(RoomTypeID[i], HotelCode, Uid, Country, Currency, MealPlan, RateTypeCode[k], CheckinR[j], CheckoutR[j],
                                 RateInclude, RateExclude, MinStay, MaxStay, MinRoom, TaxIncluded, TaxType, TaxOn, TaxValue,
                                 TaxDetails, BookingCode[k], CancelPolicy[k], Offers[k], RateNote[k], RateType[k], Daywise[k],
                                 Sunday[k], Monday[k], Tuesday[k], Wednesday[k], Thursday[k], Friday[k], Saturday[k],
                                 RoomRates[k], ExtraBeds[k], CWB[k], CWN[k], OfferNight[k], Nights[k], MinRate[k]);
                                k++;
                            }
                            #endregion
                        }
                        else if (AvlGenData == true)
                        {

                            for (int j = 0; j < CheckinR.Count; j++)
                            {
                                var ListWithinDates = dGeneralData.Where(d => DateTime.Parse(d.Checkin) <= DateTime.Parse(CheckinR[j]) && DateTime.Parse(d.Checkout) >= DateTime.Parse(CheckoutR[j])).ToList();
                                if (AvlNationality == false && ListWithinDates.Count > 0)
                                {
                                    #region If Nationality is different Add only Nationality to Existing Rates

                                    var Nationalities = dGeneralData.Select(d => d.ValidNationality).ToList();
                                    string nations = "";
                                    int count = 0;
                                    foreach (string nation in Nationalities)
                                    {
                                        nations += nation + Country;
                                    }
                                    List<string> uniques = nations.Split('^').Distinct().ToList();
                                    string newNationality = string.Join("^", uniques);
                                    foreach (var data in dGeneralData)
                                    {
                                        if (data.RoomId == Convert.ToInt64(RoomTypeID[i]) && data.RateType == RateTypeCode[i])
                                        {
                                            foreach (Int64 RTID in RateID)
                                            {
                                                using (var DB = new Click_Hotel())
                                                {
                                                    tbl_commonRoomRate Rate = DB.tbl_commonRoomRate.Single(x => x.HotelRateID == RTID);
                                                    Rate.ValidNationality = newNationality;
                                                    DB.SaveChanges();
                                                    CountRoomTypes += 1;
                                                }
                                            }
                                        }
                                    }
                                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                                    return json;
                                    #endregion

                                }
                                else
                                {
                                    AddPrices(RoomTypeID[i], HotelCode, Uid, Country, Currency, MealPlan, RateTypeCode[k], CheckinR[j], CheckoutR[j],
                                    RateInclude, RateExclude, MinStay, MaxStay, MinRoom, TaxIncluded, TaxType, TaxOn, TaxValue,
                                    TaxDetails, BookingCode[k], CancelPolicy[k], Offers[k], RateNote[k], RateType[k], Daywise[k],
                                    Sunday[k], Monday[k], Tuesday[k], Wednesday[k], Thursday[k], Friday[k], Saturday[k],
                                    RoomRates[k], ExtraBeds[k], CWB[k], CWN[k], OfferNight[k], Nights[k], MinRate[k]);
                                    k++;
                                }

                            }


                        }

                    }



                }
                if (ListAddOnsRates != null)
                {
                    using (var DB = new Click_Hotel())
                    {
                        List<Int64> RateIds = DB.tbl_commonRoomRate.OrderByDescending(obj => obj.HotelRateID).ToList().Take(ListAddOnsRates.Count).Select(r => Convert.ToInt64(r.HotelRateID)).Reverse().ToList();

                        if (RateIds.Count != null)
                        {
                            for (int i = 0; i < ListAddOnsRates.Count; i++)
                            {
                                List<Comm_AddOnsRate> ListAddOns = ListAddOnsRates[i];
                                if (ListAddOns.Count != 0)
                                {
                                    ListAddOns.ForEach(d => d.RateID = Convert.ToInt64(RateIds[i]));
                                    TaxManager.SaveTaxDetails(ListAddOns);
                                }
                            }
                        }
                    }
                    //List<Comm_TaxMapping> ListAddOns = ListAddOnsRates[i].Take(m).ToList();
                    //if (ListAddOns.Count != 0)
                    //{
                    //    ListAddOns.ForEach(d => d.ServiceID = DB.tbl_commonRoomRate.OrderByDescending(obj => obj.HotelRateID).FirstOrDefault().HotelRateID);
                    //    TaxManager.SaveTaxDetails(ListAddOns);

                    //}


                }
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                return json;

            }

            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateRates(List<string> arrRateId, Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, string CheckinR, string CheckoutR, List<string> RoomTypeID, List<string> RateTypeCode, List<string> RoomRates, List<string> ExtraBeds, List<string> CWB, List<string> CWN, List<string> BookingCode, List<string> CancelPolicy, List<string> Offers, List<string> RateNote, List<string> Daywise, List<string> Sunday, List<string> Monday, List<string> Tuesday, List<string> Wednesday, List<string> Thursday, List<string> Friday, List<string> Saturday, List<string> TypeRate, List<List<Comm_AddOnsRate>> ListAddOnsRates, List<string> OfferNight, List<Int64> Nights, List<float> MinRate)
        {
            try
            {
                //AddInventory(HotelCode,Supplier,MealPlan,CheckinR,CheckoutR,RoomTypeID,RateTypeCode)
                CUT.DataLayer.GlobalDefault objUser = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 sUserID = AccountManager.GetSuperAdminID();
                string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");
                using (var DB = new Click_Hotel())
                {
                    //  var listRate = (from obj in DB.tbl_commonRoomRate where obj.HotelID == HotelCode && obj.SupplierId == objUser.sid && obj.CurrencyCode == Currency && obj.MealPlan == MealPlan && obj.Checkin == CheckinR && obj.Checkout == CheckoutR && obj.ValidNationality.Contains(Country) select obj).ToList();

                    //var listRate = (from obj in DB.tbl_commonRoomRate where obj.HotelID == HotelCode && obj.SupplierId == objUser.sid && obj.CurrencyCode == Currency && obj.MealPlan == MealPlan && obj.Checkin == CheckinR && obj.ValidNationality.Contains(Country) select obj).ToList();

                    List<tbl_commonRoomRate> listRate = new List<tbl_commonRoomRate>();

                    for (int i = 0; i < RoomTypeID.Count(); i++)
                    {
                        string Id = "";
                        try
                        {
                            listRate = (from obj in DB.tbl_commonRoomRate where obj.HotelID == HotelCode && obj.SupplierId == AccountManager.GetSuperAdminID() && obj.Checkin == CheckinR && obj.HotelRateID == Convert.ToInt64(arrRateId[i]) select obj).ToList();

                            Id = listRate[0].RoomId.ToString();
                        }
                        catch
                        {
                            Id = "";
                        }
                        if (CancelPolicy[i] == "")
                        {
                            CancelPolicy[i] = "1";
                        }


                        if (RoomTypeID.Contains(Id))
                        {
                            for (int j = 0; j < listRate.Count; j++)
                            {

                                // if (RoomTypeID[i] == listRate[j].RoomId.ToString())
                                // {
                                tbl_commonRoomRate update = DB.tbl_commonRoomRate.Single(x => x.HotelRateID == Convert.ToInt64(arrRateId[i]));
                                update.RoomId = Convert.ToInt64(Id);
                                update.HotelID = HotelCode;
                                update.SupplierId = AccountManager.GetSuperAdminID();
                                update.ValidNationality = Country;
                                update.CurrencyCode = Currency;
                                update.MealPlan = MealPlan;
                                update.HotelContractId = 0;
                                update.RateType = RateTypeCode[i];
                                update.Type = TypeRate[i];
                                update.Checkin = CheckinR;
                                update.Checkout = CheckoutR;
                                update.BookingCode = BookingCode[i];
                                update.CancellationPolicyId = CancelPolicy[i];
                                update.OfferId = Offers[i];
                                update.TariffNote = RateNote[i];
                                update.DayWise = Daywise[i];
                                if (Daywise[i] == "Yes")
                                {
                                    if (Sunday[i] != "0")
                                    {
                                        update.SunRR = Convert.ToDecimal(Sunday[i].Split(',')[0]);
                                        // Rate.SunEB =Convert.ToDecimal( Sunday.Split(',')[1]);
                                    }
                                    if (Monday[i] != "0")
                                    {
                                        update.MonRR = Convert.ToDecimal(Monday[i].Split(',')[0]);
                                        // Rate.MonEB =Convert.ToDecimal( Monday.Split(',')[1]);
                                    }
                                    if (Tuesday[i] != "0")
                                    {
                                        update.TueRR = Convert.ToDecimal(Tuesday[i].Split(',')[0]);
                                        // Rate.TueEB = Convert.ToDecimal(TuesdaySplit(',')[1]);
                                    }
                                    if (Wednesday[i] != "0")
                                    {
                                        update.WedRR = Convert.ToDecimal(Wednesday[i].Split(',')[0]);
                                        //  Rate.WedEB =Convert.ToDecimal( Wednesday[i].Split(',')[1]);
                                    }
                                    if (Thursday[i] != "0")
                                    {
                                        update.ThuRR = Convert.ToDecimal(Thursday[i].Split(',')[0]);
                                        //  Rate.ThuEB = Convert.ToDecimal(Thursday[i].Split(',')[1]);
                                    }
                                    if (Friday[i] != "0")
                                    {
                                        update.FriRR = Convert.ToDecimal(Friday[i].Split(',')[0]);
                                        // Rate.FriEB = Convert.ToDecimal(Friday[i].Split(',')[1]);
                                    }
                                    if (Saturday[i] != "0")
                                    {
                                        update.SatRR = Convert.ToDecimal(Saturday[i].Split(',')[0]);
                                        // Rate.SatEB = Convert.ToDecimal(Saturday[i].Split(',')[1]);
                                    }
                                    update.RR = 0;
                                    //update.EB = null;
                                    //update.CWB = null;
                                    //update.CNB = null;
                                }
                                else
                                {
                                    update.RR = Convert.ToDecimal(RoomRates[i]);
                                    //update.EB = Convert.ToDecimal(ExtraBeds[j]);
                                    //update.CWB = Convert.ToDecimal(CWB[j]);
                                    //update.CNB = Convert.ToDecimal(CWN[j]);
                                    update.SunRR = null;
                                    update.MonRR = null;
                                    update.TueRR = null;
                                    update.WedRR = null;
                                    update.TueRR = null;
                                    update.FriRR = null;
                                    update.SatRR = null;
                                }
                                update.EB = Convert.ToDecimal(ExtraBeds[i]);
                                update.CWB = Convert.ToDecimal(CWB[i]);
                                update.CNB = Convert.ToDecimal(CWN[i]);
                                update.Status = "Active";
                                update.OfferNight = Convert.ToInt64(OfferNight[i]);
                                update.MinStayRate = Convert.ToDecimal(MinRate[i]);
                                update.StayNights = Convert.ToInt64(Nights[i]);
                                //DB.tbl_commonRoomRate.InsertOnSubmit(update);
                                DB.SaveChanges();
                                // }

                            }

                        }
                        //else if (RateID!=null)
                        //{
                        //    var listRateDate = (from obj in DB.tbl_commonRoomRate where obj.HotelID == HotelCode && obj.SupplierId == objUser.sid && obj.CurrencyCode == Currency && obj.MealPlan == MealPlan && obj.ValidNationality.Contains(Country) select obj).ToList();
                        //    for (int j = 0; j < listRateDate.Count; j++)
                        //    {

                        //        if (RoomTypeID[i] == listRateDate[j].RoomId.ToString())
                        //        {
                        //            tbl_commonRoomRate update = DB.tbl_commonRoomRate.Single(x => x.HotelRateID == listRateDate[j].HotelRateID && x.RoomId == listRateDate[j].RoomId);
                        //            update.RoomId = Convert.ToInt64(RoomTypeID[i]);
                        //            update.HotelID = HotelCode;
                        //            update.SupplierId = sUserID;
                        //            update.ValidNationality = Country;
                        //            update.CurrencyCode = Currency;
                        //            update.MealPlan = MealPlan;
                        //            update.HotelContractId = 0;
                        //            update.RateType = RateTypeCode[j];
                        //            update.Type = TypeRate[j];
                        //            update.Checkin = CheckinR;
                        //            update.Checkout = CheckoutR;
                        //            update.BookingCode = BookingCode[j];
                        //            update.CancellationPolicyId = CancelPolicy[j];
                        //            update.OfferId = Offers[j];
                        //            update.TariffNote = RateNote[j];
                        //            update.DayWise = Daywise[j];
                        //            if (Daywise[j] == "Yes")
                        //            {
                        //                if (Sunday[j] != "0")
                        //                {
                        //                    update.SunRR = Convert.ToDecimal(Sunday[j].Split(',')[0]);
                        //                    // Rate.SunEB =Convert.ToDecimal( Sunday.Split(',')[1]);
                        //                }
                        //                if (Monday[j] != "0")
                        //                {
                        //                    update.MonRR = Convert.ToDecimal(Monday[j].Split(',')[0]);
                        //                    // Rate.MonEB =Convert.ToDecimal( Monday.Split(',')[1]);
                        //                }
                        //                if (Tuesday[j] != "0")
                        //                {
                        //                    update.TueRR = Convert.ToDecimal(Tuesday[i].Split(',')[0]);
                        //                    // Rate.TueEB = Convert.ToDecimal(TuesdaySplit(',')[1]);
                        //                }
                        //                if (Wednesday[j] != "0")
                        //                {
                        //                    update.WedRR = Convert.ToDecimal(Wednesday[j].Split(',')[0]);
                        //                    //  Rate.WedEB =Convert.ToDecimal( Wednesday[i].Split(',')[1]);
                        //                }
                        //                if (Thursday[j] != "0")
                        //                {
                        //                    update.ThuRR = Convert.ToDecimal(Thursday[j].Split(',')[0]);
                        //                    //  Rate.ThuEB = Convert.ToDecimal(Thursday[i].Split(',')[1]);
                        //                }
                        //                if (Friday[j] != "0")
                        //                {
                        //                    update.FriRR = Convert.ToDecimal(Friday[j].Split(',')[0]);
                        //                    // Rate.FriEB = Convert.ToDecimal(Friday[i].Split(',')[1]);
                        //                }
                        //                if (Saturday[j] != "0")
                        //                {
                        //                    update.SatRR = Convert.ToDecimal(Saturday[j].Split(',')[0]);
                        //                    // Rate.SatEB = Convert.ToDecimal(Saturday[i].Split(',')[1]);
                        //                }
                        //                update.RR = null;
                        //                //update.EB = null;
                        //                //update.CWB = null;
                        //                //update.CNB = null;
                        //            }
                        //            else
                        //            {
                        //                update.RR = Convert.ToDecimal(RoomRates[j]);
                        //                //update.EB = Convert.ToDecimal(ExtraBeds[j]);
                        //                //update.CWB = Convert.ToDecimal(CWB[j]);
                        //                //update.CNB = Convert.ToDecimal(CWN[j]);
                        //                update.SunRR = null;
                        //                update.MonRR = null;
                        //                update.TueRR = null;
                        //                update.WedRR = null;
                        //                update.TueRR = null;
                        //                update.FriRR = null;
                        //                update.SatRR = null;
                        //            }
                        //            update.EB = Convert.ToDecimal(ExtraBeds[j]);
                        //            update.CWB = Convert.ToDecimal(CWB[j]);
                        //            update.CNB = Convert.ToDecimal(CWN[j]);
                        //            update.Status = "Active";
                        //            update.OfferNight = Convert.ToInt64(OfferNight[i]);
                        //            //DB.tbl_commonRoomRate.InsertOnSubmit(update);
                        //            DB.SubmitChanges();
                        //        }
                        //    }

                        //}

                        else
                        {
                            tbl_commonRoomRate Rate = new tbl_commonRoomRate();
                            Rate.RoomId = Convert.ToInt64(RoomTypeID[i]);
                            Rate.HotelID = HotelCode;
                            Rate.SupplierId = sUserID;
                            Rate.ValidNationality = Country;
                            Rate.CurrencyCode = Currency;
                            Rate.MealPlan = MealPlan;
                            Rate.HotelContractId = 0;
                            Rate.Checkin = CheckinR;
                            Rate.Checkout = CheckoutR;
                            Rate.Type = TypeRate[i];
                            Rate.RateType = RateTypeCode[i];
                            Rate.BookingCode = BookingCode[i];
                            Rate.CancellationPolicyId = CancelPolicy[i];
                            Rate.OfferId = Offers[i];
                            Rate.TariffNote = RateNote[i];
                            Rate.DayWise = Daywise[i];
                            if (Daywise[i] == "Yes")
                            {
                                if (Sunday[i] != "0")
                                {
                                    Rate.SunRR = Convert.ToDecimal(Sunday[i].Split(',')[0]);
                                    // Rate.SunEB =Convert.ToDecimal( Sunday.Split(',')[1]);
                                }
                                if (Monday[i] != "0")
                                {
                                    Rate.MonRR = Convert.ToDecimal(Monday[i].Split(',')[0]);
                                    // Rate.MonEB =Convert.ToDecimal( Monday.Split(',')[1]);
                                }
                                if (Tuesday[i] != "0")
                                {
                                    Rate.TueRR = Convert.ToDecimal(Tuesday[i].Split(',')[0]);
                                    // Rate.TueEB = Convert.ToDecimal(TuesdaySplit(',')[1]);
                                }
                                if (Wednesday[i] != "0")
                                {
                                    Rate.WedRR = Convert.ToDecimal(Wednesday[i].Split(',')[0]);
                                    //  Rate.WedEB =Convert.ToDecimal( Wednesday[i].Split(',')[1]);
                                }
                                if (Thursday[i] != "0")
                                {
                                    Rate.ThuRR = Convert.ToDecimal(Thursday[i].Split(',')[0]);
                                    //  Rate.ThuEB = Convert.ToDecimal(Thursday[i].Split(',')[1]);
                                }
                                if (Friday[i] != "0")
                                {
                                    Rate.FriRR = Convert.ToDecimal(Friday[i].Split(',')[0]);
                                    // Rate.FriEB = Convert.ToDecimal(Friday[i].Split(',')[1]);
                                }
                                if (Saturday[i] != "0")
                                {
                                    Rate.SatRR = Convert.ToDecimal(Saturday[i].Split(',')[0]);
                                    // Rate.SatEB = Convert.ToDecimal(Saturday[i].Split(',')[1]);
                                }
                                Rate.RR = null;
                                // Rate.EB = null;
                                // Rate.CWB = null;
                                // Rate.CNB = null;
                            }
                            else
                            {
                                Rate.RR = Convert.ToDecimal(RoomRates[i]);
                                //Rate.EB = Convert.ToDecimal(ExtraBeds[i]);
                                // Rate.CWB = Convert.ToDecimal(CWB[i]);
                                //Rate.CNB = Convert.ToDecimal(CWN[i]);
                                Rate.SunRR = null;
                                Rate.MonRR = null;
                                Rate.TueRR = null;
                                Rate.WedRR = null;
                                Rate.TueRR = null;
                                Rate.FriRR = null;
                                Rate.SatRR = null;
                            }
                            Rate.EB = Convert.ToDecimal(ExtraBeds[i]);
                            Rate.CWB = Convert.ToDecimal(CWB[i]);
                            Rate.CNB = Convert.ToDecimal(CWN[i]);
                            Rate.Status = "Active";
                            Rate.OfferNight = Convert.ToInt64(OfferNight[i]);
                            Rate.MinStayRate = Convert.ToDecimal(MinRate[i]);
                            Rate.StayNights = Convert.ToInt64(Nights[i]);
                            DB.tbl_commonRoomRate.Add(Rate);
                            DB.SaveChanges();

                        }
                    }

                    if (ListAddOnsRates != null)
                    {

                        List<Int64> RateIds = DB.tbl_commonRoomRate.OrderByDescending(obj => obj.HotelRateID).ToList().Take(ListAddOnsRates.Count).Select(r => Convert.ToInt64(r.HotelRateID)).Reverse().ToList();
                        if (RateIds.Count != null)
                        {
                            for (int i = 0; i < ListAddOnsRates.Count; i++)
                            {
                                List<Comm_AddOnsRate> ListAddOns = ListAddOnsRates[i];
                                if (ListAddOns.Count != 0)
                                {
                                    ListAddOns.ForEach(d => d.RateID = Convert.ToInt64(RateIds[i]));
                                    TaxManager.SaveTaxDetails(ListAddOns);
                                }
                            }
                        }


                    }
                }

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                return json;

            }

            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        // public void AddPrices(string RoomTypeID, Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, string RateTypeCode, string CheckinR, string CheckoutR, string RateInclude, string RateExclude, string MinStay, string MaxStay, string TaxIncluded, string TaxType, string TaxOn, string TaxValue, string TaxDetails, string BookingCode, string CancelPolicy, string Offers, string RateNote, string Daywise, string Sunday, string Monday, string Tuesday, string Wednesday, string Thursday, string Friday, string Saturday, string RoomRates, string ExtraBeds, string CWB, string CWN, string InventoryType, string NoOfInventoryRoom)
        public void AddPrices(string RoomTypeID, Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, string RateTypeCode, string CheckinR, string CheckoutR, string RateInclude, string RateExclude, string MinStay, string MaxStay, Int64 MinRoom, string TaxIncluded, string TaxType, string TaxOn, string TaxValue, string TaxDetails, string BookingCode, string CancelPolicy, string Offers, string RateNote, string RateType, string Daywise, string Sunday, string Monday, string Tuesday, string Wednesday, string Thursday, string Friday, string Saturday, string RoomRates, string ExtraBeds, string CWB, string CWN, string OfferNight, Int64 MinNight, float MinRate)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    tbl_commonRoomRate Rate = new tbl_commonRoomRate();
                    Rate.RoomId = Convert.ToInt64(RoomTypeID);
                    Rate.HotelID = HotelCode;
                    Rate.SupplierId = AccountManager.GetSuperAdminID();  //Supplier;
                    Rate.ValidNationality = Country;
                    Rate.CurrencyCode = Currency;
                    Rate.MealPlan = MealPlan;
                    Rate.HotelContractId = 0;
                    Rate.RateType = RateTypeCode;
                    Rate.Checkin = CheckinR;
                    Rate.Checkout = CheckoutR;
                    Rate.RateInclude = RateInclude;
                    Rate.RateExclude = RateExclude;
                    Rate.MinStay = Convert.ToInt16(MinStay);
                    Rate.MaxStay = Convert.ToInt16(MaxStay);
                    Rate.MaxStayNight = Convert.ToInt16(MaxStay);
                    Rate.StayNights = Convert.ToInt16(MinStay);
                    Rate.MinRooms = Convert.ToInt16(MinRoom);
                    Rate.MaxRooms = 0;
                    Rate.TaxIncluded = TaxIncluded;
                    Rate.TaxType = TaxType;
                    Rate.TaxOn = TaxOn;
                    Rate.TaxValue = Convert.ToInt16(TaxValue);
                    Rate.TaxDetails = TaxDetails;
                    Rate.BookingCode = BookingCode;
                    Rate.CancellationPolicyId = CancelPolicy;
                    Rate.OfferId = Offers;
                    Rate.TariffNote = RateNote;
                    Rate.Type = RateType;
                    Rate.DayWise = Daywise;
                    if (Daywise == "Yes")
                    {
                        if (Sunday != "0")
                        {
                            Rate.SunRR = Convert.ToDecimal(Sunday.Split(',')[0]);
                            // Rate.SunEB =Convert.ToDecimal( Sunday.Split(',')[1]);
                        }
                        if (Monday != "0")
                        {
                            Rate.MonRR = Convert.ToDecimal(Monday.Split(',')[0]);
                            // Rate.MonEB =Convert.ToDecimal( Monday.Split(',')[1]);
                        }
                        if (Tuesday != "0")
                        {
                            Rate.TueRR = Convert.ToDecimal(Tuesday.Split(',')[0]);
                            // Rate.TueEB = Convert.ToDecimal(TuesdaySplit(',')[1]);
                        }
                        if (Wednesday != "0")
                        {
                            Rate.WedRR = Convert.ToDecimal(Wednesday.Split(',')[0]);
                            //  Rate.WedEB =Convert.ToDecimal( Wednesday[i].Split(',')[1]);
                        }
                        if (Thursday != "0")
                        {
                            Rate.ThuRR = Convert.ToDecimal(Thursday.Split(',')[0]);
                            //  Rate.ThuEB = Convert.ToDecimal(Thursday[i].Split(',')[1]);
                        }
                        if (Friday != "0")
                        {
                            Rate.FriRR = Convert.ToDecimal(Friday.Split(',')[0]);
                            // Rate.FriEB = Convert.ToDecimal(Friday[i].Split(',')[1]);
                        }
                        if (Saturday != "0")
                        {
                            Rate.SatRR = Convert.ToDecimal(Saturday.Split(',')[0]);
                            // Rate.SatEB = Convert.ToDecimal(Saturday[i].Split(',')[1]);
                        }

                    }
                    else
                    {
                        Rate.RR = Convert.ToDecimal(RoomRates);
                        //Rate.EB = Convert.ToDecimal(ExtraBeds);
                        //Rate.CWB = Convert.ToDecimal(CWB);
                        //Rate.CNB = Convert.ToDecimal(CWN);
                    }
                    Rate.EB = Convert.ToDecimal(ExtraBeds);
                    Rate.CWB = Convert.ToDecimal(CWB);
                    Rate.CNB = Convert.ToDecimal(CWN);
                    Rate.Status = "Active";
                    Rate.OfferNight = Convert.ToInt64(OfferNight);
                    Rate.MinStayRate = Convert.ToDecimal(MinRate);
                    // Rate.StayNights = MinNight;
                    DB.tbl_commonRoomRate.Add(Rate);
                    DB.SaveChanges();
                    sid = Rate.HotelRateID;
                }
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

        }

        public class RatePrice
        {
            public string Date { get; set; }
            public string Daywise { get; set; }
            public string RRRate { get; set; }
            public string EBRate { get; set; }
            public string CWBRate { get; set; }
            public string CNBRate { get; set; }
            public string MonRate { get; set; }
            public string TueRate { get; set; }
            public string WedRate { get; set; }
            public string ThuRate { get; set; }
            public string FriRate { get; set; }
            public string SatRate { get; set; }
            public string SunRate { get; set; }
            public string CancelationPolicy { get; set; }
            public string CheckinDate { get; set; }
            public string CheckoutDate { get; set; }
        }

        public class Offer
        {
            public string OfferName { get; set; }
            public string From { get; set; }
            public string To { get; set; }
            public string OfferType { get; set; }
        }

        public class AddOnNew
        {
            public string Name { get; set; }
            public string Type { get; set; }
            public string MealType { get; set; }
            public string IsMeal { get; set; }
            public string RateType { get; set; }
            public Int64 Rate { get; set; }
            public Int64 CWB { get; set; }
            public Int64 CNB { get; set; }
        }

        public class Country
        {
            public string Name { get; set; }
        }

        public class CancelPolicy
        {
            public string CancelId { get; set; }
            public string RefundType { get; set; }
            public string CancellationPolicy { get; set; }
            public string ChargeTypes { get; set; }
            public string PolicyType { get; set; }
            public string DaysPrior { get; set; }
            public string Date { get; set; }
            public string NightToCharge { get; set; }
            public string PerToCharge { get; set; }
            public string AmntToCharge { get; set; }
            public string Note { get; set; }
            public string IsDaysPrior { get; set; }
        }
        public class Rates
        {
            public string RateType { get; set; }
            public string RoomId { get; set; }
            public string RoomName { get; set; }
            public List<RatePrice> PriceList { get; set; }
        }
        public class RateList
        {
            public string RoomId { get; set; }
            public string HotelRateID { get; set; }
            public string ValidNationality { get; set; }
            public string Checkin { get; set; }
            public string Checkout { get; set; }
            public string CurrencyCode { get; set; }
            public string RR { get; set; }
            public string EB { get; set; }
            public string CWB { get; set; }
            public string CNB { get; set; }
            public string RateType { get; set; }
            public string MealPlan { get; set; }
            public string Supplier { get; set; }

            public string DayWise { get; set; }
            public string MonRR { get; set; }
            public string TueRR { get; set; }
            public string WedRR { get; set; }
            public string ThuRR { get; set; }
            public string FriRR { get; set; }
            public string SatRR { get; set; }
            public string SunRR { get; set; }
            public string MinStayRate { get; set; }
            public string MaxStayRate { get; set; }
            public string MinRoom { get; set; }
            public string MaxRoom { get; set; }
        }
        public static IEnumerable<Tuple<DateTime, DateTime>> SplitDateRange(DateTime start, DateTime end, int dayChunkSize)
        {
            DateTime chunkEnd;
            while ((chunkEnd = start.AddDays(dayChunkSize)) < end)
            {
                yield return Tuple.Create(start, chunkEnd);
                start = chunkEnd;
            }
            yield return Tuple.Create(start, end);
        }


        [WebMethod(EnableSession = true)]
        public string GetBookingRates()
        {

            List<RatesManager.Supplier> Supplier = (List<RatesManager.Supplier>)Session["RatesDetails"];

            if (Supplier.Count != 0)
            {

                return jsSerializer.Serialize(new { retCode = 1, Session = 1, Rates = Supplier });
            }
            else
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        }

        [WebMethod(EnableSession = true)]
        public string GetHotelAddress(Int64 HotelCode)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    var HotelAddres = (from obj in DB.tbl_CommonHotelMaster where obj.sid == HotelCode select obj).FirstOrDefault();

                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, HotelAddress = HotelAddres });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }


        }

        [WebMethod(EnableSession = true)]
        public string GetRoomOccupancy(Int64 AdultCount, Int64 ChildCount, int RateTypeID, int MealID, Int64 RateID)
        {
            try
            {
                List<RatesManager.Supplier> Supplier = (List<RatesManager.Supplier>)Session["RatesDetails"];
                RatesManager.ListRateTypes = RatesManager.GetRates(Convert.ToInt64(Supplier[0].HotelCode));
                using (var DB = new Click_Hotel())
                {
                    foreach (var objDetails in Supplier[0].Details)
                    {
                        for (int i = 0; i < objDetails.Rate.ListDates.Count; i++)
                        {
                            RatesManager.Rate objRate = Supplier[0].Details.Where(
                                                           d => d.RateTypeID == RateTypeID &&
                                                           d.MealID == MealID && d.Rate.RateID == RateID
                                                           ).FirstOrDefault().Rate;
                            var sRates = (from obj in DB.tbl_commonRoomRate where obj.HotelRateID == Convert.ToInt64(objDetails.Rate.ListDates[i].RateID) select obj).FirstOrDefault();
                            // RatesManager.Rate objRate = objDetails.Rate.ListDates[i].Where(d => d.RateID == Convert.ToInt64(RateID));

                            if (objRate.MaxOccupancy >= AdultCount + ChildCount)
                            {
                                if (objRate.RoomOccupancy + objRate.MaxEB < AdultCount)
                                    throw new Exception("Maximum adults exceed");

                                Int64 RoomOccupancy = objRate.RoomOccupancy;
                                Int64 noAdultEB = 0;
                                float EBRate = 0;
                                float CWBRate = 0;
                                float CNBRate = 0;
                                RoomOccupancy = RoomOccupancy - AdultCount;
                                if (RoomOccupancy < 0)
                                {
                                    if (RoomOccupancy + objRate.MaxEB < 0)
                                        throw new Exception("Room Ocuupancy No Valid");
                                    noAdultEB = (-RoomOccupancy);
                                    RoomOccupancy = RoomOccupancy + objRate.MaxEB;
                                    //if (RoomOccupancy != 0)
                                    //    throw new Exception("No Extrabeds exceed");
                                    //EBRate = noAdultEB * objRate.EBRate;
                                    if (RoomOccupancy != 0)
                                        throw new Exception("No Extrabeds exceed");
                                    EBRate = noAdultEB * Convert.ToInt32(sRates.EB);



                                }
                                if (ChildCount != 0)
                                {
                                    Int64 CWB = 0, CNB = 0;
                                    if (noAdultEB == 0)
                                    {
                                        if (objRate.MaxEB == 1)
                                        {
                                            CWB = ChildCount + (objRate.MaxEB - ChildCount);
                                            // CWB =  (objRate.MaxEB - ChildCount);
                                            CWBRate = CWB * Convert.ToInt32(sRates.CWB);
                                        }
                                        else if (objRate.MaxEB > 1 && ChildCount == 1)
                                        {
                                            //CWB = ChildCount + (objRate.MaxEB - ChildCount);
                                            CWB = (objRate.MaxEB - ChildCount);
                                            CWBRate = CWB * Convert.ToInt32(sRates.CWB);
                                        }
                                        else
                                        {
                                            CWB = ChildCount + (objRate.MaxEB - ChildCount);
                                            // CWB = (objRate.MaxEB - ChildCount);
                                            CWBRate = CWB * Convert.ToInt32(sRates.CWB);
                                        }
                                        if ((objRate.MaxEB - ChildCount) < 0)
                                        {
                                            //RoomOccupancy = ChildCount + objRate.MaxEB;
                                            CNB = (-(objRate.MaxEB - ChildCount));
                                            CNBRate = CNB * Convert.ToInt32(sRates.CNB);
                                        }
                                        //else
                                        //CWBRate = ChildCount * objRate.CWBRate;

                                    }
                                    else
                                    {
                                        RoomOccupancy = RoomOccupancy - ChildCount;
                                        if (RoomOccupancy < 0)
                                        {
                                            CNB = -(RoomOccupancy);
                                            CNBRate = CNB * Convert.ToInt32(sRates.CNB);
                                            CWBRate = 0 * Convert.ToInt32(sRates.CWB);

                                        }

                                    }


                                }
                                objDetails.Rate.ListDates[i].CNBRate = RatesManager.GetRate(Convert.ToSingle(CNBRate), "ChildNoBeds"); ;
                                objDetails.Rate.ListDates[i].CWBRate = RatesManager.GetRate(Convert.ToSingle(CWBRate), "ChildWithBeds"); ;
                                objDetails.Rate.ListDates[i].EBRate = RatesManager.GetRate(Convert.ToSingle(EBRate), "ExtraBeds"); ;
                                objDetails.Rate.ListDates[i].TotalPrice = (Convert.ToInt32(objDetails.Rate.ListDates[i].Rate.BaseRate) + EBRate + CNBRate + CWBRate);
                                // RatesManager.GetTotalChargeTax(objDetails.Rate.ListDates[i]);

                            }
                            else
                            {
                                throw new Exception("Room Ocuupancy No Valid");
                            }
                            objDetails.Rate.TotalCharge = RatesManager.GetRate(Convert.ToSingle(objDetails.Rate.ListDates.Select(d => d.TotalPrice).ToList().Sum()), "ExtraBeds");
                        }
                        Session["RatesDetails"] = Supplier;
                        return jsSerializer.Serialize(new { retCode = 1, Session = 1, rate = objDetails.Rate });


                    }
                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1, message = ex.Message });

            }
        }

        [WebMethod(EnableSession = true)]
        public string UpdateInventory(List<RatesManager.Supplier> Supplier)
        {
            string Inventory = "";
            if (Supplier.Count != 0)
            {
                foreach (var objDetails in Supplier[0].Details)
                {
                    for (int i = 0; i < objDetails.Rate.ListDates.Count; i++)
                    {
                        tbl_CommonInventory Update = new tbl_CommonInventory();
                        DateTime InvDate = DateTime.ParseExact(objDetails.Rate.ListDates[i].Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        try
                        {
                            using (var DB = new Click_Hotel())
                            {
                                Update = DB.tbl_CommonInventory.Single(x => x.RateID == objDetails.Rate.ListDates[i].RateID && x.Date == Convert.ToString(InvDate));
                                if (Update != null)
                                    Update.Sold = (Convert.ToDecimal(Update.Sold) + Convert.ToDecimal(objDetails.noRooms)).ToString();
                                DB.SaveChanges();
                            }
                        }

                        catch { }

                        Inventory = jsSerializer.Serialize(new { retCode = 1, Session = 1 });
                    }

                }
            }
            else
            {
                Inventory = jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            return Inventory;
        }

        [WebMethod(EnableSession = true)]
        public string StartSale(List<RatesManager.Supplier> Supplier)
        {

            string Inventory = "";
            if (Supplier.Count != 0)
            {
                foreach (var objDetails in Supplier[0].Details)
                {
                    // var NoOfInv = (from obj in DB.tbl_CommonInventories where obj.HotelID == Supplier[0].HotelCode && obj.InventoryType == "FreeSale" && obj.RoomID == Convert.ToString(objDetails.RateTypeID) select obj).ToList();
                    for (int i = 0; i < objDetails.Rate.ListDates.Count; i++)
                    {
                        DateTime InvDate = DateTime.ParseExact(objDetails.Rate.ListDates[i].Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        using (var DB = new Click_Hotel())
                        {
                            var NoOfInv = (from obj in DB.tbl_CommonInventory where obj.HotelID == Supplier[0].HotelCode && obj.InventoryType == "FreeSale" && obj.RoomID == Convert.ToString(objDetails.RateTypeID) && obj.Date == Convert.ToString(InvDate) && obj.RateID == Convert.ToString(objDetails.Rate.RateID) select obj).ToList();
                            foreach (var Inv in NoOfInv)
                            {
                                if (Inv.Date == Convert.ToString(InvDate) && Inv.IsStop == false)
                                {
                                    tbl_CommonInventory set = DB.tbl_CommonInventory.Single(x => x.RateID == objDetails.Rate.ListDates[i].RateID && x.Date == Convert.ToString(InvDate));
                                    set.IsStop = true;
                                    DB.SaveChanges();
                                }
                                else
                                {
                                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, message = "Alresdy Have Start Sale" });
                                }
                            }
                        }
                    }
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, message = "Start Sale Done" });
                }

            }

            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            //  return Inventory;
            return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        }

        [WebMethod(EnableSession = true)]
        public string StopSale(List<RatesManager.Supplier> Supplier)
        {

            string Inventory = "";
            if (Supplier.Count != 0)
            {
                foreach (var objDetails in Supplier[0].Details)
                {
                    // var NoOfInv = (from obj in DB.tbl_CommonInventories where obj.HotelID == Supplier[0].HotelCode && obj.InventoryType == "FreeSale" && obj.RoomID == Convert.ToString(objDetails.RateTypeID) select obj).ToList();
                    for (int i = 0; i < objDetails.Rate.ListDates.Count; i++)
                    {
                        DateTime InvDate = DateTime.ParseExact(objDetails.Rate.ListDates[i].Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        using (var DB = new Click_Hotel())
                        {
                            var NoOfInv = (from obj in DB.tbl_CommonInventory where obj.HotelID == Supplier[0].HotelCode && obj.InventoryType == "FreeSale" && obj.RoomID == Convert.ToString(objDetails.RateTypeID) && obj.Date == Convert.ToString(InvDate) && obj.RateID == Convert.ToString(objDetails.Rate.RateID) select obj).ToList();
                            foreach (var Inv in NoOfInv)
                            {

                                if (Inv.Date == Convert.ToString(InvDate) && Inv.IsStop == true)
                                {
                                    tbl_CommonInventory set = DB.tbl_CommonInventory.Single(x => x.RateID == objDetails.Rate.ListDates[i].RateID && x.Date == Convert.ToString(InvDate));
                                    set.IsStop = false;
                                    DB.SaveChanges();
                                }
                                else
                                {
                                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, message = "Alresdy Have Stop Sale" });
                                }
                            }
                        }

                    }
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, message = "Stop Sale Done" });
                }

            }

            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            //  return Inventory;
            return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        }

        [WebMethod(EnableSession = true)]
        public string CheckFreeSale(List<RatesManager.Supplier> Supplier)
        {

            string Inventory = "";
            if (Supplier.Count != 0)
            {
                using (var DB = new Click_Hotel())
                {
                    foreach (var objDetails in Supplier[0].Details)
                    {

                        for (int i = 0; i < objDetails.Rate.ListDates.Count; i++)
                        {
                            DateTime InvDate = DateTime.ParseExact(objDetails.Rate.ListDates[i].Date, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            var NoOfInv = (from obj in DB.tbl_CommonInventory where obj.HotelID == Supplier[0].HotelCode && obj.InventoryType == "FreeSale" && obj.RoomID == Convert.ToString(objDetails.RateTypeID) && obj.Date == Convert.ToString(InvDate) select obj).ToList();
                            foreach (var Inv in NoOfInv)
                            {
                                if (Inv.Date == Convert.ToString(InvDate) && Inv.IsStop == false)
                                {
                                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, message = "These Dates are Stop Sale" });
                                }
                            }
                        }
                        return jsSerializer.Serialize(new { retCode = 1, Session = 1, message = "These Dates are Stop Sale Continue to Booking" });
                    }
                }
            }

            else
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            //  return Inventory;
            return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
        }

        [WebMethod(EnableSession = true)]
        public string GetRatelist(string HotelCode)
        {
            try
            {
                var arrRates = CutAdmin.DataLayer.HotelLayer.RateManager.GetRatePlan(HotelCode);
                if (arrRates.Count == 0)
                    throw new Exception("No Rates Found");
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, RateList = arrRates });
            }
            catch(Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 ,ex= ex.Message});
            }

        }


        [WebMethod(EnableSession = true)]
        public string GetRateData(CutAdmin.DataLayer.HotelLayer.RateManager arrRate)
        {
            try
            {
                var RateData = CutAdmin.DataLayer.HotelLayer.RateManager.GetTarrifRates(arrRate);
                return jsSerializer.Serialize(new { retCode = 1, Session = 1, RateData = RateData });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }

        [WebMethod(EnableSession = true)]
        public string ChekAvailRates(Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, List<string> CheckinR, List<string> CheckoutR)
        {
            try
            {
                CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                DateTime sCheckIn, sCheckOut;
                List<DateTime> ListDates = new List<DateTime>();
                //List<DateTime> ListOldDates = new List<DateTime>();
                for (int i = 0; i < CheckinR.Count; i++)
                {
                    sCheckIn = DateTime.ParseExact(CheckinR[i], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    sCheckOut = DateTime.ParseExact(CheckoutR[i], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    double noDays = (sCheckOut - sCheckIn).TotalDays;

                    for (int j = 0; j <= noDays; j++)
                    {
                        ListDates.Add(sCheckIn.AddDays(j));

                    }
                }

                List<tbl_commonRoomRate> ListRoomRates = new List<tbl_commonRoomRate>();
                List<tbl_commonRoomRate> dGeneralData = new List<tbl_commonRoomRate>();
                List<tbl_commonRoomRate> dNationality = new List<tbl_commonRoomRate>();

                using (var DB = new Click_Hotel())
                {
                    ListRoomRates = (from obj in DB.tbl_commonRoomRate where obj.HotelID == HotelCode && obj.SupplierId == AccountManager.GetSuperAdminID() select obj).ToList();
                }
                //for (int i = 0; i < ListRoomID.Count; i++)
                //{
                //    dGeneralData = ListRoomID.Where(d => (d.MealPlan == MealPlan) && (d.CurrencyCode == Currency) && (d.SupplierId == Supplier) && (DateTime.Parse(d.Checkin) <= DateTime.Parse(CheckinR[0]))).ToList();
                //    dNationality = ListRoomID.Where(d => (d.ValidNationality == Country)).ToList();
                //}


                bool AvlGenData, AvlNationality = false;
                var sRate = new List<tbl_commonRoomRate>();
                foreach (var Rate in ListRoomRates)
                {

                    List<DateTime> ListOldDates = new List<DateTime>();
                    if (Rate.MealPlan == MealPlan && Rate.CurrencyCode == Currency && Rate.SupplierId == AccountManager.GetSuperAdminID() && Rate.Status == "Active" && Rate.RateType == "GN")
                    {

                        sCheckIn = DateTime.ParseExact(Rate.Checkin, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        sCheckOut = DateTime.ParseExact(Rate.Checkout, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        double noDays = (sCheckOut - sCheckIn).TotalDays;

                        for (int j = 0; j <= noDays; j++)
                        {
                            ListOldDates.Add(sCheckIn.AddDays(j));

                        }
                        if (Rate.ValidNationality == Country)
                            AvlNationality = true;

                        bool result = ListOldDates.Any(x => ListDates.Contains(x));
                        if (result == true)
                            sRate.Add(Rate);


                    }
                }

                if (sRate.Count != 0)
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, sRate = sRate });
                else
                    return jsSerializer.Serialize(new { retCode = 0, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }

        [WebMethod(EnableSession = true)]
        public string GetURate(Int64 RateID)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    var Rate = (from obj in DB.tbl_commonRoomRate where obj.HotelRateID == RateID select obj).ToList();

                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, Rate = Rate });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }



        public class ListOfTaxes
        {

            public Int64 ID { get; set; }
            public Int64 TaxID { get; set; }
            public Int64 TaxOnID { get; set; }
            public Int64 IsAddOn { get; set; }
            public Int64 HotelID { get; set; }
            public Int64 ServiceID { get; set; }
            public string ServiceName { get; set; }
            public Int64 ParentID { get; set; }
        }

        [WebMethod(EnableSession = true)]
        public string GetOtherRates(Int64 HotelCode, Int64 Supplier, string Country, string Currency, string MealPlan, string CheckinR, string CheckoutR)
        {
            Click_Hotel dbTax = new Click_Hotel();
            try
            {
                List<ListOfTaxes> ListTax = new List<ListOfTaxes>();
                using (var DB = new Click_Hotel())
                {
                    var listRate = (from obj in DB.tbl_commonRoomRate where obj.HotelID == HotelCode && obj.SupplierId == Supplier && obj.CurrencyCode == Currency && obj.MealPlan == MealPlan && obj.Checkin == CheckinR && obj.Checkout == CheckoutR && obj.ValidNationality.Contains(Country) select obj).ToList();

                    for (int i = 0; i < listRate.Count; i++)
                    {
                        var Tax = (from obj in DB.Comm_TaxMapping where obj.HotelID == HotelCode && obj.ServiceID == listRate[i].HotelRateID select obj).ToList();
                        for (int j = 0; j < Tax.Count; j++)
                        {
                            ListTax.Add(new ListOfTaxes
                            {
                                ID = Tax[j].ID,
                                TaxID = Tax[j].TaxID,
                                TaxOnID = Convert.ToInt64(Tax[j].TaxOnID),
                                IsAddOn = Convert.ToInt64(Tax[j].IsAddOns),
                                HotelID = Convert.ToInt64(Tax[j].HotelID),
                                ServiceID = Convert.ToInt64(Tax[j].ServiceID),
                                ServiceName = Tax[j].ServiceName

                            });
                        }

                    }
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, listRate = listRate, ListTax = ListTax });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }

        [WebMethod(EnableSession = true)]
        public string FilterRate(List<string> RoomType, List<string> MealType, float MinPrice, float MaxPrice)
        {
            try
            {
                string AddSearchsession = HttpContext.Current.Session["session"].ToString();
                List<CommonLib.Response.RateGroup> objRateGroup = new List<RateGroup>();
                objRateGroup.Add(new RateGroup
                {
                    RoomOccupancy = RoomFilter.FilterRate(AddSearchsession, RoomType, MealType, MinPrice, MaxPrice),
                });

                return jsSerializer.Serialize(new { retCode = 1, Session = 1, objRateGroup = objRateGroup });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }


        [WebMethod(EnableSession = true)]
        public string GetOffer(Int64 RateID)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    List<Offer> ListOffer = new List<Offer>();
                    var OfferId = (from obj in DB.tbl_commonRoomRate where obj.HotelRateID == RateID select obj.OfferId).ToList();
                    var Offerse = OfferId[0].Split(',');

                    for (int i = 0; i < Offerse.Length - 1; i++)
                    {
                        var OfferName = (from obj in DB.tbl_CommonHotelOffer where obj.OfferID == Convert.ToInt64(Offerse[i]) select obj).ToList();

                        foreach (var item in OfferName)
                        {
                            ListOffer.Add(new Offer
                            {
                                OfferType = item.OfferType,
                                OfferName = item.SeasonName,
                                From = item.ValidFrom,
                                To = item.ValidTo,
                            });
                        }

                    }

                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, ListOffer = ListOffer });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }

        [WebMethod(EnableSession = true)]
        public string GetCancellation(string CancellationPolicyId)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    var Cancelations = CancellationPolicyId.Split(',');
                    List<CancelPolicy> ListCancle = new List<CancelPolicy>();



                    for (int i = 0; i < Cancelations.Length - 1; i++)
                    {

                        var CancelationsName = (from obj in DB.tbl_CommonCancelationPolicy where obj.CancelationID == Convert.ToInt64(Cancelations[i]) select obj).ToList();
                        foreach (var item in CancelationsName)
                        {
                            ListCancle.Add(new CancelPolicy
                            {
                                RefundType = item.RefundType,
                                CancellationPolicy = item.CancelationPolicy,
                                ChargeTypes = item.ChargesType,
                                PolicyType = item.PolicyType,
                                DaysPrior = item.DaysPrior.ToString(),
                                Date = item.Date,
                                NightToCharge = item.NightsToCharge,
                                PerToCharge = item.PercentageToCharge,
                                AmntToCharge = item.AmountToCharge,
                                Note = item.CancelationNote,
                                IsDaysPrior = item.IsDaysPrior
                            });
                        }

                    }

                    return jsSerializer.Serialize(new { retCode = 1, Session = 1, ListCancle = ListCancle });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }

        [WebMethod(EnableSession = true)]
        public string GetAddOn(Int64 RateID)
        {
            try
            {
                List<Comm_AddOnsRate> AddOnsRate = new List<Comm_AddOnsRate>();
                List<CutAdmin.EntityModal.Comm_AddOns> AddOn = new List<CutAdmin.EntityModal.Comm_AddOns>();
                List<AddOnNew> AddOnNew = new List<AddOnNew>();
                Object arrAddOns = new object();
                Int64 UID = AccountManager.GetSuperAdminID();

                using (var DB = new Click_Hotel())
                {
                    var Add = (from obj in DB.Comm_AddOns
                               join objR in DB.tbl_commonRoomRate on obj.UserId equals UID
                               where objR.HotelRateID == RateID && obj.IsMeal == false
                               select obj).ToList();

                    arrAddOns = (from obj in DB.Comm_AddOns
                                 where obj.UserId == UID && obj.IsMeal == false
                                 select obj).ToList();
                    //var Add = (from obj in DB.Comm_AddOns
                    //           join objR in DB.tbl_commonRoomRate on obj.UserId equals objR.SupplierId
                    //           where obj.IsMeal == false
                    //           select obj).FirstOrDefault();
                    foreach (var Addon in Add)
                    {
                        AddOnsRate = (from obj in DB.Comm_AddOnsRate where obj.RateID == RateID && obj.AddOnsID == Addon.ID select obj).ToList();
                        foreach (var item in AddOnsRate)
                        {
                            AddOnNew.Add(new AddOnNew
                            {
                                CWB = Convert.ToInt64(item.CWB),
                                CNB = Convert.ToInt64(item.CNB),
                                Rate = Convert.ToInt64(item.Rate),
                                MealType = (from objC in DB.Comm_AddOns where objC.ID == item.AddOnsID select objC.AddOnName).FirstOrDefault(),
                            });
                        }
                    }

                }


                return jsSerializer.Serialize(new { retCode = 1, Session = 1, AddOnNew = AddOnNew, arrAddOns = arrAddOns, });

            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }

        [WebMethod(EnableSession = true)]
        public string GetMeal(Int64 RateID)
        {
            try
            {
                List<Comm_AddOnsRate> AddOnsRate = new List<Comm_AddOnsRate>();
                List<CutAdmin.EntityModal.Comm_AddOns> AddOn = new List<Comm_AddOns>();
                List<AddOnNew> AddOnMeal = new List<AddOnNew>();


                using (var DB = new Click_Hotel())
                {
                    Int64 userId = AccountManager.GetSuperAdminID();
                    AddOn = (from obj in DB.Comm_AddOns
                             join objR in DB.tbl_commonRoomRate on obj.UserId equals userId
                             where objR.HotelRateID == RateID && obj.IsMeal == true
                             select obj).ToList();


                    using (var db = new Click_Hotel())
                    {
                        foreach (var item in AddOn)
                        {
                            AddOnsRate = (from obj in db.Comm_AddOnsRate where obj.RateID == RateID && obj.AddOnsID == item.ID select obj).ToList();
                            foreach (var items in AddOnsRate)
                            {
                                //if(items.CNB != Convert.ToDecimal("0.00") || items.CWB != Convert.ToDecimal("0.00") || items.Rate != Convert.ToDecimal("0.00"))
                                // {
                                AddOnMeal.Add(new AddOnNew
                                {
                                    CWB = Convert.ToInt64(items.CWB),
                                    CNB = Convert.ToInt64(items.CNB),
                                    Rate = Convert.ToInt64(items.Rate),
                                    MealType = (from objC in DB.Comm_AddOns where objC.ID == items.AddOnsID select objC.AddOnName).FirstOrDefault(),

                                });
                                //}   
                            }
                        }
                    }

                }


                return jsSerializer.Serialize(new { retCode = 1, Session = 1, AddOnMeal = AddOnMeal });

            }
            catch(Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }

        [WebMethod(EnableSession = true)]
        public string UpdateRateStatus(Int64 RateID, string status)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {

                    tbl_commonRoomRate update = DB.tbl_commonRoomRate.Single(x => x.HotelRateID == Convert.ToInt64(RateID));
                    update.Status = status;
                    DB.SaveChanges();
                }


                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });

            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }

        [WebMethod(EnableSession = true)]
        public string GetRates(Int64[] HotelCode, string Destination, string Checkin, string Checkout, string[] nationality, int Nights, int Adults, int Childs, string Supplier, string MealPlan, string CurrencyCode, string AddSearchsession, string SearchValid)
        {
            string jsonString = "";
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            int adult = 0;
            int child = 0;
            Int64 noRooms = Convert.ToInt64(AddSearchsession.Split('_')[4]);
            try
            {
                if (Session["session" + AddSearchsession] == null)
                {
                    Session["session"] = AddSearchsession;
                    int Age = 0;
                    if (HotelCode.Length != 0)
                    {
                        string ErrorMessage;
                        List<Dictionary<string, object>> RatesList = new List<Dictionary<string, object>>();
                        List<Dictionary<string, object>> HotelRatesList1 = new List<Dictionary<string, object>>();
                        List<Dictionary<string, object>> HotelRatesList = new List<Dictionary<string, object>>();
                        List<Dictionary<string, object>> SeasonOfferList = new List<Dictionary<string, object>>();
                        using (var DB = new Click_Hotel())
                        {
                            var HotelDetails = (from obj in DB.tbl_CommonHotelMaster where obj.sid == HotelCode[0] select obj).FirstOrDefault();
                            var AllRooms = (from Room in DB.tbl_commonRoomDetails where Room.HotelId == HotelCode[0] select Room).ToList();
                            List<RateGroup> RateList = RatesManager.GetRateList(HotelCode[0], Checkin, Checkout, nationality, AllRooms, SearchValid, noRooms);
                            if (RateList.Count == 0)
                            {
                                return objserialize.Serialize(new { Session = 1, retCode = -2, ErrorMessage = "Hotel is not available" });
                            }
                            double noDays = (DateTime.ParseExact(RatesManager.CheckOutDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) - DateTime.ParseExact(RatesManager.CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture)).TotalDays;
                            List<string> ListDates = new List<string>();
                            for (int i = 0; i <= noDays; i++)
                            {
                                ListDates.Add(DateTime.ParseExact(RatesManager.CheckInDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(i).ToString("dd-MM-yyyy"));
                            }
                            var GlobalDefault = (CUT.DataLayer.GlobalDefault)Session["LoginUser"];
                            var CurrencyList = (from obj in DB.tbl_CommonCurrency select obj).ToList();
                            var MealPlanList = (from obj in DB.tbl_CommonMealPlan select obj).ToList();
                            RateList = RateList.Distinct().ToList();
                            var SupplierList = RateList.Select(d => d.Name).Distinct().ToList();
                            // Store avail rate into session
                            #region AvailRates
                            var arrHotel = new CommonLib.Response.CommonHotelDetails();
                            string[] arrSearchAttr = AddSearchsession.Split('_');
                            var arrHotelInfo = (from obj in DB.tbl_CommonHotelMaster
                                                where obj.sid == HotelCode[0]
                                                select new
                                                {
                                                    obj.sid,
                                                    obj.HotelName,
                                                    obj.HotelLatitude,
                                                    obj.HotelLangitude,
                                                    obj.HotelAddress,
                                                    obj.HotelZipCode,
                                                    obj.HotelImage,
                                                    obj.SubImages,
                                                    obj.HotelCategory,
                                                    obj.HotelDescription,
                                                    obj.HotelFacilities
                                                }).FirstOrDefault();
                            string Image = arrHotelInfo.HotelImage;
                            string URL = System.Configuration.ConfigurationManager.AppSettings["URL"];
                            List<Image> arrImage = new List<Image>();
                            if (Image != null)
                            {
                                List<string> Url = Image.Split('^').ToList();

                                foreach (string Link in Url)
                                {
                                    if (Link != "")
                                        arrImage.Add(new Image { Url = URL + "/HotelImages/" + Link, Count = Url.Count });
                                }
                            }
                            if (arrHotelInfo.SubImages != null)
                            {
                                List<string> Url = arrHotelInfo.SubImages.Split('^').ToList();

                                foreach (string Link in Url)
                                {
                                    if (Link != "")
                                        arrImage.Add(new Image { Url = URL + "/HotelImages/" + Link, Count = Url.Count });
                                }
                            }
                            List<string> sFacilities = new List<string>();
                            foreach (var objID in arrHotelInfo.HotelFacilities.Split(','))
                            {
                                if (objID != "")
                                {
                                    var FacilityName = (from obj in DB.tbl_commonFacility where obj.HotelFacilityID == Convert.ToInt64(objID) select obj).FirstOrDefault();
                                    if (FacilityName != null)
                                        sFacilities.Add(FacilityName.HotelFacilityName);
                                }

                            }


                            arrHotel = new CommonHotelDetails
                            {
                                DateFrom = arrSearchAttr[2],
                                DateTo = arrSearchAttr[3],
                                HotelId = arrHotelInfo.sid.ToString(),
                                Description = arrHotelInfo.HotelDescription,
                                HotelName = arrHotelInfo.HotelName,
                                Category = arrHotelInfo.HotelCategory,
                                Address = arrHotelInfo.HotelAddress,
                                Image = arrImage,
                                List_RateGroup = RateList,
                                Langitude = arrHotelInfo.HotelLangitude,
                                Latitude = arrHotelInfo.HotelLatitude,
                                Facility = CUT.Common.ParseCommonResponse.GetCommonFacilities(sFacilities.Distinct().ToList())
                            };
                            Session["AvailDetails" + AddSearchsession] = arrHotel;
                            #endregion
                            string json = objserialize.Serialize(new { Session = 1, retCode = 1, ListRate = RateList, Usertype = AccountManager.GetUserType(), AllRooms = AllRooms, SupplierList = SupplierList, CurrencyList = CurrencyList, MealPlanList = MealPlanList, ListDates = ListDates, Filter = RoomFilter.GenrateFilter(AddSearchsession) });
                            Session["session" + AddSearchsession] = json;
                            return json;

                        }
                    }

                    return objserialize.Serialize(new { Session = 1, retCode = -2, ErrorMessage = "Hotel is not available for these Dates" });
                }
                else
                {
                    json = Session["session" + AddSearchsession].ToString();
                    return json;
                }
            }
            catch (Exception ex)
            {
                return objserialize.Serialize(new { Session = 1, retCode = 0, ex = ex.Message });
            }



        }

        [WebMethod(EnableSession = true)]
        public string UpdateMealRate(List<List<Comm_AddOnsRate>> ListAddOnsRates)
        {
            try
            {
                if (ListAddOnsRates != null)
                {
                    using (var DB = new Click_Hotel())
                    {
                        for (int i = 0; i < ListAddOnsRates.Count; i++)
                        {
                            List<Comm_AddOnsRate> ListAddOns = ListAddOnsRates[i];
                            if (ListAddOns.Count != 0)
                            {
                                TaxManager.SaveTaxDetails(ListAddOns);
                            }
                        }
                    }
                }
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0, Session = 1 });

            }

        }

        #region DeleteFile
        [WebMethod(EnableSession = true)]
        public string DeleteFile(string FileName, string sid)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new Click_Hotel())
                {
                    string OldImages = (from obj in DB.tbl_CommonHotelMaster where obj.sid.ToString() == sid select obj.SubImages).FirstOrDefault();
                    string[] OldTokens = OldImages.Split('^');
                    string AvalImages = "";
                    foreach (var img in OldTokens)
                    {
                        if (img != FileName)
                        {
                            AvalImages += img + "^";
                        }
                    }
                    tbl_CommonHotelMaster Update = DB.tbl_CommonHotelMaster.Single(x => x.sid.ToString() == sid);
                    Update.SubImages = AvalImages;
                    DB.SaveChanges();
                    return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
            }
        }
        #endregion

        #region Room Activation
        [WebMethod(EnableSession = true)]
        public string ActivateRoom(Int64 RoomId, string Status)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    tbl_commonRoomDetails Room = DB.tbl_commonRoomDetails.Single(x => x.RoomId == RoomId);
                    Room.Approved = Convert.ToBoolean(Status);
                    DB.SaveChanges();
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });
                }
            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        #endregion

        #region Room Delete
        [WebMethod(EnableSession = true)]
        public string DeleteRoom(Int64 RoomId)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    var Delete = DB.tbl_commonRoomDetails.FirstOrDefault(x => x.RoomId == RoomId);// object your want to delete
                    DB.tbl_commonRoomDetails.Remove(Delete);
                    DB.SaveChanges();
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });
                }
            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        #endregion

        #region Hotel Delete
        [WebMethod(EnableSession = true)]
        public string DeleteHotel(Int64 HotelId)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    var Delete = DB.tbl_CommonHotelMaster.FirstOrDefault(x => x.sid == HotelId);// object your want to delete
                    DB.tbl_CommonHotelMaster.Remove(Delete);
                    DB.SaveChanges();
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1 });
                }
            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        #endregion

        #region Room Details
        [WebMethod(EnableSession = true)]
        public string GetRoombyHotel(string HotelCode)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                return jsSerializer.Serialize(new { retCode=1,arrRoom = CutAdmin.Services.Rooms.GetRooms(HotelCode)});
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode=0,ex=ex.Message });
            }
        }
            #endregion
        }
}
