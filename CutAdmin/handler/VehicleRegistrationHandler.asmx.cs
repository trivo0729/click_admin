﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for VehicleRegistrationHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class VehicleRegistrationHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer objSerializer = new JavaScriptSerializer();
        TransfersHelperDataContext DB = new TransfersHelperDataContext();
        string json = "";

        [WebMethod(true)]
        public string GetVehicleModel()
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            try
            {
                using (var DB = new TransfersHelperDataContext())
                {
                    //var List = (from obj in DB.tbl_VehicleModels select obj).Distinct().ToList();
                    var List = (from obj in DB.tbl_VehicleModels
                                join objVtype in DB.tbl_VechileTypes on obj.VehicleType equals objVtype.ID
                                select new
                                {
                                    obj.ID,
                                    obj.VehicleType,
                                    objVtype.VehicleBrand,
                                    obj.SeattingCapacity,
                                    obj.BaggageCapacity,

                                }).Distinct().ToList();



                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, tbl_VehicleModel = List });


                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetVehicleAmenities()
        {
            string json = "";
            try
            {
                var List = (from obj in DB.tbl_VehicleAmenities
                           
                            select new
                            {
                                obj.ID,
                                obj.Amenity,
                               

                            }).Distinct().ToList();

                //var List = (from obj in DB.tbl_VehicleAmenities select obj).ToList();
                json = objSerializer.Serialize(new { Session = 1, retCode = 1, tbl_VehicleAmenities = List });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string GetAllVehicleDetails()
        {
            string json = "";
            try
            {
                var List = (from obj in DB.tbl_VehicleRegistrations
                            join objVModel in DB.tbl_VehicleModels on obj.VehicleModal equals objVModel.ID
                            join objVtype in DB.tbl_VechileTypes on objVModel.VehicleType equals objVtype.ID
                            select new
                            {
                                obj.ID,
                                obj.Supplier,
                                obj.OtherSupplier,
                                objVtype.VehicleBrand,
                                obj.ModalYear,
                                obj.NoPlate,
                                obj.RegistrationDate,
                                obj.RegistrationValidity,
                                obj.InsuranceNo,
                                obj.InsuranceDueDate,
                                obj.MaintenanceDue,
                                obj.Amenities,

                            }).Distinct().ToList();
                GenralHandler.ListtoDataTable lsttodt = new GenralHandler.ListtoDataTable();
                Session["dt_VechileReg"]= lsttodt.ToDataTable(List);
                json = objSerializer.Serialize(new { Session = 1, retCode = 1, tbl_VehicleDetails = List });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetVehicleByID(Int64 ID)
        {
            string json = "";
            try
            {
                var List = (from obj in DB.tbl_VehicleRegistrations
                            join objVModel in DB.tbl_VehicleModels on obj.VehicleModal equals objVModel.ID
                            join objVtype in DB.tbl_VechileTypes on objVModel.VehicleType equals objVtype.ID
                            where obj.ID == ID
                            select new
                            {
                                obj.ID,
                                obj.Supplier,
                                obj.OtherSupplier,
                                obj.VehicleModal,
                                objVtype.VehicleBrand,
                                obj.ModalYear,
                                obj.NoPlate,
                                obj.RegistrationDate,
                                obj.RegistrationValidity,
                                obj.InsuranceNo,
                                obj.InsuranceDueDate,
                                obj.MaintenanceDue,
                                obj.Amenities,


                            }).Distinct().ToList();
                var Amenities = List[0].Amenities;
                //var Amenity= Amenities.s
                string[] Amenity = Amenities.Split('^');
                List<AmenityList> Amenity_List = new List<AmenityList>();
                foreach (var item in Amenity)
                {
                    if (item!="")
                    {
                        var Amenitnyvar = (from obj in DB.tbl_VehicleAmenities
                                           where obj.ID == Convert.ToInt64(item)
                                           select new
                                           {
                                               obj.ID,
                                               obj.Amenity
                                           }).ToList();

                        Amenity_List.Add(new AmenityList
                        {
                            id = Amenitnyvar[0].ID,
                            Aminty = Amenitnyvar[0].Amenity
                        });
                    }
                    
                }
                //for (int i = 0; i < Amenity.Length; i++)
                //{
                //    var Amenityvar = (from obj in DB.tbl_VehicleAmenities where obj.ID == Convert.ToInt64(Amenity[i]) select obj.Amenity );
                //    Amenity_List = Amenityvar
                //}
                //var List = (from obj in DB.tbl_VehicleRegistrations select obj).ToList();
                json = objSerializer.Serialize(new { Session = 1, retCode = 1, tbl_VehicleDetails = List, tbl_AmenityList= Amenity_List });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string UpdateVehicle(Int64 ID, string Supplier, string OtherSupplier, Int64 VehicleModal, string ModalYear, string NoPlate, string RegistrationDate, string RegistrationValidity, string InsuranceNo, string InsuranceDueDate, string MaintenanceDue, string Amenities)
        {
            string json = "";
            try
            {
                tbl_VehicleRegistration Update = DB.tbl_VehicleRegistrations.Single(x => x.ID == ID);
                Update.Supplier = Supplier;
                Update.OtherSupplier = OtherSupplier;
                Update.VehicleModal = VehicleModal;
                Update.ModalYear = ModalYear;
                Update.NoPlate = NoPlate;
                Update.RegistrationDate = RegistrationDate;
                Update.RegistrationValidity = RegistrationValidity;
                Update.InsuranceNo = InsuranceNo;
                Update.InsuranceDueDate = InsuranceDueDate;
                Update.MaintenanceDue = MaintenanceDue;
                Update.Amenities = Amenities;
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
        [WebMethod(EnableSession = true)]
        public string AddVehicle(string Supplier, string OtherSupplier, Int64 VehicleModal, string ModalYear, string NoPlate, string RegistrationDate, string RegistrationValidity, string InsuranceNo, string InsuranceDueDate, string MaintenanceDue, string Amenities)
        {
            string json = "";
            try
            {
              CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                Int64 UserId = objGlobalDefault.sid;
                tbl_VehicleRegistration Add = new tbl_VehicleRegistration();
                Add.Supplier = Supplier;
                Add.OtherSupplier = OtherSupplier;
                Add.VehicleModal = VehicleModal;
                Add.ModalYear = ModalYear;
                Add.NoPlate = NoPlate;
                Add.RegistrationDate = RegistrationDate;
                Add.RegistrationValidity = RegistrationValidity;
                Add.InsuranceNo = InsuranceNo;
                Add.InsuranceDueDate = InsuranceDueDate;
                Add.MaintenanceDue = MaintenanceDue;
                Add.Amenities = Amenities;
                Add.ParentID = UserId;
                DB.tbl_VehicleRegistrations.InsertOnSubmit(Add);
                DB.SubmitChanges();

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        //[WebMethod(true)]
        //public string SearchQuotationDetail(string CompanyName)
        //{
        //    string json = "";
        //    try
        //    {
        //        var List = (from obj in DB.tbl_VehicleRegistrations where obj.ID.Contains(CompanyName) select obj).ToList();
        //        json = objSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
        //    }
        //    catch
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;
        //}

        //[WebMethod(EnableSession = true)]
        //public string GetAgentDetail()
        //{
        //    JavaScriptSerializer objSerializer = new JavaScriptSerializer();
        //    objSerializer.MaxJsonLength = int.MaxValue;
        //    string json = "";
        //    try
        //    {
        //        var List = (from obj in DB.tbl_AdminLogins
        //                    select new
        //                    {
        //                        obj.ID,
        //                        obj.AgencyName,
        //                    }).ToList().Distinct();
        //        json = objSerializer.Serialize(new { Session = 1, retCode = 1, List_Agent = List });
        //    }
        //    catch
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;

        //}


        //[WebMethod(EnableSession = true)]
        //public string GetAgencyDetails(Int64 AgentID)
        //{
        //    TransfersHelperDataContext DB = new TransfersHelperDataContext();
        //    JavaScriptSerializer objSerializer = new JavaScriptSerializer();
        //  CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
        //    try
        //    {
        //        var arrAgency = (from obj in DB.tbl_AdminLogins
        //                         from objContact in DB.tbl_Contacts

        //                         where obj.ContactID == objContact.ContactID &&
        //                         obj.ID == AgentID
        //                         select new
        //                         {
        //                             obj.ID,
        //                             obj.AgencyName,
        //                             obj.Agentuniquecode,
        //                             obj.ContactPerson,
        //                             obj.Designation,
        //                             objContact.Address,
        //                             Description = (from objCity in DB.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Description,
        //                             Country = (from objCity in DB.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Countryname,
        //                             objContact.PinCode,
        //                             obj.uid,
        //                             obj.password,

        //                             objContact.phone,
        //                             objContact.email,
        //                             objContact.Mobile,
        //                             objContact.Fax,
        //                             objContact.Website,
        //                             obj.IATANumber,
        //                             obj.UserType,
        //                             obj.dtLastAccess,
        //                             obj.EnableCheckAccount,
        //                             obj.Updatedate,
        //                             obj.LoginFlag,
        //                             obj.CurrencyCode,
        //                             obj.ParentID,
        //                             obj.agentCategory,
        //                             obj.PANNo,
        //                             obj.GSTNumber,

        //                         }).FirstOrDefault();



        //        return objSerializer.Serialize(new { retCode = 1, arrAgency = arrAgency });
        //    }
        //    catch
        //    {
        //        return objSerializer.Serialize(new { });
        //    }
        //}

        [WebMethod(EnableSession = true)]
        public string Search(string Supplier, string VehicleBrand, string RegistrationDate)
        {
            DataTable VehicleList = (DataTable)Session["dt_VechileReg"];
            DataRow[] row = null;
            try
            {
                if (Supplier != "")
                {
                    row = VehicleList.Select("Supplier like '%" + Supplier + "%'");
                    if (row.Length != 0)
                        VehicleList = row.CopyToDataTable();
                    //else
                    //    VehicleList.Rows.Clear();

                }
                if (VehicleBrand != "")
                {
                    row = VehicleList.Select("VehicleBrand like '%" + VehicleBrand + "%'");
                    if (row.Length != 0)
                        VehicleList = row.CopyToDataTable();
                    //else
                    //    VehicleList.Rows.Clear();
                }
                if (RegistrationDate != "")
                {
                    row = VehicleList.Select("RegistrationDate like '%" + RegistrationDate + "%'");
                    if (row.Length != 0)
                        VehicleList = row.CopyToDataTable();
                    //else
                    //    VehicleList.Rows.Clear();
                }
               
                Session["VehicleSearch"] = VehicleList;
                List<Dictionary<string, object>> arrVehicle = JsonStringManager.ConvertDataTable(VehicleList);
                VehicleList.Dispose();
                return objSerializer.Serialize(new { retCode = 1, List_Vehicle = arrVehicle });
            }
            catch(Exception)
            {
                return objSerializer.Serialize(new { retCode = 0 });
            }

        }

        [WebMethod]
        public string DeleteVehicleByID(Int64 ID)
        {
            string json = "";
            try
            {

                tbl_VehicleRegistration Delete = DB.tbl_VehicleRegistrations.Single(x => x.ID == ID);
                DB.tbl_VehicleRegistrations.DeleteOnSubmit(Delete);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
    }

    public class AmenityList
    {
        public Int64 id { get; set; }
        public string Aminty { get; set; }
    }
}