﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for VehicleMasterHandller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class VehicleMasterHandller : System.Web.Services.WebService
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        TransfersHelperDataContext DB = new TransfersHelperDataContext();
        string json = "";

        #region Add Update Vehicle Type
        [WebMethod(true)]
        public string GetVehicleType()
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            try
            {
                using (var DB = new TransfersHelperDataContext())
                {
                    var List = (from obj in DB.tbl_VechileTypes where obj.ParentID==AccountManager.GetAdminByLogin() select obj).Distinct().ToList();

                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, tbl_VehicleType = List });


                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
       
        [WebMethod(true)]
        public string AddVehicleType(string VehicleTypename)
        {

            try
            {
                using (var DB = new TransfersHelperDataContext())
                {
                  CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                    Int64 UserId = objGlobalDefault.sid;
                    string Franchisee = objGlobalDefault.Franchisee;
                    tbl_VechileType obj = new tbl_VechileType();
                    obj.VehicleBrand = VehicleTypename;
                    obj.ParentID = UserId;

                    DB.tbl_VechileTypes.InsertOnSubmit(obj);
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }
        [WebMethod(true)]
        public string UpdateVehicleType(Int64 ID, string VehicleTypename)
        {

            try
            {
                using (var DB = new TransfersHelperDataContext())
                {
                  CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                    Int64 UserId = objGlobalDefault.sid;
                    string Franchisee = objGlobalDefault.Franchisee;
                    tbl_VechileType updVehicleType = DB.tbl_VechileTypes.Single(y => y.ID == ID);
                    updVehicleType.VehicleBrand = VehicleTypename;
                   
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }
        [WebMethod(true)]
        public string DeleteVehicleType(Int64 ID)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            //dbHotelTransfersHelperDataContext DB = new dbHotelTransfersHelperDataContext();
            string jsonString = "";

            try
            {
                using (var DB = new TransfersHelperDataContext())
                {
                    tbl_VechileType Delete = DB.tbl_VechileTypes.Single(x => x.ID == ID);
                    DB.tbl_VechileTypes.DeleteOnSubmit(Delete);
                    DB.SubmitChanges();
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";

                    //tbl_Contact Condlt = DB.tbl_Contacts.Single(y => y.ContactID == contactId);
                    //DB.tbl_Contacts.DeleteOnSubmit(Condlt);
                    //DB.SubmitChanges();
                    //jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #region Add Update Vehicle Model
        [WebMethod(true)]
        public string GetVehicleModel()
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            try
            {
                using (var DB = new TransfersHelperDataContext())
                {
                    //var List = (from obj in DB.tbl_VehicleModels select obj).Distinct().ToList();
                    var List = (from obj in DB.tbl_VehicleModels
                                join objVtype in DB.tbl_VechileTypes on obj.VehicleType equals objVtype.ID
                                where objVtype.ParentID==AccountManager.GetAdminByLogin()
                                select new
                                {
                                    obj.ID,
                                    obj.VehicleType,
                                    objVtype.VehicleBrand,
                                    obj.SeattingCapacity,
                                    obj.BaggageCapacity,

                                }).Distinct().ToList();



                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, tbl_VehicleModel = List });


                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(true)]
        public string AddVehicleModel(Int64 VehicleType, String SeatingCapacity, String BaggageCapacity)
        {

            try
            {
                using (var DB = new TransfersHelperDataContext())
                {
                  CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                    Int64 UserId = objGlobalDefault.sid;
                    string Franchisee = objGlobalDefault.Franchisee;
                    tbl_VehicleModel obj = new tbl_VehicleModel();
                    obj.VehicleType = VehicleType;
                    obj.SeattingCapacity = SeatingCapacity.ToString();
                    obj.BaggageCapacity = BaggageCapacity.ToString();
                    DB.tbl_VehicleModels.InsertOnSubmit(obj);
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }
        [WebMethod(true)]
        public string UpdateVehicleModel(Int64 ID, Int64 VehicleType, String SeatingCapacity, String BaggageCapacity)
        {

            try
            {
                using (var DB = new TransfersHelperDataContext())
                {
                  CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                    Int64 UserId = objGlobalDefault.sid;
                    string Franchisee = objGlobalDefault.Franchisee;
                    tbl_VehicleModel updVmodel = DB.tbl_VehicleModels.Single(y => y.ID == ID);
                    updVmodel.VehicleType = VehicleType;
                    updVmodel.SeattingCapacity = SeatingCapacity.ToString();
                    updVmodel.BaggageCapacity = BaggageCapacity.ToString();
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }
        [WebMethod(true)]
        public string DeleteVehicleModel(Int64 ID)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            //dbHotelTransfersHelperDataContext DB = new dbHotelTransfersHelperDataContext();
            string jsonString = "";

            try
            {
                using (var DB = new TransfersHelperDataContext())
                {
                    tbl_VehicleModel Delete = DB.tbl_VehicleModels.Single(x => x.ID == ID);
                    DB.tbl_VehicleModels.DeleteOnSubmit(Delete);
                    DB.SubmitChanges();
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #region Add Update Vehicle Amenities
        [WebMethod(true)]
        public string GetVehicleAmenities()
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            try
            {
                using (var DB = new TransfersHelperDataContext())
                {
                    var List = (from obj in DB.tbl_VehicleAmenities select obj).Distinct().ToList();

                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, tbl_VehicleAmenities = List });


                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(true)]
        public string AddVehicleAmenity(string VehicleAmenityname)
        {

            try
            {
                using (var DB = new TransfersHelperDataContext())
                {
                  CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                    Int64 UserId = objGlobalDefault.sid;
                    string Franchisee = objGlobalDefault.Franchisee;
                    tbl_VehicleAmenity obj = new tbl_VehicleAmenity();
                    obj.Amenity = VehicleAmenityname;
                    obj.ParentID = UserId;

                    DB.tbl_VehicleAmenities.InsertOnSubmit(obj);
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }
        [WebMethod(true)]
        public string UpdateVehicleAmenity(Int64 ID, string VehicleAmenityname)
        {

            try
            {
                using (var DB = new TransfersHelperDataContext())
                {
                  CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                    Int64 UserId = objGlobalDefault.sid;
                    string Franchisee = objGlobalDefault.Franchisee;
                    tbl_VehicleAmenity updVehicleType = DB.tbl_VehicleAmenities.Single(y => y.ID == ID);
                    updVehicleType.Amenity = VehicleAmenityname;

                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }
        [WebMethod(true)]
        public string DeleteVehicleAmenity(Int64 ID)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            //dbHotelTransfersHelperDataContext DB = new dbHotelTransfersHelperDataContext();
            string jsonString = "";

            try
            {
                using (var DB = new TransfersHelperDataContext())
                {
                    tbl_VehicleAmenity Delete = DB.tbl_VehicleAmenities.Single(x => x.ID == ID);
                    DB.tbl_VehicleAmenities.DeleteOnSubmit(Delete);
                    DB.SubmitChanges();
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";

                    //tbl_Contact Condlt = DB.tbl_Contacts.Single(y => y.ContactID == contactId);
                    //DB.tbl_Contacts.DeleteOnSubmit(Condlt);
                    //DB.SubmitChanges();
                    //jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion

        #region Add Update Customer Support Details
        [WebMethod(true)]
        public string GetContactDetailByContactType(Int64 sid)
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            try
            {
                using (var DB = new helperDataContext())
                {
                    var List = (from obj in DB.tbl_WebContact1s
                                join objCounty in DB.tbl_HCities on obj.ContactNationality equals objCounty.Country
                                where obj.sid == sid && obj.PareniID == AccountManager.GetUserByLogin()
                                select new
                                {
                                    obj.sid,
                                    obj.ContactType,
                                    obj.ContactNo,
                                    obj.ContactEmail,
                                    objCounty.Countryname,
                                    objCounty.Country,


                                }).Distinct().ToList();

                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, tbl_ContactList = List });
                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(true)]
        public string GetAllCPContactList()
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            try
            {
                List<object> arrContacts = new List<object>();
                using (var DB = new helperDataContext())
                {
                   
                    foreach (var ContactType in DB.tbl_WebContacts.Where(x=>x.PareniID==AccountManager.GetAdminByLogin()).Select(d=>d.ContactType).Distinct().ToList())
                    {
                        arrContacts.Add(new
                        {
                            ContactType= ContactType,
                            objContacts = (from obj in DB.tbl_WebContacts
                                           where obj.ContactType == ContactType 
                                           select obj).ToList()
                        });
                    }
                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, tbl_ContactList = arrContacts });
                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(true)]
        public string AddCustomerSupportDetails(List<tbl_WebContact1> arrContacts)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    arrContacts.ForEach(d => d.PareniID = AccountManager.GetAdminByLogin());
                    DB.tbl_WebContact1s.InsertAllOnSubmit(arrContacts);
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }
        [WebMethod(true)]
        public string UpdateCustomerSupportDetails(List<tbl_WebContact1> arrContacts)
        {
            try
            {
                DeleteCustomerSupportDetails(arrContacts[0].ContactType);
                AddCustomerSupportDetails(arrContacts);
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }
        [WebMethod(true)]
        public string DeleteCustomerSupportDetails(string ContactType)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            //dbHotelTransfersHelperDataContext DB = new dbHotelTransfersHelperDataContext();
            string jsonString = "";

            try
            {
                using (var DB = new helperDataContext())
                {
                    var arrDelete = DB.tbl_WebContact1s.Where(x => x.ContactType == ContactType).ToList();
                    DB.tbl_WebContact1s.DeleteAllOnSubmit(arrDelete);
                    DB.SubmitChanges();
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        //[WebMethod(true)]
        //public string GetAllCPContactList(string )
        //{
        //    string jsonString = "";
        //    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        //    try
        //    {
        //        List<object> arrContacts = new List<object>();
        //        using (var DB = new helperDataContext())
        //        {
        //            foreach (var ContactType in DB.tbl_WebContacts.Select(d => d.ContactType).Distinct().ToList())
        //            {
        //                arrContacts.Add(new
        //                {
        //                    ContactType = ContactType,
        //                    objContacts = (from obj in DB.tbl_WebContacts where obj.ContactType == ContactType select obj).ToList()
        //                });
        //            }
        //            jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, tbl_ContactList = arrContacts });
        //        }
        //    }
        //    catch (Exception)
        //    {

        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return jsonString;
        //}

        #endregion
    }
}