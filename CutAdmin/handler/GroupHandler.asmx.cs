﻿using CutAdmin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for GroupHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class GroupHandler : System.Web.Services.WebService
    {
        public string GetGroup()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                var Arr = GroupService.GetGroup();
                return jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = Arr });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
        }
    }
}
