﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for UploadDocument
    /// </summary>
    public class UploadDocument : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string path = System.Convert.ToString(context.Request.QueryString["path"]);
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string myFilePath = file.FileName;
                    string ext = Path.GetExtension(myFilePath);
                    string FileName = myFilePath;
                    if (!Directory.Exists(context.Server.MapPath("/" + path + "/")))
                    {
                        Directory.CreateDirectory(context.Server.MapPath("/" + path + "/"));
                    }
                    FileName = Path.Combine(context.Server.MapPath("/"+ path + "/"), myFilePath);
                    file.SaveAs(FileName);
                    context.Response.Write("1");
                }
            }
            else
            {
                context.Response.Write("0");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}