﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Services;
using System.Web.Script.Serialization;
using CutAdmin.EntityModal;
using CutAdmin.DataLayer;
namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for ExchangeRateHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ExchangeRateHandler : System.Web.Services.WebService
    {
        string json = "";
        CutAdmin.HotelAdmin.Handler.ExchangeRateHandler obj = new HotelAdmin.Handler.ExchangeRateHandler();
        JavaScriptSerializer objSerializer = new JavaScriptSerializer();

        #region Exchange Rate
        [WebMethod(EnableSession = true)]
        public string GetExchangeLog()
        {
            objSerializer = new JavaScriptSerializer();
            try
            {
                using (var db = new Trivo_AmsHelper())
                {
                    var arrCodes = db.tbl_CurrencyCode;
                    var arrExchange = CutAdmin.Common.Common.Exchange;
                    return objSerializer.Serialize(new { retCode = 1, sLastLogs = arrExchange, arrCurrences = arrCodes });
                }
            }
            catch (Exception ex)
            {
                return objSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        #endregion

        #region Exchange
        [WebMethod(EnableSession = true)]
        public string GetExchange()
        {
            try
            {
                if (AccountManager.CheckSession)
                    return objSerializer.Serialize(new { retCode = 1, arrCurrency = CutAdmin.Common.Common.Exchange });
                else
                    throw new Exception("Session Expired");
            }
            catch (Exception ex)
            {
                return objSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        #endregion

        #region Search Exchange Rate
        [WebMethod(EnableSession = true)]
        public string SearchExchangeUpdate(string currency, string updatedBy, string updateDate)
        {
            return json=obj.SearchExchangeUpdate( currency, updatedBy, updateDate);
        }
         #endregion

        #region Add Update
        [WebMethod(EnableSession = true)]
        public string GetExchangeRate(decimal[] ExchangeRate, decimal[] MarkupAmt, decimal[] MarkupPer)
        {
            return json = obj.GetExchangeRate( ExchangeRate, MarkupAmt, MarkupPer);
        }

        [WebMethod(EnableSession = true)]
        public string UpdateExchangeRate(Int64  ID, float Rate, float  Markup)
        {
            objSerializer = new JavaScriptSerializer();
            try
            {
                using (var db = new Trivo_AmsHelper())
                {
                    var arrRate = (from obj in db.tbl_UserCurrency where obj.ID == ID select obj).FirstOrDefault();
                    arrRate.Markup = Markup;
                    db.SaveChanges();
                }
                return objSerializer.Serialize(new { retCode = 1});
            }
            catch (Exception ex)
            {
                return objSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        [WebMethod(EnableSession = true)]
        public string AddExchangeRate(tbl_UserCurrency arrRate)
        {
            objSerializer = new JavaScriptSerializer();
            try
            {
                using (var db = new Trivo_AmsHelper())
                {
                    if (db.tbl_UserCurrency.Where(D => D.CurrencyID == arrRate.CurrencyID && D.ParentID==AccountManager.GetSuperAdminID()).FirstOrDefault() != null)
                        throw new Exception("Currency Already Added.");
                    arrRate.UpdateBy = AccountManager.GetUserMailByAdmin();
                    arrRate.UpdateDate = DateTime.Now.ToString("dd-MM-yyyy hh:mm");
                    arrRate.ParentID = AccountManager.GetSuperAdminID();
                    db.tbl_UserCurrency.Add(arrRate);
                    db.SaveChanges();
                }
                return objSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                return objSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }

        #endregion

        #region Update Excnage from Online
        [WebMethod(EnableSession = true)]
        public string GetOnlineRate()
        {
            return json = obj.GetOnlineRate();
        }
        #endregion
    }
}
