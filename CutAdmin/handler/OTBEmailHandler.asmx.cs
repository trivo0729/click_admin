﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for OTBEmailHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class OTBEmailHandler : System.Web.Services.WebService
    {
        #region Update Otb Status
        [WebMethod(true)]
        public string UpdateConformation(string RefrenceNo, Int64 UserId, string Status)
        {
            DBHelper.DBReturnCode retCode = OTBEmailManager.UpdateOtbStatus(RefrenceNo, Status);
            if (retCode == DBHelper.DBReturnCode.SUCCESS && Status == "Otb Updated")
            {
                retCode = OTBEmailManager.ConformationEmailToAgent(RefrenceNo, UserId);
                return "{\"Session\":\"1\",\"retCsode\":\"1\",\"MailSend\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCsode\":\"1\",\"MailSend\":\"0\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        #endregion

        #region Mails TO Airlines
        [WebMethod(true)]
        public string AirlinesMails(string RefrenceNo, Int64 UserId)
        {
            DBHelper.DBReturnCode retCode = OTBEmailManager.AirlinesOtbMail(RefrenceNo, UserId);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                //retCode = OTBEmailManager.ConformationEmailToAgent(RefrenceNo, UserId);
                return "{\"Session\":\"1\",\"retCsode\":\"1\",\"MailSend\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCsode\":\"1\",\"MailSend\":\"0\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        #endregion
    }
}
