﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CUT.DataLayer;
using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
using CutAdmin.Models;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for PackageHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class PackageHandler : System.Web.Services.WebService
    {
        //DBHandlerDataContext db = new DBHandlerDataContext();
        JavaScriptSerializer objSerialize = new JavaScriptSerializer();
        string jsonString = "";
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        //static List<string> arrCategory = new List<string> { "Standard", "Deluxe", "Premium", "Luxury" };
        //[WebMethod(EnableSession = true)]
        //public string BookingList()
        //{

        //    using (var DB=new helperDataContext())
        //    {
        //        try
        //        {
        //            IQueryable<PackageReservation> List = from Reserve in DB.tbl_PackageReservations
        //                                                   join Package in DB.tbl_Packages on Reserve.PackageID equals Package.nID
        //                                                   join Agency in DB.tbl_AdminLogins on Reserve.uid equals Agency.sid
        //                                                   where Reserve.ParentId == AccountManager.GetAdminByLogin()
        //                                                   select new PackageReservation
        //                                                   {
        //                                                       Supplier = Agency.AgencyName,
        //                                                       TravelDate= Reserve.TravelDate,
        //                                                       PaxName=Reserve.LeadingPax,
        //                                                       PackageName= Package.sPackageName,
        //                                                       CatID = arrCategory[Convert.ToInt32(Reserve.CategoryId) - 1],
        //                                                       Location = Package.sPackageDestination,
        //                                                       StartDate = Reserve.StartFrom,
        //                                                       EndDate = Reserve.EndDate,
        //                                                   };
        //            return objSerialize.Serialize(new { retCode = 1, Session = 1, List = List });
        //        }
        //        catch (Exception ex)
        //        {
        //            return objSerialize.Serialize(new { retCode = 0, Session = 1, message = ex.Message });
        //        }
        //    }

        //}
        [WebMethod(EnableSession = true)]
        public void BookingList()
        {

            using (var DB = new helperDataContext())
            {
                objSerialize.MaxJsonLength = int.MaxValue;
                try
                {
                    int TotalCount = 0;
                    var Packagelist = BookingListManager.GetPackageBooking(out TotalCount);
                    var OrderList = Packagelist.Select(o => new {
                        o.Supplier,
                        o.TravelDate,
                        o.PaxName,
                        o.PackageName,
                        o.CatID,
                        o.Location,
                        o.StartDate,
                        o.EndDate
                    });
                    
                    object finalresult = DTResult.GetDataTable(OrderList, TotalCount);
                }
                catch (Exception ex)
                {
                   
                }
            }

        }


        //public string SearchByList(string Agency, string StartDate, string EndDate, string PassengerName, string BookingDate, string PackageName, string PackageType, string Location)
        //{

        //    DataTable dtResult = (DataTable)Session["dtPackages"];
        //    Int32 Get = 20;
        //    string jsonString = "";
        //    JavaScriptSerializer objSerializer = new JavaScriptSerializer();
        //    DataTable dtTable = dtResult.Clone();
        //    foreach (DataRow dr in dtResult.Rows)
        //    {

        //        dtTable.ImportRow(dr);

        //    }


        //    // DataTable SorteddtResult = dtResult.Clone();
        //    DataView myDataView = dtResult.DefaultView;
        //    //myDataView.Sort = "ID DESC";
        //    DataTable SorteddtResult = myDataView.ToTable();
        //    // Session["dtPackagesSearch"] = SorteddtResult;
        //    DataRow[] rows = null;


        //    if (StartDate != "")
        //    {
        //        rows = SorteddtResult.Select("(StartDate like '%" + StartDate + "%')");

        //        if (rows.Length != 0)
        //        {
        //            SorteddtResult = rows.CopyToDataTable();
        //        }
        //        else
        //        {
        //            SorteddtResult = null;

        //            SorteddtResult = dtResult.Clone();
        //        }
        //    }

        //    if (EndDate != "")
        //    {
        //        rows = SorteddtResult.Select("(EndDate like '%" + EndDate + "%')");
        //        if (rows.Length != 0)
        //        {
        //            SorteddtResult = rows.CopyToDataTable();
        //        }
        //        else
        //        {
        //            SorteddtResult = null;
        //            SorteddtResult = dtResult.Clone();
        //        }
        //    }

        //    if (PassengerName != "")
        //    {
        //        rows = SorteddtResult.Select("(PaxName like '%" + PassengerName + "%')");
        //        if (rows.Length != 0)
        //        {
        //            SorteddtResult = rows.CopyToDataTable();
        //        }
        //        else
        //        {
        //            SorteddtResult = null;
        //            SorteddtResult = dtResult.Clone();
        //        }
        //    }
        //    if (BookingDate != "")
        //    {
        //        rows = SorteddtResult.Select("(TravelDate like '%" + BookingDate + "%')");
        //        if (rows.Length != 0)
        //        {
        //            SorteddtResult = rows.CopyToDataTable();
        //        }
        //        else
        //        {
        //            SorteddtResult = null;
        //            SorteddtResult = dtResult.Clone();
        //        }
        //    }


        //    if (Location != "")
        //    {
        //        rows = SorteddtResult.Select("(Location like '%" + Location + "%')");
        //        if (rows.Length != 0)
        //        {
        //            SorteddtResult = rows.CopyToDataTable();
        //        }
        //        else
        //        {
        //            SorteddtResult = null;
        //            SorteddtResult = dtResult.Clone();
        //        }
        //    }

        //    if (PackageName != "")
        //    {
        //        rows = SorteddtResult.Select("(PackageName like '%" + PackageName + "%')");
        //        if (rows.Length != 0)
        //        {
        //            SorteddtResult = rows.CopyToDataTable();
        //        }
        //        else
        //        {
        //            SorteddtResult = null;
        //            SorteddtResult = dtResult.Clone();
        //        }
        //    }

        //    if (PackageType != "All")
        //    {
        //        rows = SorteddtResult.Select("(CatID like '%" + PackageType + "%')");
        //        if (rows.Length != 0)
        //        {
        //            SorteddtResult = rows.CopyToDataTable();
        //        }
        //        else
        //        {
        //            SorteddtResult = null;
        //            SorteddtResult = dtResult.Clone();
        //        }
        //    }

        //    if (Agency != "")
        //    {
        //        rows = SorteddtResult.Select("(Supplier like '%" + Agency + "%')");
        //        if (rows.Length != 0)
        //        {
        //            SorteddtResult = rows.CopyToDataTable();
        //        }
        //        else
        //        {
        //            SorteddtResult = null;
        //            SorteddtResult = dtResult.Clone();
        //        }
        //    }

        //    try
        //    {
        //        IEnumerable<DataRow> allButFirst = SorteddtResult.AsEnumerable().Skip(0).Take(Get);
        //        SorteddtResult = allButFirst.CopyToDataTable();
        //        jsonString = "";
        //        Session["dtPackagesSearch"] = SorteddtResult;
        //        List<Dictionary<string, object>> List_Packages = new List<Dictionary<string, object>>();

        //        List_Packages = JsonStringManager.ConvertDataTable(SorteddtResult);

        //        dtResult.Dispose();
        //        objSerializer.MaxJsonLength = Int32.MaxValue;

        //        //jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetails\":[" + jsonString.Trim(',') + "]}";
        //        SorteddtResult.Dispose();
        //        objSerializer.MaxJsonLength = Int32.MaxValue;
        //        return objSerializer.Serialize(new { retCode = 1, Session = 1, List_Packages = List_Packages, });

        //    }
        //    catch
        //    {
        //        SorteddtResult = dtTable.Clone();
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";

        //    }
        //    return jsonString;

        //}
        [WebMethod(true)]
        public void SearchByList(string Agency, string PassengerName, string BookingDate, string PackageName, string PackageType, string Location)
        {
            try
            {
                int TotalCount = 0;
                var SearchPackage = BookingListManager.Search(Agency, PassengerName, BookingDate, PackageName, PackageType, Location, out TotalCount);

                object finalresult = DTResult.GetDataTable(SearchPackage, TotalCount);

            }
            catch (Exception ex)
            {


            }

        }



        //public class Dropdowncities
        //{
        //    public string City { get; set; }
        //    public string Country { get; set; }
        //}

        //[WebMethod(EnableSession = true)]
        //public string GetDropdownCity()
        //{

        //    List<Dropdowncities> ListOfCities = new List<Dropdowncities>();
        //    try
        //    {
        //        var List = (from obj in db.tbl_Packages

        //                    select new
        //                    {
        //                        obj.sPackageDestination,
        //                        obj.nID,

        //                    }).Distinct().ToList();
        //        for (int i = 0; i < List.Count; i++)
        //        {
        //            var List1 = (from obj in db.tbl_Itineraries
        //                         where obj.nPackageID == List[i].nID

        //                         select new
        //                         {
        //                             obj.sItinerary_1,


        //                         }).Distinct().ToList();
        //            if (List1.Count != 0)
        //            {

        //                if (List1[0].sItinerary_1 != "")
        //                {
        //                    string ListCity = List[i].sPackageDestination;
        //                    string[] Cities = ListCity.Split('|');

        //                    for (int j = 0; j < Cities.Length - 1; j++)
        //                    {
        //                        if (ListOfCities.Exists(d => d.City == Cities[j]))
        //                        {


        //                        }
        //                        else
        //                        {
        //                            ListOfCities.Add(new Dropdowncities
        //                            {
        //                                City = Cities[j],

        //                            });
        //                        }

        //                    }
        //                }


        //            }

        //        }

        //        ListOfCities.GroupBy(x => x.City).Select(g => g.First()).ToList();

        //        jsonString = objSerialize.Serialize(new { Session = 1, retCode = 1, Arr = ListOfCities.Distinct() });
        //    }
        //    catch
        //    {
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return jsonString;
        //}

    }
}
