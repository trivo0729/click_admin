﻿using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.EntityModal;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for ChannelHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ChannelHandler : System.Web.Services.WebService
    {
        helperDataContext db = new helperDataContext();

        // Get Tax Details
        [WebMethod(EnableSession = true)]
        public string GetAuthority(Int64 Sid)
        {
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            objSerlizer.MaxJsonLength = Int32.MaxValue;
            try
            {
                var sForms = (from obj in db.tbl_AgentForms where obj.Type == "Form" select obj).Distinct().ToList();
                // var sAPI = (from obj in db.tbl_AgentForms where obj.Type == "Hotel" || obj.Type == "Activity" select obj).Distinct().ToList();

                var sAPI = (from obj in db.tbl_AgentForms
                            where obj.Type == "Hotel" || obj.Type == "Activity"
                            join
                            objForm in db.tbl_ApiRolls on obj.nId equals objForm.ApiId
                            select obj).Distinct().ToList();

                // API Grant All //
                //List<int> ListAgent = (from obj in db.tbl_AdminLogins where obj.UserType =="Agent" select obj.sid).ToList();

                //List<tblAgentRoleManager> List = new List<tblAgentRoleManager>();
                //foreach (var nUid in ListAgent)
                //{
                //    foreach (var nFormId in sAPI)
                //    {
                //        List.Add(new tblAgentRoleManager
                //        {
                //            nFormId = Convert.ToInt64(nFormId.nId),
                //            nUid = nUid,
                //        });
                //    }
                //}
                //db.tblAgentRoleManagers.InsertAllOnSubmit(List);
                //db.SubmitChanges();

                var AssigndForms = (from objForm in db.tbl_AgentForms
                                    from objAgent in db.tblAgentRoleManagers
                                    where objForm.nId == objAgent.nFormId && objAgent.nUid == Sid && objForm.Type == "Form"
                                    select new
                                    {
                                        objForm.sFormName,
                                        objAgent.nId
                                    });

                var AssigndAPI = (from objForm in db.tbl_AgentForms
                                  from objAgent in db.tblAgentRoleManagers
                                  from objApi in db.tbl_ApiRolls
                                  where objForm.nId == objAgent.nFormId && objAgent.nUid == Sid && objForm.Type == "Hotel" && objForm.nId == objApi.ApiId
                                  select new
                                  {
                                      objForm.sFormName,
                                      objAgent.nId
                                  });
                var sHold = (from obj in db.tbl_AdminLogins where obj.sid == Sid select obj.HoldGrant).FirstOrDefault();
                var SupplierVisible = (from obj in db.tbl_AdminLogins where obj.sid == Sid select obj.SupplierVisible).FirstOrDefault();
                return objSerlizer.Serialize(new
                {
                    retCode = 1,
                    Session = 1,
                    sForms = sForms,
                    AssigndForms = AssigndForms,
                    sAPI = sAPI,
                    AssigndAPI = AssigndAPI,
                    sHold = sHold,
                    SupplierVisible = SupplierVisible,

                });
            }
            catch
            {
                return objSerlizer.Serialize(new
                {
                    retCode = 0,
                    Session = 1
                });
            }



        }

        [WebMethod]
        public string SetFormsForAgentRole(Int64 nUid, bool SupplierVisible, bool OnHold, bool Cropping, string Type, params string[] arr)
        {
            try
            {
                List<tblAgentRoleManager> List = (from obj in db.tblAgentRoleManagers
                                                  join
                                                      objForm in db.tbl_AgentForms on obj.nFormId equals objForm.nId
                                                  where obj.nUid == nUid && objForm.Type == Type
                                                  select obj).ToList();
                db.tblAgentRoleManagers.DeleteAllOnSubmit(List);
                var AgentAuthority = (from obj in db.tbl_AdminLogins where obj.sid == nUid select obj).FirstOrDefault();
                AgentAuthority.SupplierVisible = SupplierVisible;
                AgentAuthority.HoldGrant = OnHold;
                db.SubmitChanges();
                List = new List<tblAgentRoleManager>();
                foreach (var nFormId in arr)
                {
                    List.Add(new tblAgentRoleManager
                    {
                        nFormId = Convert.ToInt64(nFormId),
                        nUid = nUid,
                    });
                }
                db.tblAgentRoleManagers.InsertAllOnSubmit(List);
                db.SubmitChanges();
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }


        public void DefaultAccess(Int64 UID)
        {
            List<tblAgentRoleManager> List = new List<tblAgentRoleManager>();
            List<tbl_AgentForm> sAPI = new List<tbl_AgentForm>();
            sAPI.Add(new tbl_AgentForm { nId = 6 });
            sAPI.Add(new tbl_AgentForm { nId = 9 });
            sAPI.Add(new tbl_AgentForm { nId = 11 });
            sAPI.Add(new tbl_AgentForm { nId = 12 });
            foreach (var nFormId in sAPI)
            {
                List.Add(new tblAgentRoleManager
                {
                    nFormId = Convert.ToInt64(nFormId.nId),
                    nUid = UID,
                });
            }
            db.tblAgentRoleManagers.InsertAllOnSubmit(List);
            db.SubmitChanges();
        }

        [WebMethod(EnableSession = true)]
        public string Get()
        {
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            try
            {

                objSerlizer.MaxJsonLength = Int32.MaxValue;

                var sAPI = (from obj in db.tbl_AgentForms where obj.Type == "Hotel" select obj).Distinct().ToList();

                var AssigndAPI = (from objForm in db.tbl_AgentForms
                                  from objAgent in db.tblAgentRoleManagers
                                  where objForm.nId == objAgent.nFormId && objAgent.nUid == 232 && objForm.Type == "Hotel"
                                  select new
                                  {
                                      objForm.sFormName,
                                      objAgent.nId
                                  });


                return objSerlizer.Serialize(new { retCode = 1, Session = 1, sAPI = sAPI, AssigndAPI = AssigndAPI, });
            }
            catch
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(EnableSession = true)]
        public string SetFormsForRole(string Type, params string[] arr)
        {
            try
            {
                List<tblAgentRoleManager> List = (from obj in db.tblAgentRoleManagers
                                                  join
                                                      objForm in db.tbl_AgentForms on obj.nFormId equals objForm.nId
                                                  where obj.nUid == 232 && objForm.Type == Type
                                                  select obj).ToList();
                db.tblAgentRoleManagers.DeleteAllOnSubmit(List);
                List = new List<tblAgentRoleManager>();
                foreach (var nFormId in arr)
                {
                    List.Add(new tblAgentRoleManager
                    {
                        nFormId = Convert.ToInt64(nFormId),
                        nUid = 232,
                    });
                }
                db.tblAgentRoleManagers.InsertAllOnSubmit(List);
                db.SubmitChanges();
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }


        #region Api Rolls for Admin
        [WebMethod(EnableSession = true)]
        public string GetAuthorityForAdmin(Int64 Sid)
        {
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            objSerlizer.MaxJsonLength = Int32.MaxValue;
            try
            {
                var sAPI = (from obj in db.tbl_AgentForms where obj.Type == "Hotel" select obj).Distinct().ToList();
                var AssigndAPI = (from objForm in db.tbl_AgentForms
                                  join Roll in db.tbl_ApiRolls on objForm.nId equals Roll.ApiId
                                  where Roll.ParentId==Sid
                                  select new
                                  {
                                      objForm.sFormName,
                                      objForm.nId
                                  });
                return objSerlizer.Serialize(new
                {
                    retCode = 1,
                    Session = 1,
                    sAPI = sAPI,
                    AssigndAPI = AssigndAPI,
                });
            }
            catch
            {
                return objSerlizer.Serialize(new
                {
                    retCode = 0,
                    Session = 1
                });
            }



        }

        [WebMethod(EnableSession = true)]
        public string AddAuthorityForAdmin(List<tbl_ApiRoll> arrayJson)
        {
            string json = "";
            try
            {
                var List = (from obj in db.tbl_ApiRolls where obj.ParentId == arrayJson[0].ParentId select obj).ToList();
                if (List.Any())
                {
                    db.tbl_ApiRolls.DeleteAllOnSubmit(List);
                    db.SubmitChanges();
                }
                db.tbl_ApiRolls.InsertAllOnSubmit(arrayJson);
                db.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string AddSupplierCredential(Int64 ssid, string IP, string URL, string UserName, string Password, string Accesskey, string Suppliercode)
        {
            string json = "";
            try
            {
                using (var db = new Trivo_AmsHelper())
                {
                    tbl_ApiCredential Add = new tbl_ApiCredential();
                    var List = (from obj in db.tbl_ApiCredential where obj.sSupplierCode == Suppliercode && obj.ParentId == ssid select obj).ToList();
                    if (List.Count == 0)
                    {
                        Add.ParentId = ssid;
                        Add.IP = IP;
                        Add.URL = URL;
                        Add.sUserName = UserName;
                        Add.sPassword = Password;
                        Add.sAccessKey = Accesskey;
                        Add.sSupplierCode = Suppliercode;
                        db.tbl_ApiCredential.Add(Add);
                        db.SaveChanges();
                        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";

                    }
                    else
                    {
                        tbl_ApiCredential update = db.tbl_ApiCredential.Single(x => x.ParentId == ssid && x.sSupplierCode == Suppliercode);
                        update.ParentId = ssid;
                        update.IP = IP;
                        update.URL = URL;
                        update.sUserName = UserName;
                        update.sPassword = Password;
                        update.sAccessKey = Accesskey;
                        update.sSupplierCode = Suppliercode;
                        db.SaveChanges();
                        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                    }

                }



            }
            catch (Exception ex)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]

        public string GetSupplierCredential(Int64 sid,string SupplierCode)
        {
            string json = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new Trivo_AmsHelper())
                {
                    var List = (from obj in DB.tbl_ApiCredential where obj.ParentId == sid && obj.sSupplierCode == SupplierCode select obj).ToList();
                    // get entity
                   // var List = DB.tbl_ApiCredential.Where(x => x.ParentId == sid).ToList();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });

                }


            }
            catch (Exception ex)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;

        }

        #endregion
    }
}
