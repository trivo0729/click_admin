﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for GSTDetailsHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class GSTDetailsHandler : System.Web.Services.WebService
    {
        helperDataContext DB = new helperDataContext();
       [WebMethod(EnableSession = true)]
        public string SaveDetails(string AgencyName, string AgencyGSTIN, string AgencyCode, string LoginName, string AgencyState, string AgencyClassification, string ProvisionalGSTNo, string AgencyStateCode, string ContactPerson, string ContactPersonPhone, string ContactPersonMobile, string sEmail, string ContactPersonMail, string GSTRegistrationStatus, string Composition, string HSNSACCode)
        {
            try
            {
                if (AccountManager.CheckSession)
                {
                    Int64 Sid = 0;
                    CUT.DataLayer.GlobalDefault objGlobalDefaults = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    if (objGlobalDefaults != null)
                        Sid = objGlobalDefaults.sid;
                    else
                        Sid = Convert.ToInt64(AgencyCode);

                    tbl_AdminLogin Admin = DB.tbl_AdminLogins.Single(x => x.sid == Sid);
                    Admin.AgencyName = AgencyName;
                    Admin.GSTNumber = AgencyGSTIN;
                    Admin.AgencyClassification = AgencyClassification;
                    Admin.ProvisonalGSTNo = ProvisionalGSTNo;
                    Admin.GSTRegistrationStatus = GSTRegistrationStatus;
                    Admin.CompositionLevy = Composition;
                    Admin.HSN_SAC_Code = HSNSACCode;
                    DB.SubmitChanges();

                    Int64 ContactID = 0;
                    ContactID = Admin.ContactID; tbl_Contact Contact = DB.tbl_Contacts.Single(x => x.ContactID == ContactID);
                    Contact.StateID = AgencyStateCode;
                    DB.SubmitChanges();

                    return "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
                else
                    throw new Exception("Session Expired");
                
            }
            catch(Exception ex)
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\",\"ex\":\""+ex.Message+"\"}";
            }
           
        }
        }

    }

