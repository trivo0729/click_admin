﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for ApiHandller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ApiHandller : System.Web.Services.WebService
    {
        string jsonString = "";
        helperDataContext db = new helperDataContext();
        JavaScriptSerializer jSerializer = new JavaScriptSerializer();
        [WebMethod(EnableSession = true)]
        public string GetAPI()
        {

            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            //var arr = (from obj in db.tbl_APIDetails select obj).ToList().Distinct().OrderBy(obj => obj.sid);
            var arr = (from obj in db.tbl_APIDetails
                       where obj.ParentID==AccountManager.GetAdminByLogin()
                       select new
                        {
                            obj.Supplier,
                            obj.Sup_Mobile,
                            obj.Sup_Phone,
                            obj.Hotel,
                            obj.Active,
                            obj.Flight,
                            obj.Activity,
                            obj.sid,
                            //obj.Id,
                        }).Distinct().ToList();

            if (arr != null)
            {

                jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, tbl_API = arr });

            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        #region API AddUpdate
        [WebMethod(EnableSession = true)]
        public string AddSupplier(string Supplier, bool Hotel, decimal HotelComm, decimal HotelTDS, decimal ActivityComm,decimal ActivityTDS, bool Visa, bool Otb, bool Flight,bool Pakages, bool Activity, bool Active, string sMobile, string sPhone, string Address, string sCompany, string sCity, string sCoutry, string sState, string sZip, string sEmail, string sFax, string oName, string oEmail, string oMobile, string rName, string rEmail, string rMobile, string rPhone, string aName, string aEmail, string aMobile, string aPhone)
        {
            //CUT.DataLayer.GlobalDefault  objGlobalDefaults = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
            //string AgencyName = objGlobalDefaults.AgencyName;
            //DBHelper.DBReturnCode retcode = APIManager.AddSupplier(Supplier, Hotel,HotelComm,HotelTDS,VisaComm,VisaTDS,OtbComm,OtbTDS,PackagesComm,PackagesTDS, Visa, Otb, Pakages, Active,  ContactPerson,  ContactNo,  Address);
            try
            {
              CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                //Int64 ParentID = objGlobalDefault.sid;
                var list = (from obj in db.tbl_GroupMarkups select obj).ToList();
                string AdminKey = ConfigurationManager.AppSettings["AdminKey"];
                string[] List = new string[] { "AmadeusGalileo", "SpiceJet", "Indigo", "GoAir", " AirArabia", "FlyDubai", "AirIndiaExpress", "AirAsia", "AirCosta" };

                tbl_APIDetail Supp = new tbl_APIDetail();
                Supp.Supplier = Supplier;
                Supp.Hotel = Hotel;
                Supp.HotelComm = HotelComm;
                Supp.HotelTDS = HotelTDS;
                Supp.ActivityComm = ActivityComm;
                Supp.ActivityTDS = ActivityTDS;
                Supp.Visa = Visa;
                Supp.Otb = Otb;
                Supp.Active = Active;
                Supp.Packages = Pakages;
                Supp.Flight = Flight;
                Supp.Activity = Activity;
                Supp.Sup_Company = sCompany;
                Supp.Sup_Address = Address;
                Supp.Sup_City = sCity;
                Supp.Sup_State = sState;
                Supp.Sup_Country = sCoutry;
                Supp.Sup_Zip = sZip;
                Supp.Sup_Email = sEmail;
                Supp.Sup_Phone = sPhone;
                Supp.Sup_Fax = sFax;
                Supp.Sup_Mobile = sMobile;
                Supp.Sup_OwnerName = oName;
                Supp.Sup_OwnerEmail = oEmail;
                Supp.Sup_ResPhone = rPhone;
                Supp.Sup_OwnerMobile = oMobile;
                Supp.Sup_ResName = rName;
                Supp.Sup_ResEmail = rEmail;
                Supp.Sup_ResMobile = rMobile;
                Supp.Sup_AccName = aName;
                Supp.Sup_AccEmail = aEmail;
                Supp.Sup_AccPhone = aPhone;
                Supp.Sup_AccMobile = aMobile;
                Supp.ParentID = Convert.ToInt64(AdminKey);
                db.tbl_APIDetails.InsertOnSubmit(Supp);
                db.SubmitChanges();

                Int64 Type = 0;
                if (Hotel == true)
                {
                    Type = 1;
                    // Insert in GlobalMarkup Table for Dynamic Design//
                    tbl_GlobalMarkup Glob = new tbl_GlobalMarkup();
                    Glob.Supplier = Supplier;
                    Glob.Type = Type;
                    Glob.MarkupAmmount = 0;
                    Glob.MarkupPercentage = 0;
                    Glob.CommessionAmmount = 0;
                    Glob.CommessionPercentage = 0;
                    db.tbl_GlobalMarkups.InsertOnSubmit(Glob);
                    db.SubmitChanges();

                    List<tbl_GroupMarkupDetail> arrDetails = new List<tbl_GroupMarkupDetail>();
                    for (int i = 0; i < list.Count; i++)
                    {
                        arrDetails.Add(new tbl_GroupMarkupDetail
                        {
                            Supplier = Supplier,
                            Type = Type,
                            MarkupAmmount = 0,
                            MarkupPercentage = 0,
                            CommessionAmmount = 0,
                            CommessionPercentage = 0,
                            GroupId = list[i].sid,
                            TaxApplicable = false,
                        });
                    }

                    db.tbl_GroupMarkupDetails.InsertAllOnSubmit(arrDetails);
                    db.SubmitChanges();
                }
                else if (Flight == true)
                {
                    Type = 7;

                    List<tbl_GlobalMarkup> arrDetailGlobal = new List<tbl_GlobalMarkup>();
                    for (int j = 0; j < List.Length; j++)
                    {
                        arrDetailGlobal.Add(new tbl_GlobalMarkup
                        {
                            Supplier = Supplier + "_" + List[j],
                            Type = Type,
                            MarkupAmmount = 0,
                            MarkupPercentage = 0,
                            CommessionAmmount = 0,
                            CommessionPercentage = 0,
                        });

                        // Insert in GlobalMarkup Table for Dynamic Design//
                        //tbl_GlobalMarkup Glob = new tbl_GlobalMarkup();
                        //Glob.Supplier = Supplier +"_"+ List[j];
                        //Glob.Type = Type;
                        //Glob.MarkupAmmount = 0;
                        //Glob.MarkupPercentage = 0;
                        //Glob.CommessionAmmount = 0;
                        //Glob.CommessionPercentage = 0;
                        //db.tbl_GlobalMarkups.InsertOnSubmit(Glob);
                        //db.SubmitChanges();
                    }
                    db.tbl_GlobalMarkups.InsertAllOnSubmit(arrDetailGlobal);
                    db.SubmitChanges();

                    // Insert in GroupMarkup Table for Dynamic Design //
                    List<tbl_GroupMarkupDetail> arrDetails = new List<tbl_GroupMarkupDetail>();
                    for (int k = 0; k < List.Length; k++)
                    {
                        for (int i = 0; i < list.Count; i++)
                        {

                            arrDetails.Add(new tbl_GroupMarkupDetail
                            {
                                Supplier = Supplier + "_" + List[k],
                                Type = Type,
                                MarkupAmmount = 0,
                                MarkupPercentage = 0,
                                CommessionAmmount = 0,
                                CommessionPercentage = 0,
                                GroupId = list[i].sid,
                                TaxApplicable = false,
                            });
                        }
                    }

                    db.tbl_GroupMarkupDetails.InsertAllOnSubmit(arrDetails);
                    db.SubmitChanges();

                }
                //else if (Visa == true)
                //{
                //    Type =3;
                //    tbl_GlobalMarkup Glob = new tbl_GlobalMarkup();
                //    Glob.Supplier = Supplier ;
                //    Glob.Type = Type;
                //    Glob.MarkupAmmount = 0;
                //    Glob.MarkupPercentage = 0;
                //    Glob.CommessionAmmount = 0;
                //    Glob.CommessionPercentage = 0;
                //    db.tbl_GlobalMarkups.InsertOnSubmit(Glob);
                //    db.SubmitChanges();
                //}
                //else if (Otb == true)
                //{
                //    Type =2;
                //    tbl_GlobalMarkup Glob = new tbl_GlobalMarkup();
                //    Glob.Supplier = Supplier;
                //    Glob.Type = Type;
                //    Glob.MarkupAmmount = 0;
                //    Glob.MarkupPercentage = 0;
                //    Glob.CommessionAmmount = 0;
                //    Glob.CommessionPercentage = 0;
                //    db.tbl_GlobalMarkups.InsertOnSubmit(Glob);
                //    db.SubmitChanges();
                //}
                else if (Pakages == true)
                {
                    Type = 6;
                    tbl_GlobalMarkup Glob = new tbl_GlobalMarkup();
                    Glob.Supplier = Supplier;
                    Glob.Type = Type;
                    Glob.MarkupAmmount = 0;
                    Glob.MarkupPercentage = 0;
                    Glob.CommessionAmmount = 0;
                    Glob.CommessionPercentage = 0;
                    db.tbl_GlobalMarkups.InsertOnSubmit(Glob);
                    db.SubmitChanges();
                    tbl_BookingCondition Bcon = new tbl_BookingCondition();

                    Bcon.Supplier = Supplier;
                    Bcon.Type = Type.ToString();
                    Bcon.ConditionalTime = "";
                    Bcon.ParentID = objGlobalDefault.sid;
                    db.tbl_BookingConditions.InsertOnSubmit(Bcon);
                    db.SubmitChanges();
                }
                
                else if (Activity == true)
                {
                    Type = 5;
                    tbl_GlobalMarkup Glob = new tbl_GlobalMarkup();
                    Glob.Supplier = Supplier;
                    Glob.Type = Type;
                    Glob.MarkupAmmount = 0;
                    Glob.MarkupPercentage = 0;
                    Glob.CommessionAmmount = 0;
                    Glob.CommessionPercentage = 0;
                    db.tbl_GlobalMarkups.InsertOnSubmit(Glob);
                    db.SubmitChanges();

                    tbl_BookingCondition Bcon = new tbl_BookingCondition();
                    Bcon.Supplier = Supplier;
                    Bcon.Type = Type.ToString();
                    Bcon.ConditionalTime = "";
                    Bcon.ParentID = objGlobalDefault.sid;
                    db.tbl_BookingConditions.InsertOnSubmit(Bcon);
                    db.SubmitChanges();

                }




                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"SupplierId\":\"" + Supp.sid + "\"}";
            }
            catch (Exception ex)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateSupplier(Int64 sid, string Supplier, bool Hotel, decimal HotelComm, decimal HotelTDS,decimal ActivityComm,decimal ActivityTDS, bool Visa, bool Otb, bool Flight,bool Pakages, bool Active, bool Activity ,  string sMobile, string sPhone, string Address, string sCompany, string sCity, string sCoutry, string sState, string sZip, string sEmail, string sFax, string oName, string oEmail, string oMobile, string rName, string rEmail, string rMobile, string rPhone, string aName, string aEmail, string aMobile, string aPhone)
        {
            try
            {
                
                Int64 ParentID = AccountManager.GetAdminByLogin();
                CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                tbl_APIDetail Supp = db.tbl_APIDetails.Single(x => x.sid == sid);

                Supp.Sup_Address = Address;
                Supp.Sup_City = sCity;
                Supp.Sup_Company = sCompany;
                Supp.Sup_Country = sCoutry;
                Supp.Sup_Email = sEmail;
                Supp.Sup_Fax = sFax;
                Supp.Sup_Mobile = sMobile;
                Supp.Sup_Phone = sPhone;
                Supp.Sup_State = sState;
                Supp.Sup_Zip = sZip;
                Supp.Sup_OwnerEmail = oEmail;
                Supp.Sup_OwnerMobile = oMobile;
                Supp.Sup_OwnerName = oName;
                Supp.Sup_ResEmail = rEmail;
                Supp.Sup_ResMobile = rMobile;
                Supp.Sup_ResName = rName;
                Supp.Sup_ResPhone = rPhone;
                Supp.Sup_AccEmail = aEmail;
                Supp.Sup_AccMobile = aMobile;
                Supp.Sup_AccName = aName;
                Supp.Sup_AccPhone = aPhone;
                Supp.Supplier = Supplier;
                Supp.Visa = Visa;
                Supp.Active = Active;
                Supp.Hotel = Hotel;
                Supp.HotelComm = HotelComm;
                Supp.HotelTDS = HotelTDS;
                Supp.ActivityComm = ActivityComm;
                Supp.ActivityTDS = ActivityTDS;
                Supp.Otb = Otb;
                Supp.Activity = Activity;
                Supp.Packages = Pakages;
                Supp.Flight = Flight;
                db.SubmitChanges();
                Int64 Type = 0;

                // please do not uncomment this code it is check with test database// Tausif 08-02-2019
                //if (Hotel == true)
                //{
                //    Type = 1;
                //    var list = (from obj in db.tbl_GroupMarkups select obj).ToList();
                //    List<tbl_GroupMarkupDetail> arrDetails = new List<tbl_GroupMarkupDetail>();
                //    for (int i = 0; i < list.Count; i++)
                //    {
                //        var LisGrpMark = (from obj in db.tbl_GroupMarkupDetails where obj.Supplier == Supplier && obj.Type == Type && obj.GroupId== list[i].sid select obj).ToList();
                //        if (LisGrpMark.Count==0)
                //        {
                //            arrDetails.Add(new tbl_GroupMarkupDetail
                //            {
                //                Supplier = Supplier,
                //                Type = Type,
                //                MarkupAmmount = 0,
                //                MarkupPercentage = 0,
                //                CommessionAmmount = 0,
                //                CommessionPercentage = 0,
                //                GroupId = list[i].sid,
                //                TaxApplicable = false,
                //            });
                //        }
                //    }
                //    db.tbl_GroupMarkupDetails.InsertAllOnSubmit(arrDetails);
                //    db.SubmitChanges();

                //    var LisHotelGlobal = (from obj in db.tbl_GlobalMarkups where obj.Supplier == Supplier && obj.Type == Type select obj).ToList();
                //    if (LisHotelGlobal.Count==0)
                //    {
                //        tbl_GlobalMarkup Glob = new tbl_GlobalMarkup();
                //        Glob.Supplier = Supplier;
                //        Glob.Type = Type;
                //        Glob.MarkupAmmount = 0;
                //        Glob.MarkupPercentage = 0;
                //        Glob.CommessionAmmount = 0;
                //        Glob.CommessionPercentage = 0;
                //        db.tbl_GlobalMarkups.InsertOnSubmit(Glob);
                //        db.SubmitChanges();
                //    }
                //}

                //if (Flight == true)
                //{
                //    Type = 7;
                //    var list = (from obj in db.tbl_GroupMarkups select obj).ToList();
                //    List<tbl_GroupMarkupDetail> arrDetails = new List<tbl_GroupMarkupDetail>();
                //    for (int i = 0; i < list.Count; i++)
                //    {
                //        var LisGrpMark = (from obj in db.tbl_GroupMarkupDetails where obj.Supplier == Supplier && obj.Type == Type && obj.GroupId == list[i].sid select obj).ToList();
                //        if (LisGrpMark.Count == 0)
                //        {
                //            arrDetails.Add(new tbl_GroupMarkupDetail
                //            {
                //                Supplier = Supplier,
                //                Type = Type,
                //                MarkupAmmount = 0,
                //                MarkupPercentage = 0,
                //                CommessionAmmount = 0,
                //                CommessionPercentage = 0,
                //                GroupId = list[i].sid,
                //                TaxApplicable = false,
                //            });
                //        }
                //    }
                //    db.tbl_GroupMarkupDetails.InsertAllOnSubmit(arrDetails);
                //    db.SubmitChanges();

                //    var ListFlightGlobal = (from obj in db.tbl_GlobalMarkups where obj.Supplier == Supplier && obj.Type == Type select obj).ToList();
                //    if (ListFlightGlobal.Count == 0)
                //    {
                //        tbl_GlobalMarkup Glob = new tbl_GlobalMarkup();
                //        Glob.Supplier = Supplier;
                //        Glob.Type = Type;
                //        Glob.MarkupAmmount = 0;
                //        Glob.MarkupPercentage = 0;
                //        Glob.CommessionAmmount = 0;
                //        Glob.CommessionPercentage = 0;
                //        db.tbl_GlobalMarkups.InsertOnSubmit(Glob);
                //        db.SubmitChanges();
                //    }
                //}


                if (Pakages == true)
                {
                    Type = 6;
                    var LisPckGlobal= (from obj in db.tbl_GlobalMarkups where obj.Supplier== Supplier && obj.Type== Type select obj).ToList();
                    if (LisPckGlobal.Count==0)
                    {
                        tbl_GlobalMarkup Glob = new tbl_GlobalMarkup();
                        Glob.Supplier = Supplier;
                        Glob.Type = Type;
                        Glob.MarkupAmmount = 0;
                        Glob.MarkupPercentage = 0;
                        Glob.CommessionAmmount = 0;
                        Glob.CommessionPercentage = 0;
                        db.tbl_GlobalMarkups.InsertOnSubmit(Glob);
                        db.SubmitChanges();
                    }

                    var LisPckBookCon = (from obj in db.tbl_BookingConditions where obj.Supplier == Supplier && obj.Type == Type.ToString() && obj.ParentID== ParentID select obj).ToList();

                    if (LisPckBookCon.Count==0)
                    {
                        tbl_BookingCondition Bcon = new tbl_BookingCondition();
                        Bcon.Supplier = Supplier;
                        Bcon.Type = Type.ToString();
                        Bcon.ConditionalTime = "";
                        Bcon.ParentID = ParentID;
                        db.tbl_BookingConditions.InsertOnSubmit(Bcon);
                        db.SubmitChanges();
                    }
                }

                else if (Activity == true)
                {
                    Type = 5;
                    var LisGlobal = (from obj in db.tbl_GlobalMarkups where obj.Supplier == Supplier && obj.Type == Type select obj).ToList();

                    if (LisGlobal.Count == 0)
                    {
                        tbl_GlobalMarkup Glob = new tbl_GlobalMarkup();
                        Glob.Supplier = Supplier;
                        Glob.Type = Type;
                        Glob.MarkupAmmount = 0;
                        Glob.MarkupPercentage = 0;
                        Glob.CommessionAmmount = 0;
                        Glob.CommessionPercentage = 0;
                        db.tbl_GlobalMarkups.InsertOnSubmit(Glob);
                        db.SubmitChanges();
                    }

                    var LisBookCon = (from obj in db.tbl_BookingConditions where obj.Supplier == Supplier && obj.Type == Type.ToString() && obj.ParentID == ParentID select obj).ToList();

                    if (LisBookCon.Count == 0)
                    {
                        tbl_BookingCondition Bcon = new tbl_BookingCondition();
                        Bcon.Supplier = Supplier;
                        Bcon.Type = Type.ToString();
                        Bcon.ConditionalTime = "";
                        Bcon.ParentID = ParentID;
                        db.tbl_BookingConditions.InsertOnSubmit(Bcon);
                        db.SubmitChanges();
                    }
                }

                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception ex)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetSupplier(Int64 sid)
        {
            try
            {
                var SupplierDetails = (from Detail in db.tbl_APIDetails where Detail.sid == sid  select Detail).Distinct().ToList();
                if (SupplierDetails.Count()!=0)
                {
                    jsonString = jSerializer.Serialize(new { Session = 1, retCode = 1, SupplierDetails = SupplierDetails });
                }
                else
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                //jsonString =jSerializer. "{\"Session\":\"1\",\"retCode\":\"1\",\"SupplierDetails\":\"" + SupplierDetails + "\"}";
            }
            catch (Exception ex)
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        #endregion
    }
}
