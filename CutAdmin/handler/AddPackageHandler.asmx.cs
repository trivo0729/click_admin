﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.EntityModal;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for AddPackageHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AddPackageHandler : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        //  public string AddPackageBasicDetails(string sPackageName, string sCity, string sCategory, Int64 nDuration, DateTime dvalidFrom, DateTime dvalidupto, string sThemes, string sDesctiption, Decimal dTax, Int64 nCancelDays, Decimal dCancelCharge, string sTermsCondition, string sInventory)
        public string AddPackageBasicDetails(string sPackageName, string sCity, string sCategory, Int64 nDuration, string dvalidFrom, string dvalidupto, string sThemes, string sDesctiption, Decimal dTax, Int64 nCancelDays, Decimal dCancelCharge, string sTermsCondition, string sInventory, bool IncludingTransfer, bool Domestic)
        {
          CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            Int64 ParentID = AccountManager.GetAdminByLogin();
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            //Int64 nID = 0;
            object nID = 0;
            DBHelper.DBReturnCode retcode = PackageManager.AddPackageBasicDetails(sPackageName, sCity, sCategory, nDuration, dvalidFrom, dvalidupto, sThemes, sDesctiption, dTax, nCancelDays, dCancelCharge, sTermsCondition, sInventory, ParentID, out nID, IncludingTransfer, Domestic);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {

                return objSerializer.Serialize(new { retCode = 1, nID = nID });
            }
            else
            {
                return objSerializer.Serialize(new { retCode = 0 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetItineraryCounts(Int64 nID)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            DataTable dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = PackageManager.GetItineraryCounts(nID, out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return objSerializer.Serialize(new { retCode = 1, count = Convert.ToInt64(dtResult.Rows[0]["nDuration"]), CategoryCount = dtResult.Rows[0]["sPackageCategory"].ToString() });
            }
            else
            {
                return objSerializer.Serialize(new { retCode = 0 });
            }
        }


        [WebMethod(true)]
        public string GetCategoryCounts(Int64 nID)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            DataTable dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = PackageManager.GetCategoryCounts(nID, out dtResult);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return objSerializer.Serialize(new { retCode = 1, count = dtResult.Rows[0]["sPackageCategory"] });
            }
            else
            {
                return objSerializer.Serialize(new { retCode = 0 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string SaveItinerary(Int64 nID, List<String> listItinerary, Int64 nCategoryID, string sCategoryName)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            PackageManager.DBReturnCode retCode = PackageManager.SaveItinerary(nID, listItinerary, nCategoryID, sCategoryName);
            if (retCode == PackageManager.DBReturnCode.SUCCESS)
            {
                return objSerializer.Serialize(new { retCode = 1 });
            }
            else
            {
                return objSerializer.Serialize(new { retCode = 0 });
            }
        }

        [WebMethod(EnableSession = true)]
        public string SaveHotelDetails(Int64 nID, List<String> listHotelName, List<String> listHotelCode, List<String> listHotelDesc, Int64 nCategoryID, string sCategoryName)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            DBHelper.DBReturnCode retCode = PackageManager.SaveHotelDetails(nID, listHotelName, listHotelCode, listHotelDesc, nCategoryID, sCategoryName);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return objSerializer.Serialize(new { retCode = 1 });
            }
            else
            {
                return objSerializer.Serialize(new { retCode = 0 });
            }
        }
        [WebMethod(EnableSession = true)]
        public string GetDestinationCode(string name)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = DestinationManager.Get(name, "ENG", out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("DestinationCode"),
                    value = data.Field<String>("Destination")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }
        //#endregion Destination

        [WebMethod(true)]
        public string AddTransferDetails(List<tbl_PackageCab> ArrTransfer)
        {
            string json = "";
            try
            {
                string PackID = ArrTransfer[0].PackageId.ToString();
                CUT_LIVE_UATSTEntities DB = new CUT_LIVE_UATSTEntities();
                using (var addTransfer = DB.Database.BeginTransaction())
                {
                    IQueryable<tbl_PackageCab> List = (from obj in DB.tbl_PackageCab where obj.PackageId.ToString() ==PackID select obj).Distinct();
                    if (List.ToList().Count != 0)
                    {
                        DB.tbl_PackageCab.RemoveRange(List);
                        DB.SaveChanges();
                    }
                    DB.tbl_PackageCab.AddRange(ArrTransfer);
                    DB.SaveChanges();
                    addTransfer.Commit();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception ex)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }


        [WebMethod(true)]
        public string GetActivity(string ActCity)
        {
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new helperDataContext())
                {
                    var List = (from obj in DB.tbl_aeActivityMasters where obj.City.Contains(ActCity) select obj).ToList();
                    if (List.Any())
                        return objSerializer.Serialize(new { retCode = 1, ArrActivity = List });
                    else
                        return objSerializer.Serialize(new { retCode = 0 });
                }
            }
            catch (Exception ex)
            {
                return objSerializer.Serialize(new { retCode = 0 });
            }
        }

        //MapPaymentGateway

        [WebMethod(true)]
        public string AddActivities(List<tbl_PackageActivity> ArrActAdd)
        {
            string json = "";
            try
            {
                CUT_LIVE_UATSTEntities DB = new CUT_LIVE_UATSTEntities();
                using (var Payment = DB.Database.BeginTransaction())
                {
                    Int64 ArrID = ArrActAdd[0].PackageId;
                    var List = (from obj in DB.tbl_PackageActivity where obj.PackageId == ArrID select obj).ToList();
                    if (List.Count != 0)
                    {
                        DB.tbl_PackageActivity.RemoveRange(List);
                        DB.SaveChanges();
                    }
                    DB.tbl_PackageActivity.AddRange(ArrActAdd);
                    DB.SaveChanges();
                    Payment.Commit();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception ex)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        //[WebMethod(true)]
        //public string AddActivities(List<tbl_PackageActivity> ArrActAdd)
        //{
        //    string json = "";
        //    try
        //    {

        //        using (var DB = new helperDataContext())
        //        {
        //            var List = (from obj in DB.tbl_PackageActivities where obj.PackageId == ArrActAdd[0].PackageId select obj).ToList();
        //            if (List.Count != 0)
        //            {
        //                DB.tbl_PackageActivities.DeleteAllOnSubmit(List);
        //                DB.SubmitChanges();
        //            }
        //            DB.tbl_PackageActivities.InsertAllOnSubmit(ArrActAdd);
        //            DB.SubmitChanges();
        //            json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }

        //    return json;
        //}

    }
}
