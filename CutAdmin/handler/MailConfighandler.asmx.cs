﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for MailConfighandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MailConfighandler : System.Web.Services.WebService
    {
        string json = "";
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string jsonString = "";
        JavaScriptSerializer objserialize = new JavaScriptSerializer();


        helperDataContext DB = new helperDataContext();


        [WebMethod(EnableSession = true)]
        public string AddMailConfig(string userName, string password, string accessKey)
        {

            try
            {
                using (var DB = new helperDataContext())
                {

                    if (AccountManager.CheckSession)
                    {
                        // CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                        var List = (from obj in DB.tbl_MailConfigSettings where obj.ParentId == AccountManager.GetAdminByLogin() select obj).FirstOrDefault();
                        if (List != null)
                        {
                            CutAdmin.dbml.tbl_MailConfigSetting Delete = DB.tbl_MailConfigSettings.Single(y => y.ParentId == AccountManager.GetAdminByLogin());
                            DB.tbl_MailConfigSettings.DeleteOnSubmit(Delete);
                            DB.SubmitChanges();
                        }
                       
                            tbl_MailConfigSetting Add = new tbl_MailConfigSetting();
                            Add.userName = userName;
                            Add.password = password;
                            Add.accessKey = accessKey;
                            Add.ParentId = AccountManager.GetAdminByLogin();
                            DB.tbl_MailConfigSettings.InsertOnSubmit(Add);
                            DB.SubmitChanges();
                        
                        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";

                    }
                    else
                        throw new Exception("Session Expired");

                }

            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\",\"ex\":\"" + ex.Message + "\"}";
            }
            return json;

        }

        //[WebMethod(EnableSession = true)]
        //public string LoadConfig()
        //{
        //    try
        //    {
        //        using (var DB = new CutAdmin.dbml.helperDataContext())
        //        {
        //            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //            var List = (from obj in DB.tbl_MailConfigSettings where obj.ParentId == AccountManager.GetAdminByLogin() select obj).ToList();

        //            json = jsSerializer.Serialize(new { Session = 1, retCode = 1, MailConfigDetails = List });
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;
        //}


        [WebMethod(EnableSession = true)]
        public string GetMailConfig()
        {
            try
            {
                var List = (from obj in DB.tbl_MailConfigSettings where obj.ParentId == AccountManager.GetAdminByLogin() select obj).FirstOrDefault();

                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        //[WebMethod(EnableSession = true)]
        //public string DeleteMailDetail(string ParentId)
        //{
        //    try
        //    {
        //        using (var DB = new CutAdmin.dbml.helperDataContext())
        //        {
        //            Int64 ID = Convert.ToInt64(ParentId);
        //            CutAdmin.dbml.tbl_MailConfigSetting Delete = DB.tbl_MailConfigSettings.Single(y => y.ParentId == ID);
        //            DB.tbl_MailConfigSettings.DeleteOnSubmit(Delete);
        //            DB.SubmitChanges();

        //            json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;
        //}

    }
}
