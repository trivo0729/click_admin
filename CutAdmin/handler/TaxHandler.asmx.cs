﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CutAdmin.BL;
using System.Web.Script.Serialization;
using CutAdmin.DataLayer;
using CommonLib.Response;
using CutAdmin.EntityModal;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for TaxHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class TaxHandler : System.Web.Services.WebService
    {
        //dbTaxHandlerDataContext dbTax = new dbTaxHandlerDataContext();
        Click_Hotel db = new Click_Hotel();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        #region SAVE TAXES
        [WebMethod(EnableSession = true)]
        public string SaveTax(Comm_Tax objTax)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var dbTax = new Click_Hotel())
                {
                    if (objTax.ID == 0)
                    {
                        if (HttpContext.Current.Session["LoginUser"] == null)
                            throw new Exception("Session Expired ,Please Login and try Again");
                      CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];

                        string UserType = objGlobalDefault.UserType;
                        Int64 Uid = 0;
                        CutAdmin.GenralHandler objgnrl = new CutAdmin.GenralHandler();
                        Uid = objgnrl.GetUserID(UserType);


                        //Int64 Uid = objGlobalDefault.sid;
                        //if (objGlobalDefault.UserType != "Supplier")
                        //    Uid = objGlobalDefault.ParentId;
                        if (dbTax.Comm_Tax.Where(d => d.Name == objTax.Name && d.UserID == Uid).ToList().Count != 0)
                            return jsSerializer.Serialize(new { retCode = 2, ErrorMsg = "Already Exist, You Can not add" });
                        objTax.UserID = Uid;
                        dbTax.Comm_Tax.Add(objTax);
                        dbTax.SaveChanges();
                        return jsSerializer.Serialize(new { retCode = 1 });
                    }
                    else
                    {
                        var arrTax = dbTax.Comm_Tax.Where(d => d.ID == objTax.ID).FirstOrDefault();
                        arrTax.Name = objTax.Name;
                        arrTax.ParentTaxID = objTax.ParentTaxID;
                        arrTax.Value = objTax.Value;
                        arrTax.Desciption = objTax.Desciption;
                        arrTax.Active = objTax.Active;
                        dbTax.SaveChanges();
                        return jsSerializer.Serialize(new { retCode = 1 });
                    }
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }
        [WebMethod(EnableSession = true)]
        public string ActiveTax(Comm_Tax objTax)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var dbTax = new Click_Hotel())
                {
                    var arrTax = dbTax.Comm_Tax.Where(d => d.ID == objTax.ID).FirstOrDefault();
                    arrTax.Name = objTax.Name;
                    arrTax.ParentTaxID = objTax.ParentTaxID;
                    arrTax.Value = objTax.Value;
                    arrTax.Desciption = objTax.Desciption;
                    arrTax.Active = objTax.Active;
                    dbTax.Comm_Tax.Add(objTax);
                    db.SaveChanges();
                    return jsSerializer.Serialize(new { retCode = 1 });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string DeleteTax(Int64 Taxid)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var dbTax = new Click_Hotel())
                {
                    var arrTax = dbTax.Comm_Tax.Where(d => d.ID == Taxid).FirstOrDefault();
                    dbTax.Comm_Tax.Remove(arrTax);
                    dbTax.SaveChanges();
                    return jsSerializer.Serialize(new { retCode = 1 });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }

        }

        [WebMethod(EnableSession = true)]
        public string LoadTax()
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired ,Please Login and try Again!!");
              CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                var UserType = objGlobalDefault.UserType;
                Int64 Uid = 0;
                CutAdmin.GenralHandler objgnrl = new CutAdmin.GenralHandler();
                Uid = objgnrl.GetUserID(UserType);

                //Int64 Uid = objGlobalDefault.sid;
                //if (objGlobalDefault.UserType != "Supplier")
                //    Uid = objGlobalDefault.ParentId;
                using (var dbTax = new Click_Hotel())
                {
                    var arrTax = (from obj in dbTax.Comm_Tax
                                      //where obj.UserID == Uid
                                  select obj).ToList();
                    return jsSerializer.Serialize(new { retCode = 1, arrTax = arrTax });
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }


        [WebMethod(EnableSession = true)]
        public string SaveSupplierTax(List<Comm_SupplierTax> arrTax)
        {
            jsSerializer = new JavaScriptSerializer();
            Int64 Uid = AccountManager.GetSupplierByUser();
            try
            {
                using (var dbTax = new Click_Hotel())
                {
                    var arrOldTax = dbTax.Comm_SupplierTax.Where(d => d.SupplierID == Uid).ToList();
                    if (arrOldTax.Count != 0)
                        dbTax.Comm_SupplierTax.RemoveRange(arrOldTax);
                    arrTax.ForEach(d => d.SupplierID = Uid);
                    dbTax.Comm_SupplierTax.AddRange(arrTax);
                    dbTax.SaveChanges();
                }
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }
        #endregion

        #region Tax Mapping
        [WebMethod(EnableSession = true)]
        public string SaveTaxMapping(List<Comm_TaxMapping> objTaxReturn)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var dbTax = new Click_Hotel())
                {
                    dbTax.Comm_TaxMapping.AddRange(objTaxReturn);
                    db.SaveChanges();
                    return jsSerializer.Serialize(new { retCode = 1 });
                }
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }

        }
        #endregion

        #region Get TaxMapping & Taxces
        [WebMethod(EnableSession = true)]
        public string GetTaxMapping(Int64 HotelID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var dbTax = new Click_Hotel())
                {
                    if (HttpContext.Current.Session["LoginUser"] == null)
                        throw new Exception("Session Expired ,Please Login and try Again!!");
                  CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                    Int64 UID = objGlobalDefault.sid;
                    if (objGlobalDefault.UserType == "SupplierStaff")
                        UID = objGlobalDefault.ParentId;
                    var arrTax = (from obj in dbTax.Comm_Tax
                                      // where obj.UserID == UID && obj.Active == true
                                  where obj.Active == true
                                  select obj).ToList();
                    var arrChargeRate = (from obj in dbTax.Comm_TaxMapping
                                         where obj.HotelID == HotelID && obj.IsAddOns == false
                                         select obj).ToList();

                    //var arrMeals = (from obj in db.Comm_AddOns where obj.IsMeal == true && obj.UserId == UID select obj).ToList().OrderBy(d => d.IsMeal).ToList();
                    var arrAddOns = (from obj in db.Comm_AddOns where obj.UserId == UID select obj).ToList().OrderBy(d => d.IsMeal).ToList();
                    if (arrAddOns.Count == 0 || arrAddOns.All(D => D.IsMeal == false))
                    {
                        arrAddOns = new List<Comm_AddOns>();
                        arrAddOns.Add(new Comm_AddOns
                        {
                            AddOnName = "Breakfast",
                            Details = "Breakfast",
                            Type = "-",
                            Activate = true,
                            IsMeal = true,
                            MealType = "BB,HB,FB",
                            UserId = UID
                        });
                        arrAddOns.Add(new Comm_AddOns
                        {
                            AddOnName = "Lunch",
                            Details = "Lunch",
                            Type = "-",
                            Activate = true,
                            IsMeal = true,
                            MealType = "HB,FB",
                            UserId = UID
                        });
                        arrAddOns.Add(new Comm_AddOns
                        {
                            AddOnName = "Dinner",
                            Details = "Dinner",
                            Type = "-",
                            Activate = true,
                            IsMeal = true,
                            MealType = "FB",
                            UserId = UID
                        });
                        arrAddOns.Add(new Comm_AddOns
                        {
                            AddOnName = "Hi-Tea",
                            Details = "Hi-Tea",
                            Type = "-",
                            Activate = true,
                            IsMeal = true,
                            MealType = "FB",
                            UserId = UID
                        });
                        db.Comm_AddOns.AddRange(arrAddOns);
                        db.SaveChanges();
                    }
                    var arrAddOnsTax = (from obj in dbTax.Comm_TaxMapping
                                        where obj.HotelID == HotelID && obj.IsAddOns == true
                                        select obj).ToList();
                    return jsSerializer.Serialize(new
                    {
                        retCode = 1,
                        arrTax = arrTax,
                        arrChargeRate = arrChargeRate,
                        arrAddOns = arrAddOns,
                        arrAddOnsTax = arrAddOnsTax,
                    });
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }


        [WebMethod(EnableSession = true)]
        public string GetSupplierTax()
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var dbTax = new Click_Hotel())
                {
                    if (HttpContext.Current.Session["LoginUser"] == null)
                        throw new Exception("Session Expired ,Please Login and try Again!!");
                    Int64 UID = AccountManager.GetSupplierByUser();
                    var arrTax = (from obj in dbTax.Comm_Tax
                                  where obj.UserID == UID && obj.Active == true
                                  select obj).ToList();
                    var arrChargeTax = (from obj in dbTax.Comm_SupplierTax
                                        where obj.SupplierID == UID
                                        select obj).ToList();
                    return jsSerializer.Serialize(new
                    {
                        retCode = 1,
                        arrTax = arrTax,
                        arrTaxRate = arrChargeTax,
                    });
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }
        #endregion

        #region Get AddOnsMapping & Taxces
        [WebMethod(EnableSession = true)]
        public string GetAddOnsMapping(Int64 HotelID, Int64 RoomId)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var dbTax = new Click_Hotel())
                {
                    if (HttpContext.Current.Session["LoginUser"] == null)
                        throw new Exception("Session Expired ,Please Login and try Again!!");
                  CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                    //Int64 UID = AccountManager.GetAdminByLogin();
                    Int64 UID = AccountManager.GetSuperAdminID();
                    //if (objGlobalDefault.UserType == "SupplierStaff")
                    //    UID = objGlobalDefault.ParentId;
                    var arrTax = (from obj in dbTax.Comm_Tax
                                  where obj.UserID == UID && obj.Active == true
                                  select obj).ToList();
                    var arrAddOns = (from obj in db.Comm_AddOns
                                     where obj.UserId == UID && obj.IsMeal == false
                                     select obj).ToList();

                    var arrMeals = (from obj in db.Comm_AddOns
                                    where obj.UserId == UID && obj.IsMeal == true
                                    select obj).ToList();

                    var arrAddOnsRate = (from obj in dbTax.Comm_TaxMapping
                                         where obj.HotelID == HotelID && obj.IsAddOns == true
                                         select obj).ToList();

                    var arrAddOnsPrice = (from obj in dbTax.Comm_AddOnsRate
                                          where obj.HotelID == HotelID && obj.RateID == RoomId
                                          select obj).ToList();

                    List<object> arrMealRate = new List<object>();
                    foreach (var objAddOns in arrAddOnsPrice)
                    {
                        var sMeals = (from obj in db.Comm_AddOns where obj.ID == objAddOns.AddOnsID select obj).FirstOrDefault();
                        arrMealRate.Add(new
                        {
                            sMeals.AddOnName,
                            sMeals.IsMeal,
                            sMeals.MealType,
                            Rate = objAddOns.Rate,
                            objAddOns.IsCumpulsury,
                            objAddOns.RateType,

                        });
                    }
                    return jsSerializer.Serialize(new
                    {
                        retCode = 1,
                        arrTax = arrTax,
                        arrAddOnsRate = arrAddOnsRate,
                        arrAddOns = arrAddOns,
                        arrMeals = arrMeals,
                        arrAddOnsPrice = arrMealRate
                    });
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }
        #endregion

        #region GetAddOnsByRoom
        [WebMethod(EnableSession = true)]
        public string GetAddOnsByRoom(Int64 HotelID, Int64 RateTypeID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var dbTax = new Click_Hotel())
                {
                    if (HttpContext.Current.Session["LoginUser"] == null)
                        throw new Exception("Session Expired ,Please Login and try Again!!");
                  CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                    var arrTax = (from obj in dbTax.Comm_Tax
                                  where obj.UserID == objGlobalDefault.sid
                                  select obj).ToList();
                    var arrAddOns = (from obj in db.Comm_AddOns
                                     where obj.UserId == objGlobalDefault.sid
                                     select obj).ToList();
                    var arrAddOnsRate = (from obj in dbTax.Comm_TaxMapping
                                         where obj.HotelID == HotelID && obj.IsAddOns == true && obj.ServiceID == RateTypeID
                                         select new
                                         {
                                             ID = obj.TaxID
                                         }).Distinct().ToList();
                    List<TaxRate> ListAddOnsRate = new List<TaxRate>();
                    foreach (var objAddOns in arrAddOnsRate)
                    {
                        var ListTaxes = dbTax.Comm_TaxMapping.Where(r => r.TaxID == objAddOns.ID && r.ServiceID == RateTypeID && r.IsAddOns == true).ToList();
                        TaxRate objRate = new TaxRate();
                        objRate.RateName = arrAddOns.Where(D => D.ID == objAddOns.ID).FirstOrDefault().AddOnName;
                        objRate.Type = arrAddOns.Where(D => D.ID == objAddOns.ID).FirstOrDefault().Type;
                        objRate.BaseRate = Convert.ToSingle(ListTaxes.FirstOrDefault().ServiceName);
                        objRate.TaxOn = new List<Tax>();
                        foreach (var objTax in ListTaxes)
                        {
                            if (dbTax.Comm_Tax.Where(d => d.ID == objTax.TaxOnID).ToList().Count != 0)
                            {
                                objRate.TaxOn.Add(new Tax
                                {
                                    TaxName = dbTax.Comm_Tax.Where(d => d.ID == objTax.TaxOnID).FirstOrDefault().Name,
                                    TaxPer = Convert.ToSingle(dbTax.Comm_Tax.Where(d => d.ID == objTax.TaxOnID).FirstOrDefault().Value),
                                    TaxRate = Convert.ToSingle(dbTax.Comm_Tax.Where(d => d.ID == objTax.TaxOnID).FirstOrDefault().Value) * objRate.BaseRate / 100,
                                });
                            }
                            else if (objTax.TaxOnID == 0)
                            {
                                objRate.TaxOn.Add(new Tax
                                {
                                    TaxName = "Base Rate",
                                    TaxPer = 0,
                                    TaxRate = 0,
                                });
                            }

                        }
                        objRate.TotalRate = objRate.BaseRate + objRate.TaxOn.Select(d => d.TaxRate).ToList().Sum();
                        ListAddOnsRate.Add(objRate);
                    }


                    return jsSerializer.Serialize(new { retCode = 1, arrAddOnsRate = ListAddOnsRate });
                }
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ErrorMsg = ex.Message });
            }

        }

        public List<TaxRate> GetRates(Int64 HotelCode)
        {
            List<TaxRate> arrRates = new List<TaxRate>();
            try
            {
                using (var dbTax = new Click_Hotel())
                {
                    var TaxRates = dbTax.Comm_TaxMapping.Where(d => d.HotelID == HotelCode && d.IsAddOns == false).ToList();
                    foreach (var objRate in TaxRates.Select(d => d.TaxID).Distinct().ToList())
                    {
                        var ListTaxes = TaxRates.Where(r => r.TaxID == objRate).ToList().Select(d => d.TaxOnID).Distinct().ToList();
                        List<Tax> arrTax = new List<Tax>();
                        foreach (var objTax in ListTaxes)
                        {
                            if (dbTax.Comm_Tax.Where(d => d.ID == objTax).ToList().Count != 0)
                            {
                                arrTax.Add(new Tax
                                {
                                    TaxName = dbTax.Comm_Tax.Where(d => d.ID == objTax).FirstOrDefault().Name,
                                    TaxPer = Convert.ToSingle(dbTax.Comm_Tax.Where(d => d.ID == objTax).FirstOrDefault().Value),
                                    TaxRate = 0,
                                });
                            }
                            else if (objTax == 0)
                            {
                                arrTax.Add(new Tax
                                {
                                    TaxName = "Base Rate",
                                    TaxPer = Convert.ToSingle(dbTax.Comm_Tax.Where(d => d.ID == objRate).FirstOrDefault().Value),
                                    TaxRate = 0,
                                });
                            }

                        }
                        if (dbTax.Comm_Tax.Where(d => d.ID == objRate).ToList().Count != 0)
                        {
                            arrRates.Add(new TaxRate
                            {

                                RateName = dbTax.Comm_Tax.Where(d => d.ID == objRate).FirstOrDefault().Name,
                                TaxOn = arrTax
                            });
                        }
                    }
                }
            }
            catch
            {

            }
            return arrRates;
        }
        #endregion

        #region Tax Configration
        [WebMethod(EnableSession = true)]
        public string Configration(bool bOwn, bool bHotel, bool bNetShow)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                Int64 Uid = AccountManager.GetSupplierByUser();
                using (var db = new CutAdmin.dbml.helperDataContext())
                {
                    var arrLogin = (from obj in db.tbl_AdminLogins where obj.sid == Uid select obj).FirstOrDefault();
                    //arrLogin.HotelTax = bHotel;
                    //arrLogin.ShowNet = bNetShow;
                    //arrLogin.OwnTax = bOwn;
                    db.SubmitChanges();
                }
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 1, ex = ex.Message });
            }
        }
        #endregion

    }
}
