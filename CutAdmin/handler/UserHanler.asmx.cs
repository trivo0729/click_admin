﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using CutAdmin.HotelAdmin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using Newtonsoft.Json;
using CutAdmin.Models;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for UserHanler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class UserHanler : System.Web.Services.WebService
    {

        JavaScriptSerializer objSerializer = new JavaScriptSerializer();
        helperDataContext DB = new helperDataContext();
 
        [WebMethod(EnableSession = true)]
        public void GetAgentDetail()
        {
            objSerializer.MaxJsonLength = int.MaxValue;
            try
            {                
                int TotalCount = 0;
                var Agentlist = UserManager.GetAgents(out TotalCount);
                var OrderList = Agentlist.Select(o => new {
                    o.sid,
                    Last_Name = "",
                    Iswitch = "<input type='checkbox' id='chk_On" + o.sid + "' name='switch-tiny' class='switch tiny' value='On'   onchange=\"Activate('" + o.sid + "','" + o.LoginFlag + "','" + o.ContactPerson + "','" + "" + "')\" value='' / >",
                    o.ContactPerson,
                    o.password,
                    o.AgencyName,
                    o.AvailableCredit,
                    o.uid,
                    o.Agentuniquecode,
                    o.LoginFlag,
                    o.CurrencyCode,
                    o.agentCategory,
                    o.UserType,
                });
                object finalresult = DTResult.GetDataTable(OrderList, TotalCount);           
            }
            catch (Exception ex)
            {

            }

        }

        [WebMethod(EnableSession = true)]
        public string GetAgencyDetails(Int64 AgentID)
        {
            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            try
            {
                using (var DB = new helperDataContext())
                {
                    var arrAgency = (from obj in DB.tbl_AdminLogins
                                     from objContact in DB.tbl_Contacts
                                     from objCredit in DB.tbl_AdminCreditLimits
                                     where obj.ContactID == objContact.ContactID &&
                                     obj.sid == objCredit.uid && obj.sid == AgentID
                                     select new
                                     {
                                         obj.sid,
                                         obj.AgencyName,
                                         obj.Agentuniquecode,
                                         obj.ContactPerson,
                                         obj.Designation,
                                         objContact.Address,
                                         Description = (from objCity in DB.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Description,
                                         Country = (from objCity in DB.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Countryname,
                                         objContact.PinCode,
                                         obj.uid,
                                         obj.password,
                                         objCredit.AvailableCredit,
                                         objContact.phone,
                                         objContact.email,
                                         objContact.Mobile,
                                         objContact.Fax,
                                         objContact.Website,
                                         obj.IATANumber,
                                         obj.UserType,
                                         obj.dtLastAccess,
                                         obj.EnableCheckAccount,
                                         obj.Updatedate,
                                         obj.LoginFlag,
                                         obj.CurrencyCode,
                                         obj.ParentID,
                                         obj.agentCategory,
                                         obj.PANNo,
                                         obj.GSTNumber,

                                     }).FirstOrDefault();



                    return objSerializer.Serialize(new { retCode = 1, arrAgency = arrAgency });
                }
            }
            catch
            {
                return objSerializer.Serialize(new { });
            }
        }

        [WebMethod(EnableSession = true)]
        public void Search(string Name, string Type, string Code, string Group, string Status, string Country, string City, string MinBalance)
        {

            try
            {
                int TotalCount = 0;
                var SearchAgent = UserManager.Search(Name, Type, Code, Group, Status, Country, City, MinBalance,out TotalCount);
                var OrderList = SearchAgent.Select(o => new {
                    o.sid,
                    Last_Name = "",
                    Iswitch = "<input type='checkbox' id='chk_On" + o.sid + "' name='switch-tiny' class='switch tiny' value='On'   onchange=\"Activate('" + o.sid + "','" + o.LoginFlag + "','" + o.ContactPerson + "','" + "" + "')\" value='' / >",
                    o.ContactPerson,
                    o.password,
                    o.AgencyName,
                    o.AvailableCredit,
                    o.uid,
                    o.Agentuniquecode,
                    o.LoginFlag,
                    o.CurrencyCode,
                    o.agentCategory,
                    o.UserType,
                });
                object finalresult = DTResult.GetDataTable(OrderList, TotalCount);
            }
            catch(Exception ex)
            { 
            }

        }

        [WebMethod(EnableSession = true)]
        public string AgentGroupAssign(Int64 AgentId, Int64 GroupId)
        {
            string json = "";
            try
            {
                using (var DB = new CutAdmin.dbml.helperDataContext())
                {
                    CutAdmin.dbml.tbl_AgentGroupMarkupMapping Update = DB.tbl_AgentGroupMarkupMappings.Single(x => x.AgentId == AgentId);
                    Update.GroupId = GroupId;
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }


        public class AgencyDetails
        {
            public Int64 sid { get; set; }
            public string AgencyName { get; set; }
            public string Agentuniquecode { get; set; }
            public string uid { get; set; }
            public string password { get; set; }
            public Decimal AvailableCredit { get; set; }
            public string GroupName { get; set; }
            public string Description { get; set; }
            public string Country { get; set; }
            public string ContactPerson { get; set; }
            public string Mobile { get; set; }
            public DateTime dtLastAccess { get; set; }
            public bool EnableCheckAccount { get; set; }
            public string LoginFlag { get; set; }
            public string CurrencyCode { get; set; }
            public Int64 ParentID { get; set; }
            public string agentCategory { get; set; }
            public string UserType { get; set; }
        }
    }
}
