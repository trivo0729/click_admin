﻿using CutAdmin.DataLayer;
using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.handler
{
    /// <summary>
    /// Summary description for GatewayHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class GatewayHandler : System.Web.Services.WebService
    {
        helperDataContext DB = new helperDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";

        #region PaymentGateway SupperAdmin
        [WebMethod(EnableSession = true)]
        public string AddPaymentGateway(string GatewayName)
        {
            using (helperDataContext DB = new helperDataContext())
            {
                try
                {
                    CutAdmin.dbml.tbl_PaymentGateway Add = new CutAdmin.dbml.tbl_PaymentGateway();
                    Add.PaymentGateway = GatewayName;
                    DB.tbl_PaymentGateways.InsertOnSubmit(Add);
                    DB.SubmitChanges();

                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
                catch (Exception)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetPaymentGateway()
        {
            using (helperDataContext DB = new helperDataContext())
            {
                try
                {
                    var List = (from obj in DB.tbl_PaymentGateways select obj).ToList();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Name = List });
                }
                catch (Exception)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdatePaymentGateway(Int64 Id, string PaymentGateway)
        {
            using (helperDataContext DB = new helperDataContext())
            {
                try
                {
                    CutAdmin.dbml.tbl_PaymentGateway Update = DB.tbl_PaymentGateways.Single(x => x.Id == Id);
                    Update.PaymentGateway = PaymentGateway;
                    DB.SubmitChanges();

                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
                catch (Exception)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string DeletePaymentGateway(Int64 Id)
        {
            try
            {
                CutAdmin.dbml.tbl_PaymentGateway Delete = DB.tbl_PaymentGateways.Single(x => x.Id == Id);
                DB.tbl_PaymentGateways.DeleteOnSubmit(Delete);
                DB.SubmitChanges();

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(true)]
        public string MapPaymentGateway(List<tbl_PayGatewayMapping> ArrGateway)
        {
            string json = "";
            try
            {

                using (var DB = new helperDataContext())
                {
                    var List = (from obj in DB.tbl_PayGatewayMappings where obj.ParentId == ArrGateway[0].ParentId select obj).ToList();
                    if (List.Count != 0)
                    {
                        DB.tbl_PayGatewayMappings.DeleteAllOnSubmit(List);
                        DB.SubmitChanges();
                    }
                    DB.tbl_PayGatewayMappings.InsertAllOnSubmit(ArrGateway);
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch (Exception ex)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetMappedPaymentGateway(Int64 Id)
        {
            using (helperDataContext DB = new helperDataContext())
            {
                try
                {
                    var List = (from obj in DB.tbl_PaymentGateways
                                join Name in DB.tbl_PayGatewayMappings on obj.Id equals Name.GatewayId
                                where Name.ParentId == Id
                                select new
                                {
                                    Name.GatewayId,
                                }).ToList();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Name = List });
                }
                catch (Exception)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }

            return json;
        }

        #endregion


        #region Charges By Admin

        [WebMethod(EnableSession = true)]
        public string GetPaymentGatewayDetails()
        {
            using (helperDataContext DB = new helperDataContext())
            {
                List<object> arrList = new List<object>();
                try
                {
                    var List = (from obj in DB.tbl_PaymentGateways
                                join Map in DB.tbl_PayGatewayMappings on obj.Id equals Map.GatewayId
                                where Map.ParentId == AccountManager.GetAdminByLogin() && obj.Id == Map.GatewayId
                                select new
                                {
                                    obj.PaymentGateway,
                                    Map.GatewayId,
                                    Map.ParentId,
                                }).ToList();
                    if (List.Count != 0)
                    {
                        foreach (var item in List)
                        {
                            var AmountPer = (from obj in DB.tbl_PayGatewayCharges where obj.ParentId == item.ParentId && obj.GatewayId == item.GatewayId select obj.AmountPer).FirstOrDefault();
                            if (AmountPer == null)
                                AmountPer = 0;
                            var Amount = (from obj in DB.tbl_PayGatewayCharges where obj.ParentId == item.ParentId && obj.GatewayId == item.GatewayId select obj.Amount).FirstOrDefault();
                            if (Amount == null)
                                Amount = 0;
                            arrList.Add(new
                            {
                                PaymentGateway = item.PaymentGateway,
                                GatewayId = item.GatewayId,
                                ParentId = item.ParentId,
                                AmountPer = AmountPer,
                                Amount = Amount,
                            });
                        }
                    }

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = arrList, });
                }
                catch (Exception)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string SavePaymentGatewayDetails(Int64 GatewayId, Decimal AmountPer, Decimal Amount, Int64 ParentId)
        {
            using (helperDataContext DB = new helperDataContext())
            {
                try
                {
                    var List = (from obj in DB.tbl_PayGatewayCharges where obj.ParentId == ParentId && obj.GatewayId == GatewayId select obj).ToList();
                    if (List.Count != 0)
                    {
                        DB.tbl_PayGatewayCharges.DeleteAllOnSubmit(List);
                        DB.SubmitChanges();
                    }
                    tbl_PayGatewayCharge Add = new tbl_PayGatewayCharge();
                    Add.GatewayId = GatewayId;
                    Add.AmountPer = AmountPer;
                    Add.Amount = Amount;
                    Add.ParentId = ParentId;
                    DB.tbl_PayGatewayCharges.InsertOnSubmit(Add);
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
                catch (Exception)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetPaymtGateway()
        {
            using (helperDataContext DB = new helperDataContext())
            {
                try
                {
                    var List = (from obj in DB.tbl_PaymentGateways
                                join Map in DB.tbl_PayGatewayMappings on obj.Id equals Map.GatewayId
                                where Map.ParentId == AccountManager.GetAdminByLogin()
                                select new
                                {
                                    obj.PaymentGateway,
                                    obj.Id
                                }).ToList();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Name = List });
                }
                catch (Exception)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }

            return json;
        }

        #endregion

    }
}
