﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="CityMapping.aspx.cs" Inherits="CutAdmin.GetCountryCityMapping" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script src="Scripts/CountryCityCode.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            GetCountry();
        });
    </script>
    <script src="Scripts/GetCountryCityMapping.js"></script>

	<!-- Additional styles -->
	<link rel="stylesheet" href="css/styles/form.css?v=1">
	<link rel="stylesheet" href="css/styles/switches.css?v=1">
	<link rel="stylesheet" href="css/styles/table.css?v=1">

	<!-- DataTables -->
	<link rel="stylesheet" href="js/libs/DataTables/jquery.dataTables.css?v=1">

	<!-- Microsoft clear type rendering -->
	<meta http-equiv="cleartype" content="on">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" class="clearfix with-menu with-shortcuts">

<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

		<hgroup id="main-title" class="thin">
			<h3> Cities Mapping</h3><hr />
		</hgroup>
        
		<div class="with-padding">

		
			<div class="button-height">
				Country:
				<select name="select90"  id="selCountry" class="select black-gradient glossy mid-margin-left">
                     <option selected="selected" value="-">Select Any Country</option>
				</select>
                &nbsp;&nbsp;&nbsp;
                City:
				<select name="select90"  id="selCity" class="select black-gradient glossy mid-margin-left">
                    <option selected="selected" value="-">Select Any City</option>
				</select>
                &nbsp;&nbsp;&nbsp;
				<button type="button" class="button black-gradient glossy" id="btn_GetCityCode"  onclick="GetMappingDetails();">Go</button>
			
               
            </div>
		
             <table class="table responsive-table" id="tbl_GetCityCodeMapping">
                        <tbody>
                            <tr style="background-color:#494747;color:white">
                                
                                <td>
                                    <span class="text-left">Supplier</span>
                                </td>
                                <td>
                                    <span class="text-left">Country Name</span>
                                </td>
                                <td>
                                    <span class="text-left">Code</span>
                                </td>
                                <td>
                                    <span class="text-left">City Name</span>
                                </td>
                                <td>
                                    <span class="text-left">City Code</span>
                                </td>
                                <td>
                                    <span class="text-left">Area Name</span>
                                </td>
                                <td>
                                    <span class="text-left">Area Code</span>
                                </td>
                                <td>
                                    <span class="text-left">Select</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
			<div  class="table-footer ">
                 <button type="button" class="button glossy mid-margin-right float-right" id="btn_Map" onclick="MapCities();">Map Selected Cities</button>
			<br /><br />
			</div>

	
		</div>

	</section>
	<!-- End main content -->
</asp:Content>
