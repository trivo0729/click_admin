﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="MapArea.aspx.cs" Inherits="CutAdmin.MapArea" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <%-- <script src="Scripts/AddOfferRate.js"></script>--%>
    
    <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">


    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
    <link href="css/ScrollingTable.css" rel="stylesheet" />
    <script src="Scripts/moments.js"></script>
     <script src="Scripts/jquery-ui.js"></script>
    <script src="Scripts/MapArea.js?v=1.6"></script>

    <img alt="" src="../loader1.gif" id="loders" width="128" height="128" style="position: fixed; z-index: 9999; left: 55%; margin-top: 8%; display: none">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <div class="with-padding">

            <div class="columns" id="DivAddOfferRate">

                <hgroup id="main-title" class="thin">
                    <h3>Map Area</h3>
                    <hr />


                </hgroup>

                <div class="new-row one-columns"></div>
                <div class="three-columns" id="div_MapAr">
                    <span class="text-left">Select Country:</span>
                    <br>
                    <select id="selCountryMapAr" onchange="GetLocation(this.value)" class="full-width  select OfferType">
                    </select>
                </div>
                <div class="three-columns">
                    <span class="text-left">City:</span>
                    <br>
                    <input id="txtCity" name="Text[]" list="Select_City" class="input full-width" type="text" onchange="GetCode(this.value)">
                    <datalist id="Select_City"></datalist>
                    <label style="color: red; margin-top: 3px; display: none" id="lbl_txtCity">
                        <b>* This field is required</b>
                    </label>

                </div>
                <div class="three-columns">
                    <div style="margin-top: 5px">
                        <span class="text-left">Code:</span>
                    </div>

                    <%--<input id="txtCountryCodeAr" class="input full-width" type="text" readonly>--%>
                    <div style="margin-top: 5px">
                        <b>
                            <label id="lbl_txtCountryCodeAr">
                            </label>
                        </b>
                    </div>

                </div>

                <div class="two-columns" style="float:right">
                    
                      <a href="MapAreaList.aspx">
                    <button type="button" class="button blue-gradient glossy" id="btn_list" onclick="">Map Area List</button>
                </a>
                    
                    </div>

                <section style="margin-left: 27px" id="row_section">
                    <%--<div id="row1" style="width: 100%;padding-left:20px">
                    <div class="columns">
                        

                        <div class="three-columns" id="div_GTAAr" style="margin-left: 24.125px; clear:left;">
                            <span class="text-left">GTA:</span>
                            <input type="radio" id="GTAradioAr" name="radio" onclick="GetCode(1)"/>
                            <br>
                            <select id="selGTAAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div class="three-columns" id="div_HB1">
                            <span class="text-left">HB:</span>
                            <input type="radio" id="HBradioAr" name="radio" onclick="GetCode(2)" checked/>
                            <br>
                            <select id="selHBAr" onchange="BindCode(this.value)" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div class="three-columns" id="div_TBOAr">
                            <span class="text-left">TBO:</span>
                           <input type="radio" id="TBOradioAr" name="radio" onclick="GetCode(3)"/>
                            <br>
                            <select id="selTBOAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        

                      <div class="three-columns" id="div_DOTAr" style="clear:left">
                            <span class="text-left">DOTW:</span>
                            <input type="radio" id="DOTWradioAr" name="radio" onclick="GetCode(4)"/>
                            <br>
                            <select id="selDOTAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div class="three-columns" id="div_MGHAr">
                            <span class="text-left">MGH:</span>
                             <input type="radio" id="MGHWradioAr" name="radio" onclick="GetCode(5)"/>
                            <br>
                            <select id="selMGHAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div class="three-columns" id="div_GRNAr">
                            <span class="text-left">GRN:</span>
                            <input type="radio" id="GRNradioAr" name="radio" onclick="GetCode(6)"/>
                            <br>
                            <select id="selGRNAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div id="RemoveRowDiv1" class="one-columns" style="padding-top: 16px; display:none;" title="Remove Row">
                            <i onclick="RemoveRow(1)" aria-hidden="true"><label class="button blue-gradient"><span class="icon-minus"></span></label></i>
                        </div>

                        <div id="AddRowDiv1" class="one-columns" style="padding-top:16px" title="Add Row">
                            <i onclick="AddRow()" aria-hidden="true"><label class="button blue-gradient" ><span class="icon-plus"></span></label></i>
                        </div>

                        <div class="columns">
                            <div class="one-columns" style="margin-top: 16px">
                                <button class="button glossy blue-gradient" id="btn_save" onclick="SaveCityLocation()">Save</button>
                                <button class="button glossy blue-gradient" id="btn_Update" onclick="UpdateCityLocation()" style="display:none">Update</button>
                            </div>
                        </div>



                    </div>
                </div>--%>
                </section>


                <%--<div id="row2" style="width: 100%;padding-left:20px">
                    <div class="columns">--%>
                <%--<div class="three-columns" id="div_DOTAr">
                            <span class="text-left">DOTW:</span>
                            <input type="radio" id="DOTWradioAr" name="radio" onclick="GetCode(4)"/>
                            <br>
                            <select id="selDOTAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div class="three-columns" id="div_MGHAr">
                            <span class="text-left">MGH:</span>
                             <input type="radio" id="MGHWradioAr" name="radio" onclick="GetCode(5)"/>
                            <br>
                            <select id="selMGHAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div class="three-columns" id="div_GRNAr">
                            <span class="text-left">GRN:</span>
                            <input type="radio" id="GRNradioAr" name="radio" onclick="GetCode(6)"/>
                            <br>
                            <select id="selGRNAr" class="full-width  select OfferType">
                            </select>
                        </div>

                        <div id="RemoveRowDiv1" class="one-columns" style="padding-top: 16px; display:none;" title="Remove Row">
                            <i onclick="RemoveRow(1)" aria-hidden="true"><label class="button blue-gradient"><span class="icon-minus"></span></label></i>
                        </div>

                        <div id="AddRowDiv1" class="one-columns" style="padding-top:16px" title="Add Row">
                            <i onclick="AddRow()" aria-hidden="true"><label class="button blue-gradient" ><span class="icon-plus"></span></label></i>
                        </div>

                        <div class="columns">
                            <div class="one-columns" style="margin-top: 16px">
                                <button class="button glossy blue-gradient" id="btn_save" onclick="SaveCityLocation()">Save</button>
                                <button class="button glossy blue-gradient" id="btn_Update" onclick="UpdateCityLocation()" style="display:none">Update</button>
                            </div>
                        </div>--%>


                <%--</div>
                </div>--%>
            </div>



        </div>
        <div>
            <div class="columns">
                <div class="three-columns"></div>
                <div class="three-columns"></div>
                <div class="three-columns"></div>
                <div class="three-columns">
                    <button class="button glossy blue-gradient" id="btn_save" onclick="SaveArea()">Save</button>
                    <button class="button glossy blue-gradient" id="btn_Update" onclick="UpdateAreaMapping()" style="display:none">Update</button>

                </div>
            </div>

        </div>
        <%--  </div>--%>
        <br />
        <br />
        <br />
        <%--<div class="panel panel-primary">
            <div class="with-padding">
                <div class="panel-body">

                    <div class="table-responsive">
                        <table class="table" id="tbl_AreaGroup">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th>Destination Code</th>
                                    <th>Name</th>
                                    <th>Country Code</th>
                                    <th>Group_ID</th>
                                    <th>GTA</th>
                                    <th>HB</th>
                                    <th>DOTW</th>
                                    <th>MGH</th>
                                    <th>TBO</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>--%>
        <%--<section style="padding-left: 15px; padding-right: 15px;">
            <table class="simple-table responsive-table" id="tbl_AreaGroupMapp">
                <thead>
                    <tr>
                        <th>Sr.No.</th>
                        <th>Destination Code</th>
                        <th>Name</th>
                        <th>Country Code</th>
                        <th>Group_ID</th>
                        <th>GTA</th>
                        <th>HB</th>
                        <th>DOTW</th>
                        <th>MGH</th>
                        <th>TBO</th>
                        <th>GRN</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </section>--%>
    </section>

    <script type="text/javascript">

        $(function () {
            $("#Fixeddatepicker3").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                //onSelect: insertDepartureDate,
                //dateFormat: "yy-m-d",
                minDate: "dateToday",
                //maxDate: "+3M +10D"
            });
        });
    </script>


</asp:Content>
