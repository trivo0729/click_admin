﻿using CutAdmin.EntityModal;
using CutAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CutAdmin.Webservices.Cart
{
    /// <summary>
    /// Summary description for CartHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class CartHandler : System.Web.Services.WebService
    {
        [WebMethod]
        public void AddtoCart(tbl_CartDetail arrCart, tbl_ProductDetails arrProduct)
        {
            var arrData = CartManager.AddtoCart(arrCart, arrProduct);
            DTResult.setResponse(arrData);
        }
    }
}
