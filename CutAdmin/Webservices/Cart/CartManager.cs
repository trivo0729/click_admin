﻿using CutAdmin.EntityModal;
using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.Webservices.Cart
{
    public class CartManager
    {
        public static object AddtoCart(tbl_CartDetail arrCart, tbl_ProductDetails arrProduct)
        {
            CUT_LIVE_UATSTEntities db = new CUT_LIVE_UATSTEntities();
            object Data = new object();
            try
            {
                using (var cart = db.Database.BeginTransaction())
                {
                    if (arrCart.CartID != 0)
                    {
                        db.tbl_CartDetail.Add(arrCart);
                        db.SaveChanges();
                    }
                    arrProduct.CartID = arrCart.CartID;
                    db.tbl_ProductDetails.Add(arrProduct);
                    db.SaveChanges();
                    cart.Commit();
                    Data = new
                    {
                        retCode = 1,
                        Massage = "Add to cart successfully",
                        CartID = arrCart.CartID,
                    };
                }
            }
            catch (Exception ex)
            {
                Data = new
                {
                    retCode = 0,
                    Massage = "Something went wrong",
                };
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return Data;
        }

    }
}