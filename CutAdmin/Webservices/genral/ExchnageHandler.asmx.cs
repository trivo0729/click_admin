﻿using CutAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CutAdmin.Webservices.genral
{
    /// <summary>
    /// Summary description for ExchnageHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ExchnageHandler : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public void GetCurrency(long ParentID)
        {
            try
            {
                var arrData = ExchangeManager.GetExchange(ParentID);
                DTResult.setResponse(arrData);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
