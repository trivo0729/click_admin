﻿using CutAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CutAdmin.Webservices.genral
{
    /// <summary>
    /// Summary description for locationHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
   [System.Web.Script.Services.ScriptService]
    public class locationHandler : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public void GetCountry()
        {
            try
            {
                var arrData=  CutAdmin.Services.CityAddUpdateService.GetCountryCity();
                DTResult.setResponse(arrData);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [WebMethod(EnableSession = true)]
        public void GetCity(string Country)
        {
            try
            {
                var arrData = CutAdmin.Services.CityAddUpdateService.GetCityByCode(Country);
                DTResult.setResponse(arrData);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
