﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CUT.DataLayer;
using CutAdmin.EntityModal;
namespace CutAdmin.Webservices.genral
{
    public class ExchangeManager
    {
        public class ExchangeRate
        {

            public string BaseCurrency { get; set; }
            public string Currency { get; set; }
            public Double? Exchange { get; set; }
            public string LastUpdateDt { get; set; }
            public bool MailFlag { get; set; }
            public Double? Rate { get; set; }
        }
        public static object GetExchange(long ParentID)
        {
            var Data = new object();
            List<ExchangeRate> arrRate = new List<ExchangeRate>();
            try
            {
               
                using (var db = new Trivo_AmsHelper())
                {
                    string AdminUser = "";
                    using (var DB = new CutAdmin.dbml.helperDataContext())
                    {
                        AdminUser = DB.tbl_AdminLogins.Where(d => d.sid == ParentID).FirstOrDefault().uid;
                    }
                    arrRate = (from obj in db.tbl_UserCurrency
                               join objAdmin in db.tbl_Admin on obj.ParentID equals objAdmin.ID
                               join  objCurrency in db.tbl_CurrencyCode on obj.CurrencyID equals objCurrency.ID
                               where objAdmin.EmailID == AdminUser
                               select new ExchangeRate
                               {
                                BaseCurrency = objAdmin.Currency,
                                Currency = objCurrency.CurrencyCode,
                                Rate =(1 / obj.Rate),
                                Exchange = (1 / (obj.Rate + obj.Markup)),
                                LastUpdateDt = obj.UpdateDate,
                                MailFlag = true
                               }).ToList();

                    if(arrRate.Count !=0)
                    {
                        Data = new { retCode = 1, arrCurrency = arrRate };
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return Data;

        } 
    }
}