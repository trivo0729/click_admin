﻿using CUT.DataLayer;
using CUT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.EntityModal;
using System.Globalization;
using CutAdmin.DataLayer;
using SightseeingLib;
using CutAdmin.dbml;
using Elmah;

namespace CutAdmin.Webservices.Activity.DataLayer
{
    public class Rate
    {
        public int? ID { get; set; }

        public string SlotName { get; set; }
        public float fRate { get; set; }
    }
    public class GetActivityManager
    {

        public static CUT.DataLayer.GlobalDefault objGlobalDefault { get; set; }
        public static ActivityModel Activities { get; set; }
        public static List<string> ListTourType { get; set; }
        public static List<string> ListTourMode { get; set; }
        public static List<ActivityDetails> ListActivities { get; set; }
        public static string UserCurrency { get; set; }
        public static CUT.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes { get; set; }


        /*Get Images*/
        public static Image GetImages(int? ActID, string arrImages)
        {
            Image ListImage = new Image();
            try
            {
                int i = 0;
                string ActImageURL = CutAdmin.DataLayer.AccountManager.GetAdminDomain();
                foreach (string Image in arrImages.Split(','))
                {
                    if (Image != "")
                    {
                        ListImage =
                        new Image
                        {
                            IsDefault = true,
                            Title = Image.Replace(".jpg", " ").Replace(".png", " "),
                            Type = "AS",
                            Url = ActImageURL + "/activityImages/" + ActID + "/" + Image
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            
            return ListImage;
        }


        /* Tour Types*/
        public static List<TourType> GetType(string arrTypes)
        {
            List<TourType> ListTypes = new List<TourType>();
            int i = 1;
            try
            {
                foreach (var objModes in arrTypes.Split(';'))
                {
                    if (objModes == "")
                        continue;
                    ListTypes.Add(new TourType
                    {
                        //ID = "1",
                        Name = objModes,
                    });
                    if (!ListTourType.Contains(objModes))
                    {
                        ListTourType.Add(objModes);
                    }
                    else
                        if (ListTourType == null)
                    {
                        ListTourType = new List<string>();
                        ListTourType.Add(objModes);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }

            return ListTypes;
        }

        #region Activity By Date   
        public static object GetActivitiesByDate(string LocationID, string ActivityDate)
        {
            ListActivities = new List<ActivityDetails>();
            try
            {
                DateTime Date = DateTime.Now;
                if (ActivityDate != "")
                {
                    Date = DateTime.ParseExact(ActivityDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                var Month = Date.Month;
                var Year = Date.Year;
                var Day = Date.Day.ToString();
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    IQueryable<tbl_SightseeingRates> arrList = (from obj in db.tbl_SightseeingRates
                                                                join objSlot in db.tbl_SightseeingSlot on obj.nSlotID equals objSlot.Slot_Id
                                                                where obj.sMonth == Month.ToString() && obj.sYear == Year.ToString() && obj.sPaxType == "adult" //&& obj.ParentID == ParentID
                                                                select obj);
                    if (arrList.ToList().Count != 0)
                    {

                        foreach (var objRate in arrList.ToList())
                        {
                            string Rate = CutAdmin.DataLayer.ActivityManager.GetRateByDay(Day, objRate);
                            if (Rate != "")
                            {
                                if (!ListActivities.Exists(d => d.ActivityID == objRate.Activity_Id.ToString()))
                                {
                                    decimal Ammnt = Convert.ToDecimal(Rate);
                                    Ammnt = decimal.Round(Ammnt, 2, MidpointRounding.AwayFromZero);
                                    IQueryable<ActivityDetails> arrData = (from obj in db.tbl_SightseeingMaster
                                                                           where obj.Activity_Id == objRate.Activity_Id
                                                                           select new ActivityDetails
                                                                           {
                                                                               ActivityID = obj.Activity_Id.ToString(),
                                                                               Name = obj.Act_Name.ToString(),
                                                                               City = obj.City,
                                                                               Country = obj.Country,
                                                                               Description = obj.Description,
                                                                               TotalPrice = Ammnt.ToString(),
                                                                               Supplier = "ClicUrTrip",
                                                                               CurrencyClass = "AED",
                                                                               LocationID = obj.Location_Id,
                                                                               MinPax = obj.Min_Capacity,
                                                                               MaxPax = obj.Max_Capacity,
                                                                           });
                                    if (arrData.ToList().Count != 0)
                                    {
                                        ListActivities.Add(arrData.ToList().FirstOrDefault());
                                        IQueryable<String> arrImage = (from obj in db.tbl_SightseeingMaster
                                                                       where obj.Activity_Id == objRate.Activity_Id
                                                                       select obj.Act_Images);
                                        if (arrImage.ToList().Count != 0)
                                            ListActivities.LastOrDefault().ListImage = GetImages(objRate.Activity_Id, arrImage.ToList().FirstOrDefault());

                                        IQueryable<String> arrTour_Type = (from obj in db.tbl_SightseeingMaster
                                                                           where obj.Activity_Id == objRate.Activity_Id
                                                                           select obj.Tour_Type);
                                        if (arrTour_Type.ToList().Count != 0)
                                            ListActivities.LastOrDefault().List_Type = GetType(arrTour_Type.ToList().FirstOrDefault());

                                        ListActivities.LastOrDefault().LocationDetail = GetLocationDetail(arrData.ToList().FirstOrDefault().LocationID);
                                        IQueryable<tbl_SightseeingOperatingTime> Schedule = (from obj in db.tbl_SightseeingOperatingTime where obj.Activity_Id == objRate.Activity_Id select obj);
                                        if (Schedule.ToList().Count() != 0)
                                        {
                                            foreach (var objSchedule in Schedule)
                                            {
                                                DateTime openDate = DateTime.ParseExact(objSchedule.Operating_From, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                                DateTime closeDate = DateTime.ParseExact(objSchedule.Operating_Till, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                                if (Date >= openDate && Date <= closeDate)
                                                {
                                                    ListActivities.LastOrDefault().Opening = objSchedule.Opening_Time;
                                                    ListActivities.LastOrDefault().Closing = objSchedule.Closing_Time;
                                                    ListActivities.LastOrDefault().OperationDays = objSchedule.Operating_Days;
                                                }
                                                else
                                                {
                                                    ListActivities.LastOrDefault().OperationDays = "Daily";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            object Data = new object();
            if (ListActivities.Count != 0)
            {
                Data = new
                {
                    retCode = 1,
                    Activities = ListActivities,
                    arrTouType = GetTourType(),
                    PriceRange = GetPriceRange()
                };
            }
            else
                Data = new
                {
                    retCode = 0,
                    Massage = "No tours Found",
                };
            return Data;
        }

        public static LocationDetail GetLocationDetail(long? LocationID)
        {
            LocationDetail arrTypes = new LocationDetail();
            try
            {
                using (var DB = new Trivo_AmsHelper())
                {
                    LocationDetail arrList = (from obj in DB.tbl_Location
                                              where obj.Lid == LocationID
                                              select new LocationDetail { LocationName = obj.LocationName, Latitude = obj.Latitude, Longitutde = obj.Longitutde }).FirstOrDefault();
                    arrTypes = arrList;
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }

            return arrTypes;
        }

        #endregion


        public static List<RateTypes> GetRateType(Int32 ActivityID, string ActivityDate)
        {
            List<RateTypes> ListRateType = new List<RateTypes>();
            try
            {
                DateTime Date = DateTime.Now;
                if (ActivityDate != "")
                {
                    Date = DateTime.ParseExact(ActivityDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                var Month = Date.Month;
                var Year = Date.Year;
                var Day = Date.Day.ToString();
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    List<RateTypes> arrList = (from objRate in db.tbl_SightseeingRates
                                               join
                                               obj in db.tbl_SightseeingRatePlan on objRate.nPlanID equals obj.PlanID
                                               where obj.Activity_Id == ActivityID && objRate.sMonth == Month.ToString() && objRate.sYear == Year.ToString()
                                               select new RateTypes { Type = obj.RateType }).Distinct().ToList();

                    if (arrList.ToList().Count != 0)
                    {
                        foreach (var objType in arrList)
                        {
                            ListRateType.Add(objType);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            return ListRateType;
        }


        static List<string> arrMonth = new List<string> { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
        public static List<DateRange> DateArray(string ActivityDate, string Ratetype, Int32 ActivityID)
        {
            List<DateRange> Dates = new List<DateRange>();
            try
            {
                DateTime Date = DateTime.Now;
                if (ActivityDate != "")
                {
                    Date = DateTime.ParseExact(ActivityDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                for (int i = 0; i < 7; i++)
                {
                    int Day = 0;
                    using (var db = new CUT_LIVE_UATSTEntities())
                    {
                        DateTime dt = DateTime.Parse(Date.ToString()).AddDays(i);
                        var Month = dt.Month;
                        var Year = dt.Year;
                        Day = dt.Day;
                        bool Available = false;
                        IQueryable<tbl_SightseeingRates> List = (from obj in db.tbl_SightseeingRates
                                                                 join objtype in db.tbl_SightseeingRatePlan on obj.nPlanID equals objtype.PlanID
                                                                 where obj.Activity_Id == ActivityID && obj.sMonth == Month.ToString() && obj.sYear == Year.ToString() && obj.sPaxType == "adult" && objtype.RateType == Ratetype && objtype.InventoryType == "FreeSale"
                                                                 select obj).Distinct();
                        if (List.ToList().Count != 0)
                        {
                            foreach (var objRate in List.ToList())
                            {
                                string Rate = CutAdmin.DataLayer.ActivityManager.GetRateByDay(Day.ToString(), objRate);
                                if (Rate != "")
                                {
                                    Available = true;
                                }
                            }
                        }

                        Dates.Add(new DateRange
                        {
                            Month = arrMonth[dt.Month],
                            sDate = dt.Day.ToString(),
                            Day = dt.DayOfWeek.ToString().Substring(0, 3),
                            Available = Available,
                            fDate = dt.ToString("dd-MM-yyyy")
                        });

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            return Dates;
        }


        public static List<SightseeingLib.Slots> SlotByDate(string ActivityDate, Int32 ActivityID, string Ratetype)
        {
            List<SightseeingLib.Slots> arrSlots = new List<SightseeingLib.Slots>();
            try
            {
                DateTime Date = DateTime.Now;
                if (ActivityDate != "")
                {
                    Date = DateTime.ParseExact(ActivityDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                var Month = Date.Month;
                var Year = Date.Year;
                var Day = Date.Day;
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    IQueryable<SightseeingLib.Slots> arrList = (from obj in db.tbl_SightseeingRates
                                                                join objtype in db.tbl_SightseeingRatePlan on obj.nPlanID equals objtype.PlanID
                                                                join meta in db.tbl_SightseeingSlot on obj.nSlotID equals meta.Slot_Id
                                                                where obj.Activity_Id == ActivityID && obj.sMonth == Month.ToString() && obj.sYear == Year.ToString() && objtype.RateType == Ratetype
                                                                select new SightseeingLib.Slots { ID = meta.Slot_Id, SlotsStartTime = meta.Start_Time, SlotsEndTime = meta.End_Time }).Distinct();
                    if (arrList.ToList().Count != 0)
                    {
                        arrSlots = arrList.ToList();
                    }
                    else
                        throw new Exception("No Slot found");
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            return arrSlots;
        }

        //public static List<TicketType> RateBySlot(string ActivityDate, Int32 ActivityID, string Ratetype, Int32 SlotID)
        //{
        //    List<TicketType> arrRates = new List<TicketType>();
        //    try
        //    {
        //        DateTime Date = DateTime.Now;
        //        if (ActivityDate != "")
        //        {
        //            Date = DateTime.ParseExact(ActivityDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //        }
        //        var Month = Date.Month;
        //        var Year = Date.Year;
        //        var Day = Date.Day.ToString();
        //        using (var db = new CUT_LIVE_UATSTEntities())
        //        {

        //            List<TicketType> arrList = (from objRate in db.tbl_SightseeingRates
        //                                         join obj in db.tbl_SightseeingRatePlan on objRate.nPlanID equals obj.PlanID
        //                                         join objticket in db.tbl_aePriority on objRate.sActivityType equals objticket.Sid.ToString()
        //                                         where obj.Activity_Id == ActivityID && objRate.sMonth == Month.ToString() && objRate.nSlotID == SlotID && objRate.sYear == Year.ToString() && obj.RateType == Ratetype && obj.InventoryType == "FreeSale"
        //                                         select new TicketType { TicketID = objRate.sActivityType, Ticket = objticket.PriorityType, Pax = new PaxTyp { Pax_Type = objRate.sPaxType, Rate = "", Age = "" } }).Distinct().ToList();
        //            if (arrList.ToList().Count != 0)
        //            {
        //                foreach (var objDetail in arrList)
        //                {
        //                    objDetail.arrayPax = new List<PaxTyp>();
        //                    arrRates.Add(objDetail);
        //                    var arrrRate = (from obj in db.tbl_SightseeingRates
        //                                    where obj.Activity_Id == ActivityID && obj.sMonth == Month.ToString() && obj.nSlotID == SlotID && obj.sYear == Year.ToString() && obj.sPaxType == objDetail.Pax.Pax_Type
        //                                    select obj).FirstOrDefault();

        //                    arrRates.LastOrDefault().Pax.Rate = CutAdmin.DataLayer.ActivityManager.GetRateByDay(Day, arrrRate);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //    return arrRates;
        //}

        public static List<TicketType> GetRates(string ActivityDate, Int32 ActivityID, string Ratetype, Int32 SlotID)
        {
            List<TicketType> arrRates = new List<TicketType>();
            try
            {
                DateTime Date = DateTime.Now;
                if (ActivityDate != "")
                {
                    Date = DateTime.ParseExact(ActivityDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                var Month = Date.Month;
                var Year = Date.Year;
                var Day = Date.Day.ToString();
                using (var db = new CUT_LIVE_UATSTEntities())
                {

                    List<TicketType> arrList = (from objRate in db.tbl_SightseeingRates
                                                join obj in db.tbl_SightseeingRatePlan on objRate.nPlanID equals obj.PlanID
                                                join objticket in db.tbl_aePriority on objRate.sActivityType equals objticket.Sid.ToString()
                                                where obj.Activity_Id == ActivityID && objRate.sMonth == Month.ToString() && objRate.nSlotID == SlotID && objRate.sYear == Year.ToString() && obj.RateType == Ratetype && obj.InventoryType == "FreeSale"
                                                select new TicketType { TicketID = objRate.sActivityType, Ticket = objticket.PriorityType, Currency = obj.sCurrency, PlanId = obj.PlanID, nonRefundable = obj.nonRefundable, CancellationID = obj.CancelationID, noShowID = obj.nonShow,Inclusions=obj.Inclusions,Exclusions=obj.Exclusions }).Distinct().ToList();

                    if (arrList.ToList().Count != 0)
                    {
                        foreach (var objdetail in arrList)
                        {
                            arrRates.Add(objdetail);
                            arrRates.LastOrDefault().arrayPax = GetPaxDetails(Date, ActivityID, Ratetype, SlotID, objdetail.TicketID);
                            if (objdetail.nonRefundable != true)
                            {
                                List<Int64?> arrcancl = new List<Int64?>();
                                arrcancl.Add(objdetail.CancellationID);
                                arrcancl.Add(objdetail.noShowID);
                                arrRates.LastOrDefault().arrCancellationPolicy = GetCancellation(arrcancl);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            return arrRates;
        }

        public static List<CancellationPolicy> GetCancellation(List<Int64?> arrcancl)
        {
            List<CancellationPolicy> arrCancel = new List<CancellationPolicy>();
            try
            {
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    foreach (var obj in arrcancl)
                    {
                        List<CancellationPolicy> arrList = (from objcancel in db.tbl_SightseeingCancellationPolicy
                                                            where objcancel.CancelationID == obj
                                                            select new CancellationPolicy { RefundType = objcancel.RefundType, CancelationPolicy = objcancel.CancelationPolicy, ChargesType = objcancel.ChargesType, DaysPrior = objcancel.DaysPrior, IsDaysPrior = objcancel.IsDaysPrior, PercentageToCharge = objcancel.PercentageToCharge, AmountToCharge = objcancel.AmountToCharge }).Distinct().ToList();

                        if (arrList.Count() != 0)
                        {
                            foreach (var arr in arrList)
                            {
                                arrCancel.Add(arr);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                throw new Exception(ex.Message);
            }
            return arrCancel;
        }

        public static List<PaxTyp> GetPaxDetails(DateTime Date, Int32 ActivityID, string Ratetype, Int32 SlotID, string sActivityType)
        {
            List<PaxTyp> arrRates = new List<PaxTyp>();
            var Month = Date.Month;
            var Year = Date.Year;
            var Day = Date.Day.ToString();
            try
            {
                using (var db = new CUT_LIVE_UATSTEntities())
                {

                    List<PaxTyp> arrListt = (from objRate in db.tbl_SightseeingRates
                                             join obj in db.tbl_SightseeingRatePlan on objRate.nPlanID equals obj.PlanID
                                             where obj.Activity_Id == ActivityID && objRate.sMonth == Month.ToString() && objRate.nSlotID == SlotID && objRate.sYear == Year.ToString() && obj.RateType == Ratetype && obj.InventoryType == "FreeSale" && objRate.sActivityType == sActivityType
                                             select new PaxTyp { Pax_Type = objRate.sPaxType, Rate = "", Age = "" }).Distinct().ToList();
                    if (arrListt.ToList().Count != 0)
                    {
                        foreach (var objPax in arrListt)
                        {
                            arrRates.Add(objPax);
                            var arrrRate = (from obj in db.tbl_SightseeingRates
                                            where obj.Activity_Id == ActivityID && obj.sMonth == Month.ToString() && obj.nSlotID == SlotID && obj.sYear == Year.ToString() && obj.sPaxType == objPax.Pax_Type
                                            select obj).FirstOrDefault();
                            arrRates.LastOrDefault().Rate = CutAdmin.DataLayer.ActivityManager.GetRateByDay(Day, arrrRate);
                            arrRates.LastOrDefault().Age = GetAge(ActivityID, objPax.Pax_Type);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return arrRates;
        }

        public static string GetAge(Int32 ActivityID, string PaxType)
        {
            string ChildAge = "";
            try
            {
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    var arrChild = (from objAge in db.tbl_SightseeingChildPolicy
                                    where objAge.Activity_Id == ActivityID
                                    select objAge).Distinct().ToList();
                    if (arrChild.ToList().Count != 0)
                    {
                        if (PaxType == "child1")
                        {
                            ChildAge = arrChild[0].Child_Age_Upto + " to " + arrChild[0].Child_Age_From + " yrs";
                        }
                        else if (PaxType == "child2")
                        {
                            ChildAge = arrChild[0].Small_Child_Age_Upto + " to " + arrChild[0].Child_Age_Upto + " yrs";
                        }
                        else if (PaxType == "infant")
                        {
                            ChildAge = "Below " + arrChild[0].Small_Child_Age_Upto + " yrs";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return ChildAge;
        }

        public static List<tbl_Location> GetLocations(Int64 ParentID, string Name)
        {
            List<tbl_Location> ListLocation = new List<tbl_Location>();
            try
            {
                CUT_LIVE_UATSTEntities DBS = new CUT_LIVE_UATSTEntities();
                using (var db = new Trivo_AmsHelper())
                {

                    var arrList = (from obj in DBS.tbl_SightseeingMaster where obj.ParentID == ParentID && obj.Status == true select obj);

                    if (arrList.ToList().Count != 0)
                    {
                        foreach (var objRate in arrList.ToList())
                        {
                            if (!ListLocation.Exists(d => d.Lid == objRate.Location_Id))
                            {

                                IQueryable<tbl_Location> arrData = (from obj in db.tbl_Location
                                                                    where obj.Lid == objRate.Location_Id
                                                                    select obj);
                                if (arrData.ToList().Count!=0)
                                {
                                    ListLocation.Add(arrData.ToList().FirstOrDefault());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return ListLocation;
        }

        public static List<TourType> GetTourType()
        {
            List<TourType> arrTypes = new List<TourType>();
            using (var DB = new CUT_LIVE_UATSTEntities())
            {
                try
                {
                    if (ListActivities.Count != 0)
                    {
                        foreach (var ListAct in ListActivities)
                        {
                            var arrTour_Type = (from obj in DB.tbl_SightseeingMaster
                                                where obj.Activity_Id.ToString() == ListAct.ActivityID
                                                select obj.Tour_Type).FirstOrDefault();
                            if (arrTour_Type.ToList().Count != 0)
                            {
                                foreach (var Types in arrTour_Type.Split(';'))
                                {
                                    if (Types != "")
                                    {
                                        if (!arrTypes.Exists(x => x.Name == Types))
                                        {
                                            arrTypes.Add(new TourType
                                            {
                                                Name = Types,
                                                ActivityCount = 1,
                                                Icon = GetTypeIcon(Types)
                                            });
                                        }
                                        else
                                        {
                                            arrTypes.Where(x => x.Name == Types).FirstOrDefault().ActivityCount = arrTypes.Where(x => x.Name == Types).FirstOrDefault().ActivityCount + 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                }
            }
            return arrTypes;
        }

        public static string GetTypeIcon(string Type)
        {
            string Icon = "";
            using (var DB = new CUT_LIVE_UATSTEntities())
            {
                try
                {
                    var List = (from obj in DB.tbl_aeMode where obj.TourType == Type select obj.Images).Distinct().ToList();
                    if (List.ToList().Count != 0)
                    {
                        Icon = List.FirstOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                }
            }
            return Icon;
        }

        public static List<ActivityPriceRange> GetPriceRange()
        {
            List<ActivityPriceRange> PriceRange = new List<ActivityPriceRange>();
            using (var DB = new CUT_LIVE_UATSTEntities())
            {
                try
                {
                    List<double> range = new List<double>();
                    if (ListActivities.Count != 0)
                    {
                        foreach (var obj in ListActivities)
                        {
                            range.Add(Convert.ToDouble(obj.TotalPrice));
                        }
                        PriceRange.Add(new ActivityPriceRange
                        {
                            MinPrice = Convert.ToDecimal(range.Min()),
                            MaxPrice = Convert.ToDecimal(range.Max()),
                        });
                    }
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
                }
            }
            return PriceRange;
        }

        public static List<TourType> GetAllTourType(Int64 ParentID)
        {
            List<TourType> arrTypes = new List<TourType>();

            try
            {
                using (var DB = new CUT_LIVE_UATSTEntities())
                {
                    IQueryable<TourType> arrList = (from obj in DB.tbl_aeMode
                                                    where obj.ParentID == ParentID && obj.Status == true
                                                    select new TourType { ID = obj.Sid.ToString(), Name = obj.TourType, Icon = obj.Images });

                    if (arrList.Count() != 0)
                    {
                        arrTypes = arrList.Distinct().ToList();
                        foreach (var obj in arrList)
                        {
                            arrTypes.Add(obj);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }

            return arrTypes;
        }


        public static List<Image> GetImages(int? ActivityId)
        {
            List<Image> ListImages = new List<Image>();
            try
            {
                using (var DB = new CUT_LIVE_UATSTEntities())
                {
                    var arrList = (from obj in DB.tbl_SightseeingMaster
                                   where obj.Activity_Id == ActivityId
                                   select obj.Act_Images).FirstOrDefault();

                    if (arrList.ToList().Count != 0)
                    {
                        foreach (var Image in arrList.Split(','))
                        {
                            string ActImageURL = CutAdmin.DataLayer.AccountManager.GetAdminDomain();
                            if (Image != "")
                            {
                                ListImages.Add(new Image
                                {
                                    Title = Image.Replace(".jpg", " ").Replace(".png", " "),
                                    Url = ActImageURL + "/activityImages/" + ActivityId + "/" + Image
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return ListImages;
        }


        public static object Book(List<tbl_SightseeingBooking> arrBooking)
        {
            object Data = new object();
            try
            {
                Int64 ParentID = AccountManager.GetAdminByLogin();
                string BookingDate = DateTime.Today.ToString("dd-MM-yyyy");
                Random Gen = new Random();
                Int64 RandomNo = Gen.Next(100000, 999999);
                string InvoiceNo = "INC" + '-' + RandomNo.ToString();
                string VoucherNo = "VCH" + '-' + RandomNo.ToString();

                foreach (var Booking in arrBooking)
                {
                    Booking.Request_Id = RandomNo;
                    Booking.ParentID = ParentID;
                    Booking.IsPaid = true;
                    Booking.Invoice_No = InvoiceNo;
                    Booking.Voucher_No = VoucherNo;
                    Booking.Status = "Vouchered";
                    Booking.Booking_Date = BookingDate;

                    CUT_LIVE_UATSTEntities db = new CUT_LIVE_UATSTEntities();
                    using (var booksightseeing = db.Database.BeginTransaction())
                    {
                        try
                        {
                            db.tbl_SightseeingBooking.Add(Booking);
                            db.SaveChanges();
                            booksightseeing.Commit();
                            SightseeingInventoryManager.UpdateInventory(Booking);
                            Data = new
                            {
                                retCode = 1,
                                Massage = "Sightseeing book successfully",
                            };
                        }
                        catch (Exception ex)
                        {
                            Data = new
                            {
                                retCode = 0,
                                Massage = "Something went wrong",
                            };
                            booksightseeing.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }

            return Data;
        }

        public static List<BookinDetail> GetBookingList(string UserID)
        {
            List<BookinDetail> BookingList = new List<BookinDetail>();
            try
            {
                using (var DB = new CUT_LIVE_UATSTEntities())
                {
                    var arrList = (from obj in DB.tbl_SightseeingBooking
                                   join master in DB.tbl_SightseeingMaster on obj.Activity_Id equals master.Activity_Id
                                   where obj.User_Id == UserID && obj.BookingType=="B2C"
                                   select new BookinDetail {ActivityId=obj.Activity_Id.ToString(), BookDate=obj.Booking_Date,SightseeingDate=obj.Sightseeing_Date,Name=master.Act_Name,Adult=obj.Adults.ToString(),Child1=obj.Child1.ToString(),Child2=obj.Child2.ToString(),Infant=obj.Infant.ToString(),TotalPax=obj.TotalPax.ToString(),BookingID=obj.Request_Id.ToString(),TotalAmount=obj.TotalAmount.ToString(),Status=obj.Status }).Distinct().ToList();

                    if (arrList.ToList().Count != 0)
                    {
                        foreach (var Detail in arrList)
                        {
                                BookingList.Add(new BookinDetail
                                {
                                    ActivityId=Detail.ActivityId,
                                    BookDate = Detail.BookDate,
                                    SightseeingDate=Detail.SightseeingDate,
                                    Name=Detail.Name,
                                    ActImage= GetImages(Convert.ToInt32(Detail.ActivityId)),
                                    Adult =Detail.Adult,
                                    Child1=Detail.Child1,
                                    Child2=Detail.Child2,
                                    Infant=Detail.Infant,
                                    TotalPax=Detail.TotalPax,
                                    TotalAmount=Detail.TotalAmount,
                                    BookingID=Detail.BookingID,
                                    Status=Detail.Status
                                });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return BookingList;
        }
    }
}