﻿using CutAdmin.Webservices.Activity.DataLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.Models;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin.Webservices.Activity.Handler
{
    /// <summary>
    /// Summary description for GetActivityHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class GetActivityHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        #region Get Location
        [WebMethod(EnableSession = true)]
        public void GetLocations(Int64 ParentID, string Name)
        {
            try
            {
                var arrData = GetActivityManager.GetLocations(ParentID, Name);
                DTResult.setResponse(arrData);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }
        #endregion

        #region Search Activity
        [WebMethod(EnableSession = true)]
        public void SearchActivities(string LocationID, string Date, string TourType)
        {
            var arrData = GetActivityManager.GetActivitiesByDate(LocationID, Date);
            DTResult.setResponse(arrData);
        }
        #endregion

        #region  GetRateType
        [WebMethod(EnableSession = true)]
        public void GetRateType(Int32 ID, string ActivityDate)
        {
            try
            {
                var arrData = GetActivityManager.GetRateType(ID, ActivityDate);
                DTResult.setResponse(arrData);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }
        #endregion

        #region Date Range
        [WebMethod(EnableSession = true)]
        public void DateArray(Int32 ID, string Date, string Ratetype)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                var arrData = GetActivityManager.DateArray(Date, Ratetype, ID);
                DTResult.setResponse(arrData);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }
        #endregion

        #region  SlotByDate
        [WebMethod(EnableSession = true)]
        public void SlotByDate(string Type, string Date, Int32 ID)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                var arrData = GetActivityManager.SlotByDate(Date, ID, Type);
                DTResult.setResponse(arrData);

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }
        #endregion

        #region GetRates
        [WebMethod(EnableSession = true)]
        public void GetRates(string Type, int ID, int SlotID, string Date)
        {
            try
            {
                var arrData = GetActivityManager.GetRates(Date, ID, Type, SlotID);
                DTResult.setResponse(arrData);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }
        #endregion

        #region GetTourType
        [WebMethod(EnableSession = true)]
        public void GetTourType(Int64 ParentID)
        {
            try
            {
                var arrData = GetActivityManager.GetAllTourType(ParentID);
                DTResult.setResponse(arrData);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }
        #endregion

        #region GetImages
        [WebMethod(EnableSession = true)]
        public void GetImages(int ActivityId)
        {
            try
            {
                var arrData = GetActivityManager.GetImages(ActivityId);
                DTResult.setResponse(arrData);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }
        #endregion

        #region Book Activity
        CUT_LIVE_UATSTEntities db = new CUT_LIVE_UATSTEntities();
        [WebMethod(EnableSession = true)]
        public void Book(List<tbl_SightseeingBooking> arrBooking)
        {
            try
            {
                var arrData = GetActivityManager.Book(arrBooking);
                DTResult.setResponse(arrData);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }

        #endregion

        #region Booking List

        [WebMethod(EnableSession = true)]
        public void GetBookingList(string UserID)
        {
            try
            {
                var arrData = GetActivityManager.GetBookingList(UserID);
                DTResult.setResponse(arrData);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }

        #endregion
    }
}
