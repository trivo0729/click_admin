﻿using CutAdmin.dbml;
using CutAdmin.EntityModal;
using CutAdmin.Models;
using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CutAdmin.Webservices.Packages
{
    /// <summary>
    /// Summary description for PackagesHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class PackagesHandler : System.Web.Services.WebService
    {
        #region Search Package
        [WebMethod(EnableSession = true)]
        public void SearchPackages(long AdminID)
        {
            var arrData = PackagesManager.SearchPackages(AdminID);
            DTResult.setResponse(arrData);
        }

        [WebMethod(EnableSession = true)]
        public void PackageDetail(Int64 ID)
        {
            var arrData = PackagesManager.PackageDetail(ID);
            DTResult.setResponse(arrData);
        }
        #endregion

        #region Book Package
        [WebMethod(EnableSession = true)]
        public void BookPackage(tbl_PackageReservation arrBooking)
        {
            try
            {
                var arrData = PackagesManager.BookPackage(arrBooking);
                DTResult.setResponse(arrData);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }

        #endregion
    }
}
