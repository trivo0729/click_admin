﻿using CommonLib.Response;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
using CutAdmin.EntityModal;
using Elmah;
using PackageLib.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace CutAdmin.Webservices.Packages
{
    public class PackagesManager
    {
        static DateTime dtJourney;
        static List<string> arrThemes = new List<string> { "Holidays", "Umrah", "Hajj", "Honeymoon", "Summer", "Adventure", "Deluxe", "Business", "Premium", "Wildlife", "Weekend", "New Year" };
        static List<string> arrCategory = new List<string> { "Standard", "Deluxe", "Premium", "Luxury" };
        static List<string> Facilities = new List<string> { "Airfare with Airport transfer", "Sight Seeing", "Breakfast", "Tour Guide", "Lunch", "Dinner" };
        public static List<PackageDetails> SearchPackages(Int64 AdminID)
        {
            List<PackageDetails> arrPackageList = new List<PackageDetails>();
            try
            {
                using (var DB = new CUT_LIVE_UATSTEntities())
                {
                    IQueryable<tbl_Package> arrList = (from obj in DB.tbl_Package select obj);

                    if (arrList.ToList().Count != 0)
                    {
                        foreach (var Package in arrList)
                        {
                            if (DateTime.ParseExact(Package.dValidityFrom, "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture) <= DateTime.Now || DateTime.ParseExact(Package.dValidityTo, "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture) >= DateTime.Now)
                            {
                                arrPackageList.Add(new PackageDetails
                                {
                                    Packageid = Package.nID,
                                    PackageName = Package.sPackageName,
                                    Category = GetCategory(Package.sPackageCategory, Package.nID),
                                    ValidFrom = DateTime.ParseExact(Package.dValidityFrom, "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture),
                                    ValidTo = DateTime.ParseExact(Package.dValidityTo, "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture),
                                    City = Package.sPackageDestination,
                                    Images = ListImages(Package.nID, Package.sPackageName),
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return arrPackageList;
        }

        /*Packges Details*/
        public static List<PackageLib.Response.Category> GetCategory(string CategoryId, Int64 PackageID)
        {
            List<PackageLib.Response.Category> listCategory = new List<PackageLib.Response.Category>();
            try
            {
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    for (int i = 0; i < CategoryId.Split(',').ToList().Count; i++)
                    {
                        string sCategoryID = CategoryId.Split(',')[i];
                        if (sCategoryID != "")
                        {
                            var sCategory = (from obj in db.tbl_PackagePricing where obj.nCategoryID == Convert.ToInt16(sCategoryID) && obj.nPackageID == PackageID select obj).FirstOrDefault();
                            if (sCategory == null)
                                continue;
                            listCategory.Add(new PackageLib.Response.Category
                            {
                                Categoryname = arrCategory[Convert.ToInt16(sCategoryID) - 1],
                                CategoryID = Convert.ToInt16(sCategoryID),
                            });

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }

            return listCategory;
        }

        /*Package Images */
        public static List<CommonLib.Response.Image> ListImages(Int64 PackageID, string Tiltle)
        {
            List<CommonLib.Response.Image> ListImage = new List<CommonLib.Response.Image>();
            try
            {
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    var sImages = (from obj in db.tbl_PackageImages where obj.nPackageID == PackageID select obj).FirstOrDefault().ImageArray;
                    string[] arrImg = Regex.Split(sImages.Replace("_", "-"), @"\^\-\^");
                    foreach (var Img in arrImg.ToList())
                    {
                        bool isDefault = false;
                        if (sImages.IndexOf(Img) == 0)
                            isDefault = true;
                        ListImage.Add(new CommonLib.Response.Image
                        {
                            IsDefault = isDefault,
                            Title = Tiltle,
                            Type = "",
                            Url = CutAdmin.DataLayer.AccountManager.GetAdminDomain() + "/ImagesFolder/" + PackageID + "/" + Img.Replace("-", "_"),
                            Count = arrImg.ToList().Count

                        });
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return ListImage;
        }


        /*Get Details*/

        public static List<PackageLib.Response.PackageDetails> PackageDetail(Int64 ID)
        {
            List<PackageLib.Response.PackageDetails> PackageDetails = new List<PackageLib.Response.PackageDetails>();
            try
            {
                using (var DB = new CUT_LIVE_UATSTEntities())
                {
                    IQueryable<tbl_Package> arrList = (from obj in DB.tbl_Package where obj.nID == ID select obj);

                    if (arrList.ToList().Count != 0)
                    {
                        foreach (var Package in arrList)
                        {
                            PackageDetails.Add(new PackageDetails
                            {
                                ValidTo = DateTime.ParseExact(Package.dValidityTo, "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture),
                                Category = PackageCategory(Package.sPackageCategory, Package.nID, Package.nDuration),
                                City = Package.sPackageDestination,
                                Description = Package.sPackageDescription,
                                Images = ListImages(Package.nID, Package.sPackageName),
                                noDays = Convert.ToInt64(Package.nDuration),
                                Packageid = Package.nID,
                                PackageName = Package.sPackageName,
                                TermsCondition = Package.sTermsCondition,
                                Themes = GetThemes(Package.sPackageThemes),
                                ValidFrom = DateTime.ParseExact(Package.dValidityFrom, "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture),
                                PackageActivity = GetPacActivity(Package.nID),
                                PackageVehicle = GetPacVehicle(Package.nID),
                                Transfer = Convert.ToBoolean(Package.Transfer),
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return PackageDetails;
        }


        /*Packges Details*/
        public static List<PackageLib.Response.Category> PackageCategory(string CategoryId, Int64 PackageID, Int64 Duration)
        {
            List<PackageLib.Response.Category> listCategory = new List<PackageLib.Response.Category>();
            try
            {
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    for (int i = 0; i < CategoryId.Split(',').ToList().Count; i++)
                    {
                        string sCategoryID = CategoryId.Split(',')[i];
                        if (sCategoryID != "")
                        {
                            var sCategory = (from obj in db.tbl_PackagePricing where obj.nCategoryID.ToString() == sCategoryID && obj.nPackageID == PackageID select obj).FirstOrDefault();
                            if (sCategory == null)
                                continue;
                            listCategory.Add(new PackageLib.Response.Category
                            {
                                Itinerary = GetItinerary(sCategoryID, PackageID),
                                Categoryname = arrCategory[Convert.ToInt16(sCategoryID) - 1],
                                CategoryID = Convert.ToInt16(sCategoryID),
                                Inclusions = GetFacilities(sCategory.sStaticInclusions, sCategory.sDynamicInclusion, "Inclusions"),
                                Exclusion = GetFacilities(sCategory.sStaticExclusion, sCategory.sDynamicExclusion, "Exclusion"),
                                AdultPrice = Convert.ToSingle(sCategory.dSingleAdult),
                                TwinsSharing = Convert.ToSingle(sCategory.dCouple),
                                ChildPrice = Convert.ToSingle(sCategory.dKidWOBed),
                                InfantPrice = Convert.ToSingle(sCategory.dInfantKid),
                                ChildNoBedPrice = Convert.ToSingle(sCategory.dKidWBed),
                                ExtraAdult = Convert.ToSingle(sCategory.dExtraAdult),
                                TripleSharing = Convert.ToSingle(sCategory.dCouple + sCategory.dExtraAdult),
                                //TotalPrice = (Charge.Rate + Charge.ServiceTax + Charge.TDS + Charge.AgentMarkup),
                                PackageHotels = GetPackageHotels(PackageID, sCategoryID, Duration),
                            });
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return listCategory;
        }


        /* Dasign Packges Theme */
        public static List<string> GetThemes(string Themes)
        {
            List<string> sThemes = new List<string>();
            for (int i = 0; i < Themes.Split(',').ToList().Count; i++)
            {
                string sThemeID = Themes.Split(',')[i];
                if (sThemeID != "")
                    sThemes.Add(arrThemes[Convert.ToInt16(sThemeID)]);
            }
            return sThemes;
        }

        public static List<PackageLib.Response.PackageActivity> GetPacActivity(Int64 PackageId)
        {
            List<PackageLib.Response.PackageActivity> listPackageActivity = new List<PackageLib.Response.PackageActivity>();
            try
            {
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    var pActivity = (from obj in db.tbl_PackageActivity
                                     join objact in db.tbl_SightseeingMaster on obj.ActivityId equals objact.Activity_Id
                                     where obj.PackageId == PackageId
                                     select new
                                     {
                                         obj.ActivityId,
                                         objact.Act_Name,
                                     }).ToList();

                    foreach (var item in pActivity)
                    {
                        listPackageActivity.Add(new PackageLib.Response.PackageActivity
                        {
                            PackageActivityID = item.ActivityId,
                            PackageActivityName = item.Act_Name,
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return listPackageActivity;
        }

        public static List<PackageLib.Response.PackageVehicle> GetPacVehicle(Int64 PackageId)
        {
            List<PackageLib.Response.PackageVehicle> listPackageVehicle = new List<PackageLib.Response.PackageVehicle>();
            try
            {
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    var transDB = new TransfersHelperDataContext();
                    var one = db.tbl_PackageCab.AsEnumerable();
                    var two = transDB.tbl_VehicleModels.AsEnumerable();
                    var pVahecle = (from obj in one
                                    join meta in two on obj.ModelId equals meta.ID
                                    where obj.PackageId == PackageId
                                    select new
                                    {
                                        obj.Id,
                                        obj.VehicleName,
                                        obj.Price,
                                        meta.SeattingCapacity
                                    }).ToList();
                    foreach (var item in pVahecle)
                    {
                        listPackageVehicle.Add(new PackageLib.Response.PackageVehicle
                        {
                            vehicleID = item.Id,
                            VehicleName = item.VehicleName,
                            VehiclePrice = Convert.ToDecimal(item.Price),
                            SeatCapacity = item.SeattingCapacity,
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return listPackageVehicle;
        }

        /*Package Itinerary*/
        public static List<PackageLib.Response.Itinerary> GetItinerary(string CategoryID, Int64 PackageID)
        {
            List<PackageLib.Response.Itinerary> listItinerary = new List<PackageLib.Response.Itinerary>();
            try
            {
                using (var db = new CUT_LIVE_UATSTEntities())
                {

                    var sItinerary = (from obj in db.tbl_Itinerary where obj.nPackageID == PackageID && obj.nCategoryID == Convert.ToInt64(CategoryID) select obj).FirstOrDefault();

                    if (sItinerary.sItinerary_1 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_1, 1, PackageID, CategoryID));
                    }
                    if (sItinerary.sItinerary_2 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_2, 2, PackageID, CategoryID));
                    }
                    if (sItinerary.sItinerary_3 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_3, 3, PackageID, CategoryID));
                    }
                    if (sItinerary.sItinerary_4 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_4, 4, PackageID, CategoryID));
                    }
                    if (sItinerary.sItinerary_5 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_5, 5, PackageID, CategoryID));
                    }
                    if (sItinerary.sItinerary_6 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_6, 6, PackageID, CategoryID));
                    }
                    if (sItinerary.sItinerary_7 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_7, 7, PackageID, CategoryID));
                    }
                    if (sItinerary.sItinerary_8 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_8, 8, PackageID, CategoryID));
                    }
                    if (sItinerary.sItinerary_9 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_9, 9, PackageID, CategoryID));
                    }
                    if (sItinerary.sItinerary_10 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_10, 10, PackageID, CategoryID));
                    }
                    if (sItinerary.sItinerary_11 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_11, 11, PackageID, CategoryID));
                    }
                    if (sItinerary.sItinerary_12 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_12, 12, PackageID, CategoryID));
                    }
                    if (sItinerary.sItinerary_13 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_13, 13, PackageID, CategoryID));
                    }
                    if (sItinerary.sItinerary_14 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_14, 14, PackageID, CategoryID));
                    }
                    if (sItinerary.sItinerary_15 != "")
                    {
                        listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_15, 15, PackageID, CategoryID));
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return listItinerary;
        }

        public static PackageLib.Response.Itinerary GenrateItinerary(string Itinerary, int noDays, Int64 PackageID, string CategoryID)
        {
            PackageLib.Response.Itinerary objItinerary = new PackageLib.Response.Itinerary();
            try
            {
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    string[] sData = Regex.Split(Itinerary, @"\^\-\^");
                    var arrPackageHotel = (from obj in db.tbl_ItineraryHotel where obj.nPackageID == PackageID && obj.nCategoryID == Convert.ToInt64(CategoryID) select obj).FirstOrDefault();
                    objItinerary.noDay = noDays;
                    objItinerary.Date = dtJourney.AddDays(noDays);
                    objItinerary.Description = sData[2];
                    objItinerary.HotelDetails = new PackageLib.Response.HotelDetails();
                    string Description = "";
                    string HotelName = "";
                    string HotelCode = "";
                    if (noDays == 1)
                        HotelCode = arrPackageHotel.sHotel_Code_1;
                    Description = arrPackageHotel.sHotelDescrption_1;
                    HotelName = arrPackageHotel.sHotelName_1;
                    if (noDays == 2)
                    {
                        HotelCode = arrPackageHotel.sHotel_Code_2;
                        Description = arrPackageHotel.sHotelDescrption_2;
                        HotelName = arrPackageHotel.sHotelName_2;
                    }
                    if (noDays == 3)
                    {
                        HotelCode = arrPackageHotel.sHotel_Code_3;
                        Description = arrPackageHotel.sHotelDescrption_3;
                        HotelName = arrPackageHotel.sHotelName_3;
                    }

                    if (noDays == 4)
                    {
                        HotelCode = arrPackageHotel.sHotel_Code_4;
                        Description = arrPackageHotel.sHotelDescrption_4;
                        HotelName = arrPackageHotel.sHotelName_4;
                    }
                    if (noDays == 5)
                    {
                        HotelCode = arrPackageHotel.sHotel_Code_5;
                        Description = arrPackageHotel.sHotelDescrption_5;
                        HotelName = arrPackageHotel.sHotelName_5;
                    }
                    if (noDays == 6)
                    {
                        HotelCode = arrPackageHotel.sHotel_Code_6;
                        Description = arrPackageHotel.sHotelDescrption_6;
                        HotelName = arrPackageHotel.sHotelName_6;
                    }
                    if (noDays == 7)
                    {
                        HotelCode = arrPackageHotel.sHotel_Code_7;
                        Description = arrPackageHotel.sHotelDescrption_7;
                        HotelName = arrPackageHotel.sHotelName_7;
                    }
                    if (noDays == 8)
                    {
                        HotelCode = arrPackageHotel.sHotel_Code_8;
                        Description = arrPackageHotel.sHotelDescrption_8;
                        HotelName = arrPackageHotel.sHotelName_8;
                    }
                    if (noDays == 9)
                    {
                        HotelCode = arrPackageHotel.sHotel_Code_9;
                        Description = arrPackageHotel.sHotelDescrption_9;
                        HotelName = arrPackageHotel.sHotelName_9;
                    }
                    if (noDays == 10)
                    {
                        HotelCode = arrPackageHotel.sHotel_Code_10;
                        Description = arrPackageHotel.sHotelDescrption_10;
                        HotelName = arrPackageHotel.sHotelName_10;
                    }
                    if (noDays == 11)
                    {
                        HotelCode = arrPackageHotel.sHotel_Code_11;
                        Description = arrPackageHotel.sHotelDescrption_11;
                        HotelName = arrPackageHotel.sHotelName_11;
                    }
                    if (noDays == 12)
                    {
                        HotelCode = arrPackageHotel.sHotel_Code_12;
                        Description = arrPackageHotel.sHotelDescrption_12;
                        HotelName = arrPackageHotel.sHotelName_12;
                    }
                    if (noDays == 13)
                    {
                        HotelCode = arrPackageHotel.sHotel_Code_13;
                        Description = arrPackageHotel.sHotelDescrption_13;
                        HotelName = arrPackageHotel.sHotelName_13;
                    }
                    if (noDays == 14)
                    {
                        HotelCode = arrPackageHotel.sHotel_Code_14;
                        Description = arrPackageHotel.sHotelDescrption_14;
                        HotelName = arrPackageHotel.sHotelName_14;
                    }
                    helperDataContext sdb = new helperDataContext();
                    var hotel = sdb.HOTELs.AsEnumerable();
                    var arrDetails = (from obj in hotel where obj.HotelCode == HotelCode select obj).FirstOrDefault();
                    objItinerary.HotelDetails.Details = new CommonLib.Response.CommonHotelDetails
                    {
                        Address = "",
                        Category = arrDetails.CategoryCode,    //Hotel Rsting
                        Description = Description,
                        Facility = new List<CommnFacilities>(),
                        HotelId = HotelCode,
                        HotelName = HotelName,
                        Langitude = "",
                        Latitude = "",
                        Image = new List<Image>(),
                        UserRating = 0,
                    };
                    var arrImageDetails = (from obj in db.tbl_HotelImages where obj.nPackageID == PackageID && obj.nCategoryID == Convert.ToInt64(CategoryID) select obj).ToList();
                    string ImageUrl = "";
                    if (noDays == 1)
                        ImageUrl = arrImageDetails[0].sHotelImage1;
                    if (noDays == 2)
                    {
                        ImageUrl = arrImageDetails[0].sHotelImage2;
                    }
                    if (noDays == 3)
                    {
                        ImageUrl = arrImageDetails[0].sHotelImage3;
                    }

                    if (noDays == 4)
                    {
                        ImageUrl = arrImageDetails[0].sHotelImage4;
                    }
                    if (noDays == 5)
                    {
                        ImageUrl = arrImageDetails[0].sHotelImage5;
                    }
                    if (noDays == 6)
                    {
                        ImageUrl = arrImageDetails[0].sHotelImage6;
                    }
                    if (noDays == 7)
                    {
                        ImageUrl = arrImageDetails[0].sHotelImage7;
                    }
                    if (noDays == 8)
                    {
                        ImageUrl = arrImageDetails[0].sHotelImage8;
                    }
                    if (noDays == 9)
                    {
                        ImageUrl = arrImageDetails[0].sHotelImage9;
                    }
                    if (noDays == 10)
                    {
                        ImageUrl = arrImageDetails[0].sHotelImage10;
                    }
                    if (noDays == 11)
                    {
                        ImageUrl = arrImageDetails[0].sHotelImage11;
                    }
                    if (noDays == 12)
                    {
                        ImageUrl = arrImageDetails[0].sHotelImage12;
                    }
                    if (noDays == 13)
                    {
                        ImageUrl = arrImageDetails[0].sHotelImage13;
                    }
                    if (noDays == 14)
                    {
                        ImageUrl = arrImageDetails[0].sHotelImage14;
                    }
                    List<Image> Images = new List<Image>();
                    objItinerary.HotelDetails.Details.Image.Add(new Image
                    {
                        Url = ImageUrl,
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return objItinerary;
        }

        /* Facilities */
        public static List<PackageLib.Response.Facility> GetFacilities(string static_Facilities, string Dynamic_Facilities, string Type)
        {
            List<PackageLib.Response.Facility> ListFacilities = new List<PackageLib.Response.Facility>();
            try
            {
                for (int i = 0; i < static_Facilities.Split(',').ToList().Count; i++)
                {
                    string sID = static_Facilities.Split(',')[i];
                    string sName = "";
                    if (sID != "")
                    {
                        sName = Facilities[Convert.ToInt16(sID) - 1];

                        ListFacilities.Add(new PackageLib.Response.Facility
                        {
                            ID = Convert.ToInt16(sID),
                            Name = sName,
                            Type = Type

                        });
                    }

                }

                for (int i = 0; i < Dynamic_Facilities.Split(',').ToList().Count; i++)
                {
                    if (Dynamic_Facilities.Split(',')[i] != "")
                    {
                        ListFacilities.Add(new PackageLib.Response.Facility
                        {
                            ID = ListFacilities.Count() + i,
                            Name = Dynamic_Facilities.Split(',')[i],
                            Type = Type

                        });
                    }

                }

            }
            catch(Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return ListFacilities;
        }

        public static List<PackageLib.Response.PackageHotels> GetPackageHotels(Int64 PackageID, string sCategoryID, Int64 Duration)
        {
            List<PackageLib.Response.PackageHotels> ListPackageHotels = new List<PackageLib.Response.PackageHotels>();
            try
            {
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    var arrPackageHotelDetails = (from obj in db.tbl_ItineraryHotel where obj.nPackageID == PackageID && obj.nCategoryID == Convert.ToInt64(sCategoryID) select obj).FirstOrDefault();

                    string Description = "";
                    string HotelName = "";
                    string HotelCode = "";
                    for (int i = 0; i < Duration; i++)
                    {
                        if (i == 0)
                            HotelCode = arrPackageHotelDetails.sHotel_Code_1;
                        Description = arrPackageHotelDetails.sHotelDescrption_1;
                        HotelName = arrPackageHotelDetails.sHotelName_1;
                        if (i == 1)
                        {
                            HotelCode = arrPackageHotelDetails.sHotel_Code_2;
                            Description = arrPackageHotelDetails.sHotelDescrption_2;
                            HotelName = arrPackageHotelDetails.sHotelName_2;
                        }
                        if (i == 2)
                        {
                            HotelCode = arrPackageHotelDetails.sHotel_Code_3;
                            Description = arrPackageHotelDetails.sHotelDescrption_3;
                            HotelName = arrPackageHotelDetails.sHotelName_3;
                        }

                        if (i == 3)
                        {
                            HotelCode = arrPackageHotelDetails.sHotel_Code_4;
                            Description = arrPackageHotelDetails.sHotelDescrption_4;
                            HotelName = arrPackageHotelDetails.sHotelName_4;
                        }
                        if (i == 4)
                        {
                            HotelCode = arrPackageHotelDetails.sHotel_Code_5;
                            Description = arrPackageHotelDetails.sHotelDescrption_5;
                            HotelName = arrPackageHotelDetails.sHotelName_5;
                        }
                        if (i == 5)
                        {
                            HotelCode = arrPackageHotelDetails.sHotel_Code_6;
                            Description = arrPackageHotelDetails.sHotelDescrption_6;
                            HotelName = arrPackageHotelDetails.sHotelName_6;
                        }
                        if (i == 6)
                        {
                            HotelCode = arrPackageHotelDetails.sHotel_Code_7;
                            Description = arrPackageHotelDetails.sHotelDescrption_7;
                            HotelName = arrPackageHotelDetails.sHotelName_7;
                        }
                        if (i == 7)
                        {
                            HotelCode = arrPackageHotelDetails.sHotel_Code_8;
                            Description = arrPackageHotelDetails.sHotelDescrption_8;
                            HotelName = arrPackageHotelDetails.sHotelName_8;
                        }
                        if (i == 8)
                        {
                            HotelCode = arrPackageHotelDetails.sHotel_Code_9;
                            Description = arrPackageHotelDetails.sHotelDescrption_9;
                            HotelName = arrPackageHotelDetails.sHotelName_9;
                        }
                        if (i == 9)
                        {
                            HotelCode = arrPackageHotelDetails.sHotel_Code_10;
                            Description = arrPackageHotelDetails.sHotelDescrption_10;
                            HotelName = arrPackageHotelDetails.sHotelName_10;
                        }
                        if (i == 10)
                        {
                            HotelCode = arrPackageHotelDetails.sHotel_Code_11;
                            Description = arrPackageHotelDetails.sHotelDescrption_11;
                            HotelName = arrPackageHotelDetails.sHotelName_11;
                        }
                        if (i == 11)
                        {
                            HotelCode = arrPackageHotelDetails.sHotel_Code_12;
                            Description = arrPackageHotelDetails.sHotelDescrption_12;
                            HotelName = arrPackageHotelDetails.sHotelName_12;
                        }
                        if (i == 12)
                        {
                            HotelCode = arrPackageHotelDetails.sHotel_Code_13;
                            Description = arrPackageHotelDetails.sHotelDescrption_13;
                            HotelName = arrPackageHotelDetails.sHotelName_13;
                        }
                        if (i == 13)
                        {
                            HotelCode = arrPackageHotelDetails.sHotel_Code_14;
                            Description = arrPackageHotelDetails.sHotelDescrption_14;
                            HotelName = arrPackageHotelDetails.sHotelName_14;
                        }
                        helperDataContext sdb = new helperDataContext();
                        var hotel = sdb.HOTELs.AsEnumerable();
                        var arrPHotelDetails = (from obj in hotel where obj.HotelCode == HotelCode select obj).FirstOrDefault();
                        ListPackageHotels.Add(new PackageLib.Response.PackageHotels
                        {

                            Ratings = arrPHotelDetails.CategoryCode,    //Hotel Rsting
                            Description = Description,
                            HotelId = HotelCode,
                            HotelName = HotelName,
                            PackHotelImage = new List<PackageLib.Response.PackHotelImage>(),

                        });
                    }

                    var arrPackageImageDetails = (from obj in db.tbl_HotelImages where obj.nPackageID == PackageID && obj.nCategoryID == Convert.ToInt64(sCategoryID) select obj).ToList();
                    string ImageUrl = "";
                    for (int j = 0; j < Duration; j++)
                    {
                        if (j == 0)
                            ImageUrl = arrPackageImageDetails[0].sHotelImage1;
                        if (j == 1)
                        {
                            ImageUrl = arrPackageImageDetails[0].sHotelImage2;
                        }
                        if (j == 2)
                        {
                            ImageUrl = arrPackageImageDetails[0].sHotelImage3;
                        }

                        if (j == 3)
                        {
                            ImageUrl = arrPackageImageDetails[0].sHotelImage4;
                        }
                        if (j == 4)
                        {
                            ImageUrl = arrPackageImageDetails[0].sHotelImage5;
                        }
                        if (j == 5)
                        {
                            ImageUrl = arrPackageImageDetails[0].sHotelImage6;
                        }
                        if (j == 6)
                        {
                            ImageUrl = arrPackageImageDetails[0].sHotelImage7;
                        }
                        if (j == 7)
                        {
                            ImageUrl = arrPackageImageDetails[0].sHotelImage8;
                        }
                        if (j == 8)
                        {
                            ImageUrl = arrPackageImageDetails[0].sHotelImage9;
                        }
                        if (j == 9)
                        {
                            ImageUrl = arrPackageImageDetails[0].sHotelImage10;
                        }
                        if (j == 10)
                        {
                            ImageUrl = arrPackageImageDetails[0].sHotelImage11;
                        }
                        if (j == 11)
                        {
                            ImageUrl = arrPackageImageDetails[0].sHotelImage12;
                        }
                        if (j == 12)
                        {
                            ImageUrl = arrPackageImageDetails[0].sHotelImage13;
                        }
                        if (j == 13)
                        {
                            ImageUrl = arrPackageImageDetails[0].sHotelImage14;
                        }

                        //foreach (var item in ListPackageHotels)
                        //{
                        List<PackageLib.Response.PackHotelImage> Images = new List<PackageLib.Response.PackHotelImage>();

                        ListPackageHotels[j].PackHotelImage.Add(new PackageLib.Response.PackHotelImage
                        {
                            Url = ImageUrl,
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return ListPackageHotels;
        }

        public static object BookPackage(tbl_PackageReservation arrBooking)
        {
            object Data = new object();
            try
            {
                string BookingDate = DateTime.Today.ToString("dd-MM-yyyy");
                Int64 ParentID = AccountManager.GetAdminByLogin();
                arrBooking.BookingDate = BookingDate;
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    try
                    {
                        IQueryable<tbl_PackageReservation> List = (from obj in db.tbl_PackageReservation where obj.ParentId == ParentID select obj);
                        string RefNo = "PQ-" + List.ToList().Count;
                        arrBooking.RefNo = RefNo;
                        db.tbl_PackageReservation.Add(arrBooking);
                        db.SaveChanges();
                        Data = new
                        {
                            retCode = 1,
                            Massage = "Package book successfully",
                            RefNo= RefNo,
                        };
                    }
                    catch (Exception ex)
                    {
                        Data = new
                        {
                            retCode = 0,
                            Massage = "Something went wrong",
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return Data;
        }
    }
}