﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.EntityModal;
using CutAdmin.dbml;
namespace CutAdmin.Webservices.Authorize.service
{
    public class UserManager
    {
        public class UserDetails
        {
            public long sid { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public long? ContactID { get; set; }
            public long? ParentId { get; set; }
            public string Password { get; set; }
            public string Remarks { get; set; }
            public ContacDetails ContacDetails { get; set; }
        }

        public class  ContacDetails {
            public string Mobile { get; set; }
            public string Phone { get; set; }
            public string CitiesID { get; set; }
            public string CountryID { get; set; }
            public string Address { get; set; }
        }

        public static object userlogin(string sUserName ,string sPassword, long sParentID)
        {
            var Data = new object();
            UserDetails arrLogin = new UserDetails();
            try
            {
                using (var DB =   new CUT_LIVE_UATSTEntities())
                {
                    string sEncryptedPassword = string.Empty;
                    if(sPassword != "")
                        sEncryptedPassword = CutAdmin.Common.Cryptography.EncryptText(sPassword);
                    IQueryable<UserDetails> arrData =
                                    (from obj in DB.tbl_B2C_CustomerLogin
                                     where obj.Email == sUserName &&  (sPassword !="" || obj.Password == sEncryptedPassword )
                                     select new UserDetails
                                     {
                                         sid = obj.Sid,
                                         Name = obj.Name,
                                         Email = obj.Email,
                                         Password = sPassword,
                                         ContactID = obj.ContactID
                                     });
                    if(arrData.Count() != 0)
                    {
                        arrLogin = arrData.ToList().FirstOrDefault();
                        using (var dbAdmin = new helperDataContext())
                        {
                            var arrContact = new ContacDetails();
                            arrLogin.ContacDetails = new ContacDetails();
                            if (arrLogin.ContactID != null)
                            {
                                arrContact = (from obj in dbAdmin.tbl_Contacts
                                              join  objCity in dbAdmin.tbl_HCities on obj.Code equals objCity.Code
                                              select new ContacDetails
                                                {
                                                  Address = obj.Address,
                                                  CitiesID = objCity.Description,
                                                  CountryID = objCity.Countryname,
                                                  Mobile = obj.Mobile,
                                                  Phone = obj.phone,
                                              }).FirstOrDefault();
                                if(arrContact != null)
                                {
                                    arrLogin.ContacDetails = arrContact;
                                }
                                             
                            }
                        }
                        Data = new
                        {
                            retCode = 1,
                            LoginDetail = arrLogin,
                        };
                    }
                    else
                    {
                        Data = new
                        {
                            retCode = 0,
                            Message = "Username or Password Not Match..",
                        };
                    }
                }

            }
            catch (Exception ex)
            {
                Data = new
                {
                    retCode = 0,
                    Message = "Unable to login",
                };
            }
            return Data;
        }

        public static object SignUp(UserDetails arrLogin)
        {
            var data = new object();
            try
            {
                using (var DB = new CUT_LIVE_UATSTEntities())
                {
                if (!DB.tbl_B2C_CustomerLogin.ToList().Exists(d=>d.Email== arrLogin.Email))
                {
                        tbl_Contact Contact = new tbl_Contact
                        {
                            Address = arrLogin.ContacDetails.Address,
                            Code = arrLogin.ContacDetails.CitiesID,
                            email = arrLogin.Email,
                            Mobile = arrLogin.ContacDetails.Mobile,
                            phone = arrLogin.ContacDetails.Phone,
                            sCountry = arrLogin.ContacDetails.CountryID,
                        };
                    using (var dbAdmin = new helperDataContext())
                    {
                        dbAdmin.tbl_Contacts.InsertOnSubmit(Contact);
                        dbAdmin.SubmitChanges();
                    }
                        string sEncryptedPassword = CutAdmin.DataLayer.LocationManager.RandomString(8);
                        tbl_B2C_CustomerLogin Login = new tbl_B2C_CustomerLogin
                        {
                            Name = arrLogin.Name,
                            Email = arrLogin.Email,
                            Address = Contact.Address,
                            Agent_Id = arrLogin.ParentId,
                            CreatedDate = DateTime.Now.ToString("dd-MM-yyyy"),
                            Flag = true,
                            Password = sEncryptedPassword,
                            UpdatedDate = DateTime.Now.ToString("dd-MM-yyyy"),
                            ContactID = Contact.ContactID
                        };
                        arrLogin.Password = sEncryptedPassword;
                        DB.tbl_B2C_CustomerLogin.Add(Login);
                        DB.SaveChanges();
                        data = new
                        {
                            retCode = 1,
                            sid = arrLogin.sid
                        };
                }
                else
                    {
                        data = new
                        {
                            retCode = 0,
                            Meassge = "username already taken"
                        };
                    }
                }

            }
            catch (Exception ex)
            {
                data = new
                {
                    retCode = 0,
                    Meassge = ex.Message
                };
            }
            return data;
        }

        public static object ValidLogin(string UserName,long ParentID)
        {
            var Data = new object();
            try
            {
                using (var DB = new CUT_LIVE_UATSTEntities())
                {
                    if (!DB.tbl_B2C_CustomerLogin.ToList().Exists(d => d.Email == UserName))
                    {
                        Data = new
                        {
                            retCode = 1,
                        };
                    }
                    else
                    {
                        Data = new
                        {
                            retCode = 0,
                            Message = "Email already taken",
                        };
                    }
                }
            }
            catch (Exception ex)
            {

                Data = new
                {
                    retCode = 0,
                    Message = "Please Try again later",
                };
            }
            return Data;
        }
    }
}