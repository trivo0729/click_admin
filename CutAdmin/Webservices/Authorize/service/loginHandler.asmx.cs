﻿using CUT.Admin;
using CutAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CutAdmin.Webservices.Authorize.service
{
    /// <summary>
    /// Summary description for loginHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class loginHandler : System.Web.Services.WebService
    {
        #region Userlogin
        [WebMethod(EnableSession = true)]
        public void Userlogin(string UserName, string Password, long sParentID)
        {
            try
            {
                var arrData = UserManager.userlogin(UserName, Password, sParentID);
                DTResult.setResponse(arrData);
            }
            catch (Exception ex)
            {
            }
        }

        [WebMethod(EnableSession = true)]
        public void SignUp(UserManager.UserDetails arrLogin)
        {
            try
            {
                var arrData = UserManager.SignUp(arrLogin);
                DTResult.setResponse(arrData);
            }
            catch (Exception ex)
            {
            }
        }
        #endregion
    }
}
