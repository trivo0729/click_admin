﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
using CutAdmin.EntityModal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CutAdmin
{
    public partial class ViewFlightTicket : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string CurrencyClass = "";
            string ReservationID = Request.QueryString["ReservationId"];

            //CUT.DataLayer.BookingManager.GenrateCancellationMail(ReservationID, 14794, "FR");
            //CUT.Agent.DataLayer.AutoEmailOnBooking.SendMail(ReservationID);
            string AllPassengers = "";
            if (ReservationID != "")
            {
                Int64 Uid = Convert.ToInt64(Request.QueryString["Uid"]);
                string invoice = "";
                maincontainer.InnerHtml = GetFlightTicket(ReservationID, Uid, out invoice);
                this.Title = invoice;
                string Tkt = maincontainer.InnerHtml;
                HttpContext.Current.Session["Ticket"] = Tkt;

            }
        }


        public static string GetFlightTicket(string ReservationID, Int64 Uid, out string title)
        {
            using (var DB = new Click_Hotel())
            {

                using (var db = new helperDataContext())
                {

                    string CurrencyClass = "";
                    title = "";
                    // CUT.Agent.DataLayer.AutoEmailOnBooking.SendMail(ReservationID);
                    string AllPassengers = "";
                    StringBuilder sb = new StringBuilder();
                    if (ReservationID != "")
                    {

                        Int64 ContactID = 0;
                        DataSet ds = new DataSet();
                        Int64 ParentID = AccountManager.GetSupplierByUser();
                        ContactID = db.tbl_AdminLogins.Where(d => d.sid == ParentID).FirstOrDefault().ContactID;
                        DataTable dtHotelReservation, dtBookedPassenger, dtBookedRoom, dtBookingTransactions, dtAgentDetail;
                        CUT.DataLayer.GlobalDefault objGlobalDefaults = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];

                        string Night = "";
                        SqlParameter[] SQLParams = new SqlParameter[2];
                        SQLParams[0] = new SqlParameter("@ReservationID", ReservationID);
                        SQLParams[1] = new SqlParameter("@sid", Uid);
                        DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_FlightBookingDetails", out ds, SQLParams);
                        string Logo = HttpContext.Current.Session["logo"].ToString();
                        dtHotelReservation = ds.Tables[0];
                        dtBookedPassenger = ds.Tables[1];
                        dtBookedRoom = ds.Tables[2];
                        dtBookingTransactions = ds.Tables[3];
                        dtAgentDetail = ds.Tables[4];
                        var dtSupplierDetails = (from obj in db.tbl_AdminLogins
                                                 join objc in db.tbl_Contacts on obj.ContactID equals objc.ContactID
                                                 from objh in db.tbl_HCities
                                                 where obj.ContactID == ContactID && objc.Code == objh.Code
                                                 select new
                                                 {
                                                     obj.AgencyName,
                                                     obj.Agentuniquecode,
                                                     objc.Address,
                                                     objc.email,
                                                     objc.Mobile,
                                                     objc.phone,
                                                     objc.PinCode,
                                                     objc.Fax,
                                                     objc.sCountry,
                                                     objc.Website,
                                                     objc.StateID,
                                                     objh.Countryname,
                                                     objh.Description
                                                 }).FirstOrDefault();
                        if (objGlobalDefaults.UserType == "Agent")
                        {
                            CurrencyClass = (HttpContext.Current.Session["CurrencyClass"]).ToString();
                            Uid = objGlobalDefaults.sid;
                        }
                        else
                        {
                            CurrencyClass = dtAgentDetail.Rows[0]["CurrencyCode"].ToString();
                            switch (CurrencyClass)
                            {
                                case "AED":
                                    CurrencyClass = "Currency-AED";

                                    break;
                                case "SAR":
                                    CurrencyClass = "Currency-SAR";
                                    break;
                                case "MYR":
                                    CurrencyClass = "Currency-MYR";
                                    break;
                                case "EUR":
                                    CurrencyClass = "fa fa-eur";
                                    break;
                                case "GBP":
                                    CurrencyClass = "fa fa-gbp";
                                    break;
                                case "USD":
                                    CurrencyClass = "fa fa-dollar";
                                    break;
                                case "INR":
                                    CurrencyClass = "fa fa-inr";
                                    break;
                            }

                        }
                        string ReservationDate = dtHotelReservation.Rows[0]["LastTicketDate"].ToString();
                        ReservationDate = ReservationDate.Replace("00:00:", "");
                        string Status = "";

                        if (dtHotelReservation.Rows[0]["BookingStatus"].ToString() == "Tiketed")
                            title = "Flight Ticket -  " + dtHotelReservation.Rows[0]["BookingStatus"].ToString() + " on " + ReservationDate + " For " + dtHotelReservation.Rows[0]["LeadingPaxName"].ToString();
                        else
                            title = "Flight Ticket - ";


                        decimal SalesTax = Convert.ToDecimal(dtBookingTransactions.Rows[0]["SeviceTax"]);
                        decimal Ammount = Convert.ToDecimal(dtBookingTransactions.Rows[0]["BookingAmt"]);
                        // decimal Ammount = Convert.ToDecimal(dtBookingTransactions.Rows[0]["BookingAmtWithTax"]);
                        SalesTax = decimal.Round(SalesTax, 2, MidpointRounding.AwayFromZero);
                        Ammount = decimal.Round(Ammount, 2, MidpointRounding.AwayFromZero);
                        string InvoiceID = dtHotelReservation.Rows[0]["InvoiceNo"].ToString();
                        string Hotelorigin = dtBookedRoom.Rows[0]["OriginAirport"].ToString();
                        string Hoteldestination = dtBookedRoom.Rows[0]["DestinationAirport"].ToString();
                        string VoucherID = dtHotelReservation.Rows[0]["InvoiceNo"].ToString();
                        string CheckIn = dtHotelReservation.Rows[0]["ArrivalDate"].ToString();
                        // CheckIn = CheckIn.Replace("00:00", "");
                        string CheckOut = dtHotelReservation.Rows[0]["DepartureDate"].ToString();
                        // CheckOut = CheckOut.Replace("00:00", "");
                        string HotelName = (dtHotelReservation.Rows[0]["AirlineName"].ToString());

                        Status = (dtHotelReservation.Rows[0]["BookingStatus"].ToString());


                        string AgentRef = (dtHotelReservation.Rows[0]["AgentReferenceNo"].ToString());
                        string AgencyName = dtAgentDetail.Rows[0]["AgencyName"].ToString();
                        string Address = dtAgentDetail.Rows[0]["Address"].ToString();
                        string Description = (dtAgentDetail.Rows[0]["Description"].ToString());
                        string Countryname = (dtAgentDetail.Rows[0]["Countryname"].ToString());
                        string phone = dtAgentDetail.Rows[0]["phone"].ToString();
                        string email = (dtAgentDetail.Rows[0]["email"].ToString());
                        Night = "";
                        string words;
                        //words = ToWords((Ammount) + SalesTax);
                        words = ToWords((Ammount), CurrencyClass);

                        string URL = ConfigurationManager.AppSettings["URL"].ToString();
                        string AirLogo = URL + "/images/AirlineLogo/" + HotelName + ".gif\"";

                        Flightdbml.FlightHelperDataContext DBs = new Flightdbml.FlightHelperDataContext();
                        string AirlineName = (from obj in DBs.tbl_Airlines where obj.code == HotelName select obj.AirlineName).FirstOrDefault();

                        string[] OriginCode = Hotelorigin.Split('(');
                        string[] DestinationCode = Hoteldestination.Split('(');

                        string FromTo = Hotelorigin.Split(',')[0] + "-" + Hoteldestination.Split(',')[0];

                        DateTime dt = Convert.ToDateTime(dtHotelReservation.Rows[0]["LastTicketDate"]);
                        string Date = dt.ToString("ddd, dd MMM yy");

                        string Arr = dtBookedRoom.Rows[0]["ArrTime"].ToString();
                        string Dep = dtBookedRoom.Rows[0]["DepTime"].ToString();
                        string Durat = dtBookedRoom.Rows[0]["Duration"].ToString();

                        string[] ArrDate = Arr.Split(' ');

                        DateTime ArrivDate = Convert.ToDateTime(ArrDate[0]);
                        string ArrivalDate = ArrivDate.ToString("dd MMM yy");

                        DateTime myDateTime = DateTime.Parse(Arr);

                        string DepTime = myDateTime.AddMinutes(Convert.ToInt64(Durat)).ToString();
                        string[] DepDate = DepTime.Split(' ');

                        DateTime DeptDate = Convert.ToDateTime(DepDate[0]);
                        string DepartureDate = DeptDate.ToString("dd MMM yy");
                        string Url = Convert.ToString(ConfigurationManager.AppSettings["URL"]);

                        Int64 hrs = Convert.ToInt64(Durat) / 60;
                        Int64 Min = Convert.ToInt64(Durat) % 60;
                        string Duration = hrs + "h " + Min + "m";

                        string CanAmtWoutNight = "";
                        string CanAmtWithTax = "";

                        string Supplier = (dtHotelReservation.Rows[0]["AirlineName"].ToString());


                        // BuildMyString.com generated code. Please enjoy your string responsibly.

                        sb.Append(" <div>");
                        sb.Append("        <br /><br />");
                        sb.Append("        <table style=\"height: 50px; width: 100%;\">");
                        sb.Append("            <tbody>");
                        sb.Append("                <tr style=\" color: #57585A;\">");
                        sb.Append("                    <td id=\"AgentLogo\" style=\"width: 25%;padding-left:15px\">");
                        //if (URL== "https://letsgoonatour.com")
                        //{
                        //    sb.Append("                        <span class=\"opensans size20 bold grey2\" height=\"10%\" width=\"10%\"><img src=\""+ URL + "/images/LetsGoOnATourlogo.png\"></span>");
                        //}
                        //else
                        //{
                        //    sb.Append("                        <span class=\"opensans size20 bold grey2\" height=\"10%\" width=\"10%\"><img src=\""+ URL + "/images/logo.png\"></span>");
                        //}
                        if (DefaultManager.UrlExists(Url + "AgencyLogos/" + dtSupplierDetails.Agentuniquecode + ".png"))
                            sb.Append("<img  src=\"" + Url + "AgencyLogo/" + Logo + "\" height=\"auto\" width=\"auto\"></img>");
                        else
                            sb.Append("<h1 style=\"text-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);\" height=\"auto\" width=\"auto\">" + dtSupplierDetails.AgencyName + "</h1>");
                        sb.Append("<td style=\"width: 20%\"></td>");
                        sb.Append("<td style=\"width: auto; padding-right:15px; color: #57585A\" align=\"right\" >");
                        sb.Append("                    </td>");
                        sb.Append("                    <td style=\"width: 25%;padding-left:15px\">Ticket - <b>Confirmed</b> </td>");
                        sb.Append("                    <td style=\"width: 25%; padding-right:15px\" >");
                        sb.Append("                        <span >Booking Id - <b>" + ReservationID + "</b></span>");
                        sb.Append("                    </td>");
                        sb.Append("                    <td style=\"width: 25%;  >");
                        sb.Append("                        <span style=\"margin-right: 15px\">Booking Date - <b>" + Date + "</b></span>");
                        sb.Append("                    </td>");
                        sb.Append("                </tr>");
                        sb.Append("            </tbody>");
                        sb.Append("        </table>");
                        //sb.Append("        <table style=\"height: 50px; width: 100%;\">");
                        //sb.Append("            <tbody>");
                        //sb.Append("                <tr style=\" color: #57585A;\">");
                        //sb.Append("                    <td style=\"width: 40%;\" align=\"right\"><b>" + Date + "</b> </td>");
                        //sb.Append("                    <td style=\"width: 10%;\"></td>");
                        //sb.Append("                    <td style=\"width: 40%\" align=\"left\"><b>" + FromTo + "</b></td>");
                        //sb.Append("                </tr>");
                        //sb.Append("            </tbody>");
                        //sb.Append("        </table>");
                        sb.Append("    </div><br>");
                        sb.Append("    <div>");
                        sb.Append("        <table style=\"height: 200px; width: 100%;\">");
                        sb.Append("            <tbody>");
                        sb.Append("                <tr style=\" color: #57585A;\">");
                        sb.Append("                    <td style=\"width:15%;\"></td>");
                        sb.Append("                    <td id=\"AirlineLogo\" style=\"width: 15%;\">");
                        sb.Append("                        <span class=\"opensans size30 bold grey2\"><img  src=\"" + AirLogo + " ></span>");
                        sb.Append("                        <br />");
                        sb.Append("                        <p>" + AirlineName + "<br />(" + HotelName + ")</p>");
                        sb.Append("                    </td>");
                        sb.Append("                    <td style=\"width: 15%\" align=\"right\">");
                        sb.Append("                        <b style=\"font-size:25px\">" + OriginCode[1].Split(')')[0] + "</b> <br> <span style=\"font-size:15px\">" + OriginCode[1].Split(',')[1] + "</span>  <hr /> " + ArrDate[1] + " hrs, " + ArrivalDate + " <small>  <hr />" + OriginCode[0] + "</small>");
                        sb.Append("                    </td>");
                        sb.Append("                    <td style=\"width: 15%;padding:2%\">");
                        sb.Append("                        <p style=\"padding-left:35%\"> ------> <br> <small style=\"color:#c5c3bc\">" + Duration + "</small><hr style=\"color:gray\" /> <small style=\"padding-left:35%;color:#c5c3bc\">Class - " + dtBookedRoom.Rows[0]["FareClass"].ToString() + "</small>   </p>");
                        sb.Append("                    </td>");
                        sb.Append("                    <td style=\"width: 15%\" align=\"left\">");
                        sb.Append("                        <b style=\"font-size:25px\">" + DestinationCode[1].Split(')')[0] + "</b> <br> <span style=\"font-size:15px\"> " + DestinationCode[1].Split(',')[1] + "</span> <hr /> " + DepDate[1] + " hrs, " + DepartureDate + "  <small> <hr />" + DestinationCode[0] + "</small>");
                        sb.Append("                    </td>");
                        sb.Append("                    <td style=\"width:25%;\"></td>");
                        sb.Append("                </tr>");
                        sb.Append("            </tbody>");
                        sb.Append("        </table>");
                        sb.Append("    </div>");
                        sb.Append("    <br>");
                        sb.Append("    <div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
                        sb.Append("        <span style=\"padding-left:10px;color: #57585A\"><b>PASSENGER  DETAILS</b></span>");
                        sb.Append("    </div>");
                        sb.Append("    <div>");
                        sb.Append("        <table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 1px; border-top-color: gray; border-bottom-width: 1px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
                        sb.Append("            <tbody>");
                        sb.Append("                <tr style=\"text-align: center; color: #57585A; font-weight:600;border-bottom-width: 1px;border-bottom-color: #E6DCDC;\">");
                        sb.Append("                    <td align=\"left\" style=\" padding-top: 15px; padding: 5px 0px 15px 10px; \"><b>PASSENGER NAME</b></td>");
                        sb.Append("                    <td style=\" padding-top: 5px\"><b>PNR</b>  </td>");
                        sb.Append("                    <td style=\" padding-top: 5px\"><b>E-TICKET NO.</b></td>");
                        sb.Append("                    <td style=\" padding-top: 5px\"><b>SEAT</b>  </td>");
                        sb.Append("                </tr>");


                        for (var i = 0; i < dtBookedRoom.Rows.Count; i++)
                        {
                            AllPassengers = dtBookedPassenger.Rows[i]["PaxTitle"] + " " + dtBookedPassenger.Rows[i]["FirstName"] + " " + dtBookedPassenger.Rows[i]["LastName"];
                            AllPassengers = AllPassengers.TrimEnd(' ');
                            AllPassengers = AllPassengers.TrimEnd(',');
                            string Type = "";
                            if (dtBookedPassenger.Rows[i]["PaxType"].ToString() == "1")
                                Type = "Adult";
                            else if (dtBookedPassenger.Rows[i]["PaxType"].ToString() == "2")
                                Type = "Chlid";
                            else if (dtBookedPassenger.Rows[i]["PaxType"].ToString() == "3")
                                Type = "Infants";

                            sb.Append("                <tr style=\"text-align: center; color: #57585A; font-weight:600;border-bottom-width: 1px;border-bottom-color: #E6DCDC;\">");
                            sb.Append("                    <td align=\"left\" style=\"border:none; padding-top: 15px; padding: 5px 0px 15px 10px; \">" + AllPassengers + " , <small style=\"color:#c5c3bc\"> " + Type + "</small></td>");
                            sb.Append("                    <td style=\"border:none; padding-top: 5px\">" + dtBookedRoom.Rows[i]["AirlinePNR"].ToString() + "</td>");
                            sb.Append("                    <td style=\"border:none; padding-top: 5px\"></td>");
                            sb.Append("                    <td style=\"border:none; padding-top: 5px\">  </td>");
                            sb.Append("                </tr>");

                        }
                        sb.Append("            </tbody>");
                        sb.Append("        </table>");
                        sb.Append("    </div>");
                        sb.Append("    <br />");
                        sb.Append("    <div style=\"font-size: 20px; padding-top: 8px\">");
                        sb.Append("        <span style=\"padding-left:10px;color: #57585A\"><b>Terms &amp; Conditions</b></span>");
                        sb.Append("    </div><hr style=\"border-top-width: 3px\">");
                        sb.Append("    <div style=\"height:auto; color: #57585A; font-size:small\">");
                        sb.Append("        <ul style=\"list-style-type: disc\">");
                        sb.Append("            <li><b>Check-in Time </b> : Check-in desks will close 1 hour before departure.</li>");
                        sb.Append("            <li><b>Valid ID proof needed :</b> Carry a valid photo identification proof (Driver Licence, Aadhar Card, Pan Card or any other Government recognised photo identification)</li>");
                        sb.Append("            <li><b> Web Check-in (opens 48 hrs. before departure): </b> AIR INDIA - Use E-ticket Number, flight date, departure airport only.</li>");
                        sb.Append("            <li><b>For date-change requests please contact our customer care number 1-800-103-9695 </b></li>");
                        sb.Append("            <li><b>You have paid :  " + dtAgentDetail.Rows[0]["CurrencyCode"].ToString() + "   " + Ammount + " (" + words + ")</b></li>");
                        sb.Append("        </ul>");
                        sb.Append("    </div>");
                        sb.Append("    <br>");
                        sb.Append("    <div style=\"font-size: 20px; padding-bottom: 10px; padding-top: 8px\">");
                        sb.Append("        <span style=\"padding-left:10px;color: #57585A\"><b>BAGGAGE INFORMATION</b></span>");
                        sb.Append("    </div>");
                        sb.Append("    <div>");
                        sb.Append("        <table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 1px; border-top-color: gray; border-bottom-width: 1px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
                        sb.Append("            <tbody>");
                        sb.Append("                <tr style=\"text-align: center; color: #57585A; font-weight:600;border-bottom-width: 1px;border-bottom-color: #E6DCDC;\">");
                        sb.Append("                    <td style=\"padding-top: 15px; padding: 5px 0px 15px 10px; \"><b>Type</b></td>");
                        sb.Append("                    <td style=\"padding-top: 5px\"><b>Sector</b>  </td>");
                        sb.Append("                    <td style=\"padding-top: 5px\"><b>Cabin</b></td>");
                        sb.Append("                    <td style=\"padding-top: 5px\"><b>Check-in</b>  </td>");
                        sb.Append("                </tr>");
                        sb.Append("                <tr style=\"text-align: center; color: #57585A; font-weight:600;border-bottom-width: 1px;border-bottom-color: #E6DCDC;\">");
                        sb.Append("                    <td style=\"border:none; padding-top: 15px; padding: 5px 0px 15px 10px; \">Adult</td>");
                        sb.Append("                    <td style=\"border:none; padding-top: 5px\"> >" + OriginCode[1].Split(',')[1] + " - " + DestinationCode[1].Split(',')[1] + " </td>");
                        sb.Append("                    <td style=\"border:none; padding-top: 5px\">8 Kgs</td>");
                        sb.Append("                    <td style=\"border:none; padding-top: 5px\"> 25 Kgs </td>");
                        sb.Append("                </tr>");
                        sb.Append("            </tbody>");
                        sb.Append("        </table>");
                        sb.Append("    </div>");
                        //sb.Append("    <br /><br />");
                        //sb.Append("    <div style=\"font-size: 20px;  padding-top: 8px\">");
                        //sb.Append("        <span style=\"padding-left:10px;color: #57585A\"><b>CANCELLATION  CHARGES</b></span>");
                        //sb.Append("    </div>");
                        //sb.Append("    <p style=\"padding-left:3%\">All charges below are per Pax and per Segment in INR</p>");
                        //sb.Append("    <div>");
                        //sb.Append("        <table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 1px; border-top-color: gray; border-bottom-width: 1px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
                        //sb.Append("            <tbody>");
                        //sb.Append("                <tr style=\"text-align: center; color: #57585A; font-weight:600;border-bottom-width: 1px;border-bottom-color: #E6DCDC;\">");
                        //sb.Append("                    <!--<td style=\"border:none; padding-top: 15px\">BOM-NAG </td>");
                        //sb.Append("                    <td style=\"border:none; padding-top: 15px\">Cancellation Charges </td>-->");
                        //sb.Append("                    <td style=\" padding-top: 15px\"><b>Type</b> </td>");
                        //sb.Append("                    <td style=\" padding-top: 5px\"><b>Condition</b>  </td>");
                        //sb.Append("                    <td style=\" padding-top: 5px\"><b>Airline</b></td>");
                        //sb.Append("                    <td style=\" padding-top: 5px\"><b>Makemytrip</b>  </td>");
                        //sb.Append("                </tr>");
                        //sb.Append("                <tr style=\"text-align: center; color: #57585A; font-weight:600;border-bottom-width: 1px;border-bottom-color: #E6DCDC;\">");
                        //sb.Append("                    <td style=\"border:none; padding-top: 15px\">Adult</td>");
                        //sb.Append("                    <td style=\"border:none; padding-top: 5px\"> 0 hrs - 1 day <br /> 1 day - 365 days </td>");
                        //sb.Append("                    <td style=\"border:none; padding-top: 5px\">Non-Refundabl <br /> 2300 </td>");
                        //sb.Append("                    <td style=\"border:none; padding-top: 5px\"> 0</td>");
                        //sb.Append("                </tr>");
                        //sb.Append("            </tbody>");
                        //sb.Append("        </table>");
                        //sb.Append("    </div>");
                        //sb.Append("    <br />");
                        //sb.Append("    <div style=\"font-size: 20px;  padding-top: 8px\">");
                        //sb.Append("        <span style=\"padding-left:10px;color: #57585A\"><b> DATE CHANGE CHARGES</b></span>");
                        //sb.Append("    </div>");
                        //sb.Append("    <p style=\"padding-left:3%\">All charges below are per Pax and per Segment in INR</p>");
                        //sb.Append("    <div>");
                        //sb.Append("        <table border=\"1\" style=\"margin-top: 3px; width: 100%; border-spacing: 0px; border-top-width: 1px; border-top-color: gray; border-bottom-width: 1px; border-bottom-color: #E6DCDC; border-left: none; border-right: none\">");
                        //sb.Append("            <tbody>");
                        //sb.Append("                <tr style=\"text-align: center; color: #57585A; font-weight:600;border-bottom-width: 1px;border-bottom-color: #E6DCDC;\">");
                        //sb.Append("                    <!--<td style=\"border:none; padding-top: 15px\">BOM-NAG </td>");
                        //sb.Append("                    <td style=\"border:none; padding-top: 15px\">Cancellation Charges </td>-->");
                        //sb.Append("                    <td style=\" padding-top: 15px\"><b>Type</b> </td>");
                        //sb.Append("                    <td style=\" padding-top: 5px\"><b>Condition</b>  </td>");
                        //sb.Append("                    <td style=\" padding-top: 5px\"><b>Airline</b></td>");
                        //sb.Append("                    <td style=\" padding-top: 5px\"><b>Makemytrip</b>  </td>");
                        //sb.Append("                </tr>");
                        //sb.Append("                <tr style=\"text-align: center; color: #57585A; font-weight:600;border-bottom-width: 1px;border-bottom-color: #E6DCDC;\">");
                        //sb.Append("                    <td style=\"border:none; padding-top: 15px\">Adult</td>");
                        //sb.Append("                    <td style=\"border:none; padding-top: 5px\"> 0 hrs - 1 day <br /> 1 day - 365 days </td>");
                        //sb.Append("                    <td style=\"border:none; padding-top: 5px\">Non-Refundabl <br /> 2300 </td>");
                        //sb.Append("                    <td style=\"border:none; padding-top: 5px\"> 0</td>");
                        //sb.Append("                </tr>");
                        //sb.Append("            </tbody>");
                        //sb.Append("        </table>");
                        //sb.Append("    </div>");
                        sb.Append("    <br />");
                        sb.Append("    <div style=\"background-color: #00AEEF; text-align: center; font-size: 20px; color: white\">");
                        sb.Append("        <span>Check your Ticket details carefully and inform us immediately before it’s too late...</span>");
                        sb.Append("    </div>");
                        return sb.ToString();

                    }

                    else
                        return "";
                }
            }
        }


        private static string[] ones = {
    "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine",
    "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen",
};

        private static string[] tens = { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

        private static string[] thous = { "Hundred,", "Thousand,", "Million,", "Billion,", "Trillion,", "Quadrillion," };

        public static string ToWords(decimal number, string CurrencyName)
        {
            if (number < 0)
                return "negative " + ToWords(Math.Abs(number), CurrencyName);

            int intPortion = (int)number;
            int decPortion = (int)((number - intPortion) * (decimal)100);

            if (CurrencyName == "Currency-AED")
            {
                return string.Format("{0} AED and {1} Fils", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "Currency-SAR")
            {
                return string.Format("{0} SAR and {1} Halalah", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "Currency-MYR")
            {
                return string.Format("{0} Ringgit and {1} sen", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "fa fa-eur")
            {
                return string.Format("{0} Euro and {1} 	Cent", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "fa fa-gbp")
            {
                return string.Format("{0} Great Britain Pounds and {1} Penny", ToWords(intPortion), ToWords(decPortion));
            }
            else if (CurrencyName == "fa fa-dollar")
            {
                return string.Format("{0} Dollar and {1} Cent", ToWords(intPortion), ToWords(decPortion));
            }
            else
                return string.Format("{0} Rupees and {1} Paise", ToWords(intPortion), ToWords(decPortion));
            //return string.Format("{0} Rupees and {1} Paise", ToWords(intPortion), ToWords(decPortion));        //orig line without any conditions
        }

        private static string ToWords(int number, string appendScale = "")
        {
            string numString = "";
            if (number < 100)
            {
                if (number < 20)
                    numString = ones[number];
                else
                {
                    numString = tens[number / 10];
                    if ((number % 10) > 0)
                        numString += "-" + ones[number % 10];
                }
            }
            else
            {
                int pow = 0;
                string powStr = "";

                if (number < 1000) // number is between 100 and 1000
                {
                    pow = 100;
                    powStr = thous[0];
                }
                else // find the scale of the number
                {
                    int log = (int)Math.Log(number, 1000);
                    pow = (int)Math.Pow(1000, log);
                    powStr = thous[log];
                }

                numString = string.Format("{0} {1}", ToWords(number / pow, powStr), ToWords(number % pow)).Trim();
            }

            return string.Format("{0} {1}", numString, appendScale).Trim();
        }

        #region Numbers

        private static string[] _ones =
        {
                "zero",
                "one",
                "two",
                "three",
                "four",
                "five",
                "six",
                "seven",
                "eight",
                "nine"
         };



        private string[] _teens =
        {
        "ten",
        "eleven",
        "twelve",
        "thirteen",
        "fourteen",
        "fifteen",
        "sixteen",
        "seventeen",
        "eighteen",
        "nineteen"
        };




        private string[] _tens =
        {
        "",
        "ten",
        "twenty",
        "thirty",
        "forty",
        "fifty",
        "sixty",
        "seventy",
        "eighty",
        "ninety"
        };

        // US Nnumbering`:

        private string[] _thousands =
    {
    "",
    "thousand",
    "million",
    "billion",
    "trillion",
    "quadrillion"
    };

        #endregion

        /// <summary>
        /// Converts a numeric value to words suitable for the portion of
        /// a check that writes out the amount.
        /// </summary>
        /// <param name="value">Value to be converted
        /// <returns></returns>
        public string ConvertToNum(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;
            StringBuilder builder = new StringBuilder();
            // Convert integer portion of value to string
            digits = ((long)value).ToString();
            // Traverse characters in reverse order
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                // Determine if ones, tens, or hundreds column
                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            // First digit in number (last in loop)
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            // This digit is part of "teen" value
                            temp = String.Format("{0} ", _teens[ndigit]);
                            // Skip tens position
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            // Any non-zero digit
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            // This digit is zero. If digit in tens and hundreds
                            // column are also zero, don't show "thousands"
                            temp = String.Empty;
                            // Test for non-zero digit in this grouping
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        // Show "thousands" if non-zero in grouping
                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                temp,
                                _thousands[column / 3],
                                //allZeros ? " " : ", ");
                                allZeros ? " " : " ");
                            }
                            // Indicate non-zero digit encountered
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                            _tens[ndigit],
                            (digits[i + 1] != '0') ? "-" : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            // Append fractional portion/cents
            builder.AppendFormat(" DOLLARS and {0:00} / 100", (value - (long)value) * 100);//Replace Dollars with paisa if you are using indian currencry

            // Capitalize first letter
            return String.Format("{0}{1}",
            Char.ToUpper(builder[0]),
            builder.ToString(1, builder.Length - 1));
        }
    }
}