﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for OTBUploader
    /// </summary>
    public class OTBUploader : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Files.Count > 0)
            {
                string RefrenceNo = System.Convert.ToString(context.Request.QueryString["Otb_id"]);
                string FileNo = System.Convert.ToString(context.Request.QueryString["FileNo"]);
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string myFilePath = file.FileName;
                    string ext = Path.GetExtension(myFilePath);
                    if (ext == ".pdf" || ext == ".PDF")
                    {
                        string FileName = RefrenceNo + "_" + FileNo + ".pdf";
                        FileName = Path.Combine(context.Server.MapPath("~/OTBDocument/"), FileName);
                        file.SaveAs(FileName);
                        string FN = FileName.Replace(myFilePath, "");
                        string[] IFN = (string[])HttpContext.Current.Session["VisaFileName" + 0];
                        if (IFN != null)
                        {
                            int j = IFN.Length + 1;
                            context.Session["VisaFileName" + j] = FN + "\\" + myFilePath;
                        }


                    }
                    else
                    {
                        context.Response.Write("Only PDF file is allowed");

                    }
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}