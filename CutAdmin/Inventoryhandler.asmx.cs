﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Globalization;
using CutAdmin.EntityModal;
using System.Threading.Tasks;
using System.Threading;
using CutAdmin.Services;
namespace CutAdmin
{
    /// <summary>
    /// Summary description for Inventoryhandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. e




    [System.Web.Script.Services.ScriptService]
    public class Inventoryhandler : System.Web.Services.WebService
    {
        public class Inventory
        {
            public List<Int64> ID { get; set; }
            public string Name { get; set; }
            public List<string> Date { get; set; }
        }
        JavaScriptSerializer objserialize = new JavaScriptSerializer();
        public static List<DatesInv> ListDatesNew { get; set; }
        #region Save Inventory
        [WebMethod(EnableSession = true)]
        public string SaveInventory(string HotelCode, Int64[] Room, string MaxRoom, string DtTill, string[] DateInvFr, string[] DateInvTo,List<string> pRateType, string OptRoomperDate, string InvLiveOrReq, string InType, string InventoryState,List<string> arrDays)
        {
            try
            {
              ManageInventory.SaveInventory(HotelCode, Room, MaxRoom, DtTill, DateInvFr, DateInvTo, pRateType, OptRoomperDate, InvLiveOrReq, InType, InventoryState, arrDays);
              return  objserialize.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                return objserialize.Serialize(new { retCode = 0 , ex =ex.Message});
            }
        }
        #endregion
        #region Genrate Inventory Dates
        [WebMethod(EnableSession = true)]
        public string GenrateDates(string Startdt, string Enddt, string HotelCode,string sInventory)
        {
            try
            {
                DateTime From = DateTime.ParseExact(Startdt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                DateTime To = DateTime.ParseExact(Enddt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                double noGDays = (To - From).TotalDays;
                List<object> ListDates = new List<object>();
                for (int i = 0; i <= noGDays; i++)
                {
                    ListDates.Add(new { Date = From.AddDays(i).ToString("dd-MM"), LongDate= From.AddDays(i).ToString("dd-MM-yyyy"), Day = From.AddDays(i).DayOfWeek.ToString() });
                }
                var arrRooms = CutAdmin.Services.Rooms.GetRooms(HotelCode);
                var arrMealPlan = GenralManager.GetMealPlans();
                var arrRateType = CutAdmin.Services.RateType._ByHotelInventory(sInventory,HotelCode);
                return objserialize.Serialize(new { retCode = 1, arrRooms = arrRooms, ListDates = ListDates, arrMealPlan = arrMealPlan, arrRateType= arrRateType });
            }
            catch (Exception ex)
            {
                return objserialize.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        #endregion

        [WebMethod(EnableSession = true)]
        public string GetInventory(string Startdt, string Enddt, string InventoryType, Int64 HotelCode)
        {
            try
            {
                DateTime From = DateTime.ParseExact(Startdt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                DateTime To = DateTime.ParseExact(Enddt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                double noGDays = (To - From).TotalDays;
                Int64 SupplierID = AccountManager.GetSuperAdminID();
                //if (AccountManager.GetUserType() == "Admin")
                //    SupplierID = 0;
                List<string> ListDates = new List<string>();
                for (int i = 0; i <= noGDays; i++)
                {
                    ListDates.Add(From.AddDays(i).ToString("dd-MM"));
                }
                 var arrInventory = ManageInventory.GetInventory(Startdt, Enddt, HotelCode, InventoryType, SupplierID);
                if (arrInventory.Count != 0)
                    return objserialize.Serialize(new { Session = 1, retCode = 1, arrInventory = arrInventory.OrderBy(d => d.HotelName).ToList(), ListDates = ListDates });
                else
                    return objserialize.Serialize(new { Session = 1, retCode = 2, ListDates = ListDates });
            }
            catch (Exception)
            {
                return objserialize.Serialize(new { Session = 1, retCode = 0 });
            }
        }

        public string Json { get; set; }
        [WebMethod(EnableSession = true)]
        public async Task<string>  GetInventoryByRoom(string Startdt, string Enddt, string InventoryType, string HotelCode,List<string> RoomID,string SupplierID)
        {
            Json = string.Empty;
            try
            {
                DateTime dtFrom = DateTime.ParseExact(Startdt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                DateTime dtTo = DateTime.ParseExact(Enddt, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                List<DateTime> calendar = new List<DateTime>();
                while (dtFrom <= dtTo)
                {
                    calendar.Add(dtFrom);
                    dtFrom = dtFrom.AddDays(1);
                }
                await System.Threading.Tasks.Task.Run(() =>
                {
                    Json = objserialize.Serialize(new { retCode = 1, arrRates = ManageInventory.GetRoomInventory(HotelCode, RoomID, InventoryType, SupplierID, calendar) });
                }).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Json = objserialize.Serialize(new { retCode = 0 ,ex = ex.Message });
            }
            return Json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateInventory(List<Inventory> ListRoomRate, string status)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    if (status == "Start Sale")
                        status = "fs";
                    else
                        status = "ss";

                    for (int i = 0; i < ListRoomRate.Count; i++)
                    {
                        for (int j = 0; j < ListRoomRate[i].Date.Count; j++)
                        {
                            Int64 sid = Convert.ToInt64(ListRoomRate[i].ID[j]);
                            DateTime dt = DateTime.ParseExact(ListRoomRate[i].Date[j], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            tbl_CommonHotelInventory Update = DB.tbl_CommonHotelInventory.Single(x => x.Sid == sid);
                            switch (dt.Day.ToString())
                            {

                                case "1":
                                    Update.Date_1 = UpdateDate(Update.Date_1, status);
                                    break;
                                case "2":
                                    Update.Date_2 = UpdateDate(Update.Date_2, status);
                                    break;
                                case "3":
                                    Update.Date_3 = UpdateDate(Update.Date_3, status);
                                    break;
                                case "4":
                                    Update.Date_4 = UpdateDate(Update.Date_4, status);
                                    break;
                                case "5":
                                    Update.Date_5 = UpdateDate(Update.Date_5, status);
                                    break;
                                case "6":
                                    Update.Date_6 = UpdateDate(Update.Date_6, status);
                                    break;
                                case "7":
                                    Update.Date_7 = UpdateDate(Update.Date_7, status);
                                    break;
                                case "8":
                                    Update.Date_8 = UpdateDate(Update.Date_8, status);
                                    break;
                                case "9":
                                    Update.Date_9 = UpdateDate(Update.Date_9, status);
                                    break;
                                case "10":
                                    Update.Date_10 = UpdateDate(Update.Date_10, status);
                                    break;
                                case "11":
                                    Update.Date_11 = UpdateDate(Update.Date_11, status);
                                    break;
                                case "12":
                                    Update.Date_12 = UpdateDate(Update.Date_12, status);
                                    break;
                                case "13":
                                    Update.Date_13 = UpdateDate(Update.Date_13, status);
                                    break;
                                case "14":
                                    Update.Date_14 = UpdateDate(Update.Date_14, status);
                                    break;
                                case "15":
                                    Update.Date_15 = UpdateDate(Update.Date_15, status);
                                    break;
                                case "16":
                                    Update.Date_16 = UpdateDate(Update.Date_16, status);
                                    break;
                                case "17":
                                    Update.Date_17 = UpdateDate(Update.Date_17, status);
                                    break;
                                case "18":
                                    Update.Date_18 = UpdateDate(Update.Date_18, status);
                                    break;
                                case "19":
                                    Update.Date_19 = UpdateDate(Update.Date_19, status);
                                    break;
                                case "20":
                                    Update.Date_20 = UpdateDate(Update.Date_20, status);
                                    break;
                                case "21":
                                    Update.Date_21 = UpdateDate(Update.Date_21, status);
                                    break;
                                case "22":
                                    Update.Date_22 = UpdateDate(Update.Date_22, status);
                                    break;
                                case "23":
                                    Update.Date_23 = UpdateDate(Update.Date_23, status);
                                    break;
                                case "24":
                                    Update.Date_24 = UpdateDate(Update.Date_24, status);
                                    break;
                                case "25":
                                    Update.Date_25 = UpdateDate(Update.Date_25, status);
                                    break;
                                case "26":
                                    Update.Date_26 = UpdateDate(Update.Date_26, status);
                                    break;
                                case "27":
                                    Update.Date_27 = UpdateDate(Update.Date_27, status);
                                    break;
                                case "28":
                                    Update.Date_28 = UpdateDate(Update.Date_28, status);
                                    break;
                                case "29":
                                    Update.Date_29 = UpdateDate(Update.Date_29, status);
                                    break;
                                case "30":
                                    Update.Date_30 = UpdateDate(Update.Date_30, status);
                                    break;
                                case "31":
                                    Update.Date_31 = UpdateDate(Update.Date_31, status);
                                    break;
                            }
                        }


                    }
                    DB.SaveChanges();

                    return objserialize.Serialize(new { Session = 1, retCode = 1 });
                }
            }

            catch (Exception ex)
            {
               
            }
            return objserialize.Serialize(new { Session = 1, retCode = 0 });
        }

        public string UpdateDate(string OldDate, string InventType)
        {
            string Update = "";
            try
            {
                Update += InventType;
                if (OldDate.Split('_').Length > 1)
                {
                    Update += "_" + OldDate.Split('_')[1];
                }
                if (OldDate.Split('_').Length == 3)
                {
                    Update += "_" + OldDate.Split('_')[2];
                }

            }
            catch { }
            return Update;
        }

        [WebMethod(EnableSession = true)]
        public string GetSupplierList(string HotelCode)
        {
            try
            {
                CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                using (var DB = new Click_Hotel())
                {
                    using (var db = new CutAdmin.dbml.helperDataContext())
                    {
                        var SipplierList = (from obj in DB.tbl_CommonHotelInventory
                                            join Adm in db.tbl_AdminLogins on Convert.ToInt64(obj.SupplierId) equals Adm.sid
                                            join Hotel in DB.tbl_CommonHotelMaster on Convert.ToInt64(obj.HotelCode) equals Hotel.sid
                                            where obj.HotelCode == HotelCode
                                            select new
                                            {
                                                Adm.AgencyName,
                                                obj.HotelCode,
                                                obj.SupplierId,
                                                Hotel.HotelAddress,
                                                Hotel.HotelName
                                            }).ToList().Distinct();

                        return objserialize.Serialize(new { Session = 1, retCode = 1, SipplierList = SipplierList });
                    }
                }
            }

            catch (Exception)
            {

                return objserialize.Serialize(new { Session = 1, retCode = 0 });
            }
         
        }

        public class ListDates
        {
            public List<DateTime> List_dates { get; set; }
        }

        public class ListNewDates
        {
            public List<DateTime> List_Newdates { get; set; }
        }

        public class DatesInv
        {
            public DateTime From { get; set; }
            public DateTime To { get; set; }
        }
        public class ListData
        {
            public List<tbl_CommonHotelInventory> Listdata { get; set; }
        }
    }
}
