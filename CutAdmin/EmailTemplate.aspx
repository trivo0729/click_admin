﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EmailTemplate.aspx.cs" Inherits="CutAdmin.EmailTemplate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/EmailTemplatesAdmin.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
	<section role="main" id="main">
		<hgroup id="main-title" class="thin">
			<h1>Email Templates</h1><hr />
		</hgroup>

		<div class="with-padding">
			<div class="content-panel margin-bottom">

				<div class="panel-navigation silver-gradient">

					<div class="panel-control">
						<a href="#" class="button icon-undo">Refresh</a>
					</div>

					<div class="panel-load-target scrollable" style="height:490px">

						<div class="navigable" runat="server">

							<ul class="files-list mini open-on-panel-content" id="ul_Folder" runat="server"></ul>

						</div>

					</div>

				</div>

				<div class="panel-content linen">

					<div class="panel-control align-right">
						<a href="#" class="button icon-cloud-upload margin-left">Add file...</a>
					</div>

					<div class="panel-load-target scrollable with-padding" style="height:450px">

						<p class="message icon-info-round white-gradient">Use the icons below or the sidebar to navigate in the files</p>

						
					</div>

				</div>

			</div>
		</div>

	</section>
	<!-- End main content -->
    	<script src="js/developr.content-panel.js"></script>

	<script>

		// Enable browser history
		var panel = $('.content-panel'),
			internalHashchange = false;
		panel.data('panel-options', {
			onStartLoad: function(settings, ajaxOptions)
			{
				var target = $(this).closest('.panel-navigation').length ? 'nav' : 'content';

				// Push to history
				internalHashchange = true;
				document.location.hash = '#'+target+':'+escape(ajaxOptions.url);
			}
		});

		// Listen for changes
		$(window).on('hashchange', function()
		{
			// Do not process if internal event
			if (internalHashchange)
			{
				internalHashchange = false;
				return;
			}

			// Handle only if a previous state has been saved
			if ( !document.location.hash || document.location.hash.indexOf( ':' ) < 0 )
			{
				return;
			}

			// Get hash parts
			var parts = document.location.hash.substring(1).split(':');
			panel[(parts[0] === 'nav') ? 'loadPanelNavigation' : 'loadPanelContent'](parts[1]);
		});

		// Call template init (optional, but faster if called manually)
		$.template.init();

		// Gallery size slider
		$('#gallery-slider').slider({
			min: 6,
			value: 13,
			max: 32,
			onChange: function(value)
			{
				$('#demo-gallery').css('font-size', value+'px');
			}
		});

	</script>
</asp:Content>
