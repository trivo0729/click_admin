﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
namespace CutAdmin.BL
{
    public class dbLiteHelper
    {
            SQLiteConnection conn = new SQLiteConnection("data source =" + HttpContext.Current.Server.MapPath("~/XMLS/TBO/"));
            public static void CreateDb()
            {
                string Script = string.Empty;
                Script = AppDomain.CurrentDomain.BaseDirectory + "//DatatVaseFormate.txt";
                Script = System.IO.File.ReadAllText(Script);
                string db = "wf.db3";
                System.Data.SQLite.SQLiteConnection.CreateFile(db);
                using (System.Data.SQLite.SQLiteConnection conn = new SQLiteConnection("data source = " + db))
                {
                    using (System.Data.SQLite.SQLiteCommand cmd = new SQLiteCommand(conn))
                    {
                        conn.Open();
                        cmd.CommandText = Script;
                        cmd.ExecuteNonQuery();
                        conn.Close();

                    }
                }
            }

            #region core ***********************************************
            public enum DBReturnCode
            {
                SUCCESS = 0,
                CONNECTIONFAILURE = -1,
                SUCCESSNORESULT = -2,
                SUCCESSNOAFFECT = -3,
                DUPLICATEENTRY = -4,
                EXCEPTION = -5,
                INPUTPARAMETEREMPTY = -6
            }
            protected static SQLiteConnection OpenConnection()
            {
                SQLiteConnection conn = null;
                try
                {
                    string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\w.db3";
                    conn = new SQLiteConnection("data source =" + HttpContext.Current.Server.MapPath("~/XMLS/TBO/") + @"\w.db3");
                    conn.Open();
                }
                catch
                {
                    conn = null;
                }
                return conn;
            }
            protected static void CloseConnection(SQLiteConnection conn)
            {
                try
                {
                    conn.Close();
                    conn.Dispose();
                }
                catch { }
            }
            public static DBReturnCode GetDataTable(string sQuery, out DataTable dtResult)
            {
                dtResult = null;
                SQLiteConnection connection = OpenConnection();
                DBReturnCode retcode;
                SQLiteDataAdapter dataAdapter = null;

                if (connection == null)
                {
                    retcode = DBReturnCode.CONNECTIONFAILURE;
                }
                else
                {
                    try
                    {
                        dataAdapter = new SQLiteDataAdapter(sQuery, connection);
                        DataSet dsTmp = new DataSet();
                        dataAdapter.Fill(dsTmp);
                        dtResult = dsTmp.Tables[0];
                        if (dtResult.Rows.Count == 0)
                            return DBReturnCode.SUCCESSNORESULT;
                        else
                            return DBReturnCode.SUCCESS;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        retcode = DBReturnCode.EXCEPTION;
                    }
                    finally
                    {
                        dataAdapter.Dispose();
                        CloseConnection(connection);
                    }
                }
                return retcode;

            }
            public static DBReturnCode GetDataSet(string sQuery, out DataSet ds, params SQLiteParameter[] commandParameters)
            {
                ds = null;
                SQLiteConnection connection = OpenConnection();
                DBReturnCode retcode;
                SQLiteDataAdapter dataAdapter = null;

                if (connection == null)
                {
                    retcode = DBReturnCode.CONNECTIONFAILURE;
                }
                else
                {
                    try
                    {
                        dataAdapter = new SQLiteDataAdapter(sQuery, connection);
                        dataAdapter.Fill(ds);
                        if (ds.Tables.Count == 0)
                            return DBReturnCode.SUCCESSNORESULT;
                        else
                            return DBReturnCode.SUCCESS;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        retcode = DBReturnCode.EXCEPTION;
                    }
                    finally
                    {
                        dataAdapter.Dispose();
                        CloseConnection(connection);
                    }
                }
                return retcode;
            }
            public static DBReturnCode ExecuteNonQuery(string sQuery, params SQLiteParameter[] commandParameters)
            {
                DBReturnCode retcode = DBReturnCode.SUCCESS;
                SQLiteCommand cmd = null;
                SQLiteConnection conn = OpenConnection();
                if (conn == null)
                {
                    retcode = DBReturnCode.CONNECTIONFAILURE;
                }
                else
                {
                    try
                    {
                        //cmd = new SQLiteCommand(sQuery, conn);
                        cmd = GetCommand(sQuery, conn, commandParameters);
                        if (cmd.ExecuteNonQuery() == 0)
                            return DBReturnCode.SUCCESSNOAFFECT;
                        else
                            return DBReturnCode.SUCCESS;
                    }
                    catch (SQLiteException ex)
                    {
                        if (ex.HResult == 2627 || ex.HResult == 2601) // PRIMARY or UNIQUE KEY KEY VIOLATION
                        {
                            retcode = DBReturnCode.DUPLICATEENTRY;
                        }
                        else
                            retcode = DBReturnCode.EXCEPTION;
                    }
                    finally
                    {
                        cmd.Dispose();
                        CloseConnection(conn);
                    }
                }
                return retcode;
            }
            public static DBReturnCode ExecuteScalar(string sQuery, out Int64 nRecordID)
            {
                SQLiteConnection connection = OpenConnection();
                DBReturnCode retcode;

                nRecordID = 0;
                /*HttpContext.Current.Response.Write(sQuery);
                HttpContext.Current.Response.Write("<br/>");*/

                SQLiteCommand cmd = null;
                SQLiteConnection conn = OpenConnection();
                if (conn == null)
                {
                    retcode = DBReturnCode.CONNECTIONFAILURE;
                }
                else
                {
                    try
                    {
                        sQuery += "; SELECT SCOPE_IDENTITY();";
                        cmd = new SQLiteCommand(sQuery, conn);
                        nRecordID = Convert.ToInt64(cmd.ExecuteScalar());
                        if (nRecordID <= 0)
                            return DBReturnCode.SUCCESSNOAFFECT;
                        else
                            return DBReturnCode.SUCCESS;
                    }
                    catch //(Exception e)
                    {
                        retcode = DBReturnCode.EXCEPTION;
                        //HttpContext.Current.Response.Write(e.Message); 
                    }
                    finally
                    {
                        cmd.Dispose();
                        CloseConnection(conn);
                    }
                }
                return retcode;
            }
            private static SQLiteCommand GetCommand(string cmdTxt, SQLiteConnection connection, params SQLiteParameter[] commandParameters)
            {
                SQLiteCommand command = new SQLiteCommand(cmdTxt, connection);
                if (cmdTxt.ToLower() != "")
                    command.CommandType = CommandType.Text;
                if (commandParameters != null)
                    AttachParameters(command, commandParameters);
                return command;
            }
            private static void AttachParameters(SQLiteCommand command, SQLiteParameter[] commandParameters)
            {
                if (command == null) throw new ArgumentNullException("command");
                if (commandParameters != null)
                {
                    foreach (SQLiteParameter p in commandParameters)
                    {
                        if (p != null)
                        {
                            // Check for derived output value with no value assigned
                            if ((p.Direction == ParameterDirection.InputOutput ||
                                p.Direction == ParameterDirection.Input) &&
                                (p.Value == null))
                            {
                                p.Value = DBNull.Value;
                            }
                            command.Parameters.Add(p);
                        }
                    }
                }
            }

            #endregion core
    }
}