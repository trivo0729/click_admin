﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="BookingList.aspx.cs" Inherits="CutAdmin.BookingList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/Invoice.js?v=1.5"></script>
    <script src="Scripts/BookingList.js?v=1.7"></script>
    <script src="Scripts/Booking.js?v=1.19"></script>
    <style>
        #ConfirmDate {
            z-index: 99999999;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
    <section role="main" id="main">
        <div class="standard-tabs">
            <!-- Tabs -->
            <ul class="tabs">
                <li class="black bold">Bookings</li>
                <li class="active"><a href="#hotel">Hotel</a></li>                
                <li><a href="#airlines">Airlines</a></li>
                <li><a href="#sightseeing">Sightseeing</a></li>
                <li><a href="#transport">Transport</a></li>
                <li><a href="#package">Package</a></li>
            </ul>
            <div class="tabs-content">
                <div id="hotel">
                    <hgroup id="main-title" class="thin">
                        <h1>Hotel Booking</h1>
                        <hr />
                        <h2><a href="#" class="addnew"><i class="fa fa-filter"></i></a></h2>
                        <div id="filter" class="with-padding anthracite-gradient" style="display: none;">
                            <form class="form-horizontal">
                                <div class="columns">
                                    <div class="three-columns twelve-columns-mobile">
                                        <label>Check-In</label>
                                        <span class="input full-width">
                                            <span class="icon-calendar"></span>
                                            <input type="text" name="datepicker" id="Check-In" class="input-unstyled" value="">
                                        </span>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile">
                                        <label>Check-Out</label>
                                        <span class="input full-width">
                                            <span class="icon-calendar"></span>
                                            <input type="text" name="datepicker" id="Check-Out" class="input-unstyled" value="">
                                        </span>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile">
                                        <label>Booking Date</label>
                                        <span class="input full-width">
                                            <span class="icon-calendar"></span>
                                            <input type="text" name="datepicker" id="Bookingdate" class="input-unstyled" value="">
                                        </span>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Passenger Name </label>
                                        <div class="input full-width">
                                            <input type="text" id="txt_Passenger" class="input-unstyled full-width">
                                        </div>
                                    </div>
                                </div>
                                <div class="columns">
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Reference No.</label>
                                        <div class="input full-width">
                                            <input type="text" id="txt_Reference" class="input-unstyled full-width">
                                        </div>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Hotel Name</label>
                                        <div class="input full-width">
                                            <input type="text" id="txt_Hotel" class="input-unstyled full-width">
                                        </div>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Location</label>
                                        <div class="input full-width">
                                            <input type="text" id="txt_Location" class="input-unstyled full-width">
                                        </div>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Reservation Status</label>
                                        <div class="full-width button-height typeboth">
                                            <select id="selReservation" class="select">
                                                <option selected="selected">All</option>
                                                <option>Vouchered</option>
                                                <option>Cancelled</option>
                                                <option>OnRequest</option>
                                                <option>Reconfirmed</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="columns">
                                    <div class="eight-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                    </div>
                                    <div class="two-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                        <button type="button" class="button anthracite-gradient" onclick="Search()">Search</button>
                                        <button type="button" class="button anthracite-gradient" onclick="Reset()">Reset</button>
                                    </div>
                                    <div class="two-columns twelve-columns-mobile bold text-alignright">
                                        <span class="icon-pdf right" onclick="ExportBookingDetailsToExcel('PDF')">
                                            <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                                        </span>
                                        <span class="icon-excel right" onclick="ExportBookingDetailsToExcel('excel')">
                                            <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                                    </div>
                                </div>
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </hgroup>
                    <div class="with-mid-padding">
                        <div class="respTable">
                            <table class="table responsive-table font11" id="tbl_BookingList">
                                <thead>
                                    <tr>
                                        <th scope="col">S.N</th>
                                        <th scope="col" class="align-center">Date</th>
                                        <th scope="col" class="align-center">Ref No.</th>
                                        <th scope="col" class="align-center">Agency</th>
                                        <%--<th scope="col" class="align-center">Supplier Ref</th>--%>
                                        <th scope="col" class="align-center">Passenger</th>
                                        <th scope="col" class="align-center">Hotel &amp; Location</th>
                                        <th scope="col" class="align-center">Stay</th>
                                        <%--<th scope="col" class="align-center">Check Out</th>--%>
                                        <th scope="col" class="align-center">Room</th>
                                        <th scope="col" class="align-center">Status</th>
                                        <%--<th scope="col" class="align-center">Dead Line</th>--%>
                                        <th scope="col" class="align-center">Amount</th>
                                        <th scope="col" class="align-center">Actions</th>
                                        <%--<th scope="col" class="align-center">Voucher</th>--%>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                <div id="airlines">
                    <hgroup id="main-title" class="thin">
                        <h1>Airlines Booking</h1>
                        <hr />
                        <h2><a href="#" class="addnew"><i class="fa fa-filter"></i></a></h2>
                        <div id="filter" class="with-padding anthracite-gradient" style="display: none;">
                            <form class="form-horizontal">
                                <div class="columns">
                                    <div class="three-columns twelve-columns-mobile">
                                        <label>Check-In</label>
                                        <span class="input full-width">
                                            <span class="icon-calendar"></span>
                                            <input type="text" name="datepicker" id="Check-In" class="input-unstyled" value="">
                                        </span>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile">
                                        <label>Check-Out</label>
                                        <span class="input full-width">
                                            <span class="icon-calendar"></span>
                                            <input type="text" name="datepicker" id="Check-Out" class="input-unstyled" value="">
                                        </span>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile">
                                        <label>Booking Date</label>
                                        <span class="input full-width">
                                            <span class="icon-calendar"></span>
                                            <input type="text" name="datepicker" id="Bookingdate" class="input-unstyled" value="">
                                        </span>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Passenger Name </label>
                                        <div class="input full-width">
                                            <input type="text" id="txt_Passenger" class="input-unstyled full-width">
                                        </div>
                                    </div>
                                </div>
                                <div class="columns">
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Reference No.</label>
                                        <div class="input full-width">
                                            <input type="text" id="txt_Reference" class="input-unstyled full-width">
                                        </div>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Hotel Name</label>
                                        <div class="input full-width">
                                            <input type="text" id="txt_Hotel" class="input-unstyled full-width">
                                        </div>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Location</label>
                                        <div class="input full-width">
                                            <input type="text" id="txt_Location" class="input-unstyled full-width">
                                        </div>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile bold">
                                        <label>Reservation Status</label>
                                        <div class="full-width button-height typeboth">
                                            <select id="selReservation" class="select">
                                                <option selected="selected">All</option>
                                                <option>Vouchered</option>
                                                <option>Cancelled</option>
                                                <option>OnRequest</option>
                                                <option>Reconfirmed</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="columns">
                                    <div class="eight-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                    </div>
                                    <div class="two-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                        <button type="button" class="button anthracite-gradient" onclick="Search()">Search</button>
                                        <button type="button" class="button anthracite-gradient" onclick="Reset()">Reset</button>
                                    </div>
                                    <div class="two-columns twelve-columns-mobile bold text-alignright">
                                        <span class="icon-pdf right" onclick="ExportBookingDetailsToExcel('PDF')">
                                            <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                                        </span>
                                        <span class="icon-excel right" onclick="ExportBookingDetailsToExcel('excel')">
                                            <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                                    </div>
                                </div>
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </hgroup>
                </div>
            </div>
        </div>
    </section>
  
    <script>
        jQuery(document).ready(function () {
            jQuery('.fa-filter').click(function () {
                jQuery(' #filter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #filter').slideUp();
                });
            });
        });
    </script>
</asp:Content>
