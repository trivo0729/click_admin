﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="MealPlans.aspx.cs" Inherits="CutAdmin.MealPalns" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/MealPlan.js?v=1.0"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
           <h1>Meal Plans </h1>
              <h2><a style="cursor:pointer" onclick="AddMealPlan()"  class="addnew with-tooltip" title="Add Meal Plan"><i class="fa fa-plus-circle"></i></a></h2>
            <hr />
        </hgroup>

        <div class="with-padding">
                <table class="table responsive-table font11" id="tbl_MealPlans">
                    <thead>
                        <tr>
                            <th scope="col" width="15%" class="align-center hide-on-mobile">Sr No</th>
                            <th scope="col" class="align-center hide-on-mobile">Meal Plan</th>
                            <th scope="col" class="align-center hide-on-mobile">Meals</th>
                            <th scope="col" class="align-center hide-on-mobile" style="width:50px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
        </div>
    </section>
</asp:Content>
