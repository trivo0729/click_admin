﻿function DatePickerByID(elem) {
    $('#' + elem).glDatePicker({
        zIndex: 999300,
        allowMonthSelect: true,
        allowYearSelect: true,
        todayDate: new Date(),
        position: 'inherit',
        monthNames: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
        onClick: (function (el, cell, date, data) {
            var armco = date.toLocaleDateString();
            armco = toDate(armco);
            el.val(armco);
        })
    });
}
function DatePicker(elem) {
    try {
        $(elem).glDatePicker({
            /* zIndex: 999300,*/
            minDate:new Date(),
            allowMonthSelect: true,
            allowYearSelect: true,
            //todayDate: new Date(),
            selectedDate: new Date(),
            position: 'relative',
            monthNames: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
            onClick: (function (el, cell, date, data) {
                var armco = date.toLocaleDateString();
                armco = toDate(armco);
                el.val(armco);
            }),
        });
    } catch (e) {
        AlertDanger(e.message)
    }
   
}
function toDate(selector) { /* Date Format */
    var from = selector.split("/");
    var Month = from[0];
    if (Month.length == 1)
        Month = "0" + Month;
    if (from[1].length == 1)
        from[1] = "0" + from[1];
    var myformat = from[1] + '-' + Month + '-' + from[2];
    return myformat;
}