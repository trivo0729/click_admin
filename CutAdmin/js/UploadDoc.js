﻿var MainImage = "", SubImage = "";
function SaveDoc(id, path, sucess) {
    debugger;
    try {
        var fileUpload = $("#" + id).get(0);
        var files = fileUpload.files;
        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            data.append(files[i].name, files[i]);
        }
        if (files.length != 0) {
            $.ajax({
                url: "../handler/UploadDocument.ashx?path=" + path,
                type: "POST",
                data: data,
                contentType: false,
                processData: false,
                success: function (result) {
                    if (result != "0") {
                        Success("Images / Document  Uploaded")
                        sucess({ retCode: 1 })
                    }
                    else if (result == "1") {
                        AlerrDanger('Unable to Save Images,please try latter.')
                    }
                },
                error: function (err) {
                }
            });
        }
        else
            sucess({ retCode: 1 })
    } catch (e) {
        sucess({ retCode: 1 })
    }



}

//  Images Preview
var arrImages = []
function preview_images(elem, preview) {
    var fileUpload = $(elem).get(0);
    var files = fileUpload.files;
    for (var i = 0; i < files.length; i++) {
        $('#' + preview).append('<li><img src="' + URL.createObjectURL(event.target.files[i]) + '" width="185px">' +
               '<span class="input">' +
               '<input type="text" name="pseudo-input-1" id="pseudo-input-1" readonly="readonly" class="input-unstyled ImageFileName" value="' + files[i].name + '" size="12">' +
               '<span class="button-group center">' +
               '<label for="button-radio-' + i + '" class="button green-active icon-monitor with-tooltip" title="Default Image">' +
               '<input type="radio" name="button-radio" id="button-radio-' + i + '" value="1" onclick="DefaultSelect(this,\'' + files[i].name + '\')">' +
               '</label>' +
               '<label for="pseudo-input-1" class="button icon-trash with-tooltip" title="Delete" onclick="deletePreview(this, \'' + i + '\',\'' + files[i].name + '\',\'' + preview + '\')"></label>' +
               '</span>' +
               '</span>')
        arrImages.push(files[i].name);
    }
    ImageProgress(preview)
}

/*Load Saved Image*/
function LoadPreview(arrImage, Path, div, type, id) {
    var ImgRequest = '';
    for (var j = 0; j < arrImage.length; j++) {
        if (arrImage[j] != 'undefined' && arrImage[j] != '') {
            ImgRequest += '<li><img src="./' + Path + "/" + arrImage[j] + '" width="185px">' +
                           '<span class="input">' +
                           '<input type="hidden" value="' + arrImage[j] + '" id="File' + j + '"><input type="text" name="pseudo-input-1" id="pseudo-input-1" class="input-unstyled ImageFileName" value="' + arrImage[j] + '" size="12" onchange="SetFileName(\'' + j + '\',this,\'' + Path + '\',\'' + type + '\',\'' + id + '\')">' +
                           '<span class="button-group center">' +
                           '<label for="sbutton-radio-' + j + '" class="button green-active icon-monitor with-tooltip" title="Default Image">' +
                           '<input type="radio" name="button-radio" id="sbutton-radio-' + j + '" value="1" onclick="DefaultSelect(this,\'' + arrImage[j] + '\')">' +
                           '</label>' +
                           '<label for="pseudo-input-1" class="button icon-trash with-tooltip" title="Delete" onclick="deletePreview(this, \'' + j + '\',\'' + arrImage[j] + '\',\'' + div + '\')"></label>' +
                           '</span>' +
                           '</span>'
            arrImages[j] = arrImage[j];
        }
    }
    $('#' + div).empty();
    $('#' + div).append(ImgRequest);
    $("#sbutton-radio-0").click();
    ImageProgress(div)
}


/* Progress Bar*/
function ImageProgress(elem) {
    try {
        var ndImages = $('#' + elem).find('img');
        var Per = parseInt((ndImages.length / 10) * 100) + "%";
        $(".progress-bar").css("width", Per);
        $(".ImgCount").text(10 - ndImages.length);
    }
    catch (ex) {

    }
}

function DefaultSelect(ele, filename) {
    "use strict";
    var arrImg = [];
    try {
        MainImage = filename
        arrImg[0] = filename;
        for (var i = 1; i < arrImages.length; i++) {
            if (arrImages[i] != filename) {
                arrImg[i] = arrImages[i];
            }
            else {
                arrImg[i] = arrImages[0];
            }
        }
        arrImages = arrImg;

    } catch (e) {
        console.log(e.message);
    }
}

deletePreview = function (ele, i, filename, preview) {
    "use strict";
    var arrImg = [];
    try {
        $(ele).parent().parent().parent().remove();
        $(".message ").remove();
        for (var i = 0; i < arrImages.length; i++) {
            if (arrImages[i] != filename && arrImages[i] != undefined) {
                arrImg[i] = arrImages[i];
            }
        }
        arrImages = arrImg;
        ImageProgress(preview)
    } catch (e) {
        console.log(e.message);
    }
}

function SetImage() {
    for (var i = 0; i < arrImages.length; i++) {
        if (i == 0)
            MainImage = arrImages[i];
        SubImage += arrImages[i] + "^"
    }
}

function SetFileName(index, input, Path, type, id) {
    debugger
    try {
        $.ajax({
            url: "../handler/UpdateDocument.ashx?path=" + Path + "/" + $(input).val() + "&Oldfile=" + Path + "/" + $("#File" + index).val() + "&Type=" + type + "&Id=" + id + "&OldImage=" + $("#File" + index).val(),
            type: "POST",
            data: {},
            contentType: false,
            processData: false,
            success: function (result) {
                if (result != "0") {
                    if (arrImages.length != 0) {
                        arrImages[index] = $(input).val();
                    }
                    //sucess({ retCode: 1 })
                    Success("Image Caption Change");
                }
                else if (result == "1") {
                    AlerrDanger('Unable to Save Images,please try latter.')
                }
            },
            error: function (err) {
            }
        });

    } catch (e) { }
}