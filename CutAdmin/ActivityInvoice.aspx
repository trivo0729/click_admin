﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ActivityInvoice.aspx.cs" Inherits="CutAdmin.ActivityInvoice" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/jquery-ui.css" rel="stylesheet" />
    <%--<link href="../datatables/css/demo_table.css" rel="stylesheet" type="text/css" />--%>
    <%--<link href="../datatables/css/demo_table_jui.css" rel="stylesheet" type="text/css" />--%>
    <link href="HotelAdmin/css/bootstrap.css" rel="stylesheet" />
    <link href="css/styles/custom.css" rel="stylesheet" />
   <%-- <link href="../examples/carousel/carousel.css" rel="stylesheet" />--%>
    <script src="js/jquery.v2.0.3.js"></script>
    <script src="scripts/ActivityInvoice.js"></script>


    <script type="text/javascript">
        var ReservationID;
        var Status;
        $(document).ready(function () {
            Status = GetQueryStringParamsForAgentActivityUpdates('Status');
            if (Status == "Cancelled") {
                document.getElementById("btn_Cancel").setAttribute("style", "Display:none");
            }
            debugger
            ReservationID = GetQueryStringParamsForAgentActivityUpdates('ReservationId').replace(/%20/g, " ");
            var Uid = GetQueryStringParamsForAgentActivityUpdates('Uid');
            document.getElementById("btn_InvoicePDF").setAttribute("onclick", "GetPDFActInvoice('" + ReservationID + "', '" + Uid + "','" + Status + "')");
            //$("#reservationId").text(GetQueryStringParamsForAgentRegistrationUpdate('ReservationId').replace(/%20/g, ' '));
            //InvoicePrint(ReservationID, Uid);
           //document.getElementById("btn_Cancel").setAttribute("onclick", "OpenCancellationPopup('" + ReservationID + "', '" + Status + "')");
           //document.getElementById("btn_VoucherPDF").setAttribute("onclick", "GetPDFInvoice('" + ReservationID + "', '" + Uid + "','" + Status + "')");

        });

        function GetQueryStringParamsForAgentActivityUpdates(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return sParameterName[1];
                }
            }
        }
    </script>

</head>
<body>

    <div style="margin-left:35%;" id="BtnDiv">
     
        <input type="button" class="btn btn-search" value="Email Invoice" style="cursor: pointer" title="Invoice" data-toggle="modal" data-target="#InvoiceModal" />
       
        <input type="button" class="btn btn-search" value="Download & Print" id="btn_InvoicePDF" />
       
        
    </div>
    <br />
    <div id="maincontainer" runat="server" class="print-portrait-a4" style="font-family: 'Segoe UI', Tahoma, sans-serif; margin: 0px 40px 0px 40px; width: auto; border: 2px solid gray;"></div>

    <div class="modal fade bs-example-modal-lg" id="InvoiceModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 60%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel3" style="text-align: left">Send Invoice</h4>
                </div>
                <div class="modal-body">

                    <table class="table table-hover table-responsive" id="tbl_SendInvoice" style="width: 100%">
                        <tr>
                            <td style="border: none;">
                                <span class="text-left" style="font-weight: bold;">Type your Email : </span>&nbsp;&nbsp;
                                       
                                <input type="text" id="txt_sendInvoice" placeholder="Email adress" class="form-control1 logpadding margtop5 " style="width: 70%;" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_txt_sendInvoice"><b>* This field is required</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: none;">
                                <input id="btn_sendInvoice" type="button" value="Send Invoice" class="btn-search mr20" style="width: 40%; margin-left: 30%" onclick="MailActInvoice();" />
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>

     <div class="modal fade" id="ModelMessege" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 40%; padding-top: 15%">

            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>

                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="frow2">
                            <div>
                                <center><span id="SpnMessege"></span></center>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
   
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
