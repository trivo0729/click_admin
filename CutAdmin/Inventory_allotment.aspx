﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Inventory_allotment.aspx.cs" Inherits="CutAdmin.Inventory_allotment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">

    <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
        <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
    <script src="Scripts/moments.js"></script>
    <script src="Scripts/Inventory.js?v=1.3"></script>
    <script src="Scripts/Hotels/GetRateType.js?v=1.5"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin" style="padding-bottom:0">
            <h1>
                Allotment
                <span class="float-right font18 align-right" style="line-height:1.5;" id="spn_Hotel">Hotel Name, City, Country<br /><span class="red">Room type</span> </span>
            </h1>
        </hgroup>
        <hr>
          <div class=" with-padding">
              <form id="frm_Inventory">
                  <fieldset class="fields-list">            
            <div class="field-block button-height">
                <small class="input-info">You may select more than one date range for which you want to open / close this hotel room</small>
                    <label for="validity" class="label"><b>Validity</b></label>
                    <div id="div_InventoryDate">
                </div>
                    <br />
                <div class="columns">
                    <div class="two-columns three-columns-mobile">
                            <label for="market" class=" ">
                            <p class="reversed-switches">
                                <input type="checkbox" class="switch medium wide" name="btn_slot"  id="chk_Days" checked data-text-on="All Day" data-text-off="Valid Day" onchange="SelectDays(this)">
                            </p>
                        </label>
                    </div>
                    <div class="eight-columns six-columns-mobile divDays" style="display:none">
                        <p>
                        <input type="checkbox" class="checkbox" id="mon" name="days" checked/>
                        <label class="" for="mon">Mon </label>
                        <input type="checkbox" class="checkbox" id="tue" name="days" checked/>
                        <label class="" for="tue">Tue </label>
                        <input type="checkbox" class="checkbox" id="wed" name="days" checked/>
                        <label class="" for="wed">Wed </label>
                        <input type="checkbox" class="checkbox" id="Thu" name="days" checked/>
                        <label class="" for="Thu">Thu </label>
                        <input type="checkbox" class="checkbox" id="fri" name="days" checked/>
                        <label class="" for="fri">Fri </label>
                        <input type="checkbox" class="checkbox" id="sat" name="days" checked/>
                        <label class="" for="sat">Sat </label>
                        <input type="checkbox" class="checkbox" id="sun" name="days" checked/>
                        <label class="" for="sun">Sun </label>

                    </p>
                    </div>

                </div>
                </div>    
                <div class="field-block button-height">
                    <small class="input-info">Select rate type for which you want to open / close this inventory</small>
                    <label for="validity" class="label"><b>Rate type</b></label>
                    <div class="three-columns">                        
                        <select class="select full-width" id="sel_RateType">
                         
                        </select>                       
                    </div>
                </div>
                <div class="field-block button-height">
                    <small class="input-info">Input total no of rooms you want to open for booking</small>
                    <label for="totalrooms" class="label"><b>Total rooms</b></label>
                    <span class="number input small-margin-right">
                        <button type="button" class="button number-down">-</button>
                        <input type="text" value="1" size="3" class="input-unstyled validate[required,min[1],max[200]]" id="txt_TotalRooms">
                        <button type="button" class="button number-up">+</button>
                    </span>                    
                </div>

                <div class="field-block button-height">
                    <small class="input-info">Select here if you have more conditions to define for this inventory</small>
                    <label for="conditions" class="label"><b>Conditions</b></label>
                    <p>
                        Accept booking request even after inventory is closed?
                        <input id="conditions" type="checkbox" class="switch medium" data-checkable-options='{"textOn":"YES","textOff":"NO"}'>
                    </p>
                </div>
                
                <div class="field-block button-height">
                    <a href="javascript:void(0)" class="button small-margin-right" onclick="SaveInventory('Allotment')">
                        <span class="button-icon green-gradient"><span class="icon-download"></span></span>
                        Save
                    </a>
                 
                    <a href="javascript:void(0)" class="button" onclick="CancelInventory()">
                        <span class="button-icon red-gradient"><span class="icon-cross"></span></span>
                        Cancel
                    </a>
                </div>

</fieldset>
              </form>
        
              </div>
    </section>
</asp:Content>
