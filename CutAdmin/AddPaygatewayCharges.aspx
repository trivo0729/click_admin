﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddPaygatewayCharges.aspx.cs" Inherits="CutAdmin.AddPaygatewayCharges" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/PayGatewayCharge.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Payment Gateway Charges</h1>
        </hgroup>
        <hr />
        <div class="with-padding" id="Pmtgtw">
        </div>
    </section>
</asp:Content>
