﻿$(function () {
    GatewayDetails();
});

function GatewayDetails() {
    $("#tbl_GatewayDetails").dataTable().fnClearTable();
    $("#tbl_GatewayDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../Handler/GatewayHandler.asmx/GetPaymentGateway",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);

            }
            else if (result.retCode == 1) {
                var Name = result.Name;
                var row = '';
                for (var i = 0; i < Name.length; i++) {
                    row += '<tr>'
                    row += '<td class="align-center">' + (i + 1) + '</td>'
                    row += '<td>' + Name[i].PaymentGateway + '</td>'
                    row += '<td class="align-center"><span class="button-group children-tooltip actiontab">'
                    row += '<a href="#" class="button" title="Edit" onclick="EditDetail(\'' + Name[i].Id + '\',\'' + Name[i].PaymentGateway + '\')"><span class="icon-pencil"></span></a>'
                    row += '<a href="#" class="button" title="Delete" onclick="DeletePaymentGateway(\'' + Name[i].Id + '\')"><span class="icon-trash"></span></a>'
                    row += '</span></td>'
                    row += '</tr>'
                }
                $('#tbl_GatewayDetails tbody').html(row);
                $("#tbl_GatewayDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
            else if (result.retCode == 0) {
                $("#tbl_GatewayDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
            Success("An error occured while loading details.");
        }
    });
}


function AddGatewayDetails() {
    var GatewayName = $('#txtGatewayName').val();
    if (!GatewayName) {
        Success('Please enter Payment Gateway Name.');
        return false;
    }
    var data = {
        GatewayName: GatewayName,
    }
    $.ajax({
        type: "POST",
        url: "../Handler/GatewayHandler.asmx/AddPaymentGateway",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);

            }
            else if (result.retCode == 1) {
                Success("saved successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
        },
        error: function () {
            Success("An error occured while saving details.");
        }
    });

}

var ID = "";

function EditDetail(Id, Name) {
    ID = Id;
    $('#txtGatewayName').val(Name);
    $('#btnGatewayDetails').attr("onclick", "UpdatDetail()");
    $('#btnGatewayDetails').attr("value", "Update");
    $('#btn_Cancel').css("display", "");
}


function UpdatDetail() {
    var Name = $('#txtGatewayName').val();
    if (!Name) {
        Success('Please enter Payment Gateway Name.');
        return false;
    }

    var data = {
        Id: ID,
        PaymentGateway: Name
    }

    $.ajax({
        type: "POST",
        url: "../Handler/GatewayHandler.asmx/UpdatePaymentGateway",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);

            }
            else if (result.retCode == 1) {
                Success("updated successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
        },
        error: function () {
            Success("An error occured while updating details.");
        }
    });
}

function Cancel() {
    window.location.reload();
}

function DeletePaymentGateway(Id) {
    $.modal({
        content: '<p class="avtiveDea">Are you sure you want to delete this?</strong></p>' +
            '<p class="text-alignright text-popBtn"><button type="button" class="button anthracite-gradient" onclick="Delete(' + Id + ')">OK</button></p>',
        width: 300,
        scrolling: false,
        actions: {
            'Cancle': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Cancle': {
                classes: 'anthracite-gradient glossy',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: false
    });
}

function Delete(Id) {
    var data = {
        Id: Id
    }
    $.ajax({
        type: "POST",
        url: "../Handler/GatewayHandler.asmx/DeletePaymentGateway",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);

            }
            else if (result.retCode == 1) {
                Success("Bank detail deleted successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
        },
        error: function () {
            Success("An error occured while deleting details.");
        }
    });
}
