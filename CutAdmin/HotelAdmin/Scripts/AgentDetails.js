﻿$(document).ready(function () {
    GetAgentDetail();
});
var hiddensid;
var arrFranchisee;
var UpdateUrl = "";
var List_AgentDetails = new Array();
function GetAgentDetail() {
    $("#tbl_AgentDetails").dataTable().fnClearTable();
    $("#tbl_AgentDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "handler/UserHanler.asmx/GetAgentDetail",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_AgentDetails = result.List_Agent;
                htmlGenrator();
            }
            else if (result.retCode == 0) {
                $("#tbl_AgentDetails tbody").remove();
                var tRow = '<tbody>';
                tRow += '<tr> <td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_AgentDetails").append(tRow);

                $("#tbl_AgentDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
        }

    });
}



function UpdateMarkupModal(sid, agentCategory, UserType) {
    debugger;
    //if (agentCategory == "") {
    $("#txt_Markup").val('');
    $("#hdn_Usertype").val(UserType);
    $("#lbl_ErrMarkupDefault").css("display", "");
    //$("#GroupModal").modal('show');
    $("#MarkupModal").modal('show');

    EditMarkupModal(sid);


}

function UpdateRoles(sid) {
    debugger;
    $("#hdnDCode").val(sid);
    //$("#RolesModal").modal('show');
    // AppendFormTable();
    var Name = $.grep(List_AgentDetails, function (p) { return p.sid == sid; })
        .map(function (p) { return p.AgencyName; });
    window.open("ChannelManager.aspx?uid=" + sid + "&Name=" + Name)
    // EditMarkupModal(sid);



}

var bvalid;
var sCountry;
var sCity;
var IAgencyName; var IAgentuniquecode; var IGSTNumber;
var IdtLastAccess, IContactPerson, IsDesignation, IAddress, IDescription, ICountryname, IPinCode, Iphone, IMobile, IFax, Iemail, IWebsite, IPANNo, ICurrencyCode;



function Validate_City() {

    debugger;
    bvalid = true;
    var reg = new RegExp('[0-9]$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

    if (sCountry == "-") {
        bvalid = false;
        //$("#lbl_selCountry").css("display", "");

        Success("Please select Country!");
        return bvalid;
    }
    //if (sCity == "-") {
    //    bvalid = false;
    //    //$("#lbl_selCity").css("display", "");
    //    Success("Please select City!");

    //}

    return bvalid;

}



function AgentDetailsModal(sid) {
    $.ajax({
        type: "POST",
        url: "handler/UserHanler.asmx/GetAdminDetails",
        data: '{"AgentID":"' + sid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            console.log(result);
            if (result.retCode == 1) {
                var arrAgency = result.arrAgency;
                if (arrAgency.GSTNumber == null) {
                    arrAgency.GSTNumber = "";
                }
                $("#hiddenID").val(sid)
                arrAgency.dtLastAccess = moment(arrAgency.dtLastAccess).format("L LTS");
                $.modal({
                    content: '<div class="modal-body">' +
                        '<div class="scrollingDiv">' +
                        ' <div class="columns">' +
                        ' <div class="two-columns bold">Agency Name</div>' +
                        '<div class="four-columns"><span id="spn_CompanyName" class="text-left">' + arrAgency.AgencyName + '</span></div>' +
                        ' <div class="two-columns bold">Registration Date</div>' +
                        ' <div class="four-columns"><span id="spn_Validity" class="text-left">' + arrAgency.dtLastAccess + '</span></div>' +
                        '</div>' +
                        '<div class="columns">' +
                        '<div class="two-columns bold">Contact Person</div>' +
                        '<div class="four-columns"><span id="spn_ContactPerson" class="text-left">' + arrAgency.ContactPerson + '</span></div>' +
                        '<div class="two-columns bold">Designation</div>' +
                        '<div class="four-columns"><span id="spn_Designation" class="text-left">' + arrAgency.Designation + '</span></div>' +
                        '</div> ' +
                        '<div class="columns">' +
                        '<div class="two-columns bold">Phone</div>' +
                        '<div class="four-columns"><span id="spn_Phone" class="text-left">' + arrAgency.phone + '</span></div>' +
                        '<div class="two-columns bold ">Mobile</div>' +
                        '<div class="four-columns"><span id="spn_Mobile" class="text-left">' + arrAgency.Mobile + '</span></div>' +
                        '</div>' +
                        '<div class="columns">' +
                        '<div class="two-columns bold">Address </div>' +
                        '<div class="four-columns"><span id="spn_Address" class="text-left">' + arrAgency.Address + '</span></div>' +
                        '<div class="two-columns bold">City</div>' +
                        '<div class="four-columns"><span id="spn_City" class="text-left">' + arrAgency.Description + '</span></div>' +
                        '</div>' +
                        '<div class="columns">' +
                        '<div class="two-columns bold">Country </div>' +
                        '<div class="four-columns"><span id="spn_Country" class="text-left">' + arrAgency.Country + '</span></div>' +
                        '<div class="two-columns bold">Pin Code </div>' +
                        '<div class="four-columns"><span id="spn_PinCode" class="text-left">' + arrAgency.PinCode + '</span></div>' +
                        '</div>' +
                        '<div class="columns">' +
                        '<div class="two-columns bold">Fax </div>' +
                        '<div class="four-columns"><span id="spn_Fax" class="text-left">' + arrAgency.Fax + '</span></div>' +
                        '<div class="two-columns bold">Email</div>' +
                        '<div class="four-columns"><span id="spn_Email" class="text-left">' + arrAgency.email + '</span></div>' +
                        '</div>' +
                        '<div class="columns">' +
                        '<div class="two-columns bold">Website </div>' +
                        '<div class="four-columns"><span id="spn_Website" class="text-left">' + arrAgency.Website + '</span></div>' +
                        '<div class="two-columns bold">Pan No</div>' +
                        '<div class="four-columns"><span id="spn_PanNo" class="text-left">' + arrAgency.PANNo + '</span></div>' +
                        '</div> ' +
                        '<div class="columns">' +
                        '<div class="two-columns bold">Currency </div>' +
                        '<div class="four-columns"><span id="spn_Currency" class="text-left">' + arrAgency.CurrencyCode + '</span></div>' +
                        '<div class="two-columns bold">GSTN</div>' +
                        '<div class="four-columns"><span id="spn_GST" class="text-left">' + arrAgency.GSTNumber + '</span></div>' +
                        '</div>' +
                        '<div class="columns">' +
                        '<div class="two-columns bold">Agent Code </div>' +
                        '<div class="four-columns"><span id="spn_Code" class="text-left">' + arrAgency.Agentuniquecode + '</span></div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<p class="text-alignright"><a style="cursor:pointer" href="AddSupplier.aspx?sid=' + sid + '&sUniqueCode=' + arrAgency.Agentuniquecode + '&bLoginFlag=' + arrAgency.LoginFlag + '&City=' + arrAgency.Description + '&CurrencyCode=' + arrAgency.CurrencyCode + '&sContactPerson=' + arrAgency.ContactPerson + '&sDesignation=' + arrAgency.Designation + '&sAgencyName=' + arrAgency.AgencyName + '&sEmail=' + arrAgency.email + '&sAddress=' + arrAgency.Address + '&sCountry=' + arrAgency.Country + '&sUsertype=' + arrAgency.UserType + '&nPinCode=' + arrAgency.PinCode + '&IATANumber=' + arrAgency.IATANumber + '&nPhone=' + arrAgency.phone + '&nMobile=' + arrAgency.Mobile + '&nFax=' + arrAgency.Fax + '&nPAN=' + arrAgency.PANNo + '&sWebsite=' + arrAgency.Website + '&GSTNumber=' + arrAgency.GSTNumber + '" class="button compact anthracite-gradient icon-gear editpro">Edit Profile</a></p>',
                    title: 'Admin Profile',
                    width: 800,
                    scrolling: false,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'huge anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                });
            }
        },
    });
}

function AgentDetailsModalForTransfer(AgencyName, dtLastAccess, ContactPerson, sDesignation, Address, Description, Countryname, PinCode, phone, Mobile, Fax, email, Website, PANNo, Agentuniquecode, GSTNumber, CurrencyCode) {

    IAgencyName = AgencyName;
    IAgentuniquecode = Agentuniquecode;
    if (GSTNumber == "null") {
        IGSTNumber = "";
    }
    else {
        IGSTNumber = GSTNumber;
    }


    IdtLastAccess = dtLastAccess;
    IContactPerson = ContactPerson;
    IsDesignation = sDesignation;
    IAddress = Address;
    IDescription = Description;
    ICountryname = Countryname;
    IPinCode = PinCode;
    Iphone = phone;
    IMobile = Mobile;
    IFax = Fax;
    Iemail = email;
    IWebsite = Website;
    IPANNo = PANNo;
    ICurrencyCode = CurrencyCode;

    $("#spn_ICompanyName").html(AgencyName);
    $("#spn_IValidity").html(dtLastAccess);
    $("#spn_IContactPerson").html(ContactPerson);
    $("#spn_IDesignation").html(sDesignation);
    $("#spn_IAddress").html(Address);
    $("#spn_ICity").html(Description);
    $("#spn_ICountry").html(Countryname);
    $("#spn_IPinCode").html(PinCode);
    $("#spn_IPhone").html(phone);
    $("#spn_IMobile").html(Mobile);
    $("#spn_IFax").html(Fax);
    $("#spn_IEmail").html(email);
    $("#spn_IWebsite").html(Website);
    $("#spn_IPanNo").html(PANNo);
    $("#spn_ICode").html(Agentuniquecode);
    $("#spn_IGST").html(IGSTNumber);
    $("#spn_ICurrency").html(ICurrencyCode);
}
var MyMail = "";
//function PasswordModal(sid, uid, AgencyName, password) {
//    $('#lbl_ErrPassword').css("display", "none");
//    hiddensid = sid;
//    MyMail = uid;
//    $("#txt_AgentId").val(uid);
//    $("#txt_AgencyName").val(AgencyName);
//    GetPassword(password);
//    //$("#txt_Password").val(password);
//}

function ChangePassword() {
    if ($('#txt_Password').val() != $('#hddn_Password').val()) {

        if ($('#txt_Password').val() != "") {

            $('#lbl_ErrPassword').css("display", "none");
            $.modal({
                content: '<div class="modal-body">' +
                    '<div class="scrollingDiv">' +
                    '<div class="columns">' +
                    '<div class="twelve-columns bold">Are you sure you want to change password?</div>' +
                    '</div>' +
                    '<div class="columns">' +
                    '<div class="nine-columns bold">&nbsp;</div>' +
                    '<div class="three-columns bold"><button type="button" class="button anthracite-gradient" onclick="ChangePass()">Ok</button>' +
                    '</div></div></div>',
                width: 300,
                scrolling: false,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'anthracite-gradient glossy displayNone',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: true
            });
            //Ok("Are you sure you want to change password?", "ChangePass", null)
        }
        else if ($('#txt_Password').val() == "") {
            $('#lbl_ErrPassword').css("display", "");
        }
    }
    else {
        $.modal.alert('No Change found in Password!', {
            buttons: {
                'Cancel': {
                    classes: 'huge anthracite-gradient displayNone',
                    click: function (win) { win.closeModal(); }
                }
            }
        });
    }

}
function ChangePass() {
    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/AgentChangePassword",
        data: '{"sid":"' + hiddensid + '","password":"' + $('#txt_Password').val() + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session != 1) {
                Success("Session Expired");
            }
            if (result.retCode == 1) {
                Success("Password changed successfully");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
            if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
        }
    });
}
function SendPasswordModal(uid, password) {
    $('#lbl_ErrEmailId').css("display", "none");
    $('#lbl_ErrEmailId').html("* This field is required");
    $('#lbl_ErrsendPassword').css("display", "none");
    $("#txt_EmailId").val(uid);
    hiddensid = uid;
    GetPassword(password);
}

function SendPassword() {
    $('#lbl_ErrEmailId').css("display", "none");
    $('#lbl_ErrEmailId').html("* This field is required");
    $('#lbl_ErrsendPassword').css("display", "none");
    var emailReg = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
    var bvalid = true;
    var email = $("#txt_AgentId").val();
    var password = $("#txt_SendPassword").val();
    if (email == "") {
        $('#lbl_ErrEmailId').css("display", "");
        bvalid = false;
    }
    else if (email != "" && !(emailReg.test(email))) {
        $('#lbl_ErrEmailId').css("display", "");
        $('#lbl_ErrEmailId').html("Invalid Email");
        bvalid = false;
    }
    if (bvalid == true) {
        $.modal({
            content: '<div class="modal-body">' +
                '<div class="scrollingDiv">' +
                '<div class="columns">' +
                '<div class="twelve-columns bold">Are you sure you want to send password?</div>' +
                '</div>' +
                '<div class="columns">' +
                '<div class="nine-columns bold">&nbsp;</div>' +
                '<div class="three-columns bold"><button type="button" class="button anthracite-gradient" onclick="Send(\'' + email + '\')">Ok</button>' +
                '</div></div></div>',
            width: 300,
            scrolling: false,
            actions: {
                'Close': {
                    color: 'red',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttons: {
                'Close': {
                    classes: 'anthracite-gradient glossy displayNone',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttonsLowPadding: true
        });
        //Ok("Are you sure you want to send password?", "Send", [email])
    }
}
function Send(email) {
    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/MailPassword",
        data: '{"sEmail":"' + MyMail + '","sTo":"' + email + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            debugger;
            if (result.retCode == 1) {
                Success("Password Send to mail.");
            }
        },
        error: function () {
            Success("An error occured while sending password.");
        }
    });
    //Cancel()
}
function GetPassword(sid, uid, AgencyName, password) {
    $("#txt_Password").val('');
    $.ajax({
        type: "POST",
        url: "Handler/GenralHandler.asmx/GetPassword",
        data: '{"password":"' + password + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            debugger;
            if (result.retCode == 1) {
                pass = result.password;
                $.modal({
                    content: '<div class="modal-body">' +
                        '<div class="scrollingDiv">' +
                        '<div class="columns">' +
                        '<div class="three-columns bold">Agency Name</div>' +
                        '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="Agency-value" value="' + AgencyName + '" class="input-unstyled full-width" readonly="readonly" type="text"></div></div></div> ' +
                        '<div class="columns">' +
                        '<div class="three-columns bold">User ID</div>' +
                        '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_AgentId" value="' + uid + '" class="input-unstyled full-width" readonly="readonly" type="text"></div></div></div> ' +
                        '<div class="columns">' +
                        '<div class="three-columns bold">Password</div>' +
                        '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_Password" value="' + pass + '" class="input-unstyled full-width" type="text"></div></div> ' +
                        '</div>' +
                        '<div class="columns">' +
                        '<div class="three-columns bold">&nbsp;</div>' +
                        '<div class="nine-columns bold"><button type="button" class="button anthracite-gradient" onclick="SendPassword()">Email</button>&nbsp;' +
                        '<button type="button" class="button anthracite-gradient" onclick="ChangePassword()">Change&nbsp;Password</button></div>' +
                        '</div></div></div>',

                    title: 'Edit Password',
                    width: 500,
                    scrolling: false,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'anthracite-gradient glossy displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                });
                //$("#txt_Password").val(pass);
                $("#hddn_Password").val(pass);
                //$("#txt_SendPassword").val(pass);
            }
        },
        error: function () {
            Success("An error occured while loading password.")
        }
    });
}

function hidealert() {
    $("#alSuccess").css("display", "none");
    $("#alError").css("display", "none");
}

function DeleteAgentID(uid, agencyname) {
    debugger;
    $("#alSuccess").css("display", "none");
    $("#alError").css("display", "none");
    $("#spnMsg").html("Agent status has been changed successfully!");
    debugger;
    Ok("Are you sure you want to delete  <span class=\"orange\"> " + agencyname + "</span>?", "Delete", [uid])
}
function Delete(uid) {
    $.ajax({
        url: "GenralHandler.asmx/DeleteAgentID",
        type: "post",
        data: '{"uid":"' + uid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                Success("Supplier has been deleted successfully!");
            }
            else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
        }
    });
}


function Activate(sid, flag, agencyname, agentCategory, UserType, AgentUniqueCode) {
    var status = flag.replace("True", "0").replace("False", "1");
    $.modal({
        content: '<p style="font-size:15px" class="avtiveDea">Are you sure you want to ' + flag.replace("True", "Deactivate").replace("False", "Activate") + "<span class=\"orange\"> " + agencyname + '</span>?</p>' +
            '<p class="text-alignright text-popBtn"><button type="button" class="button anthracite-gradient" onclick="Active(\'' + sid + '\' , \'' + status + '\')">OK</button></p>',
        width: 500,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Cancel': {
                classes: 'anthracite-gradient glossy',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: false
    });
}
function Active(sid, status) {
    $.ajax({
        url: "Handler/GenralHandler.asmx/ActivateLogin",
        type: "post",
        data: '{"sid":"' + sid + '","status":"' + status + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                Success("Supplier status has been changed successfully!")
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
            else if (result.retCode == 0) {
                Success("Something went wrong while processing your request! Please try again.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
            setTimeout(function () {
                window.location.reload();
            }, 2000);

        }
    });
}



function AddGroup() {
    $("#lbl_ErrMarkupgroup").css("display", "none");
    var UserType = $("#hdn_Usertype").val();
    var sid = $("#hdn_sid").val();
    var GroupId = $("#selGroup option:selected").val();
    var GroupName = $("#selGroup option:selected").text();
    var flag = $("#hdn_flag").val();
    var AgentUniqueCode = $("#hdn_AgentUniqueCode").val();
    var agencyname = $("#hdn_agencyName").val();
    if (GroupId != "-") {
        $.ajax({
            url: "GenralHandler.asmx/AddGroup",
            type: "post",
            data: '{"sid":"' + sid + '","UserType":"' + UserType + '","GroupId":"' + GroupId + '","GroupName":"' + GroupName + '","AgentUniqueCode":"' + AgentUniqueCode + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    $("#GroupModal").modal('hide');
                    Activate(sid, flag, agencyname, GroupName, UserType, AgentUniqueCode);

                } else if (result.retCode == 0) {
                    Success("Something went wrong while processing your request! Please try again.");
                }
            },
            error: function () {
                Success('Error occured while processing your request! Please try again.');
            }
        });
    }
    else {
        $("#lbl_ErrMarkupgroup").css("display", "");
    }
}

function ExportAgentDetailsToExcel(Type) {
    debugger;
    window.location.href = "Handler/ExportToExcelHandler.ashx?datatable=AgentDetails&Type=" + Type;
}

// Assign Franchisee //
function AssignFranchisee() {
    var UserId = $("#sel_Franchisee").val();
    UserId = UserId.split("^")
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/AssignFranchisee",
        data: '{"uid":"' + hiddensid + '","FranchId":"' + UserId[0] + '","ParentId":"' + UserId[1] + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#FranchiseeModal").modal("hide");
                GetAgentDetail()
                Success("Franchisee Assigned Successfully")
            }
            else {
                $("#FranchiseeModal").modal("hide");
                Success("An error occured while Assigning Franchisee")
            }
        }
    })

}
function GetFranchisee() {
    $("#sel_Franchisee").empty();
    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/GetFranchiseeName",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrFranchisee = result.tbl_Franchisee;
                var option = '';
                option += '<option value="-">-Select Franchisee-</option>'
                option += '<option value="' + '' + '^' + 127 + '">ClickUrTrip</option>'
                for (var i = 0; i < arrFranchisee.length; i++) {
                    option += '<option value="' + arrFranchisee[i].FranchiseeId + "^" + arrFranchisee[i].sid + '">' + arrFranchisee[i].AgencyName + '</option>'
                }

                $("#sel_Franchisee").append(option)
            }
        }
    })
}
function FranchiseeModal(uid, FranchId, ParentId) {
    hiddensid = uid;
    $("#sel_Franchisee").val(FranchId + "^" + ParentId)
    $("#FranchiseeModal").modal("show");
}

//For Infozeal Transfer

function AgencyTranfer() {

    var Data = {
        IAgencyName: IAgencyName,
        IAgentuniquecode: IAgentuniquecode,
        IGSTNumber: IGSTNumber,
        IdtLastAccess: IdtLastAccess,
        IContactPerson: IContactPerson,
        IsDesignation: IsDesignation,
        IAddress: IAddress,
        IDescription: IDescription,
        ICountryname: ICountryname,
        IPinCode: IPinCode,
        Iphone: Iphone,
        IMobile: IMobile,
        IFax: IFax,
        Iemail: Iemail,
        IWebsite: IWebsite,
        IPANNo: IPANNo

    }

    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/AgencyTranfer",
        data: JSON.stringify(Data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#AgentDetailsModalForTransfer").modal("hide");
                GetAgentDetail()
                Success("Agency Transfered Successfully")
            }
            else {
                $("#AgentDetailsModalForTransfer").modal("hide");
                Success("An error occured while Agency Transfered")
            }
        }
    })

}

function RedirectToEdit() {
    window.location.href = "AddAgentDetails.aspx?hiddensid=" + $("#hiddenID").val();
}
function Search() {

    $("#tbl_AgentDetails").dataTable().fnClearTable();
    $("#tbl_AgentDetails").dataTable().fnDestroy();


    var Name = $("#txt_AgencyList").val();
    var Type = $("#AgentType").val();
    var Code = $("#AgentCode").val();
    var Group = $("#selGroup option:selected").text();
    var Status = $("#selStatus").val();
    var Country = $("#selCountry option:selected").text();
    var City = $("#selCity option:selected").text();
    var MinBalance = $("#MinBalance").val();

    var data = {
        Name: Name,
        Type: Type,
        Code: Code,
        Group: Group,
        Status: Status,
        Country: Country,
        City: City,
        MinBalance: MinBalance
    }

    $.ajax({
        type: "POST",
        url: "handler/UserHanler.asmx/Search",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            // var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                List_AgentDetails = result.List_Agent;
                htmlGenrator();
            }
            else if (result.retCode == 0) {
                $("#tbl_AgentDetails tbody").remove();
                var tRow = '<tbody>';
                //tRow += '<tr><td align="center" style="padding-top: 2%" colspan="4"><span><b>No record found</b></span></td></tr>';
                tRow += '<tr> <td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td><td align="center" style="padding-top: 2%"><span><b>No record found</b></span></td></tr>';
                tRow += '</tbody>';
                $("#tbl_AgentDetails").append(tRow);

                $("#tbl_AgentDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
            Success("An error occured while loading details.")
        }
    });

}

function htmlGenrator() {
    var tRow = "";
    var input = "\/Date(1458845940000)\/";
    for (var i = 0; i < List_AgentDetails.length; i++) {

        var CurrencyClass = "";
        if (List_AgentDetails[i].CurrencyCode == "AED") {
            CurrencyClass = "Currency-AED"
        }
        else if (List_AgentDetails[i].CurrencyCode == "SAR") {
            CurrencyClass = "Currency-SAR"
        }
        else if (List_AgentDetails[i].CurrencyCode == "EUR") {
            CurrencyClass = "fa fa-gbp"
        }
        else if (List_AgentDetails[i].CurrencyCode == "USD") {
            CurrencyClass = "fa fa-dollar"
        }
        else if (List_AgentDetails[i].CurrencyCode == "INR") {
            CurrencyClass = "fa fa-inr"
        }
        moment.locale('India Standard Time');
        List_AgentDetails[i].dtLastAccess = moment(List_AgentDetails[i].dtLastAccess).format("L LTS");
        tRow += '<tr>'
        tRow += '<td style="width:5%">' + (i + 1) + '</td>';
        tRow += '<td style="width:25%"><a style="cursor:pointer" data-toggle="modal" data-target="#AgencyDetailModal" onclick="AgentDetailsModal(\'' + List_AgentDetails[i].sid + '\'); return false" title="Click to view Agency Details">' + List_AgentDetails[i].AgencyName.toUpperCase() + '</a></td>';
        tRow += '<td style="width:20%"><a style="cursor:pointer" data-toggle="modal" data-target="#PasswordModal" onclick="PasswordModal(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].uid + '\',\'' + List_AgentDetails[i].AgencyName + '\',\'' + List_AgentDetails[i].password + '\'); return false" title="Click to edit Password">' + List_AgentDetails[i].uid + ' </a></td>';
        tRow += '<td style="width:10%">' + List_AgentDetails[i].Agentuniquecode + '</td>';
        // tRow += '<td style="width:10%"><i class="' + CurrencyClass + '"></i> ' + List_AgentDetails[i].AvailableCredit + '</td>';


        //if (List_AgentDetails[i].LoginFlag == "True")
        //    tRow += '<td style="width:16%"  align="center"><span class="button-group"><label for="chk_On' + i + '" class="button blue-active active"><input type="radio" name="button-radio' + i + '" id="chk_On' + i + '" value="On" onclick="Activate(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].LoginFlag + '\',\'' + List_AgentDetails[i].AgencyName + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')">Yes</label><label for="chk_Off' + i + '" class="button red-active"><input type="radio" name="button-radio' + i + '" id="chk_Off' + i + '" value="Off" onclick="Activate(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].LoginFlag + '\',\'' + List_AgentDetails[i].AgencyName + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')">No</label></span></td>';
        //else
        //    tRow += '<td style="width:16%"  align="center"><span class="button-group"><label for="chk_On' + i + '" class="button blue-active"><input type="radio" name="button-radio' + i + '" id="chk_On' + i + '" value="On" onclick="Activate(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].LoginFlag + '\',\'' + List_AgentDetails[i].AgencyName + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')">Yes</label><label for="chk_Off' + i + '" class="button red-active active"><input type="radio" name="button-radio' + i + '" id="chk_Off' + i + '" value="Off" onclick="Activate(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].LoginFlag + '\',\'' + List_AgentDetails[i].AgencyName + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')">No</label></span></td>';


        if (List_AgentDetails[i].LoginFlag == 'True') {
            tRow += '<td class="align-center"><input type="checkbox" id="chk_On' + List_AgentDetails[i].sid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1" checked onclick="Activate(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].LoginFlag + '\',\'' + List_AgentDetails[i].AgencyName + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')"></td>';
        }
        else {
            tRow += '<td class="align-center"><input type="checkbox" id="chk_On' + List_AgentDetails[i].sid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1" onclick="Activate(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].LoginFlag + '\',\'' + List_AgentDetails[i].AgencyName + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')"></td>';
        }



        tRow += '<td tyle="width:14%" class="align-center"><span class="button-group children actiontab">'
        tRow += '			<a href="#" class="button" title="Staff Roles" onclick="GetFormList(\'' + List_AgentDetails[i].sid + '\')"><span class="icon-user"></span></a>'
        tRow += '			<a href="#" class="button" title="Assign Payment Gateway" onclick="GetPaymentGateway(\'' + List_AgentDetails[i].sid + '\')"><span class="icon-arrow tracked"></span></a>'
        tRow += '			<a href="#" class="button" title="Assign API Roles" onclick="SetApiRoles(\'' + List_AgentDetails[i].sid + '\')"><span class="icon-cc-share tracked"></span></a>'
        //tRow += '<a target="_blank" href="UpdateCredit.aspx?UniqueCode=' + List_AgentDetails[i].Agentuniquecode + '&sid=' + List_AgentDetails[i].sid + '&Name=' + List_AgentDetails[i].AgencyName + '" class="button" title="Update Credit" ><span class="fa fa-credit-card"></span></a>'
        // tRow += '<a href="#" onclick="UpdateCommissionModal(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')" class="button" title="Commission"><span class="icon-pencil"></span></a>'
        //trow += '<a href="#" class="button" title="billing cycle" onclick="setcommisioncycle(\'' + list_agentdetails[i].sid + '\',\'' + list_agentdetails[i].agentcategory + '\',\'' + list_agentdetails[i].usertype + '\')"><span class="icon-cycle"></span></a>'
        // tRow += '<a href="#" onclick="UpdateIndiviualMarkupModal(\'' + List_AgentDetails[i].sid + '\',\'' + List_AgentDetails[i].agentCategory + '\',\'' + List_AgentDetails[i].UserType + '\')" class="button" title="Individual Markup"><span class="icon-user"></span></a>'
        // tRow += '<a href="ChannelManager.aspx?UniqueCode=' + List_AgentDetails[i].Agentuniquecode + '&sid=' + List_AgentDetails[i].sid + '&Name=' + List_AgentDetails[i].AgencyName + '" class="button" title="Channel Manager"><span class="fa fa-user-plus"></span></a>'
        tRow += '</span></td>'
        tRow += '</tr>';

    }
    $("#tbl_AgentDetails tbody").append(tRow);

    $(".tiny").click(function () {
        $(this).find("input:checkbox").click();
    })

    $("#tbl_AgentDetails").dataTable({
        bSort: false, sPaginationType: 'full_numbers',
    });
    $('#tbl_AgentDetails').removeAttr("style");
}

function SetCommisionCycle(Id, Category, UserType) {
    $.modal({
        content: '<div class="modal-body">' +
            '<div class="scrollingDiv">' +
            '<div class="columns">' +
            '<div class="four-columns bold">Billing Cycle</div>' +
            '<div class="eight-columns"><div class="input full-width"><input onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="prompt-value" id="txt_Billing" value="" class="input-unstyled full-width" type="text"></div></div></div> ' +
            '</div>' +
            '<div class="columns">' +
            '<div class="four-columns bold">Comission On Checking</div>' +
            '<div class="eight-columns"><div class="input full-width"><input type="checkbox" id="chk_OnChecking" name="medium-label-3" id="medium-label-3" class="switch tiny" value="true" ></div></div></div> ' +
            '</div>' +
            '<div class="columns">' +
            '<div class="four-columns bold">Refund On Cancellation</div>' +
            '<div class="eight-columns"><div class="input full-width"><input type="checkbox" id="chk_OnCancel" name="medium-label-3" id="medium-label-3" class="switch tiny" value="true" ></div></div></div> ' +
            '</div>' +
            '<p class="text-alignright"><a style="cursor:pointer" href="#" class="button compact anthracite-gradient editpro" onclick="AddBillingCycle(\'' + Id + '\',\'' + Category + '\',\'' + UserType + '\')">Save</a></p>' +
            '</div>',
        title: 'Comission Seattings',
        width: 300,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'anthracite-gradient glossy displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}

function PasswordModal(sid, uid, AgencyName, password) {
    $('#lbl_ErrPassword').css("display", "none");
    hiddensid = sid;
    MyMail = uid;
    GetPassword(sid, uid, AgencyName, password);
};
////****Supplier Api Roles****//////
function SetApiRoles(sid) {
    hiddensid = sid;
    $('#lbl_ErrServiceTaxPercent').css("display", "none");
    $('#lbl_ErrServiceTaxPercent').html("* This field is required");
    $.ajax({
        type: "POST",
        url: "../handler/ChannelHandler.asmx/GetAuthorityForAdmin?sid=" + sid,
        data: '{"Sid":"' + hiddensid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arAPIList = result.sAPI;
                trForms = '';
                if (arAPIList.length > 0) {
                    trForms = '';
                    for (i = 0; i < arAPIList.length; i = i + 3) {
                        if (i < arAPIList.length) {

                            trForms += '<div class="columns">';
                            trForms += '<div class="two-columns eight-columns-mobile bold">';
                            trForms += '<label for="chk' + arAPIList[i].sFormName + '">' + arAPIList[i].sDisplayName + ': </label>';
                            trForms += '</div>';
                            trForms += '<div class="two-columns four-columns-mobile">';
                            trForms += '<p class="button-height">';
                            trForms += ' <input type="checkbox"  id="chk' + arAPIList[i].sFormName + '" class="checkbox mid-margin-left chkFrm" value="' + arAPIList[i].nId + '">';
                            trForms += ' <span class="icon-gear" style="margin-left:10px;" onclick="SetSpplierCredential(\'' + sid + '\',\'' + arAPIList[i].sFormName + '\')"></span></p>';
                            trForms += '</div>';

                            if ((i + 1) < arAPIList.length) {
                                trForms += '<div class="two-columns eight-columns-mobile bold">';
                                trForms += '<label for="chk' + arAPIList[i + 1].sFormName + '">' + arAPIList[i + 1].sDisplayName + ': </label>';
                                trForms += '</div>';
                                trForms += '<div class="two-columns four-columns-mobile">';
                                trForms += '<p class="button-height">';
                                trForms += ' <input type="checkbox"  id="chk' + arAPIList[i + 1].sFormName + '" class="checkbox mid-margin-left chkFrm" value="' + arAPIList[i + 1].nId + '">';
                                trForms += ' <span class="icon-gear" style="margin-left:10px;" onclick="SetSpplierCredential(\'' + sid + '\',\'' + arAPIList[i + 1].sFormName + '\')"></span></p>';
                                trForms += '</div>';
                            }
                            if ((i + 2) < arAPIList.length) {
                                trForms += '<div class="two-columns eight-columns-mobile bold">';
                                trForms += '<label for="chk' + arAPIList[i + 2].sFormName + '">' + arAPIList[i + 2].sDisplayName + ': </label>';
                                trForms += '</div>';
                                trForms += '<div class="two-columns four-columns-mobile">';
                                trForms += '<p class="button-height">';
                                trForms += ' <input type="checkbox"  id="chk' + arAPIList[i + 2].sFormName + '" class="checkbox mid-margin-left chkFrm" value="' + arAPIList[i + 2].nId + '">';
                                trForms += ' <span class="icon-gear" style="margin-left:10px;" onclick="SetSpplierCredential(\'' + sid + '\',\'' + arAPIList[i + 2].sFormName + '\')"></span></p>';
                                trForms += '</div>';
                                trForms += '</div>';
                            }
                            //    trForms += '<div class="col-md-4">' + arAPIList[i + 1].sDisplayName + ': <label style="float:right" for="chk' + arAPIList[i + 1].sFormName + '"><input id="chk' + arAPIList[i + 1].sFormName + '" type="checkbox" value="' + arAPIList[i + 1].sFormName + '" class="switch tiny"/> </label></div>';
                            //if ((i + 2) < arAPIList.length)
                            //    trForms += '<div class="col-md-4">' + arAPIList[i + 2].sDisplayName + ': <label style="float:right" for="chk' + arAPIList[i + 2].sFormName + '"><input id="chk' + arAPIList[i + 2].sFormName + '" type="checkbox" value="' + arAPIList[i + 2].sFormName + '" class="switch tiny"/> </label></div>';

                            //trForms += '</div>';

                        }
                    }
                    trForms += '<div style="float:right;margin-top:2%">'
                    trForms += '<button type="button" onclick="AddApiRoleForAdmin(\'' + sid + '\')" title="Submit Details" class="button anthracite-gradient AddMail">Add</button></div>';
                    trForms += '</div>';
                    trForms += '';
                    $("#tblAPI").append(trForms);
                    if ($("#hdnDCode").val() == "") {
                        $('input[type=checkbox]').attr("disabled", true);
                    }
                    $.modal({
                        content: '' + trForms + '',
                        title: 'Add Supplier API Role',
                        width: 650,
                        scrolling: false,
                        actions: {
                            'Close': {
                                color: 'red',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttons: {
                        },
                        buttonsLowPadding: true
                    });

                }
                arAPIListForRole = result.AssigndAPI;
                for (j = 0; j < arAPIList.length; j++) {
                    var checked = $.grep(arAPIListForRole, function (p) { return p.sFormName == arAPIList[j].sFormName; })
                  .map(function (p) { return p.sFormName; });
                    if (checked.length != 0)
                        $('#chk' + checked).click()
                    else
                        $('#chk' + arAPIList[j].sFormName)
                }
                $('#chkCropping')
            }
        },
        error: function () {
        }
    });
}

function SetSpplierCredential(sid, Suppliercode) {
    $.ajax({
        type: "POST",
        url: "../handler/ChannelHandler.asmx/GetSupplierCredential",
        data: '{"sid":"' + sid + '","SupplierCode":"' + Suppliercode + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arAPIList = result.Arr;
                trForms = '';
                //if (arAPIList.length > 0) {
                trForms = '';
                trForms += '<div class="columns">';
                trForms += '<div class="three-columns four-columns-mobile">';
                trForms += '<label >IP:</label>';
                trForms += '</div>';
                trForms += '<div class="six-columns twelve-columns-mobile">';
                trForms += '<div class="input full-width">';
                if (arAPIList.length != 0)
                    trForms += ' <input type="text" id="txt_Ip" value="' + arAPIList[0].IP + '" class="input-unstyled full-width">';
                else
                    trForms += ' <input type="text" id="txt_Ip" class="input-unstyled full-width">';
                trForms += '</div>';
                trForms += '</div>';
                trForms += '</div>';
                trForms += '<div class="columns">';
                trForms += '<div class="three-columns four-columns-mobile bold">';
                trForms += '<label >URL:</label>';
                trForms += '</div>';
                trForms += '<div class="six-columns eight-columns-mobile">';
                trForms += '<div class="input full-width">';
                if (arAPIList.length != 0)
                    trForms += ' <input type="text" id="txt_URL" value="' + arAPIList[0].URL + '" class="input-unstyled full-width">';
                else
                    trForms += ' <input type="text" id="txt_URL"  class="input-unstyled full-width">';
                trForms += '</div>';
                trForms += '</div></div>';

                trForms += '<div class="columns">';
                trForms += '<div class="three-columns four-columns-mobile bold">';
                trForms += '<label >UserName:</label>';
                trForms += '</div>';
                trForms += '<div class="six-columns eight-columns-mobile">';
                trForms += '<div class="input full-width">';
                if (arAPIList.length != 0)
                    trForms += ' <input type="text" id="txt_username" value="' + arAPIList[0].sUserName + '"  class="input-unstyled full-width">';
                else
                    trForms += ' <input type="text" id="txt_username" class="input-unstyled full-width">';
                trForms += '</div>';
                trForms += '</div></div>';
                trForms += '<div class="columns">';
                trForms += '<div class="three-columns four-columns-mobile bold">';
                trForms += '<label >Password:</label>';
                trForms += '</div>';
                trForms += '<div class="six-columns eight-columns-mobile">';
                trForms += '<div class="input full-width">';
                if (arAPIList.length != 0)
                    trForms += ' <input type="text" id="txt_Password" value="' + arAPIList[0].sPassword + '" class="input-unstyled full-width">';
                else
                    trForms += ' <input type="text" id="txt_Password" class="input-unstyled full-width">';
                trForms += '</div>';
                trForms += '</div></div>';
                trForms += '<div class="columns">';
                trForms += '<div class="three-columns four-columns-mobile bold">';
                trForms += '<label >Access Key:</label>';
                trForms += '</div>';
                trForms += '<div class="six-columns eight-columns-mobile">';
                trForms += '<div class="input full-width">';
                if (arAPIList.length != 0)
                    trForms += ' <input type="text" id="Accesskey" value="' + arAPIList[0].sAccessKey + '" class="input-unstyled full-width">';
                else
                    trForms += ' <input type="text" id="Accesskey" class="input-unstyled full-width">';
                trForms += '</div>';
                trForms += '</div></div>';
                trForms += '<div class="columns">';
                trForms += '<div class="three-columns four-columns-mobile bold">';
                trForms += '<label >Supplier Code:</label>';
                trForms += '</div>';
                trForms += '<div class="six-columns eight-columns-mobile">';
                trForms += '<div class="input full-width">';
                if (arAPIList.length != 0)
                    trForms += ' <input type="text" id="txt_code" value="' + arAPIList[0].sSupplierCode + '" class="input-unstyled full-width">';
                else
                    trForms += ' <input type="text" id="txt_code" class="input-unstyled full-width">';
                trForms += '</div>';
                trForms += '</div></div>';
                //    trForms += '<div class="col-md-4">' + arAPIList[i + 1].sDisplayName + ': <label style="float:right" for="chk' + arAPIList[i + 1].sFormName + '"><input id="chk' + arAPIList[i + 1].sFormName + '" type="checkbox" value="' + arAPIList[i + 1].sFormName + '" class="switch tiny"/> </label></div>';
                //if ((i + 2) < arAPIList.length)
                //    trForms += '<div class="col-md-4">' + arAPIList[i + 2].sDisplayName + ': <label style="float:right" for="chk' + arAPIList[i + 2].sFormName + '"><input id="chk' + arAPIList[i + 2].sFormName + '" type="checkbox" value="' + arAPIList[i + 2].sFormName + '" class="switch tiny"/> </label></div>';

                //trForms += '</div>';


                trForms += '<div style="float:right;margin-top:2%">'
                trForms += '<button type="button" onclick="AddApiCredential(\'' + sid + '\')" title="Submit Details" class="button anthracite-gradient AddMail">Add</button></div>';
                trForms += '</div>';
                trForms += '';
                $.modal({
                    content: '' + trForms + '',
                    title: 'Add Supplier API Credential',
                    width: 450,
                    scrolling: false,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                    },
                    buttonsLowPadding: true
                });

                //}
            }
        },
        error: function () {
        }
    });
}
function AddApiCredential(sid) {
    var IP = $("#txt_Ip").val();
    var URL = $("#txt_URL").val();
    var UserName = $("#txt_username").val();
    var Password = $("#txt_Password").val();
    var Accesskey = $("#Accesskey").val();
    var Suppliercode = $("#txt_code").val();

    if (!IP) {
        Success('Please enter IP.');
        return false;
    } else if (!URL) {
        Success('Please enter URL.');
        return false;
    } else if (!UserName) {
        Success('Please enter UserName.');
        return false;
    } else if (!Password) {
        Success('Please enter Password.');
        return false;
    } else if (!Accesskey) {
        Success('Please enter Access key.');
        return false;
    }else if (!Suppliercode) {
        Success('Please enter Supplier Code.');
        return false;
    }

    var data = {
        ssid: sid,
        IP: IP,
        URL: URL,
        UserName: UserName,
        Password: Password,
        Accesskey:Accesskey,
        Suppliercode: Suppliercode
    }
    $.ajax({
        type: "POST",
        url: "../Handler/ChannelHandler.asmx/AddSupplierCredential",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);

            }
            else if (result.retCode == 1) {
                Success("Api Credential saved successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
            else if (result.retCode == 2) {
                Success("Api Credential Updated successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
        },
        error: function () {
            Success("An error occured while saving details.");
        }
    });
}

var arRoleList = new Array();
var arFormList = new Array();
var arFormListForRole = new Array();
var arrayToSubmit = new Array();
var arrApiRoleForAdmin = new Array();
function AddApiRoleForAdmin(sid) {
    arrApiRoleForAdmin = new Array();
    var arrFrm = $(".chkFrm")
    for (var i = 0; i < arrFrm.length; i++) {
        if (arrFrm[i].classList.length == 5) {
            arrApiRoleForAdmin.push({
                ApiId: arrFrm[i].childNodes[1].value,
                ParentId: sid,
            });
        }
    }
    if (arrApiRoleForAdmin.length == 0) {
        Success('Please select APi!');
        return false;
    }
    var data = { arrayJson: arrApiRoleForAdmin };
    if (HiddenId != 'null') {
        $.ajax({
            type: "POST",
            url: "../Handler/ChannelHandler.asmx/AddAuthorityForAdmin",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Role authorities has been changed successfully!");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000)
                }
                else if (result.retCode == 0) {
                    Success("Something went wrong!");
                }
            },
            error: function () {
                Success("Error occured while submitting checked Role.");
            }
        });
    }
    else {
        Success('Please select a Role!');
        $("#selRoles").focus()
    }
}
////****Supplier Api Roles****//////

function GetFormList(sid) {
    HiddenId = sid;
    $("#tblStaffForms").empty();
    $.ajax({
        type: "POST",
        url: "Handler/RoleManagementHandler.asmx/GetAdminFormList?sid=" + sid,
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arFormList = result.Arr;
                var arrAuthForm = result.arrAuthForm;
                var ListForms = new Array();
                for (var i = 0; i < arFormList.length; i++) {
                    var arrForm = arrGetForms(arFormList[i], i);
                    for (var f = 0; f < arrForm.length; f++) {
                        ListForms.push(arrForm[f]);
                    }
                }
                if (arFormList.length > 0) {
                    $.modal({
                        content: '<div class=""><div id="tree"></div></div>' +
                            '<button style="float:right" type="submit" class="button anthracite-gradient" onclick="SubmitFormsForRole()">Assign Roles</button>',
                        title: 'Assign Roles',
                        width: 600,
                        scrolling: true,
                        actions: {
                            'Close': {
                                color: 'red',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttons: {
                            'Close': {
                                classes: 'anthracite-gradient displayNone',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttonsLowPadding: true
                    });
                }
                ListForms = CheckRoles(ListForms, arrAuthForm);
                var tree = simTree({
                    el: '#tree',
                    data: ListForms,
                    check: true,
                    linkParent: true,
                    onClick: function (item) {
                    },
                    onChange: function (item) {
                        debugger;
                        arrayToSubmit = new Array();
                        for (var i = 0; i < item.length; i++) {
                            if (item[i].id.indexOf("_") === -1) {
                                arrayToSubmit.push(item[i].id);
                            }

                        }
                    }
                });
            }
        },
        error: function () {
            Success("An error occured while geting form list");
        }
    });
}

function arrGetForms(Form, i) {
    var arrlist = new Array();
    try {
        var frm = new Array();
        frm = new Array();
        frm = {
            id: '',
            pid: '',
            name: Form.Name,
            checked: false,
        }
        if (Form.ChildItem.length != 0)
            frm.id = i + '_' + Form.ChildItem.length;
        else
            frm.id = Form.ID.toString();
        arrlist.push(frm);
        for (var j = 0; j < Form.ChildItem.length; j++) {
            var arrSubForms = arrGetForms(Form.ChildItem[j], frm.id + "_" + j)
            for (var f = 0; f < arrSubForms.length; f++) {
                if (arrSubForms[f].pid == "")
                    arrSubForms[f].pid = i + '_' + Form.ChildItem.length;
                arrlist.push(arrSubForms[f]);
            }
        }
    } catch (e) {

    }
    return arrlist;
}


function CheckRoles(ListForms, arFormListForRole) {
    debugger;
    if (arFormListForRole.length > 0) {
        for (j = 0; j < ListForms.length; j++) {
            for (k = 0; k < arFormListForRole.length; k++) {
                if (ListForms[j].id == arFormListForRole[k].ID) {
                    ListForms[j].checked = true;
                }
            }
        }
    }
    return ListForms;
}

function SubmitFormsForRole() {
    var arrayJson = JSON.stringify(arrayToSubmit);
    if (HiddenId != 'null') {
        $.ajax({
            type: "POST",
            url: "Handler/RoleManagementHandler.asmx/SetFormsForAdmin?sid=" + HiddenId,
            data: '{"AdminUid":"' + HiddenId + '",arr:' + arrayJson + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Role authorities has been changed successfully!");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000)
                }
                else if (result.retCode == 0) {
                    Success("Something went wrong!");
                }
            },
            error: function () {
                Success("Error occured while submitting checked forms.");
            }
        });
    }
    else {
        Success('Please select a Role!');
        $("#selRoles").focus()
    }
}


var GatewayName = new Array;
function GetPaymentGateway(Id) {
    $.ajax({
        type: "POST",
        url: "../Handler/GatewayHandler.asmx/GetPaymentGateway",
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                GatewayName = result.Name;
                GenerateGateway(Id);
                GetMapped(Id);
            }
        },
        error: function () {
            Success("An error occured while getting detail");
        }
    });
}

function GetMapped(Id) {
    $.ajax({
        type: "POST",
        url: "../Handler/GatewayHandler.asmx/GetMappedPaymentGateway",
        data: '{"Id":"' + Id + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Name = result.Name;
                if (Name.length > 0) {
                    for (var i = 0; i < Name.length; i++) {
                        $('#chk' + Name[i].GatewayId).click();
                    }
                }

            }
        },
        error: function () {
            Success("An error occured while getting detail");
        }
    });
}

function GenerateGateway(Id) {
    $("#tblGetway").empty();
    if (GatewayName.length > 0) {
        var trForms = '<tbody>';
        for (i = 0; i < GatewayName.length; i = i + 4) {
            if (i < GatewayName.length) {
                trForms += '<tr>';
                trForms += '<td><input id="chk' + GatewayName[i].Id + '" class="checkbox mid-margin-left chkGTW" type="checkbox" value="' + GatewayName[i].Id + '"/> ' + GatewayName[i].PaymentGateway + '</td>';
                if ((i + 1) < GatewayName.length)
                    trForms += '<td><input id="chk' + GatewayName[i + 1].Id + '" type="checkbox" class="checkbox mid-margin-left chkGTW" value="' + GatewayName[i + 1].Id + '"/> ' + GatewayName[i + 1].PaymentGateway + '</td>';
                if ((i + 2) < GatewayName.length)
                    trForms += '<td><input id="chk' + GatewayName[i + 2].Id + '" type="checkbox" class="checkbox mid-margin-left chkGTW" value="' + GatewayName[i + 2].Id + '"/> ' + GatewayName[i + 2].PaymentGateway + '</td>';
                if ((i + 3) < GatewayName.length)
                    trForms += '<td><input id="chk' + GatewayName[i + 3].Id + '" type="checkbox" class="checkbox mid-margin-left chkGTW" value="' + GatewayName[i + 3].Id + '"/> ' + GatewayName[i + 3].PaymentGateway + '</td>';
                trForms += '</tr>';
            }
        }
        trForms += '</tbody>';
        $.modal({
            content: '<table class="table table-striped table-bordered" id="tblGetway" cellspacing="0" cellpadding="0" border="0">' + trForms + '' +
                '</table><br>' +
                '<button style="float:right" type="submit" class="button anthracite-gradient" onclick="SaveGateway(\'' + Id + '\')">Save</button>',

            title: 'Assign Payment Gateway',
            width: 600,
            scrolling: true,
            actions: {
                'Close': {
                    color: 'red',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttons: {
                'Close': {
                    classes: 'anthracite-gradient displayNone',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttonsLowPadding: true
        });
    }
}
var ArrGatewayName = new Array();
function SaveGateway(Id) {
    var GatewayName = $(".chkGTW");
    for (var i = 0; i < GatewayName.length; i++) {
        if (GatewayName[i].classList.length == 5) {
            GatewayId = GatewayName[i].childNodes[1].value;
            ArrGatewayName.push({
                GatewayId: GatewayId,
                ParentId: Id,
            });
        }
    }

    if (ArrGatewayName == 0) {
        Success('Please Select Payment Gateway.');
        return false;
    }
    var data = {
        ArrGateway: ArrGatewayName,
    }
    $.ajax({
        type: "POST",
        url: "../Handler/GatewayHandler.asmx/MapPaymentGateway",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0 && result.retCode == 0) {
                Success("Your session has been expired!")
                setTimeout(function () {
                    window.location.href = "../Default.aspx";
                }, 2000);

            }
            else if (result.retCode == 1) {
                Success("saved successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);

            }
        },
        error: function () {
            Success("An error occured while saving");
        }
    });
}


