﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using CutAdmin.dbml;
using CutAdmin.HotelAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace CutAdmin.HotelAdmin.Handler
{
    /// <summary>
    /// Summary description for RoleManagementHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class RoleManagementHandler : System.Web.Services.WebService
    {
        helperDataContext DB = new helperDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        #region AdminRoles
        [WebMethod(EnableSession = true)]
        public string GetRoleList()
        {
            string jsonString = "";
            // DBHelper.DBReturnCode retCode = RoleManager.GetRoleList(out dtResult);

            var RoleList = (from obj in DB.tblRoles select obj).ToList();

            if (RoleList.Count > 0 && RoleList != null)
            {

                jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = RoleList });

            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetFormList()
        {
            string jsonString = "";
            try
            {
                
                    if (HttpContext.Current.Session["LoginUser"] == null)
                        throw new Exception("Session Expired , Please login and try again.");
                  CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                    if (objGlobalDefault.UserType == "Admin" || objGlobalDefault.UserType == "Franchisee")
                    {
                        Int64 UserID = Convert.ToInt64(this.Context.Request.QueryString["sid"]);
                        helperDataContext db = new helperDataContext();
                        var arrForm = (List<CutAdmin.Models.MenuItem>)HttpContext.Current.Session["Menu"];
                        var arrAuthForm = (from obj in db.tbl_Forms
                                           join objAuth in db.tblStaffRoleManagers
                                               on obj.ID equals objAuth.nFormId
                                           where objAuth.nUid == UserID
                                           select new
                                           {
                                               obj.ID,
                                               obj.Menu1,

                                           }).ToList();
                        jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = arrForm, arrAuthForm = arrAuthForm });
                    }
                    else
                        jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 2 });
                 
                
                
            }
            catch
            {

            } 
           
            return jsonString;
        }
        [WebMethod(EnableSession = true)]
        public string GetFranchiseeForm()
        {
            string jsonString = "";
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired , Please login and try again.");
                CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                if (objGlobalDefault.UserType == "Admin" || objGlobalDefault.UserType == "Franchisee")
                {
                    Int64 UserID = Convert.ToInt64(this.Context.Request.QueryString["sid"]);
                    helperDataContext db = new helperDataContext();
                    var arrForm = (List<CutAdmin.Models.MenuItem>)HttpContext.Current.Session["Menu"];
                    var arrAuthForm = (from obj in db.tbl_Forms
                                       join objAuth in db.tbl_AdminRoleManagers
                                           on obj.ID equals objAuth.nFormId
                                       where objAuth.nUid == UserID
                                       select new
                                       {
                                           obj.ID,
                                           obj.Menu1,

                                       }).ToList();
                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = arrForm, arrAuthForm = arrAuthForm });
                }
                else
                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 2 });

            }
            catch (Exception)
            {

                throw;
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetAdminFormList()
        {
            string jsonString = "";
            try
            {

                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired , Please login and try again.");
                CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
                if (objGlobalDefault.UserType == "SuperAdmin")
                {
                    Int64 UserID = Convert.ToInt64(this.Context.Request.QueryString["sid"]);
                    helperDataContext db = new helperDataContext();
                    var arrForm = CutAdmin.Models.Forms.GenrateMenu();
                    var arrAuthForm = (from obj in db.tbl_Forms
                                       join objAuth in db.tbl_AdminRoleManagers
                                           on obj.ID equals objAuth.nFormId
                                       where objAuth.nUid == UserID
                                       select new
                                       {
                                           obj.ID,
                                           obj.Menu1,

                                       }).ToList();
                   jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = arrForm, arrAuthForm = arrAuthForm });
                   // jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, arrAuthForm = arrAuthForm });
                }
                else
                    jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 2 });



            }
            catch
            {

            }

            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetFormsForRole(string sRoleName)
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            Int64 nId = (from obj in DB.tblRoles where obj.sRoleName == sRoleName select obj.nId).FirstOrDefault();

            var FormsForRoleList = (from obj in DB.tblForms
                                    join Role in DB.tblRoleManagers on obj.nId equals Role.nFormId
                                    where Role.nRoleId == nId
                                    select new
                                    {
                                        obj.sFormName,
                                    }).ToList();

            if (FormsForRoleList.Count > 0 && FormsForRoleList != null)
            {
                jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = FormsForRoleList });
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string SetFormsForRole(string sSelectedRole, params string[] arr)
        {
            try
            {
                Int64 nId = (from obj in DB.tblRoles where obj.sRoleName == sSelectedRole select obj.nId).FirstOrDefault();

                var Role = (from obj in DB.tblRoleManagers where obj.nRoleId == nId select obj).ToList();

                DB.tblRoleManagers.DeleteAllOnSubmit(Role);
                DB.SubmitChanges();

                List<tblRoleManager> ListRoleManager = new List<tblRoleManager>();
                for (int i = 0; i < arr.Length; i++)
                {
                    ListRoleManager.Add(new tblRoleManager
                    {
                        nRoleId = (DB.tblRoles.Where(d => d.sRoleName == sSelectedRole).FirstOrDefault().nId),
                        nFormId = (DB.tblForms.Where(d => d.sFormName == arr[i]).FirstOrDefault().nId),
                    });
                }

                DB.tblRoleManagers.InsertAllOnSubmit(ListRoleManager);
                DB.SubmitChanges();

                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }
        }
        #endregion


        #region Admin StaffRoles
        [WebMethod(EnableSession = true)]
        public string GetFormsForAdminStaff(Int64 StaffUid)
        {
            string jsonString = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            Int64 sid = (from obj in DB.tbl_StaffLogins where obj.sid == StaffUid select obj.sid).FirstOrDefault();

            var FormsForAdminStaffList = (from obj in DB.tblForms
                                          join Role in DB.tblStaffRoleManagers on obj.nId equals Role.nFormId
                                          where Role.nUid == sid
                                          select new
                                          {
                                              obj.sFormName,
                                          }).ToList();

            if (FormsForAdminStaffList.Count > 0 && FormsForAdminStaffList != null)
            {
                jsonString = jsSerializer.Serialize(new { Session = 1, retCode = 1, tblFormsForRole = FormsForAdminStaffList });
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;


        }

        [WebMethod(EnableSession = true)]
        public string SetFormsForAdminStaff(Int64 StaffUid, params string[] arr)
        {
            try
            {
                 if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired , Please login and try again.");
                  CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];

                    if (objGlobalDefault.UserType == "Admin" || objGlobalDefault.UserType == "Franchisee")
                    {
                        Int64 sid = (from obj in DB.tbl_StaffLogins where obj.sid == StaffUid select obj.sid).FirstOrDefault();

                        var Role = (from obj in DB.tblStaffRoleManagers where obj.nUid == sid select obj).ToList();

                        DB.tblStaffRoleManagers.DeleteAllOnSubmit(Role);
                        DB.SubmitChanges();

                        List<tblStaffRoleManager> ListblStaffRoleManager = new List<tblStaffRoleManager>();
                        for (int i = 0; i < arr.Length; i++)
                        {
                            ListblStaffRoleManager.Add(new tblStaffRoleManager
                            {
                                nUid = StaffUid,
                                nFormId = Convert.ToInt64(arr[i]),
                            });
                        }

                        DB.tblStaffRoleManagers.InsertAllOnSubmit(ListblStaffRoleManager);
                        DB.SubmitChanges();
                    }
                         return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }
        }


        [WebMethod(EnableSession = true)]
        public string SetFormsForAdmin(Int64 AdminUid, params string[] arr)
        {
            try
            {
                if (HttpContext.Current.Session["LoginUser"] == null)
                    throw new Exception("Session Expired , Please login and try again.");
                CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];

                if (objGlobalDefault.UserType == "SuperAdmin" || objGlobalDefault.UserType == "Admin")
                {
                    //Int64 sid = (from obj in DB.tbl_StaffLogins where obj.sid == StaffUid select obj.sid).FirstOrDefault();

                    var Role = (from obj in DB.tbl_AdminRoleManagers where obj.nUid == AdminUid select obj).ToList();

                    DB.tbl_AdminRoleManagers.DeleteAllOnSubmit(Role);
                    DB.SubmitChanges();

                    List<tbl_AdminRoleManager> ListblAdminRoleManager = new List<tbl_AdminRoleManager>();
                    for (int i = 0; i < arr.Length; i++)
                    {
                        ListblAdminRoleManager.Add(new tbl_AdminRoleManager
                        {
                            nUid = AdminUid,
                            nFormId = Convert.ToInt64(arr[i]),
                        });
                    }

                    DB.tbl_AdminRoleManagers.InsertAllOnSubmit(ListblAdminRoleManager);
                    DB.SubmitChanges();
                }
                return jsSerializer.Serialize(new { retCode = 1 });
            }
            catch
            {
                return jsSerializer.Serialize(new { retCode = 0 });
            }
        }

        #endregion
        //[WebMethod]
        //public string GetAgentFormList()
        //{
        //    DataTable dtResult = new DataTable();
        //    string jsonString = "";
        //    DBHelper.DBReturnCode retCode = RoleManager.GetAgentFormList(out dtResult);
        //    if (DBHelper.DBReturnCode.SUCCESS == retCode)
        //    {
        //        jsonString = "";
        //        foreach (DataRow dr in dtResult.Rows)
        //        {
        //            jsonString += "{";
        //            foreach (DataColumn dc in dtResult.Columns)
        //            {
        //                jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
        //            }
        //            jsonString = jsonString.Trim(',') + "},";
        //        }
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tblAgentForms\":[" + jsonString.Trim(',') + "]}";
        //        dtResult.Dispose();
        //    }
        //    else
        //    {
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return jsonString;
        //}
        //[WebMethod]
        //public string GetFormList()
        //{
        //    DataTable dtResult = new DataTable();
        //    string jsonString = "";
        //    DBHelper.DBReturnCode retCode = RoleManager.GetFormList(out dtResult);
        //    if (DBHelper.DBReturnCode.SUCCESS == retCode)
        //    {
        //        jsonString = "";
        //        foreach (DataRow dr in dtResult.Rows)
        //        {
        //            jsonString += "{";
        //            foreach (DataColumn dc in dtResult.Columns)
        //            {
        //                jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
        //            }
        //            jsonString = jsonString.Trim(',') + "},";
        //        }
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"tblForms\":[" + jsonString.Trim(',') + "]}";
        //        dtResult.Dispose();
        //    }
        //    else
        //    {
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return jsonString;
        //}
    }
}
