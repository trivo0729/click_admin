﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelAdmin/Master.Master" AutoEventWireup="true" CodeBehind="PaymentGateway.aspx.cs" Inherits="CutAdmin.HotelAdmin.PaymentGateway" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/PaymentGateway.js?v=1.0"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Payment Gateway Details</h1>
        </hgroup>
        <div class="with-padding">
            <hr>
            <div class="columns">
                <div class="two-columns four-columns-mobile">
                    <label>Payment Gateway<span class="red">*</span>:</label>
                </div>
                <div class="four-columns eight-columns-mobile">
                    <div class="input full-width">
                        <input class="input-unstyled full-width" placeholder="Payment Gateway Name" type="text" id="txtGatewayName">
                    </div>
                </div>
                <div class="two-columns four-columns-mobile">
                    <input type="button" class="button anthracite-gradient" id="btnGatewayDetails" onclick="AddGatewayDetails()" value="Add" title="Submit Details">
                    <a id="btn_Cancel" class="button anthracite-gradient" onclick="Cancel()" style="cursor: pointer; display: none">Cancel</a>
                </div>
            </div>
        </div>
        <div class="with-padding">
            <div class="respTable">
                <table class="table responsive-table" id="tbl_GatewayDetails">
                    <thead>
                        <tr>
                            <th scope="col" class="align-center">Sr.No.</th>
                            <th scope="col" class="align-center">Payment Gateway Name</th>
                            <th scope="col" class="align-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</asp:Content>
