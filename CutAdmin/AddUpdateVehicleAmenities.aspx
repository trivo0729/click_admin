﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddUpdateVehicleAmenities.aspx.cs" Inherits="CutAdmin.AddUpdateVehicleAmenities" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/VehicleAmenities.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>Vehicle Amenity</h1>

        </hgroup>

        <div class="with-padding">
            <div class="columns">
                <div class="twelve-columns six-columns-mobile" style="text-align:right">
                    <button type="button"  class="button compact anthracite-gradient" onclick="NewVehicleAmenity()">New Vehicle Amenity</button></div>
                <%--<div class="six-columns  six-columns-mobile text-alignright selCurrency">
                    <div class="full-width button-height">
                        <select id="selCurrency" class="select" onchange="GetPackages()">
                            <option selected="selected" value="USD">USD</option>
                            <option value="INR">INR</option>
                            <option value="AED">AED</option>
                        </select>
                    </div>
                </div>--%>
            </div>

            <div class="respTable">
                <table class="table responsive-table" id="tbl_VehicleAmenities">

                    <thead>
                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">Vehicle Amenity Name</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>


                    <tbody>
                       

                    </tbody>

                </table>

            </div>
        </div>

    </section>
    <script>

     
        // New Pakages modal
        function NewVehicleAmenity() {
            $.modal({
                content: '<div class="modal-body" id="VehicleAmenityModal">' +
				'<div class="columns">' +
'<div class="Twelve-columns twelve-columns-mobile"><label>Vehicle Amenity Name<span class="red">*</span>:</label><div class="input full-width">' +
'<input name="prompt-value" id="txt_Name" placeholder="Vehicle Amenity Name" value="" class="input-unstyled full-width"  type="text">' +
'</div></div>' +
'</div>' +
'<p class="text-alignright"><input type="button" value="Add" onclick="AddVehicleAmenity();" title="Submit Details" class="button anthracite-gradient"/></p>' +
'</div>',

                title: 'Add Vehicle Amenity',
                width: 500,
                scrolling: true,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'huge anthracite-gradient displayNone',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: true
            });
        };


  
    </script>
</asp:Content>
