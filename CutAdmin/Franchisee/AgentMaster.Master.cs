﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CutAdmin.Franchisee
{
    public partial class AgentMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Menucontainer.InnerHtml = CutAdmin.Models.Forms.GetFormByMenu();
            if (HttpContext.Current.Session["LoginUser"] != null)
            {
                CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Page.Title = objGlobalDefault.AgencyName;
            }
        }
    }
}