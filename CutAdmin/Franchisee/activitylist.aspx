﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Franchisee/AgentMaster.Master" AutoEventWireup="true" CodeBehind="activitylist.aspx.cs" Inherits="CutAdmin.Franchisee.activitylist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <script  src="../Scripts/ActivityList.js?v=1.4"></script>
    <script src="../Scripts/ActivityFiles.js?v=1.0"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#datepicker_From").datepicker({
                dateFormat: "dd-mm-yy",
                //minDate: "dateToday",
                autoclose: true,
            });
            $("#datepicker_To").datepicker({
                // minDate: $("#datepicker_To").text(),
                dateFormat: "dd-mm-yy",
                autoclose: true,
            });
        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Activities Details</h1>
            <hr />
            <h2><a href="AddActivity.aspx"  class="addnew children-tooltip" title="Add Activity"><i class="fa fa-plus-round"></i></a></h2>
            </hgroup>

        <div class="with-padding">
            <table class="table responsive-table" id="tbl_Actlist" width="100%">

                <thead>
                    <tr>

                        <th align="center" scope="col">S.N </th>
                        <th align="center" scope="col">Name </th>
                        <th align="center" scope="col">City,Country </th>
                        <th align="center" scope="col">Tour Type</th>
                        <th align="center" scope="col">Activity Type</th>
                        <th align="center" scope="col">Inventory </th>
                        <th align="center" scope="col">Edit | Delete</th>
                        <th align="center" scope="col">Rates</th>

                    </tr>
                </thead>
                <tbody>

                </tbody>

            </table>
        </div>
    </section>
    <!-- End main content --></asp:Content>
