﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Franchisee/AgentMaster.Master" AutoEventWireup="true" CodeBehind="AddActivity.aspx.cs" Inherits="CutAdmin.Franchisee.AddActivity" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     <script src="../js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/AddActivity.js?V=2.4"></script>
    <%--        <script type="text/javascript">
            $(document).ready(function () {
                GetCountry();
            });
    </script>--%>
    <style>
        .overlay {
            display: none;
            margin-top: 25%; 
            margin-left: 50%;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            opacity: 0.5;
        }
    </style>

    <script src="../Scripts/AddLocation.js?v=1.3"></script>

    <script type="../text/javascript" src="Scripts/image.js?V=1.4"></script>
    

    <script type="text/javascript" src="Scripts/ActMaster.js"></script>
    <%--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyBnIPCXTY_ul30N9GMmcSmJPLjPEYzGI7c"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>

    <!-- Additional styles -->
    <link rel="stylesheet" href="../css/styles/form.css?v=1">
    <link rel="stylesheet" href="../css/styles/switches.css?v=1">
    <link rel="stylesheet" href="../css/styles/table.css?v=1">
    <link rel="stylesheet" href="../js/libs/glDatePicker/developr.fixed.css?v=1" />

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">


    <%--<script>
        google.maps.event.addDomListener(window, 'load', function () {
            debugger;

            new google.maps.places.SearchBox(document.getElementById('txt_Location'));
            directionsDisplay = new google.maps.DirectionsRenderer({ 'draggable': true });

            var places = new google.maps.places.Autocomplete(document.getElementById('txt_Location'));
            google.maps.event.addListener(places, 'place_changed', function () {

                debugger;
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                $('#txt_longitude').val(longitude)
                $('#txt_latitude').val(latitude)

            });
        });
    </script>--%>
    <style type="text/css">
        div.Inclusion {
            height: 110px;
            overflow-y: auto;
            margin-top: 10px;
            background-color: #e6e6e6;
        }

        label.lblAtraction {
            cursor: pointer;
        }

        ::-webkit-scrollbar {
            width: 2px; /* for vertical scrollbars */
            height: 8px; /* for horizontal scrollbars */
        }

        ::-webkit-scrollbar-track {
            background: rgb(255, 255, 255);
        }

        ::-webkit-scrollbar-thumb {
            /*background: #ff9900;*/
            position: relative;
            top: 17px;
            float: right;
            width: 5px;
            height: 32px;
            background-color: rgb(204, 204, 204);
            border: 0px solid rgb(255, 255, 255);
            background-clip: padding-box;
            border-radius: 0px;
        }

        @media only screen and (max-width: 767px) {

            span {
                font-size: .8em;
            }
        }

        tr.spaceUnder > td {
            /*padding-bottom: 1em;*/
            padding-right: 1em;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Main content -->
    <section role="main" id="main">
        <input type="button" style="float: right; display: none" class="button glossy" id="Button1" value="Save file" />

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <div class="full-page-wizard">
            <form class="block wizard same-height columns" style="max-width: 100%">
                
                <h3 class="block-title">Activities</h3>

                <fieldset class="wizard-fieldset fields-list" id="Actdetail">
                    <legend class="legend">Details</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Activity Details</b></label>
                        <div class="columns" style="min-height: 300px; margin-top: 20px">

                            <%--<div class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h3 class="thin underline green">Activity Detail's</h3>
                            </div>--%>

                            <div class="four-columns">
                                <h6>Activity Name</h6>
                                <input type="text" name="ActivityName" id="txt_Activity" class="input full-width" value="">
                            </div>
                            <div class="four-columns">
                                <h6>Subtitle :</h6>
                                <input type="text" id="txt_subtitle" class="input full-width" value="">
                            </div>
                           <div class="new-row four-columns" id="div_country">
                                <h6>Country</h6>
                                <select id="ddlcountry" name="ddlcountry" class="full-width select validate[required] OfferType" onchange="GetCity(this.value)">
                                </select>
                            </div>

                            <span id="Loader1" class="loader huge overlay"></span>

                            <div class="three-columns" id="CityDiv">
                                <h6>City :</h6>
                                <select id="ddlcity" class="select multiple-as-single easy-multiple-selection allow-empty check-list PPCity full-width" multiple onchange="fnCities()">
                                </select>
                            </div>
                            <div class="three-columns">
                                <h6>Location : </h6>

                                <input type="text" onkeypress="<%--AddLocation()--%>" name="Text[]" id="txt_location" list="Select_Location" class="input full-width" />
                                <datalist id="Select_Location"></datalist>
                            </div>

                            <div id="AddLocation" class="two-columns" title="Add Location">
                                <h6>Add Location</h6>
                                <i onclick="AddPopup()" aria-hidden="true">
                                    <label for="pseudo-input-2" class="button blue-gradient"><span class="icon-plus"></span>Add</label>
                                </i>
                            </div>
                            <div class="new-row twelve-columns">
                                <h6>Description :</h6>
                                <textarea id="txt_AreaRemarks" class="input full-width autoexpanding" rows="4"></textarea>
                            </div>


                        </div>
                    </div>

                </fieldset>

                <fieldset class="wizard-fieldset fields-list" id="tab_ChildPolicy">
                    <legend class="legend">Child Policy</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Child Policy</b></label>
                        <div class="columns" style="min-height: 300px; margin-top: 20px">

                            <%-- <div class="new-row twelve-columns" style="margin-bottom: -2px">
                                <h3 class="thin underline green">Child Policy</h3>
                            </div>--%>

                            <div class="four-columns">
                                <h6>Children Allowed </h6>
                                <%-- <div id="switch-wrapper" class="margin-bottom">
                                <p class="button-height">
                                    <input type="checkbox" name="switch-custom-1" id="switch-custom-1" class="switch blue-active mid-margin-right" value="1" checked data-text-on="YES" data-text-off="NO">
                                    <label for="switch-custom-1">Custom texts &amp; active color</label>
                                </p>
                                    </div>--%>
                                <%--<select id="ddlChildrenAllowed" name="ddlChildrenAllowed" class="full-width select validate[required]" onchange="ChildPolicy()">--%>
                                <select id="ddlChildrenAllowed" name="ddlChildrenAllowed" style="height: 30px; width: 50%" class="input full-width" onchange="ChildPolicy()">
                                    <option selected="" value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>

                            <div class="new-row three-columns" id="ChildAgeFrom" style="">
                                <h6>Child Age From :</h6>
                                <%--<select id="ddlChildAgeFrom" name="ddlChildAgeFrom" class="full-width select validate[required]" onchange="ChangeAge()">--%>
                                <select id="ddlChildAgeFrom" name="ddlChildAgeFrom" style="height: 30px" class="input full-width" onchange="ChangeAge()">
                                    <option value="18">18 Years</option>
                                    <option value="17">17 Years</option>
                                    <option value="16">16 Years</option>
                                    <option value="15">15 Years</option>
                                    <option value="14">14 Years</option>
                                    <option value="13">13 Years</option>
                                    <option value="12">12 Years</option>
                                    <option value="11">11 Years</option>
                                    <option value="10">10 Years</option>
                                    <option value="9">9 Years</option>
                                    <option value="8">8 Years</option>
                                    <option value="7">7 Years</option>
                                    <option value="6">6 Years</option>
                                    <option value="5">5 Years</option>
                                    <option value="4">4 Years</option>
                                    <option value="3">3 Years</option>
                                </select>
                            </div>

                            <div class="three-columns" id="ChildAgeUpTo" style="">
                                <h6>Child Age Up To :</h6>
                                <%--<select id="ddlChildAgeUpTo" name="ddlChildAgeUpTo" class="full-width select validate[required]" onchange="AgeUpTo()">--%>
                                <select id="ddlChildAgeUpTo" name="ddlChildAgeUpTo" style="height: 30px" class="input full-width" onchange="AgeUpTo()">
                                    <%--<option selected="" value="18">18 Years</option>
                                <option value="17">17 Years</option>
                                <option value="16">16 Years</option>
                                <option value="15">15 Years</option>
                                <option value="14">14 Years</option>
                                <option value="13">13 Years</option>
                                <option value="12">12 Years</option>
                                <option value="11">11 Years</option>
                                <option value="10">10 Years</option>
                                <option value="9">9 Years</option>
                                <option value="8">8 Years</option>
                                <option value="7">7 Years</option>
                                <option value="6">6 Years</option>
                                <option value="5">5 Years</option>
                                <option value="4">4 Years</option>
                                <option value="3">3 Years</option>--%>
                                </select>
                            </div>

                            <div class="three-columns" id="SmallChildAgeUpTo" style="">
                                <h6>Small Child Age Up To  :</h6>
                                <%--<select id="ddlSmallChildAgeUpTo" name="ddlSmallChildAgeUpTo" class="full-width select validate[required]">--%>
                                <select id="ddlSmallChildAgeUpTo" name="ddlSmallChildAgeUpTo" style="height: 30px" class="input full-width">
                                    <%--<option selected="" value="18">18 Years</option>
                                <option value="17">17 Years</option>
                                <option value="16">16 Years</option>
                                <option value="15">15 Years</option>
                                <option value="14">14 Years</option>
                                <option value="13">13 Years</option>
                                <option value="12">12 Years</option>
                                <option value="11">11 Years</option>
                                <option value="10">10 Years</option>
                                <option value="9">9 Years</option>
                                <option value="8">8 Years</option>
                                <option value="7">7 Years</option>
                                <option value="6">6 Years</option>
                                <option value="5">5 Years</option>
                                <option value="4">4 Years</option>
                                <option value="3">3 Years</option>--%>
                                </select>
                            </div>
                            <div class="new-row three-columns" id="ChildHeight" style="">
                                <h6>Min Hieght Applicable</h6>
                                <%--<select id="ddlChildrenHeight" name="ddlChildrenHeight" class="full-width select validate[required]" onchange="HeightPolicy()">--%>
                                <select id="ddlChildrenHeight" name="ddlChildrenHeight" style="height: 30px" class="input full-width" onchange="HeightPolicy()">
                                    <option value="Yes">Yes</option>
                                    <option selected="" value="No">No</option>
                                </select>
                            </div>
                            <div class="three-columns" id="ChildMinHeight" style="display: none">
                                <h6>Child Min Height :</h6>
                                <input type="number" id="txt_ChildMinHight" class="input full-width" value="" placeholder="Height in cm">
                                <%--<select id="ddlKid2EndRange" name="ddlKid2EndRange" class="full-width select validate[required]">
                                    <option selected="" value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                </select>--%>
                            </div>

                            <%--<div class="three-columns" id="MaxChild" style="">
                                <h6>Max Child Allowed</h6>
                                <select id="ddlMaxChildren" name="ddlMaxChildren" class="full-width select validate[required]" onchange="MaxChildPolicy()">
                                    <option value="Yes">Yes</option>
                                    <option selected="" value="No">No</option>
                                </select>
                            </div>
                            <div class="three-columns" id="MaxChildNo" style="display: none">
                                <h6>Max Child:</h6>
                                <input type="text" id="txt_MaxChild" class="input full-width" value="">
                               
                            </div>--%>
                            <div class="three-columns" id="Infant" style="">
                                <h6>Infant :</h6>
                                <%-- <select id="ddlInfant" name="ddlInfant" class="full-width select validate[required]" onchange="Infant()">--%>
                                <select id="ddlInfant" name="ddlInfant" style="height: 30px" class="input full-width" onchange="Infant()">
                                    <option value="Allowed">Allowed</option>
                                    <option selected="" value="Not Allowed">Not Allowed</option>
                                </select>
                            </div>

                            <div class="three-columns" id="MaxNoofInfant" style="display: none">
                                <h6>Max No of Infant  :</h6>
                                <input type="number" id="txt_MaxNoofInfant" class="input full-width" value="0" />
                            </div>
                        </div>


                    </div>
                </fieldset>

                <%--<h3 class="thin underline green margin-left">Location Detail's</h3>--%>
                <fieldset class="wizard-fieldset fields-list" id="tab_Types">
                    <legend class="legend">Types</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Attraction,Tour Type Detail & Activity Type</b></label>
                        <div class="columns" style="min-height: 300px; margin-top: 20px">

                            <%-- <div class="new-row twelve-columns" style="margin-bottom: -2px">
                            <h3 class="thin underline green">Attraction , Tour Type Detail & Activity Type</h3>
                        </div>--%>

                            <div class="four-columns">
                                <h6>Activity Type :</h6>


                                <input type="checkbox" name="checkbox-2" id="chk_All" value="All" class="chk_ActivityType" style="display:none" tabindex="-1" onchange="ActivityTypeAllCheck()">
                                <label class="" for="chk1" style="display:none">All</label>
                                <input type="checkbox" name="checkbox-2" id="Chk_TktOnly" value="TKT" class="chk_ActivityType" tabindex="-1" onchange="ActivityTypeCheck()">
                                <label class="" for="chk1">Ticket Only</label>
                                <input type="checkbox" name="checkbox-2" id="Chk_Sic" value="SIC" class="chk_ActivityType" tabindex="-1" onchange="ActivityTypeCheck()">
                                <label class="" for="chk1">SIC</label>
                                <input type="checkbox" name="checkbox-2" id="Chk_Pvt" value="PVT" class="chk_ActivityType" style="display:none" tabindex="-1" onchange="ActivityTypeCheck()">
                                <label class="" for="chk1" style="display:none">PVT</label>


                                <%--<select id="ddlActivityType" name="ddlActivityType" class="full-width select validate[required]">
                                <option selected="" value="All">All</option>
                                <option value="Ticket Only">Ticket Only</option>
                                <option value="SIC">SIC</option>
                                <option value="PVT">PVT</option>
                            </select>--%>
                            </div>

                            <div class="four-columns">
                                <h6>Priority Type :</h6>
                                <select id="ddlPriorityType" name="ddlPriorityType" style="height: 30px" class="input full-width">
                                    <%--<option selected="" value="All">All</option>
                                    <option value="Ticket Only">Ticket Only</option>
                                    <option value="SIC">SIC</option>
                                    <option value="PVT">PVT</option>--%>
                                </select>
                            </div>

                            

                            <div class="new-row twelve-columns" style="margin-top: 20px">
                                <h6>Tour Type :</h6>

                                <%-- <fieldset class="fieldset">--%>
                                <%--<legend class="legend">Tour Type</legend>--%>
                                <%-- <select id="ddlTourType" class="full-width glossy select multiple-as-single easy-multiple-selection allow-empty blue-gradient check-list" multiple>                                   
                                </select>--%>
                                <table class="input full-width" id="tblForms">
                                    <thead>
                                        <tr>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>


                            </div>

                            <div class="new-row four-columns">
                                <h6>Attraction  :</h6>

                                <div id="idAttraction" class="Inclusion glossy" style="height: 60px"></div>

                                <%--<span class="icon-plus-round" title="Add Attraction" onclick="AddAttraction()"></span>--%>
                            </div>
                            <div class="four-columns">


                                <textarea type="text" name="Text[]" id="txt_Attraction" class="input full-width autoexpanding" placeholder="Add Attraction" style="margin-right: -22px; height: 60px; width: 200px; overflow: hidden; resize: none; margin-top: 44px"></textarea>


                            </div>
                            <div class="four-columns">
                                <button type="button" title="Add Attraction" style="margin-top: 80px" class="button compact" onclick="AddAttraction()">Add Attraction</button>
                            </div>
                            <%--    <div class="four-columns">
                             <h6>Status:</h6>
                            <select id="ddlStatus" name="ddlStatus" class="full-width select validate[required]">
                                <option selected="" value="">Select</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                  
                            </select>

                            </div>
                         <div class="four-columns">

                             <h6>Agency Name</h6>
                                <input type="text" id="txt_Agency" class="input full-width" value="">
                             </div>--%>
                        </div>
                    </div>
                </fieldset>


                <fieldset class="wizard-fieldset fields-list" id="tab_Timing">
                    <legend class="legend">Timing</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Activity Timing & Operations</b></label>
                        <div class="columns" style="min-height: 300px; margin-top: 20px">
                            <%--<div class="new-row twelve-columns" style="margin-bottom: -2px">
                            <h3 class="thin underline green">Activity Timing & Operations</h3>
                        </div>--%>

                            <div class="four-columns">
                                <h6>Full Year  :</h6>
                                <%--<select id="ddlFullYear" name="ddlFullYear" class="full-width select validate[required]" onchange="ChangeYear()">--%>
                                <select id="ddlFullYear" name="ddlFullYear" style="height: 30px" class="input full-width" onchange="ChangeYear()">
                                    <option selected="" value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="line2"></div>
                            <div class="four-columns" style="" id="Slotss">
                                <h6>Slots :</h6>
                                <%-- <select id="ddlSlot" name="ddlSlot" class="full-width select validate[required]" onchange="Slots()">--%>
                                <select id="ddlSlot" name="ddlSlot" style="height: 30px" class="input full-width" onchange="Slots()">
                                    <option selected="" value="No">No</option>
                                    <option value="Yes">Yes</option>
                                </select>
                            </div>
                            <div class="line2"></div>
                            <div id="DatesUI" style="width: 100%">
                            </div>

                            <%--      

                            <div class="new-row three-columns" id="SlotName" >
                                <h6>Slot Name :</h6>
                                <input type="text" id="txt_SlotName" class="input full-width" /> 
                            </div>

                            <div class="three-columns" id="TourStart" >
                                <h6>Tour Start :</h6>
                                <input type="time" id="txt_TourStart" class="input full-width" /> 
                            </div>

                            <div class="three-columns" id="TourEnd" >
                                <h6>Tour End :</h6>
                                <input type="time" id="txt_TourEnd" class="input full-width" /> 
                            </div>

                             
                            <div class="new-row three-columns" id="Pick-UpFrom" >
                                <h6>Pick-Up From :</h6>
                                <input type="text" id="txt_PickUpFrom" class="input full-width" /> 
                            </div>

                            <div class="three-columns" id="Pick-UpTime" >
                                <h6>Pick-Up Time :</h6>
                                <input type="time" id="txt_PickUpTime" class="input full-width" /> 
                            </div>

                            <div class="three-columns" id="DropOffAt" >
                                <h6>Drop-Off At  :</h6>
                                <input type="text" id="txt_DropOffAt" class="input full-width" /> 
                            </div>

                            <div class="three-columns" id="DropOffTime" >
                                <h6>Drop-Off Time :</h6>
                                <input type="time" id="txt_DropOffTime" class="input full-width" /> 
                            </div>

                            <div class="new-row three-columns" id="PriorityType" >
                                <h6>Priority Type  :</h6>
                                <input type="text" id="txt_PriorityType" class="input full-width" /> 
                            </div>--%>



                            <%-- <div class="new-row four-columns" id="OperationDays" style="display: none">
                                <h6>Operation Days :</h6>
                                <select id="ddlOperationDays" name="ddlOperationDays" class="full-width select validate[required]">
                                    <option selected="" value="Daily">Daily</option>
                                    <option value="Monday">Monday</option>
                                    <option value="Tuesday">Tuesday</option>
                                    <option value="Wednesday">Wednesday</option>
                                    <option value="Thursday">Thursday</option>
                                    <option value="Friday">Friday</option>
                                    <option value="Saturday">Saturday</option>
                                </select>
                            </div>--%>


                            <%--<div class="new-row seven-columns" id="OperationDays" style="">
                                <h6>Operation Days :</h6>

                                <input type="checkbox" name="checkbox-2" id="chk_Daily" value="Daily" class="chk_OperationDays" tabindex="-1" onchange="DaysAllChecked()">
                                <label class="" for="chk1">Daily</label>
                                <input type="checkbox" name="checkbox-2" id="Chk_Mon" value="Mon" class="chk_OperationDays" tabindex="-1" onchange="DaysChecked()">
                                <label class="" for="chk1">Mon</label>
                                <input type="checkbox" name="checkbox-2" id="Chk_Tue" value="Tue" class="chk_OperationDays" tabindex="-1" onchange="DaysChecked()">
                                <label class="" for="chk1">Tue</label>
                                <input type="checkbox" name="checkbox-2" id="Chk_Wed" value="wed" class="chk_OperationDays" tabindex="-1" onchange="DaysChecked()">
                                <label class="" for="chk1">Wed</label>
                                <input type="checkbox" name="checkbox-2" id="Chk_Thu" value="Thu" class="chk_OperationDays" tabindex="-1" onchange="DaysChecked()">
                                <label class="" for="chk1">Thu</label>
                                <input type="checkbox" name="checkbox-2" id="Chk_Fri" value="Fri" class="chk_OperationDays" tabindex="-1" onchange="DaysChecked()">
                                <label class="" for="chk1">Fri</label>
                                <input type="checkbox" name="checkbox-2" id="Chk_Sat" value="Sat" class="chk_OperationDays" tabindex="-1" onchange="DaysChecked()">
                                <label class="" for="chk1">Sat</label>
                                <input type="checkbox" name="checkbox-2" id="Chk_Sun" value="Sun" class="chk_OperationDays" tabindex="-1" onchange="DaysChecked()">
                                <label class="" for="chk1">Sun</label>
                            </div>--%>

                            <div class="new-row seven-columns" id="OperationDaysTKT" style="display:none">
                                <h6>Operation Days TKT:</h6>

                                <input type="checkbox" name="checkbox-TKT" id="chk_DailyTKT" value="Daily" class="chk_OperationDaysTKT" tabindex="-1" onchange="DaysAllCheckedTKT()">
                                <label class="" for="chk1">Daily</label>
                                <input type="checkbox" name="checkbox-TKT" id="Chk_MonTKT" value="Mon" class="chk_OperationDaysTKT" tabindex="-1" onchange="DaysCheckedTKT()">
                                <label class="" for="chk1">Mon</label>                                                                                    
                                <input type="checkbox" name="checkbox-TKT" id="Chk_TueTKT" value="Tue" class="chk_OperationDaysTKT" tabindex="-1" onchange="DaysCheckedTKT()">
                                <label class="" for="chk1">Tue</label>                                                                                    
                                <input type="checkbox" name="checkbox-TKT" id="Chk_WedTKT" value="Wed" class="chk_OperationDaysTKT" tabindex="-1" onchange="DaysCheckedTKT()">
                                <label class="" for="chk1">Wed</label>                                                                                    
                                <input type="checkbox" name="checkbox-TKT" id="Chk_ThuTKT" value="Thu" class="chk_OperationDaysTKT" tabindex="-1" onchange="DaysCheckedTKT()">
                                <label class="" for="chk1">Thu</label>                                                                                    
                                <input type="checkbox" name="checkbox-TKT" id="Chk_FriTKT" value="Fri" class="chk_OperationDaysTKT" tabindex="-1" onchange="DaysCheckedTKT()">
                                <label class="" for="chk1">Fri</label>                                                                                    
                                <input type="checkbox" name="checkbox-TKT" id="Chk_SatTKT" value="Sat" class="chk_OperationDaysTKT" tabindex="-1" onchange="DaysCheckedTKT()">
                                <label class="" for="chk1">Sat</label>                                                                                    
                                <input type="checkbox" name="checkbox-TKT" id="Chk_SunTKT" value="Sun" class="chk_OperationDaysTKT" tabindex="-1" onchange="DaysCheckedTKT()">
                                <label class="" for="chk1">Sun</label>
                            </div>
                            <div class="new-row seven-columns" id="OperationDaysSIC" style="display:none">
                                <h6>Operation Days SIC:</h6>

                                <input type="checkbox" name="checkbox-SIC" id="chk_DailySIC" value="Daily" class="chk_OperationDaysSIC" tabindex="-1" onchange="DaysAllCheckedSIC()">
                                <label class="" for="chk1">Daily</label>
                                <input type="checkbox" name="checkbox-SIC" id="Chk_MonSIC" value="Mon" class="chk_OperationDaysSIC" tabindex="-1" onchange="DaysCheckedSIC()">
                                <label class="" for="chk1">Mon</label>                                                                                               
                                <input type="checkbox" name="checkbox-SIC" id="Chk_TueSIC" value="Tue" class="chk_OperationDaysSIC" tabindex="-1" onchange="DaysCheckedSIC()">
                                <label class="" for="chk1">Tue</label>                                                                                               
                                <input type="checkbox" name="checkbox-SIC" id="Chk_WedSIC" value="Wed" class="chk_OperationDaysSIC" tabindex="-1" onchange="DaysCheckedSIC()">
                                <label class="" for="chk1">Wed</label>                                                                                               
                                <input type="checkbox" name="checkbox-SIC" id="Chk_ThuSIC" value="Thu" class="chk_OperationDaysSIC" tabindex="-1" onchange="DaysCheckedSIC()">
                                <label class="" for="chk1">Thu</label>                                                                                               
                                <input type="checkbox" name="checkbox-SIC" id="Chk_FriSIC" value="Fri" class="chk_OperationDaysSIC" tabindex="-1" onchange="DaysCheckedSIC()">
                                <label class="" for="chk1">Fri</label>                                                                                               
                                <input type="checkbox" name="checkbox-SIC" id="Chk_SatSIC" value="Sat" class="chk_OperationDaysSIC" tabindex="-1" onchange="DaysCheckedSIC()">
                                <label class="" for="chk1">Sat</label>                                                                                               
                                <input type="checkbox" name="checkbox-SIC" id="Chk_SunSIC" value="Sun" class="chk_OperationDaysSIC" tabindex="-1" onchange="DaysCheckedSIC()">
                                <label class="" for="chk1">Sun</label>
                            </div>
                            <div class="new-row seven-columns" id="OperationDaysPVT" style="display:none">
                                <h6>Operation Days PVT:</h6>

                                <input type="checkbox" name="checkbox-PVT" id="chk_DailyPVT" value="Daily" class="chk_OperationDaysPVT" tabindex="-1" onchange="DaysAllCheckedPVT()">
                                <label class="" for="chk1">Daily</label>
                                <input type="checkbox" name="checkbox-PVT" id="Chk_MonPVT" value="Mon" class="chk_OperationDaysPVT" tabindex="-1" onchange="DaysCheckedPVT()">
                                <label class="" for="chk1">Mon</label>                                                                                               
                                <input type="checkbox" name="checkbox-PVT" id="Chk_TuePVT" value="Tue" class="chk_OperationDaysPVT" tabindex="-1" onchange="DaysCheckedPVT()">
                                <label class="" for="chk1">Tue</label>                                                                                               
                                <input type="checkbox" name="checkbox-PVT" id="Chk_WedPVT" value="Wed" class="chk_OperationDaysPVT" tabindex="-1" onchange="DaysCheckedPVT()">
                                <label class="" for="chk1">Wed</label>                                                                                               
                                <input type="checkbox" name="checkbox-PVT" id="Chk_ThuPVT" value="Thu" class="chk_OperationDaysPVT" tabindex="-1" onchange="DaysCheckedPVT()">
                                <label class="" for="chk1">Thu</label>                                                                                               
                                <input type="checkbox" name="checkbox-PVT" id="Chk_FriPVT" value="Fri" class="chk_OperationDaysPVT" tabindex="-1" onchange="DaysCheckedPVT()">
                                <label class="" for="chk1">Fri</label>                                                                                               
                                <input type="checkbox" name="checkbox-PVT" id="Chk_SatPVT" value="Sat" class="chk_OperationDaysPVT" tabindex="-1" onchange="DaysCheckedPVT()">
                                <label class="" for="chk1">Sat</label>                                                                                               
                                <input type="checkbox" name="checkbox-PVT" id="Chk_SunPVT" value="Sun" class="chk_OperationDaysPVT" tabindex="-1" onchange="DaysCheckedPVT()">
                                <label class="" for="chk1">Sun</label>
                            </div>

                            <div class="line2"></div>


                            <div id="SlotUI" style="width: 100%">
                            </div>

                        </div>
                    </div>

                </fieldset>

                <fieldset class="wizard-fieldset fields-list" id="tab_Policy">
                    <legend class="legend">Policy & Note</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Activity Policy & Tour Note</b></label>
                        <div class="columns" style="min-height: 300px; margin-top: 20px">
                            <%--<div class="new-row twelve-columns" style="margin-bottom: -2px;">
                            <h3 class="thin underline green">Activity Policy & Tour Note</h3>
                        </div>--%>

                            <div class="four-columns" id="MinCapacity">
                                <h6>Minimum Capacity  :</h6>
                                <input type="number" id="txt_MinCapacity" class="input full-width" value="1" min="1" />
                            </div>

                            <div class="four-columns" id="MaximumCapacity">
                                <h6>Maximum Capacity  :</h6>
                                <input type="number" id="txt_MaximumCapacity" class="input full-width" value="" min="1" />
                            </div>

                            <div class="new-row eight-columns">
                                <h6>Tour Note :</h6>
                                <textarea id="txt_tournote" class="input full-width autoexpanding" rows="4"></textarea>
                            </div>

                        </div>
                    </div>
                </fieldset>

                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Images</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Images</b></label>
                        <div class="columns" style="min-height: 300px; margin-top: 20px">

                            <%--<div class="columns" style="height: 300px;">
                            <div class="eight-columns">
                                <div id="image-holder-Add">
                                    <img src='' id='ImgMain-Add' class='selMainImage-Add' width='70%'>
                                    <small class="input-info">New Image Url</small>
                                    <input type="text" id="ImgUrl-Add" size="9" class="input full-width" value="">
                                </div>

                            </div>
                            
                        </div>--%>

                            <div class="twelve-columns">


                                <%-- <h6>Set Default Images  :</h6>
                                <div id="div_defaultimage" class="">

                                    <table class="table table-striped table-bordered dataTable" style="height: 36px; width: 100%;">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <input id="DefaultImges" class="btn-search5 input full-width" type="file" multiple />
                                                </td>

                                               
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br />
                                    <div class="scrollingModal" id="old_DefaultImg"></div>
                                    <div class="scrollingModal">
                                        <ul id="Defaultresult" class="gallery" style="left: 0px; right: 74px;">

                                        </ul>
                                    </div>
                                </div>--%>


                                <h6>Images  :</h6>
                                <div id="div_CurrentImages1" class="">
                                    <span id="Loader" class="loader huge overlay"></span>
                                    <table class="table table-striped table-bordered dataTable" style="height: 36px; width: 100%;">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <input id="Imges" class="btn-search5 input full-width" type="file" multiple />
                                                </td>

                                                <%-- <td>
                                                <input type="button" style="float: right;" class="button glossy" id="Button1" value="Save file" />
                                            </td> --%>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br />
                                    <div class="scrollingModal" id="old_Img">
                                        <%--<ul id="resultt" class="gallery" style="left: 0px; right: 74px;">

                                        </ul>--%>
                                    </div>
                                    <br />

                                    <div class="scrollingModal">
                                        <ul id="result" class="gallery" style="left: 0px; right: 74px;">
                                        </ul>
                                    </div>
                                    <br />
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <hr />
                    <button type="button" id="btn_Save" class="button glossy blue-gradient float-right" onclick="Save()">Save</button>
                    <%--onclick="AddActivity();"--%>
                </fieldset>

            </form>
        </div>

    </section>
    <!-- End main content -->
</asp:Content>
