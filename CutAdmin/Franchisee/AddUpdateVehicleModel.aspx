﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Franchisee/AgentMaster.Master" AutoEventWireup="true" CodeBehind="AddUpdateVehicleModel.aspx.cs" Inherits="CutAdmin.Franchisee.AddUpdateVehicleModel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/VehicleModel.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>Vehicle Model</h1>

        </hgroup>

        <div class="with-padding">
            <div class="columns">
                <div class="twelve-columns six-columns-mobile" style="text-align:right">
                    <button type="button" class="button compact anthracite-gradient" onclick="NewVehicleModel()">New Vehicle Model</button></div>
                <%--<div class="six-columns  six-columns-mobile text-alignright selCurrency">
                    <div class="full-width button-height">
                        <select id="selCurrency" class="select" onchange="GetPackages()">
                            <option selected="selected" value="USD">USD</option>
                            <option value="INR">INR</option>
                            <option value="AED">AED</option>
                        </select>
                    </div>
                </div>--%>
            </div>

            <div class="respTable">
                <table class="table responsive-table" id="tbl_VehicleModel">
                    <thead>
                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">Vehicle Type Name</th>
                            <th scope="col">Seating Capacity</th>
                            <th scope="col">Baggage Capacity</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>

    </section>
    <script>

     
        // New Pakages modal
        function NewVehicleModel() {
            $.modal({
                content: '<div class="modal-body" id="VehicleModelModal">' +
				'<div class="columns">' +
'<div class="Twelve-columns twelve-columns-mobile"><label>Vehicle Type<span class="red">*</span>:</label><div class="full-width button-height" id="DivVehicleType">' +
'<select id="selVehicleType" class="select">' +
'</select> ' +
'</div></div>' +
'<div class="columns">' +
'<div class="six-columns twelve-columns-mobile"><label>Seating Capacity<span class="red">*</span>:</label> <div class="input full-width">' +
'<input name="prompt-value" id="txt_SeatingCapacity" placeholder="Seating Capacity" value="" class="input-unstyled full-width"  type="text">' +
'</div></div>' +
'<div class="six-columns twelve-columns-mobile"><label>Baggage Capacity<span class="red">*</span>: </label> <div class="input full-width">' +
'<input name="prompt-value" id="txt_BaggageCapacity" placeholder="Baggage Capacity"  class="input-unstyled full-width"  type="text">' +
'</div></div></div>' +
'</div>' +
'<p class="text-alignright"><input type="button" value="Add" onclick="AddVehicleModel();" title="Submit Details" class="button anthracite-gradient"/></p>' +
'</div>',

                title: 'Add Vehicle Model',
                width: 500,
                scrolling: true,
                actions: {
                    'Close': {
                        color: 'red',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttons: {
                    'Close': {
                        classes: 'huge anthracite-gradient displayNone',
                        click: function (win) { win.closeModal(); }
                    }
                },
                buttonsLowPadding: true
            });
            GetVehicleType();
        };
       
    </script>

</asp:Content>
