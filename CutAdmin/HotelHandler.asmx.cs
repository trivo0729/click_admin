﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.Models;
using CommonLib.Response;
using CutAdmin.EntityModal;
using CutAdmin.Services;
namespace CutAdmin
{
    /// <summary>
    /// Summary description for HotelHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class HotelHandler : System.Web.Services.WebService
    {
        string json = ""; string Texts = "";
        string jsonString = "";
        DataTable dtResult;
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //Click_Hotel DB = new Click_Hotel();
        DBHelper.DBReturnCode retCode;

        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }

        #region Destination

        [WebMethod(EnableSession = true)]
        public string GetDestinationCode(string name)
        {
            jsSerializer = new JavaScriptSerializer();
            List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
            List<Int64> arrSupplier = AuthorizationManager.GetAuthorizedSupplier(); /*Check  Authorized Supplier*/
            try
            {
                using (var DB = new Click_Hotel())
                {
                    foreach (var objSupplier in arrSupplier)
                    {
                        var arrHotels = (from obj in DB.tbl_CommonHotelMaster
                                         where obj.ParentID == objSupplier && obj.CityId.Contains(name)
                                         select new Models.AutoComplete
                                         {
                                             id = obj.CityId,
                                             value = obj.CityId + "," + obj.CountryId,
                                         }).Distinct().ToList();
                        foreach (var objLocation in arrHotels)
                        {
                            if (list_autocomplete.Where(d => d.id == objLocation.id).FirstOrDefault() == null)
                            {
                                list_autocomplete.Add(objLocation);
                            }
                        }
                    }
                }

                return jsSerializer.Serialize(list_autocomplete.Distinct());
            }
            catch (Exception)
            {

                return jsSerializer.Serialize(new { id = "", value = "No Data found" });
            }
            ////string jsonString = "";
            ////DataTable dtResult;
            ////DBHelper.DBReturnCode retcode = DestinationManager.Get(name, "ENG", out dtResult);
            ////JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            ////if (retcode == DBHelper.DBReturnCode.SUCCESS)
            ////{
            ////    list_autocomplete = dtResult.AsEnumerable()
            ////    .Select(data => new Models.AutoComplete
            ////    {
            ////        id = data.Field<String>("DestinationCode"),
            ////        value = data.Field<String>("Destination")
            ////    }).ToList();

            ////    jsonString = objSerlizer.Serialize(list_autocomplete);
            ////    dtResult.Dispose();
            ////}
            ////else
            ////{
            ////    jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            ////}
            ////return jsonString;
        }


        [WebMethod(EnableSession = true)]
        public string GetDestinationList(string name)
        {
            //jsSerializer = new JavaScriptSerializer();
            //List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
            //List<Int64> arrSupplier = AuthorizationManager.GetAuthorizedSupplier(); /*Check  Authorized Supplier*/
            //try
            //{
            //    using (var DB = new Click_Hotel())
            //    {
            //        foreach (var objSupplier in arrSupplier)
            //        {
            //            var arrHotels = (from obj in DB.tbl_CommonHotelMaster
            //                             where obj.ParentID == objSupplier && obj.CityId.Contains(name)
            //                             select new Models.AutoComplete
            //                             {
            //                                 id = obj.CityId,
            //                                 value = obj.CityId + "," + obj.CountryId,
            //                             }).Distinct().ToList();
            //            foreach (var objLocation in arrHotels)
            //            {
            //                if (list_autocomplete.Where(d => d.id == objLocation.id).FirstOrDefault() == null)
            //                {
            //                    list_autocomplete.Add(objLocation);
            //                }
            //            }
            //        }
            //    }

            //    return jsSerializer.Serialize(list_autocomplete.Distinct());
            //}
            //catch (Exception)
            //{

            //    return jsSerializer.Serialize(new { id = "", value = "No Data found" });
            //}
            string jsonString = "";
            DataTable dtResult;
            List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
            DBHelper.DBReturnCode retcode = DestinationManager.Get(name, "ENG", out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("DestinationCode"),
                    value = data.Field<String>("Destination")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }


        [WebMethod(true)]
        public string GetNationalityCOR()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = DestinationManager.GetNationalityCOR(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"NationalityMaster\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }
        #endregion Destination

        #region Mapped Hotel list

        //[WebMethod(EnableSession = true)]
        //public string GetMappedHotels()
        //{
        //    try
        //    {
        //        using (var DB = new Click_Hotel())
        //        {
        //            string jsonString = "";
        //            JavaScriptSerializer objserialize = new JavaScriptSerializer();
        //            DataTable dtResult;
        //            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //            string sid = Convert.ToString(objGlobalDefault.sid);
        //            if (objGlobalDefault.UserType == "SupplierStaff")
        //                sid = objGlobalDefault.ParentId.ToString();
        //            //DBHelper.DBReturnCode retcode = HotelMappingManager.GetMappedHotels(sid, out dtResult);

        //            //if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //            //{
        //            //List<Dictionary<string, object>> objHotelList = new List<Dictionary<string, object>>();
        //            //objHotelList = JsonStringManager.ConvertDataTable(dtResult);

        //            //if (objGlobalDefault.UserType == "Admin")
        //            //{
        //            //    var objHotelList = (from obj in DB.tbl_CommonHotelMaster orderby obj.sid ascending select obj).ToList();
        //            //}
        //            //else
        //            //{
        //                var objHotelList = (from obj in DB.tbl_CommonHotelMaster where obj.ParentID == Convert.ToInt64(sid) orderby obj.sid ascending select obj).ToList();
        //            //}

        //            var OfferList = (from Offer in DB.tbl_HotelOfferUtilities
        //                             join OfferDay in DB.tbl_OfferDayUtilities on Offer.HotelOfferId equals OfferDay.Offer_Id
        //                             join Room in DB.tbl_RoomDetailUtilities on Offer.Room_Id equals Room.Room_Id
        //                             where OfferDay.DateType == "Season Date"
        //                             select new
        //                             {
        //                                 OfferDay.BookBefore,
        //                                 OfferDay.DaysPrior,
        //                                 OfferDay.OfferType,
        //                                 OfferDay.DiscountAmount,
        //                                 OfferDay.DiscountPer,
        //                                 OfferDay.FreebiItem,
        //                                 OfferDay.FreebiItemDetail,
        //                                 OfferDay.HotelOfferCode,
        //                                 OfferDay.MinNights,
        //                                 OfferDay.MinRooms,
        //                                 OfferDay.NewRate,
        //                                 OfferDay.OfferCode,
        //                                 OfferDay.OfferNote,
        //                                 OfferDay.OfferOn,
        //                                 OfferDay.OfferTerms,
        //                                 OfferDay.ValidFrom,
        //                                 OfferDay.ValidTo,
        //                                 Offer.Hotel_Id,
        //                                 Offer.HotelOfferId,
        //                                 Offer.Room_Id,
        //                                 Room.RoomCategory
        //                             }).ToList();

        //            var RoomList = (from Room in DB.tbl_commonRoomDetails
        //                            join Hotel in DB.tbl_CommonHotelMaster on Room.HotelId equals Hotel.sid
        //                            join RoomType in DB.tbl_commonRoomTypes on Room.RoomTypeId equals RoomType.RoomTypeID
        //                            select new
        //                            {
        //                                Hotel.sid,
        //                                RoomType.RoomType,
        //                                Room.RoomId,


        //                            }).ToList();
        //            //dtResult.Dispose();
        //            return objserialize.Serialize(new { Session = 1, retCode = 1, MappedHotelList = objHotelList, OfferList = OfferList, RoomList = RoomList });
        //        }
        //        //}
        //        //else
        //        //{

        //        //}
        //    }
        //    catch (Exception)
        //    {

        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return jsonString;
        //}

        [WebMethod(EnableSession = true)]
        public string GetMappedHotels()
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    JavaScriptSerializer objserialize = new JavaScriptSerializer();
                    CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 Parentid = AccountManager.GetSuperAdminID();
                    //if (objGlobalDefault.UserType == "SupplierStaff")
                    //    sid = objGlobalDefault.ParentId.ToString();
                    List<tbl_CommonHotelMaster> objHotelList = new List<tbl_CommonHotelMaster>();
                    objHotelList = (from obj in DB.tbl_CommonHotelMaster where obj.ParentID == Parentid orderby obj.sid ascending select obj).ToList();
                    var OfferList = new object();
                    return objserialize.Serialize(new { Session = 1, retCode = 1, MappedHotelList = objHotelList, OfferList = OfferList, SupplierId = objGlobalDefault.sid });
                }
            }
            catch (Exception ex)
            {

                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetRoomListByHotel(Int64 HotelCode)
        {
            try
            {
                var RoomList = new object();
                using (var DB = new Click_Hotel())
                {
                    RoomList = (from Room in DB.tbl_commonRoomDetails
                                join Hotel in DB.tbl_CommonHotelMaster on Room.HotelId equals Hotel.sid
                                join RoomType in DB.tbl_commonRoomType on Room.RoomTypeId equals RoomType.RoomTypeID
                                where Convert.ToInt64(Room.HotelId) == HotelCode
                                select new
                                {
                                    Hotel.sid,
                                    RoomType.RoomType,
                                    Room.RoomId,
                                }).ToList();
                }
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, RoomList = RoomList });

            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetMappedHotelstoUpdate(string Country, string City)
        {
            Int64 UID = AccountManager.GetSupplierByUser();
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            try
            {
                List<CommonLib.Response.HotelInfo> objHotelList = new List<CommonLib.Response.HotelInfo>();
                var arrSupplier = AuthorizationManager.GetAuthorizedSupplier();
                using (var DB = new Click_Hotel())
                {
                    using (var db = new CutAdmin.dbml.helperDataContext())
                    {
                        foreach (var SupplierID in arrSupplier)
                        {
                            var arrHotel = (from obj in DB.tbl_CommonHotelMaster join 
                                            objRatePlan in DB.tbl_RatePlan on obj.sid equals objRatePlan.HotelID
                                            where obj.ParentID == SupplierID && obj.CityId.Contains(City) && obj.CountryId.Contains(Country)
                                            select new CommonLib.Response.HotelInfo
                                            {
                                                HotelName = obj.HotelName,
                                                sid = obj.sid,
                                                SupplierName = "",
                                            }).ToList();
                            foreach (var objHotel in arrHotel)
                            {
                                objHotelList.Add(objHotel);
                            }
                        }
                    }

                }
                if (objHotelList.Count != 0)
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 1, HotelListbyCity = objHotelList.OrderBy(d => d.HotelName).ToList() });
                }
                else
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 0 });
                }
            }
            catch (Exception ex)
            {
                return objserialize.Serialize(new { Session = 1, retCode = 0, ex = "No Hotel found for this Location." });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetMappedHotelsList(string Country, string City)
        {
            Int64 UID = AccountManager.GetSupplierByUser();
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            DataTable dtResult;
            List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
            try
            {
                List<CommonLib.Response.HotelInfo> objHotelList = new List<CommonLib.Response.HotelInfo>();
                var arrSupplier = AuthorizationManager.GetAuthorizedSupplier();
                JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
                using (var DB = new Click_Hotel())
                {
                    using (var db = new CutAdmin.dbml.helperDataContext())
                    {
                        foreach (var SupplierID in arrSupplier)
                        {
                            var arrHotel = (from obj in DB.tbl_CommonHotelMaster
                                            where obj.ParentID == SupplierID && obj.CityId.Contains(City) && obj.CountryId.Contains(Country)
                                            select new CommonLib.Response.HotelInfo
                                            {
                                                HotelName = obj.HotelName,
                                                sid = obj.sid,
                                                SupplierName = "",
                                            }).ToList();
                            foreach (var objHotel in arrHotel)
                            {
                                objHotelList.Add(objHotel);
                                list_autocomplete = objHotelList.AsEnumerable()
                                .Select(data => new Models.AutoComplete
                                {
                                    id = objHotel.sid.ToString(),
                                    value = objHotel.HotelName,
                                }).ToList();
                            }
                        }
                    }

                }
                if (objHotelList.Count != 0)
                {



                    //  dtResult.Dispose();
                    return jsonString = objSerlizer.Serialize(list_autocomplete);
                    // return objserialize.Serialize(new { Session = 1, retCode = 1, HotelListbyCity = objHotelList.OrderBy(d => d.HotelName).ToList() });
                }
                else
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 0 });
                }
            }
            catch (Exception ex)
            {
                return objserialize.Serialize(new { Session = 1, retCode = 0, ex = "No Hotel found for this Location." });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetHotelByHotelId(Int64 HotelCode)
        {
            using (var DB = new Click_Hotel())
            {
                string jsonString = "";
                JavaScriptSerializer objserialize = new JavaScriptSerializer();
                DataTable dtResult;
                //DBHelper.DBReturnCode retcode = HotelMappingManager.GetHotelByHotelId(HotelCode, out dtResult);
                var HotelList = (from Hoteltbl in DB.tbl_CommonHotelMaster where Hoteltbl.sid == HotelCode select Hoteltbl).ToList();
                if (HotelList.Count != 0)
                {
                    List<Dictionary<string, object>> objHotelList = new List<Dictionary<string, object>>();
                    List<string> objFacilities = new List<string>();
                    //objHotelList = JsonStringManager.ConvertDataTable(dtResult);
                    //objHotelList = HotelList;
                    //var ContactList = (from obj in DB.tbl_CommonHotelContacts where obj.HotelCode == HotelCode select obj).ToList();
                    var ContactList = (from obj in DB.Comm_HotelContacts where obj.HotelCode == HotelCode select obj).ToList();
                    var FacilityList = (from obj in DB.tbl_commonFacility select obj).ToList();
                    var arrLocation = LocationService._ByID(HotelList.FirstOrDefault().LocationId);
                    var arrFacility = Services.Facility._ByHotel(HotelList.FirstOrDefault().sid.ToString());
                    return objserialize.Serialize(new { Session = 1, retCode = 1, HotelListbyId = HotelList, ContactList = ContactList, FacilityList = FacilityList, arrLocation=arrLocation , arrFacility= arrFacility });

                }
                else
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                return jsonString;
            }
        }




        [WebMethod(true)]
        public string GetFacilities()
        {
            string jsonString = "";
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            //DataTable dtResult;
            //DBHelper.DBReturnCode retcode = HotelMappingManager.GetFacilities(out dtResult);
            //if (retcode == DBHelper.DBReturnCode.SUCCESS)
            //{
            //    jsonString = "";
            //    foreach (DataRow dr in dtResult.Rows)
            //    {
            //        jsonString += "{";
            //        foreach (DataColumn dc in dtResult.Columns)
            //        {
            //            jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
            //        }
            //        jsonString = jsonString.Trim(',') + "},";
            //    }
            //    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"HotelFacilities\":[" + jsonString.Trim(',') + "]}";
            //    dtResult.Dispose();
            //}
            //else
            //{
            //    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            //}
            using (var DB = new Click_Hotel())
            {
                try
                {
                    var FacilityList = (from HotelFacility in DB.tbl_commonFacility select HotelFacility).ToList();
                    return objserialize.Serialize(new { Session = 1, retCode = 1, HotelFacilities = FacilityList });
                }
                catch (Exception ex)
                {

                }
                return jsonString;
            }
        }

        [WebMethod(true)]
        public string GetHalal()
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = HotelMappingManager.GetHalal(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"HotelFacilities\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;

        }

        #endregion


        #region update Hotel Mapped Table

        //[WebMethod(true)]
        //public string SaveFacilities(string sFacilities,Int64 sHotelId)
        //{
        //    retCode = HotelMappingManager.MappedHotelSearch(sHotelId, out dtResult);

        //    CUT.DataLayer.GlobalDefault objUser= (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //    Int64 sUserID = objUser.sid;
        //    string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");

        //    if (retCode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        try
        //        {
        //            tbl_CommonHotelMaster Hotel = new tbl_CommonHotelMaster();
        //            Hotel.HotelFacilities = sFacilities;

        //            DB.SubmitChanges();
        //            json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //        }
        //        catch (Exception ex)
        //        {
        //            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //        }
        //    }
        //    else
        //    {
        //        try
        //        {
        //            tbl_CommonHotelMaster Hotel = new tbl_CommonHotelMaster();
        //            Hotel.HotelFacilities = sFacilities;

        //            DB.tbl_CommonHotelMaster.InsertOnSubmit(Hotel);
        //            DB.SubmitChanges();
        //            json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //        }
        //        catch (Exception ex)
        //        {
        //            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //        }
        //    }

        //    return json;

        //}

        [WebMethod(true)]
        public string AddSupplier(Int64 srno, string SupplierName, string SupplierCode)
        {
            //CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];

            retCode = HotelMappingManager.AddSupplier(srno, SupplierName, SupplierCode);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }


        [WebMethod(true)]
        public string SaveFacilities(string sFacilities, Int64 sHotelId)
        {
            //CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];

            retCode = HotelMappingManager.SaveFacilities(sFacilities, sHotelId);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(true)]
        public string SaveRatings(string sRatings, Int64 sHotelId)
        {
            //CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];

            retCode = HotelMappingManager.SaveRatings(sRatings, sHotelId);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }

        [WebMethod(true)]
        public string SaveImageUrl(string addedUrl, Int64 sHotelId)
        {
            //CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];

            retCode = HotelMappingManager.SaveImageUrl(addedUrl, sHotelId);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        #endregion



        #region Rooms List
        [WebMethod(EnableSession = true)]
        public string GetRoomType()
        {
            DBHelper.DBReturnCode retcode = RoomsManager.GetRoomType(out dtResult);
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonString = "";
                foreach (DataRow dr in dtResult.Rows)
                {
                    jsonString += "{";
                    foreach (DataColumn dc in dtResult.Columns)
                    {
                        jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jsonString = jsonString.Trim(',') + "},";
                }
                jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"RoomType\":[" + jsonString.Trim(',') + "]}";
                dtResult.Dispose();
            }
            else
            {
                jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return jsonString;
        }

        [WebMethod(true)]
        public string AddRoomAmenity(string RoomAmenityText, Int64 HotelId)
        {
            using (var DB = new Click_Hotel())
            {
                CUT.DataLayer.GlobalDefault objUser = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                Int64 sUserID = AccountManager.GetUserByLogin();
                string DateTimes = DateTime.Now.ToString("dd-MM-yyy HH:mm");
                tbl_commonRoomDetails RoomList = DB.tbl_commonRoomDetails.Where(x => x.HotelId == HotelId).FirstOrDefault();
                var RoomAmenityList = (from obj in DB.tbl_commonAmunity where obj.RoomAmunityName == RoomAmenityText select obj).ToList();

                if (RoomAmenityList.Count() > 0)
                {
                    try
                    {
                        tbl_commonAmunity RoomAmenity = DB.tbl_commonAmunity.Single(x => x.RoomAmunityName == RoomAmenityText);
                        RoomAmenity.RoomAmunityName = RoomAmenityText;
                        DB.SaveChanges();
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"RoomAmenityID\":" + RoomAmenity.RoomAmunityID + "}";
                    }
                    catch (Exception ex)
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }



                }
                else
                {
                    try
                    {
                        tbl_commonAmunity RoomAmenity = new tbl_commonAmunity();
                        RoomAmenity.RoomAmunityName = RoomAmenityText;
                        DB.tbl_commonAmunity.Add(RoomAmenity);
                        DB.SaveChanges();
                        Int64 RoomAmenityID = RoomAmenity.RoomAmunityID;
                        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"RoomAmenityID\":" + RoomAmenityID + "}";
                    }
                    catch (Exception ex)
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                    }
                }
                return json;
            }
        }


        [WebMethod(EnableSession = true)]
        public string GetRoomAmenities()
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    var arrAminity = (from obj in DB.tbl_commonAmunity select obj).ToList();
                    return jsSerializer.Serialize(new { retCode = 1, arrRoomAmenities = arrAminity });
                }
            }
            catch
            {

            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string SaveRoom(tbl_commonRoomType arrRoomType, tbl_commonRoomDetails arrRoom)
        {
            try
            {
                RoomManager.SaveRooms(arrRoomType, arrRoom);
                return jsSerializer.Serialize(new { retCode = 1 , RoomID = arrRoom.RoomId });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        #endregion

        #region Hotel List

        [WebMethod(EnableSession = true)]
        public string HotelActivation(Int64 HotelId, string Status)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    tbl_CommonHotelMaster Hotel = DB.tbl_CommonHotelMaster.Single(x => x.sid == HotelId);
                    Hotel.Active = Status;
                    DB.SaveChanges();
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, ActiveHotel = Hotel.Active });
                }
            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0 });
            }
            return json;
        }

        #endregion

        #region Hotel Images
        [WebMethod(true)]
        public string UpdateImage(Int64 sHotelId, string addedUrl)
        {
            retCode = HotelMappingManager.SaveImageUrl(addedUrl, sHotelId);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }


        //[WebMethod(EnableSession = true)]
        //public string DeleteSubImage(string FileNo, string noFiles, string sid)
        //{
        //    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //    try
        //    {
        //        string Path = System.Web.HttpContext.Current.Server.MapPath("~/HotelImages/");
        //        string FileName = FileNo;
        //        DirectoryInfo dir = new DirectoryInfo(Path);
        //        FileInfo[] files = null;

        //        files = dir.GetFiles();

        //        foreach (FileInfo file in files)
        //        {
        //            if (file.Name == FileName)
        //            {
        //                file.Delete();
        //                break;
        //            }
        //            else
        //            {
        //                continue;
        //            }

        //        }
        //        DataManager.DBReturnCode retCode = HotelMappingManager.SubImages(noFiles, sid);
        //        return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
        //    }
        //    catch (Exception ex)
        //    {
        //        return jsSerializer.Serialize(new { retCode = 1, Session = 1 });
        //    }
        //}
        #endregion


        #region Search Add
        [WebMethod(true)]
        public string AddSearch(string CityName, string CheckIn, string CheckOut, string HotelCode, string TotalNights, string StarRating, string Nationality, string NumberOfRooms, string EntryDate, string AddSearchsession)
        {
            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
            Int64 UserId = objGlobalDefault.sid;
            string Franchisee = objGlobalDefault.Franchisee;
            DBHelper.DBReturnCode retCode = HotelManager.AddHotelSearch(CityName, CheckIn, CheckOut, HotelCode, TotalNights, StarRating, Nationality, NumberOfRooms, EntryDate, UserId, AddSearchsession);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else
            {
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }
        #endregion

        #region Counts

        [WebMethod(EnableSession = true)]
        public string GetCount()
        {
           // EmailManager.SendEmail("KASHIFKHAN4849@GMAIL.COM", "Your Password Detail", "Information Received", "kASHIDF", "KASHIFKHAN4849@GMAIL.COM", "11111", "12457");
            try
            {
                using (var DB = new Click_Hotel())
                {
                    if (HttpContext.Current.Session["LoginUser"] == null)
                        throw new Exception("Session Expired ,Please Login and try Again");
                    CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 Hotel = 0, Agent = 0;
                    Int64 Uid = AccountManager.GetUserByLogin();
                    long ParentId = AccountManager.GetSuperAdminID();
                    using (var db = new CutAdmin.dbml.helperDataContext())
                    {
                        IQueryable<tbl_CommonHotelMaster> arrHotels = from obj in DB.tbl_CommonHotelMaster
                                                                      where obj.ParentID == ParentId
                                                                      select obj;


                        Hotel = arrHotels.Count();

                        IQueryable<CutAdmin.dbml.tbl_AdminLogin> arrAgent = from obj in db.tbl_AdminLogins
                                                                            where obj.ParentID == Uid && obj.UserType == "Agent"
                                                                            select obj;
                        Agent = arrAgent.Count();
                    }
                    var arrBookings = Reservations.BookingList();
                    List<Reservation> arrBuyerRevt = new List<Reservation>();
                    List<Reservation> arrSupplierRevt = new List<Reservation>();
                    List<Reservation> arrDestinationsRevt = new List<Reservation>();
                    List<Reservation> arrHotelsRevt = new List<Reservation>();

                    arrBookings.Where(d => d.Source.Contains("HOTELBEDS")).ToList().ForEach(r => r.Source = "HOTELBEDS");
                    foreach (Int64 AgentID in arrBookings.Select(D => D.Uid).ToList().Distinct())
                    {
                        arrBuyerRevt.Add(new Reservation
                        {
                            Buyer = arrBookings.Where(d => d.Uid == AgentID).FirstOrDefault().AgencyName,
                            Count = arrBookings.Where(d => d.Uid == AgentID).Count()
                        });
                    }
                    foreach (string Supplier in arrBookings.Select(D => D.Source).ToList().Distinct())
                    {
                        arrSupplierRevt.Add(new Reservation
                        {
                            Supplier = arrBookings.Where(d => d.Source == Supplier).FirstOrDefault().Source,
                            Count = arrBookings.Where(d => d.Source == Supplier).Count()
                        });
                    }
                    foreach (string Country in arrBookings.Select(D => D.City).ToList().Distinct())
                    {
                        arrDestinationsRevt.Add(new Reservation
                        {
                            Destination = arrBookings.Where(d => d.City == Country).FirstOrDefault().City,
                            Count = arrBookings.Where(d => d.City == Country).Count()
                        });
                    }
                    foreach (string HotelName in arrBookings.Select(D => D.HotelName).ToList().Distinct())
                    {
                        arrHotelsRevt.Add(new Reservation
                        {
                            HotelName = HotelName,
                            Count = arrBookings.Where(d => d.HotelName == HotelName).Count()
                        });
                    }

                    var arrOnRequest = new
                    {
                        OnHoldBookings = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == false).ToList().Count,
                        OnHoldRequested = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().Count,
                        OnHoldConfirmed = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().Count,
                    };
                    var arrOnHold = new
                    {
                        OnHoldBookings = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == false).ToList().Count,
                        OnHoldRequested = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().Count,
                        OnHoldConfirmed = arrBookings.Where(d => d.Status == "OnRequest" && d.IsConfirm == true).ToList().Count,
                    };
                    var arrReconfirmed = new
                    {
                        ReconfirmPending = arrBookings.Where(d => d.Status != "Cancelled" && d.IsConfirm == false).ToList().Count,
                        ReconfirmRequested = arrBookings.Where(d => d.Status == "Vouchered" && d.IsConfirm == true).ToList().Count,
                        ReconfirmReject = arrBookings.Where(d => d.Status == "Cancelled" && d.IsConfirm == true).ToList().Count,
                    };
                    var arrGroupRequest = new
                    {
                        GroupRequestPending = arrBookings.Where(d => d.Status == "GroupRequest" && d.BookingStatus == "GroupRequest").ToList().Count,
                        GroupRequestRequested = arrBookings.Where(d => d.Status == "Vouchered" && d.BookingStatus == "GroupRequest").ToList().Count,
                        GroupRequestReject = arrBookings.Where(d => d.Status == "Cancelled" && d.BookingStatus == "GroupRequest").ToList().Count,
                    };

                    json = jsSerializer.Serialize(new
                    {
                        Session = 1,
                        retCode = 1,
                        HotelList = Hotel,
                        AgentCount = Agent,
                        arrBuyerRervation = arrBuyerRevt.OrderByDescending(d => d.Count).Take(5),
                        arrSupplierRevt = arrSupplierRevt.OrderByDescending(d => d.Count).Take(5),
                        arrDestinationsRevt = arrDestinationsRevt.OrderByDescending(d => d.Count).Take(5),
                        arrHotelsRevt = arrHotelsRevt.OrderByDescending(d => d.Count).Take(5),
                        arrOnRequest = arrOnRequest,
                        arrOnHold = arrOnHold,
                        arrReconfirmed = arrReconfirmed,
                        arrGroupRequest = arrGroupRequest,
                        arrBookings = arrBookings
                    });
                }
            }
            catch (Exception ex)
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 0, ex = ex.Message });
            }
            return json;
        }

        #endregion

        [WebMethod(EnableSession = true)]
        public string SetMarkup(string HotelCode, Int64 Per, Int64 Amt)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    CUT.DataLayer.GlobalDefault objUser = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 sUserID = AccountManager.GetUserByLogin();
                    Int32 Hotelid = Convert.ToInt32(HotelCode);
                    var MrkCOunt = DB.tbl_CommHotelMarkup.Where(x => x.HotelID == Hotelid && x.SupplierId == sUserID).ToList();
                    if (MrkCOunt.Count != 0)
                    {
                        DB.tbl_CommHotelMarkup.RemoveRange(MrkCOunt);
                        DB.SaveChanges();
                    }

                    var Add = new tbl_CommHotelMarkup()
                    {
                        HotelID = Convert.ToInt32(HotelCode),
                        SupplierId = sUserID,
                        Per = Per,
                        Amt = Amt
                    };
                    DB.tbl_CommHotelMarkup.Add(Add);
                    DB.SaveChanges();

                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetMarkup(string HotelCode)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    JavaScriptSerializer objserialize = new JavaScriptSerializer();
                    CUT.DataLayer.GlobalDefault objUser = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];
                    Int64 sUserID = AccountManager.GetUserByLogin();
                    Int64 HotelId = Convert.ToInt64(HotelCode);
                    var MrkCOunt = DB.tbl_CommHotelMarkup.Where(x => x.HotelID == HotelId && x.SupplierId == sUserID).ToList();
                    return objserialize.Serialize(new { Session = 1, retCode = 1, MrkCOunt = MrkCOunt });
                }
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
        [WebMethod(EnableSession = true)]
        public string DeleteHote(Int64 Id)
        {
            try
            {
                using (var DB = new Click_Hotel())
                {
                    tbl_CommonHotelMaster Delete = DB.tbl_CommonHotelMaster.Single(x => x.sid == Id);
                    DB.tbl_CommonHotelMaster.Remove(Delete);
                    //DB.SaveChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAgents(string AgentName)
        {
            jsSerializer = new JavaScriptSerializer();
            List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
            //List<Int64> arrSupplier = AuthorizationManager.GetAuthorizedSupplier(); /*Check  Authorized Supplier*/
            Int64 SupplierID = AccountManager.GetUserByLogin();
            try
            {
                using (var DB = new CutAdmin.dbml.helperDataContext())
                {
                    var arrAgents = (from obj in DB.tbl_AdminLogins
                                     where obj.UserType == "Agent" && obj.AgencyName.Contains(AgentName) && obj.ParentID == SupplierID
                                     select new Models.AutoComplete
                                     {
                                         id = obj.sid.ToString(),
                                         value = obj.AgencyName,
                                     }).Distinct().ToList();
                    foreach (var objAgent in arrAgents)
                    {
                        if (list_autocomplete.Where(d => d.id == objAgent.id).FirstOrDefault() == null)
                        {
                            list_autocomplete.Add(objAgent);
                        }
                    }
                }

                return jsSerializer.Serialize(list_autocomplete.Distinct());
            }
            catch (Exception)
            {

                return jsSerializer.Serialize(new { id = "", value = "No Data found" });
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetHotel(string name, string destination)
        {
            string jsonString = "";
            DataTable dtResult;
            DBHelper.DBReturnCode retcode = CutAdmin.DataLayer.HotelManager.Get(name, destination, out dtResult);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = dtResult.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    id = data.Field<String>("HotelCode"),
                    value = data.Field<String>("Name")
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                dtResult.Dispose();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }

        [WebMethod(EnableSession = true)]
        public string GetContractHotelsList(string name)
        {
            Int64 UID = AccountManager.GetSuperAdminID();
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            DataTable dtResult;
            List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
            try
            {
                List<CommonLib.Response.HotelInfo> objHotelList = new List<CommonLib.Response.HotelInfo>();
                using (var DB = new Click_Hotel())
                {
                    using (var db = new CutAdmin.dbml.helperDataContext())
                    {
                        var arrHotel = (from obj in DB.tbl_CommonHotelMaster
                                        where obj.HotelName.Contains(name)
                                        select new Models.AutoComplete
                                        {
                                            id = obj.sid.ToString(),
                                            value = obj.HotelName,
                                        }).Distinct().ToList();
                        //var list1 = arrHotel.GroupBy(x => x.value).FirstOrDefault().;

                        foreach (var objHotel in arrHotel)
                        {
                            if (list_autocomplete.Where(d => d.id == objHotel.id).FirstOrDefault() == null)
                            {
                                list_autocomplete.Add(objHotel);
                            }
                        }
                    }

                }
                if (list_autocomplete.Count != 0)
                {
                    return jsonString = objserialize.Serialize(list_autocomplete.GroupBy(x => x.value).Select(h => h.First()).ToList());
                }
                else
                {
                    return objserialize.Serialize(new { id = "", value = "No Data found" });
                }
            }
            catch (Exception ex)
            {
                return objserialize.Serialize(new { Session = 1, retCode = 0, ex = "No Hotel found." });
            }
        }


        [WebMethod(EnableSession = true)]
        public string GetHotelList(string name , long ParentID)
        {
            Int64 UID = AccountManager.GetSuperAdminID();
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            DataTable dtResult;
            List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
            try
            {
                List<CommonLib.Response.HotelInfo> objHotelList = new List<CommonLib.Response.HotelInfo>();
                using (var DB = new Click_Hotel())
                {
                    using (var db = new CutAdmin.dbml.helperDataContext())
                    {
                        var arrHotel = (from obj in DB.tbl_CommonHotelMaster
                                        where obj.HotelName.Contains(name) && obj.ParentID == ParentID
                                        select new Models.AutoComplete
                                        {
                                            id = obj.sid.ToString(),
                                            value = obj.HotelName,
                                        }).Distinct().ToList();
                        //var list1 = arrHotel.GroupBy(x => x.value).FirstOrDefault().;

                        foreach (var objHotel in arrHotel)
                        {
                            if (list_autocomplete.Where(d => d.id == objHotel.id).FirstOrDefault() == null)
                            {
                                list_autocomplete.Add(objHotel);
                            }
                        }
                    }

                }
                if (list_autocomplete.Count != 0)
                {
                    return jsonString = objserialize.Serialize(list_autocomplete.GroupBy(x => x.value).Select(h => h.First()).ToList());
                }
                else
                {
                    return objserialize.Serialize(new { id = "", value = "No Data found" });
                }
            }
            catch (Exception ex)
            {
                return objserialize.Serialize(new { Session = 1, retCode = 0, ex = "No Hotel found." });
            }
        }
    }
}
