﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="sightseeingavailability_pricing.aspx.cs" Inherits="CutAdmin.sightseeingavailability_pricing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--    <script src="Scripts/availability_pricing.js?v=1.39"></script>--%>
    <script src="Scripts/sightseeingavailability_Inventory.js?v=1.1"></script>
    <script src="Scripts/Supplier.js"></script>
    <script src="Scripts/Hotels/Markets.js"></script>
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.fixed.css?v=1" />
    <link href="css/files.css" rel="stylesheet" />

    <style>
        label span {
            margin: 0px;
            float: left;
        }

        #ui-datepicker-div {
            z-index: 10000000, !important;
        }

        .dataTables_scrollBody {
        }

        ::-webkit-scrollbar {
            width: 8px; /* for vertical scrollbars */
            height: 8px; /* for horizontal scrollbars */
            z-index: 999000;
            border-radius: 3px;
        }

        ::-webkit-scrollbar-track {
            background: transparent;
        }

        ::-webkit-scrollbar-thumb {
            position: absolute;
            background: url(../img/old-browsers/grey50.png);
            background: rgba(243, 237, 237, 0.69);
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
            -webkit-box-shadow: inset 0 0 0 1px rgba(255, 255, 255, 0.25);
            -moz-box-shadow: inset 0 0 0 1px rgba(255, 255, 255, 0.25);
            box-shadow: inset 0 0 0 1px rgba(255, 255, 255, 0.25);
        }

        @media only screen and (max-width: 767px) {

            span {
                font-size: .8em;
            }
        }
    </style>
    <script>
        $(document).ready(function () {
            GetSupplier("Activity");
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h3 class="font24 "><b>Availability Calander & Rates</b>
                <p class="button-group compact float-right">
                    <label for="Freesale_Id" class="button green-active">
                        <input type="radio" name="InvType" id="Freesale_Id" onchange="GenrateDatesSlots()" value="FreeSale" checked>
                        Freesale
                    </label>
                    <label for="Allocation_Id" class="button green-active ">
                        <input type="radio" name="InvType" id="Allocation_Id" onchange="GenrateDatesSlots()" value="Allocation">
                        Allocation
                    </label>
                    <label for="FreePurchased_Id" class="button green-active">
                        <input type="radio" name="InvType" id="FreePurchased_Id" onchange="GenrateDatesSlots()" value="FreePurchased">
                        Free Purchased  
                    </label>
                </p>
                <br />
                <br />
                <p class="button-group compact float-right">
                    <label for="View_Slot" class="button green-active">
                        <input type="radio" name="View" id="View_Slot" onchange="" value="Slot" checked>
                        Slot View
                    </label>
                    <label for="View_Date" class="button green-active ">
                        <input type="radio" name="View" id="View_Date" onchange="" value="Date">
                        Datewise View
                    </label>
                </p>
            </h3>
            <hr />
        </hgroup>
        <div class="with-padding ">
            <div class="columns">
                <div class="two-columns six-columns-mobile">
                    <select id="sel_Activity" onchange="GenrateDatesSlots()" name="validation-select" class="select full-width">
                    </select>
                </div>
                <div class="two-columns twelve-columns-mobile button-height">
                    <span class="input full-width">
                        <input type="text" id="datepicker_start" class="input-unstyled validate[required] dt1" value="" placeholder="Date">
                    </span>
                </div>
                <div class="two-columns six-columns-mobile button-height">
                    <select id="sel_TicketType" onchange="GenrateDatesSlots()" name="validation-select" class="select  full-width">
                    </select>
                </div>
                <div class="two-columns six-columns-mobile button-height">
                    <select id="Sel_Supplier" onchange="GenrateDatesSlots()" name="validation-select" class="select  full-width">
                    </select>

                </div>
                 <div class="two-columns six-columns-mobile button-height">
                    <select id="Sel_Language" onchange="GenrateDatesSlots()" name="validation-select" class="select  full-width">
                    </select>

                </div>
                <div class="two-columns six-columns-mobile button-height">
                    Pricing
                    <span class="button-group compact">
                        <label for="rdb_Cost" class="button green-active">
                            <input type="radio" name="radio-buttons" id="rdb_Cost" value="Cost" checked onclick="GenrateDatesSlots()">
                            Cost
                        </label>
                        <label for="rdb_Selling" class="button green-active">
                            <input type="radio" name="radio-buttons" id="rdb_Selling" value="Selling" onclick="GenrateDatesSlots()">
                            Selling
                        </label>
                        <label for="rdb_Offer" class="button green-active" style="display: none">
                            <input type="radio" name="radio-buttons" id="rdb_Offer" value="Offer" onclick="GenrateDatesSlots()">
                            Offer
                        </label>

                    </span>
                </div>
            </div>

            <div id="table-scroll" class="table-scroll">
                <div class="respTable" id="div_tbl_InvList">
                    <%--<table class="table responsive-table font11" id="tbl_Actlist" style="width:100%">
                    
                    </table>--%>
                </div>
            </div>
        </div>

    </section>
    <script>
        function openModal(content, buttons) {
            $.modal({
                title: 'Modal window',
                content: content,
                buttons: buttons,
                beforeContent: '<div class="carbon">',
                afterContent: '</div>',
                buttonsAlign: 'center',
                resizable: false
            });
        }
    </script>

    <div id="Inventoryfilter" class="<%--green-gradient--%>" style="max-height: 200px; display: none; overflow-y: scroll">
        <form id="frm_Inventory">
            <input type="hidden" id="txt_ActivityID" />
            <input type="hidden" id="txt_ActivityName" />
            <input type="hidden" id="txt_ActivityLocation" />
            <input type="hidden" id="txt_ActivityCity" />
            <input type="hidden" id="txt_ActivityCountry" />
            <input type="hidden" id="txt_RateType" />
            <input type="hidden" id="txt_TicketType" />

            <%-- <select class="select  auto-open  full-width validate[required]" id="sel_Inventory" onchange="AddInventory()">
                <option value="">-Select Inventory Type-</option>
                <option value="SightseeingInventory_freesale.aspx">Free Sale</option>
                <option value="SightseeingInventory_allocation.aspx">Allocation</option>
                <option value="SightseeingInventory_freepurchased.aspx">Allotment</option>
            </select>--%>
        </form>

        <p class="button-height" style="">
            <select class="select easy-multiple-selection anthracite-gradient check-list" id="sel_Market" multiple>
            </select>
        </p>

    </div>

</asp:Content>

