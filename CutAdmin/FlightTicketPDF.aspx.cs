﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CutAdmin
{
    public partial class FlightTicketPDF : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string InvoicePdf = HttpContext.Current.Session["Ticket"].ToString();
            HttpContext context = System.Web.HttpContext.Current;
            var htmlContent = String.Format(InvoicePdf);
            NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();
            pdfConverter.Size = NReco.PdfGenerator.PageSize.A3;
            pdfConverter.PdfToolPath = ConfigurationManager.AppSettings["rootPath"];
            var pdfBytes = pdfConverter.GeneratePdf(htmlContent);
            context.Response.ContentType = "application/pdf";
            context.Response.BinaryWrite(pdfBytes);
        }
    }
}