﻿<%@ Page Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="MapLocation.aspx.cs" Inherits="CutAdmin.MapLocation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/MapLocation.js?v=1.9"></script>
    <!-- Additional styles -->
    <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <img alt="" src="../loader1.gif" id="loders" width="128" height="128" style="position: fixed; z-index: 9999; left: 55%; margin-top: 8%; display: none">
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyDLvhAriXSRStAxraxjCp1GtClM4slLh-k"></script>
    <script src="Scripts/GoogleMap.js?V=1.0"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">


        <hgroup id="main-title" class="thin">
            <h1>Map City</h1>
            <hr />
        </hgroup>
        <div class="with-padding">

            <div class="columns">



                <div class="new-row one-columns"></div>
                <input type="hidden" id="hdnlatitude" />
                <input type="hidden" id="hdnlongitude" />
                <input type="hidden" id="hdnPlaceid" />
                <input type="hidden" id="hdCountryCode" />
                <input type="hidden" id="hdnCountryName" />
                <input type="hidden" id="hdnCode" />
                <input type="hidden" id="hdnlat" />
                <input type="hidden" id="hdnlong" />
                <input type="hidden" id="hdnplace" />


                <div class="three-columns">
                    <span class="text-left">Select Location:</span>
                    <br>
                    <input type="text" id="txtSource" class="input full-width pointer" autocomplete="off" />
                    <%--  <select id="selCountryMap" onchange="GetLocation(this.value)" class="full-width  select OfferType">
                    </select>--%>
                </div>
                <div class="three-columns">
                    <span class="text-left">Code:</span>
                    <br>
                    <input id="txtCountryCode" class="input full-width" type="text">
                    <label style="color: red; margin-top: 3px; display: none" id="lbl_txtCountryCode">
                        <b>* This field is required</b>
                    </label>
                </div>

                <div id="row1" style="width: 100%; padding-left: 20px">
                    <div class="columns">


                        <div class="three-columns" id="div_GTA1">
                            <span class="text-left">GTA:</span>
                         <%--   <input type="radio" id="GTAradio" name="radio" onclick="GetCode(1)" />--%>
                            <br>
                            <select id="selGTA1" class="select multiple-as-single easy-multiple-selection allow-empty check-list  full-width OfferType" multiple="multiple">
                            </select>
                        </div>

                        <div class="three-columns" id="div_HB1">
                            <span class="text-left">HB:</span>
                         <%--   <input type="radio" id="HBradio" name="radio" onclick="GetCode(2)" checked />--%>
                            <br>
                            <select id="selHB1" class="select multiple-as-single easy-multiple-selection allow-empty check-list  full-width OfferType" multiple="multiple">
                            </select>  <%--//onchange="BindCode(this.value)"--%>
                        </div>

                        <div class="three-columns" id="div_TBO1">
                            <span class="text-left">TBO:</span>
                          <%--  <input type="radio" id="TBOradio" name="radio" onclick="GetCode(3)" />--%>
                            <br>
                            <select id="selTBO1" class="select multiple-as-single easy-multiple-selection allow-empty check-list  full-width OfferType" multiple="multiple">
                            </select>
                        </div>


                    </div>
                </div>
                <div id="row2" style="width: 100%; padding-left: 20px">
                    <div class="columns">
                        <div class="three-columns" id="div_DOT1">
                            <span class="text-left">DOTW:</span>
                       <%--     <input type="radio" id="DOTWradio" name="radio" onclick="GetCode(4)" />--%>
                            <br>
                            <select id="selDOT1" class="select multiple-as-single easy-multiple-selection allow-empty check-list  full-width OfferType" multiple="multiple">
                            </select>
                        </div>

                        <div class="three-columns" id="div_MGH1">
                            <span class="text-left">Expedia:</span>
                          <%--  <input type="radio" id="MGHWradio" name="radio" onclick="GetCode(5)" />--%>
                            <br>
                            <select id="selMGH1" class="select multiple-as-single easy-multiple-selection allow-empty check-list  full-width OfferType" multiple="multiple">
                            </select>
                        </div>

                        <div class="three-columns" id="div_GRN1">
                            <span class="text-left">GRN:</span>
                            <%--<input type="radio" id="GRNradio" name="radio" onclick="GetCode(6)" />--%>
                            <br>
                            <select id="selGRN1" class="select multiple-as-single easy-multiple-selection allow-empty check-list  full-width OfferType" multiple="multiple">
                            </select>
                        </div>


                        <div class="columns">
                            <div class="one-columns" style="margin-top: 16px">
                                <button class="button glossy black-gradient" id="btn_save" onclick="SaveCityLocation()">Save</button>
                                <button class="button glossy black-gradient" id="btn_Update" onclick="UpdateCityLocation()" style="display: none">Update</button>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="respTable">

                    <table class="table responsive-table font11" id="tbl_AreaGroup">
                        <thead>
                            <tr>
                                <th scope="col" class="align-center">Sr.No.</th>
                                <th scope="col" class="align-center">Destination Code</th>
                                <th scope="col" class="align-center">City Name</th>
                                <th scope="col" class="align-center">Country</th>

                                <th scope="col" class="align-center">GTA</th>
                                <th scope="col" class="align-center">HB</th>
                                <th scope="col" class="align-center">DOTW</th>
                                <th scope="col" class="align-center">Expedia</th>
                                <th scope="col" class="align-center">TBO</th>
                                <th scope="col" class="align-center">GRN</th>
                                <th scope="col" class="align-center">Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>


    </section>
</asp:Content>
