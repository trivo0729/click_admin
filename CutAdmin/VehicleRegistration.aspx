﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="VehicleRegistration.aspx.cs" Inherits="CutAdmin.VehicleRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
    <script src="Scripts/VehicleRegistration.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Add Vehicle Registration</h1>
        </hgroup>
        <div class="with-padding">
            <form action="#" id="frm_vehicleReg" class="addQuotation">
                <hr>
                <div class="columns">
                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="Supplier">
                        <label>Supplier<span class="red">*</span> :</label><br />
                        <div class="full-width button-height" id="DivSupplier">
                            <select id="selSupplier" name="validation-select" class="select OfferType full-width validate[required]" data-prompt-position="topLeft" style="width: 180px" onchange="ShowFields()">
                                <option value="Own">Own </option>
                                <option value="Attached" selected="selected">Attached </option>
                                <option value="Supplier">Supplier</option>
                            </select>
                        </div>
                    </div>
                    <div class="three-columns  six-columns-mobile six-columns-tablet" id="VehicleModal">
                        <label>Vehicle Modal<span class="red">*</span> : </label>
                        <br />
                        <div class="full-width button-height" id="DivVehicleModal">
                            <select id="selVehicleModal" name="validation-select" class="select OfferType full-width validate[required]" data-prompt-position="topLeft">
                            </select>
                        </div>
                    </div>
                    <div class="three-columns six-columns-mobile" id="DivRdbDaysPrior">
                        <label>Registration Validity :</label><br />
                        <input type="radio" id="rdb_Numberofmonths" value="Normal" onclick="ValidityOption()" checked="checked" name="Rdb" />
                        <label id="lbl_Numberofmonths" for="rdb_Numberofmonths" class="text-left">Number of months</label>
                        <input type="radio" id="rdb_Lifetime" value="Normal" name="Rdb" onclick="ValidityOption()" />
                        <label id="lbl_Lifetime" for="rdb_Lifetime" class="text-left">Lifetime</label>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="DivNumberofmonths">
                        <label>Please enter Number of months</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_Numberofmonths" placeholder="Number of months" value="" class="input-unstyled full-width validate[required]" data-prompt-position="topLeft" type="text">
                        </div>
                    </div>
                </div>
                
                <div class="columns">
                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="InsuranceNo">
                        <label>Insurance No :</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_InsuranceNo" placeholder="Insurance No" value="" class="input-unstyled full-width validate[required]" data-prompt-position="topLeft" type="text">
                        </div>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="InsuranceDueDate">
                        <label>Insurance Due Date :</label><br />
                        <div class="input full-width">
                            <input name="datepicker" id="txt_InsuranceDueDate" placeholder="Date" value="" class="input-unstyled full-width validate[required]" data-prompt-position="topLeft" type="text">
                        </div>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="MaintenanceDue">
                        <label>Maintenance Due :</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_MaintenanceDue" placeholder="Maintenance Due" value="" class="input-unstyled full-width validate[required]" data-prompt-position="topLeft" type="text">
                        </div>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="DivAmenities">
                        <label for="select_Amenities" class="label">Amenities :</label>
                        <div class="full-width button-height" id="DivSelAmenities">
                            <select id="select_Amenities" multiple="multiple" class="full-width Amenities">
                            </select>
                        </div>
                    </div>
                </div>

                <div class="columns">
                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="ModalYear" style="display: none">
                        <label>Modal Year:</label><br />
                        <div class="full-width button-height" id="DivModalYear">
                            <select id="selModalYear" name="validation-select" class="select OfferType full-width validate[required]" data-prompt-position="topLeft">
                                <option value="-">-Select Modal Year-</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2019">2020</option>
                            </select>
                        </div>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="NoPlate" style="display: none">
                        <label>No. Plate :</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_NoPlate" placeholder="No. Plate" value="" class="input-unstyled full-width validate[required]" data-prompt-position="topLeft" type="text">
                        </div>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="RegistrationDate" style="display: none">
                        <label>Registration Date :</label><br />
                        <div class="input full-width">
                            <input name="datepicker" id="txt_RegistrationDate" placeholder="Date" value="" class="input-unstyled full-width validate[required]" data-prompt-position="topLeft" type="text">
                        </div>
                    </div>
                    <div class="three-columns twelve-columns-mobile six-columns-tablet" id="DivOtherSuppName" style="display: none;">
                        <label>Supplier Name</label><br />
                        <div class="input full-width">
                            <input name="prompt-value" id="txt_OtherSup" placeholder="Supplier Name" class="input-unstyled full-width validate[required]" data-prompt-position="topLeft" type="text">
                        </div>
                    </div>
                </div>

                <p class="text-right" style="text-align: right;">
                    <button type="button" id="btn_VehicleRegistration" class="button anthracite-gradient UpdateMarkup" onclick="AddVehicleRegistration();" value="Register Vehicle">Register Vehicle</button>
                    <button type="button" onclick="window.location.href = 'VehicleRegistrationDetails.aspx'" class="button anthracite-gradient">Back</button>
                </p>
            </form>
        </div>
    </section>
    <!-- End main content -->
</asp:Content>
