﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="sightseeingrates.aspx.cs" Inherits="CutAdmin.sightseeingrates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/styles/form.css?v=3">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
    <!-- DatePicker-->
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.css">
    <script src="Scripts/moments.js"></script>
    <script src="Scripts/addsightseeingrate.js?v=1.1"></script>
    <script src="Scripts/Exchange.js"></script>
    <script src="Scripts/Supplier.js"></script>
    <script>
        $(document).ready(function () {
            GetSupplier("Activity");
        });
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h3 class="font24"><b>Add rates for</b>
                <label id="ActivityName"></label>
            </h3>
            <hr />
        </hgroup>
        <div class="with-padding" id="div_Main">
            <form method="post" action="#" class="block margin-bottom wizard same-height" id="">
                <fieldset class="wizard-fieldset fields-list" id="wiz_main">
                    <legend class="legend">Main</legend>

                    <div class="field-block button-height" id="div_InventoryType">
                        <small class="input-info">select for which type of Inventory you want to feed rates</small>
                        <label for="market" class="label "><b>Inventory Type</b></label>
                        <select name="touroptions" class="select multiple-as-single easy-multiple-selection check-list expandable-list input" multiple style="width: 180px" id="sel_InventoryType" onchange="">
                            <option value="FreeSale" selected="selected">FreeSale</option>
                            <option value="Allocation">Allocation</option>
                            <option value="FreePurchased">Free Purchased</option>
                        </select>
                    </div>

                    <div class="field-block button-height">
                        <small class="input-info">Select supplier for which these rates are being added / updated</small>
                        <label for="supplier" class="label"><b>Supplier</b></label>
                        <select name="country" class="select expandable-list loading" style="width: 180px" id="Sel_Supplier">
                        </select>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">Select in which currency you wish to add / update rates</small>
                        <label for="currency" class="label"><b>Currency</b></label>
                        <select name="Exchange" class="select expandable-list loading" style="width: 180px" id="SelCurrency">
                        </select>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">Select one or more nationality / market for which rates are valid</small>
                        <label for="market" class="label "><b>Valid for market</b></label>
                        <%--Use dynamic dropdown--%>
                        <select name="country" class="select multiple-as-single easy-multiple-selection check-list expandable-list input <%--validate[required]--%>" id="sel_Country" multiple style="width: 180px">
                        </select>

                    </div>

                    <div class="field-block button-height">
                        <small class="input-info">Select any one tour type for which rates are being feeded</small>
                        <label for="touroption" class="label "><b>Tour Options</b></label>
                        <%--Use dynamic dropdown--%>
                        <select name="touroptions" class="select expandable-list validate[required]" style="width: 180px" id="touroptions" onchange="SetTransfer()">
                            <option value="TKT">Entry Ticket</option>
                            <option value="SIC">Sharing (SIC)</option>
                            <%--<option value="PVT">Private (PVT)</option>--%>
                        </select>
                    </div>

                    <div class="field-block button-height" id="div_Priorities">
                        <small class="input-info">select for which type of sightseeing / activity you want to feed rates</small>
                        <label for="market" class="label"><b>Tour / Activity Type</b></label>
                        <select name="tourtype" class="select multiple-as-single easy-multiple-selection check-list expandable-list input validate[required]" id="sel_TourType" onchange="SetTarrifRate()" multiple style="width: 180px">
                            <option selected="selected" value="" disabled="disabled">Select Tour/Activity Type</option>
                        </select>
                    </div>

                    <div class="field-block button-height">
                        <small class="input-info">If this tour had timing restiction enable slot time or keep it No</small>
                        <label for="market" class="label ">
                            <b>Slot</b>
                            <p class="reversed-switches">
                                <input type="checkbox" class="switch medium wide" name="btn_slot" onchange="SetSlot()" id="btn_slot" checked data-text-on="Enable" data-text-off="Disable">
                            </p>
                        </label>
                        <div id="SlotUI">
                        </div>
                        <div id="Blank" style="display:none">
                            <br />
                            <br />
                        </div>
                    </div>
                    <div class="field-block button-height" id="">
                        <small class="input-info">Select one or more languages for which rates are valid</small>
                        <label for="" class="label"><b>Languages</b></label>
                        <select name="language" class="select multiple-as-single easy-multiple-selection check-list expandable-list input validate[required]" id="sel_Language" onchange="" multiple style="width: 180px">
                        </select>
                    </div>

                </fieldset>
                <fieldset class="wizard-fieldset fields-list" id="wiz_rates">
                    <legend class="legend">Rates</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Rates for</b></label>
                        <div class="columns">
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Inventory Type</label>
                                <p class="black lbl_Inventory"></p>
                            </div>
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Supplier</label>
                                <p class="black lbl_supplier"></p>
                            </div>
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Tour Option</label>
                                <p class="black lbl_TourOptions"></p>
                            </div>

                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Slot Timing</label>
                                <div class="div_SlotMode">
                                    <%--<label class="black SlotMode">Full day</label>--%>
                                </div>
                            </div>
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Market</label>
                                <p class="black lbl_Market"></p>
                            </div>
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Language</label>
                                <p class="black lbl_Language"></p>
                            </div>
                        </div>

                    </div>

                    <div class="field-block button-height">
                        <small class="input-info">You may select more than one date range to feed same rates in the different dates</small>
                        <label for="market" class="label "><b>Validity</b></label>
                        <div id="div_GNDate">
                        </div>
                    </div>

                    <%--<div class="field-block button-height">
                        <small class="input-info">Here define timing / slots for which these rates are valid</small>
                        <label for="market" class="label "><b>Slots</b></label>
                        <div id="SlotUI">
                        </div>
                    </div>--%>

                    <div class="field-block button-height" style="padding-left: inherit;">
                        <!-- Activity Type wise rates -->
                        <div class="side-tabs same-height tabs-active" style="height: 57px;">
                            <!-- Tabs -->
                            <ul class="tabs" id="Ul">
                                <%-- <li class="active" active="li_gn" id="">
                                    <a class="active strong align-center" href="#tkt-tab">Entry Ticket</a>
                                </li>
                                <li class="active" id="">
                                    <a class="strong align-center" href="#sic-tab">Sharing (SIC)</a>
                                </li>
                                <li class="active" id="">
                                    <a class="strong align-center" href="#pvt-tab">Private (PVT)</a>
                                </li>--%>
                            </ul>
                            <div class="tabs-content" style="height: auto !important" id="div_tab">
                            </div>
                        </div>
                    </div>
                    <!-- Show only when SIC Tab is active -->
                    <div class="field-block button-height" id="div_pick_drop" style="display: none">
                        <small class="input-info">You may define more than one pickup & drop areas / location & timing </small>
                        <label for="market" class="label "><b>Transfer Details</b></label>
                        <div id="div_transferdetails">
                            <%--<div class="div_transfers">
                                <div class="columns">
                                    <div class="four-columns twelve-columns-mobile seven-columns-tablet small-margin-bottom">
                                        <input type="text" class="input" id="txt_pickup" value="" size="22" placeholder="Pickup from">
                                    </div>
                                    <div class="four-columns twelve-columns-mobile seven-columns-tablet small-margin-bottom">
                                        <div class="columns">
                                            <div class="six-columns small-margin-bottom">
                                                <span class="number input margin-right">
                                                    <label style="width: 20px" for="roomtariff" class="button field-drop-input grey-gradient">Hrs</label>
                                                    <button type="button" class="button number-down">-</button>
                                                    <input type="text" value="1" id="txt_pickup_hrs" size="3" class="input-unstyled" data-number-options='{"min":1,"max":24,"increment":1.0,"shiftIncrement":5,"precision":0.25}'>
                                                    <button type="button" class="button number-up">+</button>
                                                </span>
                                            </div>
                                            <div class="six-columns small-margin-bottom">
                                                <span class="number input margin-right">
                                                    <label style="width: 20px" for="roomtariff" class="button field-drop-input grey-gradient">Min</label>
                                                    <button type="button" class="button number-down">-</button>
                                                    <input type="text" value="00" id="txt_pickup_min" size="3" class="input-unstyled" data-number-options='{"min":0,"max":50,"increment":10,"shiftIncrement":5,"precision":1}'>
                                                    <button type="button" class="button number-up">+</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="columns">
                                    <div class="four-columns twelve-columns-mobile seven-columns-tablet">
                                        <input type="text" class="input" id="txt_drop" value="" size="22" placeholder="Drop to">
                                    </div>
                                    <div class="four-columns twelve-columns-mobile seven-columns-tablet">
                                        <div class="columns">
                                            <div class="six-columns small-margin-bottom">
                                                <span class="number input margin-right">
                                                    <label style="width: 20px" for="roomtariff" class="button field-drop-input grey-gradient">Hrs</label>
                                                    <button type="button" class="button number-down">-</button>
                                                    <input type="text" value="1" id="txt_drop_hrs" size="3" class="input-unstyled" data-number-options='{"min":1,"max":24,"increment":1.0,"shiftIncrement":5,"precision":0.25}'>
                                                    <button type="button" class="button number-up">+</button>
                                                </span>
                                            </div>
                                            <div class="six-columns small-margin-bottom">
                                                <span class="number input margin-right">
                                                    <label style="width: 20px" for="roomtariff" class="button field-drop-input grey-gradient">Min</label>
                                                    <button type="button" class="button number-down">-</button>
                                                    <input type="text" value="00" id="txt_drop_min" size="3" class="input-unstyled" data-number-options='{"min":0,"max":50,"increment":10,"shiftIncrement":5,"precision":1}'>
                                                    <button type="button" class="button number-up">+</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="one-column">
                                        <span class="mid-margin-left icon-size2 icon-plus-round icon-black" onclick="" id=""></span>
                                    </div>
                                </div>
                            </div>--%>
                        </div>
                    </div>

                </fieldset>
                <fieldset class="wizard-fieldset fields-list" id="wiz_review">
                    <legend class="legend">Conditions</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Rates for</b></label>
                        <div class="columns">
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Inventory Type</label>
                                <p class="black lbl_Inventory"></p>
                            </div>
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Supplier</label>
                                <p class="black lbl_supplier"></p>
                            </div>
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Market</label>
                                <p class="black selected-inclusion lbl_Market"></p>
                            </div>
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Validity</label>
                                <div class="div_DatesValidity">
                                </div>
                            </div>
                           <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Language</label>
                                <p class="black lbl_Language"></p>
                            </div>
                        </div>
                    </div>

                    <div class="field-block button-height">
                        <label class="label"><b>Canellation Policy</b></label>
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile no-margin-bottom">
                                <label class="">Is this tariff refundable? </label>
                                <input type="checkbox" class="switch medium" id="chk_Cancel" data-text-on="YES" data-text-off="NO" onchange="ShowCancel(this)">
                            </div>
                            <div class="eight-columns twelve-columns-mobile" id="cnlpolicy" style="display: none">

                                <input type="radio" class="radio " id="percentage" value="1" name="chk_Charge" checked="checked" onchange="GetChargeCond(this, 'percentageinput')">
                                <label for="percentage">Charges in percentage</label>

                                <input type="radio" class="radio mid-margin-left" id="amount" value="2" name="chk_Charge" onchange="GetChargeCond(this, 'amountinput')">
                                <label for="amount">Charges in amount</label>

                            </div>
                        </div>
                    </div>
                    <%-- Charges in percentage --%>
                    <div class="field-drop button-height black-inputs" id="percentageinput" style="display: none">
                        <small class="input-info silver">Specify the charges in percentage in the event of cancellation</small>
                        <label for="" class="label"><b>Charges in percentage (%)</b></label>
                        <div class="columns">
                            <div class="three-columns twelve-columns-mobile six-columns-tablet small-margin-bottom">
                                <select class="validate[required] select expandable-list anthracite-gradient" id="sel_ChargePer">
                                    <option value="">Applicable Charges</option>
                                    <option value="0%">No charges will apply</option>
                                    <option value="10%">10% will be charged</option>
                                    <option value="20%">20% will be charged</option>
                                    <option value="30%">30% will be charged</option>
                                    <option value="40%">40% will be charged</option>
                                    <option value="50%">50% will be charged</option>
                                    <option value="60%">60% will be charged</option>
                                    <option value="70%">70% will be charged</option>
                                    <option value="80%">80% will be charged</option>
                                    <option value="90%">90% will be charged</option>
                                    <option value="100%">100% will be charged</option>
                                </select>
                            </div>
                            <div class="four-columns twelve-columns-mobile six-columns-tablet small-margin-bottom">
                                <select class="select expandable-list anthracite-gradient validate[required]" tabindex="0" id="sel_DayPer">
                                    <option value="">how long before service? </option>
                                    <option value="4hrs">until 4 Hrs before service time</option>
                                    <option value="6hrs">until 6 Hrs before service time</option>
                                    <option value="12hrs">until 12 Hrs before service time</option>
                                    <option value="18hrs">until 18 Hrs before service time</option>
                                    <option value="1days">until 1 day before service date</option>
                                    <option value="1days">until 1 day before service date</option>
                                    <option value="2days">until 2 days before service date</option>
                                    <option value="3days">until 3 days before service date</option>
                                    <option value="5days">until 5 days before service date</option>
                                    <option value="10days">until 10 days before service date</option>
                                    <option value="15days">until 15 days before service date</option>
                                    <option value="20days">until 20 days before service date</option>
                                    <option value="25days">until 25 days before service date</option>
                                    <option value="30days">until 30 days before service date</option>
                                    <option value="45days">until 45 days before service date</option>
                                    <option value="60days">until 60 days before service date</option>
                                </select>
                            </div>
                            <div class="four-columns twelve-columns-mobile six-columns-tablet small-margin-bottom">
                                <select class="select expandable-list anthracite-gradient validate[required]" tabindex="0" id="sel_NoShowPer">
                                    <option value="" class="select-value">in case of No Show </option>
                                    <option value="100%">100% charged in case of No Show</option>
                                    <option value="Same">Same as cancellation charge</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <%-- Charges in amount --%>
                    <div class="field-drop button-height black-inputs" id="amountinput" style="display: none;">
                        <small class="input-info silver">Specify the charges in amount, in the event of cancellation</small>
                        <label for="" class="label"><b>Charges in amount</b></label>
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">
                                <span class="input">
                                    <label style="width: 30px" for="roomtariff" class="button green-gradient sCurrency"></label>
                                    <input style="width: 50px" type="text" name="RoomTariff" id="cancellationamount" class="input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="12,345.00">
                                </span>
                                <label class="white font13 strong">will be charged</label>
                            </div>
                            <div class="four-columns twelve-columns-mobile six-columns-tablet small-margin-bottom">
                                <select class="select expandable-list anthracite-gradient  validate[required]" tabindex="0" id="sel_DayAmt">
                                    <option value="">how long before service? </option>
                                    <option value="4hrs">until 4 Hrs before service time</option>
                                    <option value="6hrs">until 6 Hrs before service time</option>
                                    <option value="12hrs">until 12 Hrs before service time</option>
                                    <option value="18hrs">until 18 Hrs before service time</option>
                                    <option value="1days">until 1 day before service date</option>
                                    <option value="1days">until 1 day before service date</option>
                                    <option value="2days">until 2 days before service date</option>
                                    <option value="3days">until 3 days before service date</option>
                                    <option value="5days">until 5 days before service date</option>
                                    <option value="10days">until 10 days before service date</option>
                                    <option value="15days">until 15 days before service date</option>
                                    <option value="20days">until 20 days before service date</option>
                                    <option value="25days">until 25 days before service date</option>
                                    <option value="30days">until 30 days before service date</option>
                                    <option value="45days">until 45 days before service date</option>
                                    <option value="60days">until 60 days before service date</option>
                                </select>
                            </div>
                            <div class="three-columns twelve-columns-mobile six-columns-tablet small-margin-bottom">
                                <select class="select expandable-list anthracite-gradient  validate[required]" id="sel_NoShowAmt">
                                    <option value="" class="select-value">in case of No Show </option>
                                    <option value="100%">100% charged in case of No Show</option>
                                    <option value="same">Same as cancellation charge</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <br />
                    <hr />


                    <div class="field-block button-height">
                        <label class="label"><b>Inclusion & Exclusion</b></label>
                        <div class="columns">
                            <div class="five-columns">
                                <small class="input-info">Mention what are the <i class="red">inclusions</i> in the rate</small>
                                <div class="columns">
                                    <div class="ten-columns">
                                        <input type="text" class="input full-width mid-margin-bottom" id="txt_Inclusions" placeholder="e.g: Entry Ticket">
                                    </div>
                                    <div class="two-columns">
                                        <button type="button" onclick="InclusionsAdd()" style="cursor: pointer"><span class="icon-plus" title="Click Here To Add"></span></button>
                                    </div>
                                </div>
                                <div id="div_inclusions" class="input full-width validate[required]" style="height: 75px; overflow: auto">
                                </div>

                            </div>
                            <div class="five-columns">
                                <small class="input-info">Mention what are the <i class="red">Exclusions</i> in the rate</small>
                                <div class="columns">
                                    <div class="ten-columns">
                                        <input type="text" class="input full-width mid-margin-bottom" id="txt_Exclusions" placeholder="e.g: Entry Ticket">
                                    </div>
                                    <div class="two-columns">
                                        <button type="button" onclick="ExclusionsAdd()" style="cursor: pointer"><span class="icon-plus" title="Click Here To Add"></span></button>
                                    </div>
                                </div>
                                <div id="div_exclusions" class="input full-width validate[required]" style="height: 75px; overflow: auto">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">If there are any conditions to book this Tour / Sightseeing</small>
                        <label class="label"><b>Min Pax Conditions</b></label>
                        <%--<div class="six-columns small-margin-bottom">--%>
                        <label>To book this tour min </label>
                        <span class="number input small-margin-right">
                            <button type="button" class="button number-down">-</button>
                            <input type="text" value="1" size="3" class="input-unstyled" id="txt_MinPax" data-number-options='{"min":1,"shiftIncrement":5}'>
                            <button type="button" class="button number-up">+</button>
                        </span>
                        <label>pax & max </label>
                        <span class="number input small-margin-right">
                            <button type="button" class="button number-down">-</button>
                            <input type="text" value="1" size="3" class="input-unstyled" id="txt_Max_Pax" data-number-options='{"min":1,"shiftIncrement":5}'>
                            <button type="button" class="button number-up">+</button>
                        </span>
                        <label>pax are required </label>
                        <%--</div>--%>
                    </div>

                    <div class="field-block button-height">
                        <small class="input-info">Any important note regarding to this rates should mention here</small>
                        <label class="label"><b>Tariff Note</b></label>
                        <p>
                            <textarea class="input full-width autoexpanding validate[required]" data-prompt-position="topLeft" id="txt_TourNote"></textarea>
                        </p>
                    </div>
                </fieldset>
                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Review</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Main Details</b></label>
                        <div class="columns">
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Inventory Type</label>
                                <p class="black lbl_Inventory"></p>
                            </div>
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Supplier</label>
                                <p class="black selected-inclusion lbl_supplier"></p>
                            </div>
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Market</label>
                                <p class="black selected-inclusion lbl_Market"></p>
                            </div>
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Validity</label>
                                <div class="div_DatesValidity">
                                </div>
                            </div>
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Slot Timing</label>
                                <div class="div_SlotMode">                                 
                                </div>
                            </div>
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom">
                                <label class="green strong">Language</label>
                                <p class="black lbl_Language"></p>
                            </div>
                        </div>
                    </div>
                    <div class="field-block button-height">
                        <label class="label"><b class="lbl_TourOptions"></b></label>
                        <div id="div_review">
                        </div>
                    </div>
                     <div class="field-block button-height" id="div_cancellation">
                        
                    </div>
                    <br />
                    <hr />
                    <button type="button" id="btn_Save" class="button glossy anthracite-gradient float-right" onclick="SaveSightseeingRates()">Save</button>
                </fieldset>
            </form>
        </div>
    </section>

    <script>

        function showdaywise(ActType) {
            debugger
            setTimeout(function () {
                $('.tab-active').refreshTabs();
            }, 200)
            var x = document.getElementById("ro-daywise" + ActType);
            if (x.style.display === "none") {
                x.style.display = "block";
                $("#chk_day" + ActType).prop("checked", true);
            } else {
                x.style.display = "none";
                $("#chk_day" + ActType).prop("checked", false);
            }

        }
        function showdaywiseMin(ActType) {
            debugger

            setTimeout(function () {
                $('#div_' + ActType + '').refreshTabs();
            }, 200)
            var x = document.getElementById("ro-daywiseMin" + ActType);
            if (x.style.display === "none") {
                x.style.display = "block";
                $("#chk_dayMin" + ActType).prop("checked", true);
            } else {
                x.style.display = "none";
                $("#chk_dayMin" + ActType).prop("checked", false);
            }

        }
    </script>
    <script>
        function genminsell(ActType) {
            setTimeout(function () {
                $('.tab-active').refreshTabs();
            }, 100)
            var x = document.getElementById("genminsell" + ActType);
            if (x.style.display === "none") {
                x.style.display = "block";
                $('.sMinCellDate').show();
                $("#ro-daywiseMin" + ActType).hide();
                $("#chk_Min" + ActType).prop("checked", true);
            } else {
                x.style.display = "none";
                $('.sMinCellDate').hide();
                $("#ro-daywiseMin" + ActType).hide();
                $("#chk_Min" + ActType).prop("checked", false);
            }
        }
    </script>
</asp:Content>
