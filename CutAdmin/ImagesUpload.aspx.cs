﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CutAdmin
{
    public partial class ImagesUpload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HiddenField2.Value = Request.QueryString["Image"].ToString();
            FileUpload0.Attributes["onchange"] = "UploadFile(this)";
            FileUpload1.Attributes["onchange"] = "UploadPassport(this)";
            FileUpload2.Attributes["onchange"] = "UploadPassportLast(this)";
            FileUpload3.Attributes["onchange"] = "UploadSupportingDocument1(this)";
            FileUpload5.Attributes["onchange"] = "UploadSupportingDocument2(this)";
            FileUpload4.Attributes["onchange"] = "UploadConfirmTicketCopy(this)";
        }

        protected void btn_Uploadfile_Click(object sender, EventArgs e)
        {
            string Serverpath = Server.MapPath("~/AgencyLogos/");
            string[] imageArray = new string[5];
            string[] sfinalArray = new string[5];
            string sFileNameArray = "";
            string ImageCode = HiddenField2.Value;

            if (FileUpload0.HasFile)
            {

                string[] sFileExtension = FileUpload0.FileName.Split('.');
                string sFileName = ImageCode + "_1" + "." + "jpg";
                string sFolderName = Serverpath + "\\";
                string PhysicalPath = sFolderName + "\\" + sFileName;
                sfinalArray[0] = ImageCode + "_1" + "." + "jpg";
                if (!Directory.Exists(sFolderName))
                {
                    Directory.CreateDirectory(sFolderName);
                }
                else
                {
                    string[] files = Directory.GetFiles(sFolderName);
                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            File.Delete(fileName);
                        }
                    }
                }
                FileUpload0.SaveAs(PhysicalPath);
            }
            if (FileUpload1.HasFile)
            {

                string[] sFileExtension = FileUpload1.FileName.Split('.');
                string sFileName = ImageCode + "_2" + "." + "jpg"; ;
                string sFolderName = Serverpath + "\\";
                string PhysicalPath = sFolderName + "\\" + sFileName;
                sfinalArray[1] = ImageCode + "_2" + "." + "jpg"; ;
                if (!Directory.Exists(sFolderName))
                {
                    Directory.CreateDirectory(sFolderName);
                }
                else
                {
                    string[] files = Directory.GetFiles(sFolderName);
                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            File.Delete(fileName);
                        }
                    }
                }
                FileUpload1.SaveAs(PhysicalPath);
            }
            if (FileUpload2.HasFile)
            {

                string[] sFileExtension = FileUpload2.FileName.Split('.');
                string sFileName = ImageCode + "_3" + "." + "jpg"; ;
                string sFolderName = Serverpath + "\\";
                string PhysicalPath = sFolderName + "\\" + sFileName;
                sfinalArray[2] = ImageCode + "_3" + "." + "jpg"; ;
                if (!Directory.Exists(sFolderName))
                {
                    Directory.CreateDirectory(sFolderName);
                }
                else
                {
                    string[] files = Directory.GetFiles(sFolderName);
                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            File.Delete(fileName);
                        }
                    }
                }
                FileUpload2.SaveAs(PhysicalPath);
            }
            if (FileUpload3.HasFile)
            {

                string[] sFileExtension = FileUpload3.FileName.Split('.');
                string sFileName = ImageCode + "_4" + "." + "jpg"; ;
                string sFolderName = Serverpath + "\\";
                string PhysicalPath = sFolderName + "\\" + sFileName;
                sfinalArray[3] = ImageCode + "_4" + "." + "jpg"; ;
                if (!Directory.Exists(sFolderName))
                {
                    Directory.CreateDirectory(sFolderName);
                }
                else
                {
                    string[] files = Directory.GetFiles(sFolderName);
                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            File.Delete(fileName);
                        }
                    }
                }
                FileUpload3.SaveAs(PhysicalPath);
            }
            if (FileUpload4.HasFile)
            {

                string[] sFileExtension = FileUpload4.FileName.Split('.');
                string sFileName = "Image_4" + "." + "jpg"; ;
                string sFolderName = Serverpath + "\\";
                string PhysicalPath = sFolderName + "\\" + sFileName;
                sfinalArray[4] = "Image_4" + "." + "jpg"; ;
                if (!Directory.Exists(sFolderName))
                {
                    Directory.CreateDirectory(sFolderName);
                }
                else
                {
                    string[] files = Directory.GetFiles(sFolderName);
                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            File.Delete(fileName);
                        }
                    }
                }
                FileUpload4.SaveAs(PhysicalPath);
            }
            foreach (string s in sfinalArray)
            {
                if (s != "" && s != null)
                {
                    sFileNameArray += s + "^_^";
                }
            }
            Response.Redirect("ImagesUpload.aspx?Image=" + ImageCode);
            //sFileNameArray = sFileNameArray.Substring(0, sFileNameArray.LastIndexOf("^_^"));
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('Photo Uploaded successfully');window.location='VisaDetails.aspx'", true);

        }
        protected void ValidateFileSize(object source, ServerValidateEventArgs e)
        {
            if (FileUpload1.PostedFile.ContentLength < 1000000) // 1024*KB of file size
            {
                e.IsValid = true;
            }
            else
            {
                //CustomValidator3.ErrorMessage = "Please upload files with less than 32KB";
                e.IsValid = false;
            }
        }
        protected void Upload(object sender, EventArgs e)
        {
            try
            {
                //HiddenField2.Value = Request.QueryString["Image"].ToString();
                string ImageCode = Request.QueryString["Image"].ToString(); ;
                string FileName = FileUpload0.FileName;
                string Serverpath = Server.MapPath("~/VisaImages/");
                string message = "Please upload files with less than 32KB";
                string Url = "Upload_Images.aspx?Image=" + ImageCode;
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "');";
                script += "window.location = '";
                script += Url;
                script += "'; }";
                if (IsPostBack && FileUpload0.PostedFile != null && FileUpload0.PostedFile.ContentLength < 1000000)
                {

                    string sFileName = ImageCode + "_1" + "." + "jpg";
                    string sFolderName = Serverpath;
                    string PhysicalPath = sFolderName + sFileName;
                    if (!Directory.Exists(sFolderName))
                    {
                        Directory.CreateDirectory(sFolderName);
                    }
                    else
                    {
                        string[] files = Directory.GetFiles(sFolderName);
                        foreach (string fileName in files)
                        {
                            if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                            {
                                File.Delete(fileName);
                            }
                        }
                    }
                    FileUpload0.SaveAs(PhysicalPath);
                    //lblMessage.Visible = true;
                    if (lblMessage.Visible == true)
                    {
                        lblMessage.Visible = false;
                    }
                    lblMessage.Visible = true;
                    if (FileUpload0.PostedFile.ContentLength < 34000)
                    {
                        lblMessage.Text = "Color Passport Photograph Uploaded.";
                        lblMessage.ForeColor = System.Drawing.Color.Green;
                        DBHelper.DBReturnCode retCode = CutAdmin.DataLayer.VisaDetailsManager.UpdateUploadStatus(ImageCode, 1);
                    }
                    else
                    {
                        //lblMessage.Text = "Please Crop Image files with less than 34KB";
                        //lblMessage.ForeColor = System.Drawing.Color.Red;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenDilog('CropedPass');", true);

                    }

                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    lblMessage.Text = "Please upload files with less than 32KB!!";
                    //ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);
                }

            }
            catch
            {

            }
        }
        protected void UploadPassport(object sender, EventArgs e)
        {
            string ImageCode = Request.QueryString["Image"].ToString(); ;
            string Serverpath = Server.MapPath("~/VisaImages/");
            string message = "Please upload files with less than 32KB";
            string Url = "Upload_Images.aspx?Image=" + ImageCode;
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "');";
            script += "window.location = '";
            script += Url;
            script += "'; }";
            if (IsPostBack && FileUpload1.PostedFile != null && FileUpload1.PostedFile.ContentLength < 1000000)
            {

                string sFileName = ImageCode + "_2" + "." + "jpg";
                string sFolderName = Serverpath;
                string PhysicalPath = sFolderName + sFileName;
                if (!Directory.Exists(sFolderName))
                {
                    Directory.CreateDirectory(sFolderName);
                }
                else
                {
                    string[] files = Directory.GetFiles(sFolderName);
                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            File.Delete(fileName);
                        }
                    }
                }
                FileUpload1.SaveAs(PhysicalPath);
                if (Label1.Visible == true)
                {
                    Label1.Visible = false;
                }
                Label1.Visible = true;
                Label1.Text = "Passport Fist Page Uploded.";
                Label1.ForeColor = System.Drawing.Color.Green;
                if (FileUpload1.PostedFile.ContentLength < 34000)
                {
                    lblMessage.Text = "Color Passport Photograph Uploaded.";
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                    DBHelper.DBReturnCode retCode = CutAdmin.DataLayer.VisaDetailsManager.UpdateUploadStatus(ImageCode, 2);
                }
                else
                {
                    //lblMessage.Text = "Please Crop Image files with less than 34KB";
                    //lblMessage.ForeColor = System.Drawing.Color.Red;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenDilog('FistPage');", true);

                }
                //DBHelper.DBReturnCode retCode = VisaDetailsManager.UpdateUploadStatus(ImageCode, 2);
                //Label1.Visible = true;
                //Response.Redirect("Upload_Images.aspx?Image=" + ImageCode);
            }
            else
            {
                Label1.Visible = true;
                Label1.ForeColor = System.Drawing.Color.Red;
                Label1.Text = "Please upload files with less than 32KB!!";
                //ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);
            }
        }
        protected void UploadPassportLast(object sender, EventArgs e)
        {
            string ImageCode = Request.QueryString["Image"].ToString(); ;
            string Serverpath = Server.MapPath("~/VisaImages/");
            string message = "Please upload files with less than 32KB";
            string Url = "Upload_Images.aspx?Image=" + ImageCode;
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "');";
            script += "window.location = '";
            script += Url;
            script += "'; }";
            if (IsPostBack && FileUpload2.PostedFile != null && FileUpload2.PostedFile.ContentLength < 1000000)
            {

                string sFileName = ImageCode + "_3" + "." + "jpg";
                string sFolderName = Serverpath;
                string PhysicalPath = sFolderName + sFileName;
                if (!Directory.Exists(sFolderName))
                {
                    Directory.CreateDirectory(sFolderName);
                }
                else
                {
                    string[] files = Directory.GetFiles(sFolderName);
                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            File.Delete(fileName);
                        }
                    }
                }
                FileUpload2.SaveAs(PhysicalPath);
                //Label2.Visible = true;
                if (Label2.Visible == true)
                {
                    Label2.Visible = false;
                }
                Label2.Visible = true;
                Label2.Text = "Passport Last Page Uploaded.";
                Label2.ForeColor = System.Drawing.Color.Green;
                //DBHelper.DBReturnCode retCode = VisaDetailsManager.UpdateUploadStatus(ImageCode, 3);
                //Response.Redirect("Upload_Images.aspx?Image=" + ImageCode);
                if (FileUpload2.PostedFile.ContentLength < 34000)
                {
                    lblMessage.Text = "Color Passport Photograph Uploaded.";
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                    DBHelper.DBReturnCode retCode = CutAdmin.DataLayer.VisaDetailsManager.UpdateUploadStatus(ImageCode, 3);
                }
                else
                {
                    //lblMessage.Text = "Please Crop Image files with less than 34KB";
                    //lblMessage.ForeColor = System.Drawing.Color.Red;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenDilog('LastPage');", true);

                }

            }
            else
            {
                Label2.Visible = true;
                Label2.ForeColor = System.Drawing.Color.Red;
                Label2.Text = "Please upload files with less than 32KB!!";
                //ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);
            }
        }
        protected void UploadSupportingDocument1(object sender, EventArgs e)
        {
            string ImageCode = Request.QueryString["Image"].ToString(); ;
            string Serverpath = Server.MapPath("~/VisaImages/");
            string message = "Please upload files with less than 32KB";
            string Url = "Upload_Images.aspx?Image=" + ImageCode;
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "');";
            script += "window.location = '";
            script += Url;
            script += "'; }";
            if (IsPostBack && FileUpload3.PostedFile != null && FileUpload3.PostedFile.ContentLength < 1000000)
            {

                string sFileName = ImageCode + "_4" + "." + "jpg";
                string sFolderName = Serverpath;
                string PhysicalPath = sFolderName + sFileName;
                if (!Directory.Exists(sFolderName))
                {
                    Directory.CreateDirectory(sFolderName);
                }
                else
                {
                    string[] files = Directory.GetFiles(sFolderName);
                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            File.Delete(fileName);
                        }
                    }
                }
                FileUpload3.SaveAs(PhysicalPath);
                //Label3.Visible = true;
                if (Label3.Visible == true)
                {
                    Label3.Visible = false;
                }
                Label3.Visible = true;
                Label3.Text = "Supporting Document Uploaded.";
                Label3.ForeColor = System.Drawing.Color.Green;
                //Response.Redirect("Upload_Images.aspx?Image=" + ImageCode);
                if (FileUpload3.PostedFile.ContentLength < 34000)
                {
                    lblMessage.Text = "Color Passport Photograph Uploaded.";
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                    DBHelper.DBReturnCode retCode = CutAdmin.DataLayer.VisaDetailsManager.UpdateUploadStatus(ImageCode, 4);
                }
                else
                {
                    //lblMessage.Text = "Please Crop Image files with less than 34KB";
                    //lblMessage.ForeColor = System.Drawing.Color.Red;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenDilog('Supporting4');", true);

                }

            }
            else
            {
                Label3.Visible = true;
                Label3.ForeColor = System.Drawing.Color.Red;
                Label3.Text = "Please upload files with less than 32KB!!";
                //ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);
            }
        }
        protected void UploadSupportingDocument2(object sender, EventArgs e)
        {
            string ImageCode = Request.QueryString["Image"].ToString(); ;
            string Serverpath = Server.MapPath("~/VisaImages/");
            string message = "Please upload files with less than 32KB";
            string Url = "Upload_Images.aspx?Image=" + ImageCode;
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "');";
            script += "window.location = '";
            script += Url;
            script += "'; }";
            if (IsPostBack && FileUpload5.PostedFile != null && FileUpload5.PostedFile.ContentLength < 1000000)
            {

                string sFileName = ImageCode + "_5" + "." + "jpg";
                string sFolderName = Serverpath;
                string PhysicalPath = sFolderName + sFileName;
                if (!Directory.Exists(sFolderName))
                {
                    Directory.CreateDirectory(sFolderName);
                }
                else
                {
                    string[] files = Directory.GetFiles(sFolderName);
                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            File.Delete(fileName);
                        }
                    }
                }
                FileUpload5.SaveAs(PhysicalPath);
                //Label4.Visible = true;
                if (Label4.Visible == true)
                {
                    Label4.Visible = false;
                }
                Label4.Visible = true;
                Label4.Text = "Supporting Document Uploaded.";
                Label4.ForeColor = System.Drawing.Color.Green;
                //Response.Redirect("Upload_Images.aspx?Image=" + ImageCode);
                if (FileUpload3.PostedFile.ContentLength < 34000)
                {
                    lblMessage.Text = "Color Passport Photograph Uploaded.";
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                    DBHelper.DBReturnCode retCode = CutAdmin.DataLayer.VisaDetailsManager.UpdateUploadStatus(ImageCode, 5);
                }
                else
                {
                    //lblMessage.Text = "Please Crop Image files with less than 34KB";
                    //lblMessage.ForeColor = System.Drawing.Color.Red;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenDilog('Supporting2');", true);

                }
            }
            else
            {
                Label4.Visible = true;
                Label4.ForeColor = System.Drawing.Color.Red;
                Label4.Text = "Please upload files with less than 32KB!!";
                //ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);
            }
        }
        protected void UploadConfirmTicketCopy(object sender, EventArgs e)
        {
            //string ImageCode = HiddenField2.Value;
            //string Serverpath = Server.MapPath("~/VisaImages/");
            //string message = "Please upload files with less than 32KB";
            //string Url = "Upload_Images.aspx?Image=" + ImageCode;
            //string script = "window.onload = function(){ alert('";
            //script += message;
            //script += "');";
            //script += "window.location = '";
            //script += Url;
            //script += "'; }";
            //if (IsPostBack && FileUpload4.PostedFile != null && FileUpload4.PostedFile.ContentLength < 1000000)
            //{

            //    string sFileName = ImageCode + "_6" + "." + "jpg";
            //    string sFolderName = Serverpath;
            //    string PhysicalPath = sFolderName + sFileName;
            //    if (!Directory.Exists(sFolderName))
            //    {
            //        Directory.CreateDirectory(sFolderName);
            //    }
            //    else
            //    {
            //        string[] files = Directory.GetFiles(sFolderName);
            //        foreach (string fileName in files)
            //        {
            //            if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
            //            {
            //                File.Delete(fileName);
            //            }
            //        }
            //    }
            //    FileUpload4.SaveAs(PhysicalPath);
            //    Label5.Visible = true;
            //    Response.Redirect("Upload_Images.aspx?Image=" + ImageCode);
            //}
            //else
            //{
            //    ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);
            //}
            string ImageCode = Request.QueryString["Image"].ToString(); ;
            string Serverpath = Server.MapPath("~/VisaImages/");
            string message = "Please upload files with less than 32KB";
            string message1 = "Visa Copy Uploded";
            string Url = "VisaDetails.aspx?Image=" + ImageCode;
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "');";
            script += "window.location = '";
            script += Url;
            script += "'; }";
            string script1 = "window.onload = function(){ alert('";
            script1 += message1;
            script1 += "');";
            script1 += "window.location = '";
            script1 += "'; }";
            string[] sFileExtension = FileUpload4.FileName.Split('.');
            string sFileName = ImageCode + "_6" + "." + sFileExtension[1];
            string Filename = FileUpload4.FileName;
            string[] ext = Filename.Split('.');
            string type = ext[1];
            if (type == "pdf" || type == "docx")
            {
                if (IsPostBack && FileUpload4.PostedFile != null && FileUpload4.PostedFile.ContentLength < 100000000)
                {

                    //sFileName = ImageCode + "_6" + "." + "jpg";
                    string sFolderName = Serverpath;
                    string PhysicalPath = sFolderName + sFileName;
                    if (!Directory.Exists(sFolderName))
                    {
                        Directory.CreateDirectory(sFolderName);
                    }
                    else
                    {
                        string[] files = Directory.GetFiles(sFolderName);
                        foreach (string fileName in files)
                        {
                            if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                            {
                                File.Delete(fileName);
                            }
                        }
                    }
                    FileUpload4.SaveAs(PhysicalPath);
                    if (Label5.Visible == true)
                    {
                        Label5.Visible = false;
                    }
                    Label5.Visible = true;
                    Label5.Text = "Ticket Copy Uploded Successfully";
                    Label5.ForeColor = System.Drawing.Color.Green;
                    //Response.Redirect("Upload_Images.aspx?Image=" + ImageCode);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);
                }
            }
            else
            {
                Label5.Visible = true;
                Label5.ForeColor = System.Drawing.Color.Red;
                Label5.Text = "Please Upload .pdf or docx File Only!!";
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('Please Upload .pdf File Only!!')", true);
            }
        }
    }
}