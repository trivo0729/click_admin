﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
namespace CutAdmin
{
    public partial class BookingList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<CutAdmin.Models.MenuItem> arrMenu = (List<CutAdmin.Models.MenuItem>)HttpContext.Current.Session["Menu"];
            StringBuilder sb = new StringBuilder();
            List<string> ListService = new List<string>();
            if (arrMenu.Where(d => d.Name == "Service").FirstOrDefault() != null)
               ListService = arrMenu.Where(d => d.Name == "Service").FirstOrDefault().ChildItem.ToList().Select(f=>f.Name).ToList();
            //foreach (var objSub in arrMenu.Where(d => d.Name == "Service").FirstOrDefault().ChildItem.ToList())
            //{
            //    if (!ListService.Contains("Hotel"))
            //    {
            //        li_Hotel.di
            //    }

            //    else if (objSub.Name == "Airline")
            //    {
            //        //sb.Append("<li class=\"Airline\" onclick='GetFlightsBookings()'><a href = \"#Airline\" > Airline</a></li>");
            //    }

            //    else if (objSub.Name == "Sightseeing")
            //    {
            //        //sb.Append("<li class=\"Sightseeing\" onclick='GetActBookingList()'><a href = \"#Sightseeing\" > Seightseeing</a></li>");

            //    }

            //    else if (objSub.Name == @"Visa \ OTB")
            //    {
            //       // sb.Append("<li class=\"Visa\" onclick='GetVisa()'><a href = \"#Visa\" > Visa</a></li>");
            //       // sb.Append("<li class=\"OTB\" onclick='GetAllOtbDetails()'><a href = \"#OTB\" > OTB</a></li>");
            //    }
            //    else if (objSub.Name == @"Package")
            //    {
            //       // sb.Append("<li class=\"Package\" onclick='GetPackageBookings()'><a href = \"#Package\" > Package</a></li>");
            //    }
            //}

            if (!ListService.Contains("Hotel"))
            {
                li_Hotel.Disabled = true;
            }
            else if (!ListService.Contains("Airline"))
            {
                li_Airline.Disabled = true;
            }
            else if (!ListService.Contains("Sightseeing"))
            {
                li_Sightseeing.Disabled = true;
            }
            else if (!ListService.Contains(@"Visa \ OTB") )
            {
                li_Visa.Disabled = true;
                li_OTB.Disabled = true;
            }
            else if (!ListService.Contains(@"Package"))
            {
                li_Package.Disabled = true;
            }
        }
    }
}