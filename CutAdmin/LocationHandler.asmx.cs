﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.EntityModal;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for LocationHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class LocationHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
       [WebMethod(EnableSession = true)]
        public string Save(tbl_Location arrLocation,tbl_MappedArea arrCity)
        {
            jsSerializer = new JavaScriptSerializer();
            try
            {
                LocationManager.SaveLocation(arrLocation, arrCity);
                return jsSerializer.Serialize(new {retCode=1 });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0,ex=ex.Message });
            }
        }
        [WebMethod(EnableSession = true)]
        public string GetLocation()
        {
            try
            {
                return jsSerializer.Serialize(new { retCode = 1, LocationList= LocationManager.GetLocation() } );
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode = 0, ex = ex.Message });
            }
        }
        [WebMethod(EnableSession = true)]
        public string DeleteLocation(Int64 LocationId)
        {
            try
            {
                LocationManager.Delete(LocationId);
                return jsSerializer.Serialize(new {retCode = 1 });
            }
            catch (Exception ex)
            {
                return jsSerializer.Serialize(new { retCode =  0 , ex= ex.Message});
            }
        }

    }
}
