﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CommonLib.Response;
namespace CutAdmin.Common
{
    public class HotelFilter
    {
        public List<CommonHotelDetails> arrHotels { get; set; }
        public List<string> arrSupplier { get; set; }
        public string SearchParams { get; set; }
        public Filter arrFilter { get; set; }
        public class Filter
        {
            public string HotelName { get; set; }
            public List<element> arrLocation { get; set; }
            public List<element> arrFacility { get; set; }
            public List<element> Category { get; set; }
            public float MinPrice { get; set; }
            public float MaxPrice { get; set; }
        }
        public class element
        {
            public Int64 ID { get; set; }
            public string Name { get; set; }
            public Int64 Count { get; set; }
        }

        public void GenrateHotelFilter()
        {
            try
            {
                arrFilter = new Filter();
                arrFilter.arrFacility = new List<element>();
                arrFilter.arrLocation = new List<element>();
                arrFilter.Category = new List<element>();
                foreach (var objHotel in arrHotels)
                {
                    foreach (var obj in objHotel.Facility)
                    {
                        if (arrFilter.arrFacility.Where(d => d.Name == obj.Name).FirstOrDefault() == null)
                        {
                            arrFilter.arrFacility.Add(new element { Count = 1, ID = 0, Name = obj.Name });
                        }
                        else
                            arrFilter.arrFacility.Where(d => d.Name == obj.Name).FirstOrDefault().Count = arrFilter.arrFacility.Where(d => d.Name == obj.Name).FirstOrDefault().Count + 1;
                    }


                    foreach (var obj in objHotel.Location)
                    {
                        if (arrFilter.arrLocation.Where(d => d.Name == obj.Name).FirstOrDefault() == null)
                        {
                            arrFilter.arrLocation.Add(new element { Count = 1, ID = 0, Name = obj.Name });
                        }
                        else
                            arrFilter.arrLocation.Where(d => d.Name == obj.Name).FirstOrDefault().Count = arrFilter.arrLocation.Where(d => d.Name == obj.Name).FirstOrDefault().Count + 1;
                    }


                    if (arrFilter.Category.Where(d => d.Name == objHotel.Category).FirstOrDefault() == null)
                    {
                        arrFilter.Category.Add(new element { Count = 1, ID = 0, Name = objHotel.Category });
                    }
                    else
                        arrFilter.Category.Where(d => d.Name == objHotel.Category).FirstOrDefault().Count = arrFilter.Category.Where(d => d.Name == objHotel.Category).FirstOrDefault().Count + 1;

                }
                if (arrHotels.Count != 0)
                {
                    arrFilter.MinPrice = Convert.ToSingle(arrHotels.Select(d => d.MinPrice).ToList().Min());
                    arrFilter.MaxPrice = Convert.ToSingle(arrHotels.Select(d => d.MinPrice).ToList().Max());
                }



            }
            catch (Exception ex)
            {
            }
        }

        public static List<CommonHotelDetails> Search(List<CommonHotelDetails> arrHotels, List<element> Category)
        {
            List<CommonHotelDetails> arrSearch = new List<CommonHotelDetails>();
            try
            {
                foreach (var obj in Category)
                {
                    if (Category.Where(D => D.Name.Contains("All")).ToList().Count == 0)
                    {
                        var ListHotel = arrHotels.Where(D => D.Category == obj.Name).ToList();
                        foreach (var objHotel in ListHotel)
                        {
                            arrSearch.Add(objHotel);
                        }
                    }
                    else
                    {
                        arrSearch = arrHotels;
                        break;
                    }
                }
                if (arrSearch.Count == 0)
                    throw new Exception("No Result found");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return arrSearch.Distinct().ToList();
        }
    }
}