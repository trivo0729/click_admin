﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CutAdmin.Models;
using CutAdmin.DataLayer;
using CutAdmin.EntityModal;

namespace CutAdmin
{
    public partial class EmailTemplate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var arrFolders = GenTemplateFolder();
            StringBuilder sMenu = new StringBuilder();

            Int64 ParentID = AccountManager.GetSuperAdminID();

            using (var db = new Trivo_AmsHelper())
            {
                var List = (from obj in db.tbl_EmailTemplates where obj.nAdminID == ParentID select obj).ToList();

                foreach (var Folder in arrFolders)
                {
                    if (Folder.ChildItem.Count != 0)
                    {
                        sMenu.Append("<li class=\"with-right-arrow grey-arrow\">");
                        sMenu.Append("<span class=\"icon folder-image\"></span>");
                        sMenu.Append("<b>" + Folder.Name + "</b>");
                        sMenu.Append("<ul class=\"files-list mini\">");
                        foreach (var ChildItem in Folder.ChildItem)
                        {
                            if (ChildItem.ChildItem.Count == 0)
                            {
                                sMenu.Append("<li><a href='" + ChildItem.Path + "' class=\"file-link\"><span class=\"icon folder-open\"></span><b>" + ChildItem.Name + "</b></a>");
                                sMenu.Append("<div class=\"controls\">");
                                sMenu.Append("<span class=\"button-group compact children-tooltip\">");

                                if (List.Count != 0)
                                {
                                    if (List.Exists(d => d.sPath == ChildItem.Path))
                                    {
                                        if (List.Exists(d => d.sPath == ChildItem.Path && d.Activate == true))
                                            sMenu.Append("<input type =\"checkbox\" name =\"Checkbox\"  class=\"switch tiny checked " + Folder.ChildItem.IndexOf(ChildItem) + "\"  onchange=Activate('" + RemoveSpace(ChildItem.Path) + "','" + RemoveSpace(Folder.Name) + "',true,1) />");
                                        else
                                            sMenu.Append("<input type =\"checkbox\" name =\"Checkbox\" class=\"switch tiny " + Folder.ChildItem.IndexOf(ChildItem) + "\" onchange=Activate('" + RemoveSpace(ChildItem.Path) + "','" + RemoveSpace(Folder.Name) + "',false,1) />");
                                    }
                                    else
                                        sMenu.Append("<input type =\"checkbox\" name =\"Checkbox\" class=\"switch tiny " + Folder.ChildItem.IndexOf(ChildItem) + "\" onchange=Activate('" + RemoveSpace(ChildItem.Path) + "','" + RemoveSpace(Folder.Name) + "',false,1) />");
                                }
                                else
                                    sMenu.Append("<input type =\"checkbox\" name =\"Checkbox\" class=\"switch tiny " + Folder.ChildItem.IndexOf(ChildItem) + "\" onchange=Activate('" + RemoveSpace(ChildItem.Path) + "','" + RemoveSpace(Folder.Name) + "',false,1) />");

                                sMenu.Append("</span>");
                                sMenu.Append("</div></li>");
                            }
                            else
                            {
                                sMenu.Append("<li class=\"with-right-arrow grey-arrow\">");
                                sMenu.Append("<span class=\"icon folder-image\"></span>");
                                sMenu.Append("<b>" + ChildItem.Name + "</b>");
                                sMenu.Append("<ul class=\"big-menu\">");
                                foreach (var ChildItemSub in ChildItem.ChildItem)
                                {
                                    if (ChildItemSub.ChildItem.Count == 0)
                                    {
                                        sMenu.Append("<li><a href=''" + ChildItemSub.Path + "' class=\"file-link\"><span class=\"icon folder-open\"></span><b>" + ChildItemSub.Name + "</b></a>");
                                        sMenu.Append("<div class=\"controls\">");
                                        sMenu.Append("<span class=\"button-group compact children-tooltip\">");

                                        if (List.Count != 0)
                                        {
                                            if (List.Exists(d => d.sPath == ChildItem.Path))
                                            {
                                                if (List.Exists(d => d.sPath == ChildItem.Path && d.Activate == true))
                                                    sMenu.Append("<input type =\"checkbox\" name =\"Checkbox\" class=\"switch tiny checked " + ChildItem.ChildItem.IndexOf(ChildItemSub) + "\"  onchange=Activate('" + RemoveSpace(ChildItemSub.Path) + "','" + RemoveSpace(ChildItem.Name) + "',true,1) />");
                                                else
                                                    sMenu.Append("<input type =\"checkbox\" name =\"Checkbox\" class=\"switch tiny " + ChildItem.ChildItem.IndexOf(ChildItemSub) + "\"  onchange=Activate('" + RemoveSpace(ChildItemSub.Path) + "','" + RemoveSpace(ChildItem.Name) + "',false,1) />");
                                            }
                                            else
                                                sMenu.Append("<input type =\"checkbox\" name =\"Checkbox\" class=\"switch tiny " + ChildItem.ChildItem.IndexOf(ChildItemSub) + "\"  onchange=Activate('" + RemoveSpace(ChildItemSub.Path) + "','" + RemoveSpace(ChildItem.Name) + "',false,1) />");

                                        }
                                        else
                                            sMenu.Append("<input type =\"checkbox\" name =\"Checkbox\" class=\"switch tiny " + ChildItem.ChildItem.IndexOf(ChildItemSub) + "\"  onchange=Activate('" + RemoveSpace(ChildItemSub.Path) + "','" + RemoveSpace(ChildItem.Name) + "',false,1) />");


                                        sMenu.Append("</span>");
                                        sMenu.Append("</div></li>");
                                    }
                                    else
                                    {
                                        sMenu.Append("<li class=\"with-right-arrow grey-arrow\">");
                                        sMenu.Append("<span class=\"icon folder-image\"></span>");
                                        sMenu.Append("<b>" + ChildItemSub.Name + "</b>");
                                        sMenu.Append("<ul class=\"big-menu\">");
                                        foreach (var OBJ in ChildItemSub.ChildItem)
                                        {
                                            sMenu.Append("<li><a href='" + OBJ.Path + "' class=\"file-link\"><span class=\"icon folder-open\"></span><b>" + OBJ.Name + "</b></a>");
                                            sMenu.Append("<div class=\"controls\">");
                                            sMenu.Append("<span class=\"button-group compact children-tooltip\">");

                                            if (List.Count != 0)
                                            {
                                                if (List.Exists(d => d.sPath == ChildItemSub.Path))
                                                {
                                                    if (List.Exists(d => d.sPath == ChildItemSub.Path && d.Activate == true))
                                                        sMenu.Append("<input type =\"checkbox\" name =\"Checkbox\" class=\"switch tiny checked " + ChildItem.ChildItem.IndexOf(OBJ) + "\"  onchange=Activate('" + RemoveSpace(OBJ.Path) + "','" + RemoveSpace(ChildItemSub.Name) + "',true,1) />");
                                                    else
                                                        sMenu.Append("<input type =\"checkbox\"  name =\"Checkbox\" class=\"switch tiny " + ChildItem.ChildItem.IndexOf(OBJ) + "\"  onchange=Activate('" + RemoveSpace(OBJ.Path) + "','" + RemoveSpace(ChildItemSub.Name) + "',false,1) />");
                                                }
                                                else
                                                    sMenu.Append("<input type =\"checkbox\"  name =\"Checkbox\" class=\"switch tiny " + ChildItem.ChildItem.IndexOf(OBJ) + "\"  onchange=Activate('" + RemoveSpace(OBJ.Path) + "','" + RemoveSpace(ChildItemSub.Name) + "',false,1) />");
                                            }
                                            else
                                                sMenu.Append("<input type =\"checkbox\" name =\"Checkbox\" class=\"switch tiny " + ChildItem.ChildItem.IndexOf(OBJ) + "\"  onchange=Activate('" + RemoveSpace(OBJ.Path) + "','" + RemoveSpace(ChildItemSub.Name) + "',false,1) />");

                                            sMenu.Append("</span>");
                                            sMenu.Append("</div></li>");
                                        }
                                        sMenu.Append("</ul>");
                                        sMenu.Append("</li>");
                                    }
                                }
                                sMenu.Append("</ul>");
                                sMenu.Append("</li>");
                            }
                        }
                        sMenu.Append("</ul>");
                        sMenu.Append("</li>");
                    }
                    else
                    {
                        sMenu.Append("<li><a href='" + Folder.Path + "' class=\"file-link\"><span class=\"icon folder-open\"></span><b>" + Folder.Name + "</b></a>");
                        sMenu.Append("<div class=\"controls\">");
                        sMenu.Append("<span class=\"button-group compact children-tooltip\">");

                        if (List.Count != 0)
                        {
                            if (List.Exists(d => d.sPath == Folder.Path))
                            {
                                if (List.Exists(d => d.sPath == Folder.Path && d.Activate == true))
                                    sMenu.Append("<input type =\"checkbox\" name =\"Checkbox\" class=\"switch tiny checked\"  onchange=Activate('" + RemoveSpace(Folder.Path) + "','" + RemoveSpace(Folder.Name) + "',true,0) />");
                                else
                                    sMenu.Append("<input type =\"checkbox\" name =\"Checkbox\" class=\"switch tiny\"  onchange=Activate('" + RemoveSpace(Folder.Path) + "','" + RemoveSpace(Folder.Name) + "',false,0) />");
                            }
                            else
                                sMenu.Append("<input type =\"checkbox\" name =\"Checkbox\" class=\"switch tiny\"  onchange=Activate('" + RemoveSpace(Folder.Path) + "','" + RemoveSpace(Folder.Name) + "',false,0) />");
                        }
                        else
                            sMenu.Append("<input type =\"checkbox\" name =\"Checkbox\" class=\"switch tiny\"  onchange=Activate('" + RemoveSpace(Folder.Path) + "','" + RemoveSpace(Folder.Name) + "',false,0) />");

                        sMenu.Append("</span>");
                        sMenu.Append("</div></li>");
                    }

                }
            }
            ul_Folder.InnerHtml = sMenu.ToString();
        }

        public string RemoveSpace(string Name)
        {
            string path = "";

            path = Name.Replace(" ", "%20");

            return path;
        }

        public string RemovePercent(string Name)
        {
            string path = "";

            path = Name.Replace("%20", " ");

            return path;
        }

        public static List<CutAdmin.Models.MenuItem> GenTemplateFolder()
        {
            List<CutAdmin.Models.MenuItem> arrMenu = new List<CutAdmin.Models.MenuItem>();
            string pathDownload = Common.Common.GetAppServerFolder;
            DirectoryInfo directory = new DirectoryInfo(pathDownload);
            DirectoryInfo[] directories = directory.GetDirectories();
            try
            {
                foreach (DirectoryInfo folder in directories)
                {
                    var arrList = GetSubMenu(folder.Name);
                    foreach (var Menu in arrList)
                    {
                        arrMenu.Add(Menu);
                    }
                }
            }
            catch (Exception)
            {

            }
            return arrMenu.OrderBy(d => d.Name).ToList();
        }

        public static List<CutAdmin.Models.MenuItem> GetSubMenu(string Folder)
        {
            List<CutAdmin.Models.MenuItem> ListMenu = new List<Models.MenuItem>();
            try
            {
                string pathDownload = Common.Common.GetAppServerFolder + "\\" + Folder;
                DirectoryInfo directory = new DirectoryInfo(pathDownload);
                DirectoryInfo[] directories = directory.GetDirectories();
                foreach (DirectoryInfo folder in directories)
                {
                    CutAdmin.Models.MenuItem arrMenu = new Models.MenuItem();
                    arrMenu = new CutAdmin.Models.MenuItem { Name = folder.Name.Split('.')[0], ChildItem = new List<CutAdmin.Models.MenuItem>(), Path = string.Empty };
                    List<FileInfo> files = new List<FileInfo>();
                    files = folder.GetFiles().ToList();
                    if (files.Count != 0)
                    {
                        if (files.Count == 1)
                            arrMenu.Path = Common.Common.GetAppRootFolder + "/" + Folder + "/" + folder + "/" + files[0].Name;
                        else
                            foreach (var File in files)
                            {
                                arrMenu.ChildItem.Add(new Models.MenuItem
                                {
                                    Name = File.Name.Split('.')[0],
                                    ChildItem = new List<CutAdmin.Models.MenuItem>(),
                                    Path = Common.Common.GetAppRootFolder + "/" + Folder + "/" + folder + "/" + File.Name,
                                });
                            }
                    }
                    ListMenu.Add(arrMenu);
                }
            }
            catch (Exception)
            {
            }
            return ListMenu;
        }
    }
}