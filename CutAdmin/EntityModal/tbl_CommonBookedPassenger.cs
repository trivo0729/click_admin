//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CutAdmin.EntityModal
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_CommonBookedPassenger
    {
        public long sid { get; set; }
        public string ReservationID { get; set; }
        public string RoomCode { get; set; }
        public string RoomNumber { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public Nullable<int> Age { get; set; }
        public string PassengerType { get; set; }
        public Nullable<bool> IsLeading { get; set; }
    }
}
