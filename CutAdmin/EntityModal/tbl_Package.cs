//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CutAdmin.EntityModal
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Package
    {
        public long nID { get; set; }
        public string sPackageName { get; set; }
        public string sPackageDestination { get; set; }
        public string sPackageCategory { get; set; }
        public string sPackageThemes { get; set; }
        public long nDuration { get; set; }
        public string dValidityTo { get; set; }
        public string sPackageDescription { get; set; }
        public string dValidityFrom { get; set; }
        public decimal dTax { get; set; }
        public long nCancelDays { get; set; }
        public decimal dCancelCharge { get; set; }
        public string sTermsCondition { get; set; }
        public string sInventory { get; set; }
        public Nullable<long> ParentID { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<bool> IsDomestic { get; set; }
        public Nullable<bool> Transfer { get; set; }
    }
}
