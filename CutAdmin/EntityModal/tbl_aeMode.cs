//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CutAdmin.EntityModal
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_aeMode
    {
        public int Sid { get; set; }
        public string TourType { get; set; }
        public string Images { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<long> ParentID { get; set; }
    }
}
