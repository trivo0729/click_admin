//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CutAdmin.EntityModal
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_SightseeingChildPolicy
    {
        public int Sid { get; set; }
        public Nullable<int> Activity_Id { get; set; }
        public Nullable<int> Child_Age_From { get; set; }
        public Nullable<int> Child_Age_Upto { get; set; }
        public Nullable<int> Small_Child_Age_Upto { get; set; }
        public Nullable<bool> Allow_Height { get; set; }
        public Nullable<decimal> Child_Min_Height { get; set; }
        public Nullable<bool> Infant_Allow { get; set; }
    }
}
