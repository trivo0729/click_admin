<div class="tabs-content">
                        <div id="BasicInformation">
                            <div class="with-padding" id="div_BasicDetails">
                                <h4>Basic Details</h4>	
                                <hr />
                                <div class="columns">
                                    <div class="three-columns twelve-columns-mobile">
                                        <label>Package Name:</label><div class="input full-width">
                                            <input value="" id="txt_PackageName" class="input-unstyled full-width validate[required]" data-prompt-position="topLeft" placeholder="Package Name" type="text">
                                        </div>
                                    </div>

                                    <div class="three-columns twelve-columns-mobile">
                                        <label>Destination</label>
                                        <div class="input full-width">
                                            <input value="" id="txt_City" class="input-unstyled full-width validate[required]" data-prompt-position="topLeft" placeholder="City" type="text">
                                        </div>
                                        <input type="hidden" id="hdnDCode" />
                                    </div>
                                    <div class="three-columns twelve-columns-mobile" id="DivDuration">
                                        <label>Duration</label><div class="full-width button-height">
                                            <select style="width: 247px" id="txt_Duration" name="validation-select" class="select validate[required]" data-prompt-position="topLeft">
                                                <option value="0" selected="selected">Select Duration</option>
                                                <option value="1">01 Days 0 Night</option>
                                                <option value="2">02 Days 1 Night</option>
                                                <option value="3">03 Days 2 Night</option>
                                                <option value="4">04 Days 3 Night</option>
                                                <option value="5">05 Days 4 Night</option>
                                                <option value="6">06 Days 5 Night</option>
                                                <option value="7">07 Days 6 Night</option>
                                                <option value="8">08 Days 7 Night</option>
                                                <option value="9">09 Days 8 Night</option>
                                                <option value="10">10 Days  9 Night</option>
                                                <option value="11">11 Days  10 Night</option>
                                                <option value="12">12 Days  11 Night</option>
                                                <option value="13">13 Days  12 Night</option>
                                                <option value="14">14 Days  13 Night</option>
                                                <option value="15">15 Days  14 Night</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile">
                                        <label>Tax</label><div class="input full-width">
                                            <input value="" id="txt_Tax" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="input-unstyled full-width validate[required]" data-prompt-position="topLeft" placeholder="Tax" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="columns">
                                    <div class="three-columns twelve-columns-mobile">
                                        <label>Category</label>
                                        <br>
                                        <div class="full-width button-height" id="Divselect_Category">
                                            <select id="select_Category" style="width: 250px" class="select multiple-as-single easy-multiple-selection check-list Category validate[required]" data-prompt-position="topLeft" multiple>
                                                <option value="1">Standard</option>
                                                <option value="2">Deluxe</option>
                                                <option value="3">Premium</option>
                                                <option value="4">Luxury</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile">
                                        <label>Themes</label>
                                        <br>
                                        <div class="full-width button-height" id="Divselect_Themes">
                                            <select id="select_Themes" style="width: 250px" class="select multiple-as-single easy-multiple-selection check-list validate[required]" data-prompt-position="topLeft" multiple>
                                                <option value="1">Holidays</option>
                                                <option value="2">Umrah</option>
                                                <option value="3">Hajj</option>
                                                <option value="4">Honeymoon</option>
                                                <option value="5">Summer</option>
                                                <option value="6">Adventure</option>
                                                <option value="7">Deluxe</option>
                                                <option value="8">Business</option>
                                                <option value="9">Premium</option>
                                                <option value="10">Wildlife</option>
                                                <option value="11">Weekend</option>
                                                <option value="12">New Year</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="three-columns twelve-columns-mobile">
                                        <label>Valid From:</label>
                                        <span class="input full-width">
                                            <span class="icon-calendar"></span>
                                            <input type="text" name="datepicker" id="datepicker" placeholder="mm/dd/yyyy" class="input-unstyled datepicker validate[required]" data-prompt-position="topLeft" value="">
                                        </span>
                                    </div>
                                    <div class="three-columns twelve-columns-mobile">
                                        <label>Valid Up to:</label><span class="input full-width">
                                            <span class="icon-calendar"></span>
                                            <input type="text" name="datepicker" id="datepicker2" placeholder="mm/dd/yyyy" class="input-unstyled datepicker validate[required]" data-prompt-position="topLeft" value="">
                                        </span>
                                    </div>
                                </div>

                                <h4>Cancellation Policy</h4>
                                <hr />
                                <div class="columns">
                                    <div class="six-columns twelve-columns-mobile">
                                        <label>Before Check In Days:</label><div class="input full-width">
                                            <input value="" id="txt_cancelDays" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="input-unstyled full-width validate[required]" data-prompt-position="topLeft" placeholder="Before Check In Days" type="text">
                                        </div>
                                    </div>
                                    <div class="six-columns twelve-columns-mobile">
                                        <label>Cancellation Charge In Percentage:</label><div class="input full-width">
                                            <input value="" id="txt_CancelCharge" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="input-unstyled full-width validate[required]" data-prompt-position="topLeft" placeholder="Cancellation Charge In Percentage" type="text">
                                        </div>
                                    </div>
                                </div>

                                <h4>Inventory</h4>
                                <hr />
                                <div class="columns">
                                    <div class="six-columns twelve-columns-mobile">
                                        <input type="radio" name="radio" style="cursor: pointer" checked id="radio_InventoryRequest" onchange="setInvetoryFixed();" class="validate[required]" data-prompt-position="topLeft">
                                        <label for="radio-2" class="label">On Request</label>
                                    </div>
                                    <div class="six-columns twelve-columns-mobile">
                                        <input type="radio" style="cursor: pointer" name="radio" id="radio_InventoryFixed" onchange="setInvetoryFixed();" class="validate[required]" data-prompt-position="topLeft">
                                        <label for="radio-2" class="label">Fixed no of packages</label>
                                        <div class="columns" id="Divtxt_Inventory" style="display: none;">
                                            <div class="six-columns twelve-columns-mobile">
                                                <div class="input full-width">
                                                    <input value="" id="txt_Inventory" class="input-unstyled full-width validate[required]" data-prompt-position="topLeft" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="columns">
                                    <div class="six-columns twelve-columns-mobile">
                                        <input type="radio" name="Type" style="cursor: pointer" checked id="Domestic">
                                        <label for="radio-2" class="label">Domestic</label>
                                    </div>
                                    <div class="six-columns twelve-columns-mobile">
                                        <input type="radio" style="cursor: pointer" name="Type" id="International">
                                        <label for="radio-2" class="label">International</label>
                                    </div>
                                </div>
                                <div class="columns">
                                    <div class="six-columns twelve-columns-mobile">
                                        <input type="checkbox" name="exclusion" id="Transfer" onchange="TransferShow()" class="checkbox">
                                        <label for="Transfer" class="label">Including Transfer</label>
                                    </div>
                                </div>
                                <h4>Description</h4>
                                <div class="columns">
                                    <div class="twelve-columns">
                                        <textarea name="txt_Desctiption" id="txt_Desctiption" class="input full-width autoexpanding validate[required]" data-prompt-position="topLeft"></textarea>
                                    </div>
                                </div>

                                <h4>Terms & Condition</h4>
                                <div class="columns">
                                    <div class="twelve-columns">
                                        <textarea name="txt_TermsCondition" id="txt_TermsCondition" class="input full-width autoexpanding validate[required]" data-prompt-position="topLeft"></textarea>
                                    </div>
                                </div>

                                <hr />
                                <p class="text-alignright">
                                    <button type="button" class="button anthracite-gradient" onclick="AddPackageBasicDetails();">Save</button>
                                </p>
                            </div>
                        </div>
                        <div id="Pricing">
                            <div class="with-padding">
                                <h4>Pricing Details</h4>
                                <hr />
                                <div class="standard-tabs margin-bottom" id="div_pricingTabContent">
                                </div>
                            </div>
                        </div>

                        <div id="Itinerary">
                            <div class="with-padding">
                                <h4>Itinerary Details</h4>
                                <hr />
                                <div id="div_IteneraryTabContent">
                                </div>
                                <hr />
                                <input type="hidden" id="hdnHCode" />
                                <p class="text-alignright">
                                    <button type="button" class="button anthracite-gradient" onclick="SaveItinerary();">Save</button>
                                </p>
                            </div>
                        </div>

                        <div id="HotelDetails">
                            <div class="with-padding">
                                <h4>Hotel Details</h4>
                                <hr />
                                <div class="standard-tabs margin-bottom" id="div_HotelTabContent">
                                </div>
                            </div>
                        </div>
                        <div id="PackageImages" class="with-padding">
                            <div class="content-panel mobile-panels margin-bottom">
                                <div class="panel-content linen">
                                    <div class="panel-control align-right">
                                        <span class="progress thin" style="width: 75px">
                                            <span class="progress-bar green-gradient" style="width: 0%"></span>
                                        </span>
                                        You may add <span class="ImgCount">10</span> more images
                        <input type="file" id="images" accept="image/*" class="hidden" onchange="preview_images(this,'result');" multiple />
                                        <a class="button icon-cloud-upload margin-left file withClearFunctions" onclick="($('#images').click())">Add file...</a>
                                    </div>
                                    <div class="panel-load-target scrollable with-padding" style="height: auto">

                                        <p class="message icon-info-round white-gradient">
                                            Add can add upto 10 images / photos related to this activity
                                    <input type="file" multiple id="Imges" onchange="preview_images()" class="btn-search5 hidden" />
                                        </p>
                                        <ul class="blocks-list fixed-size-200" id="result">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <p class="text-alignright">
                                <button type="button" class="button anthracite-gradient" onclick="add_PackageImage()">Save Images</button>
                            </p>
                            
                        </div>
                        <div id="PackageTransfer">
                            <div class="with-padding">
                                <h4>Transfer Details</h4>
                                <hr />
                                <div class="columns">
                                    <div class="three-columns twelve-columns-mobile">
                                        <label>Vehicle</label>
                                        <br>
                                        <div class="full-width button-height" id="Divselect_Vehicle0">
                                            <select id="select_Vehicle0" name="validation-select" style="width: 250px" class="select">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="three-columns">
                                        <label>Price :</label>
                                        <div class="input full-width">
                                            <input value="" id="txt_Price0" class="input-unstyled full-width" placeholder="0.00" type="text">
                                        </div>
                                    </div>
                                    <div class="one-column" id="ColPlus_0">
                                        <i aria-hidden="true" onclick="Validation(1)">
                                            <label for="pseudo-input-2" class="button anthracite-gradient" style="margin-top: 21px;"><span class="icon-plus"></span></label>
                                        </i>
                                    </div>
                                </div>
                                <div id="RowTransfer"></div>
                                <br />
                                <p class="text-alignright">
                                    <button type="button" class="button anthracite-gradient" onclick="AddPackageCab()">Save</button>
                                </p>
                            </div>
                        </div>
                        <div id="PackageActivities">
                            <div class="with-padding">
                                <h4>Transfer Details</h4>
                                <hr />
                                <div class="columns" id="Activity">
                                </div>
                                <p class="text-alignright">
                                    <button type="button" class="button anthracite-gradient" onclick="AddActivities()">Save</button>
                                </p>
                            </div>
                        </div>
                    </div>