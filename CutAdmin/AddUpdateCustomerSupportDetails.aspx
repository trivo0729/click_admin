﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddUpdateCustomerSupportDetails.aspx.cs" Inherits="CutAdmin.AddUpdateCustomerSupportDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/CustomerSupportDetails.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
            <h1>Customer Support Details</h1>
            <hr>
        </hgroup>
        <div class="with-padding">

                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <h3 class="thin underline grey">Contact Details</h3>
                </div>
                <div class="columns" >
                    <div class="three-columns">
                        <span class="text-left">Coustomer Support Type<span class="red">*</span></span>
                            <input class="input full-width" type="text" id="txt_CoustomerSupportType"  placeholder="Coustomer Support Type"/>
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_CoustomerSupportType"><b>* This field is required</b></label>
                    </div >
                </div >
                <div id="ContactDetailUI" style="width: 100%">
                </div>
                <div class="new-row twelve-columns">
                    <%--<button type="button" class="button glossy blue-gradient float-right"  id="btn" onclick="Validate()">Add Offer</button>--%>
                    <button type="button" class="button glossy anthracite-gradient float-right" id="btn" onclick="AddDetail()">Add</button>
                    <button type="button" style="display: none" class="button glossy anthracite-gradient float-right" id="btn_update" onclick="UpdateDetail()">Update</button>
                </div>
           <%-- <div class="columns" id="DivOfferList" style="min-height: 200px;display:none;">--%>
            <div class="columns" id="DivOfferList" style="min-height: 200px">
                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <h3 class="thin underline grey">Contact List</h3>
                </div>
                <div class="three-columns">
                    <span class="text-left">Coustomer Support Type</span>
                    <input class="input full-width" type="text" id="txt_CoustomerSupportType2"  placeholder="Search by Coustomer Support Type"/>
                </div>
                <div class="three-columns">
                    <%--<button type="button" class="button glossy blue-gradient float-right"  id="btn" onclick="Validate()">Add Offer</button>--%>
                    <button type="button" class="button glossy anthracite-gradient float-right" id="btn_Search" onclick="SearchByContactType()">Search</button>
                   
                </div>
                <div class="new-row twelve-columns" style="margin-bottom: -2px;">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="tbl_CPDetails">
                        <thead>
                            <tr>
                                <th align="center" style="width: 2%">S.No</th>
                                <th align="center" style="width: 5%">Customer Type</th>
                                <th align="center" style="width: 5%">Country</th>
                                <th align="center" style="width: 5%">Coustomer Support No</th>
                                <th align="center" style="width: 5%">Coustomer Support Email</th>
                                <th align="center" style="width: 5%">Edit</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
     
             
         </div>


                       
	</section>
	<!-- End main content -->

</asp:Content>


