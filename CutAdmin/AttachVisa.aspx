﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AttachVisa.aspx.cs" Inherits="CutAdmin.AttachVisa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- Main content -->
    <section role="main" id="main">
        <hgroup id="main-title" class="thin">
            <h1>Attach Visa</h1>
            <hr />
        </hgroup>
        <div class="with-padding">
            <form action="#" class="addagent" runat="server">

                <div class="columns">
                    <div class="three-columns twelve-columns-mobile six-columns-tablet">
                        <label>Visa No :</label>
                    </div>
                    <div class="two-columns twelve-columns-mobile six-columns-tablet">
                        <div class="full-width button-height" id="DivDropDownList1">
                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="select">
                                <asp:ListItem Value="212"></asp:ListItem>
                                <asp:ListItem Value="216"></asp:ListItem>
                                <asp:ListItem Value="201"></asp:ListItem>
                                <asp:ListItem Value="204"></asp:ListItem>
                                <asp:ListItem Value="206"></asp:ListItem>
                                <asp:ListItem Value="208"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="two-columns twelve-columns-mobile six-columns-tablet">
                        <div class="full-width button-height" id="DivDropDownList2">
                            <asp:DropDownList ID="DropDownList2" runat="server" CssClass="select">
                                <asp:ListItem Value="2018"></asp:ListItem>
                                <asp:ListItem Value="2017"></asp:ListItem>
                                <asp:ListItem Value="2016"></asp:ListItem>
                                <asp:ListItem Value="2015"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="five-columns twelve-columns-mobile six-columns-tablet">

                        <div class="input full-width">
                            <asp:TextBox ID="TextBox1" runat="server" CssClass="input-unstyled full-width" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="columns">
                    <div class="three-columns twelve-columns-mobile six-columns-tablet">
                        <label>Visa Copy :</label>
                    </div>
                    <div class="nine-columns twelve-columns-mobile six-columns-tablet">
                        <asp:HiddenField ID="HiddenField1" runat="server" ClientIDMode="Static" Value="" />
                        <asp:Label ID="lblMessage" runat="server" Text="File uploaded successfully." ClientIDMode="Static" Value="" ForeColor="Green" Visible="false" />
                        <div class="input full-width">
                             <asp:Button ID="btnUpload" Text="Upload" runat="server" OnClick="Upload" Style="display: none" />
                            <asp:FileUpload ID="FileUpload1" runat="server" class="form-control"  />
                        </div>
                        <%--<asp:Button ID="btnUpload" Text="Upload" runat="server" OnClick="Upload" Style="display: none" />--%>
                    </div>
                </div>
                <%--<p class="text-right" style="text-align: right; display: none;">
                    <button type="button" class="button anthracite-gradient" id="btnUpload" onclick="Upload()" title="Upload">Upload</button>
                </p>--%>
            </form>
        </div>
    </section>
    <script type="text/javascript">
        function UploadFile(fileUpload) {
            if (fileUpload.value != '') {
                document.getElementById("<%=btnUpload.ClientID %>").click();
                                                        }


                                                    }
                                                </script>
    <!-- End main content -->

</asp:Content>
