﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CutAdmin
{
    public partial class AddPackage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btn_Uploadfile_Click(object sender, EventArgs e)
        {
            //ImpersonationManager objIM = new ImpersonationManager();
            //objIM.ImpersonateStart();
            //string Serverpath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
            //if (!FileUpload0.HasFile)
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('please choose first image for package')", true);
            //    return;
            //}
            //else if (!FileUpload1.HasFile)
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('please choose second image for package')", true);
            //    return;
            //}
            //else if (!FileUpload2.HasFile)
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('please choose third image for package')", true);
            //    return;
            //}
            //else if (!FileUpload3.HasFile)
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('please choose fourth image for package')", true);
            //    return;
            //}
            //else if (!FileUpload4.HasFile)
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('please choose last image for package')", true);
            //    return;
            //}
            string Serverpath = Server.MapPath("ImagesFolder");
            Int64 nID = Convert.ToInt64(hidden_Filed_ID.Value);
            DataTable dtResult = new DataTable();
            DBHelper.DBReturnCode retC = PackageManager.GetProductImages(nID, out dtResult);
            string[] imageArray = new string[5];
            string[] sfinalArray = new string[5];
            if (retC == DBHelper.DBReturnCode.SUCCESS)
            {
                imageArray = dtResult.Rows[0]["ImageArray"].ToString().Split(new string[] { "^_^" }, StringSplitOptions.RemoveEmptyEntries);
            }
            foreach (string dr in imageArray)
            {
                if (dr.IndexOf("Image_0") >= 0)
                {
                    sfinalArray[0] = dr;
                }
                else if (dr.IndexOf("Image_1") >= 0)
                {
                    sfinalArray[1] = dr;
                }
                else if (dr.IndexOf("Image_2") >= 0)
                {
                    sfinalArray[2] = dr;
                }
                else if (dr.IndexOf("Image_3") >= 0)
                {
                    sfinalArray[3] = dr;
                }
                else if (dr.IndexOf("Image_4") >= 0)
                {
                    sfinalArray[4] = dr;
                }
            }

            string sFileNameArray = "";
            if (FileUpload0.HasFile)
            {

                string[] sFileExtension = FileUpload0.FileName.Split('.');
                string sFileName = "Image_0" + "." + sFileExtension[1];
                //string VirtualPath = "~/PromoImages/" + sFileName;
                string sFolderName = Serverpath + "\\" + nID;
                string PhysicalPath = sFolderName + "\\" + sFileName;
                sfinalArray[0] = "Image_0" + "." + sFileExtension[1];
                //sFileNameArray += "Image_0" + "." + sFileExtension[1] + "^_^";
                if (!Directory.Exists(sFolderName))
                {
                    Directory.CreateDirectory(sFolderName);
                }
                else
                {
                    string[] files = Directory.GetFiles(sFolderName);
                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            File.Delete(fileName);
                        }
                    }
                }
                FileUpload0.SaveAs(PhysicalPath);
            }
            if (FileUpload1.HasFile)
            {

                string[] sFileExtension = FileUpload1.FileName.Split('.');
                string sFileName = "Image_1" + "." + sFileExtension[1];
                //string VirtualPath = "~/PromoImages/" + sFileName;
                string sFolderName = Serverpath + "\\" + nID;
                string PhysicalPath = sFolderName + "\\" + sFileName;
                sfinalArray[1] = "Image_1" + "." + sFileExtension[1];
                //sFileNameArray += "Image_1" + "." + sFileExtension[1] + "^_^";
                if (!Directory.Exists(sFolderName))
                {
                    Directory.CreateDirectory(sFolderName);
                }
                else
                {
                    string[] files = Directory.GetFiles(sFolderName);
                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            File.Delete(fileName);
                        }
                    }
                }
                FileUpload1.SaveAs(PhysicalPath);
            }
            if (FileUpload2.HasFile)
            {

                string[] sFileExtension = FileUpload2.FileName.Split('.');
                string sFileName = "Image_2" + "." + sFileExtension[1];
                //string VirtualPath = "~/PromoImages/" + sFileName;
                string sFolderName = Serverpath + "\\" + nID;
                string PhysicalPath = sFolderName + "\\" + sFileName;
                sfinalArray[2] = "Image_2" + "." + sFileExtension[1];
                //sFileNameArray += "Image_2" + "." + sFileExtension[1] + "^_^";
                if (!Directory.Exists(sFolderName))
                {
                    Directory.CreateDirectory(sFolderName);
                }
                else
                {
                    string[] files = Directory.GetFiles(sFolderName);
                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            File.Delete(fileName);
                        }
                    }
                }
                FileUpload2.SaveAs(PhysicalPath);
            }
            if (FileUpload3.HasFile)
            {

                string[] sFileExtension = FileUpload3.FileName.Split('.');
                string sFileName = "Image_3" + "." + sFileExtension[1];
                //string VirtualPath = "~/PromoImages/" + sFileName;
                string sFolderName = Serverpath + "\\" + nID;
                string PhysicalPath = sFolderName + "\\" + sFileName;
                sfinalArray[3] = "Image_3" + "." + sFileExtension[1];
                //sFileNameArray += "Image_3" + "." + sFileExtension[1] + "^_^";
                if (!Directory.Exists(sFolderName))
                {
                    Directory.CreateDirectory(sFolderName);
                }
                else
                {
                    string[] files = Directory.GetFiles(sFolderName);
                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            File.Delete(fileName);
                        }
                    }
                }
                FileUpload3.SaveAs(PhysicalPath);
            }
            if (FileUpload4.HasFile)
            {

                string[] sFileExtension = FileUpload4.FileName.Split('.');
                string sFileName = "Image_4" + "." + sFileExtension[1];
                //string VirtualPath = "~/PromoImages/" + sFileName;
                string sFolderName = Serverpath + "\\" + nID;
                string PhysicalPath = sFolderName + "\\" + sFileName;
                sfinalArray[4] = "Image_4" + "." + sFileExtension[1];
                //sFileNameArray += "Image_4" + "." + sFileExtension[1] + "^_^";
                if (!Directory.Exists(sFolderName))
                {
                    Directory.CreateDirectory(sFolderName);
                }
                else
                {
                    string[] files = Directory.GetFiles(sFolderName);
                    foreach (string fileName in files)
                    {
                        if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                        {
                            File.Delete(fileName);
                        }
                    }
                }
                FileUpload4.SaveAs(PhysicalPath);
            }
            foreach (string s in sfinalArray)
            {
                if (s != "" && s != null)
                {
                    sFileNameArray += s + "^_^";
                }
            }
            sFileNameArray = sFileNameArray.Substring(0, sFileNameArray.LastIndexOf("^_^"));
            //string PhysicalPath = Server.MapPath(VirtualPath);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('Images uploaded successfully')", true);
            DBHelper.DBReturnCode retCode = PackageManager.add_ProductImagesData(nID, sFileNameArray);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ADMINFORCEUPDATE", "GetCurrentImages(" + nID + ");", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scr", "Javascript:alert('Something went wrong while processing your request! Please try again.')", true);
            }
            //objIM.ImpersonateEnd();
        }
    }
}