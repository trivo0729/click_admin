﻿using CutAdmin.dbml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for StaticPackage
    /// </summary>
    public class StaticPackage : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            helperDataContext db = new helperDataContext();
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            jsSerializer.MaxJsonLength = Int32.MaxValue;
            //CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];

            string sFileName = System.Convert.ToString(context.Request.QueryString["sid"]);



            if (context.Request.Files.Count > 0)
            {
                int rowsAffected = 0;
                bool ShowFlag = true;
                var VirtualPath = "";
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string myFilePath = file.FileName;
                    string ext = Path.GetExtension(myFilePath);
                    if (ext != ".pdf")
                    {
                        string FileName = file.FileName;
                        FileName = Path.Combine(context.Server.MapPath("~/StaticImage/"), sFileName + ext);
                        file.SaveAs(FileName);
                        VirtualPath = "~/StaticImage/" + file.FileName;
                    }

                    else
                    {
                        context.Response.Write("0");

                    }
                    //return json;
                }

            }

            else
            {

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}