﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddRate1.aspx.cs" Inherits="CutAdmin.AddRate1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/AddRate2.js?v=2.6"></script>
     <script src="Scripts/AddOnsCharges.js?v=2.3"></script>
      <link rel="stylesheet" href="css/styles/form.css?v=1">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
    <!-- DatePicker-->
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.css">
    <!-- JavaScript at bottom except for Modernizr -->
    <script src="js/libs/modernizr.custom.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <!-- Main content -->
    <section role="main" id="main">

        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        
        <hgroup id="main-title" class="thin">
			<h1>Add Rate Details</h1>
            <h2><b class="grey"><strong>HOTEL</strong>: <strong><label id="lblHotel"></label></strong></b></h2>
            <hr/>			
		</hgroup>
        <div class="with-padding">
        <form id="frm" class="block margin-bottom wizard same-height">
            
                  <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Main</legend>
                      <div class="field-block button-height">
                        <small class="input-info">Select supplier for which these rates are being added / updated</small>
                        <label for="supplier" class="label"><b>Supplier</b></label>
                        <select name="country" class="select expandable-list loading" style="width: 180px" id="sel_Supplier">
                            <option value="0">Directly from hotel</option>
                        </select>
                       </div>
                      <div class="field-block button-height" id="Currency">
                        <small class="input-info">SELECT IN WHICH CURRENCY YOU WISH TO ADD / UPDATE RATES</small>
                        <label for="supplier" class="label"><b>Currency</b></label>
                        <select name="country" class="select expandable-list loading" style="width: 180px" id="sel_CurrencyCode" >
                        </select>
                       </div>
                      <div class="field-block button-height">
                        <small class="input-info">Select Nationality for which these rates are being added / updated</small>
                        <label for="supplier" class="label"><b>Nationality</b></label>
                        <select name="country" class="select multiple-as-single easy-multiple-selection allow-empty check-list chkNationality loading"  multiple style="width: 180px" id="sel_Nationality" onchange="CheckNationality(this.value);">
                        </select>
                       </div>
                      <div class="field-block button-height">
                        <small class="input-info">SELECT FOR WHICH MEAL PLANS YOU WISH TO ADD/UPDATE RATES</small>
                        <label for="supplier" class="label"><b>Meal Plan</b></label>
                        <select name="country" class="select expandable-list loading" style="width: 180px" id="sel_MealPlan" >
                        </select>
                       </div>
                      <div class="field-block button-height">
                        <small class="input-info">Define tariff type and you may also upload contract / promo file if any</small>
                        <label for="tarifftype" class="label "><b>Tariff Type</b></label>
                        <select name="tarifftype" class="select expandable-list mid-margin-right" style="width: 180px" id="sel_RateType">
                            <option value="contract">Contract</option>
                            <option value="promo">Promotion</option>
                            <option value="tacticaloffer">Tactical Offer</option>
                            <option value="offer">Offer</option>

                        </select>
                        <span class="input file">
                            <span class="file-text"></span>
                            <span class="button compact">Upload file</span>
                            <input type="file" name="file-input" id="file-input" value="" class="file withClearFunctions input ">
                        </span>

                    </div>
                  

                      </fieldset>
                  <fieldset class="wizard-fieldset fields-list">
                  <legend class="legend">Rate Validity</legend>

                        <div class="field-block button-height">
                        <label for="supplier" class="label"><b>General Tarrif</b></label>
                         <label id="clickRV" style="display:none" none">1</label>
                           <div id="idRateValidity" class="cRateValidity">
                          <div class="columns" id="divRV0">
            <div class="three-columns"><span class="input"><span class="icon-calendar"></span><input type="text"  id="txtFromRV0" class="input-unstyled cRateValidFrom" value="" ></span></div>
            <div class="three-columns"><span class="input"><span class="icon-calendar"></span><input type="text"  id="txtToRV0" class="input-unstyled cRateValidTo" value=""> </span></div>
           <div class="one-columns">' <a id="lnkAddRV0"  onclick="AddRateValidity(0);" class="button"><span class="icon-plus-round blue"></span></a></div>
            </div>

                    <div class="columns" style="display:none"none;">
                          <div class="twelve-columns cRVDays" id="iRVDays0" >
                                <small class="input-info">Days:</small>
                                <label for="chkAllDays0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkAllDays0" value="All" onchange="CheckRVAllDays(0);">All</label>
                                <label for="chkSun0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkSun0" value="Sun" onchange="CheckRVSingle(0);">Sun</label>
                                <label for="chkMon0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkMon0" value="Mon" onchange="CheckRVSingle(0);">Mon</label>
                                <label for="chkTue0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkTue0" value="Tue" onchange="CheckRVSingle(0);">Tue</label>
                                <label for="chkWed0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkWed0" value="Wed" onchange="CheckRVSingle(0);">Wed</label>
                                <label for="chkThu0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkThu0" value="Thu" onchange="CheckRVSingle(0);">Thu</label>
                                <label for="chkFri0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkFri0" value="Fri" onchange="CheckRVSingle(0);">Fri</label>
                                <label for="chkSat0" class="label">
                                    <input type="checkbox" class="cDayRV" id="chkSat0" value="Sat" onchange="CheckRVSingle(0);">Sat</label>

                            </div>
                        <br />
                    </div>
                       </div>
                                
                        </div>
                       <div class="field-block button-height" style="padding-left: inherit;">
                           <div style="display:none" id="DivDates">
                    <div class="side-tabs same-height">
						<ul  class="tabs" id="TabRooms" >
						</ul>
						<div class="tabs-content NestesDiv" id="TabContent">
						</div>
					</div>
                            </div>
                           </div>
                 
                     <div class="field-block button-height">
                        <label for="supplier" class="label"  id="spldtheading"><b>Special Dates <input type="checkbox" id="chkSpecialDate" onchange="CheckSpecialDate()" /></b></label>
                         <label id="clickSD" style="display:none" none">1</label>
                    <div id="idSpecialDate" class="cSpecialDate" style="display:none" none">

                        <div class="columns" id="divSD0">

                            <div class="three-columns twelve-columns-mobile four-columns-tablet bold">
                                <label>Valid From</label>
                                <span class="input">
                                    <span class="icon-calendar"></span>
                                    <input type="text" id="txtFromSD0" class="input-unstyled ctxtfromSD" placeholder="Select Date">
                                </span>
                            </div>
                            <div class="three-columns ten-columns-mobile four-columns-tablet bold">
                                <label >Valid To</label>
                                <span class="input">
                                    <span class="icon-calendar"></span>
                                    <input type="text" id="txtToSD0" class="input-unstyled ctxttoSD" placeholder="Select Date" >
                                </span>
                            </div>
                          
                            <div class="three-column plusminus">
                                
                                <a id="lnkAddSD0" onclick="AddSpecialDates(0);" class="button">
                                    <span class="icon-plus-round blue"></span>
                                </a>

                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="columns" id="dispSD" style="display:none">
                          <div class="twelve-columns cSDDays" id="iSDDays0">
                                <small class="input-info">Days:</small>
                                <label for="chkAllSD0" class="label">
                                    <input type="checkbox" id="chkAllSD0" value="All" class="cDaySD" onclick="CheckSDAllDays(0);">All</label>
                                <label for="chkSunSD0" class="label">
                                    <input type="checkbox" id="chkSunSD0" value="Sun" class="cDaySD" onclick="CheckSDSingle(0);">Sun</label>
                                <label for="chkMonSD0" class="label">
                                    <input type="checkbox" id="chkMonSD0" value="Mon" class="cDaySD" onclick="CheckSDSingle(0);">Mon</label>
                                <label for="chkTueSD0" class="label">
                                    <input type="checkbox" id="chkTueSD0" value="Tue" class="cDaySD" onclick="CheckSDSingle(0);">Tue</label>
                                <label for="chkWedSD0" class="label">
                                    <input type="checkbox" id="chkWedSD0" value="Wed" class="cDaySD" onclick="CheckSDSingle(0);">Wed</label>
                                <label for="chkThuSD0" class="label">
                                    <input type="checkbox" id="chkThuSD0" value="Thu" class="cDaySD" onclick="CheckSDSingle(0);">Thu</label>
                                <label for="chkFriSD0" class="label">
                                    <input type="checkbox" id="chkFriSD0" value="Fri" class="cDaySD" onclick="CheckSDSingle(0);">Fri</label>
                                <label for="chkSatSD0" class="label">
                                    <input type="checkbox" id="chkSatSD0" value="Sat" class="cDaySD" onclick="CheckSDSingle(0);">Sat</label>

                            </div>
                    </div>
                    <%--End special Dates--%>

                    <hr/>
                         </div>

                      <div class="field-block button-height" style="padding-left: inherit;">
                           <div class="standard-tabs margin-bottom" id="hdndiv1" style="display:none"></div>
                          <div class="side-tabs same-height">
						<ul class="tabs" id="TabRoomsSpl">
						</ul>
						<div class="tabs-content NestesDivSD" id="TabContentSpl">
						</div>
					</div>
                      </div>
                           <a href="#" class="button anthracite-gradient" id="btn_date" style="cursor:pointer;display:none" onClick="AddDates();" >Add Dates</a>
            </fieldset>

                   
                
                  
                 <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Other Details</legend>
                       <h4 class="txt-detail">Tax Details</h4>
                     <hr/> 
                     <span class="labeltxt">(If Tax Already Included in above Rates)</span>
                      <label>Tax Included:</label>
                              <span class="button-group">
                                  <label for="chkTaxIncluded1" class="button grey-active">
                                      <input type="radio" checked name="button-radio chkTaxIncluded" id="chkTaxIncluded1" value="YES"  onclick="checkTaxApllied(this.value);" >
                                      YES
                                  </label>
                                  <label for="chkTaxIncluded2" class="button grey-active">
                                      <input type="radio"  name="button-radio chkTaxIncluded" id="chkTaxIncluded2" value="NO" onclick="checkTaxApllied(this.value);">
                                      NO
                                  </label>
                                 
                              </span>
                              
                               <input id="txtIncluded" style="display:none"none;"/>
                         
                      <div class="" id="div_TaxDetails">
                          
                     
                          
                       </div>
                      <div class="" id="div_UpdateTaxDetails" style="display:none">
                          
                     
                          
                       </div>
                      <br />
                       <br />

                       <div class="columns">
                        
                          <div class="three-columns twelve-columns-mobile addinput ClassDynamic">
                              <label>Inclusion:</label>
                               <ul id="myUL" style="margin-left:0px;">
                                  
                              </ul>
                              <div id="myDIV" class="header">
                                  <input type="text" id="Inclusions" placeholder="Title..." class="input full-width">
                                  <span onclick="InclusionsAdd()" class="button anthracite-gradient">Add</span>
                              </div>

                             <div id="idinclusionSpl" style="height:110px" class="Inclusion incspltktundefined"> </div>
                          </div>
                          <div class="three-columns twelve-columns-mobile addinput  ClassDynamic">
                              <label>Exclusion:</label>
                               <ul id="myUL1" style="margin-left:0px;">
                                  
                              </ul>
                              <div id="myDIV1" class="header">
                               <input type="text" id="Exclusions" placeholder="Title..." class="input full-width">
                                  <span onclick="ExclusionsAdd()" class="button anthracite-gradient">Add</span>
                              </div>
                              <div id="idinclusionSpl1" style="height:110px" class="Inclusion incspltktundefined"> </div>
                          </div>
                          </div>
                      <div class="columns">
                           <div class="two-columns twelve-columns-mobile">
                              <label>Min Stay:</label>
                              <input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtminStay" class="input full-width">
                          </div>
                           <div class="two-columns twelve-columns-mobile">
                              <label>Max Stay:</label>
                              <input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtmaxStay" class="input full-width">
                          </div>
                          <div class="two-columns twelve-columns-mobile">
                              <label>Min Room:</label>
                              <input type="text" onkeypress="return event.charCode >= 31 && event.charCode <= 57" id="txtminRoom" class="input full-width">
                          </div>
                            <div class="two-columns twelve-columns-mobile SelBox">
                              <label>Upload Contract:</label>
                              <input type="file" id="uploadContract" value="" class="file">
                          </div>
                      </div>
                     </fieldset>

        </form>
                 
            </div>
    </section>
    <script>

        $(document).ready(function () {
            debugger
            //setTimeout(function() {
               
            //},1000)
            // Elements
            var form = $('.wizard'),

                // If layout is centered
                centered;

            // Handle resizing (mostly for debugging)
            function handleWizardResize() {
                centerWizard(false);
            };

            // Register and first call
            $(window).on('normalized-resize', handleWizardResize);

            /*
             * Center function
             * @param boolean animate whether or not to animate the position change
             * @return void
             */
            function centerWizard(animate) {
                form[animate ? 'animate' : 'css']({ marginTop: Math.max(0, Math.round(($.template.viewportHeight - 30 - form.outerHeight()) / 2)) + 'px' });
            };

            // Initial vertical adjust
            centerWizard(false);

            // Refresh position on change step
            form.on('wizardchange', function () { centerWizard(true); });
            // Validation

            if ($.validationEngine) {
                debugger
                form.validationEngine();
            }
            form.showWizardPrevStep(true)
        });
    </script>
</asp:Content>
