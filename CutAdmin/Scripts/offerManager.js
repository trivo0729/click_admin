﻿var arrPaxces = ["RR", "EB", "CW", "CN"]
$(function () {
    $("#btn_Block").change(function () {
        if($(this).is(":checked"))
        {
            $(".Blockout").show(500);
        }
        else
        {
            $(".Blockout").hide(500);
        }
    });
    GetCountry()
    GenrateDates('Offer');
    GenrateDates('Block');
    $("#div_Early").click();
    $("#Sel_OfferBy").change(function () {
        if ($(this).val() == "supplier")
            $("#div_Suppier").removeClass("hidden")
        else
            $("#div_Suppier").addClass("hidden")
    });
   
    $('.wizard fieldset').on('wizardleave', function () {
        var sStep = $(this).find("legend").text();
        if (sStep == "Offers") {
            OfferType();
        }
        else if(sStep=="Hotel")
        {
            OfferFor();
            $("#RoomsRate").empty();
            for (var r = 0; r < arrRoom.length; r++) {
                if ($("#chk" + arrRoom[r].RoomID).is(":checked")) {
                    if ($("#RoomsRate").find("#tx_Room" + arrRoom[r].RoomID).length == 0)
                    {
                        $("#RoomsRate").append(' <div class="three-columns twelve-columns-mobile">' +
                            '<label>' + arrRoom[r].RoomName + '    </label>' +
                            '<span class="input">' +
                            '<label for="RR" class="button green-gradient glossy">RR</label>' +
                            '<input type="text" name="RR" id="tx_RoomRR' + arrRoom[r].RoomID + '" class="input-unstyled validate[required,custom[onlyNumberSp]]" style="width: 100px" placeholder="12,345">' +
                            '</span>' +
                            '</div>')

                        $("#RoomsRate").append(' <div class="three-columns twelve-columns-mobile">' +
                            '<label><br/></label>' +
                            '<span class="input">' +
                            '<label for="earlybookingnewrate1" class="button green-gradient glossy">EB</label>' +
                            '<input type="text" name="EB" id="tx_RoomEB' + arrRoom[r].RoomID + '" class="input-unstyled validate[required,custom[onlyNumberSp]]" style="width: 100px" placeholder="12,345">' +
                            '</span>' +
                            '</div>')

                        $("#RoomsRate").append(' <div class="three-columns twelve-columns-mobile">' +
                            '<label><br/></label>' +
                            '<span class="input">' +
                            '<label for="earlybookingnewrate1" class="button green-gradient glossy">CWB</label>' +
                            '<input type="text" name="CWB" id="tx_RoomCW' + arrRoom[r].RoomID + '" class="input-unstyled validate[required,custom[onlyNumberSp]]" style="width: 100px" placeholder="12,345">' +
                            '</span>' +
                            '</div>')


                        $("#RoomsRate").append(' <div class="three-columns twelve-columns-mobile">' +
                           '<label><br/></label>' +
                           '<span class="input">' +
                           '<label for="earlybookingnewrate1" class="button green-gradient glossy">CNB</label>' +
                           '<input type="text" name="CNB" id="tx_RoomCN' + arrRoom[r].RoomID + '" class="input-unstyled validate[required,custom[onlyNumberSp]]" style="width: 100px" placeholder="12,345">' +
                           '</span>' +
                           '</div>')
                    }
                }
            }
        }
        else
            OfferFor();
    });;
    /*Hotel Selection*/
    setTimeout(function () {
        AutoSelect("txt_HotelName", { name: name, ParentID: HotelAdminID }, "./HotelHandler.asmx/GetHotelList", function (data) {
            GetRoomByCode(data);
            GetTypeByHotel(data)
            GetCurrencyByHotel(data)
            $("#hdn_HotelCode").val(data);
        });
    }, 800);
    $("#dte_DayPrior").datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: "d-m-yy",
      minDate: "dateToday",
      maxDate: "+12M +10D"
    });
});
var datepickersOpt = {
    dateFormat: 'dd-mm-yy'
}
function GenrateDates(RateType) {
    debugger
    var html = "";
    var required = "validate[required]"
    try {
        if (RateType == "Block")
            required = "";
        var elem = $(".dte" + RateType);
        html += '<div class="columns dte' + RateType + '">'
        html += '<div class="four-columns ten-columns-mobile five-columns-tablet">'
        html += '<span class="input pointer">'
      
        html += '<input type="text" class="input-unstyled pointer datepicker  ' + required + ' ' + RateType + 'From" value="">'
        html += '<span class="icon-calendar"></span>'
        html += '</span>'
        html += '</div>'
        html += '<div class="five-columns twelve-columns-mobile seven-columns-tablet">'
        html += '<span class="input pointer">'
        html += '<input type="text" class="input-unstyled pointer datepicker  '+ required + '  ' + RateType + 'To" value="">'
        html += '<span class="icon-calendar"></span>'
        html += '</span>';
        if (elem.length == 0) {

            html += '<span class="mid-margin-left icon-size2 icon-plus-round icon-black" onclick="GenrateDates(\'' + RateType + '\')" id="btn_Add' + RateType + 'Date"></span>'
        }
        else
            html += '<span class="mid-margin-left icon-size2 icon-minus-round icon-red remCF" ></span>';
        html += '</div>'
        html += '</div>'
        $("#div_" + RateType + "Date").append(html);
        $(".remCF").on('click', function () {
            $(this).parent().parent().remove();
        });
        var elem_FromDate = $("#div_" + RateType + "Date").find("." + RateType + "From");
        var elem_ToDate = $("#div_" + RateType + "Date").find("." + RateType + "To");
        for (var i = 0; i < elem_FromDate.length; i++) {
            /*Previous Date Select Date*/
            var previousFrom = 0;
            var previousTo = 0;
            if (i != 0)
                previousTo = moment($(elem_ToDate[i - 1]).val(), "DD-MM-YYYY");
            var minDate = 0;
            if (previousTo != 0)
                minDate = previousTo._i;
            $(elem_FromDate[i]).datepicker($.extend({
                minDate: minDate,
                onSelect: function () {
                    debugger
                    var minDate = $(this).datepicker('getDate');
                    minDate.setDate(minDate.getDate() + 1); //add One days
                    $(elem_ToDate[i - 1]).datepicker("option", "minDate", minDate);
                }, beforeShow: function () {
                    if (RateType != "Offer") {
                        var minDate = moment($($("#div_OfferDate").find(".OfferFrom")[i - 1]).val(), "DD-MM-YYYY")._i;
                        var maxDate = moment($($("#div_OfferDate").find(".OfferTo")[i - 1]).val(), "DD-MM-YYYY")._i;
                        $(this).datepicker("option", "minDate", minDate);
                        $(this).datepicker("option", "maxDate", maxDate);
                    }
                },
            }, datepickersOpt));

            $(elem_ToDate[i]).datepicker($.extend({
                onSelect: function () {
                    var maxDate = $(this).datepicker('getDate');
                    maxDate.setDate(maxDate.getDate());
                    UpdateFlag = false;
                }, beforeShow: function () {
                    if (RateType != "Offer") {
                        var minDate = moment($($("#div_OfferDate").find(".OfferFrom")[i - 1]).val(), "DD-MM-YYYY")._i;
                        var maxDate = moment($($("#div_OfferDate").find(".OfferTo")[i - 1]).val(), "DD-MM-YYYY")._i;
                        $(this).datepicker("option", "minDate", minDate);
                        $(this).datepicker("option", "maxDate", maxDate);
                    }
                },
            }, datepickersOpt));
        }
    } catch (e) { }
}

function fnChangeBorder(boxId)
{
    $('.offerDIV').css("border", "");
    $('.offerDIV').css("margin", "0 0 20px 1.25%");
    $(boxId).css("border", "solid #AA00FF");
    var ndoffer = $(boxId).find("input:radio");
    $(ndoffer).prop("checked", true);
}


function OfferType() {
    try {
        if ($("#rdb_Early").is(":checked"))
        {
            $(".lbl_offerType").text("Early Booking");
            $(".div_earlybookingcond").removeClass("hidden");
            $("#div_FreeNightCond").addClass("hidden");
            $("#div_LastMinCond").addClass("hidden");
        }
        else if ($("#rdb_Nights").is(":checked")) {
            $(".lbl_offerType").text("Free Nights");
            $(".div_earlybookingcond").addClass("hidden");
            $("#div_FreeNightCond").removeClass("hidden");
            $("#div_LastMinCond").addClass("hidden");
            $(".div_discount").addClass("hidden");
            $(".div_rate").addClass("hidden");
        }
        else if ($("#rdb_Minute").is(":checked")) {
            $(".lbl_offerType").text("Last Minute");
            $(".div_earlybookingcond").addClass("hidden");
            $("#div_FreeNightCond").addClass("hidden");
            $("#div_LastMinCond").removeClass("hidden");
        }
        else if ($("#rdb_AddOns").is(":checked")) {
            $(".lbl_offerType").text("Free AddOns");
            $(".div_earlybookingcond").addClass("hidden");
            $("#div_FreeNightCond").addClass("hidden");
            $("#div_LastMinCond").addClass("hidden");
            $(".div_discount").addClass("hidden");
            $(".div_rate").addClass("hidden");
        } /*Offer Type Preview*/
        $(".div_Validity").empty();
        $(".div_Validity").append('<label class="green strong">Validity</label>');
        var ndOfferDate = $(".dteOffer");
        $(ndOfferDate).each(function (index, ndOffer) {
            var sFrom = $($(ndOffer).find('.OfferFrom')).val();
            var sTo = $($(ndOffer).find('.OfferTo')).val();
            $(".div_Validity").append('   <label class="black">' + sFrom + ' to '+sTo+'</label>,')
        });
    } catch (e) { }
}

function GetCountry() {
    try {
        post("GenralHandler.asmx/GetCountry", {}, function (data) {
            debugger
            $(data.Country).each(function (index, arrCountry) {
                if (index == 0)
                    $('#sel_Country').append($('<option selected="selected"></option>').val("AllCountry").html("All Market"));
                else
                    $('#sel_Country').append($('<option></option>').val(arrCountry.Country).html(arrCountry.Countryname));
                $('#sel_Country').change();
            });
        }, function (errordata) {
            alertDanger(errordata.ex)
        });
    } catch (e) { AlertDanger(e.message) }
}

function OfferRateType(Type) {
    try {
        if(Type =="discount")
        {
            $(".div_discount").removeClass("hidden");
            $(".div_rate").addClass("hidden");
        }
        else
        {
            $(".div_discount").addClass("hidden");
            $(".div_rate").removeClass("hidden");
            
        }
        

    } catch (e) {
    }
}
var arrRates = new Array();
function OfferFor() {
    try {
        arrRates = new Array();
        $(".hotelname").text($("#txt_HotelName").val());
        $(".Provider").text($("#Sel_OfferBy").val());
        $(".Provider").text($("#Sel_OfferBy").val());
        $(".OfferName").text($("#txt_offerName").val());
        var ndRooms = $("#div_Room").find("input:checkbox");
        $(".Rooms").empty();
        for (var i = 0; i < ndRooms.length; i++) {
            if($(ndRooms[i]).is(":checked"))
            {
                $(".Rooms").append($(ndRooms[i]).val() + " ,");
            }
        }
        var ndRates = $("#div_Rates").find("input:checkbox");
        $(".rateType").empty();
        //for (var i = 0; i < ndRates.length; i++) {
        //    if ($(ndRates[i]).is(":checked")) {
        //        $(".rateType").append($(ndRates[i]).val() + " ,");
        //    }
        //}
        $(".rateType").append($("#sel_RateType").val());
        $(".market").text($("#sel_Country").val())
        $(".PromoCode").text($("#txt_PromoCode").val())

        if (!$(".div_rate").hasClass("hidden"))
        {
            for (var r = 0; r < arrRoom.length; r++) {
                if ($("#chk" + arrRoom[r].RoomID).is(":checked")) {
                    $("#").append(' <div class="three-columns"><label class="green strong">' + $("#chk" + arrRoom[r].RoomID).val() + '</label>' +
                                '<p class="strong">AED ' + $("#tx_Room" + arrRoom[r].RoomID).val() + '</p>' +
                            '</div>')
                }
            }
        }
        else if (!$(".div_discount").hasClass("hidden"))
        {

        }
        $(".Note").text($("#txt_Note").val());
        $("#div_Condition").empty()
        if ($("#rdb_Early").is(":checked") || $("#rdb_Minute").is(":checked"))
        {
            var sOfferType = "";
            if ($("#rdb_Early").is(":checked") && $("#offerdiscount").is(":checked")) {
                sOfferType = "Discount";
            }
            else if ($("#rdb_Early").is(":checked"))
                sOfferType = "Amount"
            if ($("#rdb_Minute").is(":checked") && $("#lasmindiscount").is(":checked")) {
                sOfferType = "Discount";
            }
            else if ($("#rdb_Minute").is(":checked"))
                sOfferType = "Amount"
            var HotelCode = $("#hdn_HotelCode").val();
            $(arrRoom).each(function (index, Room) {
                if ($("#chk" + Room.RoomID).is(":checked")) {
                    var arrRateTypes = $("#sel_RateType").val();
                    $(arrRateTypes).each(function (index, arrRateType) {
                        var Rate = "";
                        var isDiscount = false;
                        var RateType = '';
                        var sCurrency = "";
                        if (sOfferType == "Discount") {
                            Rate = $("#txt_Discount").val();
                            if ($("#SelCurrency").val() != "%")
                                sCurrency = $("#SelCurrency").val()
                            isDiscount = true;
                            arrRates.push({
                                Rate: Rate,
                                HotelCode: HotelCode,
                                RoomID: Room.RoomID,
                                isDiscount: true,
                                RateType: arrRateType,
                                sCurrency: sCurrency
                            });
                        }
                        else {
                            $(arrPaxces).each(function (index, arrPax) {
                                arrRates.push({
                                    Rate: $("#tx_Room" + arrPax + Room.RoomID).val(),
                                    HotelCode: HotelCode,
                                    RoomID: Room.RoomID,
                                    isDiscount: false,
                                    RateType: arrRateType,
                                    sPaxType: arrPax
                                });
                            })
                        }
                    });
                }
            });
        }
        if(arrRates.length ==0)
            $("#spn_Rates").addClass("hidden");
        else
            $("#spn_Rates").addClass("hidden");
          //  $("#spn_Rates").removeClass("hidden");

        var sCondition = "";
        if ($("#rdb_Early").is(":checked"))
        {
            if($("#earlybookingcond1").is(":checked"))
                sCondition += " Offer will apply if booked " + $("#txt_DayPriorEarly").val() + " days prior (before) to check in date."
            else if($("#earlybookingcond2").is(":checked"))
                sCondition += "Offer will apply if the booking is done before " + $("#dte_DayPrior").val() + " ."
        }
        else if($("#rdb_Minute").is(":checked"))
        {
            if ($("#lastminstartdays").is(":checked"))
                sCondition += "Start this offer " + $("#txt_LastDay").val() + " days(s) before check-in day."
            else if ($("#lastminstarthr").is(":checked"))
                sCondition += "Start this offer " + $("#txt_LastHours").val() + "  hours before check-in day."
        }
        else if ($("#rdb_Minute").is(":checked")) {
            if ($("#lastminstartdays").is(":checked"))
                sCondition += "Start this offer " + $("#txt_LastDay").val() + " days before check-in day."
            else if ($("#lastminstarthr").is(":checked"))
                sCondition += "Start this offer " + $("#txt_LastHours").val() + "  hours before check-in day."
        }
        else if ($("#rdb_Nights").is(":checked"))
        {
            sCondition += "If the booking is done for min " + $("#txt_MinNight").val() + "nights " + $("#txt_FreeNight").val() + " free nights will be granted."
        }
        $("#div_Condition").append(sCondition);

    } catch (e) { alert(e.message) }
}

function GetCurrencyByHotel(HotelCode) {
    try {
        post("../handler/GenralHandler.asmx/GetCurrencyByHotel", { HotelCode: HotelCode }, function (data) {
            $('#SelCurrency').empty();
            $("#SelCurrency").append($('<option selected="selected"></option>').val("%").html("%"))
            $(data.arrCurrency).each(function (index, item) { // GETTING Sucees HERE
                $('#SelCurrency').append($('<option></option>').val(item).html(item));
                $('#SelCurrency').css("width", "100px");
                $('#SelCurrency').change();
            });
        }, function (responsedata) {// GETTING ERROR HERE
            AlertDanger(responsedata.ex)
        });
    } catch (ex) { }
}  /*Hotel Rate Currency*/

function GetSelectedRooms() {
    var arrRooms = new Array();
    try {
        ndRooms = $("#div_Room").find("input:checkbox");
        for (var i = 0; i < ndRooms.length; i++) {
            if ($(ndRooms[i]).is(":checked")) {
                arrRooms.push({ id: ndRooms[i].id, name: $(ndRooms[i]).val() })
            }
        }
    } catch (e) { }
    return arrRooms;
} /*Selected Rooms*/

function SaveOffer() {
    try {
        var arrOffer = new Array();
        var ndDates = $(".dteOffer");
        var Market = $("#").val();
        var Category = "";
        var DaysPrior = "";
        var OfferType = "";
        var BookBefore = "";
        var DiscountPercent = 0;
        var DiscountAmount = 0;
        var MinNight = 0;
        var FreeNight = 0;
        var blockout = '';
        var PromoCode = $("#txt_PromoCode").val();
        var Note = $("#txt_Note").val();
        var NewRate = new Array();
        var HotelCode = $("#hdn_HotelCode").val();
        var Market = "";
        var arrMarket = $("#sel_Country").val();
        var Market = "";
        $(arrMarket).each(function (index, item) {
            Market += item + ',';
        });
        if ($("#rdb_blockoutDay").is(":checked"))/* Checking Block DDay*/
        {
            var ndDays = $($("#div_blockout").find("input:checkbox"));
            $(ndDays).each(function (index, Day) {
                if ($(Day).is(":checked"))
                    blockout += $(Day).val();
            });
        }
        if ($("#rdb_Early").is(":checked"))
        {
            Category = "Early Booking";
            if ($("#offerdiscount").is(":checked"))
            {
                OfferType = "Discount";
            }
            else
                OfferType = "Amount"
            if ($("#earlybookingcond1").is(":checked"))
                DaysPrior = $("#txt_DayPriorEarly").val();
            else
                BookBefore = $("#dte_DayPrior").val();
        }
        else if ($("#rdb_Nights").is(":checked"))
        {
            Category = "Free Nights";
            MinNight = $("#txt_MinNight").val();
            FreeNight = $("#txt_FreeNight").val();
            OfferType = "Night"
        }
        else if ($("#rdb_Minute").is(":checked"))
        {
            Category = "Last Minute"; 
            if ($("#lastminstartdays").is(":checked"))
                DaysPrior = $("#txt_LastDay").val() + "Day";
            else
                DaysPrior = $("#txt_LastHours").val() + "Hr";

            if ($("#lasmindiscount").is(":checked")) {
                OfferType = "Discount";
            }
            else
                OfferType = "Amount"
        }
        else if ($("#rdb_AddOns").is(":checked"))
            Category = "Free AddOns";
       
        $(ndDates).each(function (index, ndDate) {
            var ndFromDates = $(ndDate).find(".OfferFrom")[0];
            var ndToDates = $(ndDate).find(".OfferTo")[0];
            arrOffer.push({
                OfferNationality: Market,
                Category: Category,
                SeasonName: $("#txt_offerName").val(),
                ValidFrom: $(ndFromDates).val(),
                ValidTo: $(ndToDates).val(),
                DateType: 'S',
                DaysPrior: DaysPrior,
                OfferType: OfferType,
                BookBefore: BookBefore,
                DiscountPercent: DiscountPercent,
                DiscountAmount: "",
                NewRate: "",
                FreeItemName: "",
                FreeItemDetail: "",
                OfferTerm: "",
                OfferNote: Note,
                HotelOfferCode: PromoCode,
                CutOfferCode: PromoCode,
                Active: true,
                BlockDay: blockout,
                SupplierID: HotelAdminID,
                MinNight: MinNight,
                FreeNight: FreeNight,
            });
        });  /*Offer Range Validity*/
        /*Block Days*/
        if ($("#rdb_blockout").is(":checked")) {
            $(ndDates).each(function (index, ndDate) {
                var ndFromDates = $(ndDate).find(".OfferFrom")[0];
                var ndToDates = $(ndDate).find(".OfferTo")[0];
                arrOffer.push({
                    OfferNationality: Market,
                    Category: Category,
                    SeasonName: $("#txt_offerName").val(),
                    ValidFrom: $(ndFromDates).val(),
                    ValidTo: $(ndToDates).val(),
                    DateType: 'D',
                    DaysPrior: DaysPrior,
                    OfferType: OfferType,
                    BookBefore: BookBefore,
                    DiscountPercent: DiscountPercent,
                    DiscountAmount: "",
                    NewRate: "",
                    FreeItemName: "",
                    FreeItemDetail: "",
                    OfferTerm: "",
                    OfferNote: Note,
                    HotelOfferCode: PromoCode,
                    CutOfferCode: PromoCode,
                    Active: true,
                    BlockDay: blockout,
                    SupplierID: HotelAdminID,
                    MinNight: MinNight,
                    FreeNight: FreeNight,
                });
            });
        } /*Block Days */
        var arrRooms = GetSelectedRooms();
        var arrRoomID = [];
        debugger
        $(arrRooms).each(function (index, arrRoom) {
            arrRoomID.push(arrRoom.id.replace("chk",""));
        })
        post("../MasterHotelOfferHandler.asmx/SaveOffers", {
            arrOffers: arrOffer,
            NewRate: arrRates,
            HotelCode: HotelCode,
            arrRoomID: arrRoomID
        }, function (data) {
            Success("Offer Saved Successfully");
            ConfirmModal("Offer Saved Successfully, Are you Want another offers", Reload, {});
        }, function(error) {
            AlertDanger(error.ex)
        })
    } catch (e) { AlertDanger(e.message) }
}
function Reload() {
    window.location.reload()
}