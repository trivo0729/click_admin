﻿
$(document).ready(function () {
   
});

function AddMail() {
    $.ajax({
        type: "POST",
        url: "../handler/MailConfighandler.asmx/GetMailConfig",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var Arr = result.Arr;
                if (Arr != null) {
                    $.modal({
                        content: '<div class="modal-body" id="AddMailModel">' +
                             '<div class="columns">' +
                             '<div class="six-columns twelve-columns-mobile"><label><b>User Name<b/><span class="red">*</span>:</label> <div class="input full-width">' +
                             '<input name="prompt-value" id="txt_UserName" placeholder="User Name" autocomplete="off" value="' + Arr.userName + '" class="input-unstyled full-width"  type="text">' +
                             '</div></div>' +
                             '<div class="six-columns twelve-columns-mobile"><label><b>Password<b/><span class="red">*</span>:</label> <div class="input full-width">' +
                             '<input name="prompt-value" id="txt_PassWord" placeholder="Password" autocomplete="off" value="' + Arr.password + '" class="input-unstyled full-width"  type="text">' +
                             '</div></div>' +
                             '<div class="six-columns twelve-columns-mobile"><label><b>Access Key<b/><span class="red">*</span>: </label> <div class="input full-width">' +
                             '<input name="prompt-value" id="txt_AccessKey" placeholder="Access Key" autocomplete="off" value="' + Arr.accessKey + '"   class="input-unstyled full-width"  type="text">' +
                             '</div></div>' +
                              '<div style="float:right;margin-top:2%">' +
                                '<button type="button" onclick="AddMailConfig()" title="Submit Details" class="button anthracite-gradient AddMail">Add</button></div>' +
                              '</div>' +

                              '</div>' +
                              '</div>',
                        title: 'Update SendInBlue Mail Configuration',
                        width: 650,
                        scrolling: false,
                        actions: {
                            'Close': {
                                color: 'red',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttons: {
                        },
                        buttonsLowPadding: true
                    });
                }

                else {
                    $.modal({
                        content: '<div class="modal-body" id="AddMailModel">' +
                             '<div class="columns">' +
                             '<div class="six-columns twelve-columns-mobile"><label><b>User Name<b/><span class="red">*</span>:</label> <div class="input full-width">' +
                             '<input name="prompt-value" id="txt_UserName" placeholder="User Name" autocomplete="off" value="" class="input-unstyled full-width"  type="text">' +
                             '</div></div>' +
                             '<div class="six-columns twelve-columns-mobile"><label><b>Password<b/><span class="red">*</span>:</label> <div class="input full-width">' +
                             '<input name="prompt-value" id="txt_PassWord" placeholder="Password" autocomplete="off" value="" class="input-unstyled full-width"  type="text">' +
                             '</div></div>' +
                             '<div class="six-columns twelve-columns-mobile"><label><b>Access Key<b/><span class="red">*</span>: </label> <div class="input full-width">' +
                             '<input name="prompt-value" id="txt_AccessKey" placeholder="Access Key" autocomplete="off"  class="input-unstyled full-width"  type="text">' +
                             '</div></div>' +
                              '<div style="float:right;margin-top:2%">' +
                                '<button type="button" onclick="AddMailConfig()" title="Submit Details" class="button anthracite-gradient AddMail">Add</button></div>' +
                              '</div>' +

                              '</div>' +
                              '</div>'
            ,

                        title: 'Add SendInBlue Mail Configuration',
                        width: 650,
                        scrolling: false,
                        actions: {
                            'Close': {
                                color: 'red',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttons: {
                            //'Close': {
                            //    classes: 'huge anthracite-gradient glossy full-width',
                            //    click: function (win) { win.closeModal(); }
                            //}
                        },
                        buttonsLowPadding: true
                    });
                }

            }
            else {
                alert("An error occured !!!");
            }
        },
    });
}

//function UpdateMail(nID, userName, password, accessKey) {
//    $.modal({
//        content: '<div class="modal-body" id="AddMailModel">' +
//             '<div class="columns">' +
//             '<div class="six-columns twelve-columns-mobile"><label><b>User Name<b/><span class="red">*</span>:</label> <div class="input full-width">' +
//             '<input name="prompt-value" id="txt_UserName" placeholder="User Name" autocomplete="off" value="' + userName + '" class="input-unstyled full-width"  type="text">' +
//             '</div></div>' +
//             '<div class="six-columns twelve-columns-mobile"><label><b>Password<b/><span class="red">*</span>:</label> <div class="input full-width">' +
//             '<input name="prompt-value" id="txt_PassWord" placeholder="Password" autocomplete="off" value="' + password + '" class="input-unstyled full-width"  type="text">' +
//             '</div></div>' +
//             '<div class="six-columns twelve-columns-mobile"><label><b>Access Key<b/><span class="red">*</span>: </label> <div class="input full-width">' +
//             '<input name="prompt-value" id="txt_AccessKey" placeholder="Access Key" autocomplete="off" value="' + accessKey + '" class="input-unstyled full-width"  type="text">' +
//             '</div></div>' +
//              '<div style="float:right;margin-top:2%">' +
//                '<button type="button" onclick="AddMailConfig()" title="Submit Details" class="button anthracite-gradient AddMail">Add</button></div>' +
//              '</div>' +

//              '</div>' +
//              '</div>'
//            ,

//        title: 'Add SendInBlue Mail Configuration',
//        width: 650,
//        scrolling: false,
//        actions: {
//            'Close': {
//                color: 'red',
//                click: function (win) { win.closeModal(); }
//            }
//        },
//        buttons: {
//            //'Close': {
//            //    classes: 'huge anthracite-gradient glossy full-width',
//            //    click: function (win) { win.closeModal(); }
//            //}
//        },
//        buttonsLowPadding: true
//    });
//}
function AddMailConfig() {
    var userName = $("#txt_UserName").val();
    if (userName == "") {
        Success("Please enter User Name");
        return false;
    }
    var password = $("#txt_PassWord").val();
    if (password == "") {
        Success("Please enter Password.");
        return false;
    }
    var accessKey = $("#txt_AccessKey").val();
    if (accessKey == "") {
        Success("Please enter Access Key");
        return false;
    }

    var data = {
        userName: userName,
        password: password,
        accessKey: accessKey
    }
    post("../handler/MailConfighandler.asmx/AddMailConfig", data, function (data) {
        $("#AddMailModel").modal("hide")
        Success("Mail Configuration saved successfully.");
        setTimeout(function () {
            window.location.reload();
        }, 2000);
    },
    function (error) {
        Success("Something went wrong.");

    })

}




//function DeleteMailConfig(ParentId,userName)
//{
//    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete <span class=\"orange\">' + userName + '</span>?</span>?</p>',
//            function () {
//                var data = {
//                    ParentId: ParentId
//                }
//                $.ajax({
//                    type: "POST",
//                    url: "../GenralHandler.asmx/DeleteBankDetail",
//                    data: JSON.stringify(data),
//                    contentType: "application/json; charset=utf-8",
//                    datatype: "json",
//                    success: function (response) {
//                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//                        if (result.Session == 0 && result.retCode == 0) {
//                            Success("Your session has been expired!")
//                            setTimeout(function () {
//                                window.location.href = "../Default.aspx";
//                            }, 2000);
//                        }
//                        else if (result.retCode == 1) {
//                            Success("Mail Configuration detail deleted successfully.");
//                            setTimeout(function () {
//                                window.location.reload();
//                            }, 2000);
//                        }
//                    },
//                    error: function () {
//                        Success("An error occured while deleting details.");
//                    }
//                });
//            },
//            function () {
//                $('#modals').remove();
//            }
//        );
//}



