﻿var arrMealPlan = new Array();
var arrMeals = new Array();
var content = '<div class="Exchange" >' +
    '<input type="hidden" id="Hdn_Plan" value="0" />' +
    '<div class="columns">' +
    '<div class="six-columns twelve-columns-mobile">' +
    '<label>Meal Plan:</label>' +
    '<input class="input full-width" id="txt_Plan" type="text" autocomplete="off">' +
    '<label style="color: red; margin-top: 3px; display: none" id="lbl_txt_Plan">' +
    '<b>* This field is required</b></label>' +
    '</div>' +

    '<div class="six-columns twelve-columns-mobile">' +
    '<label>Meals:</label>' +
    '<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>' +
    '<select id="sel_Meal" name="validation-select" class="select multiple-as-single easy-multiple-selection check-list expandable-list full-width" multiple></select>' +
    '<label style="color: red; margin-top: 3px; display: none" id="lbl_txt_FCY1">' +
    '<b>* This field is required</b></label>' +
    '</div>' +

    '</div>' +
    '<div class="clear"></div>' +
    '</div>';




$(function () {
    GetMealPlans();
});
function GetMealPlans() {
    $("#tbl_MealPlans").dataTable().fnClearTable();
    $("#tbl_MealPlans").dataTable().fnDestroy();
    post("../handler/GenralHandler.asmx/GetMealPlans", {}, function (data) {
        var html = '';
        arrMeals = data.arrMeals;
        arrMealPlan = data.arrMealPlan;
        $(data.arrMealPlan).each(function (index, MealPlan) {
            html += '<tr>';
            html += '<td>' + (index + 1) + '</td>'
            html += '<td>' + MealPlan.Meal_Plan + '</td>';
            html += '<td>'
            for (var i = 0; i < MealPlan.arrMealID.length; i++) {
                html += '<small class="tag green-bg">' + $.grep(arrMeals, function (H) { return H.ID == MealPlan.arrMealID[i] })
                    .map(function (H) { return H.AddOnName; })[0] + ' </small> '
            }
            html += '</td>'
            html += '<td>'
            html += '<div class="button-group compact">';
            html += '<a  class="button icon-pencil confirm" onclick="UpdateMeals(this,\'' + MealPlan.ID + '\',\'' + MealPlan.Meal_Plan + '\');"></a>';
            html += '<a   class="button icon-trash confirm" onclick="Delete(this,\'' + MealPlan.ID + '\',\'' + MealPlan.Meal_Plan + '\')"></a>';
            html += '</div>'
            html += '</td >'
            html += '</tr>';
        });
        $("#tbl_MealPlans").append(html);
        $("#tbl_MealPlans").dataTable({
            bSort: false, sPaginationType: 'full_numbers',
        });
        document.getElementById("tbl_MealPlans").removeAttribute("style")

    }, function (data) {// GETTING Error HERE
        AlertDanger(data.ex)
        $("#tbl_MealPlans").dataTable({
            bSort: false, sPaginationType: 'full_numbers',
        });
    });

}



function SaveMealPlan() {
    try {
        var arrPlan = {
            ID: $("#Hdn_Plan").val(),
            Meal_Plan: $("#txt_Plan").val(),
        };
        var arrMealID = $("#sel_Meal").val();
        var arrMeal = new Array();
        $(arrMealID).each(function (index, Meal) {
            arrMeal.push({
                MealID: Meal,
                PlanID: $("#Hdn_Plan").val(),
            });
        });
        post("../handler/GenralHandler.asmx/SaveMealPlans", { arrMealPlan: arrPlan, arrMeals: arrMeal }, function (data) {
            Success("Meal Plan Saved.");
            $("#modals").remove();
            GetMealPlans()
        }, function (data) {// GETTING Error HERE
            AlertDanger(data.ex)
        });
    } catch (e) {
        AlertDanger(e.message)
    }
}


function UpdateMeals(ID, PlanID, Name) {
   // $(ID).event.preventDefault();
    $(ID).confirm({
        message: 'Are you really Update ' + Name + ' Plan ?',
        onConfirm: function () {
            openModals(content, 'Update Meal Plan', function Save() {
                SaveMealPlan();
            });
            GetMeals();
            $("#Hdn_Plan").val(PlanID);
            $("#txt_Plan").val(Name);
            var MealID = $.grep(arrMealPlan, function (H) { return H.ID == PlanID })
                .map(function (H) { return H.arrMealID; })[0];
            setTimeout(function () {
                $("#sel_Meal").val(MealID);
                $('#sel_Meal').change();
            }, 500)
            return false;
        }
    });
}

function Delete(ID, PlanID, Name) {
    $(ID).confirm({
        message: 'Are you really Want to <br/> Delete ' + Name + ' Plan?',
        onConfirm: function () {
            post("../handler/GenralHandler.asmx/DeletePlan", { PlanID: PlanID }, function (data) {
                Success("Plan Deleted Successfully");
                GetMealPlans();
            }, function (error) {
                AlertDanger(error.ex);
                });
            return false;
        }
    });
}
function AddMealPlan() {
    openModals(content, 'Add Meal Plan', function Save() {
        SaveMealPlan();
    });
    GetMeals();
}

function GetMeals() {
    $(arrMeals).each(function (index, Meal) {
        debugger
        if (Meal.IsMeal)
            $('#sel_Meal').append($('<option></option>').val(Meal.ID).html(Meal.AddOnName));
        $('#sel_Meal').change();
    });
}
