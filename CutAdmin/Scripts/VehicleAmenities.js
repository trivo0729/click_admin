﻿$(function () {
    GetVehicleAmenities();

    $("#txt_ValidUpto").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
})
var HiddenId = 0;
function GetVehicleAmenities() {
    $("#tbl_VehicleAmenities").dataTable().fnClearTable();
    $("#tbl_VehicleAmenities").dataTable().fnDestroy();
    //$("#tbl_VehicleAmenities tbody tr").remove()

    $.ajax({
        url: "../Handler/VehicleMasterHandller.asmx/GetVehicleAmenities",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var arrVehicleAmenities = result.tbl_VehicleAmenities;
                var tr = '';
                for (var i = 0; i < arrVehicleAmenities.length; i++) {

                    tr += '<tr> '
                    tr += '<td>' + (i + 1) + '</td>                                                                                                                          '
                    tr += '<td>' + arrVehicleAmenities[i].Amenity + '</td>'

                    tr += '<td class="align-center"><a class="button" onclick="LoadVehicleAmenity(\'' + arrVehicleAmenities[i].ID + '\',\'' + arrVehicleAmenities[i].Amenity + '\')"><span class="icon-pencil"></span></a></td>'
                    tr += '<td class="align-center"><span class="button-group children-tooltip">                                                               '
                    tr += '    <a href="#" class="button" title="trash" onclick="DeleteVehicleAmenity(\'' + arrVehicleAmenities[i].ID + '\')"><span class="icon-trash"></span></a></span></td>                '
                    tr += '</tr>'

                }
                $("#tbl_VehicleAmenities tbody").append(tr);
                //$("#tbl_VehicleAmenities tbody").html(tr);
                $("#tbl_VehicleAmenities").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
                document.getElementById("tbl_VehicleAmenities").removeAttribute("style")
            }
        }
    })
}

function DeleteVehicleAmenity(ID) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete<br/> <span class=\"orange\">' + ID + '</span>?</span>?</p>',
        function () {
            $.ajax({
                url: "../Handler/VehicleMasterHandller.asmx/DeleteVehicleAmenity",
                type: "post",
                data: '{"ID":"' + ID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Vehicle Amenity has been deleted successfully!");
                        window.location.reload();
                    } else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}

//********************Add Edit StaticPackage*******************************
function AddVehicleAmenity() {
    var VehicleAmenityname = $("#txt_Name").val();

    var bValid = true;
    if (VehicleAmenityname == "") {
        Success("Please Insert Vehicle Amenity Name")
        bValid = false;
        $("#txt_Name").focus()

    }

    if (bValid == true) {
        var DATA = {

            VehicleAmenityname: VehicleAmenityname

        }
        var jason = JSON.stringify(DATA);
        $.ajax({
            url: "../Handler/VehicleMasterHandller.asmx/AddVehicleAmenity",
            type: "post",
            data: jason,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    $("#VehicleAmenityModal").modal("hide")
                    Success("Vehicle Amenity Added Successfully");
                    window.location.reload();
                }

                Clear()
                GetVehicleAmenities()
            }
        })
    }

}
function LoadVehicleAmenity(ID, Amenity) {


    $.modal({
        content: '<div class="modal-body" id="VehicleAmenityModal2">' +
            '<div class="columns">' +
            '<div class="Twelve-columns twelve-columns-mobile"><label>Vehicle Amenity Name:</label> <div class="input full-width">' +
            '<input name="prompt-value" id="txt_Name2" placeholder="Vehicle Amenity Name" value="' + Amenity + '" class="input-unstyled full-width"  type="text">' +
            '</div></div>' +
            '</div>' +
            '<p class="text-alignright"><input type="button" value="Update" onclick="UpdateVehicleAmenity(\'' + ID + '\');" title="Update Details" class="button anthracite-gradient"/></p>' +
            '</div>',
        title: 'Update Vehicle  Amenity',
        width: 500,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}


function UpdateVehicleAmenity(ID) {

    var ID = ID;
    var VehicleAmenityname = $("#txt_Name2").val();

    var bValid = true;
    if (VehicleAmenityname == "") {
        Success("Please Insert Vehicle Amenity Name")
        bValid = false;
        $("#txt_Name2").focus()

    }

    if (bValid == true) {
        var DATA = {

            VehicleAmenityname: VehicleAmenityname,
            ID: ID

        }
        var jason = JSON.stringify(DATA);
        $.ajax({
            url: "../Handler/VehicleMasterHandller.asmx/UpdateVehicleAmenity",
            type: "post",
            data: jason,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    $("#VehicleAmenityModal2").modal("hide")
                    Success("Vehicle Amenity Updated Successfully");
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }

                Clear()
                GetVehicleAmenities()
            }
        })
    }

}
function Clear() {
    $("#txt_Name").val("");
    $("#txt_Name2").val("");

    HiddenId = 0;
}
