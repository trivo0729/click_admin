﻿var globe_ItineraryCount = 0;
var globe_urlParamDecoded = 0;
var global_CategoryCount = 0;
var global_HotelItinerary = [];
var hcd;
function HotelDetails(globe_urlParamDecoded) {
    globe_urlParamDecoded = $("#hdpackageId").val();
    $.ajax({
        url: "../Handler/PackageDetailHandler.asmx/GetItineraryHotelDetail",
        type: "post",
        data: '{"nID":"' + globe_urlParamDecoded + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.Session == 0) {
                window.location.href = "Default.aspx";
            }
            if (result.retCode == 0) {
                alert("No hotel detail found");
            }
            else if (result.retCode == 1) {
                globe_ItineraryCount = result.List_ItineraryHotelDetail[0].nDuration;
                global_CategoryCount = result.nCount;
                global_HotelItinerary = result.List_ItineraryHotelDetail

                CreateHotelTabs(global_HotelItinerary);
            }
        },
        error: function () {
        }
    });
}
function GetCategoryName(categoryID) {
    if (categoryID == 1) {
        return "Standard";
    }
    else if (categoryID == 2) {
        return "Deluxe";
    }
    else if (categoryID == 3) {
        return "Premium";
    }
    else if (categoryID == 4) {
        return "Luxury";
    }
}

function checkFileExtension(file) {
    var flag = true;
    if (file != null) {
        var extension = file.substr((file.lastIndexOf('.') + 1));
        switch (extension) {
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
            case 'JPG':
            case 'JPEG':
            case 'PNG':
            case 'GIF':
                flag = true;
                break;
            default:
                flag = false;
        }
    }
    return flag;
}

function getNameFromPath(strFilepath) {
    var objRE = new RegExp(/([^\/\\]+)$/);
    var strName = objRE.exec(strFilepath);

    if (strName == null) {
        return null;
    }
    else {
        return strName[0];
    }
}

function ajaxFileUpload(FileFolder, filename, ImageID, CategoryID, sImageNo) {
    var fileUpload = $('#' + ImageID + '').get(0);
    var files = fileUpload.files;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    $.ajax({
        url: "../FileUpload.ashx?id=" + FileFolder + "&filename=" + filename + "&nCategoryName=" + GetCategoryName(CategoryID) + "&imageIndex=" + (sImageNo + 1) + "&nCategoryID=" + CategoryID,
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (data, status) {
            $("#img_" + CategoryID + "_" + sImageNo).attr("width", "192px");
            $("#img_" + CategoryID + "_" + sImageNo).attr("height", "192px");
            var productfolder = "HotelImagesFolder//" + FileFolder + '//' + GetCategoryName(CategoryID) + '//' + filename;
            $("#img_" + CategoryID + "_" + sImageNo).attr("src", '../' + productfolder);
            //$("#img_" + CategoryID + "_" + sImageNo).attr("src", '../HotelThumnail.aspx?fn=' + data.upfile + '&w=192&h=192&productfolder=' + productfolder);
            if (typeof (data.error) != 'undefined') {
                if (data.error != '') {
                    alert(data.error);
                } else {

                }
            }
        },
        error: function (data, status, e) {
            alert(e);
        }
    });
}

function AddHotelImage(nProductID, nCategoryID, sImageNo, nImageID) {

    var sValue = $("#" + nImageID).val();
    var sImageName = getNameFromPath(sValue);
    var bValidImage = checkFileExtension(sImageName);
    var extention = sImageName.substring(sImageName.lastIndexOf(".") + 1);
    var sNewImageName = nImageID + "." + extention;
    if (sValue == "") {
        alert("Please select an Image");
        return false;
    }
    else if (bValidImage == false) {
        alert("Not a valid image.");
        return false;
    }
    ajaxFileUpload(nProductID, sNewImageName, nImageID, nCategoryID, sImageNo);
}
function GetHotelName(k, itineraryIndex) {
    itineraryIndex = itineraryIndex + 1;
    if (itineraryIndex == 1) {
        return global_HotelItinerary[k].sHotelName_1;
    }
    else if (itineraryIndex == 2) {
        return global_HotelItinerary[k].sHotelName_2;
    }
    else if (itineraryIndex == 3) {
        return global_HotelItinerary[k].sHotelName_3;
    }
    else if (itineraryIndex == 4) {
        return global_HotelItinerary[k].sHotelName_4;
    }
    else if (itineraryIndex == 5) {
        return global_HotelItinerary[k].sHotelName_5;
    }
    else if (itineraryIndex == 6) {
        return global_HotelItinerary[k].sHotelName_6;
    }
    else if (itineraryIndex == 7) {
        return global_HotelItinerary[k].sHotelName_7;
    }
    else if (itineraryIndex == 8) {
        return global_HotelItinerary[k].sHotelName_8;
    }
    else if (itineraryIndex == 9) {
        return global_HotelItinerary[k].sHotelName_9;
    }
    else if (itineraryIndex == 10) {
        return global_HotelItinerary[k].sHotelName_10;
    }
    else if (itineraryIndex == 11) {
        return global_HotelItinerary[k].sHotelName_11;
    }
    else if (itineraryIndex == 12) {
        return global_HotelItinerary[k].sHotelName_12;
    }
    else if (itineraryIndex == 13) {
        return global_HotelItinerary[k].sHotelName_13;
    }
    else if (itineraryIndex == 14) {
        return global_HotelItinerary[k].sHotelName_14;
    }
}

function GetHotelCode(k, itineraryIndex) {
    itineraryIndex = itineraryIndex + 1;
    if (itineraryIndex == 1) {
        return global_HotelItinerary[k].sHotel_Code_1;
    }
    else if (itineraryIndex == 2) {
        return global_HotelItinerary[k].sHotel_Code_2;
    }
    else if (itineraryIndex == 3) {
        return global_HotelItinerary[k].sHotel_Code_3;
    }
    else if (itineraryIndex == 4) {
        return global_HotelItinerary[k].sHotel_Code_4;
    }
    else if (itineraryIndex == 5) {
        return global_HotelItinerary[k].sHotel_Code_5;
    }
    else if (itineraryIndex == 6) {
        return global_HotelItinerary[k].sHotel_Code_6;
    }
    else if (itineraryIndex == 7) {
        return global_HotelItinerary[k].sHotel_Code_7;
    }
    else if (itineraryIndex == 8) {
        return global_HotelItinerary[k].sHotel_Code_8;
    }
    else if (itineraryIndex == 9) {
        return global_HotelItinerary[k].sHotel_Code_9;
    }
    else if (itineraryIndex == 10) {
        return global_HotelItinerary[k].sHotel_Code_10;
    }
    else if (itineraryIndex == 11) {
        return global_HotelItinerary[k].sHotel_Code_11;
    }
    else if (itineraryIndex == 12) {
        return global_HotelItinerary[k].sHotel_Code_12;
    }
    else if (itineraryIndex == 13) {
        return global_HotelItinerary[k].sHotel_Code_13;
    }
    else if (itineraryIndex == 14) {
        return global_HotelItinerary[k].sHotel_Code_14;
    }
}

function GetHotelDescription(k, itineraryIndex) {
    itineraryIndex = itineraryIndex + 1;
    if (itineraryIndex == 1) {
        return global_HotelItinerary[k].sHotelDescrption_1;
    }
    else if (itineraryIndex == 2) {
        return global_HotelItinerary[k].sHotelDescrption_2;
    }
    else if (itineraryIndex == 3) {
        return global_HotelItinerary[k].sHotelDescrption_3;
    }
    else if (itineraryIndex == 4) {
        return global_HotelItinerary[k].sHotelDescrption_4;
    }
    else if (itineraryIndex == 5) {
        return global_HotelItinerary[k].sHotelDescrption_5;
    }
    else if (itineraryIndex == 6) {
        return global_HotelItinerary[k].sHotelDescrption_6;
    }
    else if (itineraryIndex == 7) {
        return global_HotelItinerary[k].sHotelDescrption_7;
    }
    else if (itineraryIndex == 8) {
        return global_HotelItinerary[k].sHotelDescrption_8;
    }
    else if (itineraryIndex == 9) {
        return global_HotelItinerary[k].sHotelDescrption_9;
    }
    else if (itineraryIndex == 10) {
        return global_HotelItinerary[k].sHotelDescrption_10;
    }
    else if (itineraryIndex == 11) {
        return global_HotelItinerary[k].sHotelDescrption_11;
    }
    else if (itineraryIndex == 12) {
        return global_HotelItinerary[k].sHotelDescrption_12;
    }
    else if (itineraryIndex == 13) {
        return global_HotelItinerary[k].sHotelDescrption_13;
    }
    else if (itineraryIndex == 14) {
        return global_HotelItinerary[k].sHotelDescrption_14;
    }
}

function GetHotelImages(k, itineraryIndex) {
    itineraryIndex = itineraryIndex + 1;
    if (itineraryIndex == 1) {
        return global_HotelItinerary[k].sHotelImage1;
    }
    else if (itineraryIndex == 2) {
        return global_HotelItinerary[k].sHotelImage2;
    }
    else if (itineraryIndex == 3) {
        return global_HotelItinerary[k].sHotelImage3;
    }
    else if (itineraryIndex == 4) {
        return global_HotelItinerary[k].sHotelImage4;
    }
    else if (itineraryIndex == 5) {
        return global_HotelItinerary[k].sHotelImage5;
    }
    else if (itineraryIndex == 6) {
        return global_HotelItinerary[k].sHotelImage6;
    }
    else if (itineraryIndex == 7) {
        return global_HotelItinerary[k].sHotelImage7;
    }
    else if (itineraryIndex == 8) {
        return global_HotelItinerary[k].sHotelImage8;
    }
    else if (itineraryIndex == 9) {
        return global_HotelItinerary[k].sHotelImage9;
    }
    else if (itineraryIndex == 10) {
        return global_HotelItinerary[k].sHotelImage10;
    }
    else if (itineraryIndex == 11) {
        return global_HotelItinerary[k].sHotelImage11;
    }
    else if (itineraryIndex == 12) {
        return global_HotelItinerary[k].sHotelImage12;
    }
    else if (itineraryIndex == 13) {
        return global_HotelItinerary[k].sHotelImage13;
    }
    else if (itineraryIndex == 14) {
        return global_HotelItinerary[k].sHotelImage14;
    }
}

function CreateHotelTabs(HotelItinerary) {
    globe_urlParamDecoded = $("#hdpackageId").val();
    var sTabWidth = 100 / (global_CategoryCount);
    var sTabRow = '';
    var sTabRowContent = '';
    var sclass = '';
    var sDisplay = '';
    var nCount = HotelItinerary[0].nDuration;
    sTabRowContent += '<ul class="tabs">';
    for (var Cat = 0; Cat < HotelItinerary.length; Cat++) {
        var sTabID = GetCategoryName(HotelItinerary[Cat].nCategoryID);
        if (i == 0) {
            sclass = "active";
            sDisplay = ";display:block";
        }
        else {
            sclass = "";
            sDisplay = ";";
        }
        sTabRowContent += '<li class="' + sclass + '"><a href="#Hotel' + sTabID + '">' + sTabID + '</a></li>';
    }
    sTabRowContent += '</ul>';
    $("#div_HotelTabContent").html(sTabRowContent);
    sTabRowContent += '<div class="tabs-content">';
    for (var k = 0; k < (global_CategoryCount) ; k++) {
        var sTabID = GetCategoryName(HotelItinerary[k].nCategoryID);
        sTabRowContent += '<div id="Hotel' + sTabID + '" class="with-padding">';
        for (var i = 0; i < nCount; i++) {
            var sHotelName = GetHotelName(k, i);
            var HotelCode = GetHotelCode(k, i);
            var sHotelDescription = GetHotelDescription(k, i)
            if (sHotelName == null || sHotelName == undefined) {
                sHotelName = "";
            }
            if (sHotelDescription == null || sHotelDescription == undefined) {
                sHotelDescription = "";
            }
            sTabRowContent += '<div class="columns">';
            sTabRowContent += '<div class="four-columns twelve-columns-mobile">';
            sTabRowContent += 'Day ' + (i + 1) + '';
            sTabRowContent += '<p class="uploadimg">';
            sTabRowContent += '<img src="../img/upload-image.png" id="img_' + HotelItinerary[k].nCategoryID + '_' + i + '" />';
            sTabRowContent += '</p>';
            sTabRowContent += '<p class="button-height">';
            sTabRowContent += '<input type="file" id="' + HotelItinerary[k].nCategoryID + '_' + i + '" name="' + HotelItinerary[k].nCategoryID + '_' + i + '" onchange="AddHotelImage(' + globe_urlParamDecoded + ',' + HotelItinerary[k].nCategoryID + ',' + i + ', \'' + HotelItinerary[k].nCategoryID + '_' + i + '\');" class="file" multiple>';
            sTabRowContent += '</p>';
            sTabRowContent += '</div>';
            sTabRowContent += '<div class="eight-columns twelve-columns-mobile">';
            sTabRowContent += '<div class="input full-width mrbot20">';
            sTabRowContent += '<input value="' + sHotelName + '" id="txt_HotelName_' + sTabID + '_' + i + '" class="input-unstyled full-width" placeholder="Hotel Name" onfocus="AutoComplete(this.id)" type="text">';
            sTabRowContent += '<input type="hidden" value="' + HotelCode + '" id="txtHidden_HotelCode_' + sTabID + '_' + i + '" >';
            sTabRowContent += '</div>';
            sTabRowContent += '<br>';
            sTabRowContent += '<textarea name="ckeditor" id="txt_HotelDescription_' + sTabID + '_' + i + '">' + sHotelDescription + '</textarea>';
            $("#div_HotelTabContent").html(sTabRowContent);
            sTabRowContent += '</div>';
            sTabRowContent += '</div>';
            sTabRowContent += '<script type="text/javascript">' + 'CKEDITOR.replace("txt_HotelDescription_' + sTabID + '_' + i + '", {height: 200});</script>';
            sTabRowContent += '<br>';

            if(i==0)
            {
                sTabRowContent += '<p class="text-alignright">';
                sTabRowContent += '<button type="button" class="button anthracite-gradient" onclick="CopyData(\'' + nCount + '\',\''+sTabID+'\')">Copy Same For Other</button>';
                sTabRowContent += '</p>';
                sTabRowContent += '<br>';
            }

        }
        sTabRowContent += '</div>';
    }
    sTabRowContent += '</div>';
    //sTabRowContent += '<hr />';
    //sTabRowContent += '<p class="text-alignright">';
    //sTabRowContent += '<button type="button" class="button anthracite-gradient" onclick="SaveHotelDetails();">Save</button>';
    //sTabRowContent += '</p>';
    $("#div_HotelTabContent").html(sTabRowContent);
    for (var k = 0; k < global_CategoryCount; k++) {
        for (var j = 0; j < nCount; j++) {
            $("#img_" + HotelItinerary[k].nCategoryID + "_" + j).attr("width", "192px");
            $("#img_" + HotelItinerary[k].nCategoryID + "_" + j).attr("height", "192px");
            var sHotelImagename = GetHotelImages(k, j);
            var productfolder = 'HotelImagesFolder//' + HotelItinerary[k].nPackageID + '//' + GetCategoryName(HotelItinerary[k].nCategoryID) + '//' + sHotelImagename;
            if (sHotelImagename == "" || sHotelImagename == null || sHotelImagename == undefined) {
                $("#img_" + HotelItinerary[k].nCategoryID + "_" + j).attr("src", '../img/upload-image.png');
            }
            else {
                $("#img_" + HotelItinerary[k].nCategoryID + "_" + j).attr("src", '../' + productfolder);
            }
        }
    }
}

function CopyData(nCount,Categoryy) {

    var HotelName = $("#txt_HotelName_" + Categoryy + "_0").val();
    var HotelDescription = CKEDITOR.instances['txt_HotelDescription_' + Categoryy + '_0'].getData();
   
    for (var i = 0; i < nCount-1; i++)
    {
        $("#txt_HotelName_" + Categoryy + "_" + (i + 1)).val("");
        $("#txt_HotelName_" + Categoryy + "_" + (i + 1)).val(HotelName);

        $("#txt_HotelDescription_" + Categoryy + "_" + (i + 1)).val("");
        CKEDITOR.instances['txt_HotelDescription_' + Categoryy + '_' + (i + 1)].setData(HotelDescription);

      
       
    }
}

function SaveHotelDetails() {
   
        var HotelCod = "";
        globe_urlParamDecoded = $("#hdpackageId").val();
        for (var k = 0; k < global_CategoryCount; k++) {
            for (var i = 0; i < globe_ItineraryCount; i++) {
                var sHotelName = $("#txt_HotelName_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).val();
                HotelCode = $("#txtHidden_HotelCode_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).val();
                var sHotelDesc = CKEDITOR.instances['txt_HotelDescription_' + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i].getData();
                var sImage = $('#img_' + global_HotelItinerary[k].nCategoryID + '_' + i).attr("src");
                if (sHotelName == "")
                {
                    AlertDanger("Please enter Hotel Name for Category : " + GetCategoryName(global_HotelItinerary[k].nCategoryID));
                    $("#txt_HotelName_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).focus();
                    $('.wizard fieldset').showWizardPrevStep(true);
                    return false;
                }
                else if (sHotelDesc == "") {
                    AlertDanger("Please enter Hotel Description for Category : " + GetCategoryName(global_HotelItinerary[k].nCategoryID));
                    $("#txt_HotelDescription_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).focus();
                    $('.wizard fieldset').showWizardPrevStep(true);
                    return false;
                }
                else if (sImage == "" || sImage == "../images/upload-image.png") {
                    AlertDanger("Please enter Hotel Image for Category : " + GetCategoryName(global_HotelItinerary[k].nCategoryID));
                    $('#img_' + k + '_' + i).focus();
                    $('.wizard fieldset').showWizardPrevStep(true);
                    return false;
                }
            }
        }
        var singleItinerary;
        var nSuccessCount = 0;
        for (var k = 0; k < global_CategoryCount; k++) {
            var listHotelName = [];
            var listHotelCode = [];
            var listHotelDesc = [];
            for (var i = 0; i < globe_ItineraryCount; i++) {
                var sHotelName = $("#txt_HotelName_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).val();
                HotelCode = $("#txtHidden_HotelCode_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).val();
                //var sHotelDesc = $("#txt_HotelDescription_" + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i).val();
                var sHotelDesc = CKEDITOR.instances['txt_HotelDescription_' + GetCategoryName(global_HotelItinerary[k].nCategoryID) + '_' + i].getData();
                var sImage = $('#img_' + global_HotelItinerary[k].nCategoryID + '_' + i).attr("src");
                listHotelName.push(sHotelName);
                listHotelCode.push(HotelCode);
                listHotelDesc.push(sHotelDesc);

            }
            var jsonText = JSON.stringify({ nID: globe_urlParamDecoded, listHotelName: listHotelName, listHotelCode: listHotelCode, listHotelDesc: listHotelDesc, nCategoryID: global_HotelItinerary[k].nCategoryID, sCategoryName: GetCategoryName(global_HotelItinerary[k].nCategoryID) });
            $.ajax({
                url: "../Handler/AddPackageHandler.asmx/SaveHotelDetails",
                type: "post",
                data: jsonText,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.Session == 0) {
                        window.location.href = 'Default.aspx';
                    }
                    if (result.retCode == 0) {
                        alert("Error in saving, please try again!");
                    }
                    else if (result.retCode == 1) {
                        nSuccessCount = nSuccessCount + 1;
                        if (nSuccessCount == global_CategoryCount) {
                            Success("Hotel details added successfully.");
                            $("#LiTransfer").addClass("active")
                            $("#lst_HotelDetails").attr("class", "");
                            $("#PackageTransfer").show();
                            $("#HotelDetails").hide();
                            // globe_urlParamDecoded = $("#hdpackageId").val();
                            //$('input[id$=hidden_Filed_ID]').val(globe_urlParamDecoded);
                            //GetCurrentImages(globe_urlParamDecoded);
                            GetActivityById();
                        }
                    }
                },
                error: function () {
                    alert('Error in saving, please try again!');
                }
            });
        }
    
}

var HotelCode
function AutoComplete(id) {
    var tpj = jQuery;
    var chkinMonth;
    var chkoutDate;
    var chkoutMonth;
    var id;
    var hdncc = $("#hdnDCode").val();
    tpj('#' + id).autocomplete({
        source: function (request, response) {
            tpj.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../Handler/Genralhandler.asmx/GetHotel",
                data: "{'name':'" + tpj('#' + id).val() + "','destination':'" + hdncc + "'}",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    response(result);
                },
                error: function (result) {
                    alert("No Match");
                    alert(tpj('#hdnHCode').val());
                }
            });
        },
        minLength: 4,
        select: function (event, ui) {
            HotelCode = ui.item.id;
            GetDescription(id);
        }
    });
}


function GetDescription(id) {
    $.ajax({
        type: "POST",
        url: "../handler/Genralhandler.asmx/GetHotelDescription",
        data: '{"HCode":"' + HotelCode + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {

            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {

                var arrAgentList = result.ReservationDetails;
                var txtid = id.replace("txt_HotelName_", "txt_HotelDescription_");
                $('#' + id.replace("txt_HotelName_", "txtHidden_HotelCode_")).val(HotelCode)
                CKEDITOR.instances['' + txtid].setData(arrAgentList[0].HotelFacilities);
            }
        },
        error: function () {
        }
    });

};

