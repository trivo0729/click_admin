﻿$(document).ready(function () {
    $("#txt_InsuranceDueDate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#txt_RegistrationDate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });

    ID = GetQueryStringParams('ID');
    if (ID != undefined) {
        GetRegisteredVehicleByID(ID);
        $("#btn_VehicleRegistration").val('Update Vehicle');
        $("#btn_VehicleRegistration").text('Update Vehicle');
    }
    setTimeout(function () {
        GetVehicleModel();
        GetVehicleAmenities();
    }, 500);




});
function ShowFields() {
    var value = $("#selSupplier option:selected").val();
    if (value == "Own") {
        $("#ModalYear").css("display", "block");
        $("#NoPlate").css("display", "block");
        $("#RegistrationDate").css("display", "block");
        $("#DivOtherSuppName").css("display", "none");
    }
    else if (value == "Attached") {
        $("#ModalYear").css("display", "none");
        $("#NoPlate").css("display", "none");
        $("#RegistrationDate").css("display", "none");
        $("#DivOtherSuppName").css("display", "none");
    }
    else if (value == "Supplier") {
        $("#ModalYear").css("display", "none");
        $("#NoPlate").css("display", "none");
        $("#RegistrationDate").css("display", "none");
        $("#DivOtherSuppName").css("display", "block");
    }
}

function ValidityOption() {
    if ($('#rdb_Numberofmonths').is(':checked')) {

        $("#DivNumberofmonths").css("display", "block");
        //$("#rdb_Numberofmonths").css("display", "none");
        //$("#lbl_Numberofmonths").css("display", "none");
    }
    else {
        $("#DivNumberofmonths").css("display", "none");
        //$("#rdb_Numberofmonths").css("display", "block");
        //$("#lbl_Numberofmonths").css("display", "block");
    }

    //if ($('#rdb_Numberofmonths').is(':checked')) {

    //    $("#DivNumberofmonths").css("display", "block");
    //}
    //else {
    //    $("#DivNumberofmonths").css("display", "none");
    //    //$("#FixedDate").show();
    //}
}
var arrVehicleType = new Array();
function GetVehicleModel() {
    debugger;
    // Ajax request to get categories
    $.ajax({
        type: "POST",
        url: "../Handler/VehicleRegistrationHandler.asmx/GetVehicleModel",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            try {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    arrVehicleType = result.tbl_VehicleModel;
                    if (arrVehicleType.length > 0) {
                        $("#selVehicleModal").empty();
                        var ddlRequest = "";
                         ddlRequest = '<option selected="selected" value="">-Select Vehicle Model-</option>';
                        for (i = 0; i < arrVehicleType.length; i++) {
                                ddlRequest += '<option value="' + arrVehicleType[i].ID + '">' + arrVehicleType[i].VehicleBrand + '</option>';
                            //ddlRequest += '<option   value="' + arrVehicleType[i].ID + '">' + arrVehicleType[i].VehicleBrand + ' [Seatting Capacity : ' + arrVehicleType[i].SeattingCapacity + ', Baggage Capacity : ' + arrVehicleType[i].BaggageCapacity + ' ]</option>';
                        }
                        $("#selVehicleModal").append(ddlRequest);
                    }
                }

            }
            catch (e) { }
        },
        error: function () {
            alert("Error getting");
        }
    });

}

function GetVehicleAmenities() {

    $("#select_Amenities").empty();
    $.ajax({
        type: "POST",
        url: "../Handler/VehicleRegistrationHandler.asmx/GetVehicleAmenities",
        data: '{}',
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                var Arr = obj.tbl_VehicleAmenities;
                Arr = Arr.sort();
                var Div = '';
                //Div += ' <option selected="selected" value="-">-Select Vehicle Amenities-</option>';
                //Div += ' <option  value="AllCountry">For All Nationality</option>';
                //$("#select_Amenities").empty();
                for (var i = 0; i < Arr.length; i++) {
                    Div += '<option value=' + Arr[i].ID + '>' + Arr[i].Amenity + '</option>'

                }
                $("#select_Amenities").append(Div);
                //$("#select_TerState").append(ddlRequest);
                $('#select_Amenities').change(function () {
                    console.log($(this).val());
                }).multipleSelect({
                    width: '100%'
                });
            }
        },
    });
}





function GetRegisteredVehicleByID(ID) {

    var data = { ID: ID };
    $.ajax({
        type: "POST",
        url: "../Handler/VehicleRegistrationHandler.asmx/GetVehicleByID",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var Lis_VehicleDetails = result.tbl_VehicleDetails[0];
                var Lis_AmenityList = result.tbl_AmenityList;
                $("#selSupplier").val(Lis_VehicleDetails.Supplier);
                $("#DivSupplier .select span")[0].textContent = Lis_VehicleDetails.Supplier;

                if (Lis_VehicleDetails.Supplier == "Own") {
                    $("#ModalYear").css("display", "block");

                    $("#NoPlate").css("display", "block");
                    $("#RegistrationDate").css("display", "block");
                    $("#DivOtherSuppName").css("display", "none");
                }
                else if (Lis_VehicleDetails.Supplier == "Attached") {
                    $("#ModalYear").css("display", "none");
                    $("#NoPlate").css("display", "none");
                    $("#RegistrationDate").css("display", "none");
                    $("#DivOtherSuppName").css("display", "none");
                }
                else if (Lis_VehicleDetails.Supplier == "Supplier") {
                    $("#ModalYear").css("display", "none");
                    $("#NoPlate").css("display", "none");
                    $("#RegistrationDate").css("display", "none");
                    $("#DivOtherSuppName").css("display", "block");
                }
                $("#txt_OtherSup").val(Lis_VehicleDetails.OtherSupplier);

                GetVehicleModel();
                setTimeout(function () {
                    AppendVehicleModel(Lis_VehicleDetails.VehicleModal)
                }, 1500);

                $("#selModalYear").val(Lis_VehicleDetails.ModalYear);
                $("#DivModalYear .select span")[0].textContent = Lis_VehicleDetails.ModalYear;
                $("#txt_NoPlate").val(Lis_VehicleDetails.NoPlate);
                $("#txt_RegistrationDate").val(Lis_VehicleDetails.RegistrationDate);
                if (Lis_VehicleDetails.RegistrationValidity == "Lifetime") {
                    $("#rdb_Lifetime").prop("checked", true);
                    $("#rdb_Numberofmonths").prop("checked", false);
                    //$("#rdb_Lifetime").checked("checked");
                    $("#DivNumberofmonths").hide();
                }
                else {
                    $("#DivNumberofmonths").show();
                    $("#txt_Numberofmonths").val(Lis_VehicleDetails.RegistrationValidity);
                    //$("#rdb_Numberofmonths").checked("checked");
                    $("#rdb_Numberofmonths").prop("checked", true);
                    $("#rdb_Lifetime").prop("checked", false);
                }
                $("#txt_InsuranceNo").val(Lis_VehicleDetails.InsuranceNo);
                $("#txt_InsuranceDueDate").val(Lis_VehicleDetails.InsuranceDueDate);
                $("#txt_MaintenanceDue").val(Lis_VehicleDetails.MaintenanceDue);

                GetVehicleAmenities();
                setTimeout(function () {
                    AmenityList = Lis_AmenityList;
                    var AmenityArray = [];
                    for (var i = 0; i < AmenityList.length; i++) {
                        AmenityArray.push(AmenityList[i].id);
                    }
                    $("#select_Amenities").multipleSelect("setSelects", AmenityArray);
                }, 3000)
            }
            else {
                alert("An error occured !!!");
            }
        },
        error: function () {
            alert("An error occured !!!");
        }
    });
}
function AppendVehicleModel(VehicleModal) {
    debugger;
    try {
        var checkclass = document.getElementsByClassName('check');
        var VehicleBrand = $.grep(arrVehicleType, function (p) { return p.ID == VehicleModal; })
            .map(function (p) { return p.VehicleBrand; });


        $("#DivVehicleModal .select span")[0].textContent = VehicleBrand;
        for (var i = 0; i < arrVehicleType.length; i++) {
            if (arrVehicleType[i].ID == VehicleModal) {
                $("#DivVehicleModal .select span")[0].textContent = VehicleBrand;
                $('input[value="' + VehicleModal + '"][class="OfferType"]').prop("selected", true);

                $("#selVehicleModal").val(VehicleModal);
                var selected = [];
            }


        }
    }
    catch (ex) { }
}
function AppendAmenities(Lis_AmenityList) {

    debugger;

    var checkclass = document.getElementsByClassName('check');
    var StatesArray = [];
    //$("#NationalityDiv .select span")[0].textContent = NationalityName;
    for (var i = 0; i <= Lis_AmenityList.length; i++) {
        StatesArray.push(Lis_AmenityList[i]);

        //// $('input[value="' + Tours[i].trim() + '"][class="chk_TourType"]').addclass('checked');         
        //$('input[value="' + Lis_AmenityList[i].ID + '"][class="Onataionality"]').prop("selected", true);
        //$("#select_Amenities").val(Lis_AmenityList[i].ID);
        //pNationality += Nationality[i] + ",";
        //var selected = [];
    }
    $("#select_Amenities").multipleSelect("setSelects", StatesArray);
}

function AddVehicleRegistration() {
    if ($("#frm_vehicleReg").validationEngine('validate')) {
        var bValid = true;
        var ID = GetQueryStringParams('ID');
        var Supplier = $("#selSupplier").val();
        var OtherSupplier;
        if (Supplier == "Supplier") {
            OtherSupplier = $("#txt_OtherSup").val();
        }
        else {
            OtherSupplier = '';
        }
        var VehicleModal = $("#selVehicleModal").val();
        var ModalYear = $("#selModalYear").val();
        var NoPlate = $("#txt_NoPlate").val();
        var RegistrationDate = $("#txt_RegistrationDate").val();
        var RegistrationValidity;
        if ($('#rdb_Numberofmonths').is(':checked')) {
            RegistrationValidity = $("#txt_Numberofmonths").val();
        }
        else {
            RegistrationValidity = "Lifetime";
        }
        var InsuranceNo = $("#txt_InsuranceNo").val();
        var InsuranceDueDate = $("#txt_InsuranceDueDate").val();
        var MaintenanceDue = $("#txt_MaintenanceDue").val();
        ListofAmenities = $("#select_Amenities").val();
        var Amenities = "";
        if (ListofAmenities != null) {
            for (i = 0; i < ListofAmenities.length; i++) {
                var Am = ListofAmenities[i];
                Amenities += Am + "^";
            }
        }
        if (Amenities == "") {
            bValid = false;
            Success("Please select amenities");
        }
        if (VehicleModal == "-") {
            bValid = false;
            Success("Please enter Vehicle Modal");
        }
        var data = {
            ID: ID,
            Supplier: Supplier,
            OtherSupplier: OtherSupplier,
            VehicleModal: VehicleModal,
            ModalYear: ModalYear,
            NoPlate: NoPlate,
            RegistrationDate: RegistrationDate,
            RegistrationValidity: RegistrationValidity,
            InsuranceNo: InsuranceNo,
            InsuranceDueDate: InsuranceDueDate,
            MaintenanceDue: MaintenanceDue,
            Amenities: Amenities

        }
        if (bValid == true) {

            if ($("#btn_VehicleRegistration").val() == "Update Vehicle") {
                $.ajax({
                    type: "POST",
                    url: "../Handler/VehicleRegistrationHandler.asmx/UpdateVehicle",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (result.Session == 0) {
                            window.location.href = "VehicleRegistrationDetails.aspx";
                            return false;
                        }
                        if (result.retCode == 1) {
                            Success("Vehicle Details Updated successfully")
                            window.location.href = "VehicleRegistrationDetails.aspx";
                        }
                        else {
                            Success("Something went wrong! Please contact administrator.")
                        }
                    },
                    error: function () {
                    }
                });
            }
            else if ($("#btn_VehicleRegistration").val() == "Register Vehicle") {
                $.ajax({
                    type: "POST",
                    url: "../Handler/VehicleRegistrationHandler.asmx/AddVehicle",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (result.retCode == 1) {
                            Success("Vehicle Registered Successfully");
                            window.location.href = "VehicleRegistrationDetails.aspx";
                        }
                        else {
                            alert(" Error");
                        }
                    },
                });
            }
        }
    }
}


