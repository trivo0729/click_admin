﻿var arrDate = new Array();
var arrInventory = new Array();
var Startdt;
var Enddt;
var HotelCode = 0;
var columns = [];
var aoData = new Array();
var arrRooms = new Array();
var arrDates = new Array();
var arrMealPlan = new Array();
var sTitle = '';
var date = new Date();
var arrRateTypes = new Array()
var time = new Date(date.getTime());
time.setMonth(date.getMonth() + 1);
time.setDate(0);
// var days = time.getDate() > date.getDate() ? time.getDate() - date.getDate() : 0;
var days = 10;
$(document).ready(function () {
    arrRateTypes = new Array();
    $("#datepicker_start").datepicker({
        autoclose: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        onSelect: insertDepartureDate,
        minDate: "dateToday",
    });
    $("#datepicker_end").datepicker({
        dateFormat: "dd-mm-yy",
        autoclose: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
    });
    $("#datepicker_start").datepicker("setDate", new Date());
    var dateObject = $("#datepicker_start").datepicker('getDate', days);
    dateObject.setDate(dateObject.getDate() + days);
    $("#datepicker_end").datepicker("setDate", dateObject);
    var date = $("#datepicker_end").datepicker("setDate", dateObject);
    $("#sel_Hotel").change(function () {
        HotelCode = $("#sel_Hotel").val();
        GenrateDatesRoom();
        $("#loders").remove();
    });
});

var splitMonth = "";
var lastDay = "";
function insertDepartureDate() {
    var StartSelDate = $("#datepicker_start").val();
    var splitDate = StartSelDate.split('-');
    var splitMonth = splitDate[1];
    var date = new Date(), y = date.getFullYear(), m = new Date().getMonth()
    var lastDay = new Date(y, splitMonth, 0);
    lastDay = moment(lastDay).format("DD-MM-YYYY")
    $("#datepicker_end").val(lastDay);
}
function GenrateDatesRoom() {
    try {
        Startdt = $('#datepicker_start').val();
        Enddt = $('#datepicker_end').val();
        $('#SelMealPlan').empty();
        var sInvtype = GetInventoryType();
        post("../Inventoryhandler.asmx/GenrateDates", { Startdt: Startdt, Enddt: Enddt, HotelCode: HotelCode, sInventory:sInvtype }, function (data) {
            arrMealPlan = data.arrMealPlan;
            arrRooms = data.arrRooms;
            arrDates = data.ListDates;
            arrRateTypes = data.arrRateType;
            for (var i = 0; i < arrRooms.length; i++) {
                arrRooms[i].bcollapse = true;
            }
            $(arrMealPlan).each(function (index, MealPlan) { // GETTING Sucees HERE
                $('#SelMealPlan').append($('<option></option>').val(MealPlan.ID).html(MealPlan.Meal_Plan));
                $('#SelMealPlan').change();
            });
            $('#sel_RateType').empty();
            $(arrRateTypes).each(function (index, arrRateType) { // GETTING Sucees HERE
                $('#sel_RateType').append($('<option></option>').val(arrRateType.sRateType).html(arrRateType.sRateType));
            });
            $('#sel_RateType').change();
            GenratrTable();
            GetRates_Availbility(aoData);
            $("#SelMealPlan").change(function () {
                GenratrTable();
                GetRates_Availbility(aoData);
            });
            $("#Sel_Supplier").change(function () {
                GenratrTable();
                GetRates_Availbility(aoData);
            });
            $("#sel_RateType").change(function () {
                GenratrTable();
                GetRates_Availbility(aoData);
            });
        }, function (errorData) {
            AlertDanger(errorData.ex)
        });

    } catch (e) { AlertDanger(e.message) }
}
function ChangeRate(RoomName, RoomID, textRate, DateIndex, PaxType) {
    try {
        ConfirmModal('Are you sure want to Update ' +
        PaxType.replace("RR", "Room Rate").replace("EB", "Extra Bed Rate").replace("CN", "Chil No Bed Rate").replace("CW", "Chil with Bed Rate") + ' for ' + RoomName + ' ?',
        function (data) {
            var Supplier = $("#Sel_Supplier").val();
            var MealPlan = $("#SelMealPlan").val();
            var HotelCode = $("#sel_Hotel").val();
            var Startdt = $('#datepicker_start').val();
            var Enddt = $('#datepicker_end').val();
            var RateType = "S";
            if (!rdb_Selling.checked)
                RateType = "P";
            postasync("../handler/Roomratehandler.asmx/UpdateRate", {
                Startdt: Startdt,
                Enddt: Enddt,
                MealPlan: MealPlan,
                Supplier: Supplier,
                HotelCode: HotelCode,
                RoomCode: data.RoomID,
                DateIndex: data.DateIndex,
                Rate: $(data.textRate).val(),
                PaxType: data.PaxType,
                RateType: RateType
            }, function (successdata) {
                Success(PaxType.replace("RR", "Room Rate").replace("EB", "Extra Bed Rate").replace("CN", "Child No Bed Rate").replace("CW", "Child with Bed Rate") + 'Updated for ' + RoomName)
            }, function (error) {
            })
        }, { RoomName: RoomName, RoomID: RoomID, textRate: textRate, DateIndex: DateIndex, PaxType: PaxType }
        );
    }
    catch (e) {

    }
    $(this).addClass("confirm");
}
function Enable(ID) {
    $(ID).prop("readonly", "")
}
function dropdown1(RoomID, Rate) {
    if ($(Rate).hasClass("icon-minus-round")) {
        $("." + RoomID).addClass("hidden");
        $(Rate).removeClass("icon-minus-round red")
        $(Rate).addClass("icon-plus-round black")
        for (var i = 0; i < arrRooms.length; i++) {
            if (arrRooms[i].RoomID == RoomID)
                arrRooms[i].bcollapse = true;
        }
        GenratrTable();
        GetRates_Availbility(aoData);
    }
    else {
        $("." + RoomID).removeClass("hidden")
        $(Rate).addClass("icon-minus-round red");
        $(Rate).removeClass("icon-plus-round black")
        for (var i = 0; i < arrRooms.length; i++) {
            if (arrRooms[i].RoomID == RoomID)
                arrRooms[i].bcollapse = false;
        }
        GenratrTable();
        GetRates_Availbility(aoData);
    }

}
function CheckInventory() {
    var Inventory = GetInventoryType();
    $('#sel_RateType').empty();
    post("../handler/GenralHandler.asmx/_ByHotelInventory", { Inventory: Inventory, HotelCode: HotelCode}, function (data) {
        $('#sel_RateType').empty();
        var arrRateType = data.arrRateTypes;
        $(data.arrRateTypes).each(function (index, arrRateType) { // GETTING Sucees HERE
            $('#sel_RateType').append($('<option></option>').val(arrRateType.sRateType).html(arrRateType.sRateType));
        });
        $('#sel_RateType').change();
    }, function (error_data) {// GETTING ERROR HERE
        AlertDanger("No Rates Found")
        $("#div_tbl_InvList").empty();
    });
}
function ChakedByRoom(HotelCode) {
    $(".Room" + HotelCode).click();
}
function SelectByDate(Date) {
    $("." + Date).click();
}
function Next() {
    var days = 10;
    var StartDate = $("#datepicker_end").val();
    $("#datepicker_start").val(StartDate);
    var dateObject = $("#datepicker_end").datepicker('getDate', days);
    dateObject.setDate(dateObject.getDate() + days);
    var stDate = $("#datepicker_end").datepicker("setDate", dateObject);
    Startdt = StartDate;
    Enddt = $("#datepicker_end").val();
    GenrateDatesRoom();
}
function Previous() {
    var days = -10;
    var StartDate = $("#datepicker_start").val();
    $("#datepicker_end").val(StartDate);
    var dateObject = $("#datepicker_start").datepicker('getDate', days);
    dateObject.setDate(dateObject.getDate() + days);
    var stDate = $("#datepicker_start").datepicker("setDate", dateObject);
    Startdt = StartDate;
    Enddt = $("#datepicker_start").val();
    GenrateDatesRoom()

}
var HotelName;
function GetRates_Availbility(data) {
    try {
        var sTarrifType = $("#sel_RateType").val()
        Supplier = $("#Sel_Supplier").val();
        MealPlan = $("#SelMealPlan").val();
        HotelCode = GetQueryStringParams('sHotelID');
        if (HotelCode == undefined)
            HotelCode = $("#sel_Hotel").val();
        Startdt = $('#datepicker_start').val();
        Enddt = $('#datepicker_end').val();
        var InventoryType = "FreeSale";
        var RateType = "S";
        if (!rdb_Selling.checked)
            RateType = "P";
        if (Freesale_Id.checked) {
            InventoryType = "FreeSale";
        }
        if (Allocation_Id.checked) {
            InventoryType = "Allocation";
        }

        if (Allotmnet_Id.checked) {
            InventoryType = "Allotment";
        }
        /* Check Room Rates*/
        postasync("../handler/Roomratehandler.asmx/GetRatesByRoom", {
            Startdt: Startdt,
            Enddt: Enddt,
            MealPlan: MealPlan,
            Supplier: Supplier,
            InventoryType: InventoryType,
            HotelCode: HotelCode,
            data: data,
            RateType: RateType,
            sTarrifType:sTarrifType
        }, function (Suceessdata) {
            var result = Suceessdata;
            if (result.arrRates.length != 0) {
                $(data).each(function (index, Rooms) {
                    var arrRoomsRate = $.grep(result.arrRates, function (H) { return H.RoomID == Rooms.RoomID })
                                .map(function (H) { return H; });
                    $("." + Rooms.RoomID).removeClass("loading");
                    if (arrRoomsRate.length != 0) {
                        $(columns).each(function (r, Dates) {
                            if (Dates.mData != "RoomName" && Dates.mData != "RoomID" && Dates.mData != "bcollapse") {
                                var i = parseInt(r - 3);
                                var Rate = $.grep(arrRoomsRate, function (H) { return H.sPaxType == 'RR' })
                                    .map(function (H) { return H; });

                                if (Rate != undefined && Rate[i] != undefined) {
                                    Rate = Rate[i]
                                    $(SetInput("RRate" + i + Rooms.RoomID)).parent().removeClass("loading");
                                    $(".PlanID_" + Rooms.RoomID).val(Rate.PlanID);
                                    if (Rate.arrOffer.length != 0)
                                        ShowOffer(Rate.arrOffer, ".Offer" + i + Rooms.RoomID);
                                    $(".Offer" + i + Rooms.RoomID).menuTooltip($('#Offerfilter').hide(), {
                                        classes: ['with-small-padding', 'full-width', 'anthracite-gradient']
                                    });
                                    if (Rate.sPaxType == "RR")
                                        $(SetInput("RRate" + i + Rooms.RoomID)).val(Rate.RRate);
                                    var Rate = $.grep(arrRoomsRate, function (H) { return H.sPaxType == 'EB' })
                                      .map(function (H) { return H; })[i];
                                    if (Rate.sPaxType == "EB")
                                        $(SetInput("EBRate" + i + Rooms.RoomID)).val(Rate.RRate);
                                    var Rate = $.grep(arrRoomsRate, function (H) { return H.sPaxType == 'CW' })
                                      .map(function (H) { return H; })[i];
                                    if (Rate.sPaxType == "CW")
                                        $(SetInput("CWBRate" + i + Rooms.RoomID)).val(Rate.RRate);
                                    var Rate = $.grep(arrRoomsRate, function (H) { return H.sPaxType == 'CN' })
                                      .map(function (H) { return H; })[i];
                                    if (Rate.sPaxType == "CN")
                                        $(SetInput("CNBRate" + i + Rooms.RoomID)).val(Rate.RRate);
                                }
                                else {
                                    $(SetInput("RRate" + i + Rooms.RoomID)).val(0);
                                    $(SetInput("RRate" + i + Rooms.RoomID)).parent().removeClass("loading");
                                    $(SetInput("EBRate" + i + Rooms.RoomID)).val(0);
                                    $(SetInput("CWBRate" + i + Rooms.RoomID)).val(0);
                                    $(SetInput("CNBRate" + i + Rooms.RoomID)).val(0);
                                }

                            }
                        });
                    }
                    else {
                        $(columns).each(function (c, Dates) {
                            if (Dates.mData != "RoomName" || Dates.mData != "RoomID") {
                                $(SetInput("RRate" + c + Rooms.RoomID)).parent().removeClass("loading");
                                $(SetInput("RRate" + c + Rooms.RoomID)).val('0.00');
                                $(SetInput("EBRate" + c + Rooms.RoomID)).val('0.00');
                                $(SetInput("CWBRate" + c + Rooms.RoomID)).val('0.00');
                                $(SetInput("CNBRate" + c + Rooms.RoomID)).val('0.00');
                            }
                        });
                    }
                });
            }
            else {
                var ndRates = $('.Rate')
                $(ndRates).each(function (c, ndRate) {
                    $(ndRate).val("0.00");
                    $(ndRate).parent().removeClass("loading");
                    $(ndRate).prop("disabled", true);
                });
                $(".loading").removeClass("loading");
            }
        }, function (error) { AlertDanger(error.ex) });
        /*Check Inventory*/
        var RoomID = [];
        $(arrRooms).each(function () {
            RoomID.push(this.RoomID);
        });
        debugger
        postasync("../Inventoryhandler.asmx/GetInventoryByRoom",
                { Startdt: Startdt, Enddt: Enddt, InventoryType: InventoryType, HotelCode: HotelCode, RoomID: RoomID, SupplierID: HotelAdminID }, function (Successsdata) {
                    var result = Successsdata;
                    $(data).each(function (index, Rooms) {
                        debugger
                        var arrInventory = $.grep(result.arrRates, function (H) { return H.RoomTypeId == Rooms.RoomID })
                                    .map(function (H) { return H; });
                        if (arrInventory.length != 0) {
                            $(arrInventory).each(function (d, Inventory) {
                                var sRateType = "";
                                if (Inventory.Type.split('_').length == 2)
                                    sRateType = Inventory.Type.split('_')[1]

                                var cssClass = "";
                                var RoomCount = "0";
                                if (Inventory.InventoryType == "0")
                                    cssClass = "room-not-available"

                                if (Inventory.InventoryType == "FreeSale") {
                                    RoomCount = Inventory.Type.split('_')[0];
                                    if (Inventory.discount == "ss") {
                                        cssClass = "room-not-available"
                                        RoomCount = RoomCount
                                    }
                                    else
                                        cssClass = "room-available"
                                }
                                else if (Inventory.InventoryType != "0") {
                                    RoomCount = Inventory.Type.split('_')[0];
                                    if ((Inventory.discount == "ss" || Inventory.NoOfCount == 0) && Inventory.InventoryType == "Allocation") {
                                        cssClass = "room-not-available"
                                        RoomCount = RoomCount + '/' + Inventory.NoOfCount
                                    }
                                    else if (Inventory.Type != 0 && Inventory.NoOfCount != 0) {
                                        cssClass = "room-available"
                                        RoomCount = RoomCount + '/' + Inventory.NoOfCount
                                    }
                                    else {
                                        cssClass = "room-not-available"
                                        RoomCount = RoomCount + '/' + Inventory.NoOfCount
                                    }
                                }
                                $('.' + Rooms.RoomID + "Date" + d).removeClass('loading');
                                $('.' + Rooms.RoomID + "Date" + d).addClass(cssClass);
                                $('.' + Rooms.RoomID + "Date" + d).text(RoomCount)
                                if ($('.' + Rooms.RoomID + "RateType" + d).val() == "")
                                    $('.' + Rooms.RoomID + "RateType" + d).val(sRateType);
                            });
                        }
                        else {
                            $(columns).each(function (c, Dates) {
                                if (Dates.mData != "RoomName" || Dates.mData != "RoomID") {
                                    $('.' + Rooms.RoomID + "Date" + c).removeClass("loading");
                                    $('.' + Rooms.RoomID + "Date" + c).addClass("room-not-available");
                                    $('.' + Rooms.RoomID + "Date" + c).text("0")
                                }
                            });
                        }
                    });
                }, function (errorData) { })
        /*Cancellation Polices*/
        $(arrDates).each(function (index, Date) { // GETTING Sucees HERE
            $(arrRooms).each(function () {
                if (this.bcollapse == false)
                {
                    $("body").css("pointer-events", "");
                    var RoomID = this.RoomID;
                    postasync("../handler/Roomratehandler.asmx/GetCancellation",
                    {
                        Date: Date.LongDate,
                        HotelCode: $("#sel_Hotel").val(),
                        MealPlan: $("#SelMealPlan").val(),
                        RoomCode: RoomID,
                        Supplier: $("#Sel_Supplier").val(),
                        sTarrifType: sTarrifType,
                    },
                    function (result) {
                        var arrCancel = result.arrCancellation;
                        $('.Policy' + index + RoomID).empty();
                        var html = '';
                        if (arrCancel.length != 0 && arrCancel[0].Type == "non-Refundable")
                        {
                            html += '<span class="tag red-bg " id="CancelPolicy' + index + RoomID + '" style="width: 87px;">non-Refundable</span>';
                            $('.Policy' + index + RoomID).append(html);
                            $('#CancelPolicy' + index + RoomID).on('click', GetCancelPolicy.bind(this, arrCancel, '#CancelPolicy' + index + RoomID, index, RoomID));;
                        }
                        else if (arrCancel.length != 0) {
                            html += '<span id="CancelPolicy' + index + RoomID +'" class="tag green-bg " style="width: 87px;" >';
                            html += 'Refundable</span>';
                            $('.Policy' + index + RoomID).append(html);
                            $('#CancelPolicy' + index + RoomID).on('click', GetCancelPolicy.bind(this, arrCancel, '#CancelPolicy' + index + RoomID, index, RoomID));;
                        }
                        else
                        {
                            html += '<span class="tag red-bg " id="CancelPolicy' + index + RoomID + '" style="width: 87px;">non-Refundable</span>';
                            $('.Policy' + index + RoomID).append(html);
                            $('#CancelPolicy' + index + RoomID).on('click', GetCancelPolicy.bind(this, new Array(), '#CancelPolicy' + index + RoomID, index, RoomID));;
                        }
                    }, function (error) {

                    });
                }
            });
        });
    } catch (e) { }
    $("#tooltips div").remove();
}
function GetCancelPolicy(arrCancel, dumy, DateIndex,RoomID) {
    $('#Cancelfilter').empty();
    var html = '<ul class="bullet-list">';
    $(arrCancel).each(function (c, Policy) {
        if (Policy.RefundType == "Refundable") {
            html += '<li>'
            if (Policy.Type == "Percentile")
                html += 'In the event of cancellation before <b>' + Policy.DayTill + ' days</b> <br/>from the day of check-in <b>' + Policy.Percentage + ' charge</b> will apply. <b>'
            else if (Policy.Type == "Amount") {
                html += 'In the event of cancellation before <b>' + Policy.DayTill + ' days</b> <br/>from the day of check-in <b>' + Policy.Amount + ' charge</b> will apply. <b>'
            }
            else if (Policy.Type == "Night")
                html += 'In the event of cancellation before <b>' + Policy.DayTill + ' days</b> <br/>from the day of check-in <b>' + Policy.noNights + ' night charge</b>  will apply. <b>'
            html += '</li>'
        }
        else if (Policy.RefundType == "NoShow") {
            html += '<li>'
            if (Policy.Type == "Percentile")
                html += Policy.Percentage + ' charge</b> will apply in the event of <b>No Show</b>'
            else if (Policy.RefundType == "Amount") {
                html += Policy.Amount + ' charge</b> will apply in the event of <b>No Show</b>.'
            }
            else if (Policy.Type == "Night")
                html += Policy.noNights + ' charge</b> will apply in the event of <b>No Show</b>.'
            html += '</li>'
        }
        else {
            html += '<li>This non-Refunable rate ,<br/> no charge will be refundable once cancelled.</li>'
        }
    });
    html += '</ul>'
    $('#Cancelfilter').append(html + '<input type="button" class="button tiny orange-gradient" id="btn_Cancel" value="Update" />');
    $('#btn_Cancel').on('click', ShowPolicy.bind(this, arrCancel, DateIndex, RoomID));
    $(dumy).menuTooltip($('#Cancelfilter').hide(), {
        classes: ['with-small-padding', 'full-width', 'anthracite-gradient']
    });
   
}

var arrCancelDetails = new Array();
function ShowPolicy(arrCancel, day, RoomID) {
    try {
        var HotelName = $("#sel_Hotel option:selected").text();
        var RoomName = $.grep(arrRooms, function (p) { return p.RoomID == RoomID; })
                     .map(function (p) { return p.RoomName; })[0];
        arrCancelDetails = {
            HotelAdminID:HotelAdminID,
            TarrifID: arrCancel[0].TarrifID,
            fromdate: arrDates[day].LongDate.toString(),
            to: arrDates[day].LongDate.toString(),
            hotelcode: HotelCode,
            roomid: RoomID,
            ratetype: $("#sel_ratetype").val(),
            supplier: $("#Sel_Supplier").val(),
            meal_Plan: $("#SelMealPlan").val(),
            arrcancel: arrCancel}
        OpenIframe(HotelName + ":<label class='green underline right' style='margin-left: 1%;'>" + RoomName + "</label><br/>",
        "Template/CancellationPolicy.html")
    } catch (ex) { }
}
function closeCancelIframe() {
    $('#modals').remove();
    $('#Cancelfilter').hide();
    GenratrTable();
    GetRates_Availbility(aoData);
    return false;
}

function SetInput(Rate) {
    try {
        return $('.' + Rate).find("input:text");
    } catch (e) { return ""; }
}
var sLastRateType = "";
function GenratrTable() {
    try {
        $("#div_tbl_InvList").empty();
        $("#div_tbl_InvList").append(' <table id="tbl_HotelList" class="table responsive-table font11"></table>')
        columns = new Array();
        aoData = new Array();
        columns.push({
            "mData": "RoomName", "title": 'Rooms', "sWidth": "250px", "className": "",
            "mRender": function (data, type, row, meta) {
                var sClass = "";
                var sTitle = "Hide  Rates";
                var sColapse = "icon-minus-round red"
                if (row.bcollapse) {
                    sClass = "hidden";
                    sColapse = "icon-plus-round black"
                    sTitle = "Show Rates"
                }
                var html = '';
                html += '<div class="font11 red with-small-padding" style = "border-bottom: .5px solid lightgray;"><a style="margin-right: 5px;" class="' + sColapse + ' icon-size2 with-pencil center pointer" title="' + sTitle + '"   onclick="dropdown1(\'' + row.RoomID + '\',this)" style="margin-right:5px"></a>Availability  <div class="font12" style="float:right "> <a class="  icon-marker with-tooltip black  pointer Market" title="Valid Market"   ondblclick="GetMarket(\'' + row.RoomID + '\')"></a> |    <a class="  icon-cart with-tooltip   black  pointer" title="Add Inventory"   onclick="SaveAvailability(\'' + row.RoomID + '\')"></a></div></div>'
                html += '<div class="font11 red with-small-padding" style = "border-bottom: .5px solid lightgray;">' + data + '<div class="font12 pointer" style="float:right "> <a class=" icon-pencil  with-tooltip   black" title="Add Adult Rate"   onclick="ChangePaxRate(\'' + row.RoomID + '\',\'' + 'RR' + '\',\'' + row.RoomName + '\')"></a></div></div>'
                html += '<div class="SelBox ' + row.RoomID + ' ' + sClass + '">'
                html += '<div class="font10 with-small-padding" style = "border-bottom: .5px solid lightgray;">Extra Bed <div class="font12 pointer" style="float:right "><a class=" icon-pencil  with-tooltip   black" title="Update Extra bed Rate"   onclick="ChangePaxRate(\'' + row.RoomID + '\',\'' + 'EB' + '\',\'' + row.RoomName + '\')"></a></div></div>'
                html += '<div class="font10 with-small-padding" style = "border-bottom: .5px solid lightgray;">Child With Bed <div class="font12 pointer" style="float:right "><a class=" icon-pencil  with-tooltip   black" title="Update Child Bed Rate"   onclick="ChangePaxRate(\'' + row.RoomID + '\',\'' + 'CW' + '\',\'' + row.RoomName + '\')"></a></div></div>'
                html += '<div class="font10 with-small-padding" style = "border-bottom: .5px solid lightgray;">Child No Bed<div class="font12 pointer" style="float:right "><a class=" icon-pencil  with-tooltip   black" title="Update Child No Bed Rate"   onclick="ChangePaxRate(\'' + row.RoomID + '\',\'' + 'CN' + '\',\'' + row.RoomName + '\')"></a></div></div>'
                html += '<div class="with-small-padding "> <span class="font9">Cancellation/Offers</span><br/></div>'
                html += '</div>'
                setTimeout(function () {
                    $('.Market').menuTooltip($('#Marketfilter').hide(), {
                        classes: ['with-small-padding', 'full-width', 'anthracite-gradient']
                    });
                }, 500)
                return html;
            },

        });
        columns.push({
            "mData": "RoomID", "visible": false,
        });
        columns.push({
            "mData": "bcollapse", "visible": false,
        });
        $(arrDates).each(function (index, Dates) {
            columns.push({
                "title": "<small class='black center'>" + Dates.Date.toString() + "(" + GetDayName(Dates.Day.toString()) + ")</small>", "mData": "Date" + index, "sWidth": "60px", "className": "center",
                "mRender": function (data, type, row, meta) {
                    var sClass = "";
                    if (row.bcollapse)
                        sClass = "hidden";
                    var html = '<div class="font11 with-small-padding align-center pointer ' + row.RoomID + "Date" + index + ' " style = "border-bottom: .5px solid lightgray;margin-top:12px"  onclick="SetInventory(\'' + row.RoomID + '\',\'' + row.RoomID + 'RateType' + index + '\',this,\'' + index + '\')">Loading..</div><input type="hidden" value=""  class="' + row.RoomID + 'RateType' + index + '"/>'/* Rate Format= RoomID+ Date */
                    html += '<div class="font11 with-small-padding RRate' + index + '' + row.RoomID + ' loading " style = "border-bottom: .5px solid lightgray;"><input type="text"   class="input-unstyled unstyled Rate font11" size="6" value="" readonly="true" ondblclick="Enable(this)" onchange="ChangeRate(\'' + row.RoomName + '\',\'' + row.RoomID + '\',this,' + index + ',\'' + 'RR' + '\' )" style="text-align:center"> <input type="hidden" class="PlanID_' + row.RoomID + '" value="0"></div>'
                    html += '<div class="loading ' + ' ' + row.RoomID + ' ' + sClass + '">';
                    html += '<div class="font11 with-small-padding EBRate' + index + '' + row.RoomID + ' " style = "border-bottom: .5px solid lightgray;"><input type="text"  class="input-unstyled center Rate font11" size="6" value="" readonly="true" ondblclick="Enable(this)" onchange="ChangeRate(\'' + row.RoomName + '\',\'' + row.RoomID + '\',this,' + index + ',\'' + 'EB' + '\')" style="text-align:center"></div>'
                    html += '<div class="font11 with-small-padding CWBRate' + index + '' + row.RoomID + ' " style = "border-bottom: .5px solid lightgray;"><input type="text" class="input-unstyled center Rate font11" size="6" value="" readonly="true" ondblclick="Enable(this)" onchange="ChangeRate(\'' + row.RoomName + '\',\'' + row.RoomID + '\',this,' + index + ',\'' + 'CW' + '\')" style="text-align:center"></div>'
                    html += '<div class="font11 with-small-padding CNBRate' + index + '' + row.RoomID + ' " style = "border-bottom: .5px solid lightgray;"><input type="text" class="input-unstyled center Rate font11" size="6" value="" readonly="true" ondblclick="Enable(this)" onchange="ChangeRate(\'' + row.RoomName + '\',\'' + row.RoomID + '\',this,' + index + ',\'' + 'CN' + '\')" style="text-align:center"></div>'
                    html += '<div class="font11 with-small-padding pointer Policy' + index + '' + row.RoomID + '" style="width: 87px;"></div>'
                    html += '<div class="font11 with-small-padding pointer Offer' + index + '' + row.RoomID + '" style="width: 87px;"></div>'
                    return html;
                }
            });
        });
        $(arrRooms).each(function (r, Room) {
            var ListData = new Array();
            aoData.push({
                RoomID: Room.RoomID,
                RoomName: Room.RoomName,
                bcollapse: Room.bcollapse
            });
            $(arrDates).each(function (index, Dates) {
                aoData[r]["Date" + index.toString()] = "0"
            });

        });

        $("#tbl_HotelList").dataTable({
            data: aoData,
            columns: columns,
            bSort: false,
            sPaginationType: 'full_numbers',
            bSearch: false,
            scrollY: "400px",
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: {
                leftColumns: 1,
            },
        });
        $("#tbl_HotelList thead tr th").css({ "background": "white" });
        $(".DTFC_LeftBodyLiner table").css("margin-top", "12px");
        $("#tbl_HotelList_length").empty()
        $("#tbl_HotelList_length").append("<label> <select id='sel_ratetype' class='select white-gradient glossy replacement select-styled-list tracked'></select>   Rates</label>");
        var options = $('#sel_RateType option');
        sLastRateType = $("#sel_RateType").val();
        $('#sel_RateType').find('option').each(function () {
            $('#sel_ratetype').append($('<option></option>').val($(this).val()).html($(this).val()));
        });
        if (sLastRateType != "")
        {
            $("#sel_ratetype").val(sLastRateType);
        }
        $("#sel_ratetype").change();
        $("#sel_ratetype").change(function () {
            sLastRateType = $("#sel_ratetype").val();
            $("#sel_RateType").val(sLastRateType);
            $("#sel_RateType").change();
        });
    } catch (ex) {
        AlertDanger(ex.message)
    }
}
function CheckByPrice() {
    GenratrTable();
    GetRates_Availbility(aoData);
}
function GetDayName(Day) {
    try {
        var arrDaysName = [{ FullName: "Monday", ShortName: "Mon" },
                   { FullName: "Tuesday", ShortName: "Tue" },
                   { FullName: "Wednesday", ShortName: "Wed" },
                   { FullName: "Thursday", ShortName: "Thu" },
                   { FullName: "Friday", ShortName: "Fri" },
                   { FullName: "Saturday", ShortName: "Sat" },
                   { FullName: "Sunday", ShortName: "Sun" }];

        var sDay = $.grep(arrDaysName, function (H) { return H.FullName == Day })
                                   .map(function (H) { return H.ShortName; })[0];
        Day = sDay;

    } catch (e) { }
    return Day;
}
function GetMarket(RoomID) {
    try {
        $('#sel_Market').val("");
        postasync("../handler/Roomratehandler.asmx/GetValidMarket",
        {
            FromDate: $('#datepicker_start').val(),
            ToDate: $('#datepicker_end').val(),
            HotelCode: $("#sel_Hotel").val(),
            MealPlan: $("#SelMealPlan").val(),
            RoomCode: RoomID,
            Supplier: $("#Sel_Supplier").val(),
        }, function (result) {
            $('#sel_Market').val(result.arrMarket)
            $("#txt_RoomID").val(RoomID);
        }, function (error) {
            AlerDanger(error.ex)
        });
    }
    catch (ex) {
        AlerDanger(ex.message)
    }
}

var arrRateDetails = new Array();
function ChangePaxRate(RoomID,PaxType,RoomName) {
    arrRateDetails = new Array();
    var uniquePlanID = new Array();
    var arrPlanID=[];
    arrPlanID.push($(".PlanID_" + RoomID).val());
    uniquePlanID = arrPlanID.filter(function (itm, i, arrPlanID) {
        return i == arrPlanID.indexOf(itm);
    });
    var SellType = "S";
    if (!rdb_Selling.checked)
        SellType = "P";
    arrRateDetails = {
        arrPlanID:uniquePlanID,
        Startdt: $("#datepicker_start").val(),
        Enddt: $("#datepicker_end").val(),
        MealPlan: $("#SelMealPlan").val(),
        Supplier: $("#Sel_Supplier").val(),
        SupplierName: $("#Sel_Supplier option:selected").text(),
        HotelCode: $("#sel_Hotel").val(),
        RoomCode: RoomID,
        PaxType: PaxType,
        RateType: $("#sel_ratetype").val(),
        sCurrency: "AED",
        SellType:SellType
    }
    OpenIframe($("#sel_Hotel option:selected").text() + ':<label class="green underline right" style="margin-left: 1%;">' + RoomName + '</label><br/>',
      'Template/updateRate.html');
}

function ShowOffer(arrOffer,elem) {
    debugger
    try {
        $(elem).append('<i class="icon-ribbons tag green-bg "> offer</i>');
        $(elem).on('click', GetOffer.bind(elem, arrOffer));
    } catch (e) {

    }
}
function GetOffer(arrOffers,elem) {
    try {
        debugger
        $('#Offerfilter').empty();
        var html = '<ul class="bullet-list">';
        $(arrOffers).each(function (c, arrOffer) {
            html += "<li>" + arrOffer.SeasonName + "(" + arrOffer.ValidFrom + "/" + arrOffer.ValidTo + ") <br/>"
            html += '<ul class="bullet-list"><li>'
            if (arrOffer.Category == "Early Booking")
            {
                if (arrOffer.BookBefore =="")
                    html += 'Offer will apply if booked ' + arrOffer.DaysPrior + '<br/> days prior (before) to check in date.'
                else
                    html += 'Offer will apply if the booking is done before ' + arrOffer.BookBefore
            }
            else if (arrOffer.Category == "Last Minute") {
                html += 'Start this offer ' + arrOffer.DaysPrior + ' days(s)<br/> before check-in day.'
            }
            else if (arrOffer.Category == "Free Nights") {
                html += 'If the booking is done for min ' + arrOffer.MinNight + 'nights <br/>' + arrOffer.FreeNight + ' free nights will be granted.'
            }
            html += '</li></ul>'
            html +='</li>'
        });
        html += '</ul>'
        $('#Offerfilter').append(html);
    } catch (e) {
    }
}

