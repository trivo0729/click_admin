﻿$(document).ready(function () {

    AllAgency();
    $("#txt_QuotationDate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#txt_TourStart").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#txt_TourEnd").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    id = GetQueryStringParams('ID');
    if (id != undefined) {
        GetQuotationByID(id);
        $("#btn_Quotation").val('Update');
        $("#btn_Quotation").text('Update');
    }
    //setTimeout(function () {
    //    AllAgency();
    //}, 500);

    
    //GetCountry();

    GetStaffDetails();

    $('#selCountry').change(function () {
        var sndcountry = $('#selCountry').val();
        GetCity(sndcountry);
    });



    $('#selAgency').change(function () {
        var Id = $('#selAgency').val();
        GetAgencybyId(Id);
    });



});


var m_arrAgency = "";
function AllAgency() {
    debugger;
    // Ajax request to get categories
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetAgentDetail",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            try {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    m_arrAgency = result.List_Agent;
                    if (m_arrAgency.length > 0) {
                        $("#selAgency").empty();

                        var ddlRequest = '<option selected="selected" value="-">Select Agency</option>';
                        for (i = 0; i < m_arrAgency.length; i++) {
                            ddlRequest += '<option   value="' + m_arrAgency[i].sid + '" onchange="GetAgencybyId(' + m_arrAgency[i].sid + ')">' + m_arrAgency[i].AgencyName + '</option>';
                        }
                        $("#selAgency").append(ddlRequest);
                    }
                }

            }
            catch (e) { }
        },
        error: function () {
            alert("Error getting Agency");
        }
    });

}

function GetStaffDetails() {
    debugger;
    // Ajax request to get categories
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetStaffDetails",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            try {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;

                if (result.Session == 0)// Session Expired
                {
                    window.location.href = "Default.aspx";
                    return false;
                }
                if (result.retCode == 1) {
                    m_arrStaff = result.Staff;

                    var ul = '';
                    $("#selQuotedby").empty();

                    ul += '<option selected="selected" value="-">Select Staff</option>';
                    if (m_arrStaff.length > 0) {
                        for (var i = 0; i < m_arrStaff.length; i++) {
                            ul += '<option   value=' + m_arrStaff[i].sid + '>' + m_arrStaff[i].ContactPerson + '</option>';
                        }

                    }
                    $('#selQuotedby').append(ul);
                }
                else {
                    $("#selQuotedby").append($('<option value="No record Found "></option>'));
                }
            }
            catch (e) { }
        },
        error: function () {
            alert("Error getting Staff");
        }
    });

}

function GetCountry() {
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.Country;
                if (arrCountry.length > 0) {
                    $("#selCountry").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Countryname + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    $("#selCountry").append(ddlRequest);
                    GetState();
                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });
}

var Country = "";

function GetStateDiv() {
    Country = $("#selCountry option:selected").val();
    if (Country == "IN") {
        $("#DivState").show();
        GetState();
    }
    else {
        $("#DivState").hide();
    }
}

function GetState() {
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetState",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrState = result.List;
                if (arrState.length > 0) {
                    $("#selState").empty();

                    var ddlRequest = '<option selected="selected" value="-">Select Any State</option>';
                    for (i = 0; i < arrState.length; i++) {
                        ddlRequest += '<option value="' + arrState[i].SateName + '">' + arrState[i].SateName + '</option>';
                    }
                    $("#selState").append(ddlRequest);

                }
            }
            if (result.retCode == 0) {
                $("#selState").empty();
            }
        },
        error: function () {
            alert("An error occured while loading States")
        }
    });
}

function GetCity(recCountry) {
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetCity",
        data: '{"country":"' + recCountry + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCity = result.City;
                if (arrCity.length > 0) {
                    $("#selCity").empty();
                    var ddlRequest = '<option selected="selected" value="-">Select Any City</option>';
                    for (i = 0; i < arrCity.length; i++) {
                        ddlRequest += '<option value="' + arrCity[i].Code + '" >' + arrCity[i].Description + '</option>';
                    }
                    $("#selCity").append(ddlRequest);

                }
            }
            if (result.retCode == 0) {
                $("#selCity").empty();
            }
        },
        error: function () {
            alert("An error occured while loading cities")
        }
    });
    //$('#selCity option:selected').val(tempcitycode);
}

function GetAgencybyId(Id) {

    var data = { AgentID: Id };
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetAgencyDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrAgency = result.arrAgency;
                $("#txt_Companyname").val(arrAgency.AgencyName);
                $('#txt_Address').val(arrAgency.Address);
                $('#txt_Email').val(arrAgency.email);
                $('#txt_Phone').val(arrAgency.phone);
                $('#txt_Mobile').val(arrAgency.Mobile);
                GetNationality(arrAgency.Country);

                GetCity(arrAgency.Country)
                setTimeout(function () { AppendCity(arrAgency.Description) }, 1000);

                $('#txt_Contactperson').val(arrAgency.ContactPerson);

            }
            else {
                alert("An error occured !!!");
            }
        },
        error: function () {
            alert("An error occured !!!");
        }
    });
}

function AddQuotation() {

    debugger;
    var bValid = true;

    var id = GetQueryStringParams('ID');

    var Date = $("#txt_QuotationDate").val();

    var Quoteto = $("#selAgency option:selected").val();

    var Companyname = $("#txt_Companyname").val();

    var Email = "";

    var Phone = "";

    var Mobile = "";

    var Contactperson = "";

    var Address = "";

    var City = "";

    var State = "";

    var Country = "";

    var Leadguestname = $("#txt_Leadguestname").val();

    var Adults = $("#selAdults").val();

    var Child = $("#selChild").val();

    var Infant = $("#selInfant").val();

    var TourStart = $("#txt_TourStart").val();

    var TourEnd = $("#txt_TourEnd").val();

    //var QuotationDetails = document.getElementById('noise').value;
    var QuotationDetails = CKEDITOR.instances['noise'].getData();

    var Quotedby = $("#selQuotedby option:selected").val();

    var Currentstatus = $("#selCurrentstatus option:selected").val();

    var NoteOrRemark = $("#txt_NoteorRemark").val();

    var Fname = "";
    var Lname = "";
    var Designation = "";
    var AgencyName = "";

    //if (Date == "") {
    //    bValid = false;
    //    Success("");
    //}


    //if (Quoteto == "") {
    //    bValid = false;
    //    $("#lbl_QuotetoAgency").css("display", "");
    //}



    if (Companyname == "") {
        bValid = false;
        Success("Please enter Company name");
    }

    //if (Email == "") {
    //    bValid = false;
    //    Success("Please enter Email");
    //}

    //if (Mobile == "") {
    //    bValid = false;
    //    Success("Please enter Mobile");
    //}

    //if (Contactperson = "") {
    //    bValid = false;
    //    Success("Please enter Contact Person");
    //}

    if (Adults == "") {
        bValid = false;
        Success("Please enter Adults");
    }
    if (Child == "") {
        bValid = false;
        Success("Please enter Child");
    }
    if (Infant == "") {
        bValid = false;
        Success("Please enter Infant");
    }
    if (TourStart == "") {
        bValid = false;
        Success("Please select TourStart");
    }
    if (TourEnd == "") {
        bValid = false;
        Success("Please select TourEnd");
    }
    if (Quotedby == "") {
        bValid = false;
        Success("Please enter Quotedby");
    }

    if (Currentstatus == "") {
        bValid = false;
        Success("Please select Currentstatus");
    }
    //if (Fname == "") {
    //    bValid = false;
    //    Success("Please Enter First Name");
    //}
    //if (Lname == "") {
    //    bValid = false;
    //    Success("Please enter Last Name");
    //}
    //if (Designation == "") {
    //    bValid = false;
    //    Success("Please enter Designation");
    //}
    //if (AgencyName == "") {
    //    bValid = false;
    //    Success("Please enter Agency Name");
    //}

    var data = {
        ID: id,
        Date: Date,
        Quoteto: Quoteto,
        Companyname: Companyname,
        Email: Email,
        Phone: Phone,
        Mobile: Mobile,
        Contactperson: Contactperson,
        Address: Address,
        City: City,
        State: State,
        Country: Country,
        Leadguestname: Leadguestname,
        Adults: Adults,
        Child: Child,
        Infant: Infant,
        TourStart: TourStart,
        TourEnd: TourEnd,
        QuotationDetails: QuotationDetails,
        Quotedby: Quotedby,
        Currentstatus: Currentstatus,
        NoteOrRemark: NoteOrRemark

    }
    if (bValid == true) {

        if ($("#btn_Quotation").val() == "Update") {
            $.ajax({
                type: "POST",
                url: "../Handler/QuotationHandler.asmx/UpdateQuotation",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        window.location.href = "QuotationDetails.aspx";

                        return false;
                    }
                    if (result.retCode == 1) {

                        Success("Quotation Details Updated successfully")
                        window.location.href = "QuotationDetails.aspx";
                    }
                    else {
                        Success("Something went wrong! Please contact administrator.")

                    }
                },
                error: function () {
                }
            });
        }

        else if ($("#btn_Quotation").val() == "Add Quotation") {

            $.ajax({
                type: "POST",
                url: "../Handler/QuotationHandler.asmx/AddQuotation",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {

                        Success("Quotation Added Successfully");
                        window.location.href = "QuotationDetails.aspx";
                    }
                    else {
                        alert(" Error");

                    }
                },

            });


        }

    }
}

function GetQuotationByID(Id) {

    var data = { Id: Id };
    $.ajax({
        type: "POST",
        url: "../Handler/QuotationHandler.asmx/GetQuotationByID",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var QuotationDetailArr = result.Arr[0];

                $("#txt_QuotationDate").val(QuotationDetailArr.Date);
                //AllAgency();
                //setTimeout(function () { AppendQuotedTo(QuotationDetailArr.QuotedTo) }, 500);


                var checkclass = document.getElementsByClassName('check');
                var AgencyName = $.grep(m_arrAgency, function (p) { return p.sid == QuotationDetailArr.QuotedTo; }).map(function (p) { return p.AgencyName; });
                $("#DivAgency .select span")[0].textContent = AgencyName;
              
                $('input[value="' + QuotationDetailArr.QuotedTo + '"][class="OfferType"]').prop("selected", true);
                $("#selAgency").val(QuotationDetailArr.QuotedTo);

               
                $("#txt_Companyname").val(QuotationDetailArr.Companyname);
                $('#txt_Contactperson').val(QuotationDetailArr.Contactperson);
                $('#txt_Leadguestname').val(QuotationDetailArr.Leadguestname);
                $("#selAdults").val(QuotationDetailArr.Adults);
                $("#DivAdults .select span")[0].textContent = QuotationDetailArr.Adults;
                $("#selChild").val(QuotationDetailArr.Child);
                $("#DivChild .select span")[0].textContent = QuotationDetailArr.Child;
                $("#selInfant").val(QuotationDetailArr.Infant);
                $("#DivInfant .select span")[0].textContent = QuotationDetailArr.Infant;
                $('#txt_TourStart').val(QuotationDetailArr.TourStart);
                $('#txt_TourEnd').val(QuotationDetailArr.TourEnd);
                //$('#noise').val(QuotationDetailArr.QuotationDetails);
                CKEDITOR.instances['noise'].setData(QuotationDetailArr.QuotationDetails);
                GetStaffDetails();
                setTimeout(function () { AppendQuotedBy("" + QuotationDetailArr.ContactPerson + "," + QuotationDetailArr.Quotedby + "") }, 1000);
                $('#selCurrentstatus').val(QuotationDetailArr.Currentstatus)
                $("#DivCurrentstatus .select span")[0].textContent = QuotationDetailArr.Currentstatus;
                $('#txt_NoteorRemark').val(QuotationDetailArr.Remark);
            }
            else {
                alert("An error occured !!!");
            }
        },
        error: function () {
            alert("An error occured !!!");
        }
    });
}


function GetNationality(Countryname) {
    try {
        var checkclass = document.getElementsByClassName('check');
        var Country = $.grep(arrCountry, function (p) { return p.Countryname == Countryname; }).map(function (p) { return p.Country; });

        $("#DivCountry .select span")[0].textContent = Countryname;
        for (var i = 0; i <= Country.length - 1; i++) {
            $('input[value="' + Countryname + '"][class="country"]').prop("selected", true);
            $("#selCountry").val(Countryname);
        }
    }
    catch (ex) { }
}
function AppendCountry(countryname) {
    try {
        var checkclass = document.getElementsByClassName('check');
        //var Description = $.grep(arrCity, function (p) { return p.Description == Description; }).map(function (p) { return p.Code; });

        $("#DivCountry .select span")[0].textContent = countryname;
        //for (var j = 0; j <= Description.length; j++) {
        $('input[value="' + countryname + '"][class="OfferType"]').prop("selected", true);
        $("#selCountry").val(countryname);
        //}
    }
    catch (ex) { }
}
function AppendCity(Description) {
    try {
        var checkclass = document.getElementsByClassName('check');
        //var Description = $.grep(arrCity, function (p) { return p.Description == Description; }).map(function (p) { return p.Code; });

        $("#DivCity .select span")[0].textContent = Description;
        //for (var j = 0; j <= Description.length; j++) {
        $('input[value="' + Description + '"][class="OfferType"]').prop("selected", true);
        $("#selCity").val(Description);
        //}
    }
    catch (ex) { }
}
function AppendState(statename) {
    try {
        var checkclass = document.getElementsByClassName('check');
        //var Description = $.grep(arrCity, function (p) { return p.Description == Description; }).map(function (p) { return p.Code; });

        $("#DivState .select span")[0].textContent = statename;
        //for (var j = 0; j <= Description.length; j++) {
        $('input[value="' + statename + '"][class="OfferType"]').prop("selected", true);
        $("#selState").val(statename);
        //}
    }
    catch (ex) { }
}
function AppendQuotedTo(QuotedTo) {

    try {
        var checkclass = document.getElementsByClassName('check');
        var AgencyName = $.grep(m_arrAgency, function (p) { return p.sid == QuotedTo; }).map(function (p) { return p.AgencyName; });

        $("#DivAgency .select span")[0].textContent = AgencyName;
        //for (var j = 0; j <= Description.length; j++) {
        $('input[value="' + QuotedTo + '"][class="OfferType"]').prop("selected", true);
        $("#selAgency").val(QuotedTo);
        //}
    }
    catch (ex) { }
}
//function AppendQuotedTo(AgencyName, QuotedTo) {
//    var str = AgencyName
//    var array = str.split(',');
//    var AgencyName = array[0];
//    var QuotedTo = array[1];
//    try {
//        var checkclass = document.getElementsByClassName('check');
//        //var Description = $.grep(arrCity, function (p) { return p.Description == Description; }).map(function (p) { return p.Code; });

//        $("#DivAgency .select span")[0].textContent = AgencyName;
//        //for (var j = 0; j <= Description.length; j++) {
//        $('input[value="' + QuotedTo + '"][class="OfferType"]').prop("selected", true);
//        $("#selAgency").val(QuotedTo);
//        //}
//    }
//    catch (ex)
//    { }
//}
function AppendQuotedBy(Contactperson, QuotedBy) {
    var str = Contactperson
    var array = str.split(',');
    var Contactperson = array[0];
    var QuotedBy = array[1];
    try {
        var checkclass = document.getElementsByClassName('check');
        //var Description = $.grep(arrCity, function (p) { return p.Description == Description; }).map(function (p) { return p.Code; });

        $("#DivQuotedby .select span")[0].textContent = Contactperson;
        //for (var j = 0; j <= Description.length; j++) {
        $('input[value="' + QuotedBy + '"][class="OfferType"]').prop("selected", true);
        $("#selQuotedby").val(QuotedBy);
        //}
    }
    catch (ex) { }
}
function AppendAdults(Adults) {
    try {
        var checkclass = document.getElementsByClassName('check');
        //var Description = $.grep(arrCity, function (p) { return p.Description == Description; }).map(function (p) { return p.Code; });

        $("#DivAdults .select span")[0].textContent = Adults;
        //for (var j = 0; j <= Description.length; j++) {
        $('input[value="' + Adults + '"][class="OfferType"]').prop("selected", true);
        $("#selAdults").val(Adults);
        //}
    }
    catch (ex) { }
}
//function GetState(statename) {
//    try {
//        var checkclass = document.getElementsByClassName('check');
//        var StateName = $.grep(arrState, function (p) { return p.StateName == statename; }).map(function (p) { return p.Country; });

//        $("#DivState .select span")[0].textContent = statename;
//        for (var i = 0; i <= StateName.length - 1; i++) {
//            $('input[value="' + statename + '"][class="OfferType"]').prop("selected", true);
//            $("#selState").val(statename);
//        }
//    }
//    catch (ex)
//    { }
//}

function AddAgentPopup() {
    //setTimeout(function () { GetCountry() }, 2000);

    $.modal({
        title: 'Add Agent',
        content: ' <div class="columns">' +

            '                     <div class="three-columns twelve-columns-mobile six-columns-tablet">                                                                                   ' +
            '                         <label>First Name:</label><br />                                                                                                                   ' +
            '                         <div class="input full-width">                                                                                                                     ' +
            '                             <input name="prompt-value" id="txt_FirstName" value="" class="input-unstyled full-width" placeholder="First Name" type="text">                 ' +
            '                 </div>                                                                                                                                                     ' +
            '                         </div>                                                                                                                                             ' +
            '                         <div class="three-columns twelve-columns-mobile six-columns-tablet">                                                                               ' +
            '                             <label>Last Name:</label><br />                                                                                                                ' +
            '                             <div class="input full-width">                                                                                                                 ' +
            '                                 <input name="prompt-value" id="txt_LastName" value="" class="input-unstyled full-width" placeholder="Last Name" type="text">               ' +
            '                 </div>                                                                                                                                                     ' +
            '                             </div>                                                                                                                                         ' +
            '                             <div class="three-columns twelve-columns-mobile six-columns-tablet">                                                                           ' +
            '                                 <label>Designation:</label><br />                                                                                                          ' +
            '                                 <div class="input full-width">                                                                                                             ' +
            '                                     <input name="prompt-value" id="txt_Designation" value="" class="input-unstyled full-width" placeholder="Designation" type="text">      ' +
            '                 </div>                                                                                                                                                     ' +
            '                                 </div>                                                                                                                                     ' +
            '                                 <div class="three-columns twelve-columns-mobile six-columns-tablet bold">                                                                  ' +
            '                                     <label>Company Name:</label><div class="input full-width">                                                                              ' +
            '                                         <input name="prompt-value" id="txt_AgencyName" value="" class="input-unstyled full-width" placeholder="Agency Name" type="text">   ' +
            '                 </div>                                                                                                                                                     ' +
            '                                     </div>                                                                                                                                 ' +



            //'  <div class="columns">' +
           
            '         </div>                                                                                                                                                               ' +
            '         <div class="columns">                                                                                                                        ' +
            '             <div class="four-columns  twelve-columns-mobile six-columns-tablet">                                                                                           ' +
            '                 <label>Country: </label>                                                                                                                                   ' +
            '                 <br />                                                                                                                                                     ' +
            '                 <div class="full-width button-height" id="DivCountry">                                                                                                     ' +
            '                     <select id="selCountry" name="validation-select" style="width: 235px" class="select OfferType">                                                                             ' +
            '                     </select>                                                                                                                                              ' +
            '                 </div>                                                                                                                                                     ' +
            '             </div>                                                                                                                                                         ' +


            '             <div class="four-columns  twelve-columns-mobile six-columns-tablet">                                                                                           ' +
            '                 <label>City: </label>                                                                                                                                      ' +
            '                 <br />                                                                                                                                                     ' +
            '                 <div class="full-width button-height" id="DivCity">                                                                                                        ' +
            '                     <select id="selCity" name="validation-select" style="width: 235px" class="select OfferType">                                                           ' +

            '                     </select>                                                                                                                                              ' +
            '                 </div>                                                                                                                                                     ' +
            '             </div>                                                                                                                                                         ' +

            '             <div class="four-columns twelve-columns-mobile six-columns-tablet bold" style="display:none">                                                                                        ' +
            '                 <label>Contact person<span class="red">*</span>:</label><div class="input full-width">                                                                     ' +
            '                     <input name="prompt-value" id="txt_Contactperson" placeholder="Contact person name" value="" class="input-unstyled full-width" type="text">            ' +
            '                 </div>                                                                                                                                                     ' +
            '                 </div>                                                                                                                                                     ' +

            '         </div>              ' +



            '         <div class="columns">                                                                                                                        ' +
           
            ' <div class="four-columns twelve-columns-mobile six-columns-tablet">                                                                                                          ' +
            '     <label>Email<span class="red">*</span>:</label><br />                                                                                                                    ' +
            '     <div class="input full-width">                                                                                                                                           ' +
            '         <input name="prompt-value" id="txt_Email" placeholder="Email" value="" class="input-unstyled full-width" type="text">                                                ' +
            '                 </div>                                                                                                                                                       ' +
            '     </div>                                                                                                                                                                   ' +
            '     <div class="four-columns twelve-columns-mobile six-columns-tablet mobilephone">                                                                                          ' +
            '         <label>Phone.:</label><br />                                                                                                                                         ' +
            '         <div class="input full-width">                                                                                                                                       ' +
            '             <input name="prompt-value" id="txt_Phone" placeholder="Phone" value="" class="input-unstyled full-width" type="text">                                            ' +
            '                 </div>                                                                                                                                                       ' +
            '         </div>                                                                                                                                                               ' +
            '         <div class="four-columns twelve-columns-mobile six-columns-tablet mobilephone">                                                                                      ' +
            '             <label>Mobile.<span class="red">*</span>:</label><br />                                                                                                          ' +
            '             <div class="input full-width">                                                                                                                                   ' +
            '                 <input name="prompt-value" id="txt_Mobile" placeholder="Mobile" value="" class="input-unstyled full-width" type="text">                                      ' +
            '                 </div>                                                                                                                                                       ' +
            '             </div>                                                                                                                                                           ' +


            '                     </div>                                                                                                                                                 ' +

            '         <div class="columns">                                                                                                                        ' +

            '                 <div class="six-columns twelve-columns-mobile six-columns-tablet">                                                                                         ' +
            '                     <label>Address:</label><div class="input full-width">                                                                                                  ' +
            '                         <input name="prompt-value" id="txt_Address" placeholder="Enter Address" value="" class="input-unstyled full-width" type="text">                    ' +
            '                 </div>                                                                                                                                                     ' +
            '                     </div>                                                                                                                                                 ' +

            ' <div class="two-columns  twelve-columns-mobile four-columns-tablet">          ' +
            
           '                         <input id="btn_RegisterSupplier" style="margin-top:20px" type="button" value="Register" class="button anthracite-gradient UpdateMarkup" onclick="ValidateLogin();" title="Submit Details" /> ' +
           
            ' </div> ' +

            '                 </div>                                                                                                                                                     ',


    });

    GetCountry()

    $('#selCountry').change(function () {
        var sndcountry = $('#selCountry').val();
        GetCity(sndcountry)

    });

}


function ValidateLogin() {
    var reg = new RegExp('[0-9]$');
    var regPan = new RegExp('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

    //$("#formAgent label").css("display", "none");
    //$("#formAgent label").html("* This field is required");

    var bValid = true;
    var sAgencyName = $("#txt_AgencyName").val();
    var sFirstName = $("#txt_FirstName").val();
    var sLastName = $("#txt_LastName").val();
    var sDesignation = $("#txt_Designation").val();
    var sAddress = $("#txt_Address").val();
    //var bIATAStatus = $("#Select_IATA").val();
    var nIATA ="";
    var sCity = $("#selCity").val();
    var sCountry = $("#selCountry").val();
    var nPinCode ="";
    var sEmail = $("#txt_Email").val();

    var nPhone = $("#txt_Phone").val();
    var nMobile = $("#txt_Mobile").val();
    var nFax = "";
    var sUsertype = "Agent";

    var sServiceTaxNumber = "";
    var nPAN ="";
    var sWebsite = "";
    var sGroupId = "";
    var sGroupName = "";

    var sAgencyLogo = "";
    var Currency = "";
    var sRemarks ="";
    var GSTNO = "";

    if (sAgencyName == "") {
        bValid = false;
        Success("Please Enter Agency Name");
        return;
    }

    if (sFirstName == "") {
        bValid = false;
        Success("Please Enter First Name");
        return;
    }
    if (sFirstName != "" && reg.test(sFirstName)) {
        bValid = false;
        Success("* First Name must not be numeric.");
        return;
    }
    if (sLastName == "") {
        bValid = false;
        Success("Please Enter Last Name");
        return;
    }
    if (sLastName != "" && reg.test(sLastName)) {
        bValid = false;
        Success("* Last Name must not be numeric.");
        return;
    }
    if (sDesignation == "") {
        bValid = false;
        Success("Please Enter Designation");
        return;
    }
    if (sDesignation != "" && reg.test(sDesignation)) {
        bValid = false;
        Success("* Designation must not be numeric at end.");
        return;
    }
    if (sEmail == "") {
        bValid = false;
        Success("Please Enter Email");
        return;
    }
    else {
        if (!(pattern.test(sEmail))) {
            bValid = false;
            Success("* Wrong email format.");
            return;
        }
    }

    if (sAddress == "") {
        bValid = false;
        Success("Please Enter Address");
        return;
    }
    if (sAddress != "" && reg.test(sAddress)) {
        bValid = false;
        Success("* Address must not be numeric at end.");
        return;
    }
    //if (bIATAStatus == "0") {
    //    bValid = false;
    //    Success("Please Select IATA STATUS");
    //    return;
    //}
    //if (bIATAStatus == "1" && nIATA == "") {
    //    bValid = false;
    //    Success("Please Enter IATA No");
    //    return;
    //}
    //if (bIATAStatus == "1" && nIATA != "" && !reg.test(nIATA)) {
    //    bValid = false;
    //    Success("* IATA no. must be numeric and valid.");
    //    return;
    //}
    if (sCity == "-") {
        bValid = false;
        Success("Please Select City");
        return;
    }
    if (sCountry == "-") {
        bValid = false;
        Success("Please Select Country");
        return;
    }
    if (nMobile == "") {
        bValid = false;
        Success("Please Enter Mobile No");
        return;
    }
    else {
        if (!(reg.test(nMobile))) {
            bValid = false;
            Success("* Mobile no. must be numeric.");
            return;
        }
    }
    //if (sUsertype == "") {
    //    bValid = false;
    //    Success("Please Select User Type");
    //}
    if (nFax == "") {
        nFax = '0';
    }
    else {
        if (!(reg.test(nFax))) {
            bValid = false;
            Success("* Fax no. must be numeric.");
            return;
        }
    }
    if (nPhone == "") {
        nPhone = '0';
    }
    else {
        if (!(reg.test(nPhone))) {
            bValid = false;
            Success("* Phone no must be numeric.");
            return;
        }
    }
    if (nPinCode == "") {
        nPinCode = '0';
    }
    else {
        if (!(reg.test(nPinCode))) {
            bValid = false;
            Success("* Pincode must be numeric.");
            return;
        }
    }
    if (nPAN == "") {
        nPAN = '0';
    }
    else {
        if (!(regPan.test(nPAN))) {
            bValid = false;
            Success("* PAN no must be correct.");
            return;
        }
    }

    //if (sGroupId == "-" || sGroupName == "") {
    //    bValid = false;
    //    Success("Please Select Group");
    //    return;
    //}
    if (Currency == "0") {
        bValid = false;
        Success("Please Select Currency");
        return;
    }
    //if (FrachiseId == undefined) {
    //    FrachiseId = "";
    //}
    FrachiseId = "";
    if (bValid == true) {
        //if (bIATAStatus != "1") {
            nIATA = "";
        //}

            $.ajax({
                type: "POST",
                url: "../HotelAdmin/Handler/GenralHandler.asmx/CheckForUniqueAgent",
                data: '{"sEmail":"' + sEmail + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        window.location.href = "AddQuotation.aspx";
                        //  window.location.href = "Default.aspx";
                        //  window.location.href = "../CUT/Default.aspx";
                        return false;
                    }
                    if (result.retCode == 1) {
                        Success("This email/username is already taken, Please choose another.")
                        //Success("This email/username is already taken, Please choose another.");
                        $("#txt_Email").css("border-color", "brown");
                        $("#txt_Email").focus();
                        //$("#txt_Mobile").css("border-color", "brown");
                        return false;
                    }
                    else if (result.retCode == 0) {
                        Success("Their is some error in processing your request, Please try again")
                        //Success("Their is some error in processing your request, Please try again");
                        return false;
                    }
                    else if (result.retCode == 2) {
                        $.ajax({
                            type: "POST",
                            url: "../HotelAdmin/Handler/GenralHandler.asmx/AddAgent",
                            //data: '{"sAgencyName":"' + sAgencyName + '","sEmail":"' + sEmail + '","sPassword":"' + sPassword + '","sFirstName":"' + sFirstName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '","bIATAStatus":"' + bIATAStatus + '","sUsertype":"' + sUsertype + '","nPAN":"' + nPAN + '","nPhone":"' + nPhone + '","nMobile":"' + nMobile + '","nFax":"' + nFax + '","sServiceTaxNumber":"' + sServiceTaxNumber + '","sWebsite":"' + sWebsite + '","sGroupId":"' + sGroupId + '","sAgencyLogo":"' + sAgencyLogo + '","sRemarks":"' + sRemarks + '","sbuttonvalue":"REGISTER"}',
                            // data: '{"sAgencyName":"' + sAgencyName + '","sEmail":"' + sEmail + '","sFirstName":"' + sFirstName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '","nIATA":"' + nIATA + '","sUsertype":"' + sUsertype + '","nPAN":"' + nPAN + '","nPhone":"' + nPhone + '","nMobile":"' + nMobile + '","nFax":"' + nFax + '","sServiceTaxNumber":"' + sServiceTaxNumber + '","sWebsite":"' + sWebsite + '","sGroupId":"' + sGroupId + '","sGroupName":"' + sGroupName + '","sVisaGroupId":"' + sVisaGroupId + '","sVisaGroupName":"' + sVisaGroupName + '","sAgencyLogo":"' + sAgencyLogo + '","sRemarks":"' + sRemarks + '","sbuttonvalue":"REGISTER"}',
                            data: '{"sAgencyName":"' + sAgencyName + '","sEmail":"' + sEmail + '","sFirstName":"' + sFirstName + '","sLastName":"' + sLastName + '","sDesignation":"' + sDesignation + '","sAddress":"' + sAddress + '","sCity":"' + sCity + '","nPinCode":"' + nPinCode + '","nIATA":"' + nIATA + '","sUsertype":"' + sUsertype + '","nPAN":"' + nPAN + '","nPhone":"' + nPhone + '","nMobile":"' + nMobile + '","nFax":"' + nFax + '","sServiceTaxNumber":"' + sServiceTaxNumber + '","sWebsite":"' + sWebsite + '","sGroupId":"' + sGroupId + '","sGroupName":"' + sGroupName + '","sAgencyLogo":"' + sAgencyLogo + '","sRemarks":"' + sRemarks + '","sbuttonvalue":"REGISTER","Currency":"' + Currency + '","FranchiseeId":"' + FrachiseId + '","GSTNO":"' + GSTNO + '"}',
                            contentType: "application/json; charset=utf-8",
                            datatype: "json",
                            success: function (response) {
                                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                                if (result.Session == 0) {
                                    window.location.href = "AddQuotation.aspx";
                                    //                                window.location.href = "../CUT/Default.aspx";
                                    return false;
                                }
                                if (result.retCode == 1) {
                                    //Success("Agent registered successfully. A mail containing password has been sent to registered mail. Now you can activate the agent/Upload the logo.");
                                    //Success("Agent registered successfully. A mail containing password has been sent to registered mail. Now you can activate the agent/Upload the logo.")
                                    Success("Agent registered successfully. A mail containing password has been sent to registered mail.")
                                    setTimeout(function () {
                                        window.location.href = "AddQuotation.aspx";
                                    }, 2000);
                                }
                                else if (result.retCode == 0) {
                                    //Success("Something went wrong! Please contact administrator.");
                                    Success("Something went wrong!")
                                }
                            },
                            error: function () {
                                Success("An error occured during registration!")
                                //Success("An error occured during registration! Please contact administrator.");
                            }
                        });
                    }
                },
                error: function () {
                }
            });
       
    }
}