﻿
$(document).ready(function () {
  //  GetMealPlan(0);
    GetTabs(1);
    //  GeneratesRoom(0);
    PasngrDetails(2, 0);
    $('#Select_Rooms').change(function () {
        if ($("#Select_Rooms option:selected").val() == 1) {
            $('#room2').css("display", "none");
            $('#room3').css("display", "none");
            $('#room4').css("display", "none");
            $('#room5').css("display", "none");
            $('#room6').css("display", "none");
            $('#room7').css("display", "none");
            $('#room8').css("display", "none");
            $('#room9').css("display", "none");

        }
        if ($("#Select_Rooms option:selected").val() == 2) {
            $('#room2').css("display", "");
            $('#room3').css("display", "none");
            $('#room4').css("display", "none");
            $('#room5').css("display", "none");
            $('#room6').css("display", "none");
            $('#room7').css("display", "none");
            $('#room8').css("display", "none");
            $('#room9').css("display", "none");

        }
        if ($("#Select_Rooms option:selected").val() == 3) {
            $('#room2').css("display", "");
            $('#room3').css("display", "");
            $('#room4').css("display", "none");
            $('#room5').css("display", "none");
            $('#room6').css("display", "none");
            $('#room7').css("display", "none");
            $('#room8').css("display", "none");
            $('#room9').css("display", "none");

        }
        if ($("#Select_Rooms option:selected").val() == 4) {
            $('#room2').css("display", "");
            $('#room3').css("display", "");
            $('#room4').css("display", "");
            $('#room5').css("display", "none");
            $('#room6').css("display", "none");
            $('#room7').css("display", "none");
            $('#room8').css("display", "none");
            $('#room9').css("display", "none");

        }
        if ($("#Select_Rooms option:selected").val() == 5) {
            $('#room2').css("display", "");
            $('#room3').css("display", "");
            $('#room4').css("display", "");
            $('#room5').css("display", "");
            $('#room6').css("display", "none");
            $('#room7').css("display", "none");
            $('#room8').css("display", "none");
            $('#room9').css("display", "none");

        }
        if ($("#Select_Rooms option:selected").val() == 6) {
            $('#room2').css("display", "");
            $('#room3').css("display", "");
            $('#room4').css("display", "");
            $('#room5').css("display", "");
            $('#room6').css("display", "");
            $('#room7').css("display", "none");
            $('#room8').css("display", "none");
            $('#room9').css("display", "none");

        }
        if ($("#Select_Rooms option:selected").val() == 7) {
            $('#room2').css("display", "");
            $('#room3').css("display", "");
            $('#room4').css("display", "");
            $('#room5').css("display", "");
            $('#room6').css("display", "");
            $('#room7').css("display", "");
            $('#room8').css("display", "none");
            $('#room9').css("display", "none");

        }
        if ($("#Select_Rooms option:selected").val() == 8) {
            $('#room2').css("display", "");
            $('#room3').css("display", "");
            $('#room4').css("display", "");
            $('#room5').css("display", "");
            $('#room6').css("display", "");
            $('#room7').css("display", "");
            $('#room8').css("display", "");
            $('#room9').css("display", "none");

        }
        if ($("#Select_Rooms option:selected").val() == 9) {
            $('#room2').css("display", "");
            $('#room3').css("display", "");
            $('#room4').css("display", "");
            $('#room5').css("display", "");
            $('#room6').css("display", "");
            $('#room7').css("display", "");
            $('#room8').css("display", "");
            $('#room9').css("display", "");

        }
    });


    $("#txtHotelName").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Genralhandler.asmx/GetHotelForBooking",
                data: "{'name':'" + document.getElementById('txtHotelName').value + "'}",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    response(result);

                },
                error: function (result) {
                    alert("No Match");
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            debugger
            $('#HotelCode').val(ui.item.id);
            //var test = $('#hdnAgCode').val();
            //GetAgentDetails($('#hdnAgCode').val())
        }
    });


});

function SelectType(Id) {
    //    if (Id == "rdb_Online") {
    //        $("#HotelDetals").css('display', 'none')
    //        $("#room1").css('display', 'none')
    //        $("#room2").css('display', 'none')
    //        $("#room3").css('display', 'none')
    //        $("#room4").css('display', 'none')
    //        $("#room5").css('display', 'none')
    //        $("#room6").css('display', 'none')
    //        $("#room7").css('display', 'none')
    //        $("#room8").css('display', 'none')
    //        $("#room9").css('display', 'none')
    //        $("#btn_Search").css('display', '')
    //        $("#btn_Save").css('display', 'none')
    //    }
    //    else {
    //        $("#HotelDetals").css('display', '')
    //        $("#room1").css('display', '')
    //        $("#btn_Search").css('display', 'none')
    //        $("#btn_Save").css('display', '')
    //    }

}

function GetMealPlan(Service) {
    $.ajax({
        type: "POST",
        url: "Genralhandler.asmx/GetGetMealPlan",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrMealPlan = result.MealPlan;
                $("#selMealPlan_" + Service).empty();
                if (arrMealPlan.length > 0) {
                    //  $("#selMealPlan").empty();
                    var ddlRequest = '';
                    // var ddlRequest = '<option selected="selected" value="IN">India</option>';
                    for (i = 0; i < arrMealPlan.length; i++) {
                        ddlRequest += '<option value="' + arrMealPlan[i].MealPlanName + '">' + arrMealPlan[i].MealPlanName + '</option>';
                    }
                    $("#selMealPlan_" + Service).append(ddlRequest);

                }
            }
        },
        error: function () {
        }
    });
}

//function GetTabs(value) {
//    $("#Tab").show();
//    $("#ul_Service").empty();
//    $("#Tab_Service").empty();
//    var div = ''; var ul = ''; var tab = '';
//    for (var i = 0; i < value; i++) {


//        if (i == 0) {
//            ul = '<li class="active"><a href="#Room' + i + '" data-toggle="tab" >Room No ' + (i + 1) + '</a></li>'
//            tab = '<div class="tab-pane active" id="Room' + i + '"></div>'
//            //  $("#txt_Service_" + id).val(arrServices[i])
//        }
//        else {
//            ul = '<li><a href="#Room' + i + '" data-toggle="tab" >Room No ' + (i + 1) + '</a></li>'
//            tab = '<div class="tab-pane" id="Room' + i + '"><span class="black">' + i + '</span><br/></div>'
//        }
//        $("#ul_Service").append(ul);
//        $("#Tab_Service").append(tab);

//        GeneratesRoom(i);
//    }

//    //ul += '<li><a  href="#New" style="cursor:pointer" data-toggle="tab"><i class="fa fa-edit"></i> Add More</a></li>'
//    //tab += '<div class="tab-pane " id="New"></div>'

//}

function GetTabs(value) {
    var div = ''; var ul = ''; var tab = '';
    div = '<div class="standard-tabs margin-bottom" id="add-tabs">';
    div += '<ul class="tabs">'
    for (var i = 0; i < value; i++) {
        $("#div_Rooms").empty();
        if (i == 0) {
            div += '<li class="active"><a href="#Room' + i + '" data-toggle="tab" >Room No ' + (i + 1) + '</a></li>'
        }
        else {
            div += '<li><a href="#Room' + i + '" data-toggle="tab" >Room No ' + (i + 1) + '</a></li>'
        }
    }
    div += '</ul>'
    div += '<div class="tabs-content">'
    for (var i = 0; i < value; i++) {
        div += '<div  class="with-padding" id="Room' + i + '">'
        div += '<div class="twelve-columns twelve-columns-mobile">'
        div += '<label>'
        div += 'Room Details '
        div += '</label>'
        div += ' <div class="line2"></div>'
        div += '</div>'
        div += '<br/>'


        div += '<div class="columns">'

        div += '    <div class="three-columns six-columns-mobile">'
        div += '        <label>Room Type</label>'
        div += '        <br />'
        div += '        <input type="text" placeholder="Type" id="txtRoomType_' + i + '" class="input  ui-autocomplete-input full-width RoomType_' + i + '" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_RoomType_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'

        div += '    <div class="three-columns six-columns-mobile">'
        div += '       <label>Meal Plan</label>'
        div += '        <br />'
        div += '        <select id="selMealPlan_' + i + '" class="select full-width selMealPlan_' + i + '">'
        div += '            <option value="-" selected="selected">Select Meal Plan</option>'
        div += '            <option value="BB">Bed & Breakfast</option>'
        div += '            <option value="RO">Room Only</option>'
        div += '            <option value="HB">Half Board</option>'
        div += '            <option value="FB">Full Board</option>'
        div += '        </select>'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_selMealPlan_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'

        //div += '    <div class="three-columns six-columns-mobile">'
        //div += '       <label>Booking Reference</label>'
        //div += '       <br />'
        //div += '       <input type="text" placeholder="Booking Reference" id="txtBookingRef_' + i + '" class="input  ui-autocomplete-input full-width BookingRef_' + i + '" />  '
        //div += '       <label style="color: red; margin-top: 3px; display: none" id="lbl_BookingRef_' + i + '">'
        //div += '           <b>* This field is required</b></label>'
        //div += '   </div>'

        div += '</div>'

        div += '<div class="columns">'


        div += '<div class="two-columns six-columns-mobile">'
        div += '    <label>Adults</label>'
        div += '    <select id="Select_Adults_' + i + '" class="select full-width" onchange="PasngrDetails(this.value,\'' + i + '\')">'
        div += '        <option value="1">1</option>'
        div += '        <option value="2" selected="selected">2</option>'
        div += '        <option value="3">3</option>'
        div += '        <option value="4">4</option>'
        div += '        <option value="5">5</option>'
        div += '        <option value="6">6</option>'
        div += '        <option value="7">7</option>'
        div += '        <option value="8">8</option>'
        div += '        <option value="9">9</option>'
        div += '        <option value="10">10</option>'
        div += '    </select>'
        div += '</div>'

        div += '<div class="two-columns six-columns-mobile">'
        div += '    <label>Child</label>'
        div += '    <select id="Select_Children_' + i + '" class="select full-width" onchange="Child(this.value,\'' + i + '\')">'
        div += '        <option selected="selected" value="0">0</option>'
        div += '        <option value="1">1</option>'
        div += '        <option value="2">2</option>'
        div += '        <option value="3">3</option>'
        div += '        <option value="4">4</option>'
        div += '    </select>'
        div += '</div>'

        div += '<div class="two-columns six-columns-mobile"  id="div_AgeChildFirst_' + i + '" style="display: none;">'
        div += '    <span id="Span_AgeChildFirst_' + i + '" class="text-left" style="display: none;">Age 1</span>'
        div += '    <select id="Select_AgeChildFirst_' + i + '" class="select full-width" style="display: none">'
        div += '        <option selected="selected" value="2">2</option>'
        div += '        <option value="3">3</option>'
        div += '        <option value="4">4</option>'
        div += '        <option value="5">5</option>'
        div += '        <option value="6">6</option>'
        div += '        <option value="7">7</option>'
        div += '        <option value="8">8</option>'
        div += '        <option value="9">9</option>'
        div += '        <option value="10">10</option>'
        div += '        <option value="11">11</option>'
        div += '        <option value="12">12</option>'
        div += '    </select>'
        div += '</div>'

        div += '<div class="two-columns six-columns-mobile" id="div_AgeChildSecond_' + i + '" style="display: none;">'
        div += '    <span id="Span_AgeChildSecond_' + i + '"  class="text-left" style="display: none;">Age 2</span>'
        div += '    <select id="Select_AgeChildSecond_' + i + '" class="select full-width" style="display: none;">'
        div += '        <option selected="selected" value="2">2</option>'
        div += '        <option value="3">3</option>'
        div += '        <option value="4">4</option>'
        div += '        <option value="5">5</option>'
        div += '        <option value="6">6</option>'
        div += '        <option value="7">7</option>'
        div += '        <option value="8">8</option>'
        div += '        <option value="9">9</option>'
        div += '        <option value="10">10</option>'
        div += '        <option value="11">11</option>'
        div += '        <option value="12">12</option>'
        div += '    </select>'
        div += '</div>'

        div += '<div class="two-columns six-columns-mobile" id="div_AgeChildThird_' + i + '" style="display: none;">'
        div += '    <span id="Span_AgeChildThird_' + i + '" class="text-left" style="display: none;">Age 3</span>'
        div += '    <select id="Select_AgeChildThird_' + i + '" class="select full-width" style="display: none;">'
        div += '        <option selected="selected" value="2">2</option>'
        div += '        <option value="3">3</option>'
        div += '        <option value="4">4</option>'
        div += '        <option value="5">5</option>'
        div += '        <option value="6">6</option>'
        div += '        <option value="7">7</option>'
        div += '        <option value="8">8</option>'
        div += '        <option value="9">9</option>'
        div += '        <option value="10">10</option>'
        div += '        <option value="11">11</option>'
        div += '        <option value="12">12</option>'
        div += '    </select>'
        div += '</div>'

        div += '<div class="two-columns six-columns-mobile" id="div_AgeChildFourth_' + i + '" style="display: none;">'
        div += '    <span id="Span_AgeChildFourth_' + i + '" class="text-left" style="display: none;">Age 4</span>'
        div += '    <select id="Select_AgeChildFourth_' + i + '" class="select full-width" style="display: none;">'
        div += '        <option selected="selected" value="2">2</option>'
        div += '        <option value="3">3</option>'
        div += '        <option value="4">4</option>'
        div += '        <option value="5">5</option>'
        div += '        <option value="6">6</option>'
        div += '        <option value="7">7</option>'
        div += '        <option value="8">8</option>'
        div += '        <option value="9">9</option>'
        div += '        <option value="10">10</option>'
        div += '        <option value="11">11</option>'
        div += '                <option value="12">12</option>'
        div += '            </select>'
        div += '       </div>'
        //div += '<div class="" id="ChildAges' + i + '">'
        //div += '</div>'

        div += '</div>'

        div += '<div class="" id="PassengerDetails' + i + '">'
        div += '</div>'

        div += '<div class="" id="PassengerChildDetails' + i + '">'
        div += '</div>'


        /////////////// End Room Details //////////////////////

        div += '       <br>'
        div += '       <br>'


        ///////////////  Room GST Details //////////////////////
        div += '<div class="columns">'
        // div += '<div class="three-columns six-columns-mobile" style="width:50%">'
        div += '<label>'
        div += 'Room Charge Details '
        div += '</label>'
        div += '   <div class="line2"></div>'
        div += '</div>'
        //  div += '</div>'   

        div += '       <br/>'



        div += '<div class="columns">'

        ///////////////  Supplier Charges

        div += '<div class="six-columns twelve-columns-mobile">'

        div += '<div class="columns">'
        div += '<div class="six-columns twelve-columns-mobile">'
        div += '<label>'
        div += 'Supplier Charge Details '
        div += '</label>'
        div += '   <div class="line2"></div>'
        div += '</div>'
        div += '</div>'

        div += '<div class="columns">'
        div += '    <div class="six-columns twelve-columns-mobile">'
        div += '        <span class="text-left ">Charges</span>'
        div += '        <br />'
        div += '        <input type="text" placeholder="0.0" id="txtBookingCharges_' + i + '" class="input  ui-autocomplete-input full-width BookingCharges_' + i + '" onchange="CalTotalAmount(' + i + ')" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_BookingCharges_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'
        div += '</div>'

        div += '<br/>'

        div += '<div class="columns">'
        div += '    <div class="six-columns twelve-columns-mobile">'
        div += '        <span class="text-left ">Supplier Charges</span>'
        div += '        <br />'
        div += '        <input type="text" placeholder="0.0" id="txtSupplierCharges_' + i + '" class="input  ui-autocomplete-input full-width SupplierCharges_' + i + '" onchange="CalTotalAmount(' + i + ')"  />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_SupplierCharges_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'
        div += '</div>'

        div += '<br/>'

        div += '<div class="columns">'
        div += '    <div class="six-columns twelve-columns-mobile">'
        div += '        <span class="text-left ">Markup Amount</span>'
        div += '        <input type="text" placeholder="0.0" id="txtMarkupAmount_' + i + '" class="input  ui-autocomplete-input full-width MarkupAmount_' + i + '"  onchange="SetGSTPercent(this.value,\'' + i + '\' )" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_MarkupAmount_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'
        div += '</div>'

        div += '<br/>'


        div += '<div class="columns">'
        div += '    <div class="six-columns twelve-columns-mobile">'
        div += '        <span class="text-left ">Discount</span>'
        div += '        <input type="text" placeholder="0.0" id="txtDiscount_' + i + '" class="input  ui-autocomplete-input full-width Discount_' + i + '"  onchange="CalTotalAmount(' + i + ')" / >'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_Discount_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'
        div += '</div>'

        div += '       <br/>'

        //////// GST Field ////////////

        div += '<div class="columns" id="label_' + i + '" style="display:none">'
        div += '    <div class="two-columns six-columns-mobile">'
        div += '        <span class="text-left "></span>'
       // div += '        <br />'
        div += '</div>'

        div += '<div class="two-columns six-columns-mobile">'
        div += '        <span class="text-left ">Percent</span>'
        div += '        <br />'
        div += '</div>'

        div += '<div class="two-columns six-columns-mobile">'
        div += '        <span class="text-left ">Amount</span>'
      //  div += '        <br />'
        div += '</div>'

      //  div += '</div>'

        ////////////// IGST  ////////////////////////////

        div += '<div class="columns" id="IGST_' + i + '" style="display:none">'

        div += '<div class="three-columns six-columns-mobile">'
        div += '        <span class="text-left "></span>'
        div += '        <br />'
        // div += '        <input type="text" placeholder="0.0" id="txtGST_' + Service + '" class="form-control" />'
        div += '        <label  id="lblIGST_' + i + '" style="margin-left:30%" class="">'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_GST_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '</div>'

        div += '<div class="three-columns six-columns-mobile">'
        div += '        <span class="text-left "></span>'
        div += '        <br />'
        div += '        <input type="text" placeholder="0.0" id="txtIGSTPercent_' + i + '" class="input  ui-autocomplete-input full-width" readonly="readonly"/>'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CGST_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '</div>'

        div += '<div class="three-columns six-columns-mobile">'
        div += '        <span class="text-left "></span>'
        div += '        <br />'
        div += '        <input type="text" placeholder="0.0" id="txtIGSTAmount_' + i + '" class="input  ui-autocomplete-input full-width" readonly="readonly"  />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_IGST_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '</div>'

        //div += '    <div class="col-md-2">'
        //div += '        <span class="text-left ">GST Total</span>'
        //div += '        <br />'
        //div += '        <input type="text" placeholder="0.0" id="txtGSTTotal_' + Service + '" class="form-control" />'
        //div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_GSTTotal_' + Service + '">'
        //div += '            <b>* This field is required</b></label>'
        //div += '    </div>'

        div += '</div>'

        ////////////// End IGST  ////////////////////////////

        //    ////////////// SGST  ////////////////////////////

        div += '<div class="columns" id="SGST_' + i + '" style="display:none">'

        div += '<div class="three-columns six-columns-mobile">'
        div += '        <span class="text-left "></span>'
        div += '        <br />'
        // div += '        <input type="text" placeholder="0.0" id="txtGST_' + Service + '" class="form-control" />'
        div += '        <label id="lblSGST_' + i + '" style="margin-left:30%" class="lato size20 slim">'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_GST_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '</div>'

        div += '<div class="three-columns six-columns-mobile">'
        div += '        <span class="text-left "></span>'
        div += '        <br />'
        div += '        <input type="text" placeholder="0.0" id="txtSGSTPercent_' + i + '" class="input  ui-autocomplete-input full-width" readonly="readonly"/>'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CGST_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '</div>'

        div += '<div class="three-columns six-columns-mobile">'
        div += '        <span class="text-left "></span>'
        div += '        <br />'
        div += '        <input type="text" placeholder="0.0" id="txtSGSTAmount_' + i + '" class="input  ui-autocomplete-input full-width" readonly="readonly" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_IGST_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '</div>'

        //div += '    <div class="col-md-2">'
        //div += '        <span class="text-left ">GST Total</span>'
        //div += '        <br />'
        //div += '        <input type="text" placeholder="0.0" id="txtGSTTotal_' + Service + '" class="form-control" />'
        //div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_GSTTotal_' + Service + '">'
        //div += '            <b>* This field is required</b></label>'
        //div += '    </div>'

        div += '</div>'

        ////////////// End SGST  ////////////////////////////

        //    ////////////// CGST  ////////////////////////////

        div += '<div class="columns" id="CGST_' + i + '" style="display:none">'

        div += '    <div class="three-columns six-columns-mobile">'
        div += '        <span class="text-left "></span>'
        div += '        <br />'
        // div += '        <input type="text" placeholder="0.0" id="txtGST_' + Service + '" class="form-control" />'
        div += '        <label id="lblCGST_' + i + '" style="margin-left:30%" class="lato size20 slim">'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_GST_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'

        div += '    <div class="three-columns six-columns-mobile">'
        div += '        <span class="text-left "></span>'
        div += '        <br />'
        div += '        <input type="text" placeholder="0.0" id="txtCGSTPercent_' + i + '" class="input  ui-autocomplete-input full-width" readonly="readonly"/>'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CGST_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'

        div += '    <div class="three-columns six-columns-mobile">'
        div += '        <span class="text-left "></span>'
        div += '        <br />'
        div += '        <input type="text" placeholder="0.0" id="txtCGSTAmount_' + i + '" class="input  ui-autocomplete-input full-width" readonly="readonly" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_IGST_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'

        //div += '    <div class="col-md-2">'
        //div += '        <span class="text-left ">GST Total</span>'
        //div += '        <br />'
        //div += '        <input type="text" placeholder="0.0" id="txtGSTTotal_' + Service + '" class="form-control" />'
        //div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_GSTTotal_' + Service + '">'
        //div += '            <b>* This field is required</b></label>'
        //div += '    </div>'

        div += '    </div>'

        ////////////// End CGST  ////////////////////////////

        ////////////// UGST  ////////////////////////////////

        div += '<div class="columns" id="UGST_' + i + '" style="display:none">'

        div += '    <div class="three-columns six-columns-mobile">'
        div += '        <span class="text-left "></span>'
        div += '        <br />'
        // div += '        <input type="text" placeholder="0.0" id="txtGST_' + Service + '" class="form-control" />'
        div += '        <label id="lblUGST_' + i + '" style="margin-left:30%" class="lato size20 slim">'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_UGST_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'

        div += '    <div class="three-columns six-columns-mobile">'
        div += '        <span class="text-left "></span>'
        div += '        <br />'
        div += '        <input type="text" placeholder="0.0" id="txtUGSTPercent_' + i + '" class="input  ui-autocomplete-input full-width" readonly="readonly"/>'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_UGST_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'

        div += '    <div class="three-columns six-columns-mobile">'
        div += '        <span class="text-left "></span>'
        div += '        <br />'
        div += '        <input type="text" placeholder="0.0" id="txtUGSTAmount_' + i + '" class="input  ui-autocomplete-input full-width" readonly="readonly" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_IGST_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'

        //div += '    <div class="col-md-2">'
        //div += '        <span class="text-left ">GST Total</span>'
        //div += '        <br />'s
        //div += '        <input type="text" placeholder="0.0" id="txtGSTTotal_' + Service + '" class="form-control" />'
        //div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_GSTTotal_' + Service + '">'
        //div += '            <b>* This field is required</b></label>'
        //div += '    </div>'

        div += '    </div>'
        div += '    </div>'

        ////////////// End UGST /////////////////////////////////

        ////////// End GST Field ////////////

        div += '       <br/>'

        div += '<div class="columns">'
        div += '    <div class="four-columns twelve-columns-mobile">'
        div += '        <span class="text-left ">Total</span>'
        div += '        <input type="text" placeholder="0.0" id="txtTotal_' + i + '" class="input  ui-autocomplete-input full-width Total_' + i + '" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_Total_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'
        div += '    </div>'

        div += '       <br/>'

        div += '    </div>'
      


        ///////////////// End Supplier Charges //////////////////////

        //    //////////////// Purchase Charges ///////////////////

        div += '<div class="six-columns twelve-columns-mobile">'

        div += '<div class="columns">'
        div += '<div class="six-columns twelve-columns-mobile">'
        div += '   <label>'
        div += '                 Purchase Charge Details '
        div += '   </label>'
        div += '   <div class="line2"></div>'
        div += '</div>'
        div += '</div>'

       

        div += '<div class="columns">'
        div += '    <div class="six-columns twelve-columns-mobile">'
        div += '        <span class="text-left ">Purchase Cost</span>'
        div += '        <br />'
        div += '        <input type="text" placeholder="0.0" id="txtPurchaseCharges_' + i + '" class="input  ui-autocomplete-input full-width PurchaseCharges_' + i + '"  onchange="CalPurchaseTotalAmount(' + i + ')" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_PurchaseCharges_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'
        div += '</div>'

        div += '       <br/>'

        div += '<div class="columns">'
        div += '    <div class="six-columns twelve-columns-mobile">'
        div += '        <span class="text-left ">Purchase currency</span>'
        div += '        <br />'
        div += '        <select id="selPurchasecurrency_' + i + '" class="select full-width selPurchasecurrency_' + i + '">'
        div += '            <option value="-" selected="selected">Select Purchase currency</option>'
        div += '            <option value="INR">INR</option>'
        div += '            <option value="AED">AED</option>'
        div += '            <option value="GBP">GBP</option>'
        div += '            <option value="USD">USD</option>'
        div += '            <option value="EUR">EUR</option>'
        div += '            <option value="PKR">PKR</option>'
        div += '        </select>'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_selPurchasecurrency_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'
        div += '</div>'

        div += '       <br/>'

        div += '<div class="columns">'
        div += '    <div class="six-columns twelve-columns-mobile">'
        div += '        <span class="text-left ">Commission</span>'
        div += '        <input type="text" placeholder="0.0" id="txtCommission_' + i + '" class="input  ui-autocomplete-input full-width Commission_' + i + '" onchange="CalPurchaseTotalAmount(' + i + ')" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_Commission_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'
        div += '    </div>'

        div += '       <br/>'
        
        div += '<div class="columns">'
        div += '    <div class="six-columns twelve-columns-mobile">'
        div += '        <span class="text-left ">Total</span>'
        div += '        <input type="text" placeholder="0.0" id="txtPurchaseTotal_' + i + '" class="input  ui-autocomplete-input full-width PurchaseTotal_' + i + '" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_PurchaseTotal_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'
        div += '    </div>'
        div += '       <br/>'
        div += '    </div>'

        //////////////// End Purchase Charges ///////////////////

        div += '    </div>'
        ///////////////// End Room GST Details //////////////////////
        div += '<br>'
        div += '<br>'

        //    ///////////////// Cancellation Details //////////////////////

        div += '<div class="columns">'
        div += '<div class="three-columns six-columns-mobile">'
        div += '   <label>'
        div += '                 Cancellations Details '
        div += '   </label>'
        div += '   <div class="line2"></div>'
        div += '</div>'
        div += '</div>'
      
        div += '       <br/>'
      
        div += '<div class="columns">'
      
        div += '    <div class="two-columns six-columns-mobile">'
        div += '        <span class="text-left ">Cancellation Note</span>'
        div += '        <br />'
        div += '        <input type="text" placeholder="Note" id="txtCancellationNote_' + i + '_' + i + '" class="input  ui-autocomplete-input full-width CancellationNote_' + i + '" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CancellationNote_' + i + '_' + i + '  ">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'
      
        div += '    <div class="two-columns six-columns-mobile">'
        div += '        <span class="text-left">Cancellation Date</span>'
        div += '        <br />'
        div += '       <input class="input full-width mySelectCalendar CancellationDate_' + i + '" type="text" id="datepicker_CancellationDate_' + i + '_' + i + '" readonly="readonly" style="cursor: pointer" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CancellationDate_' + i + '_' + i + '  ">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'
      
        div += '    <div class="two-columns six-columns-mobile">'
        //div += '        <span class="text-left ">Cancellation Date</span>'
        div += '        <br />'
        div += '       <input class="input full-width CancellationTime_' + i + '" type="time" id="txtancellationTime_' + i + '_' + i + '"  style="cursor: pointer" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CancellationTime_' + i + '_' + i + '  ">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'
      
        div += '    <div class="two-columns six-columns-mobile">'
        div += '        <span class="text-left ">Cancellation Amount</span>'
        div += '        <br />'
        div += '        <input type="text" placeholder="0.0" id="txtCancellationAmount_' + i + '_' + i + '" class="input  ui-autocomplete-input full-width CancellationAmount_' + i + '" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CancellationAmount_' + i + '_' + i + '  ">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'
      
        div += '    <div class="two-columns six-columns-mobile">'
        div += '        <br />'
        div += '        <input type="button" placeholder="Type" value="+" id="btnAddCancel_' + i + '_' + i + '" onclick="AddCancellation(' + i + ')" class="button anthracite-gradient" style="cursor:pointer" title="Add" />'
        div += '    </div>'
      
        div += '       </div>'
      
      
      
        div += '<div  id="CancellationDate' + i + '" style="display:none">'
        div += '       </div>'
      
        ///////////////// End Cancelllation Details //////////////////////


            $('#txtTotal_' + i).change(function () {

            TotalRoomFare += parseFloat($('#txtTotal_' + i).val());
            $("#txtTotalFare").val(TotalRoomFare);
            });
      
       


        div += '</div>'
    }
    div += '</div>'
    $("#div_Rooms").append(div);

    for (var i = 0; i < value; i++) {
        $('#datepicker_CancellationDate_' + i + '_' + i + '').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd-mm-yy",
            // onSelect: insertDepartureDate,
            // dateFormat: "yy-m-d",
            //   minDate: "dateToday",
            // maxDate: "+3M +10D"
        });
    }
}

function GeneratesRoom(Service) {

    //  $("#Room" + Service).empty();
    var div = '';

    ///////////////// Room Details //////////////////////

    div += '<div class="tabs-content">'
    div += '<div class="with-padding">'


    div += '</div>'
    div += '</div>'

    $("#div_Rooms").append(div);
}


//function GeneratesRoom(Service) {

//    $("#Room" + Service).empty();
//    var div = '';

//    ///////////////// Room Details //////////////////////


//    div += '<div class="row">'
//    div += '<div class="col-md-12">'
//    div += '   <p class="lato size20 slim">'
//    div += '                 Room Details '
//    div += '   </p>'
//    div += '   <div class="line2"></div>'
//    div += '</div>'
//    div += '</div>'

//    div += '       <br/>'


//    div += '<div class="row">'

//    div += '    <div class="col-md-4">'
//    div += '        <span class="text-left ">Room Type</span>'
//    div += '        <br />'
//    div += '        <input type="text" placeholder="Type" id="txtRoomType_' + Service + '" class="form-control RoomType_' + Service + '" />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_RoomType_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    div += '    <div class="col-md-4">'
//    div += '        <span class="text-left ">Meal Plan</span>'
//    div += '        <br />'
//    div += '        <select id="selMealPlan_' + Service + '" class="form-control selMealPlan_' + Service + '">'
//    div += '            <option value="-" selected="selected">Select Meal Plan</option>'
//    div += '        </select>'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_selMealPlan_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    div += '    <div class="col-md-4">'
//    div += '       <span class="text-left ">Booking Reference</span>'
//    div += '       <br />'
//    div += '       <input type="text" placeholder="Booking Reference" id="txtBookingRef_' + Service + '" class="form-control BookingRef_' + Service + '" />  '
//    div += '       <label style="color: red; margin-top: 3px; display: none" id="lbl_BookingRef_' + Service + '">'
//    div += '           <b>* This field is required</b></label>'
//    div += '   </div>'

//    div += '</div>'

//    div += '       <br/>'

//    ///////////////// Occupancy Details //////////////////////



//    div += '<div class="row">'

//    div += '<div class="col-md-2 col-xs-3">'
//    div += '    <span class="text-left" class="text-trans">Adults</span>'
//    div += '    <select id="Select_Adults_' + Service + '" class="form-control" onchange="PasngrDetails(this.value,\'' + Service + '\')">'
//    div += '        <option value="1">1</option>'
//    div += '        <option value="2" selected="selected">2</option>'
//    div += '        <option value="3">3</option>'
//    div += '        <option value="4">4</option>'
//    div += '        <option value="5">5</option>'
//    div += '        <option value="6">6</option>'
//    div += '        <option value="7">7</option>'
//    div += '        <option value="8">8</option>'
//    div += '        <option value="9">9</option>'
//    div += '        <option value="10">10</option>'
//    div += '    </select>'
//    div += '</div>'

//    div += '<div class="col-md-2 col-xs-3">'
//    div += '    <span class="text-left" class="text-trans">Child</span>'
//    div += '    <select id="Select_Children_' + Service + '" class="form-control" onchange="Child(this.value,\'' + Service + '\')">'
//    div += '        <option selected="selected" value="0">0</option>'
//    div += '        <option value="1">1</option>'
//    div += '        <option value="2">2</option>'
//    div += '        <option value="3">3</option>'
//    div += '        <option value="4">4</option>'
//    div += '    </select>'
//    div += '</div>'

//    div += '<div class="col-md-2 col-xs-3">'
//    div += '    <span id="Span_AgeChildFirst_' + Service + '" class="text-left" style="display: none;">Age 1</span>'
//    div += '    <select id="Select_AgeChildFirst_' + Service + '" class="form-control" style="display: none">'
//    div += '        <option selected="selected" value="2">2</option>'
//    div += '        <option value="3">3</option>'
//    div += '        <option value="4">4</option>'
//    div += '        <option value="5">5</option>'
//    div += '        <option value="6">6</option>'
//    div += '        <option value="7">7</option>'
//    div += '        <option value="8">8</option>'
//    div += '        <option value="9">9</option>'
//    div += '        <option value="10">10</option>'
//    div += '        <option value="11">11</option>'
//    div += '        <option value="12">12</option>'
//    div += '    </select>'
//    div += '</div>'

//    div += '<div class="col-md-2 col-xs-3">'
//    div += '    <span id="Span_AgeChildSecond_' + Service + '"  class="text-left" style="display: none;">Age 2</span>'
//    div += '    <select id="Select_AgeChildSecond_' + Service + '" class="form-control" style="display: none;">'
//    div += '        <option selected="selected" value="2">2</option>'
//    div += '        <option value="3">3</option>'
//    div += '        <option value="4">4</option>'
//    div += '        <option value="5">5</option>'
//    div += '        <option value="6">6</option>'
//    div += '        <option value="7">7</option>'
//    div += '        <option value="8">8</option>'
//    div += '        <option value="9">9</option>'
//    div += '        <option value="10">10</option>'
//    div += '        <option value="11">11</option>'
//    div += '        <option value="12">12</option>'
//    div += '    </select>'
//    div += '</div>'

//    div += '<div class="col-md-2 col-xs-3">'
//    div += '    <span id="Span_AgeChildThird_' + Service + '" class="text-left" style="display: none;">Age 3</span>'
//    div += '    <select id="Select_AgeChildThird_' + Service + '" class="form-control" style="display: none;">'
//    div += '        <option selected="selected" value="2">2</option>'
//    div += '        <option value="3">3</option>'
//    div += '        <option value="4">4</option>'
//    div += '        <option value="5">5</option>'
//    div += '        <option value="6">6</option>'
//    div += '        <option value="7">7</option>'
//    div += '        <option value="8">8</option>'
//    div += '        <option value="9">9</option>'
//    div += '        <option value="10">10</option>'
//    div += '        <option value="11">11</option>'
//    div += '        <option value="12">12</option>'
//    div += '    </select>'
//    div += '</div>'

//    div += '<div class="col-md-2 col-xs-3">'
//    div += '    <span id="Span_AgeChildFourth_' + Service + '" class="text-left" style="display: none;">Age 4</span>'
//    div += '    <select id="Select_AgeChildFourth_' + Service + '" class="form-control" style="display: none;">'
//    div += '        <option selected="selected" value="2">2</option>'
//    div += '        <option value="3">3</option>'
//    div += '        <option value="4">4</option>'
//    div += '        <option value="5">5</option>'
//    div += '        <option value="6">6</option>'
//    div += '        <option value="7">7</option>'
//    div += '        <option value="8">8</option>'
//    div += '        <option value="9">9</option>'
//    div += '        <option value="10">10</option>'
//    div += '        <option value="11">11</option>'
//    div += '                <option value="12">12</option>'
//    div += '            </select>'
//    div += '       </div>'

//    div += '       </div>'

//    div += '<br>'


//    div += '<div id="PassengerDetails' + Service + '" style="display:none">'
//    div += '       </div>'

//    div += '<br>'

//    div += '<div id="PassengerChildDetails' + Service + '" style="display:none">'
//    div += '       </div>'

//    ///////////////// End Occupancy Details //////////////////////

//    ///////////////// End Room Details //////////////////////

//    div += '       <br>'


//    /////////////////  Room GST Details //////////////////////
//    //div += '<div class="row">'
//    //div += '<div class="col-md-12">'
//    //div += '   <p class="lato size20 slim">'
//    //div += '                 Room Charge Details '
//    //div += '   </p>'
//    //div += '   <div class="line2"></div>'
//    //div += '</div>'
//    //div += '</div>'   

//    div += '       <br/>'

//    div += '<div class="row">'

//    /////////////////  Supplier Charges

//    div += '<div class="col-md-6" style="width:50%">'

//    div += '<div class="row">'
//    div += '<div class="col-md-12">'
//    div += '   <p class="lato size20 slim">'
//    div += '                 Supplier Charge Details '
//    div += '   </p>'
//    div += '   <div class="line2"></div>'
//    div += '</div>'
//    div += '</div>'

//    //div += '<div class="row">'
//    //div += '    <div class="col-md-6">'
//    //div += '        <span class="text-left ">Charges</span>'
//    //div += '        <br />'
//    //div += '        <input type="text" placeholder="0.0" id="txtBookingCharges_' + Service + '" class="form-control BookingCharges_' + Service + '" onchange="CalTotalAmount(' + Service + ')" />'
//    //div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_BookingCharges_' + Service + '">'
//    //div += '            <b>* This field is required</b></label>'
//    //div += '    </div>'
//    //div += '    </div>'

//    div += '       <br/>'

//    div += '<div class="row">'
//    div += '    <div class="col-md-6">'
//    div += '        <span class="text-left ">Supplier Charges</span>'
//    div += '        <br />'
//    div += '        <input type="text" placeholder="0.0" id="txtSupplierCharges_' + Service + '" class="form-control SupplierCharges_' + Service + '" onchange="CalTotalAmount(' + Service + ')"  />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_SupplierCharges_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'
//    div += '    </div>'

//    div += '       <br/>'

//    div += '<div class="row">'
//    div += '    <div class="col-md-6">'
//    div += '        <span class="text-left ">Markup Amount</span>'
//    div += '        <input type="text" placeholder="0.0" id="txtMarkupAmount_' + Service + '" class="form-control MarkupAmount_' + Service + '"  onchange="SetGSTPercent(this.value,\'' + Service + '\' )" />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_MarkupAmount_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'
//    div += '    </div>'

//    div += '       <br/>'


//    div += '<div class="row">'
//    div += '    <div class="col-md-6">'
//    div += '        <span class="text-left ">Discount</span>'
//    div += '        <input type="text" placeholder="0.0" id="txtDiscount_' + Service + '" class="form-control Discount_' + Service + '"  onchange="CalTotalAmount(' + Service + ')" / >'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_Discount_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'
//    div += '    </div>'

//    div += '       <br/>'

//    ////////// GST Field ////////////

//    div += '<div class="row" id="label_' + Service + '" style="display:none">'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left "></span>'
//    div += '        <br />'
//    div += '    </div>'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left ">Percent</span>'
//    div += '        <br />'
//    div += '    </div>'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left ">Amount</span>'
//    div += '        <br />'
//    div += '    </div>'

//    div += '    </div>'

//    ////////////// IGST  ////////////////////////////

//    div += '<div class="row" id="IGST_' + Service + '" style="display:none">'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left "></span>'
//    div += '        <br />'
//    // div += '        <input type="text" placeholder="0.0" id="txtGST_' + Service + '" class="form-control" />'
//    div += '        <label  id="lblIGST_' + Service + '" style="margin-left:30%" class="lato size20 slim">'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_GST_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left "></span>'
//    div += '        <br />'
//    div += '        <input type="text" placeholder="0.0" id="txtIGSTPercent_' + Service + '" class="form-control" readonly="readonly"/>'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CGST_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left "></span>'
//    div += '        <br />'
//    div += '        <input type="text" placeholder="0.0" id="txtIGSTAmount_' + Service + '" class="form-control" readonly="readonly"  />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_IGST_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    //div += '    <div class="col-md-2">'
//    //div += '        <span class="text-left ">GST Total</span>'
//    //div += '        <br />'
//    //div += '        <input type="text" placeholder="0.0" id="txtGSTTotal_' + Service + '" class="form-control" />'
//    //div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_GSTTotal_' + Service + '">'
//    //div += '            <b>* This field is required</b></label>'
//    //div += '    </div>'

//    div += '    </div>'

//    ////////////// End IGST  ////////////////////////////

//    ////////////// SGST  ////////////////////////////

//    div += '<div class="row" id="SGST_' + Service + '" style="display:none">'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left "></span>'
//    div += '        <br />'
//    // div += '        <input type="text" placeholder="0.0" id="txtGST_' + Service + '" class="form-control" />'
//    div += '        <label id="lblSGST_' + Service + '" style="margin-left:30%" class="lato size20 slim">'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_GST_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left "></span>'
//    div += '        <br />'
//    div += '        <input type="text" placeholder="0.0" id="txtSGSTPercent_' + Service + '" class="form-control" readonly="readonly"/>'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CGST_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left "></span>'
//    div += '        <br />'
//    div += '        <input type="text" placeholder="0.0" id="txtSGSTAmount_' + Service + '" class="form-control" readonly="readonly" />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_IGST_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    //div += '    <div class="col-md-2">'
//    //div += '        <span class="text-left ">GST Total</span>'
//    //div += '        <br />'
//    //div += '        <input type="text" placeholder="0.0" id="txtGSTTotal_' + Service + '" class="form-control" />'
//    //div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_GSTTotal_' + Service + '">'
//    //div += '            <b>* This field is required</b></label>'
//    //div += '    </div>'

//    div += '    </div>'

//    ////////////// End SGST  ////////////////////////////


//    ////////////// CGST  ////////////////////////////

//    div += '<div class="row" id="CGST_' + Service + '" style="display:none">'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left "></span>'
//    div += '        <br />'
//    // div += '        <input type="text" placeholder="0.0" id="txtGST_' + Service + '" class="form-control" />'
//    div += '        <label id="lblCGST_' + Service + '" style="margin-left:30%" class="lato size20 slim">'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_GST_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left "></span>'
//    div += '        <br />'
//    div += '        <input type="text" placeholder="0.0" id="txtCGSTPercent_' + Service + '" class="form-control" readonly="readonly"/>'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CGST_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left "></span>'
//    div += '        <br />'
//    div += '        <input type="text" placeholder="0.0" id="txtCGSTAmount_' + Service + '" class="form-control" readonly="readonly" />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_IGST_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    //div += '    <div class="col-md-2">'
//    //div += '        <span class="text-left ">GST Total</span>'
//    //div += '        <br />'
//    //div += '        <input type="text" placeholder="0.0" id="txtGSTTotal_' + Service + '" class="form-control" />'
//    //div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_GSTTotal_' + Service + '">'
//    //div += '            <b>* This field is required</b></label>'
//    //div += '    </div>'

//    div += '    </div>'

//    ////////////// End CGST  ////////////////////////////

//    ////////////// UGST  ////////////////////////////////

//    div += '<div class="row" id="UGST_' + Service + '" style="display:none">'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left "></span>'
//    div += '        <br />'
//    // div += '        <input type="text" placeholder="0.0" id="txtGST_' + Service + '" class="form-control" />'
//    div += '        <label id="lblUGST_' + Service + '" style="margin-left:30%" class="lato size20 slim">'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_UGST_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left "></span>'
//    div += '        <br />'
//    div += '        <input type="text" placeholder="0.0" id="txtUGSTPercent_' + Service + '" class="form-control" readonly="readonly"/>'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_UGST_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left "></span>'
//    div += '        <br />'
//    div += '        <input type="text" placeholder="0.0" id="txtUGSTAmount_' + Service + '" class="form-control" readonly="readonly" />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_IGST_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    //div += '    <div class="col-md-2">'
//    //div += '        <span class="text-left ">GST Total</span>'
//    //div += '        <br />'s
//    //div += '        <input type="text" placeholder="0.0" id="txtGSTTotal_' + Service + '" class="form-control" />'
//    //div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_GSTTotal_' + Service + '">'
//    //div += '            <b>* This field is required</b></label>'
//    //div += '    </div>'

//    div += '    </div>'

//    ////////////// End UGST /////////////////////////////////

//    ////////// End GST Field ////////////
//    div += '       <br/>'

//    div += '<div class="row">'
//    div += '    <div class="col-md-6">'
//    div += '        <span class="text-left ">Total</span>'
//    div += '        <input type="text" placeholder="0.0" id="txtTotal_' + Service + '" class="form-control Total_' + Service + '" />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_Total_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'
//    div += '    </div>'

//    div += '       <br/>'

//    div += '    </div>'

//    ///////////////// End Supplier Charges //////////////////////


//    //////////////// Purchase Charges ///////////////////

//    div += '<div class="col-md-6" style="width:50%">'

//    div += '<div class="row">'
//    div += '<div class="col-md-12">'
//    div += '   <p class="lato size20 slim">'
//    div += '                 Purchase Charge Details '
//    div += '   </p>'
//    div += '   <div class="line2"></div>'
//    div += '</div>'
//    div += '</div>'

//    div += '       <br/>'

//    div += '<div class="row">'
//    div += '    <div class="col-md-6">'
//    div += '        <span class="text-left ">Purchase Cost</span>'
//    div += '        <br />'
//    div += '        <input type="text" placeholder="0.0" id="txtPurchaseCharges_' + Service + '" class="form-control PurchaseCharges_' + Service + '"  onchange="CalPurchaseTotalAmount(' + Service + ')" />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_PurchaseCharges_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'
//    div += '    </div>'

//    div += '       <br/>'

//    div += '<div class="row">'
//    div += '    <div class="col-md-6">'
//    div += '        <span class="text-left ">Purchase currency</span>'
//    div += '        <br />'
//    div += '        <select id="selPurchasecurrency_' + Service + '" class="form-control selPurchasecurrency_' + Service + '">'
//    div += '            <option value="-" selected="selected">Select Purchase currency</option>'
//    div += '            <option value="INR">INR</option>'
//    div += '            <option value="AED">AED</option>'
//    div += '            <option value="GBP">GBP</option>'
//    div += '            <option value="USD">USD</option>'
//    div += '            <option value="EUR">EUR</option>'
//    div += '            <option value="PKR">PKR</option>'
//    div += '        </select>'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_selPurchasecurrency_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'
//    div += '    </div>'

//    div += '       <br/>'

//    div += '<div class="row">'
//    div += '    <div class="col-md-6">'
//    div += '        <span class="text-left ">Commission</span>'
//    div += '        <input type="text" placeholder="0.0" id="txtCommission_' + Service + '" class="form-control Commission_' + Service + '" onchange="CalPurchaseTotalAmount(' + Service + ')" />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_Commission_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'
//    div += '    </div>'

//    div += '       <br/>'
//    div += '       <br/>'

//    div += '<div class="row">'
//    div += '    <div class="col-md-6">'
//    div += '        <span class="text-left ">Total</span>'
//    div += '        <input type="text" placeholder="0.0" id="txtPurchaseTotal_' + Service + '" class="form-control PurchaseTotal_' + Service + '" />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_PurchaseTotal_' + Service + '">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'
//    div += '    </div>'

//    div += '       <br/>'

//    div += '    </div>'

//    //////////////// End Purchase Charges ///////////////////

//    div += '    </div>'
//    ///////////////// End Room GST Details //////////////////////


//    div += '<br>'
//    div += '<br>'



//    ///////////////// Cancellation Details //////////////////////

//    div += '<div class="row">'
//    div += '<div class="col-md-12">'
//    div += '   <p class="lato size20 slim">'
//    div += '                 Cancellations Details '
//    div += '   </p>'
//    div += '   <div class="line2"></div>'
//    div += '</div>'
//    div += '</div>'

//    div += '       <br/>'

//    div += '<div class="row">'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left ">Cancellation Note</span>'
//    div += '        <br />'
//    div += '        <input type="text" placeholder="Note" id="txtCancellationNote_' + Service + '_' + Service + '" class="form-control CancellationNote_' + Service + '" />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CancellationNote_' + Service + '_' + Service + '  ">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    div += '    <div class="col-md-2">'
//    div += '        <span class="text-left ">Cancellation Date</span>'
//    div += '        <br />'
//    div += '       <input class="form-control mySelectCalendar CancellationDate_' + Service + '" type="text" id="datepicker_CancellationDate_' + Service + '_' + Service + '" readonly="readonly" style="cursor: pointer" />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CancellationDate_' + Service + '_' + Service + '  ">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    div += '    <div class="col-md-2">'
//    //div += '        <span class="text-left ">Cancellation Date</span>'
//    div += '        <br />'
//    div += '       <input class="form-control CancellationTime_' + Service + '" type="time" id="txtancellationTime_' + Service + '_' + Service + '"  style="cursor: pointer" />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CancellationTime_' + Service + '_' + Service + '  ">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    div += '    <div class="col-md-3">'
//    div += '        <span class="text-left ">Cancellation Amount</span>'
//    div += '        <br />'
//    div += '        <input type="text" placeholder="0.0" id="txtCancellationAmount_' + Service + '_' + Service + '" class="form-control CancellationAmount_' + Service + '" />'
//    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CancellationAmount_' + Service + '_' + Service + '  ">'
//    div += '            <b>* This field is required</b></label>'
//    div += '    </div>'

//    div += '    <div class="col-md-1">'
//    div += '        <br />'
//    div += '        <input type="button" placeholder="Type" value="+" id="btnAddCancel_' + Service + '_' + Service + '" onclick="AddCancellation(' + Service + ')" class="btn-search margtop-2" style="cursor:pointer" title="Add" />'
//    div += '    </div>'

//    div += '       </div>'



//    div += '<div  id="CancellationDate' + Service + '" style="display:none">'
//    div += '       </div>'

//    ///////////////// End Cancelllation Details //////////////////////

//    $("#Room" + Service).append(div);

//    //$('#txtTotal_' + Service).change(function () {

//    //    TotalRoomFare += parseFloat($('#txtTotal_' + Service).val());
//    //    $("#txtTotalFare").val(TotalRoomFare);
//    //});

//    $('#datepicker_CancellationDate_' + Service + '_' + Service + '').datepicker({
//        changeMonth: true,
//        changeYear: true,
//        dateFormat: "dd-mm-yy",
//        // onSelect: insertDepartureDate,
//        //dateFormat: "yy-m-d",
//        // minDate: "dateToday",
//        // maxDate: "+3M +10D"
//    });
//    PasngrDetails(2, Service);
//    GetMealPlan(Service);
//}


function Child(value, RoomNo) {
    var div = '';

    if ($("#Select_Children_" + RoomNo + " option:selected").val() == 0) {
        $('#Select_AgeChildFirst_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Span_AgeChildFirst_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Select_AgeChildSecond_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Span_AgeChildSecond_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Select_AgeChildThird_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Span_AgeChildThird_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Select_AgeChildFourth_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Span_AgeChildFourth_' + RoomNo + '').prop('selectedIndex', 0);

        $("#Select_AgeChildFirst_" + RoomNo + "").css("display", "none");
        $('#Span_AgeChildFirst_' + RoomNo + '').css("display", "none");
        $('#Select_AgeChildSecond_' + RoomNo + '').css("display", "none");
        $('#Span_AgeChildSecond_' + RoomNo + '').css("display", "none");
        $('#Select_AgeChildThird_' + RoomNo + '').css("display", "none");
        $('#Span_AgeChildThird_' + RoomNo + '').css("display", "none");
        $('#Select_AgeChildFourth_' + RoomNo + '').css("display", "none");
        $('#Span_AgeChildFourth_' + RoomNo + '').css("display", "none");


    }
    if ($("#Select_Children_" + RoomNo + " option:selected").val() == 1) {
        $('#Select_AgeChildSecond_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Span_AgeChildSecond_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Select_AgeChildThird_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Span_AgeChildThird_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Select_AgeChildFourth_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Span_AgeChildFourth_' + RoomNo + '').prop('selectedIndex', 0);

        $('#div_AgeChildFirst_' + RoomNo + '').css("display", "");
        $('#Select_AgeChildFirst_' + RoomNo + '').css("display", "");
        $('#Span_AgeChildFirst_' + RoomNo + '').css("display", "");
        $('#Select_AgeChildSecond_' + RoomNo + '').css("display", "none");
        $('#Span_AgeChildSecond_' + RoomNo + '').css("display", "none");
        $('#Select_AgeChildThird_' + RoomNo + '').css("display", "none");
        $('#Span_AgeChildThird_' + RoomNo + '').css("display", "none");
        $('#Select_AgeChildFourth_' + RoomNo + '').css("display", "none");
        $('#Span_AgeChildFourth_' + RoomNo + '').css("display", "none");

    }
    if ($("#Select_Children_" + RoomNo + " option:selected").val() == 2) {
        $('#Select_AgeChildThird_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Span_AgeChildThird_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Select_AgeChildFourth_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Span_AgeChildFourth_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Select_AgeChildFirst_' + RoomNo + '').css("display", "");
        $('#Span_AgeChildFirst_' + RoomNo + '').css("display", "");
        $('#Select_AgeChildSecond_' + RoomNo + '').css("display", "");
        $('#Span_AgeChildSecond_' + RoomNo + '').css("display", "");
        $('#Select_AgeChildThird_' + RoomNo + '').css("display", "none");
        $('#Span_AgeChildThird_' + RoomNo + '').css("display", "none");
        $('#Select_AgeChildFourth_' + RoomNo + '').css("display", "none");
        $('#Span_AgeChildFourth_' + RoomNo + '').css("display", "none");
        $('#div_AgeChildSecond_' + RoomNo + '').css("display", "");

    }
    if ($("#Select_Children_" + RoomNo + " option:selected").val() == 3) {
        $('#Select_AgeChildFourth_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Span_AgeChildFourth_' + RoomNo + '').prop('selectedIndex', 0);
        $('#Select_AgeChildFirst_' + RoomNo + '').css("display", "");
        $('#Span_AgeChildFirst_' + RoomNo + '').css("display", "");
        $('#Select_AgeChildSecond_' + RoomNo + '').css("display", "");
        $('#Span_AgeChildSecond_' + RoomNo + '').css("display", "");
        $('#Select_AgeChildThird_' + RoomNo + '').css("display", "");
        $('#Span_AgeChildThird_' + RoomNo + '').css("display", "");
        $('#Select_AgeChildFourth_' + RoomNo + '').css("display", "none");
        $('#Span_AgeChildFourth_' + RoomNo + '').css("display", "none");
        $('#div_AgeChildThird_' + RoomNo + '').css("display", "");
    }
    if ($("#Select_Children_" + RoomNo + " option:selected").val() == 4) {
        $('#Select_AgeChildFirst_' + RoomNo + '').css("display", "");
        $('#Span_AgeChildFirst_' + RoomNo + '').css("display", "");
        $('#Select_AgeChildSecond_' + RoomNo + '').css("display", "");
        $('#Span_AgeChildSecond_' + RoomNo + '').css("display", "");
        $('#Select_AgeChildThird_' + RoomNo + '').css("display", "");
        $('#Span_AgeChildThird_' + RoomNo + '').css("display", "");
        $('#Select_AgeChildFourth_' + RoomNo + '').css("display", "");
        $('#Span_AgeChildFourth_' + RoomNo + '').css("display", "");
        $('#div_AgeChildFourth_' + RoomNo + '').css("display", "");

    }

    $("#PassengerChildDetails" + RoomNo).empty();

    for (var i = 0; i < value; i++) {
        var div = '';

        div += '<div class="columns" name="tblChildTravelers_' + RoomNo + '_' + i + '">'

        div += '<div class="three-columns twelve-columns-mobile"> '
        div += '    <br /> '
        div += '    <select class="select full-width" id="SelCGender_' + RoomNo + '_' + i + '" > '
        div += '        <option value="Master" selected="selected">Master</option> '
        //div += '        <option value="-" >Mrs</option> '
        div += '    </select> '
        div += ' </div>'

        div += '    <div class="three-columns twelve-columns-mobile">'
        div += '    <br /> '
        div += '        <input type="text" placeholder="First Name" id="txtChildName_' + RoomNo + '_' + i + '"  class="input  ui-autocomplete-input full-width ChildName" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_ChildName_' + RoomNo + '_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '    </div>'



        div += '    <div class="three-columns twelve-columns-mobile">'
        div += '        <br>'
        div += '        <input type="text" placeholder="Last Name"  id="txtChildAge_' + RoomNo + '_' + i + '" class="input  ui-autocomplete-input full-width" style="display:none" readonly="readonly"/>'
        div += '    </div>'



        div += '       </div>'

        $("#PassengerChildDetails" + RoomNo).append(div);
        //   $("#PassengerChildDetails" + RoomNo).show();
        //  $('#txtChildAge_' + Service + '_' + i + '').val(Age);

    }

}

var Count = 1;
function AddCancellation(RoomNo) {
    var div = '';


    div += '<div class="columns" id="CancellationDiv' + Count + '">'

    div += '    <div class="two-columns six-columns-mobile">'
    //div += '        <span class="text-left ">Cancellation Note</span>'
    div += '        <br />'
    div += '        <input type="text" placeholder="Note" id="txtCancellationNote_' + RoomNo + '_' + Count + '" class="input  ui-autocomplete-input full-width CancellationNote_' + RoomNo + '" />'
    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CancellationNote_' + RoomNo + '_' + Count + '  ">'
    div += '            <b>* This field is required</b></label>'
    div += '    </div>'

    div += '    <div class="two-columns six-columns-mobile">'
    //div += '        <span class="text-left ">Cancellation Date</span>'
    div += '        <br />'
    div += '       <input class="input full-width mySelectCalendar CancellationDate_' + RoomNo + '" type="text" id="datepicker_CancellationDate_' + RoomNo + '_' + Count + '" readonly="readonly" style="cursor: pointer" />'
    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CancellationDate_' + RoomNo + '_' + Count + '  ">'
    div += '            <b>* This field is required</b></label>'
    div += '    </div>'

    div += '    <div class="two-columns six-columns-mobile">'
    //div += '        <span class="text-left ">Cancellation Date</span>'
    div += '        <br />'
    div += '       <input class="input full-width CancellationTime_' + i + '" type="time" id="txtCancellationTime_' + RoomNo + '_' + Count + '"  style="cursor: pointer" />'
    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CancellationTime_' + RoomNo + '_' + Count + '  ">'
    div += '            <b>* This field is required</b></label>'
    div += '    </div>'

    div += '    <div class="two-columns six-columns-mobile">'
    //div += '        <span class="text-left ">Cancellation Amount</span>'
    div += '        <br />'
    div += '        <input type="text" placeholder="0.0" id="txtCancellationAmount_' + RoomNo + '_' + Count + '" class="input  ui-autocomplete-input full-width CancellationAmount_' + RoomNo + '" />'
    div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_CancellationAmount_' + RoomNo + '_' + Count + '  ">'
    div += '            <b>* This field is required</b></label>'
    div += '    </div>'

    div += '    <div class="two-columns six-columns-mobile">'
    div += '        <br />'
    div += '        <input type="button" placeholder="Type" value="+" id="btnAddCancel_' + RoomNo + '_' + Count + '" onclick="AddCancellation(' + RoomNo + ')" class="button anthracite-gradient" style="cursor:pointer" title="Add" />'
    div += '    </div>'

    div += '    <div class="two-columns six-columns-mobile">'
    div += '        <br />'
    div += '        <input type="button" placeholder="Type" value="-" id="btnRemoveCancel_' + RoomNo + '_' + Count + '" onclick="RemoveCancellation(\'' + RoomNo + '\',\'' + Count + '\')" class="button anthracite-gradient" style="cursor:pointer" title="Remove" />'
    div += '    </div>'

    div += '    </div>'

    if (Count == 1) {
        $('#btnAddCancel_' + RoomNo + '_' + (Count - 1) + '').hide();
        $('#btnRemoveCancel_' + RoomNo + '_' + (Count) + '').show();
    }
    if (Count != 1) {
        $('#btnAddCancel_0_0').hide();
        $('#btnAddCancel_' + RoomNo + '_' + parseInt(Count - 1) + '').hide();
        $('#btnRemoveCancel_' + RoomNo + '_' + (Count) + '').show();
    }


    $('#CancellationDate' + RoomNo).show();
    $('#CancellationDate' + RoomNo).append(div);

    // DatePicker(Service, Count);
    $('#datepicker_CancellationDate_' + RoomNo + '_' + Count + '').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        // onSelect: insertDepartureDate,
        //dateFormat: "yy-m-d",
        // minDate: "dateToday",s
        // maxDate: "+3M +10D"
    });
    Count++;
}


function RemoveCancellation(RoomNo, Cnt) {
    $('#CancellationDiv' + Cnt + '').remove();
    $('#btnAddCancel_' + RoomNo + '_' + (Cnt - 1) + '').show();
    $('#btnRemoveCancel_' + RoomNo + '_' + (Cnt - 1) + '').show();
    $('#btnAddCancel_0_0s').show();

    Count - 1;
}

function PasngrDetails(value, RoomNo) {
    $("#PassengerDetails" + RoomNo).empty();

    for (var i = 0; i < value; i++) {
        var div = '';

        // div += '<br>'
        div += '<div class="columns" name="tblTravelers_' + RoomNo + '_' + i + '">'


        div += '<div class="three-columns twelve-columns-mobile" > '
        div += '    <br /> '
        div += '    <select class="select full-width" id="SelAGender_' + RoomNo + '_' + i + '" > '
        div += '        <option value="Mr" selected="selected">Mr</option> '
        div += '        <option value="Mrs" >Mrs</option> '
        div += '        <option value="Miss" >Miss</option> '
        div += '    </select> '
        div += '</div>'

        div += '<div class="three-columns twelve-columns-mobile" >'
        div += '    <br /> '
        div += '        <input type="text" placeholder="First Name" id="txtAdultFirstName_' + RoomNo + '_' + i + '"   class="input  ui-autocomplete-input full-width AdultFirstName" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_AdultFirstName_' + RoomNo + '_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '</div>'

        div += '<div class="three-columns twelve-columns-mobile" >'
        div += '    <br /> '
        div += '        <input type="text" placeholder="Last Name" id="txtAdultLastName_' + RoomNo + '_' + i + '"   class="input  ui-autocomplete-input full-width AdultLastName" />'
        div += '        <label style="color: red; margin-top: 3px; display: none" id="lbl_AdultLastName_' + RoomNo + '_' + i + '">'
        div += '            <b>* This field is required</b></label>'
        div += '</div>'

        div += '</div>'

        $("#PassengerDetails" + RoomNo).append(div);
        // $("#PassengerDetails" + RoomNo).show();

    }

}

var bValid = true;
var AgentName; var selSupplier;
var HotelName; var City; var BookingDate; var BookingTime;
var BookingRef; var RoomNo; var RoomType; var selMealPlan; var BookingCharges; var SupplierCharges; var MarkupAmount;
var Adults;
var RoomType; var selMealPlan; var BookingCharges; var SupplierCharges; var MarkupAmount; var Total = 0; var SupplierDiscount;
var GST; var CGST; var IGST; var GSTTotal;
var PurchaseCharges; var Commission; var PurchaseTotal = 0; var Purchasecurrency;
var TotalGSTAmount = 0;
var TotalRoomFare = 0;

function SaveBooking() {
    bValid = Validation()
    if (bValid == true) {

        var arrRoom = new Array();
        var arrHotel = new Array();
        var TotalFare = 0;
        var arrGuest = new Array();

        var AgentCode = $("#hdnAgentCode").val();
        var HotelCode = $("#hdnHCode").val();
        var FirstName = $("#txtAdultFirstName_0_0").val();
        var LastName = $("#txtAdultLastName_0_0").val();
        var Gender = $("#SelAGender_0_0").val();
        var FirstCancelDate = $("#datepicker_CancellationDate_0_0").val();
        var FirstCancelTime = $("#txtancellationTime_0_0").val();

        var HotelCheckin = $("#txt_Checkin").val();
        var HotelCheckout = $("#txt_Checkout").val();
        var TotalNights = $("#sel_Nights option:selected").val();
        var Nationality = $("#Select_Nationality").val();


        RoomNo = $("#Select_Rooms option:selected").val();

        var Guest = new Array();

        var AdultCustomer = new Array();
        var ChildCustomer = new Array();

        for (var i = 0; i < RoomNo; i++) {
            var CancellationNote = ""; var CancellationAmount = ""; var CancellationDate = ""; var CancellationTime = "";

            RoomType = $("#txtRoomType_" + i).val();
            selMealPlan = $("#selMealPlan_" + i).val();
            BookingRef = $("#txtBookingRef_" + i).val();

            //  SupplierCharges = $("#txtSupplierCharges_" + i).val();
            SupplierCharges = $("#txtSupplierCharges_" + i).val();
            MarkupAmount = $("#txtMarkupAmount_" + i).val
            SupplierDiscount = $("#txtDiscount_" + i).val();
            Total = $("#txtTotal_" + i).val();

            PurchaseCharges = $("#txtPurchaseCharges_" + i).val();
            Purchasecurrency = $("#selPurchasecurrency_" + i).val();
            Commission = $("#txtCommission_" + i).val();
            PurchaseTotal = $("#txtPurchaseTotal_" + i).val();

            var string = "";

            var percentIGST = $("#txtIGSTPercent_" + i).val();
            var percentSGST = $("#txtSGSTPercent_" + i).val();
            var percentCGST = $("#txtCGSTPercent_" + i).val();
            var percentUGST = $("#txtUGSTPercent_" + i).val();


            if (percentIGST != "") {
                var Name = $("#lblIGST_" + i).text();
                var Percent = $("#txtIGSTPercent_" + i).val();
                var Amount = $("#txtIGSTAmount_" + i).val();

                string += Name + " " + "(" + Percent + "%" + ")" + "_" + Amount + "^";
            }

            if (percentSGST != "") {
                var Name = $("#lblSGST_" + i).text();
                var Percent = $("#txtSGSTPercent_" + i).val();
                var Amount = $("#txtSGSTAmount_" + i).val();

                string += Name + " " + "(" + Percent + "%" + ")" + "_" + Amount + "^";
            }

            if (percentCGST != "") {
                var Name = $("#lblCGST_" + i).text();
                var Percent = $("#txtCGSTPercent_" + i).val();
                var Amount = $("#txtCGSTAmount_" + i).val();

                string += Name + " " + "(" + Percent + "%" + ")" + "_" + Amount + "^";
            }

            if (percentUGST != "") {
                var Name = $("#lblUGST_" + i).text();
                var Percent = $("#txtUGSTPercent_" + i).val();
                var Amount = $("#txtUGSTAmount_" + i).val();
                string += Name + " " + "(" + Percent + "%" + ")" + "_" + Amount + "^";
            }

            var Purtotal = parseFloat(PurchaseTotal);

            var ttotal = parseFloat(Total);

            TotalFare += parseFloat(ttotal);

            // var j = 1;
            var k = 1;
            var AdultCount = $("#Select_Adults_" + i).val();
            debugger;
            for (var j = 0; j < AdultCount; j++) {
                var IsLeading;
                $('#PassengerDetails' + i + ' div').each(function () {

                    if ($(this).attr('name') == 'tblTravelers_' + (i) + "_" + j) {
                        //var j = 1;
                        var title = $(this).find("#SelAGender_" + (i) + "_" + j).val(); //$(this).find("select[name='Select_Gender'] option:selected").val();
                        if (j == 0 && i == 0) {
                            IsLeading = "True";
                        }
                        else {
                            IsLeading = "False";
                        }
                        var objAdultCustomer = {
                            ReservationID: BookingRef,
                            RoomCode: RoomType,
                            RoomNumber: "Room " + (i + 1),
                            Age: 0,//$(this).find("input[name='txtAge']").val(),
                            PassengerType: "AD",
                            Name: title + ' ' + $(this).find("#txtAdultFirstName_" + (i) + "_" + j).val(),
                            LastName: $(this).find("#txtAdultLastName_" + (i) + "_" + j).val(),
                            IsLeading: IsLeading,
                        }

                        debugger;
                        //  AdultCustomer.push(objAdultCustomer);
                        Guest.push(objAdultCustomer);
                        j = j + 1;
                    }
                });
                //var objGuest =
                //   {
                //       RoomNumber: (i + 1),
                //       sAdultCustomer: AdultCustomer
                //   };
                //Guest.push(objGuest);
            }


            var ChildCount = $("#Select_Children_" + i).val();
            var ChilAges = "";

            for (var l = 0; l < ChildCount; l++) {
                if (l == 0) {
                    $('#txtChildAge_' + i + '_' + l + '').val($('#Select_AgeChildFirst_' + i).val());
                }
                else if (l == 1) {
                    $('#txtChildAge_' + i + '_' + (l + 1) + '').val($('#Select_AgeChildSecond_' + i).val());
                }
                else if (l == 2) {
                    $('#txtChildAge_' + i + '_' + (l + 2) + '').val($('#Select_AgeChildThird_' + i).val());
                }
                else if (l == 3) {
                    $('#txtChildAge_' + i + '_' + (l + 3) + '').val($('#Select_AgeChildFourth_' + i).val());
                }


                $('#PassengerChildDetails' + i + ' div').each(function () {

                    if ($(this).attr('name') == 'tblChildTravelers_' + (i) + "_" + l) {
                        //if ($(this).attr('name') == 'ChildName_' + (i) + "_" + l)
                        //{
                        var title = $(this).find("#SelCGender_" + (i) + "_" + l).val();
                        var objChildCustomer = {
                            ReservationID: BookingRef,
                            RoomCode: RoomType,
                            RoomNumber: "Room " + (i + 1),
                            Age: ($(this).find("#txtChildAge_" + (i) + "_" + l).val() != "") ? $(this).find("#txtChildAge_" + (i) + "_" + l).val() : 0,
                            PassengerType: "CH",
                            Name: title + ' ' + $(this).find("#txtChildName_" + (i) + "_" + l).val(),
                            LastName: "",
                            IsLeading: "False",
                        }
                        ChilAges += $("#txtChildAge_" + (i) + "_" + l).val() + ",",
                            // ChildCustomer.push(objChildCustomer);
                            Guest.push(objChildCustomer);
                        j = j + 1;
                        // }
                    }
                });
                //var objGuest =
                //      {
                //          RoomNumber: (i + 1),
                //          sChildCustomer: ChildCustomer
                //      };               
                //Guest.push(objGuest);
            }
            var SupplierNoChargeDate = ""; var SupplierNoChargeTime = "";

            for (var k = 0; k < $('.CancellationNote_' + i + '').length; k++) {
                var DDL = $('.CancellationNote_' + i + '')[k].value;
                CancellationNote += DDL + "|";
            }

            for (var k = 0; k < $('.CancellationDate_' + i + '').length; k++) {
                var DDL = $('.CancellationDate_' + i + '')[k].value;
                CancellationDate += DDL + "|";
            }

            //SupplierNoChargeDate = $('.CancellationDate_' + i + '')[0].value;
            //SupplierNoChargeTime = $('.CancellationTime_' + i + '')[0].value;

            for (var k = 0; k < $('.CancellationTime_' + i + '').length; k++) {
                var DDL = $('.CancellationTime_' + i + '')[k].value;
                CancellationTime += DDL + "|";
            }

            for (var k = 0; k < $('.CancellationAmount_' + i + '').length; k++) {
                var DDL = $('.CancellationAmount_' + i + '')[k].value;
                CancellationAmount += DDL + "|";
            }

            var cnt = CancellationDate.split('|');
            var dnt = CancellationTime.split('|');
            var CancelDateTime = "";
            for (var m = 0; m < cnt.length - 1; m++) {
                CancelDateTime += cnt[m] + " " + dnt[m] + "|";
            }


            var sRoom = {
                ReservationID: $("#txt_Refrence").val(),
                RoomCode: "",
                RoomType: RoomType,
                RoomNumber: "Room " + (i + 1),
                BoardText: selMealPlan,
                TotalRooms: RoomNo,
                LeadingGuest: Gender + " " + FirstName + " " + LastName,
                Adults: AdultCount,
                Child: ChildCount,
                ChildAge: ChilAges,
                Remark: "",
                CutCancellationDate: CancelDateTime,
                SupplierNoChargeDate: CancelDateTime,
                //SupplierNoChargeDate + " " + SupplierNoChargeTime,
                CancellationAmount: CancellationAmount,
                CanServiceTax: 0.00,
                CanAmtWithTax: CancellationAmount,
                RoomAmount: ttotal,
                RoomServiceTax: 0.000,
                RoomAmtWithTax: ttotal,
                SupplierAmount: Purtotal,
                SupplierCurrency: Purchasecurrency,
                TaxDetails: string,

                //Guest: Guest,                 
                //BookingCharges: BookingCharges,
                //SupplierCharges: SupplierCharges,
                //MarkupAmount: MarkupAmount,
                //Total: Total,
                //GST: GST,
                //CGST: CGST,
                //IGST: IGST,
                //GSTTotal: GSTTotal,

            }

            arrRoom.push(sRoom);

        }



        var sHotel = {
            ReservationID: $("#txt_Refrence").val(),
            ReferenceCode: "",
            GTAId: "",
            Uid: AgentCode,
            HotelCode: HotelCode,
            HotelName: HotelName,
            City: City,
            RoomCode: "",
            CheckIn: HotelCheckin,
            CheckOut: HotelCheckout,
            NoOfDays: TotalNights,
            TotalRooms: RoomNo,
            TotalFare: TotalFare,
            RoomRate: 0.00,
            SupplierCurrency: Purchasecurrency,
            SalesTax: 0,
            Servicecharge: 0,
            LuxuryTax: 0,
            Discount: 0,
            NoOfAdults: AdultCount,
            Children: ChildCount,
            Infants: 0,
            ExtraBed: 0,
            CancelFlag: false,
            CancelDate: "",
            Remarks: "",
            ReservationDate: BookingDate + " " + BookingTime,
            ReservationTime: BookingTime,
            Status: "Vouchered",
            AgentMarkUp_Per: 0,
            BookingStatus: "",
            Source: selSupplier,
            ExchangeValue: 0.00,
            Updateid: AgentCode,
            Updatedate: BookingDate,
            HotelDetails: "",
            ChildAges: "",
            HotelBookingData: "",
            AgencyName: AgentName,
            terms: "",
            bookingname: Gender + " " + FirstName + " " + LastName,
            DeadLine: FirstCancelDate + " " + FirstCancelTime,
            holdbooking: 0,
            deadlineemail: false,
            sightseeing: 0,
            mealplan: "",
            mealplan_Amt: 0.00,
            AffilateCode: "",
            Type: "Offline",
            RefAgency: AgentName,
            AgentRef: "AG-" + AgentCode,
            VoucherID: "",
            InvoiceID: "",
            ComparedCurrency: "",
            ComparedFare: parseFloat(TotalFare),
            AgentContactNumber: "",
            AgentEmail: "",
            AgentRemark: "",
            LatitudeMGH: "",
            LongitudeMGH: "",
            OfferAmount: 0.00,
            OfferId: 0,
            HoldTime: ""
        }

        // arrHotel.push(sHotel);



        $.ajax({
            type: "POST",
            url: "OfflineBookingHandler.asmx/SaveOfflineBooking",
            data: JSON.stringify({ arrRoom: arrRoom, arrHotel: sHotel, Guest: Guest }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {

                    Success('Booking Save Sucessfully');
                    window.location.reload();

                }
                else {
                    Success('Something went wrong');
                }
            },
            error: function () {
            }
        });

    }

}

function Validation() {
    AgentName = $("#txt_Agents").val();
    if (AgentName == "") {
        Success("Please Select Agent")
        return bValid = false;
    }
    else {
        $("#lbl_AgentName").hide();
    }


    City = $("#txtCity").val();
    if (City == "") {
        Success("Please Select Destination")
        return bValid = false;
    }
    else {
        $("#lbl_txtCity").hide();
    }

    HotelName = $("#txt_HotelName").val();
    if (HotelName == "") {
        Success("Please Select Hotel")
        return bValid = false;
    }
    else {
        $("#lbl_HotelName").hide();
    }
    var HotelCheckin = $("#txt_Checkin").val();
    var HotelCheckout = $("#txt_Checkout").val();

    if (HotelCheckin == "") {
        Success("Please Select CheckIn Date")
        return bValid = false;
    }
    if (HotelCheckout == "") {
        Success("Please Select CheckOut Date")
        return bValid = false;
    }

    var Nationality = $("#Select_Nationality").val();
    if (Nationality == "") {
        Success("Please Select Nationality")
        return bValid = false;
    }

    BookingDate = $("#txt_BookingDate").val();
    if (BookingDate == "") {
        Success("Please Select Booking Date")
        return bValid = false;
    }
    else {
        $("#lbl_BookingDate").hide();
    }

    BookingTime = $("#txtBookingTime").val();
    if (BookingTime == "") {
        Success("Please Enter Time")
        return bValid = false;
    }
    else {
        $("#lbl_BookingDate").hide();
    }

    if ($("#txt_Refrence").val() == "") {
        Success("Please Enter Booking Reference No")
        return bValid = false;
    }

    selSupplier = $("#sel_Supplier option:selected").text();
    if (selSupplier == 0) {
        Success("Please Select Supplier")
        return bValid = false;
    }
    else {
        $("#lbl_selSupplier").hide();
    }

    ///////////// Room Validation ///////////

    RoomNo = $("#Select_Rooms option:selected").val();

    for (var i = 0; i < RoomNo; i++) {
        RoomType = $("#txtRoomType_" + i).val();
        if (RoomType == "") {
            $("#txtRoomType_" + i).focus();
            $("#lbl_RoomType_" + i).show();
            return bValid = false;
        }
        else {
            $("#lbl_RoomType_" + i).hide();
        }

        selMealPlan = $('#selMealPlan_' + i + ' Option:selected').val();
        if (selMealPlan == "-") {
            $("#selMealPlan_" + i).focus();
            $("#lbl_selMealPlan_" + i).show();
            return bValid = false;
        }
        else {
            $("#lbl_selMealPlan_" + i).hide();
        }

        BookingRef = $("#txtBookingRef_" + i).val();
        if (BookingRef == "") {
            $("#txtBookingRef_" + i).focus();
            $("#lbl_BookingRef_" + i).show();
            return bValid = false;
        }
        else {
            $("#lbl_BookingRef_" + i).hide();
        }


        Adults = $('#Select_Adults_' + i + ' option:selected').val();

        var bValue = true;
        var cValue = true;
        var dValue = true;
        var eValue = true;
        var fValue = true;
        var gValue = true;
        var hValue = true;

        $('.AdultFirstName').each(function () {
            if ($(this).val() == "")
                bValue = false;

        });

        $('.AdultLastName').each(function () {
            if ($(this).val() == "")
                cValue = false;
        });

        $('.ChildName').each(function () {
            if ($(this).val() == "")
                //$('#AlertMessage').text('Please enter childname');
                //$('#AlertModal').modal('show');
                dValue = false;

        });

        $('.CancellationNote').each(function () {
            if ($(this).val() == "")
                eValue = false;

        });

        $('.mySelectCalendar').each(function () {
            if ($(this).val() == "")
                fValue = false;

        });

        $('.CancellationTime').each(function () {
            if ($(this).val() == "")
                hValue = false;

        });

        $('.CancellationAmount').each(function () {
            if ($(this).val() == "")
                gValue = false;

        });


        if (bValue == false) {
            $('#AlertMessage').text('Please enter AdultFirstName');
            $('#AlertModal').modal('show');
            return bValid = false;
        }
        if (cValue == false) {
            $('#AlertMessage').text('Please enter AdultLastName');
            $('#AlertModal').modal('show');
            return bValid = false;
        }
        if (dValue == false) {
            $('#AlertMessage').text('Please enter childname');
            $('#AlertModal').modal('show');
            return bValid = false;
        }
        if (eValue == false) {
            $('#AlertMessage').text('Please enter Cancellation Note ');
            $('#AlertModal').modal('show');
            return bValid = false;
        }
        if (fValue == false) {
            $('#AlertMessage').text('Please select Cancellation Date ');
            $('#AlertModal').modal('show');
            return bValid = false;
        }
        if (gValue == false) {
            $('#AlertMessage').text('Please enter Cancellation Amount ');
            $('#AlertModal').modal('show');
            return bValid = false;
        }
        if (hValue == false) {
            $('#AlertMessage').text('Please enter Cancellation Time ');
            $('#AlertModal').modal('show');
            return bValid = false;
        }


        ///////////// End Supplier Validation ///////////

        SupplierCharges = $("#txtSupplierCharges_" + i).val();
        if (SupplierCharges == "") {
            $("#txtSupplierCharges_" + i).focus();
            $("#lbl_SupplierCharges_" + i).show();
            return bValid = false;
        }
        else {
            $("#lbl_SupplierCharges_" + i).hide();
        }

        MarkupAmount = $("#txtMarkupAmount_" + i).val();
        if (MarkupAmount == "") {
            $("#txtMarkupAmount_" + i).focus();
            $("#lbl_MarkupAmount_" + i).show();
            return bValid = false;
        }
        else {
            $("#lbl_MarkupAmount_" + i).hide();
        }

        SupplierDiscount = $("#txtDiscount_" + i).val();
        if (SupplierDiscount == "") {
            $("#txtDiscount_" + i).focus();
            $("#lbl_Discount_" + i).show();
            return bValid = false;
        }
        else {
            $("#lbl_Discount_" + i).hide();
        }

        Total = $("#txtTotal_" + i).val();
        if (Total == "") {
            $("#txtTotal_" + i).focus();
            $("#lbl_Total_" + i).show();
            return bValid = false;
        }
        else {
            $("#lbl_Total_" + i).hide();
        }

        ///////////// End Supplier Validation ///////////

        ////////// purchase validation /////////////////


        PurchaseCharges = $("#txtPurchaseCharges_" + i).val();
        if (PurchaseCharges == "") {
            $("#txtPurchaseCharges_" + i).focus();
            $("#lbl_PurchaseCharges_" + i).show();
            return bValid = false;
        }
        else {
            $("#lbl_PurchaseCharges_" + i).hide();
        }

        Purchasecurrency = $('#selPurchasecurrency_' + i + ' Option:selected').val();
        if (Purchasecurrency == "-") {
            $("#selPurchasecurrency_" + i).focus();
            $("#lbl_selPurchasecurrency_" + i).show();
            return bValid = false;
        }
        else {
            $("#lbl_selPurchasecurrency_" + i).hide();
        }

        Commission = $("#txtCommission_" + i).val();
        if (Commission == "") {
            $("#txtCommission_" + i).focus();
            $("#lbl_Commission_" + i).show();
            return bValid = false;
        }
        else {
            $("#lbl_Commission_" + i).hide();
        }

        PurchaseTotal = $("#txtPurchaseTotal_" + i).val();
        if (PurchaseTotal == "") {
            $("#txtPurchaseTotal_" + i).focus();
            $("#lbl_PurchaseTotal_" + i).show();
            return bValid = false;
        }
        else {
            $("#lbl_PurchaseTotal_" + i).hide();
        }

        ///////////// End purchase Validation ///////////

    }

    ///////////// End Room Validation ///////////

    return bValid = true;

}

function SetGSTPercent(Amount, RoomNo) {
    TotalGSTAmount = 0;

    var AgentName = $("#txt_Agents").val();
    if (AgentName == "") {
        $("#txt_Agents").focus();
        $("#lbl_AgentName").show();
        return bValid = false;
    }
    else {
        $("#lbl_AgentName").hide();
    }

    var AgentCode = $("#hdnAgentCode").val();


    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "OfflineBookingHandler.asmx/SetGSTPercent",
        data: "{'Amount':'" + Amount + "','AgentCode':'" + AgentCode + "'}",
        dataType: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            $("#label_" + RoomNo).show();
            for (var i = 0; i < result.length; i++) {
                var Type; var percent; var Amount = 0;

                if (result[i].Type == "IGST") {
                    // $("#IGST_" + RoomNo).show();
                    $("#IGST_" + RoomNo).css("display", "");
                    Type = result[i].Type;
                    percent = result[i].PerCentage;
                    Amount = result[i].Amount;
                    $("#lblIGST_" + RoomNo).text(Type);
                    $("#txtIGSTPercent_" + RoomNo).val(percent);
                    $("#txtIGSTAmount_" + RoomNo).val(Amount);
                    TotalGSTAmount += parseFloat(Amount);
                }
                if (result[i].Type == "SGST") {
                    //  $("#SGST_" + RoomNo).show();
                    $("#SGST_" + RoomNo).css("display", "");
                    Type = result[i].Type;
                    percent = result[i].PerCentage;
                    Amount = result[i].Amount;
                    $("#lblSGST_" + RoomNo).text(Type);
                    $("#txtSGSTPercent_" + RoomNo).val(percent);
                    $("#txtSGSTAmount_" + RoomNo).val(Amount);
                    TotalGSTAmount += parseFloat(Amount);
                }
                if (result[i].Type == "CGST") {
                    // $("#CGST_" + RoomNo).show();
                    $("#CGST_" + RoomNo).css("display", "");
                    Type = result[i].Type;
                    percent = result[i].PerCentage;
                    Amount = result[i].Amount;
                    $("#lblCGST_" + RoomNo).text(Type);
                    $("#txtCGSTPercent_" + RoomNo).val(percent);
                    $("#txtCGSTAmount_" + RoomNo).val(Amount);
                    TotalGSTAmount += parseFloat(Amount);
                }
                if (result[i].Type == "UGST") {
                    //$("#UGST_" + RoomNo).show();
                    $("#UGST_" + RoomNo).css("display", "");
                    Type = result[i].Type;
                    percent = result[i].PerCentage;
                    Amount = result[i].Amount;
                    $("#lblUGST_" + RoomNo).text(Type);
                    $("#txtUGSTPercent_" + RoomNo).val(percent);
                    $("#txtUGSTAmount_" + RoomNo).val(Amount);
                    TotalGSTAmount += parseFloat(Amount);
                }

            }

            CalTotalAmount(RoomNo);


        },
        error: function (result) {
            alert("No Match");
        }
    });
}

function CalTotalAmount(RoomNo) {
    SupplierCharges = $("#txtSupplierCharges_" + RoomNo).val();
    if (SupplierCharges == "") {
        SupplierCharges = 0;
    }

    MarkupAmount = $("#txtMarkupAmount_" + RoomNo).val();
    if (MarkupAmount == "") {
        MarkupAmount = 0;
    }

    SupplierDiscount = $("#txtDiscount_" + RoomNo).val();
    if (SupplierDiscount == "") {
        SupplierDiscount = 0;
    }

    Total = parseFloat(SupplierCharges) + parseFloat(MarkupAmount) + parseFloat(TotalGSTAmount) - parseFloat(SupplierDiscount);
    $("#txtTotal_" + RoomNo).val(Total);



}


function CalPurchaseTotalAmount(Service) {
    PurchaseCharges = $("#txtPurchaseCharges_" + Service).val();
    if (PurchaseCharges == "") {
        PurchaseCharges = 0;
    }

    Commission = $("#txtCommission_" + Service).val();
    if (Commission == "") {
        Commission = 0;
    }

    PurchaseTotal = parseFloat(PurchaseCharges) - parseFloat(Commission);
    $("#txtPurchaseTotal_" + Service).val(PurchaseTotal);
}