﻿$(document).ready(function () {
function UploadFile() {
    var fileUpload = $("#FileUpload1").val();
    if (fileUpload != '') {
        document.getElementById("btnUpload").click();
      
    }
}

});

function Upload() {
    var DropDownList1 = $("#DropDownList1").val();
    var DropDownList2 = $("#DropDownList2").val();
    var TextBox1 = $("#TextBox1").val();
    var HiddenField1 = $("#HiddenField1").val();
    var FileUpload1 = $("#FileUpload1").val();
    
    $.ajax({
        type: "POST",
        url: "../handler/VisaDetails.asmx/Upload",
        data: '{"DropDownList1":"' + DropDownList1 + ',DropDownList2":"' + DropDownList2 + ',TextBox1":"' + TextBox1 + ',HiddenField1":"' + HiddenField1 + ',FileUpload1":"' + FileUpload1 + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrVisa = result.tbl_Visa;
                services = arrVisa[0].IeService;
                if (arrVisa[0].ImageUploded != null)
                    UplodedPhoto = arrVisa[0].ImageUploded;
                if (arrVisa[0].FirstPageUpload != null)
                    UplodedFirstPage = arrVisa[0].FirstPageUpload;
                if (arrVisa[0].SecoundPageUpload != null)
                    UplodedSecPage = arrVisa[0].SecoundPageUpload;
                // alert(UplodedPhoto)
                type = services;
                CropedPassPort = arrVisa[0].CropedPassport;
                CropedPassFirst = arrVisa[0].CropedFirst;
                CropedPassLast = arrVisa[0].CropedSecond;
                CropedSupp1 = arrVisa[0].CropedSuppFirst;
                CropedSuppLast = arrVisa[0].CropedSuppLast;
                GetCurrentImages(hiddenName);
                if (services != "96 hours Visa transit Single Entery (Compulsory Confirm Ticket Copy)") {
                    $('#6').css("display", "none")
                    $('#Confirm').css("display", "none")
                    $('#Confirm1').css("display", "none")
                    $('#ConfirmTicket').css("display", "none")
                    $('#7').css("display", "none")
                }
                else {
                    $('#6').css("display", "");
                    $('#Confirm').css("display", "")
                    $('#Confirm1').css("display", "")
                    $('#ConfirmTicket').css("display", "")
                    $('#7').css("display", "")
                }
            }
            if (result.retCode == 0) {
                $('#SpnMessege').text("Update Details First");
                $('#ModelMessege').modal('show')
                // alert("Update Details First");
                window.location.href = "AddVisaDetails.aspx"
            }
        },
        error: function () {
            $('#SpnMessege').text("An error occured")
            $('#ModelMessege').modal('show')
            // alert("An error occured")
        }
    });
    //$('#selCity option:selected').val(tempcitycode);
}
