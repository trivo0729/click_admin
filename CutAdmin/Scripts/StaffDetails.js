﻿var HiddenId;
var arRoleList = new Array();
var arFormList = new Array();
var arFormListForRole = new Array();
var arrayToSubmit = new Array();

$(document).ready(function () {
    //GetStaffDetails();
    var columns = [
        {
            "mData": "ContactPerson",
            "mRender": function (data, type, row, meta) {
                var tRow = '';
                tRow = '<a style="cursor:pointer" class="with-tooltip" onclick="StaffDetailsModal(\'' + row.sid + '\');; return false" title="Click to view Staff Details">' + row.ContactPerson + '</a>';
                return tRow;
            },
        },
        {
            "mData": "uid",
            "mRender": function (data, type, row, meta) {
                var tRow = '';
                tRow = '<a style="cursor:pointer" class="with-tooltip" onclick = "PasswordModal(\'' + row.sid + '\',\'' + row.uid + '\',\'' + row.ContactPerson + '\',\'' + row.password + '\'); return false" title="Click to view Staff Details">' + row.uid + '</a>';
                return tRow;
            },
        },
         {
             "mData": "StaffUniqueCode"
         },

         {
             "mRender": function (data, type, row, meta) {
                 var sChecked = "";
                 if (row.LoginFlag == true)
                     sChecked = "checked";
                 $(".switch").click(function () {
                     $(this).find("input:checkbox").click();
                 });
                 return '<input type="checkbox"  class="switch tiny mid-margin-right" tabindex="0" onclick="Activate(\'' + row.sid + '\',\'' + row.LoginFlag + '\',\'' + row.ContactPerson + '\',\'' + row.Last_Name + '\',\'' + '' + '\')" ' + sChecked + '/>';
                // return '<span class="switch tiny mid-margin-right replacement ' + sChecked + '" tabindex="0" onclick="Activate(\'' + row.sid + '\',\'' + row.LoginFlag + '\',\'' + row.ContactPerson + '\',\'' + row.Last_Name + '\')"><span class="switch-on"><span>ON</span></span><span class="switch-off"><span>OFF</span></span><span class="switch-button"></span>' + data + '</span>';
             }, "bSortable": false, "mData": "Iswitch",
         },

        {
            "mData": "LoginFlag", "mRender": function (data, type, row, meta) {
                var tRow = '';
                tRow += '<span class="button-group">'
                tRow += '<a href="#" class="button" title="Staff Roles" onclick="GetFormList(\'' + row.sid + '\')"><span class="icon-user"></span></a>'
                tRow += '<a href="AddStaff.aspx?sid=' + row.sid + '&sName=' + row.ContactPerson + '&sLastName=' + row.Last_Name + '&sDesignation=' + row.Designation + '&sAddress=' + row.Address + '&sCity=' + row.Code + '&sCountry=' + row.Country + '&nPinCode=' + row.PinCode + '&sEmail=' + row.uid + '&nPhone=&nMobile=&bLoginFlag=&sGroup=&sDepartment=" class="button" title="Edit"><span class="icon-pencil"></span></a>'
                tRow += '<a href="#" class="button" title="trash" onclick="DeleteStaffID(\'' + row.sid + '\',\'' + row.ContactPerson + '\',\'' + row.Last_Name + '\')"><span class="icon-trash"></span></a>'
                tRow += '</span>'
                return tRow
            }, "bSortable": false
        },
    ];
    GetData("tbl_StaffDetails", [], 'handler/GenralHandler.asmx/GetStaffDetails', columns)
});
///***this method is not calling***////
function GetStaffDetails() {
    debugger;
    $("#tbl_StaffDetails").dataTable().fnClearTable();
    $("#tbl_StaffDetails").dataTable().fnDestroy();
    //$("#tbl_StaffDetails tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "handler/GenralHandler.asmx/GetStaffDetails",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var List_StaffDetails = result.Staff;
                var tRow = '';
                for (var i = 0; i < List_StaffDetails.length; i++) {
                    var Name;
                    var s = List_StaffDetails[i].ContactPerson.split('undefined');
                    if (s.length > 2)
                        Name = s[0] + " " + s[1]
                    else
                        Name = s
                    tRow += '<tr>';

                    List_StaffDetails[i].Validity = moment(List_StaffDetails[i].Validity).format("L LTS");

                    tRow += '<td align="center"><a style="cursor:pointer" data-toggle="modal" data-target="#StaffDetailModal" onclick="StaffDetailsModal(\'' + List_StaffDetails[i].AgencyName + '\',\'' + List_StaffDetails[i].Validity + '\',\'' + List_StaffDetails[i].ContactPerson + '\',\'' + List_StaffDetails[i].Last_Name + '\',\'' + List_StaffDetails[i].Designation + '\',\'' + List_StaffDetails[i].Address + '\',\'' + List_StaffDetails[i].Description + '\',\'' + List_StaffDetails[i].Countryname + '\',\'' + List_StaffDetails[i].PinCode + '\',\'' + List_StaffDetails[i].phone + '\',\'' + List_StaffDetails[i].Mobile + '\',\'' + List_StaffDetails[i].Fax + '\',\'' + List_StaffDetails[i].email + '\',\'' + List_StaffDetails[i].Website + '\',\'' + List_StaffDetails[i].PANNo + '\',\'' + List_StaffDetails[i].Department + '\'); return false" title="Click to view Staff Details">' + Name + '  ' + List_StaffDetails[i].Last_Name + '</a></td>';
                    tRow += '<td align="center"><a style="cursor:pointer" data-toggle="modal" data-target="#PasswordModal" onclick="PasswordModal(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].uid + '\',\'' + List_StaffDetails[i].ContactPerson + '\',\'' + List_StaffDetails[i].password + '\'); return false" title="Click to edit Password">' + List_StaffDetails[i].uid + '</a></td>';
                    //tRow += '<td>' + List_StaffDetails[i].Designation + '</td>';
                    //tRow += '<td>' + List_StaffDetails[i].Mobile + '</td>';
                    tRow += '<td align="center">' + List_StaffDetails[i].StaffUniqueCode + '</td>';
                    //tRow += '<td>' + List_StaffDetails[i].Description + '</td>';
                    //tRow += '<td>' + List_StaffDetails[i].Countryname + '</td>';
                    var Title = "";
                    var flag = List_StaffDetails[i].LoginFlag;
                    if (flag == true) {
                        flag = "True";
                        Title = "Deactivate";
                    }
                    else {
                        flag = "False";
                        Title = "Activate";
                    }
                    flag = flag.replace("True", "fa fa-eye").replace("False", "fa fa-eye-slash")


                    //if (List_StaffDetails[i].LoginFlag == true)
                    //    tRow += '<td style="width:16%"  align="center"><span class="button-group"><label for="chk_On' + i + '" class="button blue-active active"><input type="radio" name="button-radio' + i + '" id="chk_On' + i + '" value="On" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')">Yes</label><label for="chk_Off' + i + '" class="button red-active"><input type="radio" name="button-radio' + i + '" id="chk_Off' + i + '" value="Off" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')">No</label></span></td>';
                    //else
                    //    tRow += '<td style="width:16%"  align="center"><span class="button-group"><label for="chk_On' + i + '" class="button blue-active"><input type="radio" name="button-radio' + i + '" id="chk_On' + i + '" value="On" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')">Yes</label><label for="chk_Off' + i + '" class="button red-active active"><input type="radio" name="button-radio' + i + '" id="chk_Off' + i + '" value="Off" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')">No</label></span></td>';
                    //var sChecked = "";
                    //if (row.LoginFlag == "True")
                    //    sChecked = "checked";
                    //return '<span class="switch tiny mid-margin-right replacement ' + sChecked + '" tabindex="0" onclick="Activate(\'' + row.sid + '\',\'' + row.LoginFlag + '\',\'' + row.AgencyName + '\',\'' + '' + '\')"><span class="switch-on"><span>ON</span></span><span class="switch-off"><span>OFF</span></span><span class="switch-button"></span>' + sdata + '</span>';
                    var sChecked = "";
                    if (List_StaffDetails[i].LoginFlag == true)
                        sChecked = "checked";
                    tRow += '<span class="switch tiny mid-margin-right replacement ' + sChecked + '" tabindex="0" onclick="Activate(\'' + row.sid + '\',\'' + row.LoginFlag + '\',\'' + row.AgencyName + '\',\'' + '' + '\')"><span class="switch-on"><span>ON</span></span><span class="switch-off"><span>OFF</span></span><span class="switch-button">';
                    tRow += '<td class="align-center">'
                    tRow += '<span class="button-group">'
                    tRow += '			<a href="#" class="button" title="Staff Roles" onclick="GetFormList(\'' + List_StaffDetails[i].sid + '\')"><span class="icon-user"></span></a>'
                    tRow += '			<a href="AddStaff.aspx?sid=' + List_StaffDetails[i].sid + '&sName=' + List_StaffDetails[i].ContactPerson + '&sLastName=' + List_StaffDetails[i].Last_Name + '&sDesignation=' + List_StaffDetails[i].Designation + '&sAddress=' + List_StaffDetails[i].Address + '&sCity=' + List_StaffDetails[i].Code + '&sCountry=' + List_StaffDetails[i].Country + '&nPinCode=' + List_StaffDetails[i].PinCode + '&sEmail=' + List_StaffDetails[i].email + '&nPhone=' + List_StaffDetails[i].phone + '&nMobile=' + List_StaffDetails[i].Mobile + '&bLoginFlag=' + List_StaffDetails[i].LoginFlag + '&sGroup=' + List_StaffDetails[i].StaffCategory + '&sDepartment=' + List_StaffDetails[i].Department + '" class="button" title="Edit"><span class="icon-pencil"></span></a>'
                    //tRow += '            <a href="#" class="button" title="' + Title + '" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')"><span class="' + flag + '"></span></a>'
                    tRow += '			<a href="#" class="button" title="trash" onclick="DeleteStaffID(\'' + List_StaffDetails[i].uid + '\',\'' + List_StaffDetails[i].ContactPerson + '\',\'' + List_StaffDetails[i].Last_Name + '\')"><span class="icon-trash"></span></a>'
                    tRow += '		</span>'
                    tRow += '</td>'
                    //tRow += '<td align="center"><a style="cursor:pointer" title="Edit" href="AddStaff.aspx?sid=' + List_StaffDetails[i].sid + '&sName=' + List_StaffDetails[i].ContactPerson + '&sDesignation=' + List_StaffDetails[i].Designation + '&sAddress=' + List_StaffDetails[i].Address + '&sCity=' + List_StaffDetails[i].Code + '&sCountry=' + List_StaffDetails[i].Country + '&nPinCode=' + List_StaffDetails[i].PinCode + '&sEmail=' + List_StaffDetails[i].email + '&nPhone=' + List_StaffDetails[i].phone + '&nMobile=' + List_StaffDetails[i].Mobile + '&bLoginFlag=' + List_StaffDetails[i].LoginFlag + '&sGroup=' + List_StaffDetails[i].StaffCategory + '&sDepartment=' + List_StaffDetails[i].Department + '"><span class="icon-pencil"></span></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a style="cursor:pointer" href="#"><span class="' + flag + '" onclick="Activate(\'' + List_StaffDetails[i].sid + '\',\'' + List_StaffDetails[i].LoginFlag + '\',\'' + List_StaffDetails[i].ContactPerson + '\')" title="' + flag + '" aria-hidden="true"></span></a> &nbsp;&nbsp;|&nbsp;&nbsp;<a> <span class="icon-user" title="Staff Roles" aria-hidden="true" style="cursor:pointer" onclick="GetFormList(\'' + List_StaffDetails[i].sid + '\')"></span></a> |  <a style="cursor:pointer" href="#"><span class="icon-trash" title="Delete" aria-hidden="true" style="cursor:pointer" onclick="DeleteStaffID(\'' + List_StaffDetails[i].uid + '\',\'' + List_StaffDetails[i].ContactPerson + '\')"></span></a></td>';
                    tRow += '</tr>';
                }
                $("#tbl_StaffDetails tbody").html(tRow);
                $(".tiny").click(function () {
                    $(this).find("input:checkbox").click();
                })
                $("#tbl_StaffDetails").dataTable({
                    bSort: true,
                    sPaginationType: 'full_numbers',


                });
                $("#tbl_StaffDetails").removeAttr("style");
                //$("#tbl_StaffDetails_length select").addClass("select anthracite-gradient glossy")
            }
            else {
                AlertDanger(result.ex);
                $("#tbl_StaffDetails").dataTable({
                    bSort: false, sPaginationType: 'full_numbers',
                });
            }
        },
        error: function () {
        }
    });
}

function Activate(sid, flag, name, Last_Name) {
    var status = "Activate";
    if (flag == "true") {
        status = "Deactivate";
    }
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to ' + status + '<span class=\"orange\">  ' + name + '  ' + Last_Name + '?</p>',
   // $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to ' + status + "<br/><span class=\"orange\"> " + name + '  ' + Last_Name + '</span>?</p>',
        function () {
            $.ajax({
                url: "HotelAdmin/Handler/GenralHandler.asmx/ActivateStaffLogin",
                type: "post",
                data: '{"sid":"' + sid + '","status":"' + flag + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Staff status has been changed successfully!")
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000)

                    } else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                }
            });
        }, function () {
            $('#modals').remove();
        });
}
function GetFormList(sid) {
    HiddenId = sid;
    $("#tblStaffForms").empty();
    $.ajax({
        type: "POST",
        url: "HotelAdmin/Handler/RoleManagementHandler.asmx/GetFormList?sid=" + sid,
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arFormList = result.Arr;
                var arrAuthForm = result.arrAuthForm;
                var ListForms = new Array();
                for (var i = 0; i < arFormList.length; i++) {
                    var arrForm = arrGetForms(arFormList[i], i);
                    for (var f = 0; f < arrForm.length; f++) {
                        ListForms.push(arrForm[f]);
                    }
                }
                if (arFormList.length > 0) {
                    $.modal({
                        content: '<div class=""><div id="tree"></div></div>' +
                     '<button style="float:right" type="submit" class="button anthracite-gradient" onclick="SubmitFormsForRole()">Assign Roles</button>',
                        title: 'Assign Roles',
                        width: 600,
                        scrolling: true,
                        actions: {
                            'Close': {
                                color: 'red',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttons: {
                            'Close': {
                                classes: 'anthracite-gradient displayNone',
                                click: function (win) { win.closeModal(); }
                            }
                        },
                        buttonsLowPadding: true
                    });
                }
                ListForms = CheckRoles(ListForms, arrAuthForm);
                var tree = simTree({
                    el: '#tree',
                    data: ListForms,
                    check: true,
                    linkParent: true,
                    onClick: function (item) {
                    },
                    onChange: function (item) {
                        debugger;
                        arrayToSubmit = new Array();
                        for (var i = 0; i < item.length; i++) {
                            if (item[i].id.indexOf("_") === -1) {
                                arrayToSubmit.push(item[i].id);
                            }

                        }
                    }
                });
            }
        },
        error: function () {
            Success("An error occured while geting form list");
        }
    });
}

function arrGetForms(Form, i) {
    var arrlist = new Array();
    try {
        var frm = new Array();
        frm = new Array();
        frm = {
            id: '',
            pid: '',
            name: Form.Name,
            checked: false,
        }
        if (Form.ChildItem.length != 0)
            frm.id = i + '_' + Form.ChildItem.length;
        else
            frm.id = Form.ID.toString();
        arrlist.push(frm);
        for (var j = 0; j < Form.ChildItem.length; j++) {
            var arrSubForms = arrGetForms(Form.ChildItem[j], frm.id + "_" + j)
            for (var f = 0; f < arrSubForms.length; f++) {
                if (arrSubForms[f].pid == "")
                    arrSubForms[f].pid = i + '_' + Form.ChildItem.length;
                arrlist.push(arrSubForms[f]);
            }
        }
    } catch (e) {

    }
    return arrlist;
}



function CheckRoles(ListForms, arFormListForRole) {
    debugger;
    if (arFormListForRole.length > 0) {
        for (j = 0; j < ListForms.length; j++) {
            for (k = 0; k < arFormListForRole.length; k++) {
                if (ListForms[j].id == arFormListForRole[k].ID) {
                    ListForms[j].checked = true;
                }
            }
        }
    }
    return ListForms;
}

function SubmitFormsForRole() {
    var arrayJson = JSON.stringify(arrayToSubmit);
    if (HiddenId != 'null') {
        $.ajax({
            type: "POST",
            url: "HotelAdmin/Handler/RoleManagementHandler.asmx/SetFormsForAdminStaff?sid=" + HiddenId,
            data: '{"StaffUid":"' + HiddenId + '",arr:' + arrayJson + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    Success("Role authorities has been changed successfully!");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000)
                }
                else if (result.retCode == 0) {
                    Success("Something went wrong!");
                }
            },
            error: function () {
                Success("Error occured while submitting checked forms.");
            }
        });
    }
    else {
        Success('Please select a Role!');
        $("#selRoles").focus()
    }
}

function DeleteStaffID(uid, staffname, Last_Name) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete<br/> <span class=\"orange\">' + staffname + '  ' + Last_Name + '</span>?</span>?</p>',
        function () {
            $.ajax({
                url: "HotelAdmin/Handler/GenralHandler.asmx/Delete",
                type: "post",
                data: '{"uid":"' + uid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Staff has been deleted successfully!");
                        window.location.reload();
                    } else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}

function ExportStaffDetailsToExcel() {
    window.location.href = "HotelAdmin/Handler/ExportToExcelHandler.ashx?datatable=StaffDetails";
}


function PasswordModal(sid, uid, StaffName, password) {
    HiddenId = sid;
    GetPassword(sid, uid, StaffName, password);

};

function GetPassword(sid, uid, StaffName, password) {
    $("#txt_Password").val('');
    $.ajax({
        type: "POST",
        url: "HotelAdmin/Handler/GenralHandler.asmx/GetPassword",
        data: '{"password":"' + password + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            debugger;
            if (result.retCode == 1) {
                pass = result.password;
                $.modal({
                    content: '<div class="modal-body">' +
            '<div class="scrollingDiv">' +
            '<div class="columns">' +
            '<div class="three-columns bold">User ID:</div>' +
            '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="Agency-value" value="' + uid + '" class="input-unstyled full-width" type="text">' + '</div></div></div> ' +
            '<div class="columns">' +
            '<div class="three-columns bold">Staff Name</div>' +
            '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_AgentId" value="' + StaffName + '" class="input-unstyled full-width" type="text"></div></div></div> ' +
            '<div class="columns">' +
            '<div class="three-columns bold">Password</div>' +
            '<div class="nine-columns"><div class="input full-width"><input name="prompt-value" id="txt_Password" value="' + pass + '" class="input-unstyled full-width" type="text"></div></div> ' +
            '</div>' +
            '<div class="columns">' +
            '<div class="three-columns bold">&nbsp;</div>' +
            //'<div class="nine-columns bold"><button type="button" class="button anthracite-gradient" onclick="openEmail()">Email</button>' +
            '<div class="nine-columns bold"><button type="button" class="button anthracite-gradient" onclick="ChangePassword()">Change&nbsp;Password</button></div>' +
            '</div></div></div>',

                    title: 'Edit Password',
                    width: 500,
                    scrolling: false,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'huge anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                });
            }
        },
        error: function () {
            Success("An error occured while loading password.")
        }
    });
}

function ChangePassword() {
    if ($('#txt_Password').val() != $('#hddn_Password').val()) {
        if ($('#txt_Password').val() != "") {
            $('#lbl_ErrPassword').css("display", "none");
            $.modal.confirm('Are you sure you want to change password?', function () {
                $.ajax({
                    type: "POST",
                    url: "HotelAdmin/Handler/GenralHandler.asmx/StaffChangePassword",
                    data: '{"sid":"' + HiddenId + '","password":"' + $('#txt_Password').val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (result.Session != 1) {
                            Success("Session Expired");
                        }
                        if (result.retCode == 1) {
                            Success("Password changed successfully");
                            setTimeout(function () {
                                window.location.reload();
                            }, 500)
                        }
                        if (result.retCode == 0) {
                            Success("Something went wrong while processing your request! Please try again.");
                        }
                    },
                    error: function () {
                    }
                });
            }, function () {
                $('#modals').remove();
            });
        }
        else if ($('#txt_Password').val() == "") {
            $('#lbl_ErrPassword').css("display", "");
        }
    }
    else {
        $.modal.Success('No Change found in Password!', {
            buttons: {
                'Cancel': {
                    classes: 'huge anthracite-gradient displayNone',
                    click: function (win) { win.closeModal(); }
                }
            }
        });
    }

}

function StaffDetailsModal(sid) {
    $.ajax({
        type: "POST",
        url: "HotelAdmin/Handler/UserHanler.asmx/GetStaffDetails",
        data: '{"StaffID":"' + sid + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            console.log(result);
            if (result.retCode == 1) {
                var arrStaff = result.arrStaff;
                arrStaff.Updatedate = moment(arrStaff.Updatedate).format("L");
                $("#hiddenID").val(sid)

                $.modal({
                    content: '<div class="respTable">' +
                        '<table class="table responsive-table evenodd" id="sorting-advanced">' +
                        '<tr><td class="even">Name:</td><td>' + arrStaff.ContactPerson + '</td><td class="even">Email:</td><td>' + arrStaff.uid + '</td></tr>' +
                        '<tr><td class="even">Mobile:</td><td>' + arrStaff.Mobile + '</td><td class="even">Designation:</td><td>' + arrStaff.Designation + '</td></tr>' +
                        '<tr><td class="even">Department:</td><td>' + arrStaff.Department + '</td><td class="even">Registration Date:</td><td>' + arrStaff.Updatedate + '</td></tr></tr>' +
                        '</tr>' +
                        '</table>' +
                        '</div>',
                    title: 'Staff Profile',
                    width: 800,
                    scrolling: true,
                    actions: {
                        'Close': {
                            color: 'red',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttons: {
                        'Close': {
                            classes: 'huge anthracite-gradient displayNone',
                            click: function (win) { win.closeModal(); }
                        }
                    },
                    buttonsLowPadding: true
                });
            }
        },
    });
}

