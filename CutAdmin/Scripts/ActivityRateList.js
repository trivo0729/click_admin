﻿

var id;
var name;
var locationn;
var city;
var country;

$(function () {
    if (location.href.indexOf('?') != -1) {

        id = GetQueryStringParams('id');
        TeriffID = GetQueryStringParams('TeriffID');
        name = GetQueryStringParams('name').replace(/%20/g, ' ');
        locationn = GetQueryStringParams('location');
        city = GetQueryStringParams('pCities').replace(/%20/g, ' ');
        country = GetQueryStringParams('country').replace(/%20/g, ' ');
        //if (id != undefined)
        //{
        //    GetActivityRatelist(id);
        //}
        //alert(id)
        //GetActivity(id);
    }
    $("#ActName").text(name + ' ' + 'Rates');
    GetActivityRatelist()
    //GetSupplier()
    GetActSupplier()
});


function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

var arrActRateList = "";
var ArrChildPolicy = "";

function GetActivityRatelist() {
    $("#tbl_Rates").dataTable().fnClearTable();
    $("#tbl_Rates").dataTable().fnDestroy();
    // $("#tbl_Actlist tbody tr").remove();
    var data = {

        id: id


    }
    $.ajax({
        url: "../Handler/ActivityHandller.asmx/GetActivityRatelistt",
        type: "post",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                //$("#tbl_Rates").empty();
                arrActRateList = result.ArrRateList;
                ArrChildPolicy = result.ArrChildPolicy;
                if (arrActRateList != 0) {
                    //$('#lblStatus').css("display", "none");
                    $("#btn_AddRates").hide();
                    $("#btn_UpdateRates").show();
                    var tRow = '';
                    var DateFrom = '';
                    var DateTo = '';
                    var ChildAge = '';
                    var SChildAge = '';
                    if (ArrChildPolicy != 0) {

                        //ChildAge = "Child (" + ArrChildPolicy[0].Child_Age_From + '-' + ArrChildPolicy[0].Child_Age_Upto + ") Rates";
                        //SChildAge = "Child (" + ArrChildPolicy[0].Child_Age_Upto + '-' + ArrChildPolicy[0].Small_Child_Age_Upto + ") Rates";
                        //$("#Childage").text(ChildAge);
                        //$("#SChildage").text(SChildAge);
                        ChildAge = "(" + ArrChildPolicy[0].Child_Age_From + '-' + ArrChildPolicy[0].Child_Age_Upto + ") Rates";
                        SChildAge = "(" + ArrChildPolicy[0].Child_Age_Upto + '-' + ArrChildPolicy[0].Small_Child_Age_Upto + ") Rates";
                        $("#Childage").text(Child1);
                        $("#SChildage").text(Child2);
                    }
                    else {
                        $("#Childage").text("-");
                        $("#SChildage").text("-");
                    }


                    var count = 0;
                    for (var i = 0; i < arrActRateList.length; i++) {
                        if (arrActRateList[i].Timing_Type != "Special") {
                            DateFrom = arrActRateList[i].Valid_from.split('^');
                            DateTo = arrActRateList[i].Valid_to.split('^');

                            for (var j = 0; j < DateFrom.length - 1; j++) {
                                tRow += '        <tr>';
                                tRow += '    <td style="width:3%" align="center">' + (count + 1) + '</td>';
                                tRow += '    <td style="width:11%" align="center">' + DateFrom[j] + '</td>';
                                tRow += '    <td style="width:11%" align="center">' + DateTo[j] + '</td>';

                                tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Act_Type + '</td>';

                                if (arrActRateList[i].SlotName != null) {

                                    tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].SlotName + '</td>';
                                }
                                else {
                                    tRow += '    <td style="width:10%" align="center">-</td>';
                                }
                                tRow += '    <td style="width:12%" align="center"> ' + arrActRateList[i].Adult + '</td>';
                                if (ArrChildPolicy != 0) {
                                    if (arrActRateList[i].Child_11_5 == "") {
                                        tRow += '    <td style="width:10%" align="center">-</td>';
                                    }
                                    else {

                                        tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Child_11_5 + '</td>';
                                    }
                                    if (arrActRateList[i].Child_4_2 == "") {
                                        tRow += '    <td style="width:10%" align="center">-</td>';
                                    }
                                    else {

                                        tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Child_4_2 + '</td>';
                                    }
                                }
                                else {
                                    tRow += '    <td style="width:10%" align="center">-</td>';
                                    tRow += '    <td style="width:10%" align="center">-</td>';
                                }
                                tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Currency + '</td>';
                                tRow += '    <td style="width:13%" align="center">' + arrActRateList[i].Supplier + '</td>';

                                tRow += '</tr>';

                                GetCurrency(count, i);
                                count++;
                            }

                        }

                        else {

                            SDateFrom = arrActRateList[i].Spl_Valid_from.split('^');
                            SDateTo = arrActRateList[i].Spl_Valid_to.split('^');

                            for (var j = 0; j < SDateFrom.length - 1; j++) {
                                tRow += '        <tr>';
                                tRow += '    <td style="width:3%" align="center">' + (count + 1) + '</td>';
                                tRow += '    <td style="width:11%" align="center">' + SDateFrom[j] + '</td>';
                                tRow += '    <td style="width:11%" align="center">' + SDateTo[j] + '</td>';

                                tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Act_Type + '</td>';

                                if (arrActRateList[i].SlotName != null) {

                                    tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].SlotName + '</td>';
                                }
                                else {
                                    tRow += '    <td style="width:10%" align="center">-</td>';
                                }

                                tRow += '    <td style="width:12%" align="center"> ' + arrActRateList[i].Adult + '</td>';

                                if (ArrChildPolicy != 0) {
                                    if (arrActRateList[i].Child_11_5 == "") {
                                        tRow += '    <td style="width:10%" align="center">-</td>';
                                    }
                                    else {

                                        tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Child_11_5 + '</td>';
                                    }
                                    if (arrActRateList[i].Child_4_2 == "") {
                                        tRow += '    <td style="width:10%" align="center">-</td>';
                                    }
                                    else {
                                        tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Child_4_2 + '</td>';

                                    }
                                }
                                else {
                                    tRow += '    <td style="width:10%" align="center">-</td>';
                                    tRow += '    <td style="width:10%" align="center">-</td>';
                                }

                                tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Currency + '</td>';

                                tRow += '    <td style="width:15%" align="center">' + arrActRateList[i].Supplier + '</td>';

                                tRow += '</tr>';

                                GetCurrency(count, i);

                                count++;
                            }

                        }

                    }
                    $("#tbl_Rates tbody").empty();
                    $("#tbl_Rates tbody").append(tRow);

                    $('[data-toggle="tooltip"]').tooltip()
                    $("#tbl_Rates").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });

                }

            }

            else {
                $("#tbl_Rates").hide();
                $("#btn_UpdateRates").hide();

            }

            GetDatePicker(count);
        }
    })
}
function GetDatePicker(count) {
    for (var i = 0; i < count; i++) {
        $("#dtp_From" + i + "").datepicker({
            // minDate: $("#datepicker_To").text(),
            dateFormat: "dd-mm-yy",
            autoclose: true,
        });

        $("#dtp_To" + i + "").datepicker({
            // minDate: $("#datepicker_To").text(),
            dateFormat: "dd-mm-yy",
            autoclose: true,
        });

        $("#dtp_SFrom" + i + "").datepicker({
            // minDate: $("#datepicker_To").text(),
            dateFormat: "dd-mm-yy",
            autoclose: true,
        });

        $("#dtp_STo" + i + "").datepicker({
            // minDate: $("#datepicker_To").text(),
            dateFormat: "dd-mm-yy",
            autoclose: true,
        });
    }

}


//var adult = "";
//var child = "";
//var schild = "";
function UpdateActRates(id, val, Tid, TimingType) {
    var adult = $("#AdultRate" + val + "").val();
    var child = $("#ChildRate" + val + "").val();
    var schild = $("#SChildRate" + val + "").val();
    var Currency = "";

    if (TimingType == "Normal") {
        Currency = $("#ddlCurrency" + val + "").val();
    }
    else {
        Currency = $("#ddlCurrencyspl" + val + "").val();
    }

    var FromDate = "";
    for (var i = 0; i < $(".dtp_From" + id + "").length; i++) {
        if ($(".dtp_From" + id + "")[i].value != "")
            FromDate += $(".dtp_From" + id + "")[i].value + '^';
    }

    var FromTo = "";
    for (var i = 0; i < $(".dtp_To" + id + "").length; i++) {
        if ($(".dtp_To" + id + "")[i].value != "")
            FromTo += $(".dtp_To" + id + "")[i].value + '^';
    }


    var SFromDate = "";
    for (var i = 0; i < $(".dtp_SFrom" + id + "").length; i++) {
        if ($(".dtp_SFrom" + id + "")[i].value != "")
            SFromDate += $(".dtp_SFrom" + id + "")[i].value + '^';
    }

    var SFromTo = "";
    for (var i = 0; i < $(".dtp_STo" + id + "").length; i++) {
        if ($(".dtp_STo" + id + "")[i].value != "")
            SFromTo += $(".dtp_STo" + id + "")[i].value + '^';
    }

    var bValid = true;
    if (child == undefined) {
        child = "";
    }
    if (schild == undefined) {
        schild = "";
    }

    if (Currency == "") {
        alert("Select Currency");
        bValid = false;
    }

    if (bValid = true) {
        if (confirm("Are you sure you want to Update Rates") == true) {
            var Data = {

                id: id,
                Tid: Tid,
                TimingType: TimingType,
                adult: adult,
                child: child,
                schild: schild,
                Currency: Currency,
                FromDate: FromDate,
                FromTo: FromTo,
                SFromDate: SFromDate,
                SFromTo: SFromTo
            }

            $.ajax({
                type: "POST",
                url: "../Handler/ActivityHandller.asmx/UpdateRates",
                data: JSON.stringify(Data),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        alert("Rate Updated Successfully");
                        location.reload();
                        //GetLocation();
                    }
                }
            })

        }
    }

}


function DeleteRate(id) {
    if (confirm("Are you sure you want to delete Rate") == true) {
        $.ajax({
            url: "../Handler/ActivityHandller.asmx/DeleteRate",
            type: "post",
            data: '{"id":"' + id + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                if (result.retCode == 1) {
                    alert("Rate has been deleted successfully.");
                    location.reload();
                    //window.location.href = "AddLocation.aspx";
                } else if (result.retCode == 0) {
                    alert("Something went wrong while processing your request! Please try again.");
                }
            },
            error: function () {
                alert('Error occured while processing your request! Please try again.');
            }
        });
    }
}


function SearchTeriff() {
    debugger;
    $("#tbl_Rates").dataTable().fnClearTable();
    $("#tbl_Rates").dataTable().fnDestroy();
    var Status = "";
    var From = $('#datepicker_From').val();
    var To = $('#datepicker_To').val();
    var RatesFor = $('#ddl_Rates option:selected').val();
    var Supplier = $('#txt_Supplier').val();
    var SupplierName = $('#Select_Supplier').text();
    var Slot = $('#txt_Slot').val();
    var data = {

        id: id,
        From: From,
        To: To,
        RatesFor: RatesFor,
        Slot: Slot,
        Status: Status,
        Supplier: Supplier,
        SupplierName: SupplierName,

    }

    $.ajax({
        type: "POST",
        url: "../Handler/ActivityHandller.asmx/SearchTeriff",
        //data: '{"Agent":"' + sid + '","From":"' + From + '","To":"' + To + '","Type":"' + Type + '","Passenger":"' + Passenger + '","ResDt":"' + BookingDt + '","RefNo":"' + RefNo + '","SuppRefNo":"' + SuppRefNo + '","Supplier":"' + Supplier + '","HotelName":"' + HotelName + '","Destination":"' + Destination + '"}',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                //$("#tbl_Rates").empty();
                arrActRateList = result.Arr;
                ArrChildPolicy = result.ArrChildPolicy;
                if (arrActRateList != 0) {
                    var tRow = '';
                    var DateFrom = '';
                    var DateTo = '';
                    var ChildAge = '';
                    var SChildAge = '';
                    if (ArrChildPolicy != 0) {

                        ChildAge = "Child (" + ArrChildPolicy[0].Child_Age_From + '-' + ArrChildPolicy[0].Child_Age_Upto + ") Rates";
                        SChildAge = "Child (" + ArrChildPolicy[0].Child_Age_Upto + '-' + ArrChildPolicy[0].Small_Child_Age_Upto + ") Rates";
                        $("#Childage").text(ChildAge);
                        $("#SChildage").text(SChildAge);
                    }
                    else {
                        $("#Childage").text("-");
                        $("#SChildage").text("-");
                    }

                    var count = 0;
                    for (var i = 0; i < arrActRateList.length; i++) {
                        if (arrActRateList[i].Timing_Type != "Special") {
                            DateFrom = arrActRateList[i].Valid_from.split('^');
                            DateTo = arrActRateList[i].Valid_to.split('^');

                            for (var j = 0; j < DateFrom.length - 1; j++) {
                                tRow += '        <tr>';
                                tRow += '    <td style="width:3%" align="center">' + (count + 1) + '</td>';
                                tRow += '    <td style="width:11%" align="center">' + DateFrom[j] + '</td>';
                                tRow += '    <td style="width:11%" align="center">' + DateTo[j] + '</td>';
                                tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Act_Type + '</td>';

                                if (arrActRateList[i].SlotName != null) {
                                    tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].SlotName + '</td>';
                                }
                                else {
                                    tRow += '    <td style="width:10%" align="center">-</td>';
                                }

                                tRow += '    <td style="width:12%" align="center">' + arrActRateList[i].Adult + '</td>';
                                if (ArrChildPolicy != 0) {
                                    if (arrActRateList[i].Child_11_5 == "") {
                                        tRow += '    <td style="width:10%" align="center">-</td>';
                                    }
                                    else {
                                        tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Child_11_5 + '</td>';
                                    }
                                    if (arrActRateList[i].Child_4_2 == "") {
                                        tRow += '    <td style="width:10%" align="center">-</td>';
                                    }
                                    else {
                                        tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Child_4_2 + '</td>';
                                    }
                                }
                                else {
                                    tRow += '    <td style="width:10%" align="center">-</td>';
                                    tRow += '    <td style="width:10%" align="center">-</td>';
                                }
                                tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Currency + '</td>';
                                tRow += '    <td style="width:15%" align="center">' + arrActRateList[i].Supplier + '</td>';
                                tRow += '</tr>';

                                GetCurrency(count, i);
                                count++;
                            }

                        }

                        else {

                            SDateFrom = arrActRateList[i].Spl_Valid_from.split('^');
                            SDateTo = arrActRateList[i].Spl_Valid_to.split('^');

                            for (var j = 0; j < SDateFrom.length - 1; j++) {
                                tRow += '        <tr>';
                                tRow += '    <td style="width:3%" align="center">' + (count + 1) + '</td>';
                                tRow += '    <td style="width:11%" align="center">' + SDateFrom[j] + '</td>';
                                tRow += '    <td style="width:11%" align="center">' + SDateTo[j] + '</td>';
                                tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Act_Type + '</td>';

                                if (arrActRateList[i].SlotName != null) {
                                    $("#Slots").show();
                                    tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].SlotName + '</td>';
                                }
                                else {
                                    tRow += '    <td style="width:10%" align="center">-</td>';
                                }

                                tRow += '    <td style="width:12%" align="center">' + arrActRateList[i].Adult + '</td>';

                                if (ArrChildPolicy != 0) {
                                    if (arrActRateList[i].Child_11_5 == "") {
                                        tRow += '    <td style="width:10%" align="center">-</td>';
                                    }
                                    else {
                                        tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Child_11_5 + '</td>';
                                    }
                                    if (arrActRateList[i].Child_4_2 == "") {
                                        tRow += '    <td style="width:10%" align="center">-</td>';
                                    }
                                    else {
                                        tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Child_4_2 + '</td>';
                                    }
                                }
                                else {
                                    tRow += '    <td style="width:10%" align="center">-</td>';
                                    tRow += '    <td style="width:10%" align="center">-</td>';
                                }
                                tRow += '    <td style="width:10%" align="center">' + arrActRateList[i].Currency + '</td>';
                                tRow += '    <td style="width:15%" align="center">' + arrActRateList[i].Supplier + '</td>';
                                tRow += '</tr>';

                                GetCurrency(count, i);
                                //GetDatePicker(count);
                                count++;
                            }

                        }

                    }
                    $("#tbl_Rates tbody").append(tRow);
                    $("#tbl_Rates").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                    $('[data-toggle="tooltip"]').tooltip()
                    $("#tbl_Rates").dataTable({
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                }

            }
            else {
                $("#tbl_Rates").hide();
                alert("Result Not Found")
            }
        },
        error: function () {
        }
    });


}


function GetSupplier() { 
    var Div = '';

    $.ajax({
        url: "../Handler/ActivityHandller.asmx/GetActSupplier",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var Arr = result.dtresult;
                for (var i = 0; i < Arr.length; i++) {
                    Div += '<option value=' + Arr[i].sid + '>' + Arr[i].Supplier + '</option>';
                }
                $("#Select_Supplier").append(Div);

            }
        },
    });
}


var Cities; var pCities = "";
function AddTariffRates() {
    //window.location.href = "AddActivityTariff.aspx?id=" + id + "&pCities=" + city + "&name=" + name + "&country=" + country + "&locationn=" + locationn;
    window.location.href = "sightseeingrates.aspx?id=" + id + "&pCities=" + city + "&name=" + name + "&country=" + country + "&locationn=" + locationn;
}


function GetCurrency(count, no) {

    $.ajax({
        url: "GenralHandler.asmx/GetCurrency",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                var arr_Currency = result.CurrencyList;

                if (arr_Currency.length > 0) {
                    $("#ddlCurrency" + count + " ").empty();
                    $("#ddlCurrencyspl" + count + " ").empty();



                    var ddlRequest = "";
                    ddlRequest = '<option selected="selected" value="">Select Currency</option>';
                    for (i = 0; i < arr_Currency.length; i++) {

                        ddlRequest += '<option value="' + arr_Currency[i].CurrencyCode + '">' + arr_Currency[i].CurrencyCode + '</option>';

                    }
                    $("#ddlCurrency" + count + "").append(ddlRequest);
                    $("#ddlCurrency" + count + "").val(arrActRateList[no].Currency);

                    $("#ddlCurrencyspl" + count + "").append(ddlRequest);
                    $("#ddlCurrencyspl" + count + "").val(arrActRateList[no].Currency);
                }
            }
            if (result.retCode == 0) {
                $("#ddlCurrency").empty();
            }

        }
    })
}


function UpdateRates() {
    location.href = "sightseeingrates.aspx?id=" + id + "&pCities=" + city + "&name=" + name + "&country=" + country + "&locationn=" + locationn;
}