﻿var arrVisa;
var oldStatus;
var hidenId;
var type;
var Amount;
var Start = 0; Last = 0;
//$(document).ready(function () {
//    GetVisa();
//});
//function GetVisa() {
//    debugger;
//    $("Norecord").css("display", "none");
//    $("#tbl_VisaDetails").dataTable().fnClearTable();
//    $("#tbl_VisaDetails").dataTable().fnDestroy();
//    $("#tbl_VisaDetails").dataTable({
//        bSort: false,
//        sPaginationType: 'full_numbers',
//        bprocessing: true,
//        bServerSide: true,
//        info: true,
//        stateSave: true,
//        aLengthMenu: [[10, 20, 50, 100], [10, 20, 50, 100]],
//        fnServerData: function (sSource, aoData, fnCallback) {
//            debugger
//            try {
//                $("#tbl_VisaDetails tbody tr").remove();
//                var RequestParams = "?iDisplayLength= " + document.getElementsByName("tbl_VisaDetails_length")[0].value + "&iDisplayStart=" + Start + "&search[value]=" + $("#tbl_VisaDetails_filter label   input").val();
//                $.ajax({
//                    type: "POST",
//                    url: "../Handler/VisaDetails.asmx/Visa" + RequestParams,
//                    data: {},
//                    contentType: "application/json; charset=utf-8",
//                    datatype: "json",
//                    success: function (response) {
//                        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//                        if (result.retCode == 1) {
//                            arrVisa = result.tbl_Visa;
//                            HtmlBuilder(arrVisa)
//                        }
//                        else {
//                            $("Norecord").css("display", "");
//                            //$("#tbl_VisaDetails").dataTable({
//                            //    bSort: false, sPaginationType: 'full_numbers',
//                            //});
//                        }
//                    },

//                });
//            }
//            catch (erx) {
//                alert(erx.message)
//            }

//        }
//    });

//}

function GetVisa() {
    var columns = [
        {
            "mData": "sid",

        },

        {
            "mData": "AgencyName",
        },
        {
            "mData": "FirstName",
        },
        {
            "mData": "PassportNo",
        },
        {
            "mData": "VisaCountry",
        },
        {
            "mData": "IeService",
            "mRender": function (data, type, row, meta) {
                var Service = (row.IeService).split(' ');
                return '' + Service[0] + '-' + Service[1] + '';
            }

        },
        {
            "mData": "Status",
            "mRender": function (data, type, row, meta) {
                var tRow = '';
                var Status = row.Status;
                if (row.Status == null || row.Status == 'null')
                    Status = '';
                var Status = row.Status;
                if (Status == "Under Process-Posted")
                    Status = "Posted";
                else if (Status == "Processing-Applied")
                    Status == "Processing-Applied";
                else if (Status == "Cancelled-Cancelled Before Applying")
                    Status == "Cancelled-Cancelled Before Applying";

                if (row.ActiveStatus == "Active")
                    tRow += '<td style="max-width: 75px; vertical-align: middle; width :9%" class="center"><span class="status-vouchered"><a style="cursor:pointer ; color : white;" onclick="UpdateStatus(\'' + row.FirstName + " " + row.LastName + '\',\'' + row.Status + '\',\'' + row.sid + '\',\'' + row.History + '\',\'' + row.Vcode + '\',\'' + row.UserId + '\',\'' + row.EdnrdUserName + '\',\'' + row.Sponsor + '\',\'' + row.TReference + '\',\'' + row.TotalAmount + '\')">' + Status.replace("Under Process", "Not Posted").replace("Under Process-Applied", "Posted").replace("Approved", "Application Approved").replace("Rejected", "Application Rejected").replace("Entered in country", "Visa Holder entered in country").replace("left the country", "Visa Holder left the country").replace("Documents Required", "Not Posted").replace("Processing error", "Application processing error") + '</a></td>';
                else if (row.ActiveStatus == "In-Active")
                    tRow += '<td style="max-width:70px; vertical-align: middle; width :9%" class="center"><span class="status-cancelled"><a style="cursor:pointer ; color: white;" onclick="UpdateStatus(\'' + row.FirstName + " " + row.LastName + '\', \'' + 'Incomplete' + '\',\'' + row.sid + '\',\'' + row.History + '\',\'' + row.Vcode + '\',\'' + row.UserId + '\',\'' + row.EdnrdUserName + '\',\'' + row.Sponsor + '\',\'' + row.TReference + '\',\'' + row.TotalAmount + '\')">Incomplete</a></td>';
                return tRow;
            }

        },
        {
            "mData": "AppliedDate",

            "mRender": function (data, type, row, meta) {
                var tRow = '';
              
                AppliedDate = DateFormat(row.AppliedDate);
                tRow += ' ' + AppliedDate.split(' ')[0] + '';
                return tRow;
                //if (GenrationDate.indexOf("/") != -1) {
                //    Applieddt = GenrationDate.split('/');
                //    tRow += ''+ Applieddt[1] + '-' + Applieddt[0] + '-' + Applieddt[2]+'';
                //}
                //else if (GenrationDate.indexOf("-") != -1) {
                //    Applieddt = GenrationDate.split('/');
                //   tRow += ''+Applieddt[0] + '-' + Applieddt[1] + '-' + Applieddt[2]+'';

                //}

            }

        },
        {
            "mData": "Sponsor",
        },
        {
            "mData": "ActiveStatus",
            "CellType": "html",
            "mRender": function (data, type, row, meta) {
                var tRow = '';
                tRow += '<td>';
                tRow += '<a title = "Click for Voucher" class="voucher-btn"  onclick = "ViewVisaApplication(\'' + row.Vcode + '\',\'' + row.UserId + '\')" >View</a><br>';
                tRow += '<a title = "Click for Invoice" class="invoice-btn"  onclick = "VisaPrintInvoice(\'' + row.Vcode + '\',\'' + row.UserId + '\')" >Invoice</a><br>';
                tRow += '<a title = "Update Documents" class="modify-btn"  href="ImagesUpload.aspx?Image=' + row.Vcode + '" >Upload</a>';
                if (row.VStatus == false)
                    tRow += '<a title = "Update Documents" class="modify-btn"  href="ImagesUpload.aspx?Image=' + row.Vcode + '" >Upload</a>';
                else if (row.VStatus == true)
                    tRow += '<a title = "Update Documents" class="modify-btn"  href="ImagesUpload.aspx?Image=' + row.Vcode + '" >Upload</a>';
                tRow += '</td></tr>';
                return tRow;
            }

        },


    ];
    GetData("tbl_VisaDetails", [], '../Handler/VisaDetails.asmx/VisaDetail', columns);
    $("#tbl_VisaDetails").css("width", "100%");


}



//function SeeSnap(Ref) {
//    $("#tbl_Snap").empty()
//    $.ajax({
//        type: "POST",
//        url: "../Handler/VisaDetails.asmx/GetPostingSnap",
//        data: '{"RefrenceNo":"' + Ref + '"}',
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            debugger;
//            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//            if (result.retCode == 1) {
//                var SnapImages = result.SnapPath;
//                var trImages = '<tbody>';
//                for (i = 0; i < SnapImages.length; i = i + 2) {
//                    if (i < SnapImages.length) {
//                        trImages += '<tr>';
//                        trImages += '<td class="align-center"><center><label>' + ImageLabel(SnapImages[i]) + '</label></center><br><img src="../VisaImages/VisaSnap/' + SnapImages[i] + '" /> </td>';
//                        if ((i + 1) < SnapImages.length)
//                            trImages += '<td class="align-center"><center><label>' + ImageLabel(SnapImages[i + 1]) + '</label></center><br><img src="../VisaImages/VisaSnap/' + SnapImages[i + 1] + '" /></td>';
//                        //if ((i + 2) < SnapImages.length)
//                        //    trImages += '<td class="align-center"><img src="../VisaImages/VisaSnap/' + SnapImages[i + 2] + '"/></td>';
//                        trImages += '</tr>';
//                    }
//                }
//                trImages += '</tbody>';
//                $("#tbl_Snap").append(trImages);
//                $("#SnapModal").modal("show");
//            }
//            else {
//                Success('No Snap Found..')
//            }
//            //$("#lodergif").css("display", "none")
//        },
//        error: function (ex) {
//            $("#lodergif").css("display", "none")
//            Success('An error occured while Getting Snap')
//        }
//    });
//}
function ImageLabel(Name) {
    try {
        var lbl = Name.split('_');
        lbl = lbl[1].split('.')
        return lbl[0]
    }
    catch (ex) {
        return "";
    }

}
function Next() {
    Start = Last
    $("#tbl_VisaDetails").dataTable().fnClearTable();
    $("#tbl_VisaDetails").dataTable().fnDestroy();
    GetVisa()
}
function Getall() {
    $("#tbl_VisaDetails").dataTable().fnClearTable();
    $("#tbl_VisaDetails").dataTable().fnDestroy();
    GetVisa()
}
function LastRecord() {
    Start = parseInt(Last) - parseInt(Start)
    $("#tbl_VisaDetails").dataTable().fnClearTable();
    $("#tbl_VisaDetails").dataTable().fnDestroy();
    GetVisa()
}
function ViewVisaApplication(Vcode, Eno) {
    $.ajax({
        type: "POST",
        url: "../Handler/VisaHandler.asmx/ViewApplication",
        data: '{"RefNo":"' + Vcode + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                window.open('VisaApplicationView.aspx?RefNo=' + Vcode)
            }
            else {
                alert("error Ocuured")
            }

        },
    });


}
function GetPrintInvoice(RefNo, sid) {
    window.open('../VisaInvoice.html?RefNo=' + RefNo + '&Uid=' + sid, 'PrintInvoice', 'left=5000,top=5000,width=800,height=600');
    //window.location.href = "../VisaInvoice.html?RefNo='" + RefNo + '&Uid=' + sid;
}
function VisaPrintInvoice(RefNo, sid) {
    window.open('../VisaInvoice.html?RefNo=' + RefNo + '&Uid=' + sid, 'PrintInvoice', 'left=5000,top=5000,width=800,height=600');
    //window.location.href = "../VisaInvoice.html?RefNo='" + RefNo + '&Uid=' + sid;
}
function ViewVisa(Code) {
    $.ajax({
        type: "POST",
        url: "../Handler/VisaDetails.asmx/GetVisaDoc",
        data: '{"RefrenceNo":"' + Code + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger;
            $('#preview').empty();
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var VisaParh = result.Path;
                if (result.ext == ".pdf" || result.ext == ".PDF") {
                    tRow = '<iframe src="../VisaImages/' + VisaParh + '" width="80%" height="500px"  />';
                    $('#preview').append(tRow)
                    $("#VisaPreviewModal").modal("show")
                }
                else {
                    VisaParh = "../VisaImages/" + result.Path;
                    $('#preview').append('<a id="a_1" href="' + VisaParh + '" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Download Visa" download=""><i class="glyphicon glyphicon-save size18"></i></a><br><img src="' + VisaParh + '" />')
                    $("#VisaPreviewModal").modal("show")
                    $('[data-toggle="tooltip"]').tooltip()
                }

            }
            else {
                Success('Visa not  Found..')
            }
            //$("#lodergif").css("display", "none")
        },
        error: function (ex) {
            $("#lodergif").css("display", "none")
            Success('An error occured while Getting Visa')
        }
    });


}


//function AttachVisa(Vcode) {
//    $("#lblMessage").text('')
//    $("#TextBox1").val('')
//    $('#HiddenField1').val(Vcode);
//    $('#VisaAttacmentModal').modal("show");
//}
var CropedPassPort = "False", CropedPassFirst = "False", CropedPassLast = "False", CropedSupp1 = "False", CropedSuppLast = "False";
function GetDocuments(VisaCode, ServiceType, CropedPass, CropedFirst, CropedLast, CropedSupp1i, CropedSuppLast1) {
    //alert(ServiceType)
    hidenId = VisaCode
    type = ServiceType;
    CropedPassPort = CropedPass;
    CropedPassFirst = CropedFirst;
    CropedPassLast = CropedLast
    CropedSupp1 = CropedSupp1i;
    CropedSuppLast = CropedSuppLast1
    $("#tbl_CurrentImages1 tbody tr").remove();
    $("#tbl_CurrentImages2 tbody tr").remove();
    $('#Confirm').css("display", " ");
    $("#div_CurrentImages").css("display", "");
    $("#div_CurrentImages1").css("display", "");
    $('#Confirm').css("display", "");
    $('#Download6').css("display", "")
    var tRow = '';
    var tRow2 = '';
    var url;
    tRow += '<tr>';
    for (var i = 0; i < 3; i++) {
        url = '../Thumnail.aspx?fn=' + VisaCode + '_' + (i + 1) + '.jpg'
        tRow += '<td style="width:33.33%">'
        tRow += GenrateCroped((i + 1));
        tRow += '</td>';
    }
    tRow += '</tr>';
    tRow2 += '<tr>'
    for (var i = 3; i < 5; i++) {
        tRow2 += '<td style="width:33.33%">'
        tRow2 += GenrateCroped((i + 1));
        tRow2 += '</td>';
    }
    tRow2 += '<td style="width:33.33%"><img src="../VisaImages/icon.png" id="6" style="cursor:pointer" class="wrapper" width="25%" height="190px" style="box-shadow: 0 0 5px "  onclick="Modal96Hours(\'' + VisaCode + '\')" /></td>';
    $("#tbl_CurrentImages1 tbody").html(tRow);
    $("#tbl_CurrentImages2 tbody").html(tRow2);
    tRow2 += '</tr>';
    if (ServiceType == "96 hours Visa transit Single Entery (Compulsory Confirm Ticket Copy)") {
        $('#6').css("display", " ")
        $('#Confirm').css("display", " ");
        $('#Download6').css("display", " ")
    }
    else {
        $('#6').css("display", "none");
        $('#Confirm').css("display", "none");
        $('#Download6').css("display", "none")
    }
    $('#DocumentsModal').modal("show")

}
function ShowCroped(ID) {
    var url;

    if ($("#lblStatus" + ID).text() == "") {
        url = '../Thumnail.aspx?fn=' + hidenId + '_' + ID + '.jpg' + '&crop=' + 'crop';
        document.getElementById('img_' + ID).src = "";
        document.getElementById('img_' + ID).src = url;
        document.getElementById('a_' + ID).href = '../VisaImages/crop/' + hidenId + '_' + ID + '.jpg'
        $("#lblCroped" + ID).text("Show Main")
        $("#lblStatus" + ID).text("True")
    }
    else {
        url = '../Thumnail.aspx?fn=' + hidenId + '_' + ID + '.jpg';
        document.getElementById('img_' + ID).src = "";
        document.getElementById('img_' + ID).src = url;
        document.getElementById('a_' + ID).href = '../VisaImages/' + hidenId + '_' + ID + '.jpg'
        $("#lblCroped" + ID).text("Show Croped")
        $("#lblStatus" + ID).text("")
    }

}
function GenrateCroped(id) {
    debugger;
    var html = '';
    var url;
    url = '../Thumnail.aspx?fn=' + hidenId + '_' + id + '.jpg';
    if (id == 1) {
        if (CropedPassPort == "True") {
            html += '<span class="orange size12 right" style="cursor: pointer;" onclick="ShowCroped(\'' + id + '\')" id="lblCroped' + id + '">Show Croped</span><label style="display: none;" id="lblStatus' + id + '"></label><a  id="a_' + id + '"  href="../VisaImages/' + hidenId + "_" + id + '.jpg" download><img src="' + url + '" id="img_' + id + '" class="wrapper" width="25%" height="190px" style="box-shadow: 0 0 5px "  onerror="imgError(this);" /></a>'
            return html;
        }
        else {
            html += '<br/><a id="a_' + id + '"  href="../VisaImages/' + hidenId + "_" + id + '.jpg" download><img src="' + url + '" id="img_' + id + '" class="wrapper" width="25%" height="190px" style="box-shadow: 0 0 5px "  onerror="imgError(this);" /></a>';
            return html;
        }
    }

    else if (id == 2) {
        if (CropedPassFirst == "True") {
            html += '<span class="orange size12 right" style="cursor: pointer;" onclick="ShowCroped(\'' + id + '\')" id="lblCroped' + id + '">Show Croped</span><label style="display: none;" id="lblStatus' + id + '"></label><a id="a_' + id + '"  href="../VisaImages/' + hidenId + "_" + id + '.jpg" download><img src="' + url + '" id="img_' + id + '" class="wrapper" width="25%" height="190px" style="box-shadow: 0 0 5px "  onerror="imgError(this);" /></a>';
            return html;
        }
        else {
            html += '<br/><a id="a_' + id + '"  href="../VisaImages/' + hidenId + "_" + id + '.jpg" download><img src="' + url + '" id="img_' + id + '" class="wrapper" width="25%" height="190px" style="box-shadow: 0 0 5px "  onerror="imgError(this);" /></a>';
            return html;
        }
    }

    else if (id == 3) {
        if (CropedPassLast == "True") {
            html += '<span class="orange size12 right" style="cursor: pointer;" onclick="ShowCroped(\'' + id + '\')" id="lblCroped' + id + '">Show Croped</span><label style="display: none;" id="lblStatus' + id + '"></label><a  id="a_' + id + '" href="../VisaImages/' + hidenId + "_" + id + '.jpg" download><img src="' + url + '" id="img_' + id + '" class="wrapper" width="25%" height="190px" style="box-shadow: 0 0 5px "  onerror="imgError(this);" /></a>';
            return html;
        }

        else {
            html += '<br/><a id="a_' + id + '"  href="../VisaImages/' + hidenId + "_" + id + '.jpg" download><img src="' + url + '" id="img_' + id + '" class="wrapper" width="25%" height="190px" style="box-shadow: 0 0 5px "  onerror="imgError(this);" /></a>';
            return html;
        }
    }
    else if (id == 4) {
        if (CropedSupp1 == "True") {
            html += '<span class="orange size12 right" style="cursor: pointer;" onclick="ShowCroped(\'' + id + '\')" id="lblCroped' + id + '">Show Croped</span><label style="display: none;" id="lblStatus' + id + '"></label><a  id="a_' + id + '"  href="../VisaImages/' + hidenId + "_" + id + '.jpg" download><img src="' + url + '" id="img_' + id + '" class="wrapper" width="25%" height="190px" style="box-shadow: 0 0 5px "  onerror="imgError(this);" /></a>';
            return html;
        }
        else {
            html += '<br/><a id="a_' + id + '"  href="../VisaImages/' + hidenId + "_" + id + '.jpg" download><img src="' + url + '" id="img_' + id + '" class="wrapper" width="25%" height="190px" style="box-shadow: 0 0 5px "  onerror="imgError(this);" /></a>';
            return html;
        }

    }

    else if (id == 5) {
        if (CropedSuppLast == "True") {
            html += '<span class="orange size12 right" style="cursor: pointer;" onclick="ShowCroped(\'' + id + '\')" id="lblCroped' + id + '">Show Croped</span><label style="display: none;" id="lblStatus' + id + '"></label><a  id="a_' + id + '"  href="../VisaImages/' + hidenId + "_" + id + '.jpg" download><img src="' + url + '" id="img_' + id + '" class="wrapper" width="25%" height="190px" style="box-shadow: 0 0 5px "  onerror="imgError(this);" /></a>';
            return html;
        }

        else {
            html += '<br/><a id="a_' + id + '"  href="../VisaImages/' + hidenId + "_" + id + '.jpg" download><img src="' + url + '" id="img_' + id + '" class="wrapper" width="25%" height="190px" style="box-shadow: 0 0 5px "  onerror="imgError(this);" /></a>';
            return html;
        }
    }



}
function Modal96Hours(hiddenName) {
    $("#lblpreview").text("Show")

    $('#Ticket').empty();
    var url = "../VisaImages/" + hiddenName + "_6.pdf";
    var tRow;
    $.get(url, function () {
        //iframe.onload = function () { alert('PDF opened!'); };
        //iframe.src = url;
        tRow = '<iframe src="../VisaImages/' + hiddenName + '_6.pdf" width="80%" height="500px"  />';
        $('#Ticket').append(tRow)
        $("#Modal96Hours").modal("show")
    }).error(function () { alert('Plese Upload pdf or docx file First'); });
}
function imgError(image) {
    image.onerror = "";
    image.src = "../VisaImages/logo.png";
    return true;
}


function SearchbyVisa() {
    $("#tbl_VisaDetails").dataTable().fnClearTable();
    $("#tbl_VisaDetails").dataTable().fnDestroy();
    var RefNo = $("#txt_RefNo").val()
    var ApplicationName = $("#txt_AppName").val()
    var Doc = $("#txt_DocNo").val()
    var Status = $("#selStatus").val()
    var VisaType = $("#selService").val();
    if (VisaType == "-") {
        VisaType = "";
    }
    var Agency = $("#txt_AgentName").val();
    //if (Agency == "")
    //    Agency = 0;
    var data = {
        sid: RefNo,
        FirstName: ApplicationName,
        PassportNo: Doc,
        Status: Status,
        IeService: VisaType,
        AgencyName: Agency
    }
    var columns = [
      {
          "mData": "sid",

      },

      {
          "mData": "AgencyName",
      },
      {
          "mData": "FirstName",
      },
      {
          "mData": "PassportNo",
      },
      {
          "mData": "VisaCountry",
      },
      {
          "mData": "IeService",
          "mRender": function (data, type, row, meta) {
              var Service = (row.IeService).split(' ');
              return '' + Service[0] + '-' + Service[1] + '';
          }

      },
      {
          "mData": "Status",
          "mRender": function (data, type, row, meta) {
              var tRow = '';
              var Status = row.Status;
              if (row.Status == null || row.Status == 'null')
                  Status = '';
              var Status = row.Status;
              if (Status == "Under Process-Posted")
                  Status = "Posted";
              else if (Status == "Processing-Applied")
                  Status == "Processing-Applied";
              else if (Status == "Cancelled-Cancelled Before Applying")
                  Status == "Cancelled-Cancelled Before Applying";

              if (row.ActiveStatus == "Active")
                  tRow += '<td style="max-width: 75px; vertical-align: middle; width :9%" class="center"><span class="status-vouchered"><a style="cursor:pointer ; color : white;" onclick="UpdateStatus(\'' + row.FirstName + " " + row.LastName + '\',\'' + row.Status + '\',\'' + row.sid + '\',\'' + row.History + '\',\'' + row.Vcode + '\',\'' + row.UserId + '\',\'' + row.EdnrdUserName + '\',\'' + row.Sponsor + '\',\'' + row.TReference + '\',\'' + row.TotalAmount + '\')">' + Status.replace("Under Process", "Not Posted").replace("Under Process-Applied", "Posted").replace("Approved", "Application Approved").replace("Rejected", "Application Rejected").replace("Entered in country", "Visa Holder entered in country").replace("left the country", "Visa Holder left the country").replace("Documents Required", "Not Posted").replace("Processing error", "Application processing error") + '</a></td>';
              else if (row.ActiveStatus == "In-Active")
                  tRow += '<td style="max-width:70px; vertical-align: middle; width :9%" class="center"><span class="status-cancelled"><a style="cursor:pointer ; color: white;" onclick="UpdateStatus(\'' + row.FirstName + " " + row.LastName + '\', \'' + 'Incomplete' + '\',\'' + row.sid + '\',\'' + row.History + '\',\'' + row.Vcode + '\',\'' + row.UserId + '\',\'' + row.EdnrdUserName + '\',\'' + row.Sponsor + '\',\'' + row.TReference + '\',\'' + row.TotalAmount + '\')">Incomplete</a></td>';
              return tRow;
          }

      },
      {
          "mData": "AppliedDate",

          "mRender": function (data, type, row, meta) {
              var tRow = '';
              tRow += DateFormat(row.AppliedDate);
            
              return tRow;

          }

      },
      {
          "mData": "Sponsor",
      },
      {
          "mData": "ActiveStatus",
          "CellType": "html",
          "mRender": function (data, type, row, meta) {
              var tRow = '';
              tRow += '<td>';
              tRow += '<a title = "Click for Voucher" class="voucher-btn"  onclick = "ViewVisaApplication(\'' + row.Vcode + '\',\'' + row.UserId + '\')" >View</a><br>';
              tRow += '<a title = "Click for Invoice" class="invoice-btn"  onclick = "VisaPrintInvoice(\'' + row.Vcode + '\',\'' + row.UserId + '\')" >Invoice</a><br>';
              tRow += '<a title = "Update Documents" class="modify-btn"  href="ImagesUpload.aspx?Image=' + row.Vcode + '" >Upload</a>';
              if (row.VStatus == false)
                  tRow += '<a title = "Update Documents" class="modify-btn"  href="ImagesUpload.aspx?Image=' + row.Vcode + '" >Upload</a>';
              else if (row.VStatus == true)
                  tRow += '<a title = "Update Documents" class="modify-btn"  href="ImagesUpload.aspx?Image=' + row.Vcode + '" >Upload</a>';
              tRow += '</td></tr>';
              return tRow;
          }

      },


    ];
    GetData("tbl_VisaDetails", data, '../Handler/VisaDetails.asmx/SearchByList', columns);
    $("#tbl_VisaDetails").css("width", "100%");

}
//function Search() {
//    $("#tbl_VisaDetails tbody tr").remove();

//    $("Norecord").css("display", "none");
//    var RefNo = $("#txt_RefNo").val()
//    var ApplicationName = $("#txt_AppName").val()
//    var Doc = $("#txt_DocNo").val()
//    var Status = $("#selStatus").val()
//    var fromDate = $("#txt_Fdate").val()
//    var ToDate = $("#txt_Fdate").val();
//    var VisaType = $("#selService").val();
//    var Agency = $("#agentid").text();
//    if (Agency == "")
//        Agency = 0;
//    var RequestParams = "?iDisplayLength= " + document.getElementsByName("tbl_VisaDetails_length")[0].value + "&iDisplayStart=" + Start + "&search[value]=" + "";
//    $.ajax({
//        type: "POST",
//        url: "../Handler/VisaDetails.asmx/AdminVisaSearch" + RequestParams,
//        data: '{"RefNo":"' + RefNo + '","ApplicationName":"' + ApplicationName + '","PassportNo":"' + Doc + '","Status":"' + Status + '","fromDate":"' + fromDate + '","ToDate":"' + ToDate + '","VisaType":"' + VisaType + '","Agency":"' + Agency + '"}',
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//            if (result.retCode == 1) {
//                arrVisa = result.tbl_VisaDetails;
//                HtmlBuilder(arrVisa)
//            }

//            else if (result.retCode == 1) {
//                $("Norecord").css("display", "");
//            }
//        },
//        error: function () {
//            alert('error')
//        }
//    });

//    //}
//    //else
//    //{
//    //    alert("Please Insert Some Value for Visa Search")
//    //}

//}
function HtmlBuilder(arrVisa) {
    var tRow = '';
    if (arrVisa.length > 0) {
        for (i = 0; i < arrVisa.length; i++) {
            var Service = (arrVisa[i].IeService).split(' ')
            var AppliedDate = arrVisa[i].AppliedDate;
            var GDate = arrVisa[i].AppliedDate;
            var CompletedDate = arrVisa[i].CopletedDate;
            var Completed = "N/a";
            var s;
            var Applieddt;
            if (CompletedDate != null) {
                s = CompletedDate.split(' ');
                Completed = s[0];
            }
            s = GDate.split(' ');
            var GenrationDate = s[0];
            debugger;
            if (GenrationDate.indexOf("/") != -1) {
                Applieddt = GenrationDate.split('/');
                GenrationDate = Applieddt[1] + "-" + Applieddt[0] + "-" + Applieddt[2];
            }
            else if (GenrationDate.indexOf("-") != -1) {
                Applieddt = GenrationDate.split('/');
                GenrationDate = Applieddt[0] + "-" + Applieddt[1] + "-" + Applieddt[2];
            }
            //var Date = AppliedDate.split(" ");
            var Status = arrVisa[i].Status;
            if (Status == "Under Process-Posted")
                Status = "Posted";
            else if (Status == "Processing-Applied")
                Status == "Processing-Applied";
            else if (Status == "Cancelled-Cancelled Before Applying")
                Status == "Cancelled-Cancelled Before Applying";
            tRow += '<tr>';
            tRow += '<td class="align-center">' + parseInt(Start + i) + '</td>';
            tRow += '<td class="align-center">' + arrVisa[i].sid + '</a></td>';
            tRow += '<td class="align-center">' + arrVisa[i].AgencyName + '</td>';
            tRow += '<td class="align-center">' + arrVisa[i].FirstName + '</td>';
            tRow += '<td class="align-center">' + arrVisa[i].PassportNo + '</td>';
            tRow += '<td class="align-center">' + arrVisa[i].VisaCountry + '</td>';
            tRow += '<td class="align-center">' + Service[0] + " " + Service[1] + '</td>';
            if (arrVisa[i].Status == null || arrVisa[i].Status == 'null')
                Status = '';
            if (arrVisa[i].ActiveStatus == "Active")
                tRow += '<td style="max-width: 75px; vertical-align: middle; width :9%" class="center"><span class="status-vouchered"><a style="cursor:pointer ; color : white;" onclick="UpdateStatus(\'' + arrVisa[i].FirstName + " " + arrVisa[i].LastName + '\',\'' + arrVisa[i].Status + '\',\'' + arrVisa[i].sid + '\',\'' + arrVisa[i].History + '\',\'' + arrVisa[i].Vcode + '\',\'' + arrVisa[i].UserId + '\',\'' + arrVisa[i].EdnrdUserName + '\',\'' + arrVisa[i].Sponsor + '\',\'' + arrVisa[i].TReference + '\',\'' + arrVisa[i].TotalAmount + '\')">' + Status.replace("Under Process", "Not Posted").replace("Under Process-Applied", "Posted").replace("Approved", "Application Approved").replace("Rejected", "Application Rejected").replace("Entered in country", "Visa Holder entered in country").replace("left the country", "Visa Holder left the country").replace("Documents Required", "Not Posted").replace("Processing error", "Application processing error") + '</a></td>';
            else if (arrVisa[i].ActiveStatus == "In-Active")
                tRow += '<td style="max-width:70px; vertical-align: middle; width :9%" class="center"><span class="status-cancelled"><a style="cursor:pointer ; color: white;" onclick="UpdateStatus(\'' + arrVisa[i].FirstName + " " + arrVisa[i].LastName + '\', \'' + 'Incomplete' + '\',\'' + arrVisa[i].sid + '\',\'' + arrVisa[i].History + '\',\'' + arrVisa[i].Vcode + '\',\'' + arrVisa[i].UserId + '\',\'' + arrVisa[i].EdnrdUserName + '\',\'' + arrVisa[i].Sponsor + '\',\'' + arrVisa[i].TReference + '\',\'' + arrVisa[i].TotalAmount + '\')">Incomplete</a></td>';
            tRow += '<td class="align-center"> ' + arrVisa[i].AppliedDate + '</td>';
            tRow += '<td class="align-center">' + arrVisa[i].Sponsor + '</td>';
            tRow += '<td>';
            //if (arrVisa[i].VStatus == false)
            //    tRow += '<td align="center"><a style="cursor:pointer" target="_blank" title="Update Documents" href="ImagesUpload.aspx?Image=' + arrVisa[i].Vcode + '">Update Images</a> <br/> <a style="cursor:pointer" title="Upload Visa Copy"  href="AttachVisa.aspx?Image=' + arrVisa[i].Vcode + '">Upload</a></td>';
            //else if (arrVisa[i].VStatus == true)
            //    tRow += '<td align="center"><a style="cursor:pointer" target="_blank" title="Update Documents" href="ImagesUpload.aspx?Image=' + arrVisa[i].Vcode + '">Update Images</a> <br/><br/> <a style="cursor:pointer;" title="View Visa Copy" onclick="ViewVisa(\'' + arrVisa[i].Vcode + '\')">View Visa</a></td>';
            ////  tRow += '<td style="width:15%"  align="center"><a style="cursor:pointer" title="Download" onclick="GetDocuments(\'' + arrVisa[i].Vcode + '\',\'' + arrVisa[i].IeService + '\')">Document</a> <br/> <a style="cursor:pointer;" title="View Visa Copy" onclick="ViewVisa(\'' + arrVisa[i].Vcode + '\')">View Visa</a></td>';
            //tRow += '<td align="center"><a style="cursor:pointer" title="Print" onclick="GetPrintInvoice(\'' + arrVisa[i].Vcode + '\',\'' + arrVisa[i].UserId + '\')">View</a></td>';
            tRow += '<a title = "Click for Voucher" class="voucher-btn"  onclick = "ViewVisaApplication(\'' + arrVisa[i].Vcode + '\',\'' + arrVisa[i].UserId + '\')" >View</a><br>';
            tRow += '<a title = "Click for Invoice" class="invoice-btn"  onclick = "VisaPrintInvoice(\'' + arrVisa[i].Vcode + '\',\'' + arrVisa[i].UserId + '\')" >Invoice</a><br>';
            if (arrVisa[i].VStatus == false)
                tRow += '<a title = "Update Documents" class="modify-btn"  href="ImagesUpload.aspx?Image=' + arrVisa[i].Vcode + '" >Upload</a>';
            else if (arrVisa[i].VStatus == true)
                tRow += '<a title = "Update Documents" class="modify-btn"  href="ImagesUpload.aspx?Image=' + arrVisa[i].Vcode + '" >Upload</a>';
            tRow += '</td></tr>';
        }
        $("#tbl_VisaDetails tbody").html(tRow);
        Last = parseInt(Start) + parseInt(document.getElementsByName("tbl_VisaDetails_length")[0].value)
        $("#tbl_VisaDetails_info").text('Showing ' + Start + ' to ' + Last + ' of ' + arrVisa[0].TotalCount + ' entries')
        document.getElementById("tbl_VisaDetails").removeAttribute("style")
        document.getElementById("tbl_VisaDetails_next").setAttribute("onclick", "Next()")
        document.getElementById("tbl_VisaDetails_previous").setAttribute("onclick", "LastRecord()")
        $("#tbl_VisaDetails_first").css("display", "none")
        $("#tbl_VisaDetails_last").css("display", "none")
        document.getElementById("tbl_VisaDetails").removeAttribute("style")
    }
}

function DateFormat(Date) {
    var dFormat = Date.split('-')
    Date = dFormat[1] + '-' + dFormat[0] + '-' + dFormat[2];
    return Date;
}
function UpdateNewStatus() {
    var Status = $("#selVisaStatus").val();
    if (Status == "Posted" && oldStatus != Status) {
        var bValid = true;
        var Username = $("#txt_UserLogin").val();
        var Password = $("#txt_Password").val();
        var Application = $("#txt_ApplicationNo").val();
        if ($("#txt_UserLogin").val() == "") {
            $("#txt_UserLogin").focus();
            bValid = false;
        }
        else if ($("#txt_Password").val() == "") {
            $("#txt_Password").focus();
            bValid = false;
        }
        else if ($("#txt_ApplicationNo").val() == "") {
            $("#txt_ApplicationNo").focus();
            bValid = false;
        }
        if (bValid == true) {
            $.ajax({
                type: "POST",
                url: "../Handler/VisaDetails.asmx/PostVisa",
                data: '{"sid":"' + HiddenId + '","status":"' + Status + '","RefrenceNo":"' + VCode + '","UserId":"' + UserId + '","Username":"' + Username + '","Password":"' + Password + '","AppNo":"' + Application + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        return false;
                    }
                    if (result.retCode == 1) {
                        Success("Status Updated Successfully")
                        //$("#StatusModal").modal("hide")
                        $("#tbl_VisaDetails").dataTable().fnClearTable();
                        $("#tbl_VisaDetails").dataTable().fnDestroy();
                        GetVisa()
                    }
                    else {
                        Success("Something went wrong! Please contact administrator.")
                    }
                },
                error: function () {
                }
            });

        }

    }
    else if (Status == "Application Rejected By Admin") {
        var bValid = true;
        var Refund = false;
        var IsRefundable;
        var nonRefundable;
        if (refund.checked == false || notrefund.checked == false) {
            if (refund.checked == false && notrefund.checked == true) {
                Refund = false;
            }
            else if (refund.checked == true && notrefund.checked == false) {
                Refund = true;
            }
            else {
                Success("Please Select Refundable Condition")
                bValid = false;
            }


        }
        //else if()
        //{

        //}
        if (bValid == true) {
            $.ajax({
                type: "POST",
                url: "../Handler/VisaDetails.asmx/RejectByAdmin",
                data: '{"RefNo":"' + VCode + '","Uid":"' + UserId + '","Amount":"' + Amount + '","IsRefund":"' + Refund + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.Session == 0) {
                        return false;
                    }
                    if (result.retCode == 1) {
                        Success("Status Updated Successfully")
                        //$("#StatusModal").modal("hide")
                        $("#tbl_VisaDetails").dataTable().fnClearTable();
                        $("#tbl_VisaDetails").dataTable().fnDestroy();
                        GetVisa()
                    }
                    else {
                        Success("Something went wrong! Please contact administrator.")
                    }
                },
                error: function () {
                }
            });

        }
    }
    else if (Status != oldStatus) {
        $.ajax({
            type: "POST",
            url: "../Handler/VisaDetails.asmx/UpdateVisaStatus",
            data: '{"sid":"' + HiddenId + '","status":"' + Status + '","UserId":"' + UserId + '","RefrenceNo":"' + VCode + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    return false;
                }
                if (result.retCode == 1) {
                    //$("#StatusModal").modal("hide")
                    Success("Visa Updated Successfully")
                    $("#tbl_VisaDetails").dataTable().fnClearTable();
                    $("#tbl_VisaDetails").dataTable().fnDestroy();
                    GetVisa()
                }
                else {
                    alert("Something went wrong! Please contact administrator.");
                }
            },
            error: function () {
            }
        });

    }
    else {
        alert("Status No Changed")
    }

}
function OnPosting() {

    var Type = $("#selVisaStatus option:selected").text();
    $("#RejectCondition").css("display", "none")
    $("#tr_UserPost").css("display", "none")
    $("#tr_PasswordPost").css("display", "none")
    $("#tr_ApplicationNo").css("display", "none")
    //alert(Type)
    if (Type == "Posted") {
        $("#tr_UserPost").css("display", "")
        $("#tr_PasswordPost").css("display", "")
        $("#tr_ApplicationNo").css("display", "")
    }
    else if (Type == "Application Rejected By Admin") {
        $("#RejectCondition").css("display", "")
    }
    else {
        $("#tr_UserPost").css("display", "none")
        $("#tr_PasswordPost").css("display", "none")
        $("#tr_ApplicationNo").css("display", "none")
    }
}
function GetStatus(VisaCode, Application) {
    VCode = VisaCode;
    HiddenId = Application;
    $("#txt_Application").val(Application)
    $("#GetStatusModal").modal("show")
}
function GetIEStatus() {
    var bValid = true;
    var UserName = $("#txt_Username").val();
    var Password = $("#txt_UserPassword").val();
    if (UserName == "") {
        bValid = false;
        $("#txt_Username").focus();
    }
    else if (Password == "") {
        bValid = false;
        $("#txt_UserPassword").focus();
    }
    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "../Handler/VisaDetails.asmx/GetIEVisaStatus",
            data: '{"VCode":"' + VCode + '","ApplicationNo":"' + HiddenId + '","Username":"' + UserId + '","Password":"' + Password + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    return false;
                }
                if (result.retCode == 1) {
                    $("#StatusModal").modal("hide")
                    GetVisa()
                }
                else {
                    alert("Something went wrong! Please contact administrator.");
                }
            },
            error: function () {
            }
        });

    }

}

function UpdateMultipuleStatus() {
    var ApplicationNo = [];
    var VisaNo = [];
    var inpfields = document.getElementsByTagName('input');
    var nr_inpfields = inpfields.length;
    for (var i = 0; i < nr_inpfields; i++) {
        if (inpfields[i].type == 'checkbox' && inpfields[i].checked == true) {
            ApplicationNo.push(inpfields[i].value);
            VisaNo.push(inpfields[i].name);
        }
    }
    var bValid = true;
    var UserName = $("#txt_Username").val();
    var Password = $("#txt_UserPassword").val();
    if (UserName == "") {
        bValid = false;
        $("#txt_Username").focus();
    }
    else if (Password == "") {
        bValid = false;
        $("#txt_UserPassword").focus();
    }
    if (bValid == true) {
        $.ajax({
            type: "POST",
            url: "../Handler/VisaDetails.asmx/GetIEVisaStatus",
            data: '{"VCode":"' + VisaNo + '","ApplicationNo":"' + ApplicationNo + '","Username":"' + UserId + '","Password":"' + Password + '"}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.Session == 0) {
                    return false;
                }
                if (result.retCode == 1) {
                    $("#StatusModal").modal("hide")
                    GetVisa()
                }
                else {
                    alert("Something went wrong! Please contact administrator.");
                }
            },
            error: function () {
            }
        });

    }
}
function CropImage(Id) {
    debugger;

    var Url = '../Agent/CropImage.aspx?File=' + Id + '&RefId=' + hidenId

    window.open(Url, 'Crop Image', 'left=5000,top=5000,width=900,height=800')


}

function ExportToExcel(Document) {
    var RefNo = $("#txt_RefNo").val()
    var ApplicationName = $("#txt_AppName").val()
    var Doc = $("#txt_DocNo").val()
    var Status = $("#selStatus").val()
    //var fromDate = $("#txt_Fdate").val()
    //var ToDate = $("#txt_Fdate").val();
    var VisaType = $("#selService").val();
    if (VisaType == "-") {
        VisaType = "";
    }
    var Agency = $("#agentid").text();
    //var RequestParams = "?RefNo=" + RefNo + "&FirstName=" + ApplicationName + "&Doc=" + Doc + "&Status=" + Status
                      //  +"&VisaType=" + VisaType + "&Agency=" + Agency;
  //  window.location.href = "../Handler/ExportToExcelHandler.ashx?datatable=VisaBookingDetails" + RequestParams;
                        window.location.href = "../Handler/ExportToExcelHandler.ashx?datatable=VisaBookingDetails&Document=" + Document + "&RefNo=" + RefNo + "&FirstName=" + ApplicationName + "&Doc=" + Doc + "&Status=" + Status
                        + "&VisaType=" + VisaType + "&Agency=" + Agency;
}
var HiddenId, VCode, UserId;

function UpdateStatus(PaxName, Status, sid, history, Vcode, AgentId, Username, Password, AppNo, TotalAmmount) {

    $.modal({
        content:

            '<div class="columns" id="StatusModal">' +
            '<div class="columns">                                                                                    ' +
            '    <div class="six-columns twelve-columns-mobile four-columns-tablet bold">                             ' +
            '        <label>Pax Name:</label><span id="spn_PaxName"></span>                                           ' +
            '    </div>                                                                                               ' +
            '    <div class="six-columns twelve-columns-mobile four-columns-tablet bold">                             ' +
            '        <label>Refrence No:</label> <span id ="spn_Ref"></span>                                          ' +
            '    </div>                                                                                               ' +
            '                                                                                                         ' +
            ' </div>                                                                                                  ' +
            '<h2><a href="#" class="addnew"><i class="formTab fa fa-angle-left"></i></a></h2>' +
            '<div id="filter1" class="with-padding anthracitef-gradient" >' +
            '<div class="twelve-columns twelve-columns-mobile four-columns-tablet bold"> ' +
            '<span  data-toggle="collapse" data-target="#history" id="hisbtn" style="cursor:pointer">History</span> ' +
            '<span id="spn_Status"></span> ' +
            '<span id="spn_Status"></span> ' +
            '<div id="history" style="scrollbar-arrow-color:darkblue;overflow-y: scroll;width: auto;height: 150px"class="collapse"></div> ' +
            '</div>' +
            //'<div class="columns">                                                                                    ' +
            //'<div class="twelve-columns twelve-columns-mobile four-columns-tablet bold"> ' +
            //'<span  data-toggle="collapse" data-target="#history" id="hisbtn" style="cursor:pointer">History</span> ' +
            //'<span id="spn_Status"></span> ' +
            //'<div id="history" style="scrollbar-arrow-color:darkblue;overflow-y: scroll;width: auto;height: 150px"class="collapse"></div>                                                                                 ' +
            //'                                         </div>' +
        ' </div></br>' +

            '<div class="columns">' +
                    '<div class="twelve-columns twelve-columns-mobile" id="divVisaStatus">' +
                        '<div class="full-width button-height">' +
                            '<select id="selVisaStatus" class="select OfferType" onchange="OnPosting()">' +
                                '<option value="Incomplete">InComplete</option>                                        ' +
                                '<option value="Under Process">Not Posted</option>                                     ' +
                                '<option value="Posted">Posted</option>                                                ' +
                                '<option value="Approved">Application Approved</option>                                ' +
                                '<option value="Rejected">Application Rejected</option>                                ' +
                                '<option value="Entered in country">Visa Holder entered in country</option>            ' +
                                '<option value="Left the country">Visa Holder left the country</option>                ' +
                                '<option value="Documents Required">Documents Required for further processing</option> ' +
                                '<option value="Processing error">Application processing error</option>                ' +
                                '<option value="Application Rejected By Admin"  >Application Rejected By Admin</option>' +
                            '</select>' +
                        '</div>' +
                    '</div>' +
                 ' </div>  ' +
                  '<div class="columns" style="display:none" id="RejectCondition">' +
                  '  <div class="six-columns twelve-columns-mobile">' +
                  '  <input type="radio" name="rejected" value="Return Ammount" id="refund" class="radio mid-margin-left">  <label for="radio-2" class="label">Ammount Refund</label>' +
                  '  </div>' +
                  '  <div class="six-columns twelve-columns-mobile">' +
                  '      <input type="radio" name="rejected" value="Ammount not Return" id="notrefund" class="radio mid-margin-left"> <label for="radio-2" class="label"> Amount not Refund</label>' +
                  '  </div>' +
                  '  </div>' +
                  '<div class="columns" style="display:none" id="tr_UserPost">' +
                  '  <div class="twelve-columns twelve-columns-mobile four-columns-tablet bold">' +
                  '<label>Supplier</label>' +
                  '<div class="input full-width">' +
                  '<input type="text" class="input-unstyled full-width" id="txt_UserLogin" placeholder="User Login">' +
                  '</div>' +
                  '</div>' +
                  '<div class="twelve-columns twelve-columns-mobile four-columns-tablet bold">' +
                  '<label>Application No</label>' +
                  '<div class="input full-width">' +
                  '<input type="text" class="input-unstyled full-width" id="txt_ApplicationNo" placeholder="Application No">' +
                  '</div>' +
                   '</div>' +
                 '</div>' +
                ' </div>' +
                '<p class="text-right" style="text-align:right;">' +
                    '<button type="button" class="button anthracite-gradient" onclick="UpdateNewStatus()">UpdateNewStatus</button>' +
                '</p>' +
        '</div>',

        title: 'Update Status ',
        width: 500,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
    $("#Rejected").css("display", "none")
    HiddenId = sid;
    VCode = Vcode;
    UserId = AgentId;
    oldStatus = Status;
    Amount = TotalAmmount
    $('input[name=rejected]').attr('checked', false);
    //alert(Amount)
    if (Username == 'null')
        $("#txt_UserLogin").val("n/a");
    else
        $("#txt_UserLogin").val(Username);
    if (Password == 'null')
        $("#txt_Password").val("n/a");
    else
        $("#txt_Password").val(Password);
    $("#txt_ApplicationNo").val(AppNo);
    $('#selVisaStatus').val(Status)
    var txt = $("#selVisaStatus option:selected").text();
    //$("#divVisaStatus .select span")[0].textContent = txt;
    AppendVisaStatus(Status, txt);
    //$("#selVisaStatus").val(Status);
    $("#spn_PaxName").html(PaxName);
    $("#spn_Ref").html(sid);
    switch (Status) {
        case "Under Process":
            $("#Rejected").css("display", "")
            break;
        case "Posted":
            $("#Rejected").css("display", "")
            break;
    }
    if (Status == "Posted") {
        $("#tr_UserPost").css("display", "")
        $("#tr_PasswordPost").css("display", "")
        $("#tr_ApplicationNo").css("display", "")
    }
    else {
        $("#tr_UserPost").css("display", "none")
        $("#tr_PasswordPost").css("display", "none")
        $("#tr_ApplicationNo").css("display", "none")
    }

    if (history != "") {
        $("#history").empty();
        var splited = history.split('^')
        var length = splited.length;
        var s, Date;
        var label;
        for (var i = 0; i < length; i++) {
            s = splited[i].split(':')
            label += '' + (i + 1) + '. <label class="size11">' + s[0] + ": " + DateFormat(s[1]) + '</label><br/>';
        }
        var s = label.split('undefined')
        $("#history").append(s[1])
    }

    $('.formTab').click(function () {
        $(' #filter1').slideToggle();
        $('.searchBox li a ').on('click', function () {
            $('.filterBox #filter1').slideUp();
        });
    });
    $(".formTab").toggleClass("fa-angle-right");
    $(".formTab").removeClass("fa-angle-left");

}
function AppendVisaStatus(Status, txt) {
    try {

        var checkclass = document.getElementsByClassName('check');
        //var Description = $.grep(arrCity, function (p) { return p.Description == Description; }).map(function (p) { return p.Code; });

        $("#divVisaStatus .select span")[0].textContent = txt;
        //for (var j = 0; j <= Description.length; j++) {
        $('input[value="' + Status + '"][class="OfferType"]').prop("selected", true);
        $("#selVisaStatus").val(Status);
        //}
    }
    catch (ex)
    { }
}
//function UpdateStatus(RefrenceNo, Uid, OTBStatus) {
//    Ref = RefrenceNo;
//    UserId = Uid;
//    OldStatus = OTBStatus;
//    $.modal({
//        content: '<div class="columns" id="OtbStatusModal">' +
//                    '<div class="twelve-columns twelve-columns-mobile" id="divOtbStatus">' +
//                        '<div class="full-width button-height OtbStatus">' +
//                            '<select id="selOtbStatus" class="form-control" onchange="Notification()">' +
//                                '<option value="-" selected="selected">--Salect Otb Status--</option>' +
//                                '<option value="Processing">Recived</option>' +
//                                '<option value="Processing-Applied">Applied</option>' +
//                                '<option value="Otb Updated">Approved</option>' +
//                                '<option value="Otb Not Required">Not Required</option>' +
//                                '<option value="Rejected">Cancelled By Admin</option>' +
//                            '</select>' +
//                        '</div>' +
//                    '</div>' +
//                '<p class="text-right" style="text-align:right;">' +
//                    '<button type="button" class="button anthracite-gradient" onclick="OtbEmailCOnformation()">Update Status</button>' +
//                '</p>',

//        title: 'Update Status ',
//        width: 500,
//        scrolling: true,
//        actions: {
//            'Close': {
//                color: 'red',
//                click: function (win) { win.closeModal(); }
//            }
//        },
//        buttons: {
//            'Close': {
//                classes: 'huge anthracite-gradient displayNone',
//                click: function (win) { win.closeModal(); }
//            }
//        },
//        buttonsLowPadding: true
//    });

//}




