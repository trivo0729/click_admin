﻿function GetRateAllType() {
    setTimeout(function() {
        post("../handler/GenralHandler.asmx/_ByRateType", { HotelAdminID: HotelAdminID }, function (data) {
            $('#sel_Hotel').empty();
            $(data.arrRateTypes).each(function (index, arrRateType) { // GETTING Sucees HERE
                $('#sel_RateType').append($('<option></option>').val(arrRateType.sRateType).html(arrRateType.sRateType));
            });
            $('#sel_RateType').change();
        }, function (error_data) {// GETTING ERROR HERE
            AlertDanger(error_data.ex)
        });
    },500)
   
}

function GetTypeByRate() {
    setTimeout(function() {
        post("../handler/GenralHandler.asmx/_ByRate", { HotelAdminID: HotelAdminID }, function (data) {
            $('#sel_Hotel').empty();
            $(data.arrRateTypes).each(function (index, arrRateType) { // GETTING Sucees HERE
                $('#sel_RateType').append($('<option></option>').val(arrRateType.sRateType).html(arrRateType.sRateType));
            });
            $('#sel_RateType').change();
        }, function (error_data) {// GETTING ERROR HERE
            AlertDanger(error_data.ex)
        });
    },500)
}

function GetTypeByRate(Inventory, HotelCode, RoomCode) {
    $("#sel_RateType").addClass('validate[required]')
    $('#sel_RateType').append($('<option disabled></option>').val("").html('-Please Select Rate Type-'));
    post("../handler/GenralHandler.asmx/_ByRateInventory", { Inventory: Inventory, HotelCode: HotelCode, RoomCode: RoomCode }, function (data) {
        $('#sel_RateType').empty();
        var arrRateType = data.arrRateTypes;
        $(data.arrRateTypes).each(function (index, arrRateType) { // GETTING Sucees HERE
            $('#sel_RateType').append($('<option></option>').val(arrRateType.sRateType).html(arrRateType.sRateType));
        });
        $('#sel_RateType').change();
    }, function (error_data) {// GETTING ERROR HERE
        AlertDanger(error_data.ex)
    });
}

function AddRate() {
  var  HotelCode = GetQueryStringParams('sHotelID');
  var  HotelName = GetQueryStringParams('HName').replace(/%20/g, ' ');
  var  RoomCode = GetQueryStringParams('RoomID');
  var RoomName = GetQueryStringParams('RName').replace(/%20/g, ' ');
  window.location.href = 'roomrate.aspx?sHotelID=' + HotelCode + '&HotelName=' + HotelName + '&RoomId=' + RoomCode + '&RoomType=' + RoomName;
}



function GetTypeByHotel(HotelCode) {
    $("#sel_RateType").addClass('validate[required]')
    $('#sel_RateType').append($('<option disabled></option>').val("").html('-Please Select Rate Type-'));
    post("../handler/GenralHandler.asmx/_ByHotel", { HotelCode: HotelCode}, function (data) {
        $('#sel_RateType').empty();
        var arrRateType = data.arrRateTypes;
        $(data.arrRateTypes).each(function (index, arrRateType) { // GETTING Sucees HERE
            $('#sel_RateType').append($('<option></option>').val(arrRateType.sRateType).html(arrRateType.sRateType));
        });
        $('#sel_RateType').change();
    }, function (error_data) {// GETTING ERROR HERE
        AlertDanger(error_data.ex)
    });
}
