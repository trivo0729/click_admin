﻿var HotelList = "";
var HotelCountryList = "";
var Div = '';
var OfferList = "";
var sid = 0;
var HotelName = "";
$(document).ready(function () {
    debugger

    GetAllCPContactList();
    AddContactDetailUI();
    ContactType = GetQueryStringParams('ContactType');
    if (ContactType != undefined) {
        $("#btn_update").show();
        $("#btn").hide();
        GetContactDetailByContactType(ContactType);
    }

});
function GetCountry(Id) {
    $.ajax({
        type: "POST",
        url: "../Handler/SalesHandler.asmx/GetCountry",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrCountry = result.CountryList;
                Arr_Country = result.CountryList;
                if (arrCountry.length > 0) {
                    $("#selCountry").empty();
                    $("#selTerCityCountry").empty();

                    var ddlRequest = '<option selected="selected" value="-">Select Any Country</option>';
                    for (i = 0; i < arrCountry.length; i++) {
                        ddlRequest += '<option value="' + arrCountry[i].Country + '">' + arrCountry[i].Countryname + '</option>';
                    }
                    //if (Id == 1) {
                    $("#selCountry" + Id).append(ddlRequest);

                    //}
                    //else {
                    $("#selCountry" + Id).append(ddlRequest);
                    //}
                    //$("#selCountry").append(ddlRequest);
                    //$("#selTerCityCountry").append(ddlRequest);
                }
            }
        },
        error: function () {
            alert("An error occured while loading countries")
        }
    });
}

var Count = 1;
//var Count = 1;
function AddContactDetailUI() {
    var Div = "";
    Div += '<div class="columns" id="MyDiv' + Count + '">'

    Div += '<div class="three-columns">'
    Div += '<span class="text-left">Coustomer Support Country <span class="red">*</span></span>'
    Div += '<div class="full-width button-height" id = "DivselCountry' + Count + '" >'
    Div += '<select id="selCountry' + Count + '" class="select OfferType">'
    Div += '</select>'
    Div += '</div >'
    //Div += '<input type="text" class="input full-width name" id="txtSeasonName' + Count + '"/>'
    Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_selCountry' + Count + '">'
    Div += '<b>* This field is required</b></label>'
    Div += '</div>'

    Div += '<div class="three-columns">'
    Div += '<span class="text-left">Coustomer Support No <span class="red">*</span></span>'
    Div += '<input class="input full-width CPNo" type="text" id="CPNo' + Count + '"  placeholder="Coustomer Support No"/>'
    Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_CPNo' + Count + '">'
    Div += '<b>* This field is required</b></label>'
    Div += '</div>'

    Div += '<div class="three-columns">'
    Div += '<span class="text-left">Coustomer Support Email <span class="red">*</span></span>'
    Div += '<input class="input full-width CPEmail" type="text" id="CPEmail' + Count + '"  placeholder="Coustomer Support Email" />'
    Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_CPEmail' + Count + '">'
    Div += '<b>* This field is required</b></label>'
    Div += '</div>'



    Div += '<br><div id="btnAddContactDetail' + Count + '" class="one-columns" title="Add Customer Support Detail">'
    Div += '<i Onclick="CheckEmpty(\'' + Count + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button anthracite-gradient" ><span class="icon-plus"></span></label></i>'

    Div += '</div>'
    if (Count != 1) {
        $("#btnAddContactDetail" + parseInt(Count - 1)).hide();
        Div += '<div class="one-columns" title="Remove Contact Detail">'
        Div += '<i Onclick="RemoveContactDetailUI(\'' + Count + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button anthracite-gradient" ><span class="icon-minus"></span></label></i>'
        Div += '</div>'
    }
    Div += '</div>'
    $("#ContactDetailUI").append(Div);
    GetCountry(Count);
    //AppendControls(Count);
    Count += 1;
    //Count++;
}
function CheckEmpty(Count) {
    var chk = true;
    //var Ndt = parseInt(parseInt(dt) + parseInt(1));
    //var Ntxt = parseInt(parseInt(txt) + parseInt(1));
    //var Ndt = parseInt(parseInt(dt) + parseInt(1));
    if (Count == 1) {
        if ($("#CPNo" + Count).val() == "") {
            $("#lbl_CPNo" + Count).css("display", "");
            chk = false;
        }
        else {
            $("#lbl_CPNo" + Count).css("display", "none");
        }

        if ($("#CPEmail" + Count).val() == "") {
            $("#lbl_CPEmail" + Count).css("display", "");
            chk = false;
        }
        else {
            $("#lbl_CPEmail" + Count).css("display", "none");
        }
    }
    else {
        if ($("#CPNo" + Count).val() == "") {
            $("#lbl_CPNo" + Count).css("display", "");
            chk = false;
        }
        else {
            $("#lbl_CPNo" + Count).css("display", "none");
        }

        if ($("#CPEmail" + Count).val() == "") {
            $("#lbl_CPEmail" + Count).css("display", "");
            chk = false;
        }
        else {
            $("#lbl_CPEmail" + Count).css("display", "none");
        }

    }
    if ($("#selCountry" + Count).val() == "-") {
        $("#lbl_selCountry" + Count).css("display", "");
        chk = false;
    }
    else {
        $("#lbl_selCountry" + Count).css("display", "none");
    }

    GetCountry(Count);
    if (chk) {
        AddContactDetailUI();
    }
}
function RemoveContactDetailUI(Cnt) {
    var c = parseInt(Cnt) - 1;

    $("#MyDiv" + Cnt).remove();
    $("#btnAddContactDetail" + c).show();

}
function Validate() {
    bValid = true;
    ContactType = "";
    //HotelId = sid;
    ContactType = ContactType;
    Country = "";
    CPNo = "";
    CPEmail = "";
    ContactType = $("#txt_CoustomerSupportType").val();
    if (ContactType == "" || ContactType == null) {
        $('#lbl_CoustomerSupportType').css("display", "");
        bValid = false;
    }
    else {
        $('#lbl_CoustomerSupportType').css("display", "none");
    }

    for (var i = 0; i < $(".CPNo").length; i++) {

        var name = $(".CPNo")[i].value;
        CPNo += name + "^";
        if (name == "") {
            $('#lbl_' + $(".CPNo")[i].id).css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_' + $(".CPNo")[i].id).css("display", "none");
        }
    }
    for (var i = 0; i < $(".CPEmail").length; i++) {

        var name = $(".CPEmail")[i].value;
        CPEmail += name + "^";
        if (name == "") {
            $('#lbl_' + $(".CPEmail")[i].id).css("display", "");
            bValid = false;
        }
        else {
            $('#lbl_' + $(".CPEmail")[i].id).css("display", "none");
        }
    }
    for (var i = 0; i < $(".OfferType").length; i++) {
    //for (var i = 0; i < Count-1; i++) {   
        var name = $("#selCountry" +(parseInt(i+1))).val();
        //var name = $("#selCountry" + (i + 1)).val();
        Country += name + "^";
        if (name == "-" || name == undefined) {
            //$('#lbl_' + $("#selCountry")[i + 1].id).css("display", "");
            bValid = false;
        }
        else {
            //$('#lbl_' + $("#selCountry")[i + 1].id).css("display", "none");
        }
    }
    return bValid;
}
function AddDetail() {
    bValid = Validate();
    if (bValid == true) {

        var Data = {
            Country: Country, CPNo: CPNo, CPEmail: CPEmail, ContactType: ContactType
        };

        $.ajax({
            type: "POST",
            url: "../Handler/VehicleMasterHandller.asmx/AddCustomerSupportDetails",
            data: JSON.stringify(Data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {

                var obj = JSON.parse(response.d)
                if (obj.retCode == 1) {
                    // var Offer_Id = obj.Offer_Id;
                    Success("Details Save Successfully.");
                    window.location.reload();
                    ClearAll();
                }
            },
        });
    }
    else
        Success("Please fill All Compulsary Details");
}
var OfferId;
function UpdateDetail() {
    bValid = Validate();
    if (bValid == true) {
        for (var i = 0; i < $(".sid").length; i++) {

            var name = $(".sid")[i].value;
            sid += name + "^";
        }

        ContactType = ContactType;
        sid: sid;

        var Data = {
            sid: sid, ContactType: ContactType, Country: Country, CPNo: CPNo, CPEmail: CPEmail, ContactType: ContactType
        };

        $.ajax({
            type: "POST",
            url: "../handler/VehicleMasterHandller.asmx/UpdateCustomerSupportDetails",
            data: JSON.stringify(Data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {

                var obj = JSON.parse(response.d)
                if (obj.retCode == 1) {
                    // var Offer_Id = obj.Offer_Id;
                    Success("Contact Detail Updated.");
                    setTimeout(function () {
                        window.location.href = "AddUpdateCustomerSupportDetails.aspx";
                    }, 500);
                }
            },
        });
    }

}
function GetContactDetailByContactType(ContactType) {
    $.ajax({
        type: "POST",
        url: "../handler/VehicleMasterHandller.asmx/GetContactDetailByContactType",
        data: '{"ContactType":"' + ContactType + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var list_ContactList = result.tbl_ContactList;
            if (result.Session == 0) {

                //alert("Error in deleting ");
                return false;
            }
            if (result.retCode == 1) {
                var Div = "";
                $("#txt_CoustomerSupportType").val(list_ContactList[0].ContactType);
                $("#ContactDetailUI").empty();
                //var length = list_ContactList.length;
                for (var i = 0; i < list_ContactList.length; i++) {

                    Div += '<div class="columns" id="MyDiv' + i + '">'

                    Div += '<div class="three-columns" style="display:none;">'
                    Div += '<input class="input full-width sid" type="hidden"  value="' + list_ContactList[i].sid + '" id="sid' + i + '" />'
                    Div += '</div>'

                    //Div += '<input class="input full-width sid  " type="text" value="' + list_ContactList[i].sid + '" id="sid' + i + '" />'
                    Div += '<div class="three-columns">'
                    Div += '<span class="text-left">Coustomer Support Country <span class="red">*</span></span>'
                    Div += '<div class="full-width button-height" id = "DivselCountry' + i + '" >'
                    Div += '<select id="selCountry' + i + '" class="select OfferType">'
                    Div += '</select>'
                    Div += '</div >'
                    //Div += '<input type="text" class="input full-width name" id="txtSeasonName' + Count + '"/>'
                    Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_selCountry' + i + '">'
                    Div += '<b>* This field is required</b></label>'
                    Div += '</div>'
                    GetCountry(i);
                    setTimeout(function () {
                        $("#DivselCountry .select span")[i].textcontent = list_ContactList[i].Countryname;
                        $('input[value="' + list_ContactList[i].Country + '"][class="OfferType"]').prop("selected", true);
                        $("#selCountry" + i).val(list_ContactList[i].Country);
                    }, 500);

                    Div += '<div class="three-columns">'
                    Div += '<span class="text-left">Coustomer Support No <span class="red">*</span></span>'
                    Div += '<input class="input full-width CPNo" type="text" value="' + list_ContactList[i].ContactNo + '" id="CPNo' + i + '"  placeholder="Coustomer Support No"/>'
                    Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_CPNo' + i + '">'
                    Div += '<b>* This field is required</b></label>'
                    Div += '</div>'

                    Div += '<div class="three-columns">'
                    Div += '<span class="text-left">Coustomer Support Email <span class="red">*</span></span>'
                    Div += '<input class="input full-width CPEmail" type="text" value="' + list_ContactList[i].ContactEmail + '" id="CPEmail' + i + '"  placeholder="Coustomer Support Email" />'
                    Div += '<label style="color: red; margin-top: 3px; display: none" id="lbl_CPEmail' + i + '">'
                    Div += '<b>* This field is required</b></label>'
                    Div += '</div>'



                    Div += '<br><div id="btnAddContactDetail' + i + '" class="one-columns" title="Add Customer Support Detail">'
                    Div += '<i Onclick="CheckEmpty(\'' + i + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button anthracite-gradient" ><span class="icon-plus"></span></label></i>'

                    Div += '</div>'
                    if (i == 1) {
                        $("#btnAddContactDetail" + parseInt(i - 1)).hide();
                        Div += '<div class="one-columns" title="Remove Contact Detail">'
                        Div += '<i Onclick="RemoveContactDetailUI(\'' + i + '\')" aria-hidden="true"><label for="pseudo-input-2" class="button anthracite-gradient" ><span class="icon-minus"></span></label></i>'
                        Div += '</div>'
                    }
                    Div += '</div>'
                    

                }
                $("#ContactDetailUI").append(Div);
            }
        },
        error: function () {
            // alert("Error in deleting ");
        }
    });
}
//function AppendCountry(id) {
//    debugger;
//    try {
//        var checkclass = document.getelementsbyclassname('check');
//        //var VehicleBrand = $.grep(arrVehicleType, function (p) { return p.ID == VehicleType; })
//        //    .map(function (p) { return p.VehicleBrand; });
//        //$("#DivVehicleType2 .select span")[0].textcontent = VehicleBrand;
//        //for (var i = 0; i < arrVehicleType.length; i++) {
//        //    if (arrVehicleType[i].ID == VehicleType) {
//        $("#DivselCountry .select span")[id].textcontent = VehicleBrand;
//                $('input[value="' + VehicleType + '"][class="Vehicle"]').prop("selected", true);
//                $("#selVehicleType2").val(VehicleType);
//            //    //  pgta += gta[i] + ",";
//            //    var selected = [];
//            //}


//        }
//    }
//    catch (ex) { }
//}
function EditContact(ContactType) {
    window.location.href = "AddUpdateCustomerSupportDetails.aspx?ContactType=" + ContactType + "";
}
function GetAllCPContactList() {
    var Divs = '';
    $("#tbl_CPDetails").dataTable().fnClearTable();
    $("#tbl_CPDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../handler/VehicleMasterHandller.asmx/GetAllCPContactList",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            var List_Contact = obj.tbl_ContactList;
            if (obj.retCode == 1) {
                for (var i = 0; i < List_Contact.length; i++) {
                    Divs += '<tr>'
                    Divs += '<td>' + parseInt(parseInt(i) + parseInt(1)) + '</td>'
                    Divs += '<td>' + List_Contact[i].ContactType + '</td>'
                    Divs += '<td>' + List_Contact[i].Countryname + '</td>'
                    Divs += '<td>' + List_Contact[i].ContactNo + '</td>'
                    Divs += '<td>' + List_Contact[i].ContactEmail + '</td>'

                    //Divs += '<td align="center" style="width:10%"><i style="cursor:pointer" aria-hidden="true" title="Update Details"><label class="icon-pencil icon-size2" onclick="EditOffer(\'' + List_Contact[i].OfferID + '\')" ></label></i> | <i style="cursor:pointer" onclick="Delete(\'' + List_Contact[i].OfferID + '\',\'' + List_Contact[i].Sid + '\')" aria-hidden="true" title="Delete"><label class="icon-trash" ></label></i></td>'
                    Divs += '<td class="align-center">'
                    Divs += '<a href="#" title="Edit" class="button" style="" onclick="EditContact(\'' + List_Contact[i].ContactType + '\')"><span class="icon-pencil"></span></a>'
                    Divs += '<a  href="#" class="button" title="Delete" onclick="DeleteCustomerSupportDetails(\'' + List_Contact[i].sid + '\')"><span class="icon-trash"></span></a>'
                    Divs += '</td>'
                    Divs += '</tr>'
                }
            }
            $("#tbl_CPDetails tbody").append(Divs);
            $("#tbl_CPDetails").dataTable(
                {

                    bSort: true, sPaginationType: 'full_numbers',

                });

        },
    });
}
function SearchByContactType() {
    var ContactType = $("#txt_CoustomerSupportType2").val();
    var Divs = '';
    $("#tbl_CPDetails").dataTable().fnClearTable();
    $("#tbl_CPDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../handler/VehicleMasterHandller.asmx/GetContactDetailByContactType",
        data: '{"ContactType":"' + ContactType + '"}',
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            var List_Contact = obj.tbl_ContactList;
            if (obj.retCode == 1) {
                for (var i = 0; i < List_Contact.length; i++) {
                    Divs += '<tr>'
                    Divs += '<td>' + parseInt(parseInt(i) + parseInt(1)) + '</td>'
                    Divs += '<td>' + List_Contact[i].ContactType + '</td>'
                    Divs += '<td>' + List_Contact[i].Countryname + '</td>'
                    Divs += '<td>' + List_Contact[i].ContactNo + '</td>'
                    Divs += '<td>' + List_Contact[i].ContactEmail + '</td>'

                    //Divs += '<td align="center" style="width:10%"><i style="cursor:pointer" aria-hidden="true" title="Update Details"><label class="icon-pencil icon-size2" onclick="EditOffer(\'' + List_Contact[i].OfferID + '\')" ></label></i> | <i style="cursor:pointer" onclick="Delete(\'' + List_Contact[i].OfferID + '\',\'' + List_Contact[i].sid + '\')" aria-hidden="true" title="Delete"><label class="icon-trash" ></label></i></td>'
                    Divs += '<td class="align-center">'
                    Divs += '<a href="#" title="Edit" class="button" style="" onclick="EditContact(\'' + List_Contact[i].ContactType + '\')"><span class="icon-pencil"></span></a>'
                    Divs += '<a  href="#" class="button" title="Delete" onclick="DeleteCustomerSupportDetails(\'' + List_Contact[i].sid + '\')"><span class="icon-trash"></span></a>'
                    Divs += '</td>'
                    Divs += '</tr>'
                }
            }
            $("#tbl_CPDetails tbody").append(Divs);
            $("#tbl_CPDetails").dataTable(
                {

                    bSort: true, sPaginationType: 'full_numbers',

                });

        },
    });
}

//function AppendControls(Id) {
//    GetCountry()

//}





function DeleteCustomerSupportDetails(sid) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure you want to delete<br/> <span class=\"orange\">' + sid + '</span>?</span>?</p>',
        function () {
            $.ajax({
                url: "../Handler/VehicleMasterHandller.asmx/DeleteCustomerSupportDetails",
                type: "post",
                data: '{"sid":"' + sid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Contact has been deleted successfully!");
                        window.location.reload();
                    } else if (result.retCode == 0) {
                        Success("Something went wrong while processing your request! Please try again.");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}