﻿$(document).ready(function () {
    var Status = getParameterByName('Status');
    var type = getParameterByName('Type');
  //  BookingListAll();
    if (type != "") {
        BookingListFilter(Status, type);
    }
    else
        BookingListAll();
    $("#Check-In").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#Check-Out").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#Bookingdate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var BookingList;

function BookingListAll() {
    var columns = [
        {
            "mData": "ReservationDate",
            "sWidth":"7%",
        },
        {
            "mData": "AgencyName",
            "sWidth": "10%",
            "mRender": function (data, type, row, meta) {
                var ResrvationID = trimData(row.ReservationID);
                var trow = '<div style="text-align: center;">';
                if (row.Status == 'Cancelled') {
                    trow += '<a style="cursor:pointer" title="Booking Cancelled">' + ResrvationID + '</a><br><span class="status-sup-cancelled">Cancelled</span>';
                } else if (row.BookStatus == 'OnRequest') {
                    trow += '<a style="cursor:pointer" title="Booking On Request" onclick="ConfirmOnRequestBooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')">' + ResrvationID + ' </a><br><span class="status-sup-onrequest">On Request</span>';
                } else if (row.BookStatus == 'Rejected') {
                    trow += '<a style="cursor:pointer" title="Booking Rejected" >' + row.ReservationID + ' </a><br><span class="status-sup-rejected">Rejected</span>';
                } else if (row.Status == 'Onhold') {
                    trow += '<a style="cursor:pointer" title="Booking On Hold" onclick="ConfirmHoldBooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')">' + ResrvationID + ' </a><br><span class="status-sup-onhold">On Hold</span>';
                } else if (row.isReconfirm) {
                    trow += '<a style="cursor:pointer" title="Reconfirmation Pending" onclick="ConfirmHoldBooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')">' + ResrvationID + ' </a><br><span class="status-sup-reconfirmed">Reconfirmed</span>';
                } else {
                    trow += '<a style="cursor:pointer" title="Reconfirmation Pending" onclick="ReconfirmBooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')">' + ResrvationID + ' </a><br><span class="status-sup-confpending">Pending</span>';
                }
                trow += '<br/><span class="strong">' + row.Source + '</span>';
                return trow;
            },
        },
        {
            "sWidth": "10%",
            "mData": "AgencyName",
        },
        {
            "sWidth": "10%",
            "mData": "bookingname",
        },
        {
            "mData": "HotelName",
            "sWidth": "15%",
            "mRender": function (data, type, row, meta) {
                return '' + row.HotelName + '</br><b>' + row.City + '</b>';
            }
        },
        {
            "mData": "CheckIn",
            "mRender": function (data, type, row, meta) {
                return '<div style="text-align: center;">' + row.CheckIn + '<br> to </br>' + row.CheckOut + '</div>';
            }
        },
        {
            "mData": "TotalRooms",
            "sWidth": "6%",
        },
        {
            "mData": "CheckIn",
            "mRender": function (data, type, row, meta) {
                var trow = '';
                if (row.Status == 'Cancelled') {
                    trow += '<span class="status-cancelled">Cancelled</span>';
                } else if (row.Status == 'Vouchered') {
                    trow += '<span class="status-vouchered">Vouchered</span>';
                } else if (row.Status == 'onrequest') {
                    trow += '<span class="status-onrequest">On Request</span>';
                } else if (row.Status == 'onhold') {
                    trow += '<span class="status-onhold">On Hold</span>';
                } else {
                    trow += '<span class="status-rejected">Rejected</span>';
                }
                trow += '<br/><br/><div width="100%" class="vertical-center align-center no-padding  anthracite-bg">' + row.Deadline + '</div>'
                return trow;
            }, className: "vertical-center",
        },
        {
            "mData": "CheckIn",
            "sWidth": "7%",
            "mRender": function (data, type, row, meta) {
                return '<i class="' + GetCurrency(row.CurrencyCode) + '"></i> ' + numberWithCommas(row.TotalFare) + '</span>';
            }
        },
        {
            "mData": "CheckIn", "bSortable": false,
            "mRender": function (data, type, row, meta) {
                var trow = '';
              
                trow += '<a title = "Click for Voucher" class="voucher-btn"  onclick = "GetPrintVoucher(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.LatitudeMGH + '\',\'' + row.LongitudeMGH + '\',\'' + row.Status + '\',\'' + row.Source + '\')" >Voucher</a >';
                trow += '<a title = "Click for Invoice" class="invoice-btn"  onclick = "HotelPrintInvoice(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')" >Invoice</a >';

                if (row.Status != 'Cancelled') {
                    trow += '<a title = "Click to Modify Booking" class="modify-btn"  onclick = "Modifybooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.LatitudeMGH + '\',\'' + row.LongitudeMGH + '\',\'' + row.Status + '\',\'' + row.Source + '\')" >Modify</a >';
                    trow += '<a title = "Click to Cancel the Booking" class="cancel-btn" onclick="OpenCancellationModel(\'' + row.ReservationID + '\',\'' + row.Status + '\')">Cancel</a> ';
                }
                return trow;
            }
        },
    ];
    GetData("tbl_BookingList", [], 'Handler/BookingHandler.asmx/BookingList', columns);
}

function trimData(data) {
    try {
        data = $.trim(data).substring(0, 10)
    } catch (e) { }
    return data;
}

function BookingListFilter(status, type) {

    //$("#tbl_BookingList").dataTable().fnClearTable();
    //$("#tbl_BookingList").dataTable().fnDestroy();
    var data = {
        status: status,
        type: type
    }
    var columns = [
         {
             "mData": "ReservationDate",
            
         },
         {
             "mData": "AgencyName",
             "sWidth": "10%",
             "mRender": function (data, type, row, meta) {
                 var ResrvationID = trimData(row.ReservationID);
                 var trow = '<div style="text-align: center;">';
                 if (row.Status == 'Cancelled') {
                     trow += '<a style="cursor:pointer" title="Booking Cancelled">' + row.ReservationID + '</a><br><span class="status-sup-cancelled">Cancelled</span>';
                 } else if (row.BookStatus == 'OnRequest') {
                     trow += '<a style="cursor:pointer" title="Booking On Request" onclick="ConfirmOnRequestBooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')">' + row.ReservationID + ' </a><br><span class="status-sup-onrequest">On Request</span>';
                 } else if (row.BookStatus == 'Rejected') {
                     trow += '<a style="cursor:pointer" title="Booking Rejected" >' + row.ReservationID + ' </a><br><span class="status-sup-rejected">Rejected</span>';
                 } else if (row.Status == 'Onhold') {
                     trow += '<a style="cursor:pointer" title="Booking On Hold" onclick="ConfirmHoldBooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')">' + row.ReservationID + ' </a><br><span class="status-sup-onhold">On Hold</span>';
                 } else if (row.isReconfirm) {
                     trow += '<a style="cursor:pointer" title="Reconfirmation Pending" onclick="ConfirmHoldBooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')">' + row.ReservationID + ' </a><br><span class="status-sup-reconfirmed">Reconfirmed</span>';
                 } else {
                     trow += '<a style="cursor:pointer" title="Reconfirmation Pending" onclick="ReconfirmBooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')">' + row.ReservationID + ' </a><br><span class="status-sup-confpending">Pending</span>';
                 }
                 trow += '<br/><span class="strong">' + row.Source + '</span>';
                 return trow;
             },
         },
         {
             "sWidth": "10%",
             "mData": "AgencyName",
         },
         {
             "sWidth": "10%",
             "mData": "bookingname",
            
         },
         {
             "sWidth": "15%",
             "mData": "HotelName",
             "mRender": function (data, type, row, meta) {
                 return '' + row.HotelName + '</br><b>' + row.City + '</b>';
             }
         },
         {
             "mData": "CheckIn",
             "sWidth": "10%",
             "mRender": function (data, type, row, meta) {
                 return '<div style="text-align: center;">' + row.CheckIn + '<br> to </br>' + row.CheckOut + '</div>';
             }
         },
         {
             "mData": "TotalRooms",
             "sWidth": "6%",
         },
         {
             "mData": "CheckIn",
             "mRender": function (data, type, row, meta) {
                 var trow = '';
                 if (row.Status == 'Cancelled') {
                     trow += '<span class="status-cancelled">Cancelled</span>';
                 } else if (row.Status == 'Vouchered') {
                     trow += '<span class="status-vouchered">Vouchered</span>';
                 } else if (row.Status == 'onrequest') {
                     trow += '<span class="status-onrequest">On Request</span>';
                 } else if (row.Status == 'onhold') {
                     trow += '<span class="status-onhold">On Hold</span>';
                 } else {
                     trow += '<span class="status-rejected">Rejected</span>';
                 }
                 trow += '<br/><br/><div width="100%" class="vertical-center align-center no-padding  anthracite-bg">' + row.Deadline + '</div>'
                 return trow;
             }, className: "vertical-center",
         },
         {
             "mData": "CheckIn",
             "sWidth": "7%",
             "mRender": function (data, type, row, meta) {
                 return '<i class="' + GetCurrency(row.CurrencyCode) + '"></i> ' + numberWithCommas(row.TotalFare) + '</span>';
             }
         },
         {
             "mData": "CheckIn", "bSortable": false,
             "mRender": function (data, type, row, meta) {
                 var trow = '';
                 trow += '<a title = "Click for Voucher" class="voucher-btn"  onclick = "GetPrintVoucher(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.LatitudeMGH + '\',\'' + row.LongitudeMGH + '\',\'' + row.Status + '\',\'' + row.Source + '\')" >Voucher</a >';
                 trow += '<a title = "Click for Invoice" class="invoice-btn"  onclick = "HotelPrintInvoice(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')" >Invoice</a >';

                 if (row.Status != 'Cancelled') {
                     trow += '<a title = "Click to Modify Booking" class="modify-btn"  onclick = "Modifybooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.LatitudeMGH + '\',\'' + row.LongitudeMGH + '\',\'' + row.Status + '\',\'' + row.Source + '\')" >Modify</a >';
                     trow += '<a title = "Click to Cancel the Booking" class="cancel-btn" onclick="OpenCancellationModel(\'' + row.ReservationID + '\',\'' + row.Status + '\')">Cancel</a> ';
                 }
                 return trow;
             }
         },
    ];
    GetData("tbl_BookingList", data, 'Handler/BookingHandler.asmx/BookingListFilter', columns);
   // $("#tbl_BookingList").css("width", "100%");
}

function htmlGenrator() {
    // $("#tbl_BookingList").empty();
    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();
    var trow = '';
    for (var i = 0; i < BookingList.length; i++) {
        var NoOfPassenger = BookingList[i].NoOfAdults + BookingList[i].Children;
        trow += '<tr>';
        trow += '<td style="max-width:25px; vertical-align: middle;" class="center">' + (i + 1) + '</td>';
        trow += '<td style="min-width:60px; vertical-align: middle;" class="center">' + BookingList[i].ReservationDate + ' </td>';
        trow += '<td style="max-width: 115px; vertical-align: middle;" class="center">'
        if (BookingList[i].Status == 'Cancelled') {
            trow += '<a style="cursor:pointer" title="Booking Cancelled">' + BookingList[i].ReservationID + '</a><br><span class="status-sup-cancelled">Cancelled</span>';
        } else if (BookingList[i].BookStatus == 'OnRequest') {
            trow += '<a style="cursor:pointer" title="Booking On Request" onclick="ConfirmOnRequestBooking(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')">' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-onrequest">On Request</span>';
        } else if (BookingList[i].BookStatus == 'Rejected') {
            trow += '<a style="cursor:pointer" title="Booking Rejected" >' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-rejected">Rejected</span>';
        } else if (BookingList[i].Status == 'Onhold') {
            trow += '<a style="cursor:pointer" title="Booking On Hold" onclick="ConfirmHoldBooking(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')">' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-onhold">On Hold</span>';
        } else if (BookingList[i].isReconfirm){
            trow += '<a style="cursor:pointer" title="Reconfirmation Pending" onclick="ConfirmHoldBooking(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')">' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-reconfirmed">Reconfirmed</span>';
        } else{
            trow += '<a style="cursor:pointer" title="Reconfirmation Pending" onclick="ReconfirmBooking(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')">' + BookingList[i].ReservationID + ' </a><br><span class="status-sup-confpending">Pending</span>';
        }
        trow += '<br/><span class="strong">' + BookingList[i].Source+'</span></td>'
        trow += '<td style="vertical-align: middle; width: 100px;">' + BookingList[i].AgencyName + ' </td>';
        trow += '<td style="vertical-align: middle;width: 100px;">' + BookingList[i].bookingname + '</td>';
        trow += '<td style="min-width: 100px; vertical-align: middle;">' + BookingList[i].HotelName + '</br><b>' + BookingList[i].City + '</b></td>';
        trow += '<td style="min-width: 60px; vertical-align: middle;" class="center">' + BookingList[i].CheckIn + '<br> to </br>' + BookingList[i].CheckOut + ' </td>';
        /* trow += '<td style="min-width: 60px; vertical-align: middle;" class="center">' + BookingList[i].CheckOut + ' </td>';*/
        trow += '<td style="max-width: 20px; vertical-align: middle;" class="center">' + BookingList[i].TotalRooms + ' </td>';
        if (BookingList[i].Status == 'Cancelled') {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-cancelled">Cancelled</span></td>';
        } else if (BookingList[i].Status == 'Vouchered') {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-vouchered">Vouchered</span></td>';
        } else if (BookingList[i].Status == 'onrequest') {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-onrequest">On Request</span></td>';
        } else if (BookingList[i].Status == 'onhold') {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-onhold">On Hold</span></td>';
        } else {
            trow += '<td style="max-width:70px; vertical-align: middle;" class="center"><span class="status-rejected">Rejected</span></td>';
        }

        trow += '<td style="vertical-align: middle; min-width: 60px;" class="align-right"><span class="strong"><i class="' + GetCurrency(BookingList[i].CurrencyCode) + '"></i> ' + numberWithCommas(BookingList[i].TotalFare) + '</span></td>';
        /* trow += '<td style="width:3%" class="align-center"><a style="cursor:pointer" title="Invoice" class="button"  onclick="GetPrintInvoice(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')"> <span class="icon-cc-share tracked"></span></a></td>';*/
        trow += '<td style="max-width: 55px; vertical-align: middle;" class="align-center action-button">';
        trow += '<a title = "Click for Voucher" class="voucher-btn"  onclick = "GetPrintVoucher(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].LatitudeMGH + '\',\'' + BookingList[i].LongitudeMGH + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')" >Voucher</a >';
        trow += '<a title = "Click for Invoice" class="invoice-btn"  onclick = "HotelPrintInvoice(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')" >Invoice</a >';

        if (BookingList[i].Status != 'Cancelled') {
            trow += '<a title = "Click to Modify Booking" class="modify-btn"  onclick = "Modifybooking(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Uid + '\',\'' + BookingList[i].LatitudeMGH + '\',\'' + BookingList[i].LongitudeMGH + '\',\'' + BookingList[i].Status + '\',\'' + BookingList[i].Source + '\')" >Modify</a >';
            trow += '<a title = "Click to Cancel the Booking" class="cancel-btn" onclick="OpenCancellationModel(\'' + BookingList[i].ReservationID + '\',\'' + BookingList[i].Status + '\')">Cancel</a> ';
        }
        trow += ' </td>';
        trow += '</tr>';
    }
    trow += '</tbody>'
    $("#tbl_BookingList").append(trow);
    $('[data-toggle="tooltip"]').tooltip()
    $("#tbl_BookingList").dataTable({
        bSort: false, sPaginationType: 'full_numbers',
    });
    $("#tbl_BookingList").removeAttr("style");
}

function SearchBooking() {

    $("#tbl_BookingList").dataTable().fnClearTable();
    $("#tbl_BookingList").dataTable().fnDestroy();

    var CheckIn = $("#Check-In").val();
    var CheckOut = $("#Check-Out").val();
    var Passenger = $("#txt_Passenger").val();
    var BookingDate = $("#Bookingdate").val();
    var Reference = $("#txt_Reference").val();
    var HotelName = $("#txt_Hotel").val();
    var Location = $("#txt_Location").val();
    var ReservationStatus = $("#selReservation option:selected").val();

    var data = {
        CheckIn: CheckIn,
        CheckOut: CheckOut,
        Passenger: Passenger,
        BookingDate: BookingDate,
        Reference: Reference,
        HotelName: HotelName,
        Location: Location,
        ReservationStatus: ReservationStatus
    }
    var columns = [
        {
            "mData": "ReservationDate",
        },
        {
            "mData": "AgencyName",
            "mRender": function (data, type, row, meta) {
                var trow = '<div style="text-align: center;">';
                if (row.Status == 'Cancelled') {
                    trow += '<a style="cursor:pointer" title="Booking Cancelled">' + row.ReservationID + '</a><br><span class="status-sup-cancelled">Cancelled</span>';
                } else if (row.BookStatus == 'OnRequest') {
                    trow += '<a style="cursor:pointer" title="Booking On Request" onclick="ConfirmOnRequestBooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')">' + row.ReservationID + ' </a><br><span class="status-sup-onrequest">On Request</span>';
                } else if (row.BookStatus == 'Rejected') {
                    trow += '<a style="cursor:pointer" title="Booking Rejected" >' + row.ReservationID + ' </a><br><span class="status-sup-rejected">Rejected</span>';
                } else if (row.Status == 'Onhold') {
                    trow += '<a style="cursor:pointer" title="Booking On Hold" onclick="ConfirmHoldBooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')">' + row.ReservationID + ' </a><br><span class="status-sup-onhold">On Hold</span>';
                } else if (row.isReconfirm) {
                    trow += '<a style="cursor:pointer" title="Reconfirmation Pending" onclick="ConfirmHoldBooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')">' + row.ReservationID + ' </a><br><span class="status-sup-reconfirmed">Reconfirmed</span>';
                } else {
                    trow += '<a style="cursor:pointer" title="Reconfirmation Pending" onclick="ReconfirmBooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')">' + row.ReservationID + ' </a><br><span class="status-sup-confpending">Pending</span>';
                }
                trow += '<br/><span class="strong">' + row.Source + '</span>';
                return trow;
            },
        },
        {
            "mData": "AgencyName",
        },
        {
            "mData": "bookingname",
        },
        {
            "mData": "HotelName",
            "mRender": function (data, type, row, meta) {
                return '' + row.HotelName + '</br><b>' + row.City + '</b>';
            }
        },
        {
            "mData": "CheckIn",
            "mRender": function (data, type, row, meta) {
                return '<div style="text-align: center;">' + row.CheckIn + '<br> to </br>' + row.CheckOut + '</div>';
            }
        },
        {
            "mData": "TotalRooms",
        },
        {
            "mData": "CheckIn",
            "mRender": function (data, type, row, meta) {
                var trow = '';
                if (row.Status == 'Cancelled') {
                    trow += '<span class="status-cancelled">Cancelled</span>';
                } else if (row.Status == 'Vouchered') {
                    trow += '<span class="status-vouchered">Vouchered</span>';
                } else if (row.Status == 'onrequest') {
                    trow += '<span class="status-onrequest">On Request</span>';
                } else if (row.Status == 'onhold') {
                    trow += '<span class="status-onhold">On Hold</span>';
                } else {
                    trow += '<span class="status-rejected">Rejected</span>';
                }
                trow += '<br/><div width="100%" class="vertical-center align-center no-padding  anthracite-bg">' + row.Deadline + '</div>'
                return trow;
            }, className: "vertical-center",
        },
        {
            "mData": "CheckIn",
            "mRender": function (data, type, row, meta) {
                return '<i class="' + GetCurrency(row.CurrencyCode) + '"></i> ' + numberWithCommas(row.TotalFare) + '</span>';
            }
        },
        {
            "mData": "CheckIn", "bSortable": false,
            "mRender": function (data, type, row, meta) {
                var trow = '';
                trow += '<a title = "Click for Voucher" class="voucher-btn"  onclick = "GetPrintVoucher(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.LatitudeMGH + '\',\'' + row.LongitudeMGH + '\',\'' + row.Status + '\',\'' + row.Source + '\')" >Voucher</a >';
                trow += '<a title = "Click for Invoice" class="invoice-btn"  onclick = "HotelPrintInvoice(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.Status + '\',\'' + row.Source + '\')" >Invoice</a >';

                if (row.Status != 'Cancelled') {
                    trow += '<a title = "Click to Modify Booking" class="modify-btn"  onclick = "Modifybooking(\'' + row.ReservationID + '\',\'' + row.Uid + '\',\'' + row.LatitudeMGH + '\',\'' + row.LongitudeMGH + '\',\'' + row.Status + '\',\'' + row.Source + '\')" >Modify</a >';
                    trow += '<a title = "Click to Cancel the Booking" class="cancel-btn" onclick="OpenCancellationModel(\'' + row.ReservationID + '\',\'' + row.Status + '\')">Cancel</a> ';
                }
                return trow;
            }
        },
    ];
    GetData("tbl_BookingList", data, 'Handler/BookingHandler.asmx/Search', columns);
}

function Reset() {
    $("#Check-In").val('');
    $("#Check-Out").val('');
    $("#txt_Passenger").val('');
    $("#Bookingdate").val('');
    $("#txt_Reference").val('');
    $("#txt_Hotel").val('');
    $("#txt_Location").val('');
    $("#selReservation").val('All');
}

function numberWithCommas(x) {
    if (x != null) {
        var sValue = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var retValue = sValue.split(".");
        return retValue[0];
    }
    else
        return 0;
}

function ExportBookingDetailsToExcel(Document) {
    var CheckIn = $("#Check-In").val();
    var CheckOut = $("#Check-Out").val();
    var Passenger = $("#txt_Passenger").val();
    var BookingDate = $("#Bookingdate").val();
    var Reference = $("#txt_Reference").val();
    var HotelName = $("#txt_Hotel").val();
    var Location = $("#txt_Location").val();
    var ReservationStatus = $("#selReservation option:selected").val();
    var Type = "All";
    window.location.href = "../Handler/ExportToExcelHandler.ashx?datatable=SupplierBookingDetails&Type=" + Type + "&Document=" + Document + "&CheckIn=" + CheckIn + "&CheckOut=" + CheckOut + "&Passenger=" + Passenger + "&BookingDate=" + BookingDate + "&Reference=" + Reference + "&HotelName=" + HotelName + "&Location=" + Location + "&ReservationStatus=" + ReservationStatus;
}

function ReconfirmBooking(ReservationID, Uid, Status, Source) {
    var data = {
        ReservationID: ReservationID
    }
    post("../handler/BookingHandler.asmx/GetBookingDetail", data, function (result) {
        var Cancle = "";
        var Cancleamnt = "";
        var Policy = "";
        var Detail = result.HotelReservation;
        $.modal({
            content:
                '<div class="modal-body">' +
                '' +
                '<table class="table table-hover table-responsive" id="tbl_Confirmation" style="width: 100%">' +
                '<tr>' +
                '<h4>Booking Detail</h4>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<span class="text-left">Hotel:&nbsp;&nbsp;' + Detail[0].HotelName + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">CheckIn:&nbsp;&nbsp;' + Detail[0].CheckIn + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">CheckOut:&nbsp;&nbsp;' + Detail[0].CheckOut + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<span class="text-left">Passenger: &nbsp;&nbsp;' + Detail[0].bookingname + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">Location:&nbsp;&nbsp; ' + Detail[0].City + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">Nights:&nbsp;&nbsp; ' + Detail[0].NoOfDays + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<span class="text-left">Booking Id:&nbsp;&nbsp; ' + ReservationID + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">Booiking Date:&nbsp;&nbsp; ' + Detail[0].ReservationDate + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">Amount:&nbsp;&nbsp;' + Detail[0].TotalFare + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '</tr>' +
                '</table>' +


                '<table class="table table-hover table-responsive" style="width: 100%">' +
                '<tr>' +
                '<h4>Re-Confirmation Detail</h4>' +
                '</tr>' +

                '<tr>' +
                '<td>' +
                '<span class="text-left">Date :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" placeholder="dd-mm-yyyy" id="ConfirmDate" class="input mySelectCalendar" ></span> ' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">Hotel Confirmation No :&nbsp;&nbsp;<input type="text" id="HotelConfirmationNo" class="input" ></span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '</tr>' +

                '<tr>' +
                '<td>' +
                '<span class="text-left">Staff Name :&nbsp;&nbsp; <input type="text" id="StaffName" class="input" > </span> ' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">Reconfirm Through :&nbsp;&nbsp;&nbsp;&nbsp; <select id="ReconfirmThrough" class="select"><option selected="selected" value="-">Select Reconfirm Through</option><option value="Mail">Mail</option><option value="Phone">Phone</option><option value="Whatsapp">Whatsapp</option></select></span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '</tr>' +
                '</table>' +

                '<table class="table table-hover table-responsive"  style="width: 100%,margin-top:5%">' +
                '<tr>' +
                '<td style="border-bottom: none;" >' +
                '<span class="text-left">Comment :  <input type="text" id="Comment"  style="width: 95%" class="input" > </span> ' +
                '' +
                '</td>' +
                '</tr>' +
                '</table>' +

                '<br/><input id="btn_ReconfirmBooking" type="button" value="Submit" class="button anthracite-gradient" style="width: 20%; float:right" onclick="SaveConfirmDetail(\'' + ReservationID + '\',\'' + Status + '\',\'' + Detail[0].HotelName + '\');" />' +

                '</div>',
            title: 'Re-confirm Booking',
            width: 700,
            height: 400,
            scrolling: true,
            actions: {
                'Close': {
                    color: 'red',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttons: {
                'Close': {
                    classes: 'huge anthracite-gradient displayNone',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttonsLowPadding: true

        });
        $('#ConfirmDate').glDatePicker({
            zIndex: 999300,
            allowMonthSelect: true,
            allowYearSelect: true,
            todayDate: new Date(),
            position: 'inherit',
            monthNames: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
            onClick: (function (el, cell, date, data) {
                var armco = date.toLocaleDateString();
                armco = toDate(armco);
                el.val(armco);
            })
        });
    }, function (error) {
        if (error.ex != undefined)
            AlertDanger(error.ex)
        else
            AlertDanger("Server not responding");
    })
}


function SaveConfirmDetail(ReservationID, status, HotelName) {
    var ConfirmDate = $("#ConfirmDate").val();
    if (ConfirmDate == "") {
        Success('Please Enter Confirm Date.');
        return false;
    }

    var HotelConfirmationNo = $("#HotelConfirmationNo").val();
    if (HotelConfirmationNo == "") {
        Success('Please Enter Hotel Confirmation No.');
        return false;
    }

    var StaffName = $("#StaffName").val();
    if (StaffName == "") {
        Success("Please Enter Staff Name");
        return false;
    }

    var ReconfirmThrough = $("#ReconfirmThrough option:selected").val();
    if (ReconfirmThrough == "-") {
        Success('Please Select Reconfirm Through.');
        return false;
    }

    var Comment = $("#Comment").val();

    var data = {
        HotelName: HotelName,
        ReservationId: ReservationID,
        ConfirmDate: ConfirmDate,
        StaffName: StaffName,
        ReconfirmThrough: ReconfirmThrough,
        HotelConfirmationNo: HotelConfirmationNo,
        Comment: Comment,
    }

    post("../handler/BookingHandler.asmx/SaveConfirmDetail", data, function () {
        Success("Confirm Detail Save.");
        BookingListAll();
    }, function() {
        AlertDanger("Something went wrong");
    })
}

function ReConfirmBooking(ReservationID, Uid, Status, Source) {
    var data = {
        ReservationID: ReservationID
    }
    post("../handler/BookingHandler.asmx/GetDetail", data, function () {
        var Detail = result.Detail;
        var Cancle = "";
        var Cancleamnt = "";
        var Policy = "";
        $.modal({
            content:

                '<div class="modal-body">' +
                '' +
                '<table class="table table-hover table-responsive" id="tbl_Confirmation" style="width: 100%">' +
                '<tr>' +
                '<h4>Booking Detail</h4>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<span class="text-left">Hotel:&nbsp;&nbsp;' + Detail[0].HotelName + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">CheckIn:&nbsp;&nbsp;' + Detail[0].CheckIn + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">CheckOut:&nbsp;&nbsp;' + Detail[0].CheckOut + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<span class="text-left">Passenger: &nbsp;&nbsp;' + Detail[0].bookingname + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">Location:&nbsp;&nbsp; ' + Detail[0].City + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">Nights:&nbsp;&nbsp; ' + Detail[0].NoOfDays + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<span class="text-left">Booking Id:&nbsp;&nbsp; ' + Detail[0].ReservationID + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">Booiking Date:&nbsp;&nbsp; ' + Detail[0].ReservationDate + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">Amount:&nbsp;&nbsp;' + Detail[0].TotalFare + '</span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '</tr>' +
                '</table>' +


                '<table class="table table-hover table-responsive" style="width: 100%">' +
                '<tr>' +
                '<h4>Re-Confirmation Detail</h4>' +
                '</tr>' +

                '<tr>' +
                '<td>' +
                '<span class="text-left">Date :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" style="float:right" placeholder="dd-mm-yyyy" id="ConfirmDate" class="input mySelectCalendar" ></span> ' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">Hotel Confirmation No :&nbsp;&nbsp;<input style="float:right" type="text" id="HotelConfirmationNo" class="input" ></span>&nbsp;&nbsp;' +
                '' +
                '</td>' +
                '</tr>' +

                '<tr>' +
                '<td>' +
                '<span class="text-left">Hotel Staff Name :&nbsp;&nbsp; <input style="float:right" type="text" id="StaffName" class="input" > </span> ' +
                '' +
                '</td>' +
                '<td>' +
                '<span class="text-left">Reconfirm Through :&nbsp;&nbsp;&nbsp;&nbsp; <select style="float:right" id="ReconfirmThrough" class="select"><option selected="selected" value="-">Select Reconfirm Through</option><option value="Mail">Mail</option><option value="Phone">Phone</option><option value="Whatsapp">Whatsapp</option></select></span>' +
                '' +
                '</td>' +
                '</tr>' +
                '</table>' +

                '<table class="table table-hover table-responsive"  style="width: 100%,margin-top:5%">' +
                '<tr>' +
                '<td style="border-bottom: none;" >' +
                '<span class="text-left">Comment :  <input  type="text" id="Comment"  style="width: 95%" class="input" > </span> ' +
                '' +
                '</td>' +
                '</tr>' +
                '</table>' +

                '<br/><input id="btn_ReconfirmBooking" type="button" value="Submit" class="button anthracite-gradient" style="width: 20%; float:right" onclick="SaveConfirmDetail(\'' + ReservationID + '\',\'' + Status + '\',\'' + Detail[0].HotelName + '\');" />' +

                '</div>',
            title: 'Re-confirm Booking',
            width: 700,
            height: 400,
            scrolling: true,
            actions: {
                'Close': {
                    color: 'red',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttons: {
                'Close': {
                    classes: 'huge anthracite-gradient displayNone',
                    click: function (win) { win.closeModal(); }
                }
            },
            buttonsLowPadding: true

        });
        $('#ConfirmDate').glDatePicker({
            zIndex: 999300,
            allowMonthSelect: true,
            allowYearSelect: true,
            todayDate: new Date(),
            position: 'inherit',
            monthNames: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
            onClick: (function (el, cell, date, data) {
                var armco = date.toLocaleDateString();
                armco = toDate(armco);
                el.val(armco);
            })
        });
    }, function() {
    })
}

