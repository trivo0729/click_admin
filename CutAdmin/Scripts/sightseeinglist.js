﻿$(document).ready(function () {
    $("#frm_Inventory").validationEngine();
    GetHotelList()
});

function GetHotelList() {

    $("#tbl_Actlist").dataTable().fnClearTable();
    $("#tbl_Actlist").dataTable().fnDestroy();
   
    $.ajax({
        url: "../Handler/ActivityHandller.asmx/ActList",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 1) {
                arrActivity = result.arrActivity
                for (var i = 0; i < arrActivity.length; i++) {
                    var html = '';
                    html += '<tr><td style="width:3%" align="center">' + (i + 1) + '</td>'
                    html += '<td style="width:20%"  align="center">' + arrActivity[i].Name + ' </td>'
                    html += '<td style="width:10%"  align="center">' + arrActivity[i].City + ' ,' + arrActivity[i].Country + ' </td>'
                    html += '<td style="width:30%"  align="center">'
                    for (var j = 0; j < arrActivity[i].TourType.length; j++) {
                        html += arrActivity[i].TourType[j] + " ";
                    }
                    if (arrActivity[i].Mode.length == 0)
                        html += '-'
                    html += '</td>'

                    html += '<td style="width:10%" align="center">'
                    for (var j = 0; j < arrActivity[i].Mode.length; j++) {
                        html += arrActivity[i].Mode[j].Type.replace('TKT', '<img src="../fonts/glyphicons_free/glyphicons/png/glyphicons-67-tags.png" style="height: 15px;" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="Ticket Only"> ').replace('SIC', '<img src="../fonts/glyphicons_free/glyphicons/png/glyphicons-44-group.png" style="height: 15px;" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="Sharing"> ').replace('PVT', '<img src="../fonts/glyphicons_free/glyphicons/png/glyphicons-6-car.png" alt="" style="height: 15px;" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Private"> ');
                    }
                    if (arrActivity[i].Mode.length == 0)
                        html += '-'
                    html += '</td>'
                    var checked = '';
                    if (arrActivity[i].Status == "True")
                        checked = 'checked';
                    html += '<td class="align-center" id="' + arrActivity[i].Aid + '"><input type="checkbox" id="chk' + arrActivity[i].Aid + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"  ' + checked + '   onclick="RedirectStatus(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Status + '\')"></td>';
                    html += '<td style="width:25%" align="center"><span class="button-group"><a class="button" style="cursor:pointer" href="AddActivity.aspx?id=' + arrActivity[i].Aid + '"><span class="icon-pencil icon-size2" title="Edit"></span></a><a class="button" style="cursor:pointer" onclick="Delete(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Name + '\')"><span class="icon-trash" title="Delete"></span></a></span></td>'
                    html += '<td style="width:10%" align="center"><a class="button" style="cursor:pointer" onclick="Rates(\'' + arrActivity[i].Aid + '\',\'' + arrActivity[i].Name + '\',\'' + arrActivity[i].Location + '\',\'' + arrActivity[i].City + '\',\'' + arrActivity[i].Country + '\')"><span class="icon-pencil icon-size2" title="Edit Rates"></span></a></td></tr>'
                    $("#tbl_Actlist tbody").append(html);
                }
                $(".tiny").click(function () {
                    $(this).find("input:checkbox").click();
                })
                GenrateTable()
                $("#tbl_Actlist").dataTable(
                    {
                        sSort: true, sPaginationType: 'full_numbers',
                    });
                $("#tbl_Actlist").removeAttr("style");
                }
            else if (result.retCode == 0) {
                $("#tbl_Actlist").dataTable(
                    {
                        bSort: false, sPaginationType: 'full_numbers',
                    });
                $("#tbl_Actlist").removeAttr("style");
            }
            }
    });
}

function GenrateTable() {
    $.template.init();
    $('#tbl_Actlist').tablesorter({
        headers: {
            0: { sorter: false },
            6: { sorter: false }
        }
    }).on('click', 'tbody td', function (event) {
        var ID = this.parentNode.id;
        if (ID == "")
            return true;
        if (event.target !== this) {
            return;
        }
        var tr = $(this).parent(),
            row = tr.next('.row-drop'),
            rows;
        if (tr.hasClass('row-drop')) {
            return;
        }
        if (row.length > 0) {
            tr.children().removeClass('anthracite-gradient glossy');
            row.remove();
            return;
        }
        rows = tr.siblings('.row-drop');
        if (rows.length > 0) {
            rows.prev().children().removeClass('anthracite-gradient glossy');
            rows.remove();
        }
        // Add fake row
        $('<tr  class="row-drop">' +
            '<td  colspan="' + tr.children().length + '">' +
            '<div class="columns">' +
            '<div id="DropLeft" class="four-columns"> </div>' +
            '<div id="Dropmiddle" class="three-columns align-center"> </div>' +
            '<div id="DropRight" class="five-columns"> </div>' +
            '</div>' +
            '</td>' +
            '</tr>').insertAfter(tr);
        DropRowwithId(ID)
    }).on('sortStart', function () {
        var rows = $(this).find('.row-drop');
        if (rows.length > 0) {
            rows.prev().children().removeClass('anthracite-gradient glossy');
            rows.remove();
        }
    });
}

function DropRowwithId(ID) {
    var trDropleft = "";
    var trDropmiddle = "";
    var trDropRight = "";
    $.ajax({
        type: "POST",
        url: "../handler/RoomHandler.asmx/GetRooms",
        data: '{"sHotelId":"' + ID + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrRoomDetails = result.RoomList;
                var HotelAddress = $.grep(arrHotellist, function (p) { return p.sid == ID; })
                    .map(function (p) { return p.HotelAddress; });
                var HotelCity = $.grep(arrHotellist, function (p) { return p.sid == ID; })
                    .map(function (p) { return p.CityId; });
                var HotelCountry = $.grep(arrHotellist, function (p) { return p.sid == ID; })
                    .map(function (p) { return p.CountryId; }); trDropRight = "";
                var HotelName = $.grep(arrHotellist, function (p) { return p.sid == ID; })
                    .map(function (p) { return p.HotelName; }); trDropRight = "";
                trDropRight = "";
                //Left
                trDropleft += '<label class="font11 silver">' + HotelAddress + '<br>' + HotelCity + ',' + HotelCountry + '&nbsp;&nbsp;&nbsp;<a onclick="GetMap(\'' + ID + '\')" class="icon-marker icon-size2 font11 silver" title="Map"></a></label>';
                if (arrRoomDetails.length > 0) {
                    for (var i = 0; i < arrRoomDetails.length; i++) {
                        trDropRight += '<table class="room-table">';
                        trDropRight += '<tbody>'
                        trDropRight += '<tr class="silver">';
                        trDropRight += '<td style="width:175px;" class="with-small-padding">' + arrRoomDetails[i].RoomType + '</td>';
                        trDropRight += '<td style="width:50px;" class="with-small-padding"> ' + arrRoomDetails[i].RoomOccupancy + '</td>';
                        if (arrRoomDetails[i].Approved == true) {
                            trDropRight += '<td style="width:50px;" class="with-small-padding"> <input type="checkbox" class="switch mini" checked  onclick="ActivateRoom(\'' + ID + '\',\'' + arrRoomDetails[i].RoomId + '\',\'False\')"></td>';
                        }
                        else {
                            trDropRight += '<td style="width:50px;" class="with-small-padding"> <input type="checkbox" class="switch mini" onclick="ActivateRoom(\'' + ID + '\',\'' + arrRoomDetails[i].RoomId + '\',\'True\')"></td>';
                        }
                        trDropRight += '<td class="with-small-padding"><div class="font12"><a href="AddRooms.aspx?sHotelID=' + ID + '&HotelName=' + HotelName + '&RoomId=' + arrRoomDetails[i].RoomId + '" class="silver icon-pencil" title="Edit Room Details"></a>';
                        trDropRight += '<a   class="silver pointer icon-cart mid-margin-left Inventory" onclick="SetInventory(\'' + ID + '\',\'' + HotelName + '-' + HotelAddress + ',' + HotelCity + ', ' + HotelCountry + '\',\'' + arrRoomDetails[i].RoomId + '\',\'' + arrRoomDetails[i].RoomType + '\')" title="Add Inventory"></a>';
                        trDropRight += '<a href="#" class=" silver icon-trash mid-margin-left" onclick="DeleteRoomModal(\'' + ID + '\',\'' + arrRoomDetails[i].RoomId + '\',\'' + arrRoomDetails[i].RoomType + '\')" title="Delete Room"></a>';
                        trDropRight += '<a href="roomrate.aspx?sHotelID=' + ID + '&HotelName=' + HotelName + '&RoomId=' + arrRoomDetails[i].RoomTypeID + '&RoomType=' + arrRoomDetails[i].RoomType + '" class="silver fa fa-money mid-margin-left" title="Add Rates"></a>';
                        trDropRight += '</div></td></tr></tbody></table>';
                    }
                    trDropRight += '<a target="_blank" href="AddRooms.aspx?HotelCode=' + ID + '&HotelName=' + HotelName + '" class="mid-margin-left font11 white pointer">Add room</a> ';
                    trDropRight += ' |<a target="_blank" href="Ratelist.aspx?sHotelID=' + ID + '&HName=' + HotelName + '" class="mid-margin-left font11 white pointer">View Rates</a>';
                    trDropRight += ' |<a onclick="AddInventory(\'' + ID + '\',\'' + '0' + '\',\'' + HotelName + '\',\'' + HotelAddress + '\')" class="mid-margin-left font11 white pointer">Add Inventory</a>';
                }
                else {
                    trDropRight += '<span>No Room Available </span><a target="_blank" href="AddRooms.aspx?HotelCode=' + ID + '&HotelName=' + HotelName + '" class="mid-margin-left font11 white">Add room</a>';
                }
                $('#DropLeft').append(trDropleft);
                $('#Dropmiddle').append(trDropmiddle);
                $('#DropRight').append(trDropRight);
                $('.Inventory').menuTooltip($('#Inventoryfilter').hide(), {
                    classes: ['with-small-padding', 'full-width', 'anthracite-gradient']
                });
            }
            $(".mini").click(function () {
                $(this).find("input:checkbox").click();
            })
        }
    });
}