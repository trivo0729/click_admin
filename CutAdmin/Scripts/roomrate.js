﻿var HotelCode = 0;
var RoomTypeID = 0;
var HotelName = "";
var sRoom_Type = "";
$(function () {
    GetSupplier("Hotel");
    GetRateAllType();
    HotelCode = GetQueryStringParams("sHotelID").replace(/%20/g, ' ');
    RoomTypeID = GetQueryStringParams("RoomId");
    HotelName = GetQueryStringParams('HotelName').replace(/%20/g, ' ')
    sRoom_Type = GetQueryStringParams('RoomType').replace(/%20/g, ' ')
    var form = $('.wizard');
    GetMealPlan();
    GenrateDates('GN')
    GenrateDates('SP')
    GetCountry();
    $("#lblHotel").text(HotelName)
    $("#lblHotel").append('<br><span class="red" id="lblroom">' + sRoom_Type + '</span>');
    $(".radio").click(function () {
        $(this).find("input:checkbox").click();
    });
    $(".SPCancel").click(function () {
        $(this).find("input:checkbox").click();
    })
    $('.wizard fieldset').on('wizardleave', function () {
        // Called everytime a step (fieldset) stops being the active one
        var sStep = $(this).find("legend").text();
        PreviewRate();
    });;
});
function GetCountry() {
    try
    {
        post("GenralHandler.asmx/GetCountry", {}, function(data) {
            $(data.Country).each(function (index, arrCountry) {
                if (index == 0)
                    $('#sel_Country').append($('<option selected="selected"></option>').val("All Market").html("All Market"));
                else
                    $('#sel_Country').append($('<option></option>').val(arrCountry.Country).html(arrCountry.Countryname));
                $('#sel_Country').change();
            });
        }, function(errordata) {
            alertDanger(errordata.ex)
        })
    }catch(e){}
}

var arrMealPlan = new Array();
var arrMeals = new Array();
function GetMealPlan() {
    try {
        $("#div_Meals").append('<small class="input-info">Select for which meal plans you wish to add/update rates</small>' +
            '<span class= "label" > <b>Meal Plan</b></span>');
        post("../handler/GenralHandler.asmx/GetMealPlans", {}, function (data) {
            arrMealPlan = data.arrMealPlan
            arrMeals = data.arrMeals
            $(data.arrMealPlan).each(function (index, MealPlan) {
                $("#div_Meals").append('<input type="checkbox" name="roomonly" id="chk_' + MealPlan.ID + '" value="' + MealPlan.Meal_Plan + '" class="validate[required]" onclick="AddTarrifRate(\'' + MealPlan.ID + '\',this)">' +
                    '<label for="chk_' + MealPlan.ID + '"> ' + MealPlan.Meal_Plan + '</label> ');
                /*TarrifPlan('GN', MealPlan.ID, index);
                GetTab(index, MealPlan.Meal_Plan, MealPlan.ID,"GN");*/
            });
            $('.checkbox').enableInput();
            GetMeals();
        }, function(error) {
            AlertDanger("server not responds..")
        })

    } catch (e) {
        AlertDanger("server not responds..")
    }
} /*Meal Plan Master*/

function GetTab(index, MealPlan, PlanID,RateType) {
    try {
        var CSS = '"active"';
        if (index)
            CSS = ''
        $("#ul_" + RateType).append('<li class="' + CSS + ' li_' + RateType+'" id="li_' + RateType + '_' + PlanID + '"><a class="active strong align-center" href="#div_tab' + RateType + '_' + PlanID + '">' + MealPlan + '</a></li>')  

    } catch (e) {

    }
}/*Tarrif Tab */

function TarrifPlan(RateType ,MealPlanID,index) {
    var html = '';
    var css = '';
    if (index != 0)
        css = 'class="with-padding"';
    else
        css = 'style="height: auto !important;"';
    html += '<div id="div_tab' + RateType + '_' + MealPlanID +'" >' + 
        '<div class="with-padding" style = "padding-bottom: 0px !important;" >'+
        '<small class="input-info">Cost Price is to be entered here</small>'+
        '<div class="columns">' +
         /*Room Tariff*/
        '<div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">' +
        '<label class="">Room Tariff Rate </label>' +
        '<span class="input tariff-textbox">' +
        '<label style="width: 30px" for="roomtariff" class="button field-drop-input green-gradient sCurrency"></label>' +
        '<input style="width: 60px" type="text" name="RoomTariff" id="txt_tariffRate_' + RateType + '_' + MealPlanID + '" onchange="AutoSetRate(this.value,\'' + RateType + '\',true,\'' + MealPlanID + '\')" class="input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>'+
        '</div>' +

       
         /*Extra Bed*/
        '<div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">'+
        '<label class="">Extra Bed Rate </label>'+
        '<span class="input tariff-textbox">'+
        '<label style="width: 30px" for="extrabed" class="button field-drop-input green-gradient sCurrency"></label>' +
        '<input style="width: 60px" type="text" name="Extrabed" id="txt_extrabedRate_' + RateType + '_' + MealPlanID + '" class="input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>'+
        '</div>' +

        /*Child With Bed*/
        '<div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">'+
        '<label class="">Child With Bed</label>'+
        '<span class="input tariff-textbox">'+
        '<label style="width: 30px" for="cwb" class="button field-drop-input green-gradient sCurrency"></label>' +
        '<input style="width: 60px" type="text" name="Child-with-bed" id="txt_cwbrate_' + RateType + '_' + MealPlanID + '" class="input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>'+
        '</div>' +

         /*Child With Bed*/
        '<div class="three-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">'+
        '<label class="">Child No Bed </label>'+
        '<span class="input tariff-textbox">'+
        '<label style="width: 30px" for="cnb" class="button field-drop-input green-gradient sCurrency"></label>' +
        '<input style="width: 60px" type="text" name="Child-no-bed" id="txt_cnbrate_' + RateType + '_' + MealPlanID + '" class="input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>'+
        '</div>' +

        '</div>' +

        /*Day Wise Rates*/
        '<div class="row">' +
        '<small><a class="pointer gray strong" onclick="showdaywise(\'' + RateType + '_' + MealPlanID + '\')">Daywise Tariff <input type="checkbox" id="chk_day'+RateType + '_' + MealPlanID +'" style="display:none"/> </a></small>' +
        '</div>'+
        '</div>'+
      
        '<div class="silver-bg field-drop-tariff" id="ro-daywise' + RateType + '_' + MealPlanID + '" style="display: none;">'+
        '<div class="columns">'+
        '<div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">'+
        '<span class="input">'+
        '<label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>'+
        '<input type="text" name="Mon" id="txt_mon_' + RateType + '_' + MealPlanID + '" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>'+
        '</div>'+
        '<div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">'+
        '<span class="input">'+
        '<label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label>'+
        '<input type="text" name="Tue" id="txt_tue_' + RateType + '_' + MealPlanID + '" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>'+
        '</div>'+
        '<div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">'+
        '<span class="input">'+
        '<label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label>'+
        '<input type="text" name="Wed" id="txt_wed_' + RateType + '_' + MealPlanID + '" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>'+
        '</div>'+
        '<div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">'+
        '<span class="input">'+
        '<label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label>'+
        '<input type="text" name="Thu" id="txt_thu_' + RateType + '_' + MealPlanID + '" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>'+
        '</div>'+
        '<div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">'+
        '<span class="input">'+
        '<label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label>'+
        '<input type="text" name="Fri" id="txt_fri_' + RateType + '_' + MealPlanID + '" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>'+
        '</div>'+
        '<div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">'+
        '<span class="input">'+
        '<label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label>'+
        '<input type="text" name="Sat" id="txt_sat_' + RateType + '_' + MealPlanID + '" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>'+
        '</div>'+
        '<div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">'+
        '<span class="input">'+
        '<label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label>'+
        '<input type="text" name="Sun" id="txt_sun_' + RateType + '_' + MealPlanID + '" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>'+
        '</div>'+
        '<div class="twelve-columns twelve-columns-mobile"></div>'+
        '</div>'+
        '</div>'+
        '<div class="with-padding" style="padding-bottom: 0px !important;">' +
        '<small class="input-info"><a class="pointer" onclick="genminsell(\'' + RateType + '_' + MealPlanID + '\')">Enter minimum selling price here<input type="checkbox" id="chk_Min' + RateType + '_' + MealPlanID + '" style="display:none"></a></small>' +
        
        '<div class="columns" id="genminsell' + RateType + '_' + MealPlanID+'" style="display: none;">'+
        '<div class="three-columns twelve-columns-mobile small-margin-bottom">'+
        '<label class="">Room Tariff Rate </label>' +
        '<span class="input tariff-textbox">'+
        '<label style="width: 30px" for="roomtariff" class="button field-drop-input green-gradient sCurrency"></label>' +
        '<input style="width: 60px" type="text" name="RoomTariff" id="txt_roomsell_' + RateType + '_' + +MealPlanID + '" onchange="AutoSetRate(this.value,\'' + RateType + '\',false,\'' + MealPlanID + '\')" class="input-unstyled onlyNumberSp" placeholder="0.00">' +
        '</span>'+
        '</div>'+
        '<div class="three-columns twelve-columns-mobile small-margin-bottom">'+
        '<label class="">Extra Bed Rate </label>' +
        '<span class="input tariff-textbox">'+
        '<label style="width: 30px" for="extrabed" class="button field-drop-input green-gradient sCurrency"></label>' +
        '<input style="width: 60px" type="text" name="Extrabed" id="txt_ebsell_' + RateType + '_' + +MealPlanID + '" class="input-unstyled onlyNumberSp" placeholder="0.00">' +
        '</span>'+
        '</div>'+
        '<div class="three-columns twelve-columns-mobile small-margin-bottom">'+
        '<label class="">Chid with bed </label>'+
        '<span class="input tariff-textbox">'+
        '<label style="width: 30px" for="cwb" class="button field-drop-input green-gradient sCurrency"></label>' +
        '<input style="width: 60px" type="text" name="Child-with-bed" id="txt_cwbsell_' + RateType + '_' + +MealPlanID + '" class="input-unstyled onlyNumberSp" placeholder="0.00"> ' +
        '</span>'+
        '</div>'+
        '<div class="three-columns twelve-columns-mobile small-margin-bottom">'+
        '<label class="">Child No Bed </label>'+
        '<span class="input tariff-textbox">'+
        '<label style="width: 30px" for="cnb" class="button field-drop-input green-gradient sCurrency"></label>' +
        '<input style="width: 60px" type="text" name="Child-no-bed" id="txt_cnbsell_' + RateType + '_' + +MealPlanID + '" class="input-unstyled onlyNumberSp" placeholder="0.00">   ' +
        '</span>' +                                                                                                                 
        '</div>' +
        '</div>' +
        '<div class="row sMinCellDate" style="display:none" >' +
        '<small><a class="pointer gray strong" onclick="showdaywiseMin(\'' + RateType + '_' + MealPlanID + '\')">Daywise Tariff <input type="checkbox" id="chk_dayMin' + RateType + '_' + MealPlanID + '" style="display:none"/> </a></small>' +
        '</div>' +
     
         '</div>' +
            '<div class="silver-bg field-drop-tariff sMinCell" id="ro-daywiseMin' + RateType + '_' + MealPlanID + '"  style="display: none;">' +
        '<div class="columns">' +
        '<div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
        '<span class="input">' +
        '<label for="ro-Mon" class="button field-drop-input-days green-gradient">Mon</label>' +
        '<input type="text" name="Mon" id="txt_mon_Min' + RateType + '_' + MealPlanID + '" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>' +
        '</div>' +
        '<div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
        '<span class="input">' +
        '<label for="ro-Tue" class="button field-drop-input-days green-gradient">Tue</label>' +
        '<input type="text" name="Tue" id="txt_tue_Min' + RateType + '_' + MealPlanID + '" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>' +
        '</div>' +
        '<div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
        '<span class="input">' +
        '<label for="ro-Wed" class="button field-drop-input-days green-gradient">Wed</label>' +
        '<input type="text" name="Wed" id="txt_wed_Min' + RateType + '_' + MealPlanID + '" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>' +
        '</div>' +
        '<div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
        '<span class="input">' +
        '<label for="ro-Thu" class="button field-drop-input-days green-gradient">Thu</label>' +
        '<input type="text" name="Thu" id="txt_thu_Min' + RateType + '_' + MealPlanID + '" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>' +
        '</div>' +
        '<div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
        '<span class="input">' +
        '<label for="ro-Fri" class="button field-drop-input-days green-gradient">Fri</label>' +
        '<input type="text" name="Fri" id="txt_fri_Min' + RateType + '_' + MealPlanID + '" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>' +
        '</div>' +
        '<div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
        '<span class="input">' +
        '<label for="ro-Sat" class="button field-drop-input-days green-gradient">Sat</label>' +
        '<input type="text" name="Sat" id="txt_sat_Min' + RateType + '_' + MealPlanID + '" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>' +
        '</div>' +
        '<div class="three-columns six-columns-mobile four-columns-tablet small-margin-bottom">' +
        '<span class="input">' +
        '<label for="ro-Sun" class="button field-drop-input-days green-gradient">Sun</label>' +
        '<input type="text" name="Sun" id="txt_sun_Min' + RateType + '_' + MealPlanID + '" class="field-drop-input input-unstyled validate[custom[onlyNumberSp]]" placeholder="0.00">' +
        '</span>' +
        '</div>' +
        '<div class="twelve-columns twelve-columns-mobile"></div>' +
        '</div>' +
         '</div>' +
        '</div>'
    $('#div_' + RateType + 'tab').append(html);
    /*Set Rate Currency*/
    SetCurrency($("#SelCurrency").val());
}/*Tarrif Rate */

function AddTarrifRate(ID,ndchk) {
    try {
        debugger
        index = 0;
        if ($(ndchk).is(":checked")) {
            index = $("#li_" + "GN").length;
            var MealName = $.grep(arrMealPlan, function (H) { return H.ID == ID })
                .map(function (H) { return H.Meal_Plan; })[0];
            TarrifPlan('GN', ID, index);
            GetTab(index, MealName, ID, "GN");
            GenrateDates(MealName)
            index = $("#li_" + "SP").length;
            TarrifPlan('SP', ID, index);
            GetTab(index, MealName, ID, "SP");
        }
        else {
            $("#li_" + 'GN' + '_' + ID).remove();
            $("#div_tab" + 'GN' + '_' + ID).remove();
            $("#li_" + 'SP' + '_' + ID).remove();
            $("#div_tab" + 'SP' + '_' + ID).remove();
        }
        setTimeout(function () {
            debugger
            $("#div_Main").refreshInnerTabs();
        }, 500)
      
       
    } catch (e) {
        AlertDanger(e.message)
    }
}

/* Tab Genrate*/
var datepickersOpt = {
    dateFormat: 'dd-mm-yy'
}
function GenrateDates(RateType) {
    debugger
    var html = "";
    try {
        var elem = $(".dte" + RateType);
        html += '<div class="columns dte' + RateType + '">'
        html += '<div class="four-columns ten-columns-mobile five-columns-tablet">'
        html +='<span class="input">'
        html +='<span class="icon-calendar"></span>'
        html += '<input type="text" class="input-unstyled datepicker validate[required] ' + RateType + 'From" value="">'
        html +='</span>'
        html +='</div>'
        html += '<div class="five-columns twelve-columns-mobile seven-columns-tablet">'
        html +='<span class="input">'
        html +='<span class="icon-calendar"></span>'
        html += '<input type="text" class="input-unstyled datepicker validate[required] ' + RateType + 'To" value="">'
        html += '</span>';
        if (elem.length == 0) {

            html += '<span class="mid-margin-left icon-size2 icon-plus-round icon-black" onclick="GenrateDates(\'' + RateType + '\')" id="btn_Add' + RateType + 'Date"></span>'
        }
        else
            html += '<span class="mid-margin-left icon-size2 icon-minus-round icon-red remCF" ></span>';
        html += '</div>'
        html += '</div>'
        $("#div_" + RateType + "Date").append(html);
        $(".remCF").on('click', function () {
            $(this).parent().parent().remove();
        });
        var elem_FromDate = $("#div_" + RateType + "Date").find("." + RateType + "From");
        var elem_ToDate = $("#div_" + RateType + "Date").find("." + RateType + "To");
        for (var i = 0; i < elem_FromDate.length; i++) {
            //DatePicker(elem_FromDate[i]);
            //DatePicker(elem_ToDate[i])
            /*Previous Date Select Date*/
            var previousFrom = 0;
            var previousTo = 0;
            if (i != 0)
                previousTo = moment($(elem_ToDate[i - 1]).val(), "DD-MM-YYYY");
            var minDate = 0;
            if (previousTo != 0)
                minDate = previousTo._i;
            $(elem_FromDate[i]).datepicker($.extend({
                minDate: minDate,
                onSelect: function () {
                    var minDate = $(this).datepicker('getDate');
                    minDate.setDate(minDate.getDate() + 1); //add One days
                    $(elem_ToDate[i-1]).datepicker("option", "minDate", minDate);
                    //UpdateFlag = false;
                    // ValidateCommon(elem_ToDate[i]);
                }, beforeShow: function () {
                    if(RateType !="GN")
                    {
                        var minDate = moment($($("#div_GNDate").find(".GNFrom")[i - 1]).val(), "DD-MM-YYYY")._i;
                        var maxDate = moment($($("#div_GNDate").find(".GNTo")[i - 1]).val(), "DD-MM-YYYY")._i;
                        $(this).datepicker("option", "minDate", minDate);
                        $(this).datepicker("option", "maxDate", maxDate);
                    }
                },
            }, datepickersOpt));

            $(elem_ToDate[i]).datepicker($.extend({
                onSelect: function () {
                    var maxDate = $(this).datepicker('getDate');
                    maxDate.setDate(maxDate.getDate());
                    UpdateFlag = false;
                }, beforeShow: function () {
                    if (RateType != "GN") {
                        var minDate = moment($($("#div_GNDate").find(".GNFrom")[i - 1]).val(), "DD-MM-YYYY")._i;
                        var maxDate = moment($($("#div_GNDate").find(".GNTo")[i - 1]).val(), "DD-MM-YYYY")._i;
                        $(this).datepicker("option", "minDate", minDate);
                        $(this).datepicker("option", "maxDate", maxDate);
                    }
                },
            }, datepickersOpt));
        }
    } catch (e) {  }
}



function GetChargeCond(elem , Input) {
    try {
        if(Input=="percentageinput")
        {
            $("#percentageinput").css("display", "");
            $("#amountinput").css("display", "none")
            $("#nightlyinput").css("display", "none")
        }
        else if (Input == "amountinput")
        {
            $("#percentageinput").css("display", "none");
            $("#amountinput").css("display", "")
            $("#nightlyinput").css("display", "none")
        }
        else if (Input == "nightlyinput") {
            $("#percentageinput").css("display", "none");
            $("#amountinput").css("display", "none")
            $("#nightlyinput").css("display", "")
        }
    } catch (e) {

    }
}/* Charge Condition*/


function SetCurrency(val) {
    try {
        $(".sCurrency").text(val);
    } catch (e) { }
}/*Set Currency*/

function ShowSpeicalCancel(elem, Type) {
    try {
        if($(elem).is(":checked"))
        {
            $("#definespecialdate-amount").css("display", "none")
            $("#definespecialdate-percentage").css("display", "none")
            $("#definespecialdate-nightly").css("display", "none")
            $("#definespecialdate-" + Type).css("display","")
        }
        else
        {
            $("#definespecialdate-amount").css("display", "none")
            $("#definespecialdate-percentage").css("display", "none")
            $("#definespecialdate-nightly").css("display", "none")
        }

    } catch (e) { }
}/*SP Charge Condition*/
function ShowCancel(elem) {
    try {
        if ($(elem).is(":checked")) {
            $('#cnlpolicy').slideToggle();
            $("#cnlpolicy").css("display", "")
            $("#percentage").prop("checked", true);
            $("#percentageinput").css("display", "");
            $("#amountinput").css("display", "none")
            $("#nightlyinput").css("display", "none")
        }
        else {
            $("#cnlpolicy").css("display", "none")
            $("#percentageinput").css("display", "none");
            $("#amountinput").css("display", "none")
            $("#nightlyinput").css("display", "none")
        }
       
    } catch (e) { }
}/*Cancel Show*/

function ShowMeal(exMeal) {
    if (exMeal.checked)
    {
        $(".chk_Meal").show(1000);
    }
    else
        $(".chk_Meal").hide(1000);
}

function GetMeals() {
    var html = '';
    try {
        for (var i = 0; i < arrMeals.length; i++) {
            html += '<div class="three-columns six-columns-mobile three-columns-mobile-landscape four-columns-tablet small-margin-bottom">'
            html += '<p class="no-margin-bottom">' + arrMeals[i].AddOnName + '</p>'
            html +='<span class="input">'
            html +='<label style="width: 30px" for="adultbreakfast" class="button disable green-gradient sCurrency"></label>'
            html += '<input style="width: 60px" type="text" name="RoomTariff" id="adult' + arrMeals[i].ID + '" class="input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">'
            html +='</span>'
            html +='<p>'
            html += '<input type="checkbox" class="checkbox hidden" id="adult' + arrMeals[i].ID + 'compulsory" value="yes">'
            html += '<label for="adult' + arrMeals[i].ID + 'compulsory" class="font11 hidden"> Compulsory</label>'
            html +='</p>'
            html +='</div>'
        }
        $("#div_AdultMeals").append(html);
        html = '';
        for (var i = 0; i < arrMeals.length; i++) {
            html += '<div class="three-columns six-columns-mobile three-columns-mobile-landscape four-columns-tablet small-margin-bottom">'
            html += '<p class="no-margin-bottom">' + arrMeals[i].AddOnName + '</p>'
            html += '<span class="input">'
            html += '<label style="width: 30px" for="adultbreakfast" class="button disable green-gradient sCurrency"></label>'
            html += '<input style="width: 60px" type="text" name="RoomTariff" id="child' + arrMeals[i].ID + '" class="input-unstyled validate[required,custom[onlyNumberSp]]" placeholder="0.00">'
            html += '</span>'
            html += '<p>'
            html += '<input type="checkbox" class="checkbox hidden" id="child' + arrMeals[i].ID + 'compulsory" value="yes">'
            html += '<label for="child' + arrMeals[i].ID + 'compulsory" class="font11 hidden"> Compulsory</label>'
            html += '</p>'
            html += '</div>'
        }
        $("#div_ChildMeals").append(html);
        $("#div_AdultMeals").append('<div class="twelve-columns twelve-columns-mobile four-columns-tablet small-margin-bottom">'+
                                    '<p class="no-margin-bottom"></p>'+
                                    '<input type="checkbox" class="checkbox" id="childmeal"  value="1"  onchange="sDifferentMeal()">' +
                                    '<label for="childmeal">Different rates for child</label>'+
                                    '</div>');
        
    } catch (e) { }
}/*Meals*/
function sDifferentMeal() {
    if ($("#childmeal").is(":checked"))
        $(".cChildMeals").show(1000);
    else
        $(".cChildMeals").hide(1000);
}
var arrRateType = ["GN", "SP"];
var arrDays = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
function PreviewRate() {
    $("#div_RatePreview").empty()
    var html = '';
    try {
        $("#div_RatePreview").append(GetRatePlan())
        var BookingCode = $("#ratecode").val();
        for (var r = 0; r < arrRateType.length; r++) {
            $(arrMealPlan).each(function (index, MealPlan) {
                if ($("#chk_" + MealPlan.ID).is(":checked"))
                {
                    var sRate = $("#div_tab" + arrRateType[r] + "_" + MealPlan.ID).find("#txt_tariffRate_" + arrRateType[r] + "_" + MealPlan.ID).val();
                    var sMinRate = $("#chk_Min" + arrRateType[r] + "_" + MealPlan.ID).is(":checked").toString().replace("true", "six").replace("false", "twelve");
                    if (sRate != "")
                    {
                        html += '<div class="field-block button-height wizard-control">';
                        /*Rate Validity*/
                        html += '<label class="label">'
                        html += '<b>' + MealPlan.Meal_Plan + '</b>'
                        html += '<div class="new-row-mobile font12 red">'
                        var ndDates = $(".dte" + arrRateType[r]);
                        $(ndDates).each(function (index, ndDate) {
                            var ndFromDates = $(ndDate).find("." + arrRateType[r] + "From")[0];
                            var ndToDates = $(ndDate).find("." + arrRateType[r] + "To")[0];
                            html += '<span class="no-margin-bottom">' + $(ndFromDates).val() + ' </span>to<span> ' + $(ndToDates).val() + '</span><br/>'
                        });
                        html += '</div>'
                        html += '<div class="new-row-mobile font12 green" style="line-height: 20px;">'
                        html += '<span class="no-margin-bottom">Booking Code </span><span class="black sBooking">' + BookingCode + '</span>'
                        html += '</div>'
                        html += '</label>'
                        /* Rate Booking Details*/
                        html += '<div class="columns">'
                        html += '<div class="' + sMinRate + '-columns twelve-columns-mobile small-margin-left">'
                        html += '<div class="columns with-small-padding">'
                        html += '<h4 class="mid-margin-left">Cost Rate</h4>'
                        /*Room Rate*/
                        html += '<div class="three-columns three-columns-mobile mid-margin-bottom">'
                        html += '<p class="green strong no-margin-bottom">Room</p>'
                        html += '<p class="black">' + sRate + '</p>'
                        html += '</div>'
                        /*Extra Bed Rate*/
                        var EBRate = $("#txt_extrabedRate_" + arrRateType[r] + "_" + MealPlan.ID).val()
                        html += '<div class="three-columns three-columns-mobile mid-margin-bottom">'
                        html += '<p class="green strong no-margin-bottom">Extra Bed</p>'
                        html += '<p class="black">' + EBRate + '</p>'
                        html += '</div>'
                        /*CWB Rate*/
                        var CWBRate = $("#txt_cwbrate_" + arrRateType[r] + "_" + MealPlan.ID).val()
                        html += '<div class="three-columns three-columns-mobile mid-margin-bottom">'
                        html += '<p class="green strong no-margin-bottom">CWB</p>'
                        html += '<p class="black">' + CWBRate + '</p>'
                        html += '</div>'
                        /*CNB Rate*/
                        var CNBRate = $("#txt_cnbrate_" + arrRateType[r] + "_" + MealPlan.ID).val()
                        html += '<div class="three-columns three-columns-mobile mid-margin-bottom">'
                        html += '<p class="green strong no-margin-bottom">CNB</p>'
                        html += '<p class="black">' + CNBRate + '</p>'
                        html += '</div>'
                        html += '</div>';
                        /* daywise rates*/
                        var vDays = $("#chk_day" + arrRateType[r] + "_" + MealPlan.ID).is(":checked").toString().replace("true", "").replace("false", "none");
                        var MonRate = 0, TueRate = 0, WedRate = 0, ThuRate = 0, FriRate = 0, SatRate = 0; SunRate = 0;
                        html += ' <div class="columns with-small-padding font11" style="border-top: .5px solid lightgray; display:' + vDays + ';">'

                        MonRate = $("#txt_mon_" + arrRateType[r] + "_" + MealPlan.ID).val();
                        if (MonRate == "")
                            MonRate = 0;
                        html +=' <div class="two-column three-columns-mobile mid-margin-bottom">'+
                                        '<p class="green strong no-margin-bottom">MON</p>'+
                                        '<p class="black">' + MonRate + '</p>' +
                                        '</div>'

                        TueRate = $("#txt_tue_" + arrRateType[r] + "_" + MealPlan.ID).val();
                        if (TueRate == "")
                            TueRate = 0;
                        html += ' <div class="two-column three-columns-mobile mid-margin-bottom">' +
                                       '<p class="green strong no-margin-bottom">TUE</p>' +
                                       '<p class="black">' + TueRate + '</p>' +
                                       '</div>'

                        WedRate = $("#txt_wed_" + arrRateType[r] + "_" + MealPlan.ID).val();
                        if (WedRate == "")
                            WedRate = 0;
                        html += ' <div class="two-column three-columns-mobile mid-margin-bottom">' +
                                       '<p class="green strong no-margin-bottom">WED</p>' +
                                       '<p class="black">' + WedRate + '</p>' +
                                       '</div>'

                        ThuRate = $("#txt_thu_" + arrRateType[r] + "_" + MealPlan.ID).val();
                        if (ThuRate == "")
                            ThuRate = 0;
                        html += ' <div class="two-column three-columns-mobile mid-margin-bottom">' +
                                       '<p class="green strong no-margin-bottom">THU</p>' +
                                       '<p class="black">' + ThuRate + '</p>' +
                                       '</div>'

                        FriRate = $("#txt_fri_" + arrRateType[r] + "_" + MealPlan.ID).val();
                        if (FriRate == "")
                            FriRate = 0;
                        html += ' <div class="two-column three-columns-mobile mid-margin-bottom">' +
                                       '<p class="green strong no-margin-bottom">FRI</p>' +
                                       '<p class="black">' + FriRate + '</p>' +
                                       '</div>'

                        SatRate = $("#txt_sat_" + arrRateType[r] + "_" + MealPlan.ID).val();
                        if (SatRate == "")
                            SatRate = 0;
                        html += ' <div class="two-column three-columns-mobile mid-margin-bottom">' +
                                       '<p class="green strong no-margin-bottom">SAT</p>' +
                                       '<p class="black">' + SatRate + '</p>' +
                                       '</div>'

                        SunRate = $("#txt_sun_" + arrRateType[r] + "_" + MealPlan.ID).val();
                        if (SunRate == "")
                            SunRate = 0;
                        html += ' <div class="two-column three-columns-mobile mid-margin-bottom">' +
                                       '<p class="green strong no-margin-bottom">SUN</p>' +
                                       '<p class="black">' + SunRate + '</p>' +
                                       '</div>'
                        html += '</div>';
                                /*Day Rate End*/
                        html += '</div>';
                        /*Selling Rate End*/
                        /*Min Selling Rate */
                        if ($("#chk_Min" + arrRateType[r] + "_" + MealPlan.ID).is(":checked"))
                        {
                            html += '<div class="six-columns twelve-columns-mobile small-margin-left">'
                            html += '<div class="columns with-small-padding">'
                            html += '<h4 class="mid-margin-left">Min Selling Rate</h4>'
                            /*Room Rate*/
                            var Rate = $("#txt_roomsell_" + arrRateType[r] + "_" + MealPlan.ID).val();
                            html += '<div class="three-columns three-columns-mobile mid-margin-bottom">'
                            html += '<p class="green strong no-margin-bottom">Room</p>'
                            html += '<p class="black">' + Rate + '</p>'
                            html += '</div>'
                            /*Extra Bed Rate*/
                            var EBRate = $("#txt_ebsell_" + arrRateType[r] + "_" + MealPlan.ID).val();
                            html += '<div class="three-columns three-columns-mobile mid-margin-bottom">'
                            html += '<p class="green strong no-margin-bottom">Extra Bed</p>'
                            html += '<p class="black">' + EBRate + '</p>'
                            html += '</div>'
                            /*CWB Rate*/
                            var CWBRate = $("#txt_cwbsell_" + arrRateType[r] + "_" + MealPlan.ID).val();
                            html += '<div class="three-columns three-columns-mobile mid-margin-bottom">'
                            html += '<p class="green strong no-margin-bottom">CWB</p>'
                            html += '<p class="black">' + CWBRate + '</p>'
                            html += '</div>'
                            /*CNB Rate*/
                            var CNBRate = $("#txt_cnbsell_" + arrRateType[r] + "_" + MealPlan.ID).val();
                            html += '<div class="three-columns three-columns-mobile mid-margin-bottom">'
                            html += '<p class="green strong no-margin-bottom">CWB</p>'
                            html += '<p class="black">' + CNBRate + '</p>'
                            html += '</div>'
                            html += '</div>';
                            /* daywise rates*/
                            var vDays = $("#chk_dayMin" + arrRateType[r] + "_" + MealPlan.ID).is(":checked").toString().replace("true", "").replace("false", "none");
                            var MonRate = 0, TueRate = 0, WedRate = 0, ThuRate = 0, FriRate = 0, SatRate = 0; SunRate = 0;
                            html += ' <div class="columns with-small-padding font11" style="border-top: .5px solid lightgray; display:' + vDays + ';">'

                            MonRate = $("#txt_mon_Min" + arrRateType[r] + "_" + MealPlan.ID).val();
                            if (MonRate == "")
                                MonRate = 0;
                            html += ' <div class="two-column three-columns-mobile mid-margin-bottom">' +
                                            '<p class="green strong no-margin-bottom">MON</p>' +
                                            '<p class="black">' + MonRate + '</p>' +
                                            '</div>'

                            TueRate = $("#txt_tue_Min" + arrRateType[r] + "_" + MealPlan.ID).val();
                            if (TueRate == "")
                                TueRate = 0;
                            html += ' <div class="two-column three-columns-mobile mid-margin-bottom">' +
                                           '<p class="green strong no-margin-bottom">TUE</p>' +
                                           '<p class="black">' + TueRate + '</p>' +
                                           '</div>'

                            WedRate = $("#txt_wed_Min" + arrRateType[r] + "_" + MealPlan.ID).val();
                            if (WedRate == "")
                                WedRate = 0;
                            html += ' <div class="two-column three-columns-mobile mid-margin-bottom">' +
                                           '<p class="green strong no-margin-bottom">WED</p>' +
                                           '<p class="black">' + WedRate + '</p>' +
                                           '</div>'

                            ThuRate = $("#txt_thu_Min" + arrRateType[r] + "_" + MealPlan.ID).val();
                            if (ThuRate == "")
                                ThuRate = 0;
                            html += ' <div class="two-column three-columns-mobile mid-margin-bottom">' +
                                           '<p class="green strong no-margin-bottom">THU</p>' +
                                           '<p class="black">' + ThuRate + '</p>' +
                                           '</div>'

                            FriRate = $("#txt_fri_Min" + arrRateType[r] + "_" + MealPlan.ID).val();
                            if (FriRate == "")
                                FriRate = 0;
                            html += ' <div class="two-column three-columns-mobile mid-margin-bottom">' +
                                           '<p class="green strong no-margin-bottom">FRI</p>' +
                                           '<p class="black">' + FriRate + '</p>' +
                                           '</div>'

                            SatRate = $("#txt_sat_Min" + arrRateType[r] + "_" + MealPlan.ID).val();
                            if (SatRate == "")
                                SatRate = 0;
                            html += ' <div class="two-column three-columns-mobile mid-margin-bottom">' +
                                           '<p class="green strong no-margin-bottom">SAT</p>' +
                                           '<p class="black">' + SatRate + '</p>' +
                                           '</div>'

                            SunRate = $("#txt_sun_Min" + arrRateType[r] + "_" + MealPlan.ID).val();
                            if (SunRate == "")
                                SunRate = 0;
                            html += ' <div class="two-column three-columns-mobile mid-margin-bottom">' +
                                           '<p class="green strong no-margin-bottom">SUN</p>' +
                                           '<p class="black">' + SunRate + '</p>' +
                                           '</div>'
                            html += '</div>';
                            /*Day Rate End*/
                            html += '</div>';
                        }
                     
                        /*Min Selling End */
                        html += '</div>';
                        html += '</div>';
                    }
                }
            });
        }
        $("#div_RatePreview").append(html)
        if (chk_mealrates.checked)
                $("#div_RatePreview").append(MealPreview())
        $("#div_RatePreview").append(ChragePreview())
        $("#div_RatePreview").append('<div class="field-block button-height wizard-controls align-right">' +
        '<div class="wizard-spacer" style="height: 19px"></div>' +
        '<a style="cursor:pointer" class="button glossy mid-margin-right" onclick="SaveRates()">' +
        '<span class="button-icon"><span class="icon-tick"></span></span>' +
        'Save' +
        '</a>' +
        '</div>');
    } catch (e) {
    }
}

function GetRatePlan() {
    try {
        return ' <legend class="legend">Tariff</legend>' +
        '<div class="field-block button-height">' +
        '<label class="label"><b>Tariff for</b></label>' +
        '<div class="columns">' +
        '<div class="four-columns six-columns-mobile mid-margin-bottom">' +
        '<label class="green strong">Supplier</label>' +
        '<label class="black"> ' + $("#Sel_Supplier option:selected").text() + '</label>' +
        '</div>' +
        '<div class="four-columns six-columns-mobile mid-margin-bottom">' +
        '<label class="green strong">Tariff Type</label>' +
        '<label class="black"> ' + $("#sel_RateType").val() + '</label>' +
        '</div>' +
        '<div class="four-columns twelve-columns-mobile mid-margin-bottom">' +
        '<label class="green strong">Market</label>' +
        '<label class="black"> ' + $("#sel_Country option:selected").text() + '</label>' +
        '</div>' +
        '</div>' +
        '</div>';
    } catch (ex) { }
}

function AutoSetRate(RRate,sRateType,bSell,MealID) {
    try {
        if(bSell)
        {
            if ($("#txt_roomsell_" + sRateType + "_" + MealID).val() == "")
                     $("#txt_roomsell_" + sRateType + "_" + MealID).val(RRate)
            for (var i = 0; i < arrDays.length; i++) {
                $("#txt_" + arrDays[i] + "_" + sRateType + "_" + MealID).val(RRate);
                $("#txt_" + arrDays[i] + "_Min" + sRateType + "_" + MealID).val($("#txt_roomsell_" + sRateType + "_" + MealID).val());
            }
        }
        else
        {
            for (var i = 0; i < arrDays.length; i++) {
                $("#txt_" + arrDays[i] + "_Min" + sRateType + "_" + MealID).val(RRate);
            }
        }
    } catch (e) { }
}/*Auto Set*/

function MealPreview() {
    var html='';
    try {
        html += '<div class="field-block button-height wizard-control">'+
                 '<label class="label"><b>Extra Meal Charges</b></label>'+
                 '<div class="columns">'+
                 '<div class="six-columns  twelve-columns-mobile small-margin-left">' +
                 ' <div class="columns with-small-padding">'+
                 '<h4 class="mid-margin-left">Adult</h4>';
        for (var i = 0; i < arrMeals.length; i++) {
            var Rate = $("#adult" + arrMeals[i].ID).val();
            if (Rate == "")
                Rate = 0.00;
            if (Rate != "" || $("#adult" + arrMeals[i].ID + "compulsory").is(":checked"))
            {
                html += '<div class="three-columns three-columns-mobile mid-margin-bottom">'
                html += '<p class="green strong no-margin-bottom">' +arrMeals[i].AddOnName + '</p>'
                html += '<p class="black no-margin-bottom">'+Rate+'</p>'
                if($("#adult"+arrMeals[i].ID+"compulsory").is(":checked"))
                    html += ' <small class="red no-margin-bottom">Compulsory</small>'
                 html += '</div>'
            }
           }
        html += '</div>'+ 
                 '</div>'+
                 ' <div class="six-columns  twelve-columns-mobile small-margin-left">' +
                 '<div class="columns with-small-padding">'+
                  '<h4 class="mid-margin-left">Child</h4>';
           for (var i = 0; i < arrMeals.length; i++) {
               var Rate = $("#child" + arrMeals[i].ID).val();
               if (!childmeal.checked)
                   Rate = $("#adult" + arrMeals[i].ID).val();
               if (Rate == "")
                   Rate = 0.00;
               if (Rate != "" || $("#child" + arrMeals[i].ID + "compulsory").is(":checked"))
            {
                html += '<div class="three-columns three-columns-mobile mid-margin-bottom">'
                html += '<p class="green strong no-margin-bottom">' + arrMeals[i].AddOnName + '</p>'
                html += '<p class="black no-margin-bottom">'+Rate+'</p>'
                if ($("#child" + arrMeals[i].ID + "compulsory").is(":checked"))
                    html += ' <small class="red no-margin-bottom">Compulsory</small>'
                html += '</div>'
            }
          }
           html += '</div></div></div></div>';
     return html;
    } catch (e) { }
}/*Meal Preview*/

function ChragePreview() {
    var html ='';
    try {
        html += '<div class="field-block button-height wizard-control">'+
                '<label class="label"><b>Cancellation Policy</b></label>'
        '<div class="wrapped">';
        if($("#chk_Cancel").is(":checked"))
        {
            if ($("#percentage").is(":checked"))
                html += 'In the event of cancellation before <b>' + $("#sel_DayPer").val() + '</b> from the day of check-in <b>' + $("#sel_ChargePer").val() + 'charge</b> will apply. <b>' + $("#sel_NoShowPer").val() + ' charge</b> will apply in the event of <b>No Show</b>.'
            else if ($("#amount").is(":checked"))
                html += 'In the event of cancellation before <b>' + $("#sel_DayAmt").val() + '</b> from the day of check-in <b>' + $("#cancellationamount").val() + 'charge</b> will apply. <b>' + $("#sel_NoShowAmt").val() + ' charge</b> will apply in the event of <b>No Show</b>.'
            else if ($("#nightly").is(":checked"))
                html += 'In the event of cancellation before <b>' + $("#sel_DaysNight").val() + '</b> from the day of check-in <b>' + $("#validation-number").val() + 'night charge</b>  will apply. <b>' + $("#sel_NoShowNight").val() + ' charge</b> will apply in the event of <b>No Show</b>.'
        }
        else
        {
            html += 'This non-Refunable rate , no charge will be refundable once cancelled.'
        }
        html += '</div>'
        html += '</div> '
        return html;

    }catch(e){}
}/*Charge Preview*/


function SaveRates() {
    try {
        var RoomTypeID = GetQueryStringParams('RoomId');
        var sBooingCode = $("#ratecode").val();
        var arrTarifPlan = new Array();
        var arrRates = new Array();
        var arrRatePlan = new Array();
        var arrCancellation = GetCancellationPolicy();
        for (var r = 0; r < arrRateType.length; r++) {
            $(arrMealPlan).each(function (index, MealPlan) {
                if ($("#chk_" + MealPlan.ID).is(":checked")) {
                    var sRate = $("#div_tab" + arrRateType[r] + "_" + MealPlan.ID).find("#txt_tariffRate_" + arrRateType[r] + "_" + MealPlan.ID).val();
                    var arrMarket = $("#sel_Country").val();
                    var Market = "";
                    $(arrMarket).each(function (index, item) {
                        Market += item + ',';
                    })
                   if (sRate != undefined && sRate != "")
                   {
                       var RateID = arrRateType[r] + "_" + MealPlan.ID;
                        arrRatePlan = {
                           PlanID: 0,
                           HotelID: HotelCode,
                           SupplierID: $("#Sel_Supplier").val(),
                           RoomTypeID: RoomTypeID,
                           RateType: $("#sel_RateType").val(),
                           Attachment: '',
                           Market: Market,
                           sCurrency: $("#SelCurrency").val(),
                       };
                       var ndDates = $(".dte" + arrRateType[r]);
                     
                       $(ndDates).each(function (index, ndDate) {
                           var ndFromDates = $(ndDate).find("." + arrRateType[r] + "From")[0];
                           var ndToDates = $(ndDate).find("." + arrRateType[r] + "To")[0];
                           arrTarifPlan.push({
                               nRateID: 0,
                               nMeal_PlanID: MealPlan.ID,
                               RoomTypeID :RoomTypeID,
                               nHotelID: HotelCode,
                               nPlanID: 0,
                               nCancellation: 0,
                               bStatus: true,
                               sRate_Type: arrRateType[r],
                               sBooingCode: sBooingCode,
                               sFromDate: $(ndFromDates).val(),
                               sToDate: $(ndToDates).val(),
                               nMinStay: $("#txt_MinStay").val(),
                               nMaxStay: $("#txt_MaxStay").val(),
                               nMinRoom: $("#txt_MinRoom").val(),
                               nMaxRoom: $("#txt_MaxRoom").val(),
                           });
                           var RRate = $("#txt_tariffRate_"+ RateID).val();
                           var EBRate = $("#txt_extrabedRate_" + RateID).val();
                           var CWBRate = $("#txt_cwbrate_" + RateID).val();
                           var CNBRate = $("#txt_cnbrate_" + RateID).val();
                           var arrDaysRate = new Array();
                           for (var d = 0; d < arrDays.length; d++) {
                               arrDaysRate.push({
                                   Day: arrDays[d],
                                   Rate: $("#txt_" + arrDays[d] + "_" + RateID).val(),
                               });
                           }
                          
                           var MinRRate = $("#txt_tariffRate_" + RateID).val();
                           var MinEBRate = $("#txt_extrabedRate_" + RateID).val();
                           if (MinEBRate == "")
                               MinEBRate = $("#txt_extrabedRate_" + RateID).val();
                           var MinCWBRate = $("#txt_cwbrate_" + RateID).val();
                           if (MinCWBRate == "")
                               MinCWBRate = $("#txt_cwbrate_" + RateID).val();
                           var MinCNBRate = $("#txt_cnbrate_" + RateID).val();
                           if (MinCNBRate == "")
                               MinCNBRate = $("#txt_cnbrate_" + RateID).val();
                           var arrMinDaysRate = new Array();
                           for (var d = 0; d < arrDays.length; d++) {
                               arrMinDaysRate.push({
                                   Day: arrDays[d],
                                   Rate: $("#txt_" + arrDays[d] + "_Min" + RateID).val(),
                               });
                           }
                           arrRates.push({
                               RRate: RRate,
                               EBRate: EBRate,
                               CWBRate: CWBRate,
                               CNBRate: CNBRate,
                               arrDays: arrDaysRate,
                               MinSelling: { RRate: MinRRate, EBRate: MinCWBRate, CWBRate: MinCWBRate, CNBRate: MinCNBRate, arrDays: arrMinDaysRate }
                           });
                       });
                   }
                }
            });
        }
        post("../handler/Roomratehandler.asmx/SaveRates",
            {
                arrRatePlan: arrRatePlan,
                arrTarrif: arrTarifPlan,
                arrRates: arrRates,
                arrCancellationPolicy: arrCancellation
            }, function (data) {
                Success("Rate Saved Successfully");
                var Upload = $("#file-input").val();
                debugger;
                if (Upload != "")
                {
                    try {
                        $(data.arrTarrifID).each(function (index, arrTarrifID) {
                            SaveDoc("file-input", "doc/RateDoc/" + HotelAdminID + "/" + HotelCode + "/" + arrTarrifID)
                        })
                        setTimeout(function () {
                            window.location.href = 'availability_pricing.aspx?sHotelID=' + HotelCode + '&HName=' + HotelName
                                /*'Ratelist.aspx?sHotelID=' + HotelCode + '&HName=' + HotelName;*/;
                        }, 1000);
                    } catch (e) {

                    }
                   
                }
                else
                {
                    setTimeout(function () {
                        window.location.href = 'availability_pricing.aspx?sHotelID=' + HotelCode + '&HName=' + HotelName
                    }, 800);
                }
               
        }, function (error) {
            AlertDanger(error.ex);
        })
    } catch (e) { AlertDanger("Unable to saved Rates.") }
}

function GetCancellationPolicy() {
    var arrCancellation = new Array();
    try {
        var CancelationPolicy = "REF/FD";
        var NoShowPolicy = "No Show/";
        if ($("#chk_Cancel").is(":checked")) {
            if ($("#percentage").is(":checked")) {
                CancelationPolicy += "(" + $("#sel_DayPer").val().replace("days", "") + ")/(" + $("#sel_ChargePer").val() + ")";
                arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "Refundable", DaysPrior: $("#sel_DayPer").val().replace("days", ""), IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_ChargePer").val() });
                if ($("#sel_NoShowPer").val() != "Same") {
                    NoShowPolicy += $("#sel_NoShowPer").val();
                    arrCancellation.push({ CancelationPolicy: NoShowPolicy, RefundType: "NoShow", DaysPrior: 0, IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: 0, PercentageToCharge: $("#sel_NoShowPer").val() });/*No Show*/
                }

                else {
                    NoShowPolicy += $("#sel_ChargePer").val();
                    arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "NoShow", DaysPrior: $("#sel_DayPer").val().replace("days", ""), IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_ChargePer").val() });

                }
                if ($("#definespecialdate-percentage").css('display') != 'none') {
                    CancelationPolicy = "REF/FD";
                    NoShowPolicy = "No Show/";
                    CancelationPolicy += "(" + $("#sel_DayPer").val().replace("days", "") + ")/(" + $("#sel_ChargePer").val() + ")";
                    arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "Refundable", DaysPrior: $("#sel_DaySPPer").val().replace("days", ""), IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_ChargeSPPer").val() });
                    if ($("#sel_NoShowSPPer").val() != "Same") {
                        NoShowPolicy += $("#sel_NoShowSPPer").val();
                        arrCancellation.push({ CancelationPolicy: NoShowPolicy, RefundType: "NoShow", DaysPrior: 0, IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_NoShowSPPer").val() });/*No Show*/
                    }

                    else {
                        NoShowPolicy += $("#sel_ChargeSPPer").val();
                        arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "NoShow", DaysPrior: $("#sel_DaySPPer").val().replace("days", ""), IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_ChargeSPPer").val() });
                    }
                }

            }
            else if ($("#nightly").is(":checked")) {
                CancelationPolicy += "(" + $("#sel_DaysNight").val().replace("days", "") + ")/N(" + $("#validation-number").val() + ")";
                arrCancellation.push({ RefundType: "Refundable", DaysPrior: $("#sel_DaysNight").val().replace("days", ""), NightsToCharge: $("#validation-number").val(), ChargesType: "Nights", SupplierID: HotelAdminID });
                if ($("#sel_NoShowNight").val() != "Same") {
                    NoShowPolicy += $("#sel_NoShowNight").val();
                    arrCancellation.push({ RefundType: "NoShow", DaysPrior: $("#sel_DaysNight").val().replace("days", ""), IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_NoShowNight").val() });;/*No Show*/
                }
                else {
                    NoShowPolicy += $("#validation-number").val();
                    arrCancellation.push({ RefundType: "NoShow", DaysPrior: $("#sel_DaysNight").val().replace("days", ""), NightsToCharge: $("#validation-number").val(), ChargesType: "Nights", SupplierID: HotelAdminID });

                }
                if ($("#definespecialdate-nightly").css('display') != 'none') {
                    CancelationPolicy = "REF/FD";
                    NoShowPolicy = "No Show/";
                    CancelationPolicy += "(" + $("#sel_DaysSPNight").val().replace("days", "") + ")/N(" + $("#validation-spnumber").val() + ")";
                    arrCancellation.push({ RefundType: "Refundable", DaysPrior: $("#sel_DaysSPNight").val().replace("days", ""), NightsToCharge: $("#validation-spnumber").val(), ChargesType: "Nights", SupplierID: HotelAdminID });
                    if ($("#sel_NoShowSPNight").val() != "Same") {
                        NoShowPolicy += $("#sel_NoShowSPNight").val();
                        arrCancellation.push({ RefundType: "NoShow", DaysPrior: 0, IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_NoShowSPNight").val() });;/*No Show*/
                    }
                    else {
                        NoShowPolicy += $("#validation-spnumber").val();
                        arrCancellation.push({ RefundType: "NoShow", DaysPrior: $("#sel_DaysSPNight").val().replace("days", ""), NightsToCharge: $("#validation-spnumber").val(), ChargesType: "Nights", SupplierID: HotelAdminID });
                    }
                }
            }
            else if ($("#amount").is(":checked")) {
                CancelationPolicy += "(" + $("#sel_DayAmt").val().replace("days", "") + ")/(" + $("#cancellationamount").val() + ")";
                arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "Refundable", DaysPrior: $("#sel_DayAmt").val().replace("days", ""), AmountToCharge: $("#cancellationamount").val(), ChargesType: "Amount", SupplierID: HotelAdminID });
                if ($("#sel_NoShowAmt").val() != "Same") {
                    NoShowPolicy += $("#sel_NoShowAmt").val();
                    arrCancellation.push({ CancelationPolicy: NoShowPolicy, RefundType: "NoShow", DaysPrior: 0, IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_NoShowAmt").val() });;/*No Show*/
                }
                else {
                    NoShowPolicy += $("#cancellationamount").val();
                    arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "NoShow", DaysPrior: $("#sel_DayAmt").val().replace("days", ""), AmountToCharge: $("#cancellationamount").val(), ChargesType: "Amount", SupplierID: HotelAdminID });
                }
                if ($("#definespecialdate-amount").css('display') != 'none') {
                    CancelationPolicy += "(" + $("#sel_DaySPAmt").val().replace("days", "") + ")/(" + $("#sp-cancellationamount").val() + ")";
                    arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "Refundable", DaysPrior: $("#sel_DaySPAmt").val().replace("days", ""), AmountToCharge: $("#sp-cancellationamount").val(), ChargesType: "Amount", SupplierID: 0 });
                    if ($("#sel_NoShowAmt").val() != "Same") {
                        NoShowPolicy += $("#sel_NoShowSPAmt").val();
                        arrCancellation.push({ CancelationPolicy: NoShowPolicy, RefundType: "NoShow", DaysPrior: 0, IsDaysPrior: "true", ChargesType: "Percentile", SupplierID: HotelAdminID, PercentageToCharge: $("#sel_NoShowSPAmt").val() });;/*No Show*/
                    }
                    else {
                        NoShowPolicy += $("#sp-cancellationamount").val();
                        arrCancellation.push({ CancelationPolicy: CancelationPolicy, RefundType: "NoShow", DaysPrior: $("#sel_DaySPAmt").val().replace("days", ""), AmountToCharge: $("#sp-cancellationamount").val(), ChargesType: "Amount", SupplierID: HotelAdminID });
                    }
                }

            }
        }
        return arrCancellation;
    } catch (e) { return new Array() }
}
