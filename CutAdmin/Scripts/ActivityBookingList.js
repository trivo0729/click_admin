﻿$(document).ready(function () {
    //   Validate();
    //BookingList()

    $("#actbookdate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#actdate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
});

var BookingList = "";
//function GetActBookingList() {
//    $("#tbl_Sightseeing").dataTable().fnClearTable();
//    $("#tbl_Sightseeing").dataTable().fnDestroy();
//    // $("#tbl_BookingList tbody tr").remove();
//    $.ajax({
//        type: "POST",
//        url: "../Handler/ActivityHandller.asmx/ActivityBookingList",
//        data: JSON.stringify({}),
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//            //   Success("Booking Success");
//            //  window.location.href = "BookingList.aspx"
//            if (result.retCode == 1) {

//                BookingList = result.BookingList;
//                htmlGenratorAct();

//            }
//            else {
//                $("#tbl_Sightseeing").dataTable({
//                    bSort: false, sPaginationType: 'full_numbers',
//                });
//            }
            
//        }

//    })
//}

function GetActBookingList() {
    $("#tbl_Sightseeing").dataTable().fnClearTable();
    $("#tbl_Sightseeing").dataTable().fnDestroy();

    var columns = [
        {
            "mData": "BookingDate",
            "mRender": function (data, type, row, meta) {
                if (row.BookingDate != null) {
                    return '' + row.BookingDate + '';
                }
                else {
                    return '';
                }
            }
        },

        {
            "mData": "Date",
            "mRender": function (data, type, row, meta) {
                if (row.Date != null) {
                    return '' + row.Date + '';
                }
                else {
                    return '';
                }
            }
        },

        {
            "mData": "AgencyName",
            "mRender": function (data, type, row, meta) {
                if (row.BookingType == "B2B") {
                    return '' + row.AgencyName + '';
                }
                else {
                    return '' + row.PassengerName + '';
                }
            }
        },
        {
            "mData": "ActivityName",
            "mRender": function (data, type, row, meta) {
                return '' + row.ActivityName + '/' + row.City + '-' + row.Country + '';
            }
        },
        {
            "mData": "Status",
            "mRender": function (data, type, row, meta) {
                var trow = '';
                if (row.Status != null) {
                    trow += '<span class="status-vouchered">Vouchered</span>';
                }
                else {
                    trow += '';
                }
                return trow;
            }
        },
        {
            "mData": "TotalFare",
            "mRender": function (data, type, row, meta) {
                if (row.TotalFare != null) {
                    return ''+ GetCurrency(row.CurrencyCode)+ '' + numberWithCommas(parseFloat(row.TotalFare).toFixed(2)) + '';
                }
                else {
                    return '';
                }
            }
        },
        {
            "mData": "User_Id",
            "mRender": function (data, type, row, meta) {
                var trow = '';
                if (row.Status != null) {
                    trow += '<a title = "Click for Invoice" class="invoice-btn align-center"  onclick="GetActPrintInvoice(\'' + row.Request_Id + '\',\'' + row.User_Id + '\',\'' + row.Status + '\',\'A\')" >Invoice</a >';
                    // trow += '<a title = "Click for Voucher" class="voucher-btn align-center"  onclick="GetActPrintVoucher(\'' + row.Request_Id + '\',\'' + row.User_Id + '\',\'' + row.Status + '\')" >Voucher</a >';
                }
                else {
                    return trow;
                }
                return trow;
            },
        },
    ];

    GetData("tbl_Sightseeing", [], 'Handler/ActivityHandller.asmx/ActivityBookingList', columns);
    $("#tbl_Sightseeing").css("width", "100%");
}
//    // $("#tbl_BookingList tbody tr").remove();
//    //$.ajax({
//    //    type: "POST",
//    //    url: "../Handler/ActivityHandller.asmx/ActivityBookingList",
//    //    data: JSON.stringify({}),
//    //    contentType: "application/json; charset=utf-8",
//    //    datatype: "json",
//    //    success: function (response) {
//    //        var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
//    //           Success("Booking Success");
//    //          window.location.href = "BookingList.aspx"
//    //        if (result.retCode == 1) {

//    //            BookingList = result.BookingList;
//    //            htmlGenratorAct();

//    //        }
//    //        else {
//    //            $("#tbl_Sightseeing").dataTable({
//    //                bSort: false, sPaginationType: 'full_numbers',
//    //            });
//    //        }

//    //    }

//    //})
//}

function htmlGenratorAct() {
    $("#tbl_Sightseeing").dataTable().fnClearTable();
    $("#tbl_Sightseeing").dataTable().fnDestroy();
    var trow = '';
    for (var i = 0; i < BookingList.length; i++) {
        trow += '<tr>';
        trow += '<td class="align-center">' + (i + 1) + '</td>';
        trow += '<td class="align-center">' + BookingList[i].BookDate + ' </td>';
        trow += '<td class="align-center">' + BookingList[i].Date + ' </td>';
        if (BookingList[i].BookingType == "B2B") {
            trow += '<td class="align-center">' + BookingList[i].AgencyName + '</td>';
        }
        else {
            trow += '<td class="align-center">' + BookingList[i].Passenger_Name + '</td>';
        }
        trow += '<td class="align-center">' + BookingList[i].Act_Name + '/' + BookingList[i].City + '-' + BookingList[i].Country + '   </td>';
        var adults = parseFloat(BookingList[i].Adults);
        var Childs1 = parseFloat(BookingList[i].Childs1);
        var Childs2 = parseFloat(BookingList[i].Childs2);
        var Total = adults + Childs1 + Childs2;

        if (BookingList[i].Status != null)
            //trow += '<td class="align-center">' + BookingList[i].Status + '</td>';
            trow += '<td style="max-width: 75px; vertical-align: middle; width: 7% ;float:left" class="center"><span class="status-vouchered" style="float:left">Vouchered</span></td>';
        else
        trow += '<td class="center">-</td>';
            //trow += '<td style="max-width:70px; vertical-align: middle; width: 7%" class="center"><span class="status-cancelled">Cancelled</td>';
        //trow += '<td class="align-center">' + BookingList[i].Total.toFixed(2) + ' ' + BookingList[i].CurrencyCode + '</td>';
        trow += '<td style="vertical-align: middle; min-width: 60px;" class="align-center"><span class="strong"><i class="' + GetCurrency(BookingList[i].CurrencyCode) + '"></i> ' + numberWithCommas(parseFloat(BookingList[i].Total).toFixed(2)) + '</span></td>';
        if (BookingList[i].Status != null) {
            trow += '<td style="max-width: 55px; vertical-align: middle;" class="align-center action-button">';
            trow += '<a title = "Click for Invoice" class="invoice-btn align-center"  onclick="GetActPrintInvoice(\'' + BookingList[i].Request_Id + '\',\'' + BookingList[i].User_Id + '\',\'' + BookingList[i].Status + '\',\'A\')" >Invoice</a >';
            trow += '<a title = "Click for Voucher" class="voucher-btn align-center"  onclick="GetActPrintVoucher(\'' + BookingList[i].Request_Id + '\',\'' + BookingList[i].User_Id + '\',\'' + BookingList[i].Status + '\')" >Voucher</a >';
            trow += ' </td>';
        }
        else {
            trow += '<td>-</td>';
        }
        trow += '</tr>';
    }
        trow += '</tbody>'
        $("#tbl_Sightseeing").append(trow);
        $('[data-toggle="tooltip"]').tooltip()
        $("#tbl_Sightseeing").dataTable({
            bSort: false, sPaginationType: 'full_numbers',
            //"width": "104%"
        });
        document.getElementById("tbl_Sightseeing").removeAttribute("style")
}

function GetActPrintInvoice(ReservationID, Uid, Status) {
    window.open('ActivityInvoice.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status);
    //window.open('ActivityInvoice.aspx?ReservationId=' + ReservationID);
}

function GetActPrintVoucher(ReservationID, Uid, Status) {
    var win = window.open('ActivityVoucher.aspx?ReservationId=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status);
}


//function SearchAct() {

//    $("#tbl_Sightseeing").dataTable().fnClearTable();
//    $("#tbl_Sightseeing").dataTable().fnDestroy();

//    var BookDate = $("#actbookdate").val();
//    var ActDate = $("#actdate").val();
//    var Passenger = $("#txt_actpassenger").val();
//    var ActivityName = $("#txt_actname").val();
//    var Status = $("#selactStatus option:selected").val();

//    var data = {
//        BookDate: BookDate,
//        ActDate: ActDate,
//        Passenger: Passenger,
//        ActivityName: ActivityName,
//        Status: Status
//    }

//    $.ajax({
//        type: "POST",
//        url: "../Handler/ActivityHandller.asmx/SearchActBooklist",
//        data: JSON.stringify(data),
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            var result = JSON.parse(response.d)
//            if (result.retCode == 1) {
//                BookingList = result.BookingList;
//                htmlGenratorAct();
//            }
//            else if (result.retCode == 0) {
//                $("#tbl_Sightseeing").dataTable({
//                    bSort: false, sPaginationType: 'full_numbers',
//                });
//            }
//        },
//        error: function () {
//            Success("An error occured while loading details.");
//        }
//    });

//}

function SearchAct()
{
        $("#tbl_Sightseeing").dataTable().fnClearTable();
        $("#tbl_Sightseeing").dataTable().fnDestroy();

        var BookDate = $("#actbookdate").val();
        var ActDate = $("#actdate").val();
        var Passenger = $("#txt_actpassenger").val();
        var ActivityName = $("#txt_actname").val();
        var Status = $("#selactStatus option:selected").val();
        var data = {
            BookDate: BookDate,
            ActDate: ActDate,
            Passenger: Passenger,
            ActivityName: ActivityName,
            Status: Status
        }
        var columns = [

        {
            "mData": "BookingDate",
            "mRender": function (data, type, row, meta) {
                if (row.BookingDate != null) {
                    return '' + row.BookingDate + '';
                }
                else {
                    return '';
                }
            }
        },

        {
            "mData": "Date",
            "mRender": function (data, type, row, meta) {
                if (row.Date != null) {
                    return '' + row.Date + '';
                }
                else {
                    return '';
                }
            }
        },

        {
            "mData": "AgencyName",
            "mRender": function (data, type, row, meta) {
                if (row.BookingType == "B2B") {
                    return '' + row.AgencyName + '';
                }
                else {
                    return '' + row.PassengerName + '';
                }

            }
        },


        {
            "mData": "ActivityName",
            "mRender": function (data, type, row, meta) {
                return '' + row.ActivityName + '/' + row.City + '-' + row.Country + '';
            }
        },
        {
            "mData": "Status",
            "mRender": function (data, type, row, meta) {
                var trow = '';
                if (row.Status != null) {
                    trow += '<span class="mj">Vouchered</span>';
                }
                else {
                    trow += '';
                }
                return trow;
            }
        },
        {
            "mData": "TotalFare",
            "mRender": function (data, type, row, meta) {
                if (row.TotalFare != null) {
                    return '' + GetCurrency(row.CurrencyCode) + '' + numberWithCommas(parseFloat(row.TotalFare).toFixed(2)) + '';
                }
                else {
                    return '';
                }

            }
        },
        {
            "mData": "User_Id",
            "cellType": "HTML",
            "mRender": function (data, type, row, meta) {
                var trow = '';
                if (row.Status != null) {
                    trow += '<a title = "Click for Invoice" class="invoice-btn align-center"  onclick="GetActPrintInvoice(\'' + row.Request_Id + '\',\'' + row.User_Id + '\',\'' + row.Status + '\',\'A\')" >Invoice</a >';
                    // trow += '<a title = "Click for Voucher" class="voucher-btn align-center"  onclick="GetActPrintVoucher(\'' + row.Request_Id + '\',\'' + row.User_Id + '\',\'' + row.Status + '\')" >Voucher</a >';
                }
                else {
                    return trow;
                }
                return trow;
            },
        },
        ];
        GetData("tbl_Sightseeing", data , '../ActivityHandller.asmx/SearchActBooklist', columns);
        $("#tbl_Sightseeing").css("width", "100%");

}

function ResetAct() {
    $("#actbookdate").val('');
    $("#actdate").val('');
    $("#txt_actpassenger").val('');
    $("#txt_actname").val('');
    $("#selactStatus").val('All');
}

function ExportActBookingDetailsToExcel(Document) {
    var BookDate = $("#actbookdate").val();
    var ActDate = $("#actdate").val();
    var Passenger = $("#txt_actpassenger").val();
    var ActivityName = $("#txt_actname").val();
    var Status = $("#selactStatus option:selected").val();

    var Type = "All";
    //if (BookDate == "" && ActDate == "" && Passenger == "" && ActivityName == "" && Status == "All") {
    window.location.href = "../Handler/ExportToExcelHandler.ashx?datatable=ActivityBookingDetails&Type=" + Type + "&Document=" + Document + "&BookDate=" + BookDate + "&ActDate=" + ActDate + "&Passenger=" + Passenger + "&ActivityName=" + ActivityName + "&Status=" + Status;
    //}
    //else {
    //    Type = "Search";
       // window.location.href = "../Handler/ExportToExcelHandler.ashx?datatable=ActivityBookingDetails&Type=" + Type + "&Document=" + Document;
    //}
}