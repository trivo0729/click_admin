﻿var AgeOfChild1 = "";
var AgeOfChild2 = "";
var AgeOfInfant = "";
var RateofAdult = 0;
var RateforChild1 = 0;
var RateforChild2 = 0;
var RateforInfant = 0;
var SupplierExRate = 0;
var exRateofAdult = 0;
var exRateforChild1 = 0;
var exRateforChild2 = 0;
var exRateforInfant = 0;
var arrBooking = new Array();
$(function () {
    GetAgency();
    GetDetails();
})

function GetDetails() {
    debugger
    arrBooking = window.parent.arrBooking;
    if (arrBooking.length != 0) {
        /*Set Currency*/
        $(".sCurrency").text(arrBooking.Currency);

        /*set adult details*/
        RateofAdult = window.parent.$("#" + arrBooking.RateTypes + "_" + arrBooking.SlotID + "_adult").val();
        //$("#lbl_rateofadult").text(RateofAdult);

        /*set childs details*/
        if (arrBooking.arrChildPolicy.length != 0) {

            AgeOfChild1 = "Child(" + arrBooking.arrChildPolicy[0].Child_Age_Upto + " " + "to " + arrBooking.arrChildPolicy[0].Child_Age_From + " yrs) ";
            AgeOfChild2 = "Child(" + arrBooking.arrChildPolicy[0].Small_Child_Age_Upto + " " + "to " + arrBooking.arrChildPolicy[0].Child_Age_Upto + " yrs)";
            $("#spn_childage1").text(AgeOfChild1);
            $("#spn_childage2").text(AgeOfChild2);

            RateforChild1 = window.parent.$("#" + arrBooking.RateTypes + "_" + arrBooking.SlotID + "_child1").val();
            RateforChild2 = window.parent.$("#" + arrBooking.RateTypes + "_" + arrBooking.SlotID + "_child2").val();
            $("#lbl_rateofchild1").text(RateforChild1);
            $("#lbl_rateofchild2").text(RateforChild2);

            var x = document.getElementById("div_child");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }

            /*set infant details*/
            if (arrBooking.arrChildPolicy[0].Infant_Allow == true) {
                AgeOfInfant = "Infant(Below " + arrBooking.arrChildPolicy[0].Small_Child_Age_Upto + " yrs)";
                $("#spn_infantage").text(AgeOfInfant);

                RateforInfant = window.parent.$("#" + arrBooking.RateTypes + "_" + arrBooking.SlotID + "_infant").val();
                $("#lbl_rateofinfant").text(RateforInfant);

                var x = document.getElementById("div_infant");
                if (x.style.display === "none") {
                    x.style.display = "block";
                } else {
                    x.style.display = "none";
                }
            }
        }
        GetSuppExchangeRate();
        CalculateRates();
    }
}
var adulttotal = "";
var child1total = "";
var child2total = "";
var infanttotal = "";
var TotalAmount = "";
var TotalPax = "";
var exadulttotal = "";
var exchild1total = "";
var exchild2total = "";
var exinfanttotal = "";
var exTotalAmount = "";
function CalculateRates() {
    debugger
   //adulttotal = parseFloat(RateofAdult) * parseFloat($("#txt_adult").val());
   //child1total = parseFloat(RateforChild1) * parseFloat($("#txt_child1").val());
   //child2total = parseFloat(RateforChild2) * parseFloat($("#txt_child2").val());
   //infanttotal = parseFloat(RateforInfant) * parseFloat($("#txt_infant").val());
   //TotalAmount = parseFloat(adulttotal) + parseFloat(child1total) + parseFloat(child2total) + parseFloat(infanttotal);
    TotalPax = parseFloat($("#txt_adult").val()) + parseFloat($("#txt_child1").val()) + parseFloat($("#txt_child2").val()) + parseFloat($("#txt_infant").val());

    exadulttotal = (parseFloat(exRateofAdult) * parseFloat($("#txt_adult").val())).toFixed(2);
    exchild1total = (parseFloat(exRateforChild1) * parseFloat($("#txt_child1").val())).toFixed(2);
    exchild2total = (parseFloat(exRateforChild2) * parseFloat($("#txt_child2").val())).toFixed(2);
    exinfanttotal = (parseFloat(exRateforInfant) * parseFloat($("#txt_infant").val()).toFixed(2));
    exTotalAmount = (parseFloat(exadulttotal) + parseFloat(exchild1total) + parseFloat(exchild2total) + parseFloat(exinfanttotal)).toFixed(2);

   // $("#lbl_rateofadult").text(adulttotal);
   // $("#lbl_rateofchild1").text(child1total);
   // $("#lbl_rateofchild2").text(child2total);
   // $("#lbl_rateofinfant").text(infanttotal);
   // $("#lbl_totalamount").text(TotalAmount);

    $("#lbl_exrateofadult").text(exadulttotal);
    $("#lbl_exrateofchild1").text(exchild1total);
    $("#lbl_exrateofchild2").text(exchild2total);
    $("#lbl_exrateofinfant").text(exinfanttotal);
    $("#lbl_extotalamount").text(exTotalAmount);
}
var arrAgencyDetail = new Array();
function GetAgency() {
    try {
        post("../GenralHandler.asmx/Getagency", {}, function (data) {
            arrAgencyDetail = data.arrAgencyDetail;
            $(arrAgencyDetail).each(function (index, Agency) {
                $('#Sel_Agency').append($('<option></option>').val(Agency.AgencyID).html(Agency.AgencyName));
                //$('#Sel_Agency').change();
            });
        }, function (errordata) {
            alertDanger(errordata.ex)
        })
    } catch (e) { }
}

var arrBookingDetail = new Array();
function BookSightseeing() {
    debugger
    var Child1_Cost = 0;
    var Child2_Cost = 0;
    var Infant_Cost = 0;
    if (arrBooking.arrChildPolicy.length != 0) {
        //Child1_Cost = $("#lbl_rateofchild1").text();
        //Child2_Cost = $("#lbl_rateofchild2").text();
        Child1_Cost = $("#lbl_exrateofchild1").text();
        Child2_Cost = $("#lbl_exrateofchild2").text();
        if (arrBooking.arrChildPolicy[0].Infant_Allow == true) {
            //Infant_Cost = $("#lbl_rateofinfant").text();
            Infant_Cost = $("#lbl_exrateofinfant").text();
        }
    }
    try {
        if ($("#frm_booking").validationEngine('validate')) {
            var PassengerName = $("#txt_name").val() + ' ' + $("#txt_lname").val();
            arrBookingDetail = {
                Activity_Id: arrBooking.ActivityID,
                Passenger_Name: PassengerName,
                Email: $("#txt_email").val(),
                Contact_No: $("#txt_contact").val(),
                Adults: $("#txt_adult").val(),
                Child1: $("#txt_child1").val(),
                Child2: $("#txt_child2").val(),
                Infant: $("#txt_infant").val(),
                //Adult_Cost: $("#lbl_rateofadult").text(),
                Adult_Cost: $("#lbl_exrateofadult").text(),
                Child1_Cost: Child1_Cost,
                Child2_Cost: Child2_Cost,
                Infant_Cost: Infant_Cost,
                TotalPax: TotalPax,
                //TotalAmount: $("#lbl_totalamount").text(),
                TotalAmount: $("#lbl_extotalamount").text(),
                User_Id: $("#Sel_Agency").val(),
                SupplierId: arrBooking.Supplier,
                Sightseeing_Date: arrBooking.Sightseeing_Date,
                SlotID: arrBooking.SlotID,
                Ticket_Type: arrBooking.TicketType,
                Activity_Type: arrBooking.RateTypes,
                BookingType: "B2B",
                InventoryType: arrBooking.InventoryType,
                PlanID: arrBooking.PlanID
            };

            post("GenralHandler.asmx/BookSightseeing",
        {
            arrBookingDetail: arrBookingDetail,
        }, function (data) {
            Success("Sightseeing Booked Successfully");
            setTimeout(function () {
                window.parent.closeIframe();
            }, 2000)
        }, function (error) {
            AlertDanger(error.ex);
        })
        }
    }
    catch (e) {
    }
}
function CancelBooking() {

    window.parent.closeIframe();
}


function GetExchangeRate() {
    debugger
    var id = $("#Sel_Agency").val();
    var Currency = $.grep(arrAgencyDetail, function (p) { return p.AgencyID == id; })
              .map(function (p) { return p.Currency; });
    $(".eCurrency").text(Currency);
    //if (arrBooking.Currency != Currency)
    //{
        $(".spn_exchange").show();
        post("GenralHandler.asmx/GetExchangeRate", { Currency: Currency[0] }, function (data) {
            ExchangeRate = data.Rate;
            exRateofAdult = (parseFloat(supRateofAdult) / parseFloat(ExchangeRate)).toFixed(2)
            exRateforChild1 = (parseFloat(supRateforChild1) / parseFloat(ExchangeRate)).toFixed(2)
            exRateforChild2 = (parseFloat(supRateforChild2) / parseFloat(ExchangeRate)).toFixed(2)
            exRateforInfant = (parseFloat(supRateforInfant) / parseFloat(ExchangeRate)).toFixed(2)
            $("#lbl_exrateofadult").text(exRateofAdult);
            CalculateRates()
        }, function (error) {
            AlertDanger(error.ex);
        })
    //}
}
var supRateofAdult =0
var supRateforChild1=0
var supRateforChild2=0
var supRateforInfant=0
function GetSuppExchangeRate() {

    post("GenralHandler.asmx/GetExchangeRate", { Currency: arrBooking.Currency }, function (data) {
        SupplierExRate = data.Rate;
        supRateofAdult = parseFloat(parseFloat(RateofAdult) * parseFloat(SupplierExRate)) * parseFloat($("#txt_adult").val());
        supRateforChild1 = parseFloat(parseFloat(RateforChild1) * parseFloat(SupplierExRate)) * parseFloat($("#txt_adult").val());
        supRateforChild2 = parseFloat(parseFloat(RateforChild2) * parseFloat(SupplierExRate)) * parseFloat($("#txt_adult").val());
        supRateforInfant = parseFloat(parseFloat(RateforInfant) * parseFloat(SupplierExRate)) * parseFloat($("#txt_adult").val());

    }, function (error) {
        AlertDanger(error.ex);
    })
}