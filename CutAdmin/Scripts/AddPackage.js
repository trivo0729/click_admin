﻿var hiddenID = 0;

$(document).ready(function () {
    hiddenID = GetQueryStringParams('ID');
    //if (hiddenID != 0 && hiddenID != undefined)
    //{
    //    GetPackageDetail(hiddenID);
    //    $("#hdpackageId").val(hiddenID);
    //    $('#lst_Pricing').removeClass("disabled");
    //    $('#lst_Itinerary').removeClass("disabled");
    //    $('#lst_HotelDetails').removeClass("disabled");
    //    $('#lst_PackageImages').removeClass("disabled");
    //}

    $("#frm_package").validationEngine();

    if (hiddenID != 0 && hiddenID != undefined)
    {
        GetPackageDetail(hiddenID);
        $("#hdpackageId").val(hiddenID);
        $('#lst_Pricing').hide("disabled");
        $('#lst_Itinerary').removeClass("disabled");
        $('#lst_HotelDetails').removeClass("disabled");
        $('#lst_PackageImages').removeClass("disabled");
    }

    GetVehicleName();

    $('.wizard fieldset').on('wizardenter', function () {
        var sStep = $(this).find("legend").text();
        
        if (sStep == "Basic Information")
        {                                         
            CheckValidationForTabs('BasicInformation');
        }
        else if (sStep == "Pricing")
        {
            CheckValidationForTabs('Pricing')
        }
        else if (sStep == "Itinerary") {
            CheckValidationForTabs('Itinerary')
        }
        else if (sStep == "Hotel Details")
        {
            CheckValidationForTabs('HotelDetails')
        }
        else if (sStep == "Package Transfer")
        {
            GetTransfer()
        }
        else if (sStep == "Package Activities")
        {
            GetActivityById()
        }
        else if (sStep == "Package Images")
        {
            CheckValidationForTabs('PackageImages')
        }
    })

    $('.wizard fieldset').on('wizardleave', function () {
        var sStep = $(this).find("legend").text();
        if (sStep == "Basic Information")
        {
            AddPackageBasicDetails();
        }
        else if (sStep == "Pricing")
        {
             SavePricingDetails();
        }
        else if (sStep == "Itinerary")
        {
           SaveItinerary();
        }
        else if (sStep == "Hotel Details") {
            SaveHotelDetails();
        }
        else if (sStep == "Package Transfer")
        {
              AddPackageCab();
        }
        else if (sStep == "Package Activities")
        {
             AddActivities();
        }
        else if (sStep == "Package Images") {
          add_PackageImage();
        }
    })
});


var VehicleName = new Array();
var ArrActivity = new Array();
function GetVehicleName() {
    $.ajax({
        url: "../Handler/PackageDetailHandler.asmx/GetVehicleName",
        type: "post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode == 0) {
                Success("Error occurred");
            }
            else if (result.retCode == 1) {
                var html = '';
                $("#select_Vehicle0").empty();
                VehicleName = result.Vehicle;
                for (var i = 0; i < VehicleName.length; i++) {
                    if (VehicleName[i].VehicleModel != null) {
                        html += '<option value="' + VehicleName[i].ID + '">' + VehicleName[i].VehicleModel + '</option>';
                    }
                }
                $("#select_Vehicle0").append(html);
                $("#Divselect_Vehicle0 .select span")[0].textContent = "" + VehicleName[0].VehicleModel + "";
            }
        },
        error: function () {
        }
    });
}

function GetTheme(ThemeId) {
    if (ThemeId == 1) {
        return "Holidays";
    }
    if (ThemeId == 2) {
        return "Umrah";
    }
    if (ThemeId == 3) {
        return "Hajj";
    }
    if (ThemeId == 4) {
        return "Honeymoon";
    }
    if (ThemeId == 5) {
        return "Summer";
    }
    if (ThemeId == 6) {
        return "Adventure";
    }
    if (ThemeId == 7) {
        return "Deluxe";
    }
    if (ThemeId == 8) {
        return "Business";
    }
    if (ThemeId == 9) {
        return "Premium";
    }
    if (ThemeId == 10) {
        return "Wildlife";
    }
    if (ThemeId == 11) {
        return "Weekend";
    }
    if (ThemeId == 12) {
        return "New Year";
    }
}

var hcdNewCity;
function GetPackageDetail(nID) {
    $.ajax({
        url: "../handler/PackageDetailHandler.asmx/GetPackageDetail",
        type: "post",
        data: '{"nID":"' + nID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.Session == 0) {
                window.location.href = "Default.aspx";
            }
            if (result.retCode == 0) {
                alert("Package detail found.");
            }
            else if (result.retCode == 1) {
                debugger;
                List_PackageDetail = result.List_PackageDetail;
                $("#txt_PackageName").val(List_PackageDetail[0].sPackageName);

                var city = (List_PackageDetail[0].sPackageDestination).split('|');
                if (hcdNewCity == '') {
                    shdnDCode = city[1];
                }
                else {
                    shdnDCode = hcdNewCity;
                }
                shdnDCode = city[1];
                $("#txt_City").val(city[0]);
                $("hdnDCode").val(city[1]);
                $("#hdpackageCity").val(city[0]);
                GetActivity();
                var sCategorys = List_PackageDetail[0].sPackageCategory.split(',');
                var sCategoryArray = [];
                var CategoryName = [];
                for (var scat = 0; scat < sCategorys.length; scat++) {
                    if (sCategorys != "") {
                        CategoryName.push(GetCategoryName(sCategorys[scat]));
                        sCategoryArray.push(sCategorys[scat]);
                    }

                }
                $("#select_Category").trigger('change').val(sCategoryArray);
                $("#Divselect_Category .select span")[0].textContent = CategoryName;
                $("#DivDuration .select span")[0].textContent = "" + List_PackageDetail[0].nDuration + " Days " + (List_PackageDetail[0].nDuration - 1) + " Night"
                $("#txt_Duration").val(List_PackageDetail[0].nDuration);
                var dFrom = (List_PackageDetail[0].dValidityFrom).split(' ');
                $("#datepicker").val(dFrom[0]);
                var dTo = (List_PackageDetail[0].dValidityTo).split(' ');
                $("#datepicker2").val(dTo[0]);
                var sThemes = List_PackageDetail[0].sPackageThemes.split(',');
                var sThemeArray = [];
                var ThemeName = [];
                for (var stheme = 0; stheme < sThemes.length; stheme++) {
                    sThemeArray.push(sThemes[stheme]);
                    ThemeName.push(GetTheme(sThemes[stheme]));
                }
                $("#select_Themes").trigger('change').val(sThemeArray);
                $("#Divselect_Themes .select span")[0].textContent = ThemeName;
                $("#txt_PriceSingle").val(List_PackageDetail[0].nPriceSingleAdult);
                $("#txt_Tax").val(List_PackageDetail[0].dTax);
                $("#txt_cancelDays").val(List_PackageDetail[0].nCancelDays);
                $("#txt_CancelCharge").val(List_PackageDetail[0].dCancelCharge);
                CKEDITOR.instances['txt_Desctiption'].setData(List_PackageDetail[0].sPackageDescription);
                //$("#txt_Desctiption").val(List_PackageDetail[0].sPackageDescription);
                //$("#txt_TermsCondition").val(List_PackageDetail[0].sTermsCondition);
                CKEDITOR.instances['txt_TermsCondition'].setData(List_PackageDetail[0].sTermsCondition);
                if (List_PackageDetail[0].sInventory == "0") {
                    $("#radio_InventoryRequest").attr("checked", "checked");
                    $("#txt_Inventory").css("display", "none");
                }
                else {
                    $("#radio_InventoryFixed").attr("checked", "checked");
                    $("#txt_Inventory").css("display", "");
                    $("#Divtxt_Inventory").css("display", "");
                    $("#txt_Inventory").val(List_PackageDetail[0].sInventory);
                }

                if (List_PackageDetail[0].IsDomestic == true && (List_PackageDetail[0].IsDomestic != null || List_PackageDetail[0].IsDomestic != "")) {
                    $("#Domestic").attr("checked", "checked");
                }
                else {
                    $("#International").attr("checked", "checked");
                }
                if (List_PackageDetail[0].Transfer == true)
                {
                         $("#Transfer").click();
                         $("#TransferDiv").show();
                         GetTransfer();
                }

            }
        },
        error: function () {
            alert('Errror in getting Product details, Please try again!');
        }
    });
}

$(function () {
    var tpj = jQuery;
    var chkinDate;
    var chkinMonth;
    var chkoutDate;
    var chkoutMonth;
    tpj(document).ready(function () {
        tpj("#txt_City").autocomplete({
            source: function (request, response) {
                tpj.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "../Handler/AddPackageHandler.asmx/GetDestinationCode",
                    data: "{'name':'" + document.getElementById('txt_City').value + "'}",
                    dataType: "json",
                    success: function (data) {
                        var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                        response(result);
                    },
                    error: function (result) {
                        alert("No Match");
                    }
                });
            },
            minLength: 4,
            select: function (event, ui) {
                ;
                tpj('#hdnDCode').val(ui.item.id);
                $("#hdpackageCity").val(ui.item.value);
                GetActivity();
                var h1 = ui.item.id;
            }
        });
    });


    $("#datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#datepicker2").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "d-m-yy"
    });

});

function GetActivity() {
    var ActCity = $("#hdpackageCity").val();
    ActCity = ActCity.split(",");
    ActCity = ActCity[0];
    CheckActivity(ActCity);
}

function CheckActivity(ActCity) {
    $.ajax({
        type: "POST",
        url: "../Handler/AddPackageHandler.asmx/GetActivity",
        data: '{"ActCity":"' + ActCity + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                $("#LiActivities").show();
                ArrActivity = result.ArrActivity;
                GenerateActivity();
            }
            else {
                $("#LiActivities").hide();
            }
        },
        error: function () {
            Success("errors")
        }

    });
}

function UpdatePricingDetails() {
    Success("Please first fill basic detail");
}
function setInvetoryFixed() {
    if ($("#radio_InventoryFixed").is(":checked"))
    {
        $("#Divtxt_Inventory").css("display", "");
        $("#txt_Inventory").css("display", "");
        
    }
    else {
        $("#Divtxt_Inventory").css("display", "none");
        $("#txt_Inventory").css("display", "none");
    }
}
var shdnDCode;
function AddPackageBasicDetails() {
   
            var bValid = true;
            var sPackageName = $("#txt_PackageName").val();
            var sCity = $("#txt_City").val() + " |" + $("#hdnDCode").val();
            var sCategory = $("#select_Category").val();
            var nDuration = $("#txt_Duration").val();
            var dvalidFrom = $("#datepicker").val();
            var dvalidupto = $("#datepicker2").val();
            var sThemes = $("#select_Themes").val();
            var sDesctiption = CKEDITOR.instances['txt_Desctiption'].getData();
            var sTax = $("#txt_Tax").val();
            if (sTax == "")
                sTax = 0;
            var sCancelDays = $("#txt_cancelDays").val();
            if (sCancelDays == "")
                sCancelDays = 0;
            var sCancelCharge = $("#txt_CancelCharge").val();
            if (sCancelCharge == "")
                sCancelCharge = 0;
            var sTermsCondition = CKEDITOR.instances['txt_TermsCondition'].getData();
            shdnDCode = $("#hdnDCode").val();
            var IncludingTransfer = false;
            var Domestic = true;
            var sInventory = 0;
            if ($("#radio_InventoryRequest").is(":checked")) {
                sInventory = 0;
            }
            else if ($("#radio_InventoryFixed").is(":checked")) {
                sInventory = $("#txt_Inventory").val();
            }

            if ($("#International").is(":checked")) {
                Domestic = false;
            }
            if ($("#Transfer").is(":checked")) {
                IncludingTransfer = true;
            }

            if (sPackageName == "") {
                bValid = false;
                AlertDanger("Please Enter Package Name");
                $("#txt_PackageName").focus();
                $('.wizard fieldset').showWizardPrevStep(true);
                return false;
            }
            if (sCity == "") {
                bValid = false;
                AlertDanger("Please Enter City");
                $("#txt_City").focus();
                $('.wizard fieldset').showWizardPrevStep(true);
                return false;
            }
            if (sCategory == "0") {
                bValid = false;
                AlertDanger("Please select category");
                $("#select_Category").focus();
                $('.wizard fieldset').showWizardPrevStep(true);
                return false;
            }
            if (nDuration == "0") {
                bValid = false;
                AlertDanger("Please select Duration");
                $("#txt_Duration").focus();
                $('.wizard fieldset').showWizardPrevStep(true);
                return false;
            }
            if (dvalidFrom == "") {
                bValid = false;
                AlertDanger("Please select Valid From");
                $("#datepicker").focus();
                $('.wizard fieldset').showWizardPrevStep(true);
                return false;
            }
            if (dvalidupto == "") {
                bValid = false;
                AlertDanger("Please select Valid To");
                $("#datepicker2").focus();
                $('.wizard fieldset').showWizardPrevStep(true);
                return false;
            }
            if (process(dvalidupto) < process(dvalidFrom)) {
                bValid = false;
                AlertDanger("Validate Date should be After the Validity Start Dat");
                $("#datepicker2").focus();
                $('.wizard fieldset').showWizardPrevStep(true);
                return false;
            }
            if (sThemes == "0") {
                bValid = false;
                AlertDanger("Please select Theme");
                $("#select_Themes").focus();
                $('.wizard fieldset').showWizardPrevStep(true);
                return false;
            }
            //if (sTax == "") {
            //    bValid = false;
            //    AlertDanger("Please enter Tax");
            //}
            //if (sCancelDays == "") {
            //    bValid = false;
            //    AlertDanger("Please enter Cancel Days");
            //    $("#txt_cancelDays").focus();
            //    return false;
            //}
            //if (sCancelCharge == "") {
            //    bValid = false;
            //    AlertDanger("Please enter Cancel Charge");
            //    $("#txt_CancelCharge").focus();
            //    return false;
            //}

            //if (sTermsCondition == "") {
            //    bValid = false;
            //    AlertDanger("Please write Terms & Condition");
            //    return false;
            //}
            //if (sDesctiption == "") {
            //    bValid = false;
            //    AlertDanger("Please enter Description");
            //    return false;
            //}
            if (bValid == true) {
                if (hiddenID != 0 && hiddenID != undefined && $("#hdpackageId").val() != 0) {
                    $.ajax({
                        type: "POST",
                        url: "../Handler/PackageDetailHandler.asmx/UpdatePackageBasicDetails",
                        data: '{"nID":"' + hiddenID + '","sPackageName":"' + sPackageName + '","sCity":"' + sCity + '","sCategory":"' + sCategory + '","nDuration":"' + nDuration + '","dvalidFrom":"' + dvalidFrom + '","dvalidupto":"' + dvalidupto + '","sThemes":"' + sThemes + '","sDesctiption":"' + sDesctiption + '","dTax":"' + sTax + '","nCancelDays":"' + sCancelDays + '","dCancelCharge":"' + sCancelCharge + '","sTermsCondition":"' + sTermsCondition + '","sInventory":"' + sInventory + '","IncludingTransfer":"' + IncludingTransfer + '","Domestic":"' + Domestic + '"}',
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        success: function (response) {
                            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                            if (result.Session == 0) {
                                window.location.href = "Default.aspx";
                                return false;
                            }
                            if (result.retCode == 1) {
                                Success("Basic details updated successfully.");
                                $("#hdpackageId").val(result.nID);
                                $("#lst_Pricing").removeClass("disabled")
                                $("#lst_Pricing").addClass("active")
                                $("#lst_BasicInformation").attr("class", "");
                                $("#Pricing").show();
                                $("#BasicInformation").hide();

                                $("#lst_Itinerary").removeClass("disabled")
                                $("#lst_HotelDetails").removeClass("disabled")
                                $("#lst_PackageImages").removeClass("disabled")
                                if ($("#Transfer").is(":checked"))
                                {
                                    AddPackageCab();
                                }
                                GetPricingDetail(result.nID);
                            }
                            else {
                            }
                        },
                        error: function () {
                            Success("errors")
                        }
                    });
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "../Handler/AddPackageHandler.asmx/AddPackageBasicDetails",
                        data: '{"sPackageName":"' + sPackageName + '","sCity":"' + sCity + '","sCategory":"' + sCategory + '","nDuration":"' + nDuration + '","dvalidFrom":"' + dvalidFrom + '","dvalidupto":"' + dvalidupto + '","sThemes":"' + sThemes + '","sDesctiption":"' + sDesctiption + '","dTax":"' + sTax + '","nCancelDays":"' + sCancelDays + '","dCancelCharge":"' + sCancelCharge + '","sTermsCondition":"' + sTermsCondition + '","sInventory":"' + sInventory + '","IncludingTransfer":"' + IncludingTransfer + '","Domestic":"' + Domestic + '"}',
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        success: function (response) {
                            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                            if (result.Session == 0) {
                                window.location.href = "Default.aspx";
                                return false;
                            }
                            if (result.retCode == 1) {
                                Success("Basic details added successfully, Please add Pricing details.");
                                $("#hdpackageId").val(result.nID);
                                $("#lst_Pricing").removeClass("disabled")
                                $("#lst_Pricing").addClass("active")
                                $("#lst_BasicInformation").attr("class", "");
                                $("#Pricing").show();
                                $("#BasicInformation").hide();

                                $("#lst_Itinerary").removeClass("disabled")
                                $("#lst_HotelDetails").removeClass("disabled")
                                $("#lst_PackageImages").removeClass("disabled")
                                if ($("#Transfer").is(":checked")) {
                                    AddPackageCab();
                                }
                                GetPricingDetail(result.nID);
                            }
                            else {

                            }
                        },
                        error: function () {
                            AlertDanger("something went wrong")
                        }

                    });
                }

            }
        }

function process(date) {
    var parts = date.split("/");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

function CheckValidationForTabs(id) {
    if (id == "BasicInformation") {
        $('#BasicInformation').show();
    }
    else if (id == "Pricing") {
        var bValid = true;
        var sPackageName = $("#txt_PackageName").val();
        var sCity = $("#txt_City").val() + " |" + $("#hdnDCode").val();
        var sCategory = $("#select_Category").val();
        var nDuration = $("#txt_Duration").val();
        var dvalidFrom = $("#datepicker").val(); //$(this).attr('rel');
        var dvalidupto = $("#datepicker2").val();
        var sThemes = $("#select_Themes").val();
        //var sDesctiption = $("#txt_Desctiption").val();
        var sDesctiption = CKEDITOR.instances['txt_Desctiption'].getData();
        var sTax = $("#txt_Tax").val();
        var sCancelDays = $("#txt_cancelDays").val();
        var sCancelCharge = $("#txt_CancelCharge").val();
        //var sTermsCondition = $("#txt_TermsCondition").val();
        var sTermsCondition = CKEDITOR.instances['txt_TermsCondition'].getData();
        shdnDCode = $("#hdnDCode").val();
        var Domestic = false;
        var sInventory = 0;
        if ($("#radio_InventoryRequest").is(":checked")) {
            sInventory = 0;
        }
        if ($("#International").is(":checked")) {
            Domestic = true;
        }
        else if ($("#radio_InventoryFixed").is(":checked")) {
            sInventory = $("#txt_Inventory").val();
        }
        if (sPackageName == "") {
            bValid = false;
        }
        if (sCity == "") {
            bValid = false;
        }
        if (sCategory == "0") {
            bValid = false;
        }
        if (nDuration == "0") {
            bValid = false;
        }
        if (dvalidFrom == "") {
            bValid = false;
        }
        if (dvalidupto == "") {
            bValid = false;
        }
        if (process(dvalidupto) < process(dvalidFrom)) {
            bValid = false;
        }
        if (sThemes == "0") {
            bValid = false;
        }
        if (sDesctiption == "") {
            bValid = false;
        }
        if (sTax == "") {
            bValid = false;
        }
        if (sCancelDays == "") {
            bValid = false;
        }
        if (sCancelCharge == "") {
            bValid = false;
        }
        if (sTermsCondition == "") {
            bValid = false;
        }
        if (bValid == true) {
            var Id = $("#hdpackageId").val();
            GetPricingDetail(Id);
            $("#lst_Pricing").removeClass("disabled")
            $("#lst_Pricing").addClass("active")
            $("#lst_Itinerary").removeClass("disabled")
            $("#lst_HotelDetails").removeClass("disabled")
            $("#lst_PackageImages").removeClass("disabled")
        }
        else {
           // Success("Please first fill basic detail");
            $('#BasicInformation').show();
            $('#Pricing').hide();
            $('#Itinerary').hide();
            $('#HotelDetails').hide();
            $('#PackageImages').hide();
            $("#BasicInformation").addClass("active")
            $("#lst_Pricing").removeClass("active")
            $("#lst_Pricing").removeClass("disabled")
            $("#lst_Itinerary").removeClass("disabled")
            $("#lst_HotelDetails").removeClass("disabled")
            $("#lst_PackageImages").removeClass("disabled")
        }

    }
    else if (id == "Itinerary") {
        $('#BasicInformation').show();
        $('#Pricing').hide();
        $('#Itinerary').hide();
        $('#HotelDetails').hide();
        $('#PackageImages').hide();
        var Id = $("#hdpackageId").val();
        GetItineraryDetail(Id);
    }
    else if (id == "HotelDetails") {
        $('#BasicInformation').show();
        $('#Pricing').hide();
        $('#Itinerary').hide();
        $('#HotelDetails').hide();
        $('#PackageImages').hide();
        var Id = $("#hdpackageId").val();
        HotelDetails(Id);
    }
    else if (id == "PackageImages") {
        $('#BasicInformation').show();
        $('#Pricing').hide();
        $('#Itinerary').hide();
        $('#HotelDetails').hide();
        $('#PackageImages').hide();
        var Id = $("#hdpackageId").val();
        $('input[id$=hidden_Filed_ID]').val(Id);
        GetCurrentImages(Id);
    }

}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function TransferShow() {
    if ($("#Transfer").is(':checked'))
    {
        $("#TransferDiv").show();
    }
    else {
        $("#TransferDiv").hide();
    }
}
function GenerateActivity() {
    $("#Activity").empty();
    var html = "";
    for (var i = 0; i < ArrActivity.length; i++) {
        html += '<div class="four-columns twelve-columns-mobile">'
        html += '<input  type="checkbox" name="Activity" id="Activity_' + ArrActivity[i].P_Id + '" value="' + ArrActivity[i].P_Id + '" class="checkbox ActivityChk mid-margin-left">'
        html += ' <label for="Inclusion" class="label"> ' + ArrActivity[i].Act_Name + '</label>'
        html += '</div>'
    }
    $("#Activity").append(html);
}

var ArrActAdd = new Array();
function AddActivities() {
    var ActivityId = "";
    ArrActAdd = new Array();
    var Act = $(".ActivityChk")
    var arrAct = [];
    for (var l = 0; l < Act.length; l++) {
        if (Act[l].classList.length == 5) {
            arrAct.push({
                class: Act[l],
            })
        }
    }
    for (var i = 0; i < arrAct.length; i++) {
        ActivityId = arrAct[i].class.childNodes[1].value;
        var PackageId = $("#hdpackageId").val();
        ArrActAdd.push({
            ActivityId: ActivityId,
            PackageId: PackageId,
        });
    }
    if (arrAct.length == 0) {
        Success("Please Select at least One Activity");
        $('.wizard fieldset').showWizardPrevStep(true);
        return false;

    }
    var data = { ArrActAdd: ArrActAdd };
    $.ajax({
        type: "POST",
        url: "../Handler/AddPackageHandler.asmx/AddActivities",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {
                window.location.href = "Default.aspx";
            }
            if (result.retCode == 1) {
                Success("Activity added successfully");
            }
        },
        error: function () {
            Success("errors")
        }

    });

}
var Click = false;
function GetActivityById() {
    var Id = $("#hdpackageId").val();
    var data = { Id: Id };
    if (Id != "") {
        $.ajax({
            type: "POST",
            url: "../Handler/PackageDetailHandler.asmx/GetActivityById",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (Click == false) {
                    if (result.retCode == 1) {
                        var Arr = result.Arr;
                        for (var i = 0; i < Arr.length; i++) {
                            $("#Activity_" + Arr[i].ActivityId).click();
                        }
                        Click = true;
                    }
                }

            },
            error: function () {
                AlertDanger("An error occured !!!");
            }
        });
    }
}
//***************************** code for Pricing tab *************************************
