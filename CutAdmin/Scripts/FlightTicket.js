﻿function GetPDFTicket(ReservationID, Uid, Status) {
    debugger;
    var win = window.open('FlightTicketPDF.aspx?ReservationID=' + ReservationID + '&Uid=' + Uid + '&Status=' + Status + '', '_blank');
}

function CancelBooking(BookingID, Type) {
    var data = {
        BookingID: BookingID,
        Type: Type
    }
    $.ajax({
        type: "POST",
        url: "../handler/FlightHandler.asmx/CancelBooking",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                alert("Ticket Cancelled Successfully")
            }
        },
        error: function () {
            $('#SpnMessege').text("An error occured while cancel booking")
            $('#ModelMessege').modal('show')
        }
    });
}

function EmailModal() {
    $.modal({
        content: '<div class="modal-body">' +
            '<div class="scrollingDiv">' +
            '<div class="columns">' +
            '<div class="tow-columns bold">Email :</div>' +
            '<div class="ten-columns"><div class="input full-width"><input name="prompt-value" id="txt_sendInvoice" class="input-unstyled full-width" type="text"></div></div></div> ' +
            '<div class="columns">' +
            '<div class="three-columns bold">&nbsp;</div>' +
            '<div class="nine-columns bold"><button type="button" style="float:right;cursor:pointer" class="button anthracite-gradient" onclick="SendMail()">Send Mail</button>&nbsp;' +
            '</div>' +
            '</div></div></div>',

        title: 'Ticket Mail',
        width: 400,
        scrolling: false,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'anthracite-gradient glossy displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });

    HideButton();

}

function HideButton() {
    $(".displayNone").hide();
}

function SendMail() {
    ReservationId = GetQueryStringParamsForAgentRegistrationUpdate('ReservationId');
    Status = GetQueryStringParamsForAgentRegistrationUpdate('Status');
    if (Status == 'Tiketed' || Status == 'Cancelled') {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        var bValid = true;
        var sEmail = $("#txt_sendInvoice").val();
        if (sEmail == "") {
            Success("Please Insert Email.")
            bValid = false;
        }
        else {
            if (!(pattern.test(sEmail))) {
                bValid = false;
                Success("* Wrong email format.")
            }
        }

        if (bValid == true) {
            $.ajax({
                type: "POST",
                url: "handler/GenralHandler.asmx/SendFlightTicket",
                data: '{"sEmail":"' + sEmail + '","ReservationId":"' + ReservationId + '"}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result != true) {
                        Success("Some error occured, Please try again in some time.")
                        return false;
                    }
                    if (result == true) {
                        Success("Email has been sent successfully.")
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                    else if (result.retCode == 0) {
                        Success("Sorry Please Try Later.")
                        return false;
                    }
                },
                error: function () {
                    Success("Something Went Wrong")
                }

            });
        }
    }
    else {
        Success("Please Confirm Your Booking!!!!!")
    }
}


