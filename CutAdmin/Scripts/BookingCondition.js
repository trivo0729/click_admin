﻿

$(document).ready(function () {
    GetBookingCondition();
});
var arrBookingCondition = new Array();
var arrServiceName = new Array();
function GetBookingCondition() {

    $.ajax({
        type: "POST",
        url: "../handler/BookingConditionHandler.asmx/GetBookingCondition",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                arrBookingCondition = result.Arr;
                arrServiceName = result.ServiceName;
                GenrateConditionHtml();
            }
        },
        error: function () {
        }

    });
}



function GenrateConditionHtml() {
    $("#div_condition").empty();
    var html = '<div class="standard-tabs margin-bottom" id="add-tabs">';
    html += '<ul class="tabs">'
    for (var i = 0; i < arrBookingCondition.length; i++) {
        if (i == 0)
            html += '<li class="active"><a href="#' + arrBookingCondition[i].ServiceType + '">' + arrBookingCondition[i].ServiceType + '</a></li>'
        else
            html += '<li class=""><a href="#' + arrBookingCondition[i].ServiceType + '">' + arrBookingCondition[i].ServiceType + '</a></li>'
    }
    html += '</ul>'
    html += '<div class="tabs-content">'
    for (var i = 0; i < arrBookingCondition.length; i++) {
        html += '<div  class="with-padding" id="' + arrBookingCondition[i].ServiceType + '"><div class="respTable">'
        html += '<table class="table responsive-table" id="sorting-advanced">'
        html += '<thead>'
        html += '<tr>                                                                                          '
        html += '<th scope="col">Supplier</th>                                                                 '
        html += '<th scope="col">Time:</th>                                                   '
        html += '</tr> '
        html += '</thead><tbody>'
        var arrSupplier = arrBookingCondition[i].ListSupplier;
        
        for (var s = 0; s < arrSupplier.length; s++) {
            
            html += '<tr class="' + arrBookingCondition[s].ServiceType + '"><td><label class="SupplierName">' + arrSupplier[s].SupplierName + '</label></td>'
            html += '<td>'
            html += '<div id="div_selCondition' + i + s + '">'
                html += '    <select id = "sel_Condition' + i + s + '" name = "" class="full-width select validate[required] sel_Conditionn">'
                html += '        <option selected="" value="">Hours</option>                                                    '
                html += '        <option value="00">00</option>                                                                 '
                html += '        <option value="01">01</option>                                                                 '
                html += '        <option value="02">02</option>                                                                 '
                html += '        <option value="03">03</option>                                                                 '
                html += '        <option value="04">04</option>                                                                 '
                html += '        <option value="05">05</option>                                                                 '
                html += '        <option value="06">06</option>                                                                 '
                html += '        <option value="07">07</option>                                                                 '
                html += '        <option value="08">08</option>                                                                 '
                html += '        <option value="09">09</option>                                                                 '
                html += '        <option value="10">10</option>                                                                 '
                html += '        <option value="11">11</option>                                                                 '
                html += '        <option value="12">12</option>                                                                 '
                html += '        <option value="13">13</option>                                                                 '
                html += '        <option value="14">14</option>                                                                 '
                html += '        <option value="15">15</option>                                                                 '
                html += '        <option value="16">16</option>                                                                 '
                html += '        <option value="17">17</option>                                                                 '
                html += '        <option value="18">18</option>                                                                 '
                html += '        <option value="19">19</option>                                                                 '
                html += '        <option value="20">20</option>                                                                 '
                html += '        <option value="21">21</option>                                                                 '
                html += '        <option value="22">22</option>                                                                 '
                html += '        <option value="23">23</option>                                                                 '
                html += '                            </select >                                                                 '
                html += '</div>'
            html += '</td>'

                html += '</tr> '
        }
        html += '</tbody></table></div></div>'
    }
    html += '</div> '
    html += '</div><p class="SearchBtn"><input type="button" class="button anthracite-gradient buttonmrgTop" onclick="SaveCondition();" value="Save" /></p> '
    $("#div_condition").append(html);
    for (var j = 0; j < arrBookingCondition.length; j++)
    {
        for (var k = 0; k < arrBookingCondition[j].ListSupplier.length; k++) {
            $("#div_selCondition" + j + k + " .select span")[0].textContent = arrBookingCondition[j].ListSupplier[k].TimeCondition;
            $("#sel_Condition" + j + k + "").val(arrBookingCondition[j].ListSupplier[k].TimeCondition);
        }

    }
    
}

function GetCondition() {
    var arrCondition = new Array();
    try {
        for (var i = 0; i < arrBookingCondition.length; i++) {
            var ndSupplier = $('.' + arrBookingCondition[i].ServiceType);
            var ListSupplier = new Array();
            for (var s = 0; s < ndSupplier.length; s++) {
                var arrSupplier = $($(ndSupplier[s]).find('label')[0]).text()
                ListSupplier.push({
                    SupplierName: arrSupplier,
                    TimeCondition: $($(ndSupplier[s]).find('.sel_Conditionn option:selected')[0]).val(),
                });
            }
            arrCondition.push({ ServiceType: arrBookingCondition[i].ServiceType, ListSupplier: ListSupplier });
        }
    }
    catch (ex) {

    }
    return arrCondition
}



function SaveCondition() {
    var arrCondition = GetCondition();
    $.ajax({
        type: "POST",
        url: "../handler/BookingConditionHandler.asmx/SaveBookingCondition",
        data: JSON.stringify({ arrService: arrCondition }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.retCode == 1) {
                Success("Saved")
                GetBookingCondition();
            }
            else {
                Success("Something went wrong")
            }
        },
        error: function () {
        }

    });
}