﻿var VisaCode, HiddenId, UserId, Ammount = 0;
var sProcessing;
var dteArrival;
var dteDeparting;
var sType;
var sFirst;
var sMiddle;
var sLast;
var sFather;
var sMother;
var sHusband;
var sLanguage;
var sGender;
var sMarital;
var sNationality;
var sBirth;
var sDepartment;
var sBirthPlace;
var sBirthCountry;
var sReligion;
var sProfession;
var sPassport;
var sPassportType;
var sIssuing;
var sIssueDate;
var sExpirationDate;
var sAddress1;
var sAddress2;
var sCity;
var sCountry;
var sTelephone;
var sVisa;
var e;
var sArriAirLine;
var sArriFlight;
var sArrivalFrom;
var sDeptAirLine;
var sDeptFlight;
var sDeptFrom;
var Zip;
var sVisaCountry;
var bvalid;
var AgentId;
var sid;
$(document).ready(function () {

    $('#husband').css("display", "")
    //$("#Check-In").datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    dateFormat: "dd-mm-yy"
    //});
    //alert(today)
    $("#txtDob").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        maxDate: new Date(),
        //minDate: "-1D"
    });
    $("#txtDoi").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#txtED").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        minDate: "+3M +6D"
    });
    $("#dteArrival").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy"
    });
    $("#dteDeparting").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        //beforeShow: insertDepartureDate,
        //onSelect: insertDays,

    });
    if (location.href.indexOf('?') != -1) {
        $(':input').attr('disabled', 'disabled');
        $('#btn_Update').css("display", "none");

        VisaCode = GetQueryStringParamsForVisaApplication('RefNo');


        GetVisaApplication(VisaCode)
    }
});
function GetVisaApplication(VisaCode) {
    debugger;
    $("#tbl_VisaDetails tbody tr").remove();
    $.ajax({
        type: "POST",
        url: "../Handler/VisaDetails.asmx/GetVisaApplication",
        data: '{"VisaCode":"' + VisaCode + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                arrVisa = result.tbl_VisaApplication;
                var tRow = '';
                if (arrVisa.length > 0) {

                    for (i = 0; i < arrVisa.length; i++) {
                        HiddenId = arrVisa[i].sid;
                        UserId = arrVisa[i].UserId;
                        var marital = arrVisa[i].MaritalStatus;
                        if (marital == "1")
                            marital = "Single";
                        else if (marital == "2")
                            marital = "Married";
                        else if (marital == "3")
                            marital = "Dvorced";
                        else if (marital == "4")
                            marital = "Window";
                        //var Gender = arrVisa[i].Gender
                        //if (Gender == "1")
                        //    Gender = "Male";
                        //else if (Gender == "2")
                        //    Gender = "Female";
                        //var VisaType = arrVisa[i].Processing;
                        //if (VisaType == "1")
                        //    VisaType = "Normal";
                        //else if (VisaType == "2")
                        //    VisaType = "Urgent";
                        $("#selProcessing").val(arrVisa[i].Processing)
                        var txtProcessing = $("#selProcessing option:selected").text();
                        $("#DivProcessing .select span")[0].textContent = txtProcessing;
                        var Visa
                        var VisaImage;
                        $("#VisaImage").empty();
                        $("#PassportFirst").empty();
                        $("#PassportLast").empty();
                        VisaImage = '<img src="../VisaImages/' + VisaCode + "_" + (0 + 1) + '.jpg" id="' + (0 + 1) + '" "  height="190px" style="border:1px solid #000000"  onerror="imgError(this);"/>';
                        $("#VisaImage").append(VisaImage);
                        var PassportFirst
                        PassportFirst = '<img src="../VisaImages/' + VisaCode + "_" + (2) + '.jpg" id="' + (2) + '"  height="190px" style="border:1px solid #000000"  onerror="imgError(this);"/>'
                        $("#PassportFirst").append(PassportFirst);
                        var PassportLast
                        PassportLast = '<img src="../VisaImages/' + VisaCode + "_" + (3) + '.jpg" id="' + (3) + '"  height="190px" style="border:1px solid #000000"  onerror="imgError(this);"/>'
                        $("#PassportLast").append(PassportLast);
                        $("lblstatus").val(PassportFirst)
                        //$('#txtProcessing').val(VisaType);
                        $('#selVisaCountry').val(arrVisa[i].VisaCountry);
                        var txtVisaCountry = $("#selVisaCountry option:selected").text();
                        $("#DivVisaCountry .select span")[0].textContent = txtVisaCountry;
                        $('#txtService').val(arrVisa[i].IeService);
                        $('#VisaFee').text(Math.round(arrVisa[i].VisaFee));
                        $('#ServiceTax').text(Math.round(arrVisa[i].ServiceTax));
                        $('#UrgentFee').text(Math.round(arrVisa[i].UrgentFee));
                        $('#OtherFee').text(Math.round(arrVisa[i].OtherFee));
                        $('#TotalAmount').text(Math.round(arrVisa[i].TotalAmount));
                        Ammount = arrVisa[i].TotalAmount;
                        $('#txtFirst').val(arrVisa[i].FirstName);
                        $('#txtMiddle').val(arrVisa[i].MiddleName);
                        $('#txtLast').val(arrVisa[i].LastName);
                        $('#txtFather').val(arrVisa[i].FatherName);
                        $('#txtMother').val(arrVisa[i].MotherName);
                        $('#txtHusband').val(arrVisa[i].HusbandMame);
                        $('#selLanguage').val(arrVisa[i].Language);
                        var txtLanguage = $("#selLanguage option:selected").text();
                        $("#DivLanguage .select span")[0].textContent = txtLanguage;
                        $('#selGender').val(arrVisa[i].Gender);
                        var textGender = $("#selGender option:selected").text();
                        $("#DivGender .select span")[0].textContent = textGender;
                        $('#selMarital').val(arrVisa[i].MaritalStatus);
                        var textMarital = $("#selMarital option:selected").text();
                        $("#DivMarital .select span")[0].textContent = textMarital;
                        $('#selNationality').val(arrVisa[i].PresentNationality);
                        var textNationality = $("#selNationality option:selected").text();
                        $("#DivNationality .select span")[0].textContent = textNationality;
                        $('#txtDob').val(arrVisa[i].Birth);
                        $('#txt_BirthPlace').val(arrVisa[i].BirthPlace);
                        $('#selCountry1').val(arrVisa[i].BirthCountry);
                        var textCountry1 = $("#selCountry1 option:selected").text();
                        $("#DivCountry1 .select span")[0].textContent = textCountry1;
                        $('#selReligion').val(arrVisa[i].Religion);
                        var textReligion = $("#selReligion option:selected").text();
                        $("#DivReligion .select span")[0].textContent = textReligion;
                        $('#selProfession').val(arrVisa[i].Profession);
                        var textProfession = $("#selProfession option:selected").text();
                        $("#DivProfession .select span")[0].textContent = textProfession;
                        $('#txtPassport').val(arrVisa[i].PassportNo);
                        //$('#txtPassport').val(arrVisa[i].PassportType);
                        $('#txtIssuing').val(arrVisa[i].IssuingGovernment);
                        $('#txtDoi').val(arrVisa[i].IssuingDate);
                        $('#txtED').val(arrVisa[i].ExpDate);
                        $('#txtAddress1').val(arrVisa[i].AddressLine1);
                        $('#txtAddress2').val(arrVisa[i].AddressLine2);
                        $('#selCountry').val(arrVisa[i].Country);
                        var textCountry = $("#selCountry option:selected").text();
                        $("#DivCountry .select span")[0].textContent = textCountry;
                        $('#selCity').val(arrVisa[i].City);
                        $('#txtTelephone').val(arrVisa[i].Telephone);
                        //$('#selProcessing').val(arrVisa[i].Vcode);
                        $('#dteArrival').val(arrVisa[i].ArrivalDate);
                        $('#dteDeparting').val(arrVisa[i].DepartingDate);
                        $('#lblrefno').val(arrVisa[i].Vcode);
                        $("#selAirLine").val(arrVisa[i].ArrivalAirLine)
                        var textAirLine = $("#selAirLine option:selected").text();
                        $("#DivselAirLine .select span")[0].textContent = arrVisa[i].ArrivalAirLine;
                        $('#txtflightNo').val(arrVisa[i].ArrivalflightNo)
                        $('#txtArrivalfrom').val(arrVisa[i].Arrivalfrom)
                        $('#selDeprtureAirLine').val(arrVisa[i].DeprtureAirLine)
                        var textDeprtureAirLine = $("#selDeprtureAirLine option:selected").text();
                        $("#DivDeprtureAirLine .select span")[0].textContent = arrVisa[i].DeprtureAirLine;
                        $('#txtDeprtureflightNo').val(arrVisa[i].DeprtureflightNo)
                        $('#txtDeprturefrom').val(arrVisa[i].Deprturefrom);
                        $('#ZIP1').val(arrVisa[i].ZIPCode)
                        $("#lblrefno").val(arrVisa[i].VCode)
                        $("#txt_ApplicationNo").val(arrVisa[i].TReference)
                        $("#txt_VisaNo").val(arrVisa[i].VisaNo)
                        $("#txt_Sponsor").val(arrVisa[i].Sponsor)
                        $('#UploadFirstCopy').css("display", "none")
                        $('#UploadPassportFirst').css("display", "none")
                        $('#UploadPassportLast').css("display", "none")
                        $("#selService").val(GetVisaTypes(arrVisa[i].IeService))
                        var txtService = $("#selService option:selected").text();
                        $("#DivService .select span")[0].textContent = txtService;
                        sid = arrVisa[i].sid;
                    }
                }
            }
        },
        error: function () {
            alert('error')
        }
    });
}
function GetVisaTypes(Visa) {
    if (Visa == "96 hours Visa transit Single Entery (Compulsory Confirm Ticket Copy)") {
        Visa = "6"
    }
    else if (Visa == "14 days Service VISA") {
        Visa = "1"
    }
    else if (Visa == "30 days Tourist Single Entry") {
        Visa = "2"
    }
    else if (Visa == "30 days Tourist Multiple Entry") {
        Visa = "3"
    }
    else if (Visa == "90 days Tourist Single Entry") {
        Visa = "4"
    }
    else if (Visa == "90 days Tourist Multiple Entry") {
        Visa = "5"
    }
    else if (Visa == "90 Days Tourist Single Entry Convertible") {
        Visa = "7";
    }
    return Visa;
}
function imgError(image) {
    image.onerror = "";
    image.src = "../VisaImages/logo.png";
    return true;
}
function GetQueryStringParamsForVisaApplication(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}
function GetBirthCountry(BirthCountry) {
    $.ajax({
        type: "POST",
        url: "../Admin/VisaDetails.asmx/GetCountry",
        data: '{"Code":"' + BirthCountry + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Country = result.tbl_VisaCountry;
                $('#lblbCountry').val(Country[0].Country)

            }
        },
        error: function () {
            alert("An error occured")
        }
    });
}
function GetPresentNationality(PresentNationality) {
    $.ajax({
        type: "POST",
        url: "../Admin/VisaDetails.asmx/GetCountry",
        data: '{"Code":"' + PresentNationality + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Country = result.tbl_VisaCountry;
                $('#lblCountry').val(Country[0].Country);
            }
        },
        error: function () {
            alert("An error occured")
        }
    });
}
function GetCountry(Countryes) {
    $.ajax({
        type: "POST",
        url: "../Admin/VisaDetails.asmx/GetCountry",
        data: '{"Code":"' + Countryes + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Country = result.tbl_VisaCountry;
                $('#Country1').val(Country[0].Country);
            }
        },
        error: function () {
            alert("An error occured")
        }
    });
}
var Lang
var UpdateEnable = false;
function GetLanguages(Id) {
    debugger;
    $.ajax({
        type: "POST",
        url: "../Admin/VisaDetails.asmx/GetLanguages",
        data: '{"Code":"' + Id + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                Lang = result.tbl_Languaes;
                $('#Language1').val(Lang[0].Language);
                // $('#Language1').val(arrVisa[0].Language1);
            }
        },
        error: function () {
            alert("An error occured")
        }
    });
    //$('#selCity option:selected').val(tempcitycode);
}
function UpdateApplication() {
    if (UpdateEnable == false) {
        //$(":input").removeAttr("readonly");
        //$('#btn_Update').css("display", "")
        //$('#UploadFirstCopy').css("display", "")
        //$('#UploadPassportFirst').css("display", "")
        //$('#UploadPassportLast').css("display", "")
        //UpdateEnable=true
        window.location.href = "AddVisaDetails.aspx?Vcode=" + VisaCode
    }
    else {
        $(':input').attr('disabled', 'disabled');
        $('#btn_Update').css("display", "none");
        $('#UploadFirstCopy').css("display", "none")
        $('#UploadPassportFirst').css("display", "none")
        $('#UploadPassportLast').css("display", "none")
        UpdateEnable = false
    }

}
function Validate_VisaRegisteration() {
    debugger;
    if ($("#txt_Agent").val() == "") {
        $("#txt_Agent").focus();
        $("#lbl_txt_Agent").css("display", "");
        return false;
    }
    if (sProcessing == "") {
        $("#selProcessing").focus();
        $("#lbl_selProcessing").css("display", "");
        return false;
    }
    $("#lbl_selProcessing").text('');
    //sType = $('#selService').val();
    if (sType == "--Salect Visa Type--") {
        $("#selService").focus();
        $("#lbl_selService").css("display", "");
        return false;
    }
    $("#lbl_selService").text('');
    if ($('#selVisaCountry').val() == "-") {
        $("#selVisaCountry").focus();
        $("#lbl_selVisaCountry").css("display", "");
        return false;
    }
    $("#lbl_selVisaCountry").text('');
    if (sFirst == "") {
        $("#txtFirst").focus();
        $("#lbl_txtFirst").css("display", "");
        bvalid = false;
    }
    //$("#lbl_txtFirst").text('');
    //if (sMiddle == "") {
    //    $("#txtMiddle").focus();
    //    $("#lbl_txtMiddle").css("display", "");
    //    return false;
    //}
    //$("#lbl_txtMiddle").text('');
    if (sLast == "") {
        $("#txtLast").focus();
        $("#lbl_txtLast").css("display", "");
        bvalid = false;
    }
    //$("#lbl_txtLast").text('');
    if (sFather == "") {
        $("#txtFather").focus();
        $("#lbl_txtFather").css("display", "");
        bvalid = false;
    }
    // $("#lbl_txtFather").text('');
    if (sMother == "") {
        $("#txtMother").focus();
        $("#lbl_txtMother").css("display", "");
        bvalid = false;
    }
    //$("#lbl_txtMother").text('');
    sGender = $('#selGender').val();
    var selMarital = $('#selMarital').val();
    $("#lbl_txtHusband").css("display", "none");
    if (sGender == "2" && selMarital == "2" || selMarital == "3" || selMarital == "4") {
        if ($("#txtHusband").val() == "") {
            $("#txtHusband").focus();
            $("#lbl_txtHusband").css("display", "");
            bvalid = false;
        }
    }
    //$("#lbl_txtHusband").text('');
    if (sLanguage == "-") {
        $("#selLanguage").focus();
        $("#lbl_selLanguage").css("display", "");
        bvalid = false;
    }
    //$("#lbl_selLanguage").text('');
    if (sGender == "-") {
        $("#selGender").focus();
        $("#lbl_selGender").css("display", "");
        bvalid = false;
    }
    else if (sGender == "Male") {
        sHusband = "";
    }
    //$("#lbl_selGender").text('');
    if (sMarital == "-") {
        $("#selMarital").focus();
        $("#lbl_selMarital").css("display", "");
        bvalid = false;
    }
    //$("#lbl_selMarital").text('');
    if (sNationality == "") {
        $("#selNationality").focus();
        $("#lbl_selNationality").css("display", "");
        return false;
    }
    //$("#lbl_selNationality").text('');
    if (sBirth == "") {
        $("#txtDob").focus();
        $("#lbl_txtDob").css("display", "");
        bvalid = false;
    }
    //$("#lbl_txtDob").text('');
    if (sBirthPlace == "") {
        $("#txt_BirthPlace").focus();
        $("#lbl_txt_BirthPlace").css("display", "");
        bvalid = false;
    }
    $("#lbl_txtBirthPlace").text('');
    if (sBirthCountry == "") {
        $("#selCountry").focus();
        $("#lbl_selCountry").css("display", "");
        bvalid = false;
    }
    //$("#lbl_selCountry").text('');
    if (sReligion == "-") {
        $("#selReligion").focus();
        $("#lbl_selReligion").css("display", "");
        bvalid = false;
    }
    //$("#lbl_selReligion").text('');
    if (sProfession == "--Select Profession--") {
        $("#selProfession").focus();
        $("#lbl_selProfession").css("display", "");
        bvalid = false;
    }
    //$("#lbl_selProfession").text('');
    if (sPassport == "") {
        $("#txtPassport").focus();
        $("#lbl_txtPassport").css("display", "");
        bvalid = false;
    }
    //$("#lbl_txtPassport").text('');
    //$("#lbl_selPassportType").text('');
    if (sIssuing == "") {
        $("#txtIssuing").focus();
        $("#lbl_txtIssuing").css("display", "");
        bvalid = false;
    }
    if (sIssueDate == "") {
        //$("#txtDoi").focus();
        $("#lbl_txtDoi").css("display", "");
        bvalid = false;
    }
    //$("#lbl_txtDoi").text('');
    if (sExpirationDate == "") {
        //$("#txtED").focus();
        $("#lbl_txtED").css("display", "");
        bvalid = false;
    }
    //$("#lbl_txtED").text('');
    if (sAddress1 == "") {
        $("#txtAddress1").focus();
        $("#lbl_txtAddress1").css("display", "");
        bvalid = false;
    }
    //$("#lbl_txtAddress1").text('');
    if (sAddress2 == "") {
        $("#txtAddress2").focus();
        $("#lbl_txtAddress2").css("display", "");
        bvalid = false;
    }
    //$("#lbl_txtAddress2").text('');
    if (sCity == "") {
        $("#txtCity").focus();
        $("#lbl_txtCity").css("display", "");
        bvalid = false;
    }
    //$("#lbl_txtAddress2").text('');
    if (sCountry == "") {
        $("#selCountries").focus();
        $("#lbl_selCountries").css("display", "");
        bvalid = false;
    }
    //$("#lbl_selCountries").text('');
    var ChekType = $("#selService").val();
    if (ChekType == "6" || ChekType == "96 hours Visa transit Single Entery (Compulsory Confirm Ticket Copy)") {
        $("#lbl_selAirLine").css("display", "none");
        if ($("#selAirLine option:selected").text() == "-Select AirLine-") {
            $("#selAirLine").focus();
            $("#lbl_selAirLine").css("display", "");
            bvalid = false;
        }
        //$("#lbl_selAirLine").text('');
        $("#lbl_txtflightNo").css("display", "none");
        if ($("#txtflightNo").val() == "") {
            $("#txtflightNo").focus();
            $("#lbl_txtflightNo").css("display", "");
            bvalid = false;
        }
        //$("#lbl_txtflightNo").text('');
        $("#lbl_txtArrivalfrom").css("display", "none");
        if ($("#txtArrivalfrom").val() == "") {
            $("#txtArrivalfrom").focus();
            $("#lbl_txtArrivalfrom").css("display", "");
            bvalid = false;
        }
        //$("#lbl_txtArrivalfrom").text('')
        $("#lbl_dteArrival").css("display", "none");
        if (dteArrival == "") {
            $("#sel_Member").focus();
            $("#lbl_dteArrival").css("display", "");
            bvalid = false;
        }
        //$("#lbl_dteArrival").text('');
        $("#lbl_selDeprtureAirLine").css("display", "none");
        if ($("#selDeprtureAirLine option:selected").text() == "-Select AirLine-") {
            $("#selDeprtureAirLine").focus();
            $("#lbl_selDeprtureAirLine").css("display", "");
            bvalid = false;
        }
        //$("#lbl_selDeprtureAirLine").text('');
        $("#lbl_txtDeprtureflightNo").css("display", "none");
        if ($("#txtDeprtureflightNo").val() == "") {
            $("#txtDeprtureflightNo").focus();
            $("#lbl_txtDeprtureflightNo").css("display", "");
            bvalid = false;
        }
        //$("#lbl_txtDeprtureflightNo").text('');
        $("#lbl_txtDeprturefrom").css("display", "none");
        if ($("#txtDeprturefrom").val() == "") {
            $("#txtDeprturefrom").focus();
            $("#lbl_txtDeprturefrom").css("display", "");
            bvalid = false;
        }
        //$("#lbl_txtDeprturefrom").text('')
        $("#lbl_dteDeparting").css("display", "none");
        if (dteDeparting == "") {
            $("#selSponsor").focus();
            $("#lbl_dteDeparting").css("display", "");
            bvalid = false;
        }
        //$("#lbl_dteDeparting").text('');
    }

    return bvalid;
}
function UpdateStatus() {
    sProcessing = $('#selProcessing').val();
    e = document.getElementById("selService");
    sType = e.options[e.selectedIndex].text;
    sFirst = $('#txtFirst').val();
    sMiddle = $('#txtMiddle').val();
    sLast = $('#txtLast').val();
    sFather = $('#txtFather').val();
    sMother = $('#txtMother').val();
    sHusband = $('#txtHusband').val();
    sLanguage = $('#selLanguage').val();
    sGender = $('#selGender').val();
    sMarital = $('#selMarital').val();
    sNationality = $('#selNationality').val();
    sBirth = $('#txtDob').val();
    sDepartment = $('#selDepartment').val();
    sBirthPlace = $('#txt_BirthPlace').val();
    e = document.getElementById("selCountry1");
    sBirthCountry = $('#selCountry1').val();
    sReligion = $('#selReligion').val();
    e = document.getElementById("selProfession");
    sProfession = e.options[e.selectedIndex].text;
    sPassport = $('#txtPassport').val();
    sIssuing = $('#txtIssuing').val();
    sIssueDate = $('#txtDoi').val();
    sExpirationDate = $('#txtED').val();
    sAddress1 = $('#txtAddress1').val();
    sAddress2 = $('#txtAddress2').val();
    sCity = $('#txtCity').val();
    e = document.getElementById("");
    sCountry = $('#selCountry').val();
    sTelephone = $('#txtTelephone').val();
    if (sTelephone = "")
        sTelephone = "0";
    sVisa = $('#Ref').text();
    dteArrival = $('#dteArrival').val();
    dteDeparting = $('#dteDeparting').val();
    e = document.getElementById("selAirLine");
    sArriAirLine = e.options[e.selectedIndex].text;
    sArriFlight = $('#txtflightNo').val();
    sArrivalFrom = $('#txtArrivalfrom').val();
    e = document.getElementById("selDeprtureAirLine");
    sDeptAirLine = e.options[e.selectedIndex].text;
    sDeptFlight = $('#txtDeprtureflightNo').val();
    sDeptFrom = $('#txtDeprturefrom').val();
    Zip = "0";
    sVisaCountry = $('#selVisaCountry option:selected').text();
    AgentId = $('#hdnDCode').val()
    var VisaFee, OtherFee, UrgentFee, ServiceTax, TotalAmount;

    if ($("#lbl_Manual").text() == "false") {

        VisaFee = $('#VisaFee').text();
        OtherFee = $('#OtherFee').text();
        UrgentFee = $('#UrgentFee').text();
        ServiceTax = $('#ServiceTax').text();
        TotalAmount = $('#TotalAmount').text();

    }
    else {
        VisaFee = $('#txt_VisaFee').val();
        OtherFee = $('#txt_OtherFee').val();
        UrgentFee = $('#txt_UrgentFee').val();
        ServiceTax = $('#txt_ServiceTax').val();
        TotalAmount = $('#txt_TotalAmount').val();

    }
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = dd + '-' + mm + '-' + yyyy;
    bvalid = true;
    bvalid = Validate_VisaRegisteration();
    if (bvalid == true) {
        var dataToPass = {
            sProcessing: sProcessing,
            dteArrival: dteArrival,
            dteDeparting: dteDeparting,
            sType: sType,
            sFirst: sFirst,
            sMiddle: sMiddle,
            sLast: sLast,
            sFather: sFather,
            sMother: sMother,
            sHusband: sHusband,
            sLanguage: sLanguage,
            sGender: sGender,
            sMarital: sMarital,
            sNationality: sNationality,
            sBirth: sBirth,
            sBirthPlace: sBirthPlace,
            sBirthCountry: sBirthCountry,
            sReligion: sReligion,
            sProfession: sProfession,
            sPassport: sPassport,
            sIssuing: sIssuing,
            sIssueDate: sIssueDate,
            sExpirationDate: sExpirationDate,
            sAddress1: sAddress1,
            sAddress2: sAddress2,
            sCity: sCity,
            sCountry: sCountry,
            sTelephone: sTelephone,
            Zip: Zip,
            sVisa: sVisa,
            sArriAirLine: sArriAirLine,
            sArriFlight: sArriFlight,
            sArrivalFrom: sArrivalFrom,
            sDeptAirLine: sDeptAirLine,
            sDeptFlight: sDeptFlight,
            sDeptFrom: sDeptFrom,
            VisaFee: VisaFee,
            OtherFee: OtherFee,
            UrgentFee: UrgentFee,
            ServiceTax: ServiceTax,
            TotalAmount: TotalAmount,
            sVisaCountry: sVisaCountry,
            AppDate: today,
            AgentId: UserId
        };
        var jsonText = JSON.stringify(dataToPass);
        $.ajax({
            type: "POST",
            url: "VisaHandler.asmx/Update",
            data: jsonText,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    alert("Registeration successfully,Please  Upload Images");
                    window.location.href = "ImagesUpload.aspx?Image=" + sVisa;
                }
                if (result.retCode == 0) {
                    alert("Something went wrong!");
                }
            },
            error: function () {
                alert("An error occured while  Adding Details");
            }
        });
    }
}
function UploadDocument() {
    var fileUpload = $("#UploadFirstCopy").get(0);
    var files = fileUpload.files;
    FNOUT = files[0].name;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    $("#img_Passport").css("display", "")
    $("#lbl_VisaImage").css("display", "none")
    $.ajax({
        url: "UploadDocument.ashx",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result != "Only PDF/jpg/doc file is allowed") {
                //$("#VisaImage").empty()
                $("#img_Passport").css("display", "none")
                $("#lbl_VisaImage").css("display", "none")
            }
            else {
                $("#img_Passport").css("display", "none")
                $("#lbl_VisaImage").html("Please Insert .jpg format onnly")
                $("#lbl_VisaImage").css("display", "")
            }
        },

    });
}
function UploadFirstPage() {
    var fileUpload = $("#UploadPassportFirst").get(0);
    var files = fileUpload.files;
    FNOUT = files[0].name;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    $("#img_UploadOutbound").css("display", "")
    $("#lbl_UploadOutbound").css("display", "none")
    $.ajax({
        url: "PassportFirstPage.ashx",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result != "Only jpg file is allowed") {
                $("#PassportFirst").append(PassportFirst);

            }
            else {
                $("#img_UploadOutbound").css("display", "none")
                $("#lbl_UploadOutbound").html("An Error Occured!!")
                $("#lbl_UploadOutbound").css("display", "")
            }
        },

    });
}
function UploadLastPage() {
    var fileUpload = $("#UploadPassportLast").get(0);
    var files = fileUpload.files;
    FNOUT = files[0].name;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(files[i].name, files[i]);
    }
    $("#img_UploadOutbound").css("display", "")
    $("#lbl_UploadOutbound").css("display", "none")
    $.ajax({
        url: "PassportLastPage.ashx",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result != "Only jpg file is allowed") {
                $("#lbl_UploadOutbound").css("display", "none")
                $("#img_UploadOutbound").css("display", "none")
            }
            else {
                $("#img_UploadOutbound").css("display", "none")
                $("#lbl_UploadOutbound").html("An Error Occured!!")
                $("#lbl_UploadOutbound").css("display", "")
            }
        },

    });
}
function Rejected() {
    //if (confirm("Are you Really want to Reject this Application..") == true)
    //{


    //}
    Ok("Are you Really want to Reject this Application..", "Reject", null)

}
function Reject() {
    $.ajax({
        type: "POST",
        url: "VisaHandler.asmx/RejectApplication",
        data: '{"Vcode":"' + VisaCode + '","AgentId":"' + UserId + '","DepositAmmount":"' + Ammount + '"}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.Session == 0) {
                return false;
            }
            if (result.retCode == 1) {
                $("#StatusModal").modal("hide")
                alert("Application Rejected")
                window.location.href = "VisaDetails.aspx";
            }
            else {
                alert("Something went wrong! Please contact administrator.");
            }
        },
        error: function () {
        }
    });
}
function GetPrintInvoice() {
    window.open('../Agent/VisaInvoice.html?RefNo=' + VisaCode + '&Uid=' + UserId, 'Print Invoice', 'left=5000,top=5000,width=800,height=600');
    //window.location.href = "../Agent/VisaInvoice.html?RefNo='" + RefNo + '&Uid=' + sid;
}
//window.onload = function () {
//    var image = document.getElementById("1");

//    function updateImage() {
//        var image = document.getElementById("1");
//        image.src = image.src.split("?")[0] + "?" + new Date().getTime();
//    }
//    function updateFirst() {
//        var image = document.getElementById("2");
//        image.src = image.src.split("?")[0] + "?" + new Date().getTime();
//    }
//    function updateLast() {
//        var image = document.getElementById("3");
//        image.src = image.src.split("?")[0] + "?" + new Date().getTime();
//    }

//    setInterval(updateImage, 500);
//    setInterval(updateFirst, 600);
//    setInterval(updateLast, 700);

//}