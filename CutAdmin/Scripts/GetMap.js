﻿function GetMap(HotelCode) {
   // var HotelCode = $('#listHotels option').filter(function () { return this.value == HotelName; }).data('id');
    $.ajax({
        type: "POST",
        url: "../handler/GenralHandler.asmx/GetHotelInfo",
        data: JSON.stringify({ HotelCode: HotelCode }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var objHotel = result.arrHotel;
                $.modal({
                    content: '<div > <div id="map"></div></div>',
                    title: objHotel.HotelName,
                    width: 800,
                    height: 400,
                    scrolling: false,
                });
                debugger;
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 12,
                    center: new google.maps.LatLng(objHotel.HotelLatitude, objHotel.HotelLangitude),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                var infowindow = new google.maps.InfoWindow();
                var marker;
                var location = {};
                var Result = []

                location = {
                    name: objHotel.HotelName,
                    Image: objHotel.HotelImage,
                    pointlat: parseFloat(objHotel.HotelLatitude),
                    pointlng: parseFloat(objHotel.HotelLangitude),
                    Rating: objHotel.HotelCategory,
                    Description: objHotel.HotelDescription,
                };
                console.log(location);
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(location.pointlat, location.pointlng),
                    map: map,
                    zoom: 12,
                });
                setTimeout(function myfunction() {
                    $("#map").removeAttr("style");
                },500)
            }
            else {
                Success("Something Went Wrong")
                return false;
            }
        }
    })

}


function GetMapAll() {
    setTimeout(GetMap(), 300);
}


