﻿$(document).ready(function () {
    GetPackageDetails();
});

function GetCategoryName(categoryID) {
    if (categoryID == 1) {
        return "Standard";
    }
    else if (categoryID == 2) {
        return "Deluxe";
    }
    else if (categoryID == 3) {
        return "Premium";
    }
    else if (categoryID == 4) {
        return "Luxury";
    }
}

function GetPackageType(id) {
    if (id == 1) {
        return "Holidays";
    }
    else if (id == 2) {
        return "Umrah";
    }
    else if (id == 3) {
        return "Hajj";
    }
    else if (id == 4) {
        return "Honeymoon";
    }
    else if (id == 5) {
        return "Summer";
    }
    else if (id == 6) {
        return "Adventure";
    }
    else if (id == 7) {
        return "Deluxe";
    }
    else if (id == 8) {
        return "Business";
    }
    else if (id == 9) {
        return "Premium";
    }
    else if (id == 10) {
        return "Wildlife";
    }
    else if (id == 11) {
        return "Weekend";

    }
    else if (id == 12) {
        return "New Year";
    }
    else {
        return "No Theme";
    }
}
function GetPackageDetails() {
    debugger;
    $("#tbl_PackageDetails").dataTable().fnClearTable();
    $("#tbl_PackageDetails").dataTable().fnDestroy();
    $.ajax({
        url: "../Handler/ViewPackageHandler.asmx/GetPackageDetails",
        type: "Post",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.Session == 0) {
                window.location.href = "index.htm";
            }
            if (result.retCode == 0) {
                alert("No packages found");
            }
            else if (result.retCode == 1) {
                List_Packages = result.List_Packages;
                var tRow = '';
                for (var i = 0; i < List_Packages.length; i++) {
                    var sCategory = List_Packages[i].sPackageCategory.split(',');
                    var sCategoryName = "";
                    //if (sCategory.length == 1) {
                    for (var k = 0; k < sCategory.length; k++) {
                        if (sCategory[k] != "") {
                            sCategoryName = sCategoryName + "," + GetCategoryName(sCategory[k]);
                        }
                    }
                    //}
                    //else {
                    //    for (var k = 0; k < sCategory.length - 1; k++) {
                    //        sCategoryName = sCategoryName + "," + GetCategoryName(sCategory[k]);
                    //    }
                    //}


                    var dfrom = (List_Packages[i].dValidityFrom).split(' ');

                    var dTO = (List_Packages[i].dValidityTo).split(' ');

                    var city = (List_Packages[i].sPackageDestination).split('|');
                    tRow += '<tr>';
                    tRow += '<td class="align-center">' + List_Packages[i].sPackageName + '</td>';
                    tRow += '<td class="align-center">' + city[0] + '</td>';
                    tRow += '<td class="align-center">' + List_Packages[i].nDuration + ' Days</td>';
                    tRow += '<td class="align-center">' + sCategoryName.replace(/^,|,$/g, ''); + '</td>';
                    tRow += '<td class="align-center">' + dfrom[0] + '</td>';
                    tRow += '<td class="align-center">' + dTO[0] + '</td>';
                    if (List_Packages[i].Status == true)
                        tRow += '<td class="align-center" id="' + List_Packages[i].nID + '"><input type="checkbox" id="chk' + List_Packages[i].nID + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"  checked  onchange="PackageStatus(' + List_Packages[i].nID + ',\'Deactive\')"></td>';
                    else
                        tRow += '<td class="align-center" id="' + List_Packages[i].nID + '"><input type="checkbox" id="chk' + List_Packages[i].nID + '" name="medium-label-3" id="medium-label-3" class="switch tiny" value="1"   onchange="PackageStatus(' + List_Packages[i].nID + ',\'Active\')"></td>';


                    //tRow += '<td class="align-center"><a class="button" title="Update"><span class="icon-pencil " style="cursor:pointer" title="Click here to View package detail" onclick="PackageDetails(' + List_Packages[i].nID + ')"></span></a></td>';
                    //tRow += '<td data-title="Click here to delete the package" style="text-align:center;"><a style="cursor:pointer" href="#"><span class="icon-trash" title="Click here to delete the package" style="cursor:pointer" onclick="DeletePackage(' + List_Packages[i].nID + ')"></span></a></td>';
                    tRow += '<td class="align-center" style="vertical-align:middle;">';
                    tRow += '<div class="button-group compact">';
                    tRow += '<a class="button icon-pencil with-tooltip" title="Edit Details" onclick="PackageDetails(' + List_Packages[i].nID + ')"></a>'
                    tRow += '<a class="button icon-trash with-tooltip confirm" title="Delete" onclick="DeletePackage(' + List_Packages[i].nID + ')"></a>'
                    tRow += '<a class="button icon-new-tab with-tooltip" title="Mail" onclick="MailPackage(\'' + List_Packages[i].nID + '\',\'' + List_Packages[i].sPackageName.replace(" ", "") + '\')"></a>'
                    tRow += '</div ></td>';
                    tRow += '</tr>';
                }

                $("#tbl_PackageDetails tbody").html(tRow);
                $("#tbl_PackageDetails").dataTable({
                    bSort: false,
                    sPaginationType: 'full_numbers',
                    sSearch: false
                });
                document.getElementById("tbl_PackageDetails").removeAttribute("style")
            }
        },
        error: function () {
            alert('Error in getting package!');
        }
    });
}
////// PackageDetails.aspx linked required here //////
function PackageDetails(ID) {

    window.location.href = "AddPackage.aspx?ID=" + ID;

}

function DeletePackage(nID) {
    debugger;
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure want to delete the package<br/> <span class=\"orange\">' + nID + '</span>?</span>?</p>',
        function () {

            $.ajax({
                url: "../Handler/PackageDetailHandler.asmx/DeletePackage",
                type: "post",
                data: '{"nID":"' + nID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        GetPackageDetails();
                        Success("Package deleted successfully");
                    } else if (result.retCode == 0) {
                        Success("Error in deleting package");
                        setTimeout(function () {
                            window.location.reload();
                        }, 500)
                    }
                },
                error: function () {
                    Success('Error occured while processing your request! Please try again.');
                    setTimeout(function () {
                        window.location.reload();
                    }, 500)
                }
            });

        },
        function () {
            $('#modals').remove();
        }
    );
}

function PackageStatus(nID, Status) {
    var StatusNew = "";
    if (Status == "Active")
        StatusNew = "Activate"
    else
        StatusNew = "Deactivate"
    $.modal.confirm('<p style="font-size:15px" class="avtiveDea">Are you sure want to ' + StatusNew + ' the package</p>',
        function () {
            $.ajax({
                url: "../Handler/PackageDetailHandler.asmx/PackageStatus",
                type: "post",
                data: '{"nID":"' + nID + '","Status":"' + Status + '"}',
                contentType: "application/json; charset=utf-8 ",
                dataType: "json",
                success: function (data) {
                    var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                    if (result.retCode == 1) {
                        Success("Status Updated successfully");
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);

                        return false;
                    }
                    else {
                        alert("Error in Update package");
                        return false;
                    }
                },
                error: function () {

                }
            });
        },
        function () {
            $('#modals').remove();
        }
    );
}

var List_PackageDetail;
var List_pricingDetail;
var PricingDetailnCount;
var List_ItineraryDetail;
var ItineraryDetailnCount;
var List_ItineraryHotelDetail;
var List_ImageDetail;
var List_TransferDetail;
var List_PackageActivityDetail;
var PackageActivityDetailnCount;

function MailPackage(nID, Name)
{
   ViewPackageDetails(nID)
}

function ViewPackageDetails(nID) {
    $.modal({
        //content: '<div class="respTable">' +
        //  '<table class="table responsive-table evenodd" id="sorting-advanced">' +
        //   '<tr> ' +
        //  ' <td class="even">Email ID</td><td><input type="text" id="EmailID"  class="input full-width" /></td>    ' +
        //   ' <td class="even">Name</td><td><input type="text" id="txt_Name"  class="input full-width" /></td>    ' +
        //  '</tr>' +
        //   '<tr> ' +
        //   '<td class="even">Remark</td><td><input type="text" id="txt_Remark"  class="input full-width" /></td>    ' +
        //    '<td class="even"></td><td class="even"></td>' +
        //  '</tr>' +
        //  '</table>' +
        //  '<input type="button"  value="Send"  class="button anthracite-gradient" onclick="SendPackagesHtml(\'' + nID + '\')" />' +
        //  '</div>',

        content: '<div class="modal-body">' +
                '<div class="scrollingDiv">' +
                '<div class="columns">' +
                '<div class="one-columns bold">Email ID : </div>' +
                  '<div class="four-columns "><input type="text" id="EmailID"  class="input full-width" /></div>' +
                '<div class="one-columns bold">Name : </div>' +
                  '<div class="four-columns "><input type="text" id="txt_Name"  class="input full-width" /></div>' +
                '</div>' +
                '<div class="columns">' +
                '<div class="one-columns bold">Remark : </div>' +
                 '<div class="nine-columns"> <textarea type="text" id="txt_Remark"  class="input full-width" ></textarea></div>' +
                     '</div>' +
                   '<div class="columns">' +
                '<div class="ten-columns bold"><button type="button" style="width:15%;float:right" class="button anthracite-gradient" onclick="SendPackagesHtml(\'' + nID + '\')">Send</button>' +
                '</div></div></div>',

        title: 'Send Package Detail',
        width: 800,
        scrolling: true,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) { win.closeModal(); }
            }
        },
        buttons: {
            'Close': {
                classes: 'huge anthracite-gradient displayNone',
                click: function (win) { win.closeModal(); }
            }
        },
        buttonsLowPadding: true
    });
}

var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

function SendPackagesHtml(nID)
{
    var Email = $("#EmailID").val();
    if(Email=="")
    {
        alert("Enter Email ID");
        return false;
    }
    else if (!emailReg.test(Email))
    {
        document.getElementById('EmailID').focus();
        alert("The email address you have entered is invalid");
        return false;
    }

    var Name = $("#txt_Name").val();
    if (Name == "")
    {
        alert("Enter Name");
        return false;
    }

    var Remark = $("#txt_Remark").val();

    $.ajax({
        type: "POST",
        url: "GenralHandler.asmx/SendPackagesDetails",
        data: '{"ID":"' + nID + '","Email":"' + Email + '","Name":"' + Name + '","Remark":"' + Remark + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1)
            {
                Success("Mail package detail send successfully.");
                setTimeout(function () {
                window.location.reload();
            }, 500)
            }
            else if (result.retCode == 0) {
                Success("Error in sending package detail.");
                setTimeout(function () {
                    window.location.reload();
                }, 500)
            }
        },
        error: function () {
            Success('Error occured while processing your request! Please try again.');
            setTimeout(function () {
                window.location.reload();
            }, 500)
        }
    });

}