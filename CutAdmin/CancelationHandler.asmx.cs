﻿using CutAdmin.BL;
using CutAdmin.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using CutAdmin.EntityModal;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for CancelationHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class CancelationHandler : System.Web.Services.WebService
    {
        CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];

        [WebMethod(EnableSession = true)]
        public string GetCancelationPolicies()
        {
            using (var DB = new Click_Hotel())
            {
                string jsonString = "";
                JavaScriptSerializer objserialize = new JavaScriptSerializer();
                Int64 Uid = AccountManager.GetSuperAdminID();
                List<Dictionary<string, object>> objHotelList = new List<Dictionary<string, object>>();
                var CancelationList = (from obj in DB.tbl_CommonCancelationPolicy where obj.SupplierID == Uid select obj).ToList();

                if (CancelationList.Count() > 0)
                {
                    return objserialize.Serialize(new { Session = 1, retCode = 1, CancelationList = CancelationList });
                }
                else
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                return jsonString;
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetCancellationDays()
        {
            string json = "";
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            try
            {
                using (var DB = new Click_Hotel())
                {
                    Int64 Uid = AccountManager.GetSupplierByUser();
                    var List = (from obj in DB.Comm_CancellationDay where obj.UserID == Uid select obj).ToList();
                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, CancellationDays = List });
                }
            }
            catch (Exception)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string SaveCancellationDays(string CancellationDays)
        {
            string jsonString = "";
            using (var DB = new Click_Hotel())
            {
                try
                {
                    Int64 Uid = AccountManager.GetSupplierByUser();
                    var List = (from obj in DB.Comm_CancellationDay where obj.UserID == Uid select obj).ToList();
                    if (List.Count == 0)
                    {
                        Comm_CancellationDay Add = new Comm_CancellationDay();
                        Add.DaysBefore = Convert.ToInt64(CancellationDays);
                        Add.UserID = Uid;
                        DB.Comm_CancellationDay.Add(Add);
                        DB.SaveChanges();
                    }
                    else
                    {
                        Comm_CancellationDay Update = DB.Comm_CancellationDay.Single(x => x.UserID == Uid);
                        Update.DaysBefore = Convert.ToInt64(CancellationDays);
                        DB.SaveChanges();
                    }

                    jsonString = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
                catch {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                return jsonString;
            }
        }

        [WebMethod(EnableSession = true)]
        public string AddCancelPolicy(string PolicyName, string PolicyNote, string PriorDays, string Date, string Amount, string Percent, string Nights, string RateType, string DaysORDate, string RefundNoShow)
        {
            JavaScriptSerializer objserialize = new JavaScriptSerializer();
            using (var DB = new Click_Hotel())
            {
                string jsonString = "";
                //Click_Hotel db  = new Click_Hotel();
                Int64 Uid = AccountManager.GetSuperAdminID();
                var CancelationList = (from obj in DB.tbl_CommonCancelationPolicy where obj.CancelationPolicy == PolicyName && obj.SupplierID == Uid select obj).ToList();
                try
                {
                    if (CancelationList.Count() == 0)
                    {
                        tbl_CommonCancelationPolicy CancelPoclicy = new tbl_CommonCancelationPolicy();
                        CancelPoclicy.CancelationPolicy = PolicyName;
                        CancelPoclicy.CancelationNote = PolicyNote;
                        CancelPoclicy.RefundType = RefundNoShow;

                        CancelPoclicy.PolicyType = DaysORDate;
                        if (PriorDays != "")
                        {
                            CancelPoclicy.DaysPrior = Convert.ToInt32(PriorDays);
                            CancelPoclicy.IsDaysPrior = "true";
                        }
                        else { CancelPoclicy.IsDaysPrior = "false"; }
                        if (Date != "")
                        {
                            CancelPoclicy.Date = Date;
                        }
                        CancelPoclicy.ChargesType = RateType;
                        if (RateType == "Amount")
                        {
                            CancelPoclicy.AmountToCharge = Amount;
                        }
                        if (RateType == "Percentile")
                        {
                            CancelPoclicy.PercentageToCharge = Percent;
                        }
                        if (RateType == "Nights")
                        {
                            CancelPoclicy.NightsToCharge = Nights;
                        }
                        CancelPoclicy.SupplierID = Uid;

                        DB.tbl_CommonCancelationPolicy.Add(CancelPoclicy);
                        DB.SaveChanges();

                        return objserialize.Serialize(new { Session = 1, retCode = 1 });
                    }
                    else
                    {
                        jsonString = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }
                }
                catch
                {
                    jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }

                return jsonString;
            }
        }
    }
}
