﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="sightseeing.aspx.cs" Inherits="CutAdmin.sightseeing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" href="css/styles/form.css?v=3">
    <link rel="stylesheet" href="css/styles/switches.css?v=1">
    <link rel="stylesheet" href="css/styles/table.css?v=1">
    <!-- jQuery Form Validation -->
    <link rel="stylesheet" href="js/libs/formValidator/developr.validationEngine.css?v=1">
    <!-- DatePicker-->
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.css">

    <script src="js/developr.auto-resizing.js"></script>
    <script>
        $(function () {
            $("#kUI_timepicker").kendoTimePicker(); /*Date Time Picker*/
            //$("#kUI_calendar").kendoCalendar();    /*kendoCalendar*/
            $("#dtp_from").kendoDatePicker({   /*kendoCalendar*/
                format: "d-MM-yyyy"
            });
        })
    </script>

    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyDLvhAriXSRStAxraxjCp1GtClM4slLh-k"></script>
    <script src="Scripts/GoogleMap.js"></script>
    <link rel="stylesheet" href="js/libs/glDatePicker/developr.css">
    <script src="Scripts/moments.js"></script>
    <script src="Scripts/addsightseeing.js?v=1.3"></script>
    <script src="js/UploadDoc.js?v=1.0"></script>
    <%-- <script src="Scripts/image.js"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="button" style="float: right; display: none" class="button glossy" id="Button1" value="Save file" />
    <section role="main" id="main">
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
        <hgroup id="main-title" class="thin">
        </hgroup>
        <div class="with-padding" id="div_Main">
            <form method="post" action="#" class="block margin-bottom wizard same-height" id="">
                <fieldset class="wizard-fieldset fields-list" id="wiz_basic">
                    <legend class="legend">Sightseeing</legend>
                    <div class="field-block button-height">
                        <small class="input-info">Where is this activity / sightseeing / tour operating</small>
                        <label for="supplier" class="label"><b>Location</b></label>
                        <div class="columns">

                            <input type="hidden" id="hdnlatitude" />
                            <input type="hidden" id="hdnlongitude" />
                            <input type="hidden" id="hdnPlaceid" />
                            <input type="hidden" id="hdnName" />
                            <input type="hidden" id="hdnCitylati" />
                            <input type="hidden" id="hdnCitylongi" />
                            <input type="hidden" id="City_Name" />
                            <input type="hidden" id="CityPlaceID" />
                            <input type="hidden" id="lat_City" />
                            <input type="hidden" id="log_City" />

                            <div class="four-columns twelve-columns-mobile six-columns-mobile-landscape">
                                <input class="input validate[required]" id="txt_location" type="text" placeholder="Location" autocomplete="off">
                            </div>
                            <!-- replace with city once location is selected-->
                            <div class="three-columns twelve-columns-mobile six-columns-mobile-landscape">
                                <label class="small-margin-right strong green" id="lbl_city"></label>
                                <input type="hidden" id="hdCityCode" />
                            </div>
                            <!-- replace with city once location is selected-->
                            <div class="four-columns twelve-columns-mobile six-columns-mobile-landscape">
                                <label class="small-margin-right strong green" id="lbl_country"></label>
                                <input type="hidden" id="hdCountryCode" />
                            </div>
                        </div>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">What is the name of this activity / sightseeing / tour</small>
                        <label for="supplier" class="label"><b>Name</b></label>
                        <input class="input validate[required]" type="text" id="txt_name" />
                    </div>
                    <div class="field-block button-height" id="div_multiselect">
                        <small class="input-info">Tag the activity / sightseeing / tour with tour type for whom is this suitable for</small>
                        <label for="supplier" class="label"><b>Tour Type</b></label>
                        <select id="sel_tourtype" class="validate[required]" data-prompt-position="topLeft" onchange="SetValidation()" name="tourtype" multiple="multiple" data-placeholder="attraction...">
                            <%--<option>Family</option>
                    <option>Honeymoon</option>
                    <option>Adventure</option>
                    <option>Youth</option>       --%>
                        </select>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">Give details about this activity / sightseeing / tour</small>
                        <label for="supplier" class="label"><b>Description</b></label>
                        <textarea class="input full-width autoexpanding validate[required]" data-prompt-position="topLeft" type="text" id="txt_description"></textarea>
                    </div>
                </fieldset>

                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Policy</legend>
                    <div class="field-block button-height">
                        <small class="input-info">There min age restrictions to participate in this tour</small>
                        <label for="childpolicy" class="label"><b>Child Policy</b></label>
                        <p>
                            <label>Is there any age restriction for children on this tour?</label>
                            <input type="checkbox" class="switch medium" data-checkable-options='{"textOn":"YES","textOff":"NO"}' id="btn_childpolicy" onchange="SetChildPolicy(this)">
                        </p>
                    </div>
                    <div class="field-drop black-inputs button-height" id="div_childpolicy" style="display: none">
                        <label for="childagefrom" class="label"><b>Age Restrictions:</b></label>
                        <p>
                            <label>Kids below</label>
                            <span class="number input small-margin-right">
                                <button type="button" class="button number-down">-</button>
                                <input id="childagefrom" onchange="ChangeAge()" type="text" value="12" size="3" class="input-unstyled" data-number-options='{"min":2,"max":17,"increment":1}'>
                                <button type="button" class="button number-up">+</button>
                            </span>
                            <label>years old and above </label>
                            <span class="number input small-margin-right" id="div_childageabove"></span>
                            <label for="childageto">year old, are considered as <b>big child</b></label>
                        </p>
                        <p class="no-margin-bottom">
                            <label>Kids below</label>
                            <span class="number input small-margin-right">
                                <button type="button" class="button number-down">-</button>
                                <input id="child2agefrom" disabled type="text" value="5" size="3" class="input-unstyled" data-number-options='{"min":2,"max":17,"increment":1.0}'>
                                <button type="button" class="button number-up">+</button>
                            </span>
                            <label>years old and above </label>
                            <span class="number input small-margin-right" id="div_smallchildageabove"></span>
                            <label for="childageto">years old, are considered as <b>small child</b></label>
                        </p>
                        <small class="input-info white">You can change this policy as per rates at the time of rates feeding</small>
                    </div>

                    <br />
                    <div id="div_childdetails" style="display: none">
                        <div class="field-block button-height">
                            <small class="input-info">Many tour / attractions has max height restrictions</small>
                            <label for="height polich" class="label"><b>Height Policy</b></label>
                            <p>
                                <label>Is there any height restriction for children on this tour?</label>
                                <input type="checkbox" class="switch medium" data-checkable-options='{"textOn":"YES","textOff":"NO"}' id="btn_heightpolicy" onchange="SetheightPolicy(this)">
                            </p>
                        </div>
                        <div class="field-drop black-inputs button-height" id="div_heightpolicy" style="display: none">
                            <label for="childagefrom" class="label"><b>Height Restrictions:</b></label>
                            <p class="no-margin-bottom">
                                <label>Kids above</label>
                                <span class="number input small-margin-right">
                                    <button type="button" class="button number-down">-</button>
                                    <input id="childfeet" type="text" value="1" size="3" class="input-unstyled" data-number-options='{"min":0,"max":7,"increment":1}'>
                                    <button type="button" class="button number-up">+</button>
                                </span>
                                <label><b>feet</b> and </label>
                                <span class="number input small-margin-right">
                                    <button type="button" class="button number-down">-</button>
                                    <input id="childinch" type="text" value="0" size="3" class="input-unstyled" data-number-options='{"min":0,"max":11,"increment":1}'>
                                    <button type="button" class="button number-up">+</button>
                                </span>
                                <label><b>inch</b> are cosidered as adult</label>
                            </p>
                            <small class="input-info white">You can change this policy as per rates at the time of rates feeding</small>
                        </div>
                        <div class="field-block button-height">
                            <small class="input-info">Some tour / attractions has Infant restrictions</small>
                            <label for="height polich" class="label"><b>Infant Policy</b></label>
                            <p>
                                <label>Is <b>Infant </b>allowed on this tour?</label>
                                <input type="checkbox" id="btn_infantpolicy" class="switch medium" data-checkable-options='{"textOn":"YES","textOff":"NO"}'>
                            </p>
                        </div>
                    </div>


                    <div class="field-block button-height">
                        <small class="input-info">Important information which you want your customers to know</small>
                        <label for="height polich" class="label"><b>Important Note</b></label>
                        <textarea class="input full-width autoexpanding validate[required]" data-prompt-position="topLeft" type="text" id="txt_impnote"></textarea>
                    </div>

                </fieldset>

                <fieldset class="wizard-fieldset fields-list" id="wiz_operation">
                    <legend class="legend">Operations</legend>
                    <div class="field-block button-height">
                        <small class="input-info">Sightseeing / Activity / Tour may be operating entire year or for specific period, kindly define the same</small>
                        <label for="childpolicy" class="label"><b>Duration</b></label>
                        <p>
                            <label>Is this sightseeing / Tour / Activity is operational full year?</label>
                            <input type="checkbox" class="switch medium" checked data-checkable-options='{"textOn":"YES","textOff":"NO"}' id="btn_duration" onchange="SetDuration(this)">
                        </p>
                    </div>
                    <%-- if no --%>
                    <div class="field-drop black-inputs button-height" id="div_duration" style="display: none">
                        <small class="input-info">This Tour / Sightseeing / Activity is only operational between below dates</small>
                        <label for="childagefrom" class="label"><b>Operation Dates</b></label>
                        <div id="div_operationdates">
                        </div>
                        <br />
                        <p class="small-margin-bottom">
                            <input type="checkbox" class="chk_OperationDays" value="Daily" id="Chk_Daily" onchange="DaysAllChecked()">
                            <label for="Daily">Daily</label>
                            <input type="checkbox" class="chk_OperationDays" value="Mon" id="Chk_mon" onchange="DaysChecked()">
                            <label for="Mon">Mon</label>
                            <input type="checkbox" class="chk_OperationDays" value="Tue" id="Chk_tue" onchange="DaysChecked()">
                            <label for="tue">Tue</label>
                            <input type="checkbox" class="chk_OperationDays" value="Wed" id="Chk_wed" onchange="DaysChecked()">
                            <label for="wed">Wed</label>
                            <input type="checkbox" class="chk_OperationDays" value="Thu" id="Chk_thu" onchange="DaysChecked()">
                            <label for="thu">Thu</label>
                            <input type="checkbox" class="chk_OperationDays" value="Fri" id="Chk_fri" onchange="DaysChecked()">
                            <label for="fri">Fri</label>
                            <input type="checkbox" class="chk_OperationDays" value="Sat" id="Chk_sat" onchange="DaysChecked()">
                            <label for="sat">Sat</label>
                            <input type="checkbox" class="chk_OperationDays" value="Sun" id="Chk_sun" onchange="DaysChecked()">
                            <label for="sun">Sun</label>
                        </p>
                    </div>
                    <div class="field-block button-height">
                        <small class="input-info">Mention if there is any minimum or maximum capacity of this tour</small>
                        <label for="supplier" class="label"><b>Capacity</b></label>
                        <p>
                            <label>Is there any min or max condition for this sightseeing / Tour / Activity?</label>
                            <input type="checkbox" class="switch medium" data-checkable-options='{"textOn":"YES","textOff":"NO"}' id="btn_capacity" onchange="SetCapacity(this)">
                        </p>
                    </div>
                    <div class="field-drop black-inputs button-height" id="div_capacity" style="display: none">
                        <small class="input-info">How many person are allowed per booking?</small>
                        <%--<label for="supplier" class="label"><b>Allowed</b></label>--%>
                        <p>
                            <label for="minpax"><b>Minimum</b></label>
                            <span class="number input small-margin-right">
                                <button type="button" class="button number-down">-</button>
                                <input id="minpax" type="text" value="2" size="3" class="input-unstyled" data-number-options='{"min":2,"max":17,"increment":1}'>
                                <button type="button" class="button number-up">+</button>
                            </span>
                            <label for="maxpax"><b>and Maximum</b></label>
                            <span class="number input small-margin-right">
                                <button type="button" class="button number-down">-</button>
                                <input id="maxpax" type="text" value="17" size="3" class="input-unstyled" data-number-options='{"min":2,"max":17,"increment":1.0}'>
                                <button type="button" class="button number-up">+</button>
                            </span>
                            <label for="maxpax"><b>are allowed per booking</b></label>
                        </p>
                    </div>
                </fieldset>

                <fieldset class="wizard-fieldset no-padding">
                    <legend class="legend">Images</legend>
                    <div class="content-panel mobile-panels margin-bottom">
                        <div class="panel-content linen">
                            <div class="panel-control align-right">
                                <span class="progress thin" style="width: 75px">
                                    <span class="progress-bar green-gradient" style="width: 70%"></span>
                                </span>
                                You may add <span class="ImgCount">10</span> more images
                                <input type="file" id="images" accept="image/*" class="hidden" onchange="preview_images(this,'result');" multiple />
                        <a class="button icon-cloud-upload margin-left file withClearFunctions" onclick="($('#images').click())" id="file-input" multiple>Add file...</a>
                            </div>
                            <div class="panel-load-target scrollable with-padding" style="height: auto">

                                <p class="message icon-info-round white-gradient">
                                    Add can add upto 10 images / photos related to this activity
                                    <input type="file" multiple id="Imges" onchange="preview_images(this,'result')" class="btn-search5 hidden" />
                                </p>
                                <ul class="blocks-list fixed-size-200" id="result">
                                </ul>
                                
                            </div>
                        </div>
                    </div>
                </fieldset>


                <fieldset class="wizard-fieldset fields-list">
                    <legend class="legend">Review</legend>
                    <div class="field-block button-height">
                        <label class="label"><b>Main Details</b></label>
                        <div class="columns">
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Name</label>
                                <p class="black selected-inclusion lbl_Name"></p>
                            </div>
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Location</label>
                                <p class="black selected-inclusion lbl_Location"></p>
                            </div>
                            <div class="four-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Country & City</label>
                                <p class="black selected-inclusion lbl_CountryCity"></p>
                            </div>

                        </div>
                    </div>
                    <div class="field-block button-height">
                        <label class="label"><b>Operation Details</b></label>
                        <div class="columns">
                            <div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Validity</label>
                                <div id="div_Validity">
                                </div>
                            </div>
                            <div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Opening/Closing Time</label>
                                <div id="div_Opening_Closing_Time">
                                </div>
                            </div>
                            <div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Operation Days</label>
                                <p class="black selected-inclusion lbl_OperationDays"></p>
                            </div>
                            <div class="three-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Min/Max Person</label>
                                <div id="div_Paxcapacity">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field-block button-height" id="div_reviewchildpolicy" style="display: none">
                        <label class="label"><b>Child Policy</b></label>
                        <div class="columns">
                            <div class="five-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Child Age</label>
                                <div id="div_ChildAges">
                                </div>
                            </div>
                            <div class="five-columns twelve-columns-mobile mid-margin-bottom align-center" id="div_ChildHeight" style="display: none">
                                <label class="green strong">Child Height</label>
                                <div id="div_ChildHeightdetail">
                                </div>
                            </div>
                            <div class="two-columns twelve-columns-mobile mid-margin-bottom align-center">
                                <label class="green strong">Infant</label>
                                <p class="black selected-inclusion lbl_Infant"></p>
                            </div>

                        </div>
                    </div>
                    <br />
                    <hr />
                    <button type="button" id="btn_Save" class="button glossy anthracite-gradient float-right" onclick="SaveSightseeing()">Save</button>
                </fieldset>

            </form>
        </div>
    </section>
    <!-- waitForImages  -->
    <script src="newadmintheme/assets/js/custom/jquery.waitforimages.min.js"></script>
    <!--  gallery functions -->
    <script src="newadmintheme/assets/js/pages/page_gallery.min.js"></script>
</asp:Content>

