﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="FlightReport.aspx.cs" Inherits="CutAdmin.FlightReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/FlightBookings.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment-with-locales.min.js"></script>
    <script src="js/moment.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section role="main" id="main">
        <div class="with-padding">
            <hgroup id="main-title" class="thin">
                <h1>Flight Booking Report</h1>
                <hr />
                <h2><a href="#" class="addnew"><i class="fa fa-filter"></i></a></h2>
                <div id="filter" class="with-padding anthracite-gradient" style="display: none;">
                    <form class="form-horizontal">
                        <div class="columns">
                            <div class="three-columns twelve-columns-mobile">
                                <label>Booking Date</label>
                                <span class="input full-width">
                                    <span class="icon-calendar"></span>
                                    <input type="text" name="datepicker" id="Bookingdate" class="input-unstyled" value="">
                                </span>
                            </div>
                            <div class="three-columns twelve-columns-mobile">
                                <label>Departure</label>
                                <span class="input full-width">
                                    <span class="icon-calendar"></span>
                                    <input type="text" name="datepicker" id="Departure" class="input-unstyled" value="">
                                </span>
                            </div>
                            <div class="three-columns twelve-columns-mobile">
                                <label>Arrival</label>
                                <span class="input full-width">
                                    <span class="icon-calendar"></span>
                                    <input type="text" name="datepicker" id="Arrival" class="input-unstyled" value="">
                                </span>
                            </div>
                            <div class="three-columns twelve-columns-mobile bold">
                                <label>Passenger Name </label>
                                <div class="input full-width">
                                    <input type="text" id="txt_Passenger" class="input-unstyled full-width">
                                </div>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="three-columns twelve-columns-mobile bold">
                                <label>Reference No.</label>
                                <div class="input full-width">
                                    <input type="text" id="txt_Reference" class="input-unstyled full-width">
                                </div>
                            </div>
                            <div class="three-columns twelve-columns-mobile bold" style="display: none">
                                <label>Airline</label>
                                <div class="input full-width">
                                    <input type="text" id="txt_Hotel" class="input-unstyled full-width">
                                </div>
                            </div>
                            <div class="three-columns twelve-columns-mobile bold" style="display: none">
                                <label>Destination</label>
                                <div class="input full-width">
                                    <input type="text" id="txt_Destination" class="input-unstyled full-width">
                                </div>
                            </div>
                            <div class="three-columns twelve-columns-mobile bold">
                                <label>Reservation Status</label>
                                <div class="full-width button-height typeboth">
                                    <select id="selReservation" class="select">
                                        <option selected="selected">All</option>
                                        <option>Tiketed</option>
                                        <option>Cancelled</option>
                                        <option>Hold</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="eight-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                            </div>
                            <div class="two-columns twelve-columns-mobile formBTn searhbtn text-alignright">
                                <button type="button" class="button anthracite-gradient" onclick="SearchFlights()">Search</button>
                                <button type="button" class="button anthracite-gradient" onclick="Reset()">Reset</button>
                            </div>
                            <%--<div class="two-columns twelve-columns-mobile bold text-alignright">
                                <span class="icon-pdf right" onclick="ExportBookingDetailsToExcel('PDF')">
                                    <img src="../img/PDF-Viewer-icon.png" style="cursor: pointer" title="Export To Pdf" height="35" width="35">
                                </span>
                                <span class="icon-excel right" onclick="ExportBookingDetailsToExcel('excel')">
                                    <img src="../img/Excel-2-icon.png" style="cursor: pointer" title="Export To Excel" id="btnPDF" height="35" width="35"></span>
                            </div>--%>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                </div>
                <br />
            </hgroup>
            <div class="with-padding">
                <div class="respTable">
                    <table class="table responsive-table font11" id="tbl_AirTicket">
                        <thead>
                            <tr>
                                <th scope="col" style="max-width: 20px; vertical-align: middle;" class="align-center">S.N</th>
                                <th scope="col" style="max-width: 60px; vertical-align: middle;" class="align-center">Date</th>
                                <th scope="col" style="max-width: 115px; vertical-align: middle;" class="align-center">Transaction No.</th>
                                <th scope="col" style="max-width: 115px; vertical-align: middle;" class="align-center">Agency / Customer</th>
                                <th scope="col" style="max-width: 115px; vertical-align: middle;" class="align-center">Ticket # / PNR </th>
                                <th scope="col" style="max-width: 115px; vertical-align: middle;" class="align-center">Travllers Name</th>
                                <th scope="col" style="max-width: 90px; vertical-align: middle;" class="align-center">Travelling Details</th>
                                <th scope="col" style="max-width: 75px; vertical-align: middle;" class="align-center">Status</th>
                                <th scope="col" style="min-width: 100px; vertical-align: middle;" class="align-center">Total Fare</th>
                                <!--<th scope="col" style="max-width: 115px; vertical-align: middle;" class="align-center">Sell</th>-->
                                <%--<th scope="col" class="align-center">Payment Mode</th>--%>
                                <th scope="col" style="max-width: 115px; vertical-align: middle;" class="align-center">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <script>
        jQuery(document).ready(function () {
            jQuery('.fa-filter').click(function () {
                jQuery(' #filter').slideToggle();
                jQuery('.searchBox li a ').on('click', function () {
                    jQuery('.filterBox #filter').slideUp();
                });
            });
        });
    </script>
</asp:Content>
