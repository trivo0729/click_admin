﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using CommonLib.Response;
using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class PackagesManager
    {
        #region Old
        #region core ***********************************************
        public enum DBReturnCode
        {
            SUCCESS = 0,
            CONNECTIONFAILURE = -1,
            SUCCESSNORESULT = -2,
            SUCCESSNOAFFECT = -3,
            DUPLICATEENTRY = -4,
            EXCEPTION = -5,
            INVALIDXML = -11,
            INPUTPARAMETEREMPTY = -6,
            UNIQUEKEYVIOLATION = -7,
            REFERENCECONFLICT = -8,
            NOQUESTIONS = -9,
            INTERMISSION = -10,
            TESTCOMPLETED = -12,
            TESTTIMEOUT = -13,
            QUESTIONALREADYANSWERED = -14,
            TESTNOTFOUND = -15
        }
        public enum CountryReturnCode
        {
            SUCCESS = 0,
            EXCEPTION = -1,
            NORECORDSFOUND = -2,
            COUNTRYNOTFOUND = -3,
            COUNTRYEXISTS = -4,
        }
        private static SqlConnection OpenConnection()
        {
            SqlConnection conn = null;
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ClickurTripConnectionString"].ToString());
                //conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PortalIntegrationConnectionString"].ToString());
                conn.Open();
            }
            catch
            {
                conn = null;
            }
            return conn;
        }
        private static void CloseConnection(SqlConnection conn)
        {
            try
            {
                conn.Close();
                conn.Dispose();
            }
            catch { }
        }
        protected static DBReturnCode ExecuteQuery(string sQuery, out DataTable dtResult)
        {

            DBReturnCode retcode = DBReturnCode.SUCCESS;
            dtResult = null;
            SqlDataAdapter dataAdapter = null;
            SqlConnection conn = OpenConnection();
            if (conn == null)
            {
                retcode = DBReturnCode.CONNECTIONFAILURE;
            }
            else
            {
                try
                {
                    dataAdapter = new SqlDataAdapter(sQuery, conn);
                    DataSet dsTmp = new DataSet();
                    dataAdapter.Fill(dsTmp);
                    dtResult = dsTmp.Tables[0];
                    if (dtResult.Rows.Count == 0)
                        return DBReturnCode.SUCCESSNORESULT;
                    else
                        return DBReturnCode.SUCCESS;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    retcode = DBReturnCode.EXCEPTION;
                }
                finally
                {
                    dataAdapter.Dispose();
                    CloseConnection(conn);
                }
            }
            return retcode;
        }
        public static DBReturnCode ExecuteNonQuery(string sQuery)
        {
            DBReturnCode retcode = DBReturnCode.SUCCESS;
            SqlCommand cmd = null;
            SqlConnection conn = OpenConnection();
            if (conn == null)
            {
                retcode = DBReturnCode.CONNECTIONFAILURE;
            }
            else
            {
                try
                {
                    cmd = new SqlCommand(sQuery, conn);
                    if (cmd.ExecuteNonQuery() == 0)
                        return DBReturnCode.SUCCESSNOAFFECT;
                    else
                        return DBReturnCode.SUCCESS;
                }
                catch (SqlException ex)
                {
                    if (ex.Number == 2627 || ex.Number == 2601) // PRIMARY or UNIQUE KEY KEY VIOLATION
                    {
                        retcode = DBReturnCode.DUPLICATEENTRY;
                    }
                    else
                        retcode = DBReturnCode.EXCEPTION;
                }
                finally
                {
                    cmd.Dispose();
                    CloseConnection(conn);
                }
            }
            return retcode;
        }
        private static DBReturnCode ExecuteScaler(string sQuery, out Int64 nRecordID)
        {
            nRecordID = 0;
            /*HttpContext.Current.Response.Write(sQuery);
            HttpContext.Current.Response.Write("<br/>");*/
            DBReturnCode retcode = DBReturnCode.SUCCESS;
            SqlCommand cmd = null;
            SqlConnection conn = OpenConnection();
            if (conn == null)
            {
                retcode = DBReturnCode.CONNECTIONFAILURE;
            }
            else
            {
                try
                {
                    sQuery += "; SELECT SCOPE_IDENTITY();";
                    cmd = new SqlCommand(sQuery, conn);
                    nRecordID = Convert.ToInt64(cmd.ExecuteScalar());
                    if (nRecordID <= 0)
                        return DBReturnCode.SUCCESSNOAFFECT;
                    else
                        return DBReturnCode.SUCCESS;
                }
                catch //(Exception e)
                {
                    retcode = DBReturnCode.EXCEPTION;
                    //HttpContext.Current.Response.Write(e.Message); 
                }
                finally
                {
                    cmd.Dispose();
                    CloseConnection(conn);
                }
            }
            return retcode;
        }

        #region " DB Opration With Transection "

        public static DBReturnCode ExecuteQueryT(SqlConnection Conn, String sSQL, out DataTable dtResult)
        {
            DBReturnCode retCode = DBReturnCode.EXCEPTION;
            dtResult = null;

            try
            {
                using (SqlCommand Cmd = new SqlCommand(sSQL, Conn))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(sSQL, Conn))
                    {
                        da.Fill(dtResult);

                        if (dtResult.Rows.Count > 0)
                            retCode = DBReturnCode.SUCCESS;
                        else
                            retCode = DBReturnCode.SUCCESSNORESULT;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {

            }

            return retCode;
        }

        public static DBReturnCode ExecuteNonQueryT(SqlConnection Conn, SqlTransaction Tran, String SQL)
        {
            DBReturnCode retCode = DBReturnCode.SUCCESS;
            SqlCommand Cmd = null;
            try
            {
                Cmd = new SqlCommand();
                Cmd.Connection = Conn;
                Cmd.Transaction = Tran;
                Cmd.CommandText = SQL;

                if (Cmd.ExecuteNonQuery() == 0)
                    retCode = DBReturnCode.SUCCESSNOAFFECT;
                else
                    retCode = DBReturnCode.SUCCESS;

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);

                if (ex.Number == 2627) // PRIMARY KEY KEY VIOLATION
                {
                    retCode = DBReturnCode.DUPLICATEENTRY;
                }
                else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
                {
                    retCode = DBReturnCode.UNIQUEKEYVIOLATION;
                }
                else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
                {
                    retCode = DBReturnCode.REFERENCECONFLICT;
                }
                else
                {
                    retCode = DBReturnCode.EXCEPTION;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Cmd != null)
                    Cmd.Dispose();
            }

            return retCode;
        }

        public static DBReturnCode ExecuteNonQueryT(SqlConnection Conn, SqlTransaction Tran, String SQL, SqlParameter[] ParameterList)
        {
            DBReturnCode retCode = DBReturnCode.SUCCESS;
            SqlCommand Cmd = null;
            try
            {
                Cmd = new SqlCommand();
                Cmd.Connection = Conn;
                Cmd.Transaction = Tran;
                Cmd.CommandText = SQL;

                foreach (SqlParameter par in ParameterList)
                {
                    if (par != null)
                        Cmd.Parameters.Add(par);
                }

                if (Cmd.ExecuteNonQuery() == 0)
                    retCode = DBReturnCode.SUCCESSNOAFFECT;
                else
                    retCode = DBReturnCode.SUCCESS;

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);

                if (ex.Number == 2627) // PRIMARY KEY VIOLATION
                {
                    retCode = DBReturnCode.DUPLICATEENTRY;
                }
                else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
                {
                    retCode = DBReturnCode.UNIQUEKEYVIOLATION;
                }
                else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
                {
                    retCode = DBReturnCode.REFERENCECONFLICT;
                }
                else
                {
                    retCode = DBReturnCode.EXCEPTION;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Cmd != null)
                    Cmd.Dispose();
            }

            return retCode;
        }

        public static DBReturnCode ExecuteScalerT(SqlConnection Conn, SqlTransaction Tran, String SQL, out Int64 RecordID, out String ErrorMessage)
        {
            DBReturnCode retCode = DBReturnCode.SUCCESS;
            SqlCommand Cmd = null;
            RecordID = 0;
            ErrorMessage = String.Empty;
            try
            {
                Cmd = new SqlCommand();
                Cmd.Connection = Conn;
                Cmd.Transaction = Tran;

                SQL += "; SELECT SCOPE_IDENTITY();";

                Cmd.CommandText = SQL;

                RecordID = Convert.ToInt64(Cmd.ExecuteScalar());

                if (RecordID == 0)
                    retCode = DBReturnCode.SUCCESSNOAFFECT;
                else
                    retCode = DBReturnCode.SUCCESS;

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                ErrorMessage = "FROM DataManger Class \n\r " + ex.Message;
                ErrorMessage += "\n\r " + SQL;

                if (ex.Number == 2627) // PRIMARY KEY VIOLATION
                {
                    retCode = DBReturnCode.DUPLICATEENTRY;
                }
                else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
                {
                    retCode = DBReturnCode.UNIQUEKEYVIOLATION;
                }
                else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
                {
                    retCode = DBReturnCode.REFERENCECONFLICT;
                }
                else
                {
                    retCode = DBReturnCode.EXCEPTION;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Cmd != null)
                    Cmd.Dispose();
            }

            return retCode;
        }

        public static DBReturnCode ExecuteScalerT(SqlConnection Conn, SqlTransaction Tran, String SQL, SqlParameter[] ParameterList, out Int64 RecordID, out String ErrorMessage)
        {
            DBReturnCode retCode = DBReturnCode.SUCCESS;
            SqlCommand Cmd = null;
            RecordID = 0;
            ErrorMessage = String.Empty;
            try
            {
                Cmd = new SqlCommand();
                Cmd.Connection = Conn;
                Cmd.Transaction = Tran;

                SQL += "; SELECT SCOPE_IDENTITY();";

                Cmd.CommandText = SQL;

                foreach (SqlParameter par in ParameterList)
                {
                    if (par != null)
                        Cmd.Parameters.Add(par);
                }

                RecordID = Convert.ToInt64(Cmd.ExecuteScalar());

                if (RecordID == 0)
                    retCode = DBReturnCode.SUCCESSNOAFFECT;
                else
                    retCode = DBReturnCode.SUCCESS;

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                ErrorMessage = "FROM DataManger Class \n\r " + ex.Message;
                ErrorMessage += "\n\r " + SQL;

                if (ex.Number == 2627) // PRIMARY KEY VIOLATION
                {
                    retCode = DBReturnCode.DUPLICATEENTRY;
                }
                else if (ex.Number == 2601) //UNIQUE KEY VIOLATION
                {
                    retCode = DBReturnCode.UNIQUEKEYVIOLATION;
                }
                else if (ex.Number == 547)   // FORENIGN KEY VIOLATION
                {
                    retCode = DBReturnCode.REFERENCECONFLICT;
                }
                else
                {
                    retCode = DBReturnCode.EXCEPTION;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (Cmd != null)
                    Cmd.Dispose();
            }

            return retCode;
        }

        #endregion " DB Opration With Transection "

        #endregion core

        public static DBReturnCode GetPackageDetails(out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
            sSQL.Append("SELECT tbl_Package.*, tbl_PackageImages.ImageArray,tbl_PackagePricing.dSingleAdult FROM tbl_Package INNER JOIN  tbl_PackageImages ON tbl_Package.nID = tbl_PackageImages.nPackageID INNER JOIN tbl_PackagePricing ON tbl_Package.nID = tbl_PackagePricing.nPackageID ORDER BY tbl_PackagePricing.dSingleAdult");
            return ExecuteQuery(sSQL.ToString(), out dtResult);
        }

        public static DBReturnCode SinglePackageDetails(Int64 nID, out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
            sSQL.Append("SELECT tbl_Package.*, tbl_Itinerary.*  FROM tbl_Package INNER JOIN tbl_Itinerary ON tbl_Package.nID = tbl_Itinerary.nPackageID  WHERE tbl_Package.nID= ").Append(nID);
            return ExecuteQuery(sSQL.ToString(), out dtResult);
        }

        public static DBReturnCode PackageDetails(Int64 nID, out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
            sSQL.Append("SELECT tbl_Package.*,tbl_PackageImages.ImageArray, tbl_Itinerary.nCategoryID, tbl_Itinerary.nCategoryName, tbl_Itinerary.sItinerary_1, tbl_Itinerary.sItinerary_2, tbl_Itinerary.sItinerary_3, tbl_Itinerary.sItinerary_4, tbl_Itinerary.sItinerary_5, tbl_Itinerary.sItinerary_6, tbl_Itinerary.sItinerary_7,")
            .Append(" tbl_Itinerary.sItinerary_8, tbl_Itinerary.sItinerary_9, tbl_Itinerary.sItinerary_10, tbl_Itinerary.sItinerary_11, tbl_Itinerary.sItinerary_12, tbl_Itinerary.sItinerary_13, tbl_Itinerary.sItinerary_14, tbl_Itinerary.sItinerary_15, tbl_HotelImages.sHotelImage1,tbl_HotelImages.sHotelImage2,tbl_HotelImages.sHotelImage3,")
            .Append(" tbl_HotelImages.sHotelImage4,tbl_HotelImages.sHotelImage5,tbl_HotelImages.sHotelImage6,tbl_HotelImages.sHotelImage7,tbl_HotelImages.sHotelImage8,tbl_HotelImages.sHotelImage9,tbl_HotelImages.sHotelImage10,tbl_HotelImages.sHotelImage11,tbl_HotelImages.sHotelImage12,tbl_HotelImages.sHotelImage13,tbl_HotelImages.sHotelImage14,")
            .Append(" tbl_PackagePricing.dSingleAdult,tbl_PackagePricing.dCouple,tbl_PackagePricing.dExtraAdult,tbl_PackagePricing.dInfantKid,tbl_PackagePricing.dKidWBed,tbl_PackagePricing.dKidWOBed,tbl_PackagePricing.sStaticInclusions,tbl_PackagePricing.sDynamicInclusion,tbl_PackagePricing.sStaticExclusion,tbl_PackagePricing.sDynamicExclusion,")
            .Append(" tbl_ItineraryHotel.sHotelName_1,tbl_ItineraryHotel.sHotelName_2,tbl_ItineraryHotel.sHotelName_3,tbl_ItineraryHotel.sHotelName_4,tbl_ItineraryHotel.sHotelName_5,tbl_ItineraryHotel.sHotelName_6,tbl_ItineraryHotel.sHotelName_7,tbl_ItineraryHotel.sHotelName_8,tbl_ItineraryHotel.sHotelName_9,tbl_ItineraryHotel.sHotelName_10,")
            .Append(" tbl_ItineraryHotel.sHotelName_11,tbl_ItineraryHotel.sHotelName_12,tbl_ItineraryHotel.sHotelName_13,tbl_ItineraryHotel.sHotelName_14,tbl_ItineraryHotel.sHotelDescrption_1,tbl_ItineraryHotel.sHotelDescrption_2,tbl_ItineraryHotel.sHotelDescrption_3,tbl_ItineraryHotel.sHotelDescrption_4,tbl_ItineraryHotel.sHotelDescrption_5,")
            .Append(" tbl_ItineraryHotel.sHotelDescrption_6,tbl_ItineraryHotel.sHotelDescrption_7,tbl_ItineraryHotel.sHotelDescrption_8,tbl_ItineraryHotel.sHotelDescrption_9,tbl_ItineraryHotel.sHotelDescrption_10,tbl_ItineraryHotel.sHotelDescrption_11,tbl_ItineraryHotel.sHotelDescrption_12,tbl_ItineraryHotel.sHotelDescrption_13,")
            .Append(" tbl_ItineraryHotel.sHotelDescrption_14 FROM tbl_Package INNER JOIN tbl_PackageImages ON tbl_Package.nID = tbl_PackageImages.nPackageID INNER JOIN tbl_Itinerary ON tbl_Itinerary.nPackageID = tbl_Package.nID INNER JOIN tbl_ItineraryHotel ON (tbl_Itinerary.nPackageID = tbl_ItineraryHotel.nPackageID AND tbl_Itinerary.nCategoryID = tbl_ItineraryHotel.nCategoryID)")
            .Append(" INNER JOIN tbl_PackagePricing ON (tbl_ItineraryHotel.nPackageID = tbl_PackagePricing.nPackageID AND tbl_PackagePricing.nCategoryID = tbl_ItineraryHotel.nCategoryID) INNER JOIN tbl_HotelImages ON (tbl_PackagePricing.nPackageID = tbl_HotelImages.nPackageID  AND tbl_PackagePricing.nCategoryID = tbl_HotelImages.nCategoryID) WHERE tbl_Package.nID=").Append(nID);
            return ExecuteQuery(sSQL.ToString(), out dtResult);
        }

        public static DBReturnCode GetProductImages(Int64 nPackageID, out DataTable dtResult)
        {
            StringBuilder sSQL = new StringBuilder();
            sSQL.Append("SELECT * FROM tbl_PackageImages WHERE nPackageID=").Append(nPackageID);
            return ExecuteQuery(sSQL.ToString(), out dtResult);
        }
        #endregion

        static helperDataContext db = new helperDataContext();

        static DateTime dtJourney;
        static List<string> arrThemes = new List<string> { "Holidays", "Umrah", "Hajj", "Honeymoon", "Summer", "Adventure", "Deluxe", "Business", "Premium", "Wildlife", "Weekend", "New Year" };
        static List<string> arrCategory = new List<string> { "Standard", "Deluxe", "Premium", "Luxury" };
        static MarkupsAndTaxes objMarkupsAndTaxes;

        //static List<string> Facilities = new List<string> { "Airfare with Airport transfer", "Sight Seeing", "Breakfast", "Tour Guide", "Lunch", "Dinner" };
        //public static List<PackageLib.Response.PackageDetails> GetPackges(string Session)
        //{
        //    List<PackageLib.Response.PackageDetails> ListPackeges = new List<PackageLib.Response.PackageDetails>();
        //    HttpContext.Current.Session["searchPackge"] = Session;
        //    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //    try
        //    {

        //        string[] arrSearch = Session.Split('_');
        //        dtJourney = DateTime.Now;
        //        var sData = (from obj in db.tbl_Packages where obj.sPackageDestination.Contains(arrSearch[0]) && obj.ParentID == objGlobalDefault.ParentId select obj).ToList();
        //        if (sData == null)
        //            throw new Exception("No Package found for this Destinations");
        //        List<tbl_Package> ListPackges = new List<tbl_Package>();
        //        objMarkupsAndTaxes = MarkupTaxManager.GetMarkupTax(objGlobalDefault.ParentId, objGlobalDefault.sid, "Packages");
        //        MarkupTaxManager.objMarkupsAndTaxes = objMarkupsAndTaxes;
        //        UserCurrency = objGlobalDefault.Currency;
        //        foreach (var Package in sData)
        //        {

        //            if (DateTime.ParseExact(Package.dValidityFrom, "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture) <= DateTime.Now || DateTime.ParseExact(Package.dValidityTo, "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture) >= DateTime.Now)
        //            {
        //                ListPackeges.Add(new PackageLib.Response.PackageDetails
        //                {
        //                    ValidTo = DateTime.ParseExact(Package.dValidityTo, "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture),
        //                    Category = GetCategory(Package.sPackageCategory, Package.nID),
        //                    City = Package.sPackageDestination,
        //                    Description = Package.sPackageDescription,
        //                    Images = ListImages(Package.nID, Package.sPackageName),
        //                    noDays = Convert.ToInt64(Package.nDuration),
        //                    Packageid = Package.nID,
        //                    PackageName = Package.sPackageName,
        //                    TermsCondition = Package.sTermsCondition,
        //                    Themes = GetThemes(Package.sPackageThemes),
        //                    ValidFrom = DateTime.ParseExact(Package.dValidityFrom, "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture),


        //                });
        //            }
        //            //ListPackges.Add(Package);
        //        }


        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    HttpContext.Current.Session["PackageDetails"] = ListPackeges;
        //    return ListPackeges;
        //}

        ///* Dasign Packges Theme */
        //public static List<string> GetThemes(string Themes)
        //{
        //    List<string> sThemes = new List<string>();
        //    for (int i = 0; i < Themes.Split(',').ToList().Count; i++)
        //    {
        //        string sThemeID = Themes.Split(',')[i];
        //        if (sThemeID != "")
        //            sThemes.Add(arrThemes[Convert.ToInt16(sThemeID)]);
        //    }
        //    return sThemes;
        //}


        /*Packges Details*/
        //public static List<PackageLib.Response.Category> GetCategory(string CategoryId, Int64 PackageID)
        //{
        //    List<PackageLib.Response.Category> listCategory = new List<PackageLib.Response.Category>();
        //    GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //    string Currency = objGlobalDefault.Currency;
        //    string Supplier = objGlobalDefault.Supplier;
        //    try
        //    {
        //        for (int i = 0; i < CategoryId.Split(',').ToList().Count; i++)
        //        {
        //            string sCategoryID = CategoryId.Split(',')[i];
        //            if (sCategoryID != "")
        //            {
        //                float AgentMarkup = 0;
        //                float ChildMarkup = 0;

        //                var sCategory = (from obj in db.tbl_PackagePricings where obj.nCategoryID == Convert.ToInt16(sCategoryID) && obj.nPackageID == PackageID select obj).FirstOrDefault();
        //                if (sCategory == null)
        //                    continue;
        //                CommonLib.Response.ServiceCharge Charge = GetCharges(Convert.ToSingle(sCategory.dCouple), 0, Currency, Supplier, 2);
        //                listCategory.Add(new PackageLib.Response.Category
        //                {
        //                    Itinerary = GetItinerary(sCategoryID, PackageID),
        //                    Categoryname = arrCategory[Convert.ToInt16(sCategoryID) - 1],
        //                    CategoryID = Convert.ToInt16(sCategoryID),
        //                    Inclusions = GetFacilities(sCategory.sStaticInclusions, sCategory.sDynamicInclusion, "Inclusions"),
        //                    Exclusion = GetFacilities(sCategory.sStaticExclusion, sCategory.sDynamicExclusion, "Exclusion"),
        //                    AdultPrice = GetCharges(Convert.ToSingle(sCategory.dSingleAdult), 0, Currency, Supplier, 1).TotalPrice,
        //                    TwinsSharing = GetCharges(Convert.ToSingle(sCategory.dCouple), 0, Currency, Supplier, 1).TotalPrice,
        //                    ChildPrice = GetCharges(Convert.ToSingle(sCategory.dKidWOBed), 0, Currency, Supplier, 1).TotalPrice,
        //                    InfantPrice = GetCharges(Convert.ToSingle(sCategory.dInfantKid), 0, Currency, Supplier, 1).TotalPrice,
        //                    Charges = GetCharges(Convert.ToSingle(sCategory.dCouple), 0, Currency, Supplier, 1),
        //                    ChildNoBedPrice = GetCharges(Convert.ToSingle(sCategory.dKidWBed), 0, Currency, Supplier, 1).TotalPrice,
        //                    ExtraAdult = GetCharges(Convert.ToSingle(sCategory.dExtraAdult), 0, Currency, Supplier, 1).TotalPrice,
        //                    TripleSharing = GetCharges(Convert.ToSingle(sCategory.dCouple + sCategory.dExtraAdult), 0, Currency, Supplier, 1).TotalPrice,
        //                    TotalPrice = (Charge.Rate + Charge.ServiceTax + Charge.TDS + Charge.AgentMarkup),

        //                });

        //            }
        //        }
        //    }
        //    catch
        //    {

        //    }

        //    return listCategory;
        //}


        /*Package Images */
        public static List<CommonLib.Response.Image> ListImages(Int64 PackageID, string Tiltle)
        {
            List<CommonLib.Response.Image> ListImage = new List<CommonLib.Response.Image>();
            try
            {
                var sImages = (from obj in db.tbl_PackageImages where obj.nPackageID == PackageID select obj).FirstOrDefault().ImageArray;
                string[] arrImg = Regex.Split(sImages.Replace("_", "-"), @"\^\-\^");
                foreach (var Img in arrImg.ToList())
                {
                    bool isDefault = false;
                    if (sImages.IndexOf(Img) == 0)
                        isDefault = true;
                    ListImage.Add(new CommonLib.Response.Image
                    {
                        IsDefault = isDefault,
                        Title = Tiltle,
                        Type = "",
                        Url = ConfigurationManager.AppSettings["URL"] + "Admin/thumbnail.aspx?fn=" + Img.Replace("-", "_") + "&w=342&h=342&productfolder=" + PackageID + "&rf=",
                        Count = arrImg.ToList().Count

                    });
                }
            }
            catch
            {

            }
            return ListImage;
        }


        /*Package Itinerary*/
        //public static List<PackageLib.Response.Itinerary> GetItinerary(string CategoryID, Int64 PackageID)
        //{
        //    List<PackageLib.Response.Itinerary> listItinerary = new List<PackageLib.Response.Itinerary>();
        //    var sItinerary = (from obj in db.tbl_Itineraries where obj.nPackageID == PackageID && obj.nCategoryID == Convert.ToInt64(CategoryID) select obj).FirstOrDefault();
        //    try
        //    {
        //        if (sItinerary.sItinerary_1 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_1, 1, PackageID, CategoryID));
        //        }
        //        if (sItinerary.sItinerary_2 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_2, 2, PackageID, CategoryID));
        //        }
        //        if (sItinerary.sItinerary_3 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_3, 3, PackageID, CategoryID));
        //        }
        //        if (sItinerary.sItinerary_4 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_4, 4, PackageID, CategoryID));
        //        }
        //        if (sItinerary.sItinerary_5 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_5, 5, PackageID, CategoryID));
        //        }
        //        if (sItinerary.sItinerary_6 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_6, 6, PackageID, CategoryID));
        //        }
        //        if (sItinerary.sItinerary_7 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_7, 7, PackageID, CategoryID));
        //        }
        //        if (sItinerary.sItinerary_8 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_8, 8, PackageID, CategoryID));
        //        }
        //        if (sItinerary.sItinerary_9 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_9, 9, PackageID, CategoryID));
        //        }
        //        if (sItinerary.sItinerary_10 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_10, 10, PackageID, CategoryID));
        //        }
        //        if (sItinerary.sItinerary_11 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_11, 11, PackageID, CategoryID));
        //        }
        //        if (sItinerary.sItinerary_12 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_12, 12, PackageID, CategoryID));
        //        }
        //        if (sItinerary.sItinerary_13 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_13, 13, PackageID, CategoryID));
        //        }
        //        if (sItinerary.sItinerary_14 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_14, 14, PackageID, CategoryID));
        //        }
        //        if (sItinerary.sItinerary_15 != "")
        //        {
        //            listItinerary.Add(GenrateItinerary(sItinerary.sItinerary_15, 15, PackageID, CategoryID));
        //        }
        //    }
        //    catch
        //    {


        //    }
        //    return listItinerary;
        //}
        //public static PackageLib.Response.Itinerary GenrateItinerary(string Itinerary, int noDays, Int64 PackageID, string CategoryID)
        //{
        //    PackageLib.Response.Itinerary objItinerary = new PackageLib.Response.Itinerary();
        //    try
        //    {
        //        string[] sData = Regex.Split(Itinerary, @"\^\-\^");
        //        var arrPackageHotel = (from obj in db.tbl_ItineraryHotels where obj.nPackageID == PackageID && obj.nCategoryID == Convert.ToInt64(CategoryID) select obj).FirstOrDefault();
        //        objItinerary.noDay = noDays;
        //        objItinerary.Date = dtJourney.AddDays(noDays);
        //        objItinerary.Description = sData[2];
        //        objItinerary.HotelDetails = new PackageLib.Response.HotelDetails();

        //        string HotelCode = "";
        //        if (noDays == 1)
        //            HotelCode = arrPackageHotel.sHotel_Code_1;
        //        if (noDays == 2)
        //        {
        //            HotelCode = arrPackageHotel.sHotel_Code_2;
        //        }
        //        if (noDays == 3)
        //        {
        //            HotelCode = arrPackageHotel.sHotel_Code_3;
        //        }

        //        if (noDays == 4)
        //        {
        //            HotelCode = arrPackageHotel.sHotel_Code_4;
        //        }
        //        if (noDays == 5)
        //        {
        //            HotelCode = arrPackageHotel.sHotel_Code_5;
        //        }
        //        if (noDays == 6)
        //        {
        //            HotelCode = arrPackageHotel.sHotel_Code_6;
        //        }
        //        if (noDays == 7)
        //        {
        //            HotelCode = arrPackageHotel.sHotel_Code_7;
        //        }
        //        if (noDays == 8)
        //        {
        //            HotelCode = arrPackageHotel.sHotel_Code_8;
        //        }
        //        if (noDays == 9)
        //        {
        //            HotelCode = arrPackageHotel.sHotel_Code_9;
        //        }
        //        if (noDays == 10)
        //        {
        //            HotelCode = arrPackageHotel.sHotel_Code_10;
        //        }
        //        if (noDays == 11)
        //        {
        //            HotelCode = arrPackageHotel.sHotel_Code_11;
        //        }
        //        if (noDays == 12)
        //        {
        //            HotelCode = arrPackageHotel.sHotel_Code_12;
        //        }
        //        if (noDays == 13)
        //        {
        //            HotelCode = arrPackageHotel.sHotel_Code_13;
        //        }
        //        if (noDays == 14)
        //        {
        //            HotelCode = arrPackageHotel.sHotel_Code_14;
        //        }

        //        string Address = (from obj in db.CONTACTs where obj.HotelCode == HotelCode select obj).FirstOrDefault().Address;
        //        var arrDetails = (from obj in db.HOTELs where obj.HotelCode == HotelCode select obj).FirstOrDefault();
        //        //string HotelName = (from obj in db.HOTELs where obj.HotelCode == HotelCode select obj).FirstOrDefault().Name;
        //        string Description = (from obj in db.HOTEL_DESCRIPTIONs where obj.HotelCode == HotelCode && obj.LanguageCode == "ENG" select obj).FirstOrDefault().HotelFacilities;
        //        objItinerary.HotelDetails.Details = new CommonLib.Response.CommonHotelDetails
        //        {
        //            Address = Address,
        //            Category = arrDetails.CategoryCode,    //Hotel Rsting
        //            Description = Description,
        //            Facility = new List<CommnFacilities>(),
        //            HotelId = HotelCode,
        //            HotelName = arrDetails.Name,
        //            Langitude = arrDetails.Longitude,
        //            Latitude = arrDetails.Latitude,
        //            Image = new List<Image>(),
        //            UserRating = 0,
        //        };

        //        var arrImageDetails = (from obj in db.HOTEL_IMAGEs where obj.HotelCode == HotelCode select obj).ToList();
        //        List<Image> Images = new List<Image>();
        //        foreach (var objImage in arrImageDetails)
        //        {
        //            objItinerary.HotelDetails.Details.Image.Add(new Image
        //            {
        //                Url = objImage.ImagePath,
        //            });
        //        }
        //        var arrHotel = (from obj in db.FACILITies
        //                        where obj.HotelCode == HotelCode
        //                        select new
        //                        {
        //                            obj.Code,
        //                            obj.Group_
        //                        }).Take(12).ToList();
        //        List<string> sFacility = new List<string>();
        //        foreach (var obj in arrHotel)
        //        {
        //            string Facility = (from objFaciliy in db.FACILITY_DESCRIPTIONs where objFaciliy.Code == obj.Code && objFaciliy.Group_ == obj.Group_ && objFaciliy.LanguageCode == "ENG" select objFaciliy.Name).FirstOrDefault();
        //            //objItinerary.HotelDetails.Details.Facility.Add(new CommnFacilities
        //            //{
        //            //    Name = Facility
        //            //});
        //            sFacility.Add(Facility);
        //        }
        //        objItinerary.HotelDetails.Details.Facility = ParseCommonResponse.GetCommonFacilities(sFacility);

        //    }
        //    catch
        //    {

        //    }
        //    return objItinerary;
        //}

        ///* Facilities */
        //public static List<PackageLib.Response.Facility> GetFacilities(string static_Facilities, string Dynamic_Facilities, string Type)
        //{
        //    List<PackageLib.Response.Facility> ListFacilities = new List<PackageLib.Response.Facility>();
        //    try
        //    {
        //        for (int i = 0; i < static_Facilities.Split(',').ToList().Count; i++)
        //        {
        //            string sID = static_Facilities.Split(',')[i];
        //            string sName = "";
        //            if (sID != "")
        //            {
        //                sName = Facilities[Convert.ToInt16(sID) - 1];

        //                ListFacilities.Add(new PackageLib.Response.Facility
        //                {
        //                    ID = Convert.ToInt16(sID),
        //                    Name = sName,
        //                    Type = Type

        //                });
        //            }

        //        }

        //        for (int i = 0; i < Dynamic_Facilities.Split(',').ToList().Count; i++)
        //        {
        //            if (Dynamic_Facilities.Split(',')[i] != "")
        //            {
        //                ListFacilities.Add(new PackageLib.Response.Facility
        //                {
        //                    ID = ListFacilities.Count() + i,
        //                    Name = Dynamic_Facilities.Split(',')[i],
        //                    Type = Type

        //                });
        //            }

        //        }

        //    }
        //    catch
        //    {

        //    }
        //    return ListFacilities;
        //}


        //#region Set Price & TAX
        //static float TotalCommission = 0;
        //static float TotalMarkup = 0;
        //public static float GetCutAmount(float BaseAmt, string currency, Int64 NoofPax, string Supplier, out float AgentRoomMarkup)
        //{
        //    try
        //    {
        //        TotalCommission = 0;
        //        TotalMarkup = 0;
        //        float CutRoomAmtWithMarkup = 0;
        //        float CutRoomAmount = 0;

        //        //For Global Markup Amount // 
        //        float CutGlobalMarkupAmt = 0; float CutGlobalMarkupPer = 0; float ActualGlobalMarkupAmt = 0;

        //        float CutGlobalCommAmt = 0; float CutGlobalCommPer = 0; float ActualGlobalCommAmt = 0;

        //        //For Group Markup Amount // 
        //        float CutGroupMarkupAmt = 0; float CutGroupMarkupPer = 0; float ActualGroupMarkupAmt = 0;
        //        float CutGroupCommAmt = 0; float CutGroupCommPer = 0; float ActualGroupCommAmt = 0;

        //        //For Individual Markup Amount // 
        //        float CutIndividualMarkupAmt = 0; float CutIndividualMarkupPer = 0; float ActualIndividualMarkupAmt = 0;
        //        float CutIndividualCommAmt = 0; float CutIndividualCommPer = 0; float ActualIndividualCommAmt = 0;

        //        //For Agent Own Markup Amount // 
        //        float ActualAgentRoomMarkup = 0; AgentRoomMarkup = 0; float AgentOwnMarkupPer = 0; float AgentOwnMarkupAmt = 0;

        //        float GlobalMarkup = 0; float GroupMarkup = 0; float IndividualMarkup = 0;

        //        float ServiceTaxAmount = 0;
        //        float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).GlobalMarkupPer;
        //        float GlobalCommPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).GlobalCommPer,
        //         GlobalCommAmt = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).GlobalCommAmt;

        //        float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).GroupMarkupPer;
        //        if (currency == "INR")
        //        {
        //            // CutGlobalMarkup //
        //            CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * BaseAmt);


        //            //CutGlobalMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GlobalMarkAmt;
        //            ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
        //            TotalMarkup += ActualGlobalMarkupAmt;

        //            // Global Commission
        //            GlobalMarkup = ActualGlobalMarkupAmt + BaseAmt;
        //            CutGlobalCommPer = ((GlobalCommPer / 100) * GlobalMarkup);
        //            TotalCommission += CutGlobalCommPer;
        //            GlobalMarkup = GlobalMarkup - CutGlobalCommPer;


        //            // CutGroupMarkup //

        //            CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
        //            //CutGroupMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GroupMarkAmt;
        //            ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
        //            TotalMarkup += ActualGroupMarkupAmt;

        //            // Group Commision 
        //            GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;
        //            float GroupCommPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).GroupCommPer;
        //            CutGroupCommPer = ((GroupCommPer / 100) * GroupMarkup);
        //            TotalCommission += CutGroupCommPer;
        //            GroupMarkup = GroupMarkup - CutGroupCommPer;

        //            // CutIndividualMarkup //
        //            if (objMarkupsAndTaxes.listIndividualMarkup != null && objMarkupsAndTaxes.listIndividualMarkup.Count != 0)
        //            {
        //                float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).IndMarkupPer;
        //                CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
        //                float IndMarkAmt = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).IndMarkAmt;
        //                CutIndividualMarkupAmt = (NoofPax) * IndMarkAmt;
        //                ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
        //                TotalMarkup += ActualIndividualMarkupAmt;
        //                IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;

        //                // Individual Commission
        //                float IndividualCommPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).IndCommPer;
        //                IndividualCommPer = ((IndividualCommPer / 100) * IndividualMarkup);
        //                IndividualMarkup = IndividualMarkup - IndividualCommPer;
        //                TotalCommission += IndividualCommPer;
        //            }
        //            CutRoomAmtWithMarkup = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
        //            bool TaxOnMarkup = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).TaxOnMarkup;
        //            if (TaxOnMarkup == true)
        //            {
        //                ServiceTaxAmount = GetMarkupServiceTax(ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt, objMarkupsAndTaxes.PerServiceTax);
        //                CutRoomAmtWithMarkup = CutRoomAmtWithMarkup + ServiceTaxAmount;
        //            }
        //            // Start Agent Markup //

        //            //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
        //            float AgentMarkupPer = 0;
        //            if (objMarkupsAndTaxes.listAgentMarkup != null && objMarkupsAndTaxes.listAgentMarkup.Count != 0)
        //            {
        //                AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "6").AgentOwnMarkupPer;
        //                AgentOwnMarkupAmt = (NoofPax) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "6").AgentOwnMarkupAmt;
        //            }

        //            AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);

        //            ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
        //            //End Agent Markup // 

        //            CutRoomAmount = (BaseAmt + CutRoomAmtWithMarkup);
        //            AgentRoomMarkup = ActualAgentRoomMarkup;


        //        }
        //        else
        //        {
        //            // CutGlobalMarkup //
        //            CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * BaseAmt);
        //            //CutGlobalMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GlobalMarkAmt;
        //            ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
        //            GlobalMarkup = ActualGlobalMarkupAmt + BaseAmt;

        //            // CutGroupMarkup //
        //            CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
        //            //CutGroupMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GroupMarkAmt;
        //            ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
        //            GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

        //            // CutIndividualMarkup //
        //            if (objMarkupsAndTaxes.listIndividualMarkup != null && objMarkupsAndTaxes.listIndividualMarkup.Count != 0)
        //            {
        //                float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).IndMarkupPer;
        //                CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
        //                float IndMarkAmt = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).IndMarkAmt;
        //                CutIndividualMarkupAmt = (NoofPax) * IndMarkAmt;
        //                ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
        //                IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
        //            }


        //            CutRoomAmtWithMarkup = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
        //            bool TaxOnMarkup = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).TaxOnMarkup;
        //            if (TaxOnMarkup == true)
        //            {
        //                ServiceTaxAmount = GetMarkupServiceTax(ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt, objMarkupsAndTaxes.PerServiceTax);
        //                CutRoomAmtWithMarkup = CutRoomAmtWithMarkup + ServiceTaxAmount;
        //            }
        //            // Start Agent Markup //

        //            //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
        //            float AgentMarkupPer = 0;
        //            if (objMarkupsAndTaxes.listAgentMarkup != null && objMarkupsAndTaxes.listAgentMarkup.Count != 0)
        //            {
        //                AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "6").AgentOwnMarkupPer;
        //            }
        //            AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
        //            //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.AgentOwnMarkupAmt;
        //            ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
        //            //End Agent Markup // 

        //            CutRoomAmount = (BaseAmt + CutRoomAmtWithMarkup);
        //            AgentRoomMarkup = ActualAgentRoomMarkup;


        //        }
        //        return CutRoomAmount;
        //    }
        //    catch (Exception ex)
        //    {
        //        string Line = ex.StackTrace.ToString();
        //        throw new Exception("");
        //    }
        //}

        //public static float AgentMarkups(float BaseAmount, float CutMarkup, float FranchiseeMarkup)
        //{
        //    float AgentMarkup = 0, AgentOwnMarkupPer = 0, AgentOwnMarkupAmt = 0;
        //    float IndividualMarkup = BaseAmount + CutMarkup + FranchiseeMarkup;
        //    float AgentMarkupPer = 0;
        //    if (objMarkupsAndTaxes.listAgentMarkup != null && objMarkupsAndTaxes.listAgentMarkup.Count != 0)
        //    {
        //        AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "6").AgentOwnMarkupPer;
        //        AgentOwnMarkupAmt = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "6").AgentOwnMarkupAmt;
        //    }
        //    AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
        //    AgentMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
        //    return AgentMarkup;
        //}
        //public static float GetMarkupServiceTax(float MarkupAmt, float ServiceTax)
        //{
        //    return (MarkupAmt * ServiceTax / 100);
        //}

        //public static float Getgreatervalue(float Percentage, float Ammount)
        //{
        //    if (Percentage > Ammount)
        //    {
        //        return Percentage;
        //    }
        //    else
        //    {
        //        return Ammount;
        //    }
        //}
        //public static float GetComm_value(float Percentage, float Ammount)
        //{
        //    if (Percentage < Ammount)
        //    {
        //        return Percentage;
        //    }
        //    else
        //    {
        //        return Ammount;
        //    }
        //}
        ////public static List<GSTdetails> GetFranchiseGST(float Rate, float CutMarkup, float CUTCommission, float CutGst, float TDS, int noRoom, out float Markup)
        ////{
        ////    Markup = 0;
        ////    List<GSTdetails> List_GST = new List<GSTdetails>();
        ////    float Percantage = 0, Amount = 0, FranchGroupMarkup = 0, FranchTotalMarkup = 0;
        ////    float BaseAmt = (Rate + CutMarkup + CutGst + TDS) - CUTCommission;
        ////    ListGstTax objGst = objMarkupsAndTaxes.ListGstTax.Where(d => d.IsCUTGST == true).FirstOrDefault();
        ////    if (objMarkupsAndTaxes.List_FranchGroup.Count != 0)
        ////    {
        ////        Percantage = objMarkupsAndTaxes.List_FranchGroup.Where(d => d.SupplierType == "Hotel").FirstOrDefault().AgentOwnMarkupPer / 100 + BaseAmt;
        ////        Amount = objMarkupsAndTaxes.List_FranchGroup.Where(d => d.SupplierType == "Hotel").FirstOrDefault().AgentOwnMarkupAmt + BaseAmt;
        ////        FranchTotalMarkup = Getgreatervalue(Percantage, Amount);
        ////    }
        ////    // Individual Markup
        ////    if (objMarkupsAndTaxes.List_FranchIndividual.Count != 0)
        ////    {
        ////        try
        ////        {
        ////            Percantage = 0; Amount = 0;
        ////            Percantage = objMarkupsAndTaxes.List_FranchIndividual.Where(d => d.SupplierType == "Hotel").FirstOrDefault().AgentOwnMarkupPer / 100 + FranchTotalMarkup;
        ////            Amount = objMarkupsAndTaxes.List_FranchIndividual.Where(d => d.SupplierType == "Hotel").FirstOrDefault().AgentOwnMarkupAmt + FranchTotalMarkup;
        ////            FranchTotalMarkup = Getgreatervalue(Percantage, Amount);
        ////        }
        ////        catch
        ////        {

        ////        }

        ////    }
        ////    if (objMarkupsAndTaxes.List_FranchGroup.Count != 0 || objMarkupsAndTaxes.List_FranchIndividual.Count != 0)
        ////    {
        ////        if (FranchTotalMarkup != 0)
        ////            Markup = (FranchTotalMarkup - BaseAmt) * noRoom;
        ////        List_GST = MarkupTaxManager.GetGstDetails(Markup, FranchTotalMarkup, "Hotel", true);
        ////    }

        ////    return List_GST;

        ////}

        //#region Set Service Charges For Hotel
        //public static CommonLib.Response.ServiceCharge GetCharges(float SupplierRate, float Tax, string Currency, string Supplier, int noPax)
        //{
        //    CommonLib.Response.ServiceCharge objCharge = new ServiceCharge();
        //    try
        //    {
        //        if (HttpContext.Current == null)
        //            HttpContext.Current = Context;
        //        Models.CommonHotels objCommonHotels = (Models.CommonHotels)HttpContext.Current.Session["Common"];
        //        CUT.Models.AvailCommonRequest DisplayRequest = (CUT.Models.AvailCommonRequest)HttpContext.Current.Session["DisplayRequest"]; ;
        //        GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
        //        float RateINR = 0;
        //        if (Currency != "INR" && UserCurrency != Currency)
        //        {
        //            RateINR = SupplierRate * objGlobalDefault.ExchangeRate.Where(d => d.Currency == Currency).FirstOrDefault().Rate;

        //        }
        //        else
        //            RateINR = SupplierRate;
        //        if (UserCurrency != "INR" && UserCurrency != Currency)
        //        {
        //            RateINR = RateINR / objGlobalDefault.ExchangeRate.Where(d => d.Currency == UserCurrency).FirstOrDefault().Rate;
        //        }
        //        objCharge.Rate = RateINR;
        //        objCharge.ServiceTax = Tax;
        //        objCharge.Discount = 0;
        //        float CutMarkup = 0, AgentMarkup = 0;
        //        CutMarkup = GetCutAmount(RateINR, objGlobalDefault.Currency, noPax, Supplier, out AgentMarkup);
        //        objCharge.Commission = TotalCommission;
        //        objCharge.CUTMarkup = TotalMarkup;
        //        objCharge.AgentCurrency = objGlobalDefault.Currency;

        //        // Gst Check // 
        //        //ListGstTax objGst = objMarkupsAndTaxes.ListGstTax.Where(d => d.Service == "Hotel").FirstOrDefault();
        //        //objCharge.IsOnMarkup = objGst.OnMarkup;
        //        if (objMarkupsAndTaxes.SupplierCommision.Exists(c => c.Supplier == Supplier.Replace("Mgh", "MGH")))
        //            objCharge.TDS = TotalCommission * objMarkupsAndTaxes.SupplierCommision.Where(c => c.Supplier == Supplier.Replace("Mgh", "MGH")).FirstOrDefault().TDS / 100;
        //        objCharge.GSTdetails = MarkupTaxManager.GetGstDetails(TotalMarkup, ((RateINR + Tax + TotalMarkup) - (objCharge.Commission + 0)), "Hotel", false);
        //        float FranchiseMarkup = 0;
        //        float TotalCutGST = objCharge.GSTdetails.Select(d => d.Amount).ToList().Sum();
        //        objCharge.FranchiseGSTdetails = GetFranchiseGST(RateINR, TotalMarkup, TotalCommission, TotalCutGST, objCharge.TDS, noPax, out FranchiseMarkup);
        //        objCharge.FranchiseeMarkup = FranchiseMarkup;
        //        objCharge.AgentMarkup = AgentMarkups(RateINR, TotalMarkup, FranchiseMarkup / noPax);
        //        objCharge.TotalPrice = (RateINR + objCharge.FranchiseeMarkup + objCharge.ServiceTax +
        //                                objCharge.TDS + objCharge.FranchiseGSTdetails.Select(d => d.Amount).ToList().Sum()) - (objCharge.Commission + objCharge.Discount);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }

        //    return objCharge;
        //}
        //#endregion
        //#endregion

        //#region Packages Deatinations By Country
        //public static List<CUT.Models.Country> GetDestination()
        //{
        //    List<CUT.Models.Country> ListDestinations = new List<Models.Country>();
        //    try
        //    {
        //        var arrtDestination = (from obj in db.tbl_Packages select obj.sPackageDestination).Distinct().ToList();
        //        List<string> ListDestination = new List<string>();
        //        foreach (var Destination in arrtDestination)
        //        {
        //            string[] split = Destination.Split(',');
        //            if (split.Length >= 2)
        //            {
        //                string DES = split[1];
        //                ListDestination.Add(DES.Split('|')[0].TrimEnd(' ').ToUpper());
        //            }
        //        }
        //        foreach (var Destination in ListDestination.Distinct().ToList())
        //        {
        //            var sPackage = (from obj in db.tbl_Packages
        //                            where obj.sPackageDestination.Contains(Destination)
        //                            select new { ID = obj.nID, Name = obj.sPackageName, obj.dValidityFrom }).ToList();
        //            List<Models.Package> ListPackage = new List<Models.Package>();
        //            foreach (var Package in sPackage)
        //            {
        //                if (DateTime.ParseExact(Package.dValidityFrom, "dd-MM-yyyy hh:mm", System.Globalization.CultureInfo.InvariantCulture) <= DateTime.Now)
        //                {
        //                    ListPackage.Add(new Models.Package { ID = Package.ID, Name = Package.Name });
        //                }
        //            }
        //            ListDestinations.Add(new Models.Country
        //            {
        //                CountyryName = Destination,
        //                ListPackage = ListPackage
        //            });
        //        }

        //    }
        //    catch
        //    {

        //    }
        //    return ListDestinations;
        //}
        //#endregion

        //#region Get Category Details
        //public static PackageLib.Response.PackageDetails GetPackageDetails(Int64 ID)
        //{
        //    PackageLib.Response.PackageDetails objPackage = new PackageLib.Response.PackageDetails();
        //    try
        //    {
        //        if (HttpContext.Current.Session["PackageDetails"] == null)
        //            throw new Exception("Packages Not Found Please Reseach");
        //        List<PackageLib.Response.PackageDetails> ListPackages = (List<PackageLib.Response.PackageDetails>)HttpContext.Current.Session["PackageDetails"];
        //        objPackage = ListPackages.Where(d => d.Packageid == ID).FirstOrDefault();
        //        if (objPackage == null)
        //            throw new Exception("Packages Not Found Please Reseach");
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }

        //    return objPackage;
        //}
        //#endregion


        #region GetBooking List

        public static List<Dictionary<string, object>> GetBookingByAgent()
        {
            List<Dictionary<string, object>> List = new List<Dictionary<string, object>>();
            DataTable dtResult = new DataTable();
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            var UserType = objGlobalDefault.UserType;
            if (UserType == "Admin")
            {
                try
                {
                    var arrList = (from obj in db.tbl_PackageReservations
                                   join objPck in db.tbl_Packages on obj.PackageID equals objPck.nID
                                   join objAgnt in db.tbl_AdminLogins on obj.uid equals objAgnt.sid
                                   select new
                                   {
                                       //ID = obj.PackageID,
                                       Supplier = objAgnt.AgencyName,
                                       TravelDate = obj.TravelDate,
                                       PaxName = obj.LeadingPax,
                                       PackageName = objPck.sPackageName,
                                       CatID = arrCategory[Convert.ToInt32(obj.CategoryId) - 1],
                                       Location = objPck.sPackageDestination,
                                       StartDate = obj.StartFrom,
                                       EndDate = obj.EndDate,
                                       Status = obj.Status
                                   }).ToList();
                    CutAdmin.GenralHandler.ListtoDataTable lsttodt = new CutAdmin.GenralHandler.ListtoDataTable();
                    dtResult = lsttodt.ToDataTable(arrList);
                    HttpContext.Current.Session["dtPackages"] = dtResult;
                    List = JsonStringManager.ConvertDataTable(dtResult);
                }
                catch
                {

                }
            }

            else
            {
                try
                {
                    var arrList = (from obj in db.tbl_PackageReservations
                                   join objPck in db.tbl_Packages on obj.PackageID equals objPck.nID
                                   where obj.uid == objGlobalDefault.sid
                                   select new
                                   {
                                       //ID = obj.PackageID,
                                       TravelDate = obj.TravelDate,
                                       PaxName = obj.LeadingPax,
                                       PackageName = objPck.sPackageName,
                                       CatID = arrCategory[Convert.ToInt32(obj.CategoryId) - 1],
                                       Location = objPck.sPackageDestination,
                                       StartDate = obj.StartFrom,
                                       EndDate = obj.EndDate,
                                       Status = obj.Status
                                   }).ToList();
                    CutAdmin.GenralHandler.ListtoDataTable lsttodt = new CutAdmin.GenralHandler.ListtoDataTable();
                    dtResult = lsttodt.ToDataTable(arrList);
                    HttpContext.Current.Session["dtPackages"] = dtResult;
                    List = JsonStringManager.ConvertDataTable(dtResult);
                }
                catch
                {

                }
            }

            return List;
        }

        #endregion
    }


}
