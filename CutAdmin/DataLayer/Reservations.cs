﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CutAdmin.dbml;
using System.Globalization;
using System.Data;
using CutAdmin.Models;

namespace CutAdmin.DataLayer
{
    public class Reservations
    {
        public int Sid { get; set; }
        public string ReservationID { get; set; }
        public string ReservationDate { get; set; }
        public string AgencyName { get; set; }
        public string Status { get; set; }
        public string BookStatus { get; set; }
        public string bookingname { get; set; }
        public decimal? TotalFare { get; set; }
        public int TotalRooms { get; set; }
        public string HotelName { get; set; }
        public string sCheckIn { get; set; }
        public string CheckIn { get; set; }
        public string CheckOut { get; set; }
        public string City { get; set; }
        public int? Children { get; set; }
        public string BookingStatus { get; set; }
        public int? NoOfAdults { get; set; }
        public string Source { get; set; }
        public Int64 Uid { get; set; }
        public string LatitudeMGH { get; set; }
        public string LongitudeMGH { get; set; }
        public string CurrencyCode { get; set; }
        public bool? IsConfirm { get; set; }
        public bool? isReconfirm { get; set; }
        public string HotelConfirmID { get; set; }
        public string Deadline { get; set; }
        //static    static helperDataContext db = new helperDataContext();
        public static string search{get;set;}
        public static List<Reservations> BookingList()
        {
            List<Reservations> ListReservation = new List<Reservations>();
          CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            Int64 Uid = AccountManager.GetAdminByLogin();
            try
            {
                if (search == null)
                    search = "";
                using (var DB = new CutAdmin.dbml.helperDataContext())
                {
                   IQueryable<Reservations> result = (from obj in DB.tbl_HotelReservations
                                       join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                        where (search == null
                                        || (obj.ReservationDate != null && obj.ReservationDate.ToLower().Contains(search.ToLower())
                                        || AgName.AgencyName != null && obj.AgencyName.ToLower().Contains(search.ToLower())
                                        || obj.bookingname != null && obj.bookingname.ToString().ToLower().Contains(search.ToLower())
                                        || obj.HotelName != null && obj.HotelName.ToString().ToLower().Contains(search.ToLower())
                                        || obj.CheckIn != null && obj.CheckIn.ToString().ToLower().Contains(search.ToLower())
                                        || obj.TotalRooms != null && obj.TotalRooms.ToString().ToLower().Contains(search.ToLower())
                                        || obj.TotalFare != null && obj.TotalFare.ToString().ToLower().Contains(search.ToLower())))
                                        && AgName.ParentID == Uid
                                                      select new Reservations
                                       {
                                           AgencyName = AgName.AgencyName,
                                           bookingname = obj.bookingname,
                                           BookingStatus = obj.BookingStatus,
                                           CurrencyCode = AgName.CurrencyCode,
                                           IsConfirm = obj.IsConfirm,
                                           CheckIn = obj.CheckIn,
                                           sCheckIn = obj.CheckIn,
                                           CheckOut = obj.CheckOut,
                                           Children = obj.Children,
                                           City = obj.City,
                                           HotelName = obj.HotelName,
                                           LatitudeMGH = obj.LatitudeMGH,
                                           LongitudeMGH = obj.LongitudeMGH,
                                           NoOfAdults = obj.NoOfAdults,
                                           ReservationDate = obj.ReservationDate,
                                           ReservationID = obj.ReservationID,
                                           Sid = obj.Sid,
                                           Source = obj.Source != AccountManager.GetSuperAdminID().ToString() ? obj.Source : objGlobalDefault.AgencyName,
                                           Status = obj.Status,
                                           BookStatus = obj.BookingStatus,
                                           TotalFare = obj.TotalFare,
                                           TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                           Uid = Convert.ToInt64(obj.Uid),
                                           isReconfirm = obj.HotelConfirmationNo != "" ,
                                           HotelConfirmID = obj.HotelConfirmationNo,
                                           Deadline = obj.DeadLine
                                       });
                    ListReservation = result.ToList();
                    ListReservation = ListReservation.Where(s => s.AgencyName != "TEST AGENCY M").ToList();
                    ListReservation = ListReservation.OrderByDescending(s => s.Sid).ToList();
                }
            }
            catch (Exception ex)
            {
            }
            return ListReservation;
        }

        public static object GetBookings()
        {
            GenralHandler.ListtoDataTable lsttodt = new GenralHandler.ListtoDataTable();
            DataTable dtResult = new DataTable();
            HttpContext.Current.Session["SupplierBookingList"] = null;
            object arrData = new { };
            try
            {
                
                int TotalCount = 0;
                search = DTResult.sSearch;

                string sortOrder = DTResult.sSortDir_0;
                int start = DTResult.iDisplayStart;
                int length = DTResult.iDisplayLength;
                List<Reservations> arrResult = BookingList();
                TotalCount = arrResult.Count;
                arrResult = arrResult.Skip(start).Take(length).ToList();
                int Sorting = DTResult.iSortingCols;
                //if (Sorting == 1)
                {
                    string sortColumns = Sorting.ToString();

                    switch (sortColumns)
                    {
                        case "0":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.ReservationDate).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.ReservationDate).ToList();
                            break;
                        case "1":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.ReservationID).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.ReservationID).ToList();
                            break;
                        case "2":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.AgencyName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.AgencyName).ToList();
                            break;
                        case "3":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.bookingname).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.bookingname).ToList();
                            break;
                        case "4":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.HotelName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.HotelName).ToList();
                            break;
                        case "5":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.CheckIn).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.CheckIn).ToList();
                            break;
                        case "6":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.TotalRooms).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.TotalRooms).ToList();
                            break;
                        case "7":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.TotalFare).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.TotalFare).ToList();
                            break;
                    }
                }
                dtResult = lsttodt.ToDataTable(arrResult);
                HttpContext.Current.Session["SupplierBookingList"] = dtResult;
                dtResult.Dispose();
                arrData = DTResult.GetDataTable(arrResult, TotalCount);
            }
            catch (Exception ex)
            {
            }
            return arrData;
        }
        public static List<Reservations> GroupBookingList()
        {
            List<Reservations> ListReservation = new List<Reservations>();
          CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                using (var DB = new CutAdmin.dbml.helperDataContext())
                {
                    ListReservation = (from obj in DB.tbl_HotelReservations
                                       join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                       where obj.Uid == Uid && obj.BookingStatus == "GroupRequest"
                                       select new Reservations
                                       {
                                           AgencyName = AgName.AgencyName,
                                           bookingname = obj.bookingname,
                                           BookingStatus = obj.BookingStatus,
                                           CurrencyCode = AgName.CurrencyCode,
                                           IsConfirm = obj.IsConfirm,
                                           CheckIn = obj.CheckIn,
                                           sCheckIn = obj.CheckIn,
                                           CheckOut = obj.CheckOut,
                                           Children = obj.Children,
                                           City = obj.City,
                                           HotelName = obj.HotelName,
                                           LatitudeMGH = obj.LatitudeMGH,
                                           LongitudeMGH = obj.LongitudeMGH,
                                           NoOfAdults = obj.NoOfAdults,
                                           ReservationDate = obj.ReservationDate,
                                           ReservationID = obj.ReservationID,
                                           Sid = obj.Sid,
                                           Source = obj.Status,
                                           Status = obj.Status,
                                           TotalFare = obj.TotalFare,
                                           TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                           Uid = Convert.ToInt64(obj.Uid)

                                       }).Distinct().ToList();
                    if (objGlobalDefault.UserType != "Agent")
                    {
                        var arrResrvation = (from obj in DB.tbl_HotelReservations
                                             join AgName in DB.tbl_AdminLogins on obj.Uid equals AgName.sid
                                             where AgName.ParentID == Uid && obj.BookingStatus == "GroupRequest"
                                             select new Reservations
                                             {
                                                 AgencyName = AgName.AgencyName,
                                                 bookingname = obj.bookingname,
                                                 BookingStatus = obj.BookingStatus,
                                                 CurrencyCode = AgName.CurrencyCode,
                                                 CheckIn = obj.CheckIn,
                                                 CheckOut = obj.CheckOut,
                                                 sCheckIn = obj.CheckIn,
                                                 Children = obj.Children,
                                                 City = obj.City,
                                                 IsConfirm = obj.IsConfirm,
                                                 HotelName = obj.HotelName,
                                                 LatitudeMGH = obj.LatitudeMGH,
                                                 LongitudeMGH = obj.LongitudeMGH,
                                                 NoOfAdults = obj.NoOfAdults,
                                                 ReservationDate = obj.ReservationDate,
                                                 ReservationID = obj.ReservationID,
                                                 Sid = obj.Sid,
                                                 Source = obj.Status,
                                                 Status = obj.Status,
                                                 TotalFare = obj.TotalFare,
                                                 TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                                 Uid = Convert.ToInt64(obj.Uid)

                                             }).ToList().Distinct();
                        foreach (var obj in arrResrvation)
                        {
                            ListReservation.Add(obj);
                        }
                    }
                    ListReservation = ListReservation.OrderByDescending(s => s.ReservationID).ToList();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return ListReservation;
        }

        #region SearchBooking
        public static List<Reservations> Search(string CheckIn, string CheckOut, string Passenger, string BookingDate, string Reference, string HotelName, string Location, string ReservationStatus, out int TotalCount)
        {
            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            Int64 Uid = AccountManager.GetAdminByLogin();
            List<Reservations> ListReservation = new List<Reservations>();
            List<Reservations> arrResult = new List<Reservations>();
            TotalCount = 0;
            var arrData = DTResult.arrTable;
            string search = DTResult.sSearch;

            string sortOrder = DTResult.sSortDir_0;
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;
            using (var db = new CutAdmin.dbml.helperDataContext())
            {
                IQueryable<Reservations> result = (from obj in db.tbl_HotelReservations
                                                   join AgName in db.tbl_AdminLogins on obj.Uid equals AgName.sid
                                                   where (search == null
                                                   || (obj.ReservationDate != null && obj.ReservationDate.ToLower().Contains(search.ToLower())
                                                   || AgName.AgencyName != null && obj.AgencyName.ToLower().Contains(search.ToLower())
                                                   || obj.bookingname != null && obj.bookingname.ToString().ToLower().Contains(search.ToLower())
                                                   || obj.HotelName != null && obj.HotelName.ToString().ToLower().Contains(search.ToLower())
                                                   || obj.CheckIn != null && obj.CheckIn.ToString().ToLower().Contains(search.ToLower())
                                                   || obj.TotalRooms != null && obj.TotalRooms.ToString().ToLower().Contains(search.ToLower())
                                                   || obj.TotalFare != null && obj.TotalFare.ToString().ToLower().Contains(search.ToLower())))
                                                   && AgName.ParentID == Uid
                                                   select new Reservations
                                                   {
                                                       AgencyName = AgName.AgencyName,
                                                       bookingname = obj.bookingname,
                                                       BookingStatus = obj.BookingStatus,
                                                       CurrencyCode = AgName.CurrencyCode,
                                                       IsConfirm = obj.IsConfirm,
                                                       CheckIn = obj.CheckIn,
                                                       sCheckIn = obj.CheckIn,
                                                       CheckOut = obj.CheckOut,
                                                       Children = obj.Children,
                                                       City = obj.City,
                                                       HotelName = obj.HotelName,
                                                       LatitudeMGH = obj.LatitudeMGH,
                                                       LongitudeMGH = obj.LongitudeMGH,
                                                       NoOfAdults = obj.NoOfAdults,
                                                       ReservationDate = obj.ReservationDate,
                                                       ReservationID = obj.ReservationID,
                                                       Sid = obj.Sid,
                                                       Source = obj.Source != AccountManager.GetSuperAdminID().ToString() ? obj.Source : objGlobalDefault.AgencyName,
                                                       Status = obj.Status,
                                                       BookStatus = obj.BookingStatus,
                                                       TotalFare = obj.TotalFare,
                                                       TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                                       Uid = Convert.ToInt64(obj.Uid),
                                                       isReconfirm = obj.HotelConfirmationNo != "",
                                                       HotelConfirmID = obj.HotelConfirmationNo,
                                                       Deadline = obj.DeadLine
                                                   });
                ListReservation = result.ToList();
                arrResult = result.ToList();
                var arrFilter = new List<Reservations>();
                if (CheckIn != "")
                {
                    arrFilter = arrResult.Where(item => item.CheckIn == CheckIn).ToList();
                    arrResult = arrFilter;
                }

                if (CheckOut != "")
                {
                    arrFilter = arrResult.Where(item => item.CheckOut == CheckOut).ToList();
                    arrResult = arrFilter;
                }

                if (Passenger != "")
                {
                    arrFilter = arrResult.Where(item => item.bookingname == Passenger).ToList();
                    arrResult = arrFilter;
                }

                if (BookingDate != "")
                {
                    arrFilter = arrResult.Where(item => item.ReservationDate == BookingDate).ToList();
                    arrResult = arrFilter;
                }

                if (Reference != "")
                {
                    arrFilter = arrResult.Where(item => item.ReservationID == Reference).ToList();
                    arrResult = arrFilter;
                }

                if (HotelName != "")
                {
                    arrFilter = arrResult.Where(item => item.HotelName == HotelName).ToList();
                    arrResult = arrFilter;
                }

                if (Location != "")
                {
                    arrFilter = arrResult.Where(item => item.City == Location).ToList();
                    arrResult = arrFilter;
                }

                if (ReservationStatus != "" && ReservationStatus != "All")
                {
                    arrFilter = arrResult.Where(item => item.Status == ReservationStatus).ToList();
                    arrResult = arrFilter;
                }

                TotalCount = arrResult.Count();
                arrResult = arrResult.Skip(start).Take(length).ToList();
         
                }
                return arrResult;
            }

        }
        #endregion

    }