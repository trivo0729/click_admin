﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.EntityModal;
using System.Globalization;
using CommonLib.Response;
using CutAdmin.DataLayer;
using System.Data;
using static CutAdmin.ActivityHandller;
using Elmah;

namespace CutAdmin.DataLayer
{
    public class SightseeingInventoryManager
    {
        public static void SaveInventory(Int32 ActivityID, Int64 Supplier, Int32[] arrSlot, Int32[] arrTicketType, string MaxTicket, string DtTill, string[] DateInvFr, string[] DateInvTo, string OptTicketperDate, string InvLiveOrReq, string InType, string InventoryState, List<string> arrDays)
        {
            try
            {
                using (var DB = new CUT_LIVE_UATSTEntities())
                {
                    //string SupplierID = AccountManager.GetSuperAdminID().ToString();
                    List<tbl_SightseeingInventory> arrInventory = new List<tbl_SightseeingInventory>();
                    tbl_SightseeingInventory Inventory = new tbl_SightseeingInventory();
                    foreach (var Slot in arrSlot)/*Slot*/
                    {
                        string InvSoldType = InvLiveOrReq;
                        foreach (var sFromDate in DateInvFr.ToList())
                        {
                            CutAdmin.Common.Common.arrDays = new List<string>();
                            CutAdmin.Common.Common.StartDate = DateTime.ParseExact(sFromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            CutAdmin.Common.Common.EndDate = DateTime.ParseExact(DateInvTo[DateInvFr.ToList().IndexOf(sFromDate)], "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            foreach (var TicketType in arrTicketType)  /*Ticket Type*/
                            {
                                List<DateTime> ListDates = CutAdmin.Common.Common.Calander;
                                foreach (var nMonth in ListDates.Select(m => m.Month).Distinct().ToList())
                                {
                                    string sMonth = nMonth.ToString();
                                    string sYear = ListDates.Where(m => m.Month == nMonth).ToList().FirstOrDefault().Year.ToString();
                                    IQueryable<tbl_SightseeingInventory> arrOldInv = from obj in DB.tbl_SightseeingInventory
                                                                                     where obj.Activity_Id == ActivityID
                                                                                     && obj.SlotID == Slot &&
                                                                                     obj.Year == sYear && obj.Month == sMonth && obj.TicketType == TicketType
                                                                                     && obj.SupplierId == Supplier
                                                                                     && obj.InventoryType == InType
                                                                                     select obj;/* Check Old Inventory */
                                    foreach (var sDate in ListDates)
                                    {
                                        if (arrOldInv.ToList().Count == 0)
                                        {
                                            if (!arrInventory.Exists(d => d.Activity_Id == ActivityID && d.SlotID == Slot && d.TicketType == TicketType && d.Month == sMonth && d.Year == sYear))
                                            {
                                                arrInventory.Add(new tbl_SightseeingInventory
                                                {
                                                    Activity_Id = ActivityID,
                                                    SupplierId = Supplier,
                                                    SlotID = Slot,
                                                    TicketType = TicketType,
                                                    Month = sMonth,
                                                    InventoryType = InType,
                                                    Year = sYear,
                                                    FreeSaleTill = DtTill,
                                                });
                                                string sInventory = InType + "_" + MaxTicket + "_" + "0";
                                                if (InventoryState == "ss")
                                                    sInventory = InventoryState + "_" + MaxTicket + "_" + "0";

                                                string OptSlot = OptTicketperDate + "_" + DtTill + "_" + InvLiveOrReq;
                                                SetInventory(arrInventory.LastOrDefault(), sDate.Day.ToString(), sInventory, OptSlot);
                                            }
                                            else
                                            {
                                                Inventory = arrInventory.Where(d => d.Activity_Id == ActivityID && d.SlotID == Slot && d.TicketType == TicketType && d.Month == sMonth && d.Year == sYear).FirstOrDefault();
                                                string sInventory = InType + "_" + MaxTicket + "_" + "0";
                                                string OptSlot = OptTicketperDate + "_" + DtTill + "_" + InvLiveOrReq;
                                                SetInventory(arrInventory.LastOrDefault(), sDate.Day.ToString(), sInventory, OptSlot);
                                            }
                                        }
                                        else
                                        {
                                            string OptSlot = "";
                                            Inventory = arrOldInv.ToList().Where(d => d.Activity_Id == ActivityID && d.SlotID == Slot && d.TicketType == TicketType && d.Month == sMonth && d.Year == sYear).FirstOrDefault();
                                            string sInventory = GettInventory(Inventory, sDate.Day.ToString(), out OptSlot);
                                            if (sInventory!=null)
                                            {
                                                if (sInventory.Split('_').ToList().Count != 0)
                                                {
                                                    sInventory = InType + "_" + MaxTicket + "_" + sInventory.Split('_').ToList()[2];
                                                    if (InventoryState == "ss")
                                                        sInventory = InventoryState + "_" + MaxTicket + "_" + sInventory.Split('_').ToList()[2];
                                                }
                                                else
                                                {
                                                    sInventory = InType + "_" + MaxTicket + "_" + "0";
                                                    if (InventoryState == "ss")
                                                        sInventory = InventoryState + "_" + MaxTicket + "_" + "0";
                                                }
                                            }
                                            else
                                            {
                                                sInventory = InType + "_" + MaxTicket + "_" + "0";
                                                if (InventoryState == "ss")
                                                    sInventory = InventoryState + "_" + MaxTicket + "_" + "0";
                                            }
                                            
                                            if (OptSlot != null && OptSlot.Split('_').ToList().Count != 0 && OptSlot.Split('_').ToList().Count == 2)
                                            {
                                                OptSlot = DtTill + "_" + InvSoldType;
                                            }
                                            else
                                            {
                                                OptSlot = OptTicketperDate + "_" + DtTill + "_" + InvLiveOrReq;
                                            }
                                            SetInventory(Inventory, sDate.Day.ToString(), sInventory, OptSlot);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    DB.tbl_SightseeingInventory.AddRange(arrInventory);
                    DB.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }


        public static tbl_SightseeingInventory SetInventory(tbl_SightseeingInventory Update, string Day, string Inventory, string Option_Date)
        {
            try
            {
                switch (Day)
                {
                    case "1":
                        Update.Date_1 = Inventory;
                        Update.Option_Date_1 = Option_Date;
                        break;
                    case "2":
                        Update.Date_2 = Inventory;
                        Update.Option_Date_2 = Option_Date;
                        break;
                    case "3":
                        Update.Date_3 = Inventory;
                        Update.Option_Date_3 = Option_Date;
                        break;
                    case "4":
                        Update.Date_4 = Inventory;
                        Update.Option_Date_4 = Option_Date;
                        break;
                    case "5":
                        Update.Date_5 = Inventory;
                        Update.Option_Date_5 = Option_Date;
                        break;
                    case "6":
                        Update.Date_6 = Inventory;
                        Update.Option_Date_6 = Option_Date;
                        break;
                    case "7":
                        Update.Date_7 = Inventory;
                        Update.Option_Date_7 = Option_Date;
                        break;
                    case "8":
                        Update.Date_8 = Inventory;
                        Update.Option_Date_8 = Option_Date;
                        break;
                    case "9":
                        Update.Date_9 = Inventory;
                        Update.Option_Date_9 = Option_Date;
                        break;
                    case "10":
                        Update.Date_10 = Inventory;
                        Update.Option_Date_10 = Option_Date;
                        break;
                    case "11":
                        Update.Date_11 = Inventory;
                        Update.Option_Date_11 = Option_Date;
                        break;
                    case "12":
                        Update.Date_12 = Inventory;
                        Update.Option_Date_12 = Option_Date;
                        break;
                    case "13":
                        Update.Date_13 = Inventory;
                        Update.Option_Date_13 = Option_Date;
                        break;
                    case "14":
                        Update.Date_14 = Inventory;
                        Update.Option_Date_14 = Option_Date;
                        break;
                    case "15":
                        Update.Date_15 = Inventory;
                        Update.Option_Date_15 = Option_Date;
                        break;
                    case "16":
                        Update.Date_16 = Inventory;
                        Update.Option_Date_16 = Option_Date;
                        break;
                    case "17":
                        Update.Date_17 = Inventory;
                        Update.Option_Date_17 = Option_Date;
                        break;
                    case "18":
                        Update.Date_18 = Inventory;
                        Update.Option_Date_18 = Option_Date;
                        break;
                    case "19":
                        Update.Date_19 = Inventory;
                        Update.Option_Date_19 = Option_Date;
                        break;
                    case "20":
                        Update.Date_20 = Inventory;
                        Update.Option_Date_1 = Option_Date;
                        break;
                    case "21":
                        Update.Date_21 = Inventory;
                        Update.Option_Date_21 = Option_Date;
                        break;
                    case "22":
                        Update.Date_22 = Inventory;
                        Update.Option_Date_22 = Option_Date;
                        break;
                    case "23":
                        Update.Date_23 = Inventory;
                        Update.Option_Date_23 = Option_Date;
                        break;
                    case "24":
                        Update.Date_24 = Inventory;
                        Update.Option_Date_24 = Option_Date;
                        break;
                    case "25":
                        Update.Date_25 = Inventory;
                        Update.Option_Date_25 = Option_Date;
                        break;
                    case "26":
                        Update.Date_26 = Inventory;
                        Update.Option_Date_26 = Option_Date;
                        break;
                    case "27":
                        Update.Date_27 = Inventory;
                        Update.Option_Date_27 = Option_Date;
                        break;
                    case "28":
                        Update.Date_28 = Inventory;
                        Update.Option_Date_28 = Option_Date;
                        break;
                    case "29":
                        Update.Date_29 = Inventory;
                        Update.Option_Date_29 = Option_Date;
                        break;
                    case "30":
                        Update.Date_30 = Inventory;
                        Update.Option_Date_30 = Option_Date;
                        break;
                    case "31":
                        Update.Date_31 = Inventory;
                        Update.Option_Date_31 = Option_Date;
                        break;
                }
            }
            catch (Exception)
            {
            }
            return Update;
        } /*Set Inventory*/


        public static string GettInventory(tbl_SightseeingInventory Update, string Day, out string OptSlot)
        {
            string Inventory = string.Empty;
            OptSlot = string.Empty;
            try
            {
                switch (Day)
                {
                    case "1":
                        Inventory = Update.Date_1;
                        OptSlot = Update.Option_Date_1;
                        break;
                    case "2":
                        Inventory = Update.Date_2;
                        OptSlot = Update.Option_Date_2;
                        break;
                    case "3":
                        Inventory = Update.Date_3;
                        OptSlot = Update.Option_Date_3;
                        break;
                    case "4":
                        Inventory = Update.Date_4;
                        OptSlot = Update.Option_Date_4;
                        break;
                    case "5":
                        Inventory = Update.Date_5;
                        OptSlot = Update.Option_Date_5;
                        break;
                    case "6":
                        Inventory = Update.Date_6;
                        OptSlot = Update.Option_Date_6;
                        break;
                    case "7":
                        Inventory = Update.Date_7;
                        OptSlot = Update.Option_Date_7;
                        break;
                    case "8":
                        Inventory = Update.Date_8;
                        OptSlot = Update.Option_Date_5;
                        break;
                    case "9":
                        Inventory = Update.Date_9;
                        OptSlot = Update.Option_Date_9;
                        break;
                    case "10":
                        Inventory = Update.Date_10;
                        OptSlot = Update.Option_Date_10;
                        break;
                    case "11":
                        Inventory = Update.Date_11;
                        OptSlot = Update.Option_Date_11;
                        break;
                    case "12":
                        Inventory = Update.Date_12;
                        OptSlot = Update.Option_Date_12;
                        break;
                    case "13":
                        Inventory = Update.Date_13;
                        OptSlot = Update.Option_Date_13;
                        break;
                    case "14":
                        Inventory = Update.Date_14;
                        OptSlot = Update.Option_Date_14;
                        break;
                    case "15":
                        Inventory = Update.Date_15;
                        OptSlot = Update.Option_Date_14;
                        break;
                    case "16":
                        Inventory = Update.Date_16;
                        OptSlot = Update.Option_Date_16;
                        break;
                    case "17":
                        Inventory = Update.Date_17;
                        OptSlot = Update.Option_Date_17;
                        break;
                    case "18":
                        Inventory = Update.Date_18;
                        OptSlot = Update.Option_Date_18;
                        break;
                    case "19":
                        Inventory = Update.Date_19;
                        OptSlot = Update.Option_Date_19;
                        break;
                    case "20":
                        Inventory = Update.Date_20;
                        OptSlot = Update.Option_Date_20;
                        break;
                    case "21":
                        Inventory = Update.Date_21;
                        OptSlot = Update.Option_Date_21;
                        break;
                    case "22":
                        Inventory = Update.Date_22;
                        OptSlot = Update.Option_Date_22;
                        break;
                    case "23":
                        Inventory = Update.Date_23;
                        OptSlot = Update.Option_Date_23;
                        break;
                    case "24":
                        Inventory = Update.Date_24;
                        OptSlot = Update.Option_Date_24;
                        break;
                    case "25":
                        Inventory = Update.Date_25;
                        OptSlot = Update.Option_Date_25;
                        break;
                    case "26":
                        Inventory = Update.Date_26;
                        OptSlot = Update.Option_Date_26;
                        break;
                    case "27":
                        Inventory = Update.Date_27;
                        OptSlot = Update.Option_Date_27;
                        break;
                    case "28":
                        Inventory = Update.Date_28;
                        OptSlot = Update.Option_Date_28;
                        break;
                    case "29":
                        Inventory = Update.Date_29;
                        OptSlot = Update.Option_Date_29;
                        break;
                    case "30":
                        Inventory = Update.Date_30;
                        OptSlot = Update.Option_Date_30;
                        break;
                    case "31":
                        Inventory = Update.Date_31;
                        OptSlot = Update.Option_Date_31;
                        break;
                }
            }
            catch (Exception)
            {
            }
            return Inventory;
        }

        public static List<sdate> GetSlotInventory(Int32 ActivityID, List<Int32> arrSlots, Int32 TicketType, string InventoryType, Int32 SupplierID, List<DateTime> arrDates)
        {
            List<sdate> arrInventory = new List<sdate>();
            try
            {
                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    IQueryable<tbl_SightseeingInventory> listInventory = (from obj in db.tbl_SightseeingInventory
                                                                          where obj.Activity_Id == ActivityID && arrSlots.Contains(obj.SlotID) &&
                                                                          obj.SupplierId == SupplierID && obj.InventoryType == InventoryType && obj.TicketType == TicketType
                                                                          select obj);
                    if (listInventory.ToList().Count != 0)
                    {
                        var ListInventory = listInventory.ToList();
                        foreach (var Slot in arrSlots)
                        {
                            foreach (var Date in arrDates)
                            {
                                string Type = "";
                                string noTickets = "";
                                string dateMonth = Date.Month.ToString();
                                string dateYear = Date.Year.ToString();
                                var arrSlotInventory = ListInventory.Where(d => d.SlotID == Slot && d.Year == dateYear && d.Month == dateMonth).ToList();
                                Int64 Count = 0;
                                Int64 Booked = 0;
                                string Saletype = "";
                                if (arrSlotInventory.Count != 0)
                                {
                                    ListtoDataTable lsttodt = new ListtoDataTable();
                                    DataTable dt = lsttodt.ToDataTable(arrSlotInventory);

                                    string date = "";
                                    if (Date.ToString("dd").StartsWith("0"))
                                        date = "Date_" + Date.ToString("dd").TrimStart('0');
                                    else
                                        date = "Date_" + Date.ToString("dd");
                                    noTickets = dt.Rows[0][date].ToString();
                                    if (noTickets != "")
                                    {
                                        string noTotalTickets = noTickets.Split('_')[1];
                                        if (noTotalTickets != "")
                                        {
                                            Count = Count + Convert.ToInt64(noTotalTickets);
                                            Booked = Convert.ToInt64(noTickets.Split('_')[2]);
                                            Saletype = noTickets.Split('_')[0];
                                        }
                                        else
                                        {
                                            Booked = Convert.ToInt64(noTickets.Split('_')[2]);
                                            Saletype = noTickets.Split('_')[0];
                                        }
                                    }

                                }
                                Int64 Sid = 0;
                                if (noTickets != "")
                                {
                                    Sid = arrSlotInventory.FirstOrDefault().Sid;
                                    Type = Booked + "_" + arrSlotInventory.FirstOrDefault().TicketType;
                                    InventoryType = arrSlotInventory.FirstOrDefault().InventoryType;
                                }
                                else
                                    InventoryType = "0";

                                arrInventory.Add(new sdate
                                {
                                    SlotId = Convert.ToInt16(Slot),
                                    RateTypeId = Convert.ToInt16(Sid),
                                    datetime = Date.ToString("dd-MM-yyyy"),
                                    day = Date.Day.ToString(),
                                    NoOfCount = Count,
                                    Type = Type,
                                    discount = Saletype,
                                    InventoryType = InventoryType,
                                });
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return arrInventory;
        }


        #region Get Valid Markets & Cancellation
        public class Market
        {
            public string Markets { get; set; }
        }
        //public static List<string> GetMarket(string FromDate, string ToDate, string HotelCode, string RoomID, string MealPlan, string Supplier)
        public static List<string> GetMarket(string FromDate, string ActivityID, string TourType, string TicketType, string InventoryType, string Supplier)
        {
            List<string> arrMarket = new List<string>();
            Int64 AdminID = AccountManager.GetAdminByLogin();
            try
            {
                DateTime Date = DateTime.ParseExact(FromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                var Month = Date.Month;
                var Year = Date.Year;
                var Day = Date.Day.ToString();

                using (var db = new CUT_LIVE_UATSTEntities())
                {
                    IQueryable<Market> arrData = (from obj in db.tbl_SightseeingRates
                                                  join objPlan in db.tbl_SightseeingRatePlan on obj.nPlanID.ToString() equals objPlan.PlanID.ToString()
                                                  where objPlan.InventoryType == InventoryType && obj.sMonth == Month.ToString() && obj.sYear == Year.ToString() && obj.Activity_Id.ToString() == ActivityID &&
                                                  objPlan.RateType.ToString() == TourType && obj.sActivityType.ToString() == TicketType &&
                                                  obj.nSupplierID.ToString() == Supplier && obj.ParentID == AdminID
                                                  select new Market
                                                  {
                                                      Markets = objPlan.Market
                                                  });
                    if (arrData.ToList().Count != 0)
                    {
                        var arrMarkets = arrData.ToList();
                        foreach (var obj in arrData.ToList())
                        {
                            if (!arrMarket.Contains(obj.Markets))
                            {
                                foreach (var Market in obj.Markets.Split(','))
                                {
                                    if (!arrMarket.Contains(obj.Markets) && Market != "")
                                        arrMarket.Add(Market);
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("No Rates Found.");
            }
            return arrMarket.Distinct().ToList();

        }
        #endregion

        #region Update Inventory
        public static CUT_LIVE_UATSTEntities db { get; set; }
        public static void UpdateInventory(tbl_SightseeingBooking arrBookingDetail, Int64 Supplier)
        {
            try
            {
                DateTime Date = DateTime.ParseExact(arrBookingDetail.Sightseeing_Date, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                var Month = Date.Month;
                var Year = Date.Year;
                var Day = Date.Day;
                db = new CUT_LIVE_UATSTEntities();
                using (var updateinvetory = db.Database.BeginTransaction())
                {
                    tbl_SightseeingInventory Inventory = new tbl_SightseeingInventory();

                    IQueryable<tbl_SightseeingInventory> arrInv = from obj in db.tbl_SightseeingInventory
                                                                  where obj.Activity_Id == arrBookingDetail.Activity_Id
                                                                  && obj.SlotID == arrBookingDetail.SlotID &&
                                                                  obj.Year == Year.ToString() && obj.Month == Month.ToString() && obj.TicketType == arrBookingDetail.Ticket_Type
                                                                  && obj.SupplierId == Supplier
                                                                  && obj.InventoryType == arrBookingDetail.InventoryType
                                                                  select obj;/* Check Old Inventory */

                    if (arrInv.ToList().Count != 0)
                    {
                        var ListInventory = arrInv.ToList();
                        var arrSlotInventory = ListInventory.Where(d => d.SlotID == arrBookingDetail.SlotID && d.Year == Year.ToString() && d.Month == Month.ToString()).ToList();

                        if (arrSlotInventory.Count != 0)
                        {
                            ListtoDataTable lsttodt = new ListtoDataTable();
                            DataTable dt = lsttodt.ToDataTable(arrSlotInventory);
                            Int64 Count = 0;
                            Int64 Booked = 0;
                            string Saletype = "";
                            string date = "";
                            string UpdInventory = "";
                            if (Date.ToString("dd").StartsWith("0"))
                                date = "Date_" + Date.ToString("dd").TrimStart('0');
                            else
                                date = "Date_" + Date.ToString("dd");
                            string noTickets = dt.Rows[0][date].ToString();
                            string noTotalTickets = "";
                            string sMaxTicket = "";

                            if (noTickets != "")
                            {
                                if (noTickets.Split('_').Length == 3)
                                {
                                    noTotalTickets = noTickets.Split('_')[2];
                                    sMaxTicket = noTickets.Split('_')[1];
                                    Count = Convert.ToInt64(noTotalTickets) + Convert.ToInt64(1);
                                    UpdInventory = arrBookingDetail.InventoryType + '_' + sMaxTicket + '_' + Count;
                                }

                                else
                                {
                                    noTotalTickets = noTickets.Split('_')[1];
                                    Count = Convert.ToInt64(noTotalTickets) + Convert.ToInt64(1);
                                    UpdInventory = arrBookingDetail.InventoryType + '_' + Count;
                                }
                                //Booked = Convert.ToInt64(noTickets.Split('_')[2]);
                                //Saletype = noTickets.Split('_')[0];
                            }

                            tbl_SightseeingInventory Update = db.tbl_SightseeingInventory.Single(x => x.Sid == arrSlotInventory[0].Sid);
                            //Update.date = UpdInventory;
                            db.SaveChanges();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static void UpdateInventory(tbl_SightseeingBooking arrBookingDetail)
        {
            try
            {
                using (var DB = new CUT_LIVE_UATSTEntities())
                {
                    List<tbl_SightseeingInventory> arrInventory = new List<tbl_SightseeingInventory>();
                    tbl_SightseeingInventory Inventory = new tbl_SightseeingInventory();
                    Decimal Count = 0;

                    CutAdmin.Common.Common.arrDays = new List<string>();
                    CutAdmin.Common.Common.StartDate = DateTime.ParseExact(arrBookingDetail.Sightseeing_Date, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    CutAdmin.Common.Common.EndDate = DateTime.ParseExact(arrBookingDetail.Sightseeing_Date, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    List<DateTime> ListDates = CutAdmin.Common.Common.Calander;
                    foreach (var nMonth in ListDates.Select(m => m.Month).Distinct().ToList())
                    {
                        string sMonth = nMonth.ToString();
                        string sYear = ListDates.Where(m => m.Month == nMonth).ToList().FirstOrDefault().Year.ToString();
                        IQueryable<tbl_SightseeingInventory> arrOldInv = from obj in DB.tbl_SightseeingInventory
                                                                         where obj.Activity_Id == arrBookingDetail.Activity_Id
                                                                         && obj.SlotID == arrBookingDetail.SlotID &&
                                                                         obj.Year == sYear && obj.Month == sMonth && obj.TicketType == arrBookingDetail.Ticket_Type
                                                                         && obj.SupplierId == arrBookingDetail.SupplierId
                                                                         && obj.InventoryType == arrBookingDetail.InventoryType
                                                                         select obj;/* Check Old Inventory */
                        foreach (var sDate in ListDates)
                        {
                            if (arrOldInv.ToList().Count == 0)
                            {
                                if (!arrInventory.Exists(d => d.Activity_Id == arrBookingDetail.Activity_Id && d.SlotID == arrBookingDetail.SlotID && d.TicketType == arrBookingDetail.Ticket_Type && d.Month == sMonth && d.Year == sYear))
                                {
                                    arrInventory.Add(new tbl_SightseeingInventory
                                    {
                                        Activity_Id = Convert.ToInt32(arrBookingDetail.Activity_Id),
                                        SupplierId = arrBookingDetail.SupplierId,
                                        SlotID = Convert.ToInt32(arrBookingDetail.SlotID),
                                        TicketType = Convert.ToInt32(arrBookingDetail.Ticket_Type),
                                        Month = sMonth,
                                        InventoryType = arrBookingDetail.InventoryType,
                                        Year = sYear,

                                    });
                                }
                            }
                            else
                            {
                                string OptSlot = "";
                                Inventory = arrOldInv.ToList().Where(d => d.Activity_Id == arrBookingDetail.Activity_Id && d.SlotID == arrBookingDetail.SlotID && d.TicketType == arrBookingDetail.Ticket_Type && d.Month == sMonth && d.Year == sYear).FirstOrDefault();
                                string sInventory = GettInventory(Inventory, sDate.Day.ToString(), out OptSlot);

                                if (sInventory.Split('_').ToList().Count != 0)
                                {
                                    Count = Convert.ToDecimal(sInventory.Split('_').ToList()[2]) + Convert.ToDecimal(arrBookingDetail.TotalPax);
                                    sInventory = sInventory.Split('_')[0] + "_" + sInventory.Split('_')[1] + "_" + Count;
                                }
                                SetInventory(Inventory, sDate.Day.ToString(), sInventory, OptSlot);
                            }
                        }
                    }
                    DB.tbl_SightseeingInventory.AddRange(arrInventory);
                    DB.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
        }


        #endregion
    }

    public class sdate
    {
        public int? SlotId { get; set; }
        public int RateTypeId { get; set; }
        public string datetime { get; set; }
        public string day { get; set; }
        public long NoOfCount { get; set; }
        public string Type { get; set; }
        public string discount { get; set; }
        public string InventoryType { get; set; }
        
    }
}