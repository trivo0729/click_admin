﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CutAdmin.dbml;
using CutAdmin.EntityModal;

namespace CutAdmin.DataLayer
{
    public class RoomManager
    {

        #region Images
        public static DataLayer.DataManager.DBReturnCode RoomImages(string ImagePath, string RoomId, string HotelId)
        {
            string[] Path = ImagePath.Split('^');
            string MainImage = ImagePath.Split('^')[0];
            int i = 0; ImagePath = "";
            foreach (string Image in Path)
            {


                if (Image != "")
                {
                    if ((Path.Length - 1) != i)
                    {
                        string Img = Image.Split('.')[0];

                        ImagePath += Img + ".jpg" + "^";
                    }

                    else
                    {
                        string Imge = Image.Split('.')[0];
                        ImagePath += Imge + ".jpg";
                    }

                }
                i++;
            }
            
            StringBuilder sSQL = new StringBuilder();
            DataLayer.DataManager.DBReturnCode retCopde = DataManager.DBReturnCode.EXCEPTION;
            if (ImagePath != "")
            {
                // sSQL.Append("Update tbl_commonRoomDetails set RoomImage ='" + MainImage + "', SubImages='" + ImagePath + "' where RoomId='" + RoomId + "' and  HotelId='" + HotelId + "'");
                using (var db = new Click_Hotel())
                {
                    tbl_commonRoomDetails Update = db.tbl_commonRoomDetails.Single(x => x.RoomId.ToString() == RoomId);
                    if(Update.RoomImage == null || Update.RoomImage == "")
                    {
                        Update.RoomImage = MainImage;
                        Update.SubImages += ImagePath;
                    }
                   else
                    {
                        Update.SubImages += ImagePath;
                    }
                    db.SaveChanges();
                    return retCopde = DataManager.DBReturnCode.SUCCESS;
                }
            }
           
            return retCopde = DataManager.DBReturnCode.SUCCESS;
        }

        #endregion

        #region Room Type
        public static tbl_commonRoomType SaveRoomType(tbl_commonRoomType arrRoomType)
        {
            tbl_commonRoomType RoomType = new tbl_commonRoomType();
            try
            {
                using (var db = new Click_Hotel() )
                {
                    if (!db.tbl_commonRoomType.ToList().Exists(d => d.RoomType == arrRoomType.RoomType))
                    {
                        db.tbl_commonRoomType.Add(arrRoomType);
                        db.SaveChanges();
                        RoomType = arrRoomType;
                    }
                    
                    else
                    {
                        RoomType = db.tbl_commonRoomType.Where(d => d.RoomType == arrRoomType.RoomType).FirstOrDefault();
                    }
                   
                    
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return RoomType;
        }

        public static void SaveRooms(tbl_commonRoomType arrRoomType , tbl_commonRoomDetails arrRoom)
        {
            try
            {
                arrRoomType= SaveRoomType(arrRoomType);
                using (var db = new Click_Hotel())
                {
                    if(arrRoom.RoomId==0)
                    {
                        arrRoom.RoomTypeId = arrRoomType.RoomTypeID;
                        db.tbl_commonRoomDetails.Add(arrRoom);
                    }
                    else
                    {
                        var arrData = (db.tbl_commonRoomDetails.Where(d => d.RoomId.ToString() == arrRoom.RoomId.ToString()).FirstOrDefault());
                        arrData.RoomAmenitiesId = arrRoom.RoomAmenitiesId;
                        arrData.RoomDescription = arrRoom.RoomDescription;
                        arrData.RoomImage = arrRoom.RoomImage;
                        arrData.SubImages = arrRoom.SubImages;
                        arrData.SmokingAllowed = arrRoom.SmokingAllowed;
                        arrData.BeddingType = arrRoom.BeddingType;
                        arrData.RoomOccupancy = arrRoom.RoomOccupancy;
                        arrData.MaxExtrabedAllowed = arrRoom.MaxExtrabedAllowed;
                        arrData.NoOfChildWithoutBed = arrRoom.NoOfChildWithoutBed;
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

    }
}