﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;using CutAdmin.dbml;
using CutAdmin.EntityModal;

namespace CutAdmin.DataLayer
{
    public class MailManager
    {

        #region Send Invoice and Vouchers
        public static void SendMail(string sTo, string title, string sMail, string DocPath, string Cc)
        {

            List<string> from = new List<string>();
            List<string> DocPathList = new List<string>();
            Dictionary<string, string> Email1List = new Dictionary<string, string>();
            Dictionary<string, string> BccList = new Dictionary<string, string>();
            from.Add(Convert.ToString(ConfigurationManager.AppSettings["HotelMail"]));

            foreach (string mail in sTo.Split(',').Distinct().ToList())
            {
                if (mail != "" )
                {
                    Email1List.Add(mail, mail);
                }
            }
            foreach (string mail in Cc.Split(';'))
            {
                if (mail != "")
                {
                    BccList.Add(mail, mail);
                }
            }
            foreach (string link in DocPath.Split(';'))
            {
                if (link != "")
                {
                    DocPathList.Add(link);
                }
            }
            string accessKey = EmailManager.GetAccessKey(); 
            //string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
            MailManager.SendMail(accessKey, Email1List, BccList, title, sMail.ToString(), from, DocPathList);
        }


        public static void SendInvoice(string ReservationID,Int64 Uid,string sTo)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    Click_Hotel db  = new Click_Hotel();
                    /*Tausif Work*/
                    #region Send Mail
                    string URL = System.Configuration.ConfigurationManager.AppSettings["URL"];
                    Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"]);
                    string From = ConfigurationManager.AppSettings["HotelMail"];

                    var sData = (from obj in DB.tbl_AdminLogins where obj.sid == ParentID select obj).FirstOrDefault();
                    var dtHotelReservation = (from obj in DB.tbl_HotelReservations where obj.ReservationID == ReservationID select obj).FirstOrDefault();
                    var dtHotelContact = (from obj in db.Comm_HotelContacts where obj.HotelCode == Convert.ToInt64(dtHotelReservation.HotelCode) && obj.SupplierCode == AccountManager.GetSupplierByUser() && obj.Type != "HotelContact" select obj).ToList();
                    string HResEmail = "";
                    if (dtHotelContact.Count !=0)
                    {
                        foreach (var obj in dtHotelContact)
                        {
                            HResEmail += obj.email +";";
                        }
                        HResEmail = HResEmail.TrimEnd(';');
                    }

                    string title = "";
                    InvoiceMailManager.CheckIfProcessAlreadyRunning();
                    Int64 AgentID = AccountManager.GetUserByLogin();
                    string Invoice = InvoiceManager.GetInvoice(ReservationID, AgentID, out  title);
                    CutAdmin.DataLayer.EmailManager.GenrateAttachment(Invoice, ReservationID, "Invoice");
                    InvoiceMailManager.CheckIfProcessAlreadyRunning();
                    string Voucher = VoucherManager.GenrateVoucher(ReservationID, AgentID.ToString(), "Vouchered");
                    InvoiceMailManager.CheckIfProcessAlreadyRunning();
                    if (Voucher != "")
                        CutAdmin.DataLayer.EmailManager.GenrateAttachment(Voucher, ReservationID, "Voucher");
                    string sMail = CutAdmin.DataLayer.EmailManager.BookingMail4Agent(ReservationID);
                    string DocPath = URL + "InvoicePdf/" + ReservationID + "_Invoice.pdf";
                    if (Voucher != "")
                        DocPath += ";" + URL + "InvoicePdf/" + ReservationID + "_Voucher.pdf";

                    // Booking Mail To Agent 
                    //SendMail(sTo, title, sMail, DocPath, HResEmail);
                    //Booking mail to hotel
                    CutAdmin.DataLayer.EmailManager.BookingMail2Hotel(ReservationID, AgentID.ToString(), "Vouchered");
                    // Booking Confirm Mail To Admin
                    string Admin_Bc, Admin_Cc;
                    string sAdminMail = EmailManager.BookingMail2CUT(ReservationID, Uid, out Admin_Bc, out Admin_Cc);
                    SendMail(Admin_Bc, title, sAdminMail, DocPath, Admin_Cc);
                    #endregion
                }
            }
            catch
            {

            }
        }

        public static void SendReconfrmInvoice(string ReservationID, Int64 Uid, string sTo)
        {
            try
            {
                using (var DB = new helperDataContext())
                {
                    //Click_Hotel db  = new Click_Hotel();
                    /*Tausif Work*/
                    #region Send Mail
                    string URL = AccountManager.GetAdminDomain();
                    Int64 ParentID = Convert.ToInt64(AccountManager.GetAdminByLogin());
                    string From = AccountManager.GetUserMailByAdmin();

                    var sData = (from obj in DB.tbl_AdminLogins where obj.sid == ParentID select obj).FirstOrDefault();

                    string title = "";
                    InvoiceMailManager.CheckIfProcessAlreadyRunning();
                    Int64 AgentID = AccountManager.GetUserByLogin();
                    string Invoice = InvoiceManager.GetInvoice(ReservationID, AgentID, out  title);
                    CutAdmin.DataLayer.EmailManager.GenrateAttachment(Invoice, ReservationID, "Invoice");
                    InvoiceMailManager.CheckIfProcessAlreadyRunning();
                    string Voucher = VoucherManager.GenrateVoucher(ReservationID, AgentID.ToString(), "Vouchered");
                    InvoiceMailManager.CheckIfProcessAlreadyRunning();
                    if (Voucher != "")
                        CutAdmin.DataLayer.EmailManager.GenrateAttachment(Voucher, ReservationID, "Voucher");
                    string sMail = CutAdmin.DataLayer.EmailManager.BookingMail4Agent(ReservationID);
                    string DocPath = URL + "InvoicePdf/" + ReservationID + "_Invoice.pdf";
                    if (Voucher != "")
                        DocPath += ";" + URL + "InvoicePdf/" + ReservationID + "_Voucher.pdf";

                    // Booking Mail To Agent 
                    SendMail(sTo, title, sMail, DocPath, From);

                    // Booking Confirm Mail To Admin
                    string Admin_Bc, Admin_Cc;
                    string sAdminMail = EmailManager.BookingMail2CUT(ReservationID, Uid, out Admin_Bc, out Admin_Cc);
                    SendMail(Admin_Bc, title, sAdminMail, DocPath, Admin_Cc);
                    #endregion
                }
            }
            catch
            {

            }
        }
        #endregion


        public static bool GetAdminMails(string MailType, string Activity, string MailBody, string Subject,string Attachments,string To)
        {
            List<string> arrFrom = new List<string>();
            List<string> arrAttachments = new List<string>();
            Dictionary<string, string> arrTo = new Dictionary<string, string>();
            Dictionary<string, string> BccMails = new Dictionary<string, string>(), CcMails = new Dictionary<string, string>();
            string AccessKey = string.Empty;
            AccessKey = string.Empty;
            arrTo.Add(To, To);
            try
            {
                using (var db = new helperDataContext())
                {
                    var arrKeys = (from obj in db.tbl_AdminLogins where obj.sid == AccountManager.GetAdminByLogin() select obj).FirstOrDefault();
                    if (arrKeys != null)
                    {
                        AccessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                    }
                    else 
                    {
                        AccessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                    }
                    var arrMails = (from obj in db.tbl_ActivityMails where obj.Type == MailType && obj.Activity == Activity && obj.ParentID == AccountManager.GetAdminByLogin() select obj).FirstOrDefault();
                    if (arrMails != null)
                    {
                        if(arrMails.Email !="")
                            arrFrom.Add(arrMails.Email);
                        else
                            arrFrom.Add(AccountManager.GetUserMailByAdmin());
                        if (arrMails.BCcMail != null && arrMails.BCcMail != "")
                        {
                            foreach (var Mail in arrMails.BCcMail.Split(';'))
                            {
                                BccMails.Add(Mail, Mail);
                            }
                        }

                        if (arrMails.CcMail != null && arrMails.CcMail != "")
                        {
                            foreach (var Mail in arrMails.CcMail.Split(';'))
                            {
                                CcMails.Add(Mail, Mail);
                            }
                        }
                    }
                    else
                    {
                        arrFrom.Add(AccountManager.GetUserMailByAdmin());
                    }
                    foreach (var objAttachment in Attachments.Split(','))
                    {
                        if(objAttachment !="")
                        arrAttachments.Add(objAttachment);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return MailSent(AccessKey, arrTo, BccMails,CcMails,Subject,MailBody,arrFrom, arrAttachments);
        }

        public static bool SendMail(string Key,
                            Dictionary<string, string> to,
                            string subject,
                            string MailBody,
                            List<string> from_name,
                            List<string> attachment)
        {
            try
            {
                string accessKey = EmailManager.GetAccessKey();

               API test = new API(accessKey);
                Dictionary<string, Object> data = new Dictionary<string, Object>();
                data.Add("to", to);
                //data.Add("bcc", to);
                data.Add("from", from_name);
                data.Add("subject", subject);
                data.Add("html", MailBody);
                if (attachment != null && attachment.Count != 0)                    
                    data.Add("attachment", attachment);
                Object sendEmail = test.send_email(data);
               // return sendEmail.ToString();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public static bool SendMail(string Key,

                             Dictionary<string, string> to,
                             Dictionary<string, string> Bcc,
                             string subject,
                             string MailBody,
                             List<string> from_name,
                             List<string> attachment)
        {
            try
            {
                string accessKey = EmailManager.GetAccessKey();
                API test = new API(accessKey);
                //API test = new API(Key);
                Dictionary<string, Object> data = new Dictionary<string, Object>();
                data.Add("to", to);
                //data.Add("bcc", Bcc);
                data.Add("cc", Bcc);
                data.Add("from", from_name);
                data.Add("subject", subject);
                data.Add("html", MailBody);
                if (attachment != null && attachment.Count != 0)
                    data.Add("attachment", attachment);
                Object sendEmail = test.send_email(data);
                //return sendEmail.ToString();
                return true;
            }
            catch
            {
                return false;
            }

        }


        public static bool MailSent(string Key,

                             Dictionary<string, string> to,
                             Dictionary<string, string> Bcc,
                              Dictionary<string, string> Cc,
                             string subject,
                             string MailBody,
                             List<string> from_name,
                             List<string> attachment)
        {
            try
            {
                string accessKey = EmailManager.GetAccessKey();

                API test = new API(accessKey);
                // API test = new API(Key);
                Dictionary<string, Object> data = new Dictionary<string, Object>();
                data.Add("to", to);
                data.Add("bcc", Bcc);
                data.Add("cc", Cc);
                data.Add("from", from_name);
                data.Add("subject", subject);
                data.Add("html", MailBody);
                if (attachment != null && attachment.Count != 0)
                    data.Add("attachment", attachment);
                Object sendEmail = test.send_email(data);
                //return sendEmail.ToString();
                return true;
            }
            catch
            {
                return false;
            }

        }


    }
}