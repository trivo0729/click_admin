﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Transactions;
using System.Web;
using CutAdmin.EntityModal;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.IO;

namespace CutAdmin.DataLayer
{
    public class CurrencyList
    {
        public Int64 Sid { get; set; }
        public string ExchageCurrency { get; set; }
        public string PreferredCurrency { get; set; }
        public decimal Amount { get; set; }
        public decimal Percentage { get; set; }
        public ExchaneList ExchaneList { get; set; }
    }
    public class ExchaneList
    {
        public string Currency { get; set; }
        public string Preferred { get; set; }
        public decimal ExchaneRate { get; set; }
        public decimal MarkupRate { get; set; }
        public decimal TotalExchange { get; set; }

        public Double? Rate { get; set; }
        public Double? Exchange { get; set; }
    }
    public class ExchangeRateManager
    {
        #region Add Update ExcgangeRate
        public static DBHelper.DBReturnCode GetLastRecordDate(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_ExChangeRateLoadAll", out dtResult);
            return retCode;
        }

        public static DBHelper.DBReturnCode InsertExchangeRate(List<string> Currency, string ApplicableDate, List<decimal> INR)
        {
            Int64 ID = 0;
            int rowsAffected = 0;
            decimal FCY = 1;
            string MerchantID = "";
            bool Flag = true;
            string MerchantURL = "";
            DateTime dt2 = DateTime.ParseExact(ApplicableDate, "d/M/yyyy", CultureInfo.InvariantCulture);
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.SUCCESS;
            //DateTime date = DateTime.ParseExact(ApplicableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            for (int i = 0; i < Currency.Count; i++)
            {
                SqlParameter[] sqlParams = new SqlParameter[8];
                sqlParams[0] = new SqlParameter("@ID", ID);
                sqlParams[1] = new SqlParameter("@Currency", Currency[i]);
                sqlParams[2] = new SqlParameter("@FCY", FCY);
                sqlParams[3] = new SqlParameter("@INR", INR[i]);
                sqlParams[4] = new SqlParameter("@ApplicableDate", dt2);
                sqlParams[5] = new SqlParameter("@MerchantID", MerchantID);
                sqlParams[6] = new SqlParameter("@Flag", Flag);
                sqlParams[7] = new SqlParameter("@MerchantURL", MerchantURL);
                retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExChangeRateAddUpdate", out rowsAffected, sqlParams);
            }
            return retCode;
        }

        //public static DBHelper.DBReturnCode InsertExchangeRate(string Currency, string ApplicableDate, decimal INR)
        //{
        //    Int64 ID = 0;
        //    int rowsAffected = 0;
        //    decimal FCY = 1;
        //    string MerchantID = "";
        //    bool Flag = true;
        //    string MerchantURL = "";
        //    //DateTime date = DateTime.ParseExact(ApplicableDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
        //    SqlParameter[] sqlParams = new SqlParameter[8];
        //    sqlParams[0] = new SqlParameter("@ID", ID);
        //    sqlParams[1] = new SqlParameter("@Currency", Currency);
        //    sqlParams[2] = new SqlParameter("@FCY", FCY);
        //    sqlParams[3] = new SqlParameter("@INR", INR);
        //    sqlParams[4] = new SqlParameter("@ApplicableDate", ApplicableDate);
        //    sqlParams[5] = new SqlParameter("@MerchantID", MerchantID);
        //    sqlParams[6] = new SqlParameter("@Flag", Flag);
        //    sqlParams[7] = new SqlParameter("@MerchantURL", MerchantURL);
        //    DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExChangeRateAddUpdate", out rowsAffected, sqlParams);
        //    return retCode;
        //}

        public static DBHelper.DBReturnCode UpdateExchangeRate(Int64 ID, decimal INR)
        {
          CUT.DataLayer.GlobalDefault objGlobalDefaults = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];

            string Role = (objGlobalDefaults.RoleID).ToString();
            string UpdateBy = objGlobalDefaults.uid;


            //if (Role == "1")
            //{
            //    UpdateBy = objGlobalDefaults.ContactPerson;
            //}
            //if (Role == "2")
            //{
            //    UpdateBy = objGlobalDefaults.ContactPerson;
            //}
            //if (Role == "3")
            //{
            //    UpdateBy = objGlobalDefaults.ContactPerson;
            //}
            //if (Role == "4")
            //{
            //    UpdateBy = objGlobalDefaults.ContactPerson;
            //}
            string Currency = "";
            int rowsAffected = 0;
            decimal FCY = 1;
            string ApplicableDate = "";
            string MerchantID = "";
            bool Flag = true;
            string MerchantURL = "";
            SqlParameter[] sqlParams = new SqlParameter[10];
            sqlParams[0] = new SqlParameter("@ID", ID);
            sqlParams[1] = new SqlParameter("@Currency", Currency);
            sqlParams[2] = new SqlParameter("@FCY", FCY);
            sqlParams[3] = new SqlParameter("@INR", INR);
            sqlParams[4] = new SqlParameter("@ApplicableDate", ApplicableDate);
            sqlParams[5] = new SqlParameter("@MerchantID", MerchantID);
            sqlParams[6] = new SqlParameter("@Flag", Flag);
            sqlParams[7] = new SqlParameter("@MerchantURL", MerchantURL);
            sqlParams[8] = new SqlParameter("@UpdateBy", UpdateBy);
            sqlParams[9] = new SqlParameter("@LastUpdateDt", DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture));
            DBHelper.DBReturnCode retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExChangeRateAddUpdate", out rowsAffected, sqlParams);
            return retCode;
        }

        //public static DBHelper.DBReturnCode UpdateExchangeRate(List<Int64> ID, List<decimal> INR)
        //{
        // CUT.DataLayer.GlobalDefault objGlobalDefaults = (CUT.DataLayer.GlobalDefault)HttpContext.Current.Session["LoginUser"];

        //   string UpdateBy = (objGlobalDefaults.sid).ToString();
        //    string Currency = "";
        //    int rowsAffected = 0;
        //    decimal FCY = 1;
        //    string ApplicableDate = "";
        //    string MerchantID = "";
        //    bool Flag = true;
        //    string MerchantURL = "";
        //    DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.SUCCESS;


        //    SqlParameter[] sqlParams = new SqlParameter[9];
        //    for (int i = 0; i < ID.Count; i++)
        //    {
        //        sqlParams[0] = new SqlParameter("@ID", ID[i]);
        //        sqlParams[1] = new SqlParameter("@Currency", Currency);
        //        sqlParams[2] = new SqlParameter("@FCY", FCY);
        //        sqlParams[3] = new SqlParameter("@INR", INR[i]);
        //        sqlParams[4] = new SqlParameter("@ApplicableDate", ApplicableDate);
        //        sqlParams[5] = new SqlParameter("@MerchantID", MerchantID);
        //        sqlParams[6] = new SqlParameter("@Flag", Flag);
        //        sqlParams[7] = new SqlParameter("@MerchantURL", MerchantURL);
        //        sqlParams[8] = new SqlParameter("@UpdateBy", UpdateBy);

        //        retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExChangeRateAddUpdate", out rowsAffected, sqlParams);
        //    }

        //    return retCode;
        //}

        public static DBHelper.DBReturnCode GetForeignExchange(string ApplicableDate, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];

            //DateTime dt = Convert.ToDateTime(ApplicableDate);
            DateTime dt = DateTime.ParseExact(ApplicableDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            sqlParams[0] = new SqlParameter("@ApplicableDate", dt);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_ExChangeRateLoadByKey", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetForeignExchangeRate(string currency, out DataTable dtResult)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Currency", currency);
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_ExChangeRateLoadByRate", out dtResult, sqlParams);
            return retCode;
        }

        public static DBHelper.DBReturnCode GetCurrentForeignExchangeRate(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_ExChangeRateLoadByRecent", out dtResult);
            return retCode;
        }

        #endregion

        #region Exchange Log
        public static DBHelper.DBReturnCode GeExchangLog(out DataSet dsResult)
        {
          CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];

            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@sid", objGlobalDefault.sid);

            DBHelper.DBReturnCode retCode = DBHelper.GetDataSet("Proc_tbl_ExchangeRateLogLoadAll", out dsResult);
            DataRow[] row = null;

            return retCode;
        }
        public static DBHelper.DBReturnCode UpdateExchangeRate(Int64[] MarkupSid, Int64[] LogSid, string[] Currency, decimal[] ExchangeRate, decimal[] MarkupAmt, decimal[] MarkupPer)
        {
            int Rowseffected = 0;
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
          CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            string Update = DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);

            using (TransactionScope objTransactionScope = new TransactionScope())
            {
                for (int i = 0; i < MarkupAmt.Length; i++)
                {
                    SqlParameter[] sqlParams = new SqlParameter[4];
                    sqlParams[0] = new SqlParameter("@Amount", MarkupAmt[i]);
                    sqlParams[1] = new SqlParameter("@Percent", MarkupPer[i]);
                    sqlParams[2] = new SqlParameter("@UpdateBy", objGlobalDefault.uid);
                    sqlParams[3] = new SqlParameter("@sid", MarkupSid[i]);
                    retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExchangeMarkupUpdate", out Rowseffected, sqlParams);
                }
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    for (int i = 0; i < ExchangeRate.Length; i++)
                    {
                        decimal TotalAmt = Math.Round((ExchangeRate[i] + MarkupRate(ExchangeRate[i], MarkupAmt[i], MarkupPer[i])), 2, MidpointRounding.AwayFromZero);
                        SqlParameter[] sqlParams = new SqlParameter[6];
                        sqlParams[0] = new SqlParameter("@Currency", Currency[i]);
                        sqlParams[1] = new SqlParameter("@sid", LogSid[i]);
                        sqlParams[2] = new SqlParameter("@MarkupRate", MarkupRate(ExchangeRate[i], MarkupAmt[i], MarkupPer[i]));
                        sqlParams[3] = new SqlParameter("@TotalExchange", TotalAmt);
                        sqlParams[4] = new SqlParameter("@UpdateBy", objGlobalDefault.uid);
                        sqlParams[5] = new SqlParameter("@UpdateDate", Update);
                        retCode = DBHelper.ExecuteNonQuery("Proc_ExchangeRateLogUpdate", out Rowseffected, sqlParams);
                    }
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        for (int i = 0; i < ExchangeRate.Length; i++)
                        {
                            decimal TotalAmt = Math.Round((ExchangeRate[i] + MarkupRate(ExchangeRate[i], MarkupAmt[i], MarkupPer[i])), 2, MidpointRounding.AwayFromZero);
                            SqlParameter[] sqlParams = new SqlParameter[5];
                            sqlParams[0] = new SqlParameter("@Currency", Currency[i]);
                            sqlParams[1] = new SqlParameter("@INR", TotalAmt);
                            sqlParams[2] = new SqlParameter("@ApplicableDate", "");
                            sqlParams[3] = new SqlParameter("@UpdateBy", objGlobalDefault.uid);
                            sqlParams[4] = new SqlParameter("@LastUpdateDt", Update);
                            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExChangeRateAddUpdateByMarkupSet", out Rowseffected, sqlParams);
                        }
                        if (retCode == DBHelper.DBReturnCode.SUCCESS)
                            objTransactionScope.Complete();
                        else
                            objTransactionScope.Dispose();
                    }
                    else
                    {
                        objTransactionScope.Dispose();
                    }

                }
                else
                {
                    objTransactionScope.Dispose();
                }

            }
            return retCode;
        }
        public static void GetMarkupAmt(decimal[] ExchangeRate, decimal[] MarkupAmt, decimal[] MarkupPer, out decimal[] Amt, out decimal[] TotalExchange)
        {
            Amt = new decimal[ExchangeRate.Length]; TotalExchange = new decimal[ExchangeRate.Length];
            for (int i = 0; i < ExchangeRate.Length; i++)
            {
                Amt[i] = MarkupRate(ExchangeRate[i], MarkupAmt[i], MarkupPer[i]);
                TotalExchange[i] = Math.Round((ExchangeRate[i] + Amt[i]), 2, MidpointRounding.AwayFromZero);

            }
        }
        public static decimal MarkupRate(decimal Amt, decimal MarkupAmt, decimal MarkupPer)
        {
            decimal Rate = 0;
            decimal ActualAmt = MarkupAmt;
            decimal ActualPerAmt = (Amt * MarkupPer / 100);
            if (ActualAmt > ActualPerAmt)
                Rate = ActualAmt;
            else
                Rate = ActualPerAmt;
            return Rate;
        }
        #endregion

        #region GetOnline Rate
        public static CurrencyList[] arrCurrency { get; set; }
        public static IWebDriver driver { get; set; }
        public static string Path { get; set; }
        public static DBHelper.DBReturnCode GetExchangeRate()
        {
            try
            {
                List<string> ListSource = new List<string>();
                List<string> arrCurrency = new List<string>();
                using (var db = new Trivo_AmsHelper())
                {
                    string UID = AccountManager.GetSuperAdminID().ToString();
                    List<tbl_CurrencyCode> arrCodes = db.tbl_CurrencyCode.ToList();
                    ListSource = db.tbl_Admin.Select(d => d.Currency).Distinct().ToList();
                    List<String> arrParentID = db.tbl_Admin.Select(d => d.ID.ToString()).Distinct().ToList();
                    List<string> CurrencyID = (from obj in db.tbl_UserCurrency
                                              where obj.ParentID.ToString() == UID
                                              select obj.CurrencyID.ToString()).Distinct().ToList();
                    arrCurrency = (from obj in db.tbl_UserCurrency
                                   join
                                    objCurrency in db.tbl_CurrencyCode on obj.CurrencyID equals objCurrency.ID
                                   where obj.ParentID.ToString() == UID
                                   select objCurrency.CurrencyCode).Distinct().ToList();
                    string Currency = string.Empty;
                    String[] data = arrCurrency.ToArray();
                    Currency = string.Join(",", data.Select(o => string.Concat("", o, "")));
                    foreach (var Source in ListSource)
                    {
                        string sRespnse = "";
                        var Request = "https://apilayer.net/api/live?access_key=689286d565c2915fb6a403bdbbd8967a&currencies=" + Currency + "&source=" + Source + "&format=1";
                        bool bResponse = GetResponse(Request, out sRespnse);
                        if (bResponse)
                        {
                            var arrJson = JsonConvert.DeserializeObject<dynamic>(sRespnse);
                            List<tbl_CurrencyRate> arrRates = new List<tbl_CurrencyRate>();
                            var dynamicRates = arrJson.quotes;
                            foreach (var objCurrency in arrCurrency)
                            {
                                string Rate = dynamicRates[Source + objCurrency].ToString();
                                arrRates.Add(new tbl_CurrencyRate
                                {
                                    CurrencyID = objCurrency,
                                    SourceCurrency = Source,
                                    UpdateBy = AccountManager.LoginUserName,
                                    UpdateDate = DateTime.Now.ToString("dd-MM-yyyy hh:mm"),
                                    ExchangeRate = Convert.ToSingle(Rate),
                                });
                            }


                            var arrUpdateRate = (from obj in db.tbl_CurrencyCode
                            join objUserCurrency in db.tbl_UserCurrency on obj.ID equals objUserCurrency.CurrencyID
                            join objAdmin in db.tbl_Admin on objUserCurrency.ParentID equals objAdmin.ID
                            where arrCurrency.Contains(obj.CurrencyCode.ToString()) &&
                            objAdmin.Currency == Source && objUserCurrency.ParentID.ToString() == UID
                            select objUserCurrency).Distinct().ToList();
                            foreach (var objRate in arrUpdateRate)
                            {
                                string Code = arrCodes.Where(d => d.ID == objRate.CurrencyID).FirstOrDefault().CurrencyCode;
                                var arrUpdate = arrRates.Where(d => d.CurrencyID == Code).FirstOrDefault();
                                if (arrUpdate != null)
                                {
                                    objRate.Rate = Convert.ToSingle(arrUpdate.ExchangeRate);
                                    objRate.UpdateDate = arrUpdate.UpdateDate;
                                    objRate.UpdateBy = AccountManager.LoginUserName;
                                }

                            }
                            db.tbl_CurrencyRate.AddRange(arrRates);
                            db.SaveChanges();
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return DBHelper.DBReturnCode.SUCCESS;
        }

        public static bool GetResponse(string Request, out string Response)
        {
            bool response = false;
            Response = string.Empty;
            try
            {
                byte[] data = Encoding.UTF8.GetBytes(Request);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Request);
                request.Timeout = 300000;
                request.Method = "POST";
                request.ContentType = "application/json";
                //request.Headers.Add("Accept-Encoding", "gzip");
                Stream dataStream = request.GetRequestStream();
                //dataStream.Write(data, 0, data.Length);
                //dataStream.Close();
                WebResponse webResponse = request.GetResponse();
                var rsp = webResponse.GetResponseStream();
                if (rsp == null)
                {
                    //throw exception
                }
                using (StreamReader readStream = new StreamReader(rsp))
                {
                    Response = readStream.ReadToEnd();
                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return response;
        }
        public static ExchaneList GetExchaneList(string ExchangeCurrency, string Preferred, decimal MarkupAmt, decimal MarkupPerentage)
        {
            ExchaneList objExchaneList = new ExchaneList();

            string Url = "http://www.xe.com/currencyconverter/convert/?Amount=1&From=" + ExchangeCurrency + "&To=" + Preferred;
            driver.Navigate().GoToUrl(Url);
            while (driver.Url != Url) { }
            IWebElement Status = driver.FindElement(By.ClassName("uccResultAmount"));
            objExchaneList.ExchaneRate = Convert.ToDecimal(Status.Text);
            objExchaneList.Currency = ExchangeCurrency;
            objExchaneList.Preferred = Preferred;
            objExchaneList.MarkupRate = MarkupRate(objExchaneList.ExchaneRate, MarkupAmt, MarkupPerentage);
            return objExchaneList;

        }
        public static DBHelper.DBReturnCode UpdateExchangeRate()
        {
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            string Update = DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
            foreach (CurrencyList objCurrencyList in arrCurrency)
            {
                int Rowseffected = 0;
                decimal TotalAmt = Math.Round((objCurrencyList.ExchaneList.ExchaneRate + objCurrencyList.ExchaneList.MarkupRate), 2, MidpointRounding.AwayFromZero);
                if (TotalAmt != 0)
                {
                    SqlParameter[] sqlParams = new SqlParameter[7];
                    sqlParams[0] = new SqlParameter("@Currency", objCurrencyList.ExchageCurrency);
                    sqlParams[1] = new SqlParameter("@Preferred", objCurrencyList.PreferredCurrency);
                    sqlParams[2] = new SqlParameter("@ExchangeRate", objCurrencyList.ExchaneList.ExchaneRate);
                    sqlParams[3] = new SqlParameter("@MarkupRate", objCurrencyList.ExchaneList.MarkupRate);
                    sqlParams[4] = new SqlParameter("@TotalExchange", TotalAmt);
                    sqlParams[5] = new SqlParameter("@UpdateBy", "System");
                    sqlParams[6] = new SqlParameter("@UpdateDate", Update);
                    retCode = DBHelper.ExecuteNonQuery("Proc_ExchangeRateLogAdd", out Rowseffected, sqlParams);
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        SqlParameter[] sqlParams1 = new SqlParameter[5];
                        sqlParams1[0] = new SqlParameter("@Currency", objCurrencyList.ExchageCurrency);
                        sqlParams1[1] = new SqlParameter("@INR", TotalAmt);
                        sqlParams1[2] = new SqlParameter("@ApplicableDate", "");
                        sqlParams1[3] = new SqlParameter("@UpdateBy", "System");
                        sqlParams1[4] = new SqlParameter("@LastUpdateDt", Update);
                        retCode = DBHelper.ExecuteNonQuery("Proc_tbl_ExChangeRateAddUpdateByMarkupSet", out Rowseffected, sqlParams1);
                    }
                }

            }
            return retCode;

        }
        #endregion

        //from Main datalayer ... Azhar
        public static float GetExchange()
        {
            float ExchangeRate = 0;
          CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            string PreferedCurrency = objGlobalDefault.Currency;
            CUT.DataLayer.AgentDetailsOnAdmin objAgentDetailsOnAdmin = new CUT.DataLayer.AgentDetailsOnAdmin();
            if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
            {
                if (objAgentDetailsOnAdmin.Currency == "INR")
                {
                    ExchangeRate = 1;
                }
                else
                {
                    ExchangeRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == objAgentDetailsOnAdmin.Currency).Rate;
                }
            }
            else
            {
                if (PreferedCurrency == "INR")
                {
                    ExchangeRate = 1;
                }
                else
                {
                    ExchangeRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == PreferedCurrency).Rate;
                }
            }
            return ExchangeRate;
        }

        public static float GetExchange(HttpContext Context)
        {
            float ExchangeRate = 0;
          CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)Context.Session["LoginUser"];
            string PreferedCurrency = objGlobalDefault.Currency;
            CUT.DataLayer.AgentDetailsOnAdmin objAgentDetailsOnAdmin = new CUT.DataLayer.AgentDetailsOnAdmin();
            if (Context.Session["AgentDetailsOnAdmin"] != null)
            {
                if (objAgentDetailsOnAdmin.Currency == "INR")
                {
                    ExchangeRate = 1;
                }
                else
                {
                    ExchangeRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == objAgentDetailsOnAdmin.Currency).Rate;
                }
            }
            else
            {
                if (PreferedCurrency == "INR")
                {
                    ExchangeRate = 1;
                }
                else
                {
                    ExchangeRate = (float)objGlobalDefault.ExchangeRate.Single(CurrencyType => CurrencyType.Currency == PreferedCurrency).Rate;
                }
            }
            return ExchangeRate;
        }

        public static string GetCurentCurrency(HttpContext Context)
        {
          CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)Context.Session["LoginUser"];
            string CurrencyClass = objGlobalDefault.Currency;
            CUT.DataLayer.AgentDetailsOnAdmin objAgentDetailsOnAdmin = new CUT.DataLayer.AgentDetailsOnAdmin();
            if (Context.Session["AgentDetailsOnAdmin"] != null)
            {
                objAgentDetailsOnAdmin = (CUT.DataLayer.AgentDetailsOnAdmin)Context.Session["AgentDetailsOnAdmin"];
                CurrencyClass = objAgentDetailsOnAdmin.Currency;
            }
            return CurrencyClass;
        }
        public static string GetCurentCurrency()
        {
          CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            string CurrencyClass = objGlobalDefault.Currency;
            CUT.DataLayer.AgentDetailsOnAdmin objAgentDetailsOnAdmin = new CUT.DataLayer.AgentDetailsOnAdmin();
            if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
            {
                objAgentDetailsOnAdmin = (CUT.DataLayer.AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                CurrencyClass = objAgentDetailsOnAdmin.Currency;
            }
            return CurrencyClass;
        }

        public static List<Common.Exchange> GetExchangeByAdmin()
        {
            List<Common.Exchange> Exchange = new List<Common.Exchange>();
            try
            {
               
                using (var db = new Trivo_AmsHelper())
                {
                    string UID = AccountManager.GetSuperAdminID().ToString();
                 IQueryable<Common.Exchange>  arrData = (from objUser in db.tbl_UserCurrency
                                    join 
                                    objCurrency in db.tbl_CurrencyCode on objUser.CurrencyID equals objCurrency.ID
                                    where objUser.ParentID.ToString() == UID
                                                         select new  Common.Exchange
                                    {
                                      ID =   objUser.ID,
                                      Rate=   objUser.Rate,
                                      Markup=  objUser.Markup,
                                      TotalExchange = objUser.Rate + objUser.Markup,
                                      UpdateDate=  objUser.UpdateDate,
                                      UpdateBy = objUser.UpdateBy,
                                      CurrencyID = objUser.CurrencyID,
                                      CurrencyCode=  objCurrency.CurrencyCode,
                                      CurrencyDetails=  objCurrency.CurrencyDetails,
                                    });
                    if (arrData.ToList().Count != 0)
                        Exchange = arrData.ToList().OrderBy(d => d.CurrencyCode).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return Exchange;
        }
    }
}