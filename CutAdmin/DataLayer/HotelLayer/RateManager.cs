﻿using CutAdmin.EntityModal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CutAdmin.DataLayer.HotelLayer
{
    public class RateManager
    {
        public long HotelCode { get; set; }
        public long RatePlan { get; set; }
        public long TarrifID { get; set; }
        public long DateID { get; set; }
        public string RoomId { get; set; }
        public string HotelRateID { get; set; }
        public string ValidNationality { get; set; }
        public string Checkin { get; set; }
        public DateTime dFromDate { get; set; }
        public string Checkout { get; set; }
        public DateTime dToDate { get; set; }
        public string CurrencyCode { get; set; }
        public decimal RR { get; set; }
        public string EB { get; set; }
        public string CWB { get; set; }
        public string CNB { get; set; }
        public string RateType { get; set; }
        public long? MealPlan_ID { get; set; }
        public string MealPlan { get; set; }
        public string Supplier { get; set; }
        public List<DayRate> arrDayRate { get; set; }
        public string DayWise { get; set; }
        public string MonRR { get; set; }
        public string TueRR { get; set; }
        public string WedRR { get; set; }
        public string ThuRR { get; set; }
        public string FriRR { get; set; }
        public string SatRR { get; set; }
        public string SunRR { get; set; }
        public long? MinStay { get; set; }
        public long? MaxStay { get; set; }
        public long? MinRoom { get; set; }
        public long? MaxRoom { get; set; }

        public string RoomType { get; set; }

        public static List<RateManager> GetRatePlan(string HotelCode)
         {
            List<RateManager> arrRates = new List<RateManager>();
            try
            {
                var AdminID = AccountManager.GetSuperAdminID();
                using (var DB = new Click_Hotel())
                {
                    IQueryable<RateManager> RateList1 = (from obj in DB.tbl_RatePlan
                                                         join objTarrif in DB.tbl_TarrifPlan on obj.PlanID.ToString() equals objTarrif.nPlanID.ToString()
                                                         join objMeals in DB.tbl_MealPlan on objTarrif.nMeal_PlanID.ToString() equals objMeals.ID.ToString()
                                                         where obj.HotelID.ToString() == HotelCode && objTarrif.sRate_Type == "GN" && obj.ParentID == AdminID
                                                         select new RateManager
                                                         {
                                                             ValidNationality = obj.Market,
                                                             Checkin = objTarrif.sFromDate,
                                                             //dFromDate = DateTime.ParseExact(objTarrif.sFromDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                                             Checkout = objTarrif.sToDate,
                                                             //dToDate = DateTime.ParseExact(objTarrif.sToDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                                             CurrencyCode = obj.sCurrency,
                                                             MealPlan_ID = objMeals.ID,
                                                             RateType = obj.RateType,
                                                             MealPlan = objMeals.Meal_Plan,
                                                             MinStay = objTarrif.nMinStay,
                                                             MaxStay = objTarrif.nMaxStay,
                                                             MinRoom = objTarrif.nMinRoom,
                                                             MaxRoom = objTarrif.nMinRoom,
                                                             RatePlan = obj.PlanID,
                                                             HotelCode = obj.HotelID
                                                         });
                    var RateList = new List<RateManager>();
                    if (RateList1.ToList().Count != 0)
                    {
                        RateList = RateList1.ToList();
                        RateList.ForEach(s => s.dFromDate = DateTime.ParseExact(s.Checkin, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                        RateList.ForEach(s => s.dToDate = DateTime.ParseExact(s.Checkout, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                        RateList = RateList.OrderBy(s => s.dFromDate).Distinct().ToList();
                    }
                  
                    foreach (var item in RateList)
                    {
                        if (!arrRates.Exists(d =>( d.dFromDate>=item.dFromDate || d.dToDate <= item.dToDate) && d.MealPlan == item.MealPlan && d.MinStay == item.MinStay && d.MaxStay == item.MaxStay && d.MinRoom == item.MinRoom))
                        {
                            DateTime minFromDate = RateList.Where(d => d.MealPlan == item.MealPlan && d.MinStay == item.MinStay && d.MaxStay == item.MaxStay && d.MinRoom == item.MinRoom).Select(d => d.dFromDate).ToList().Min();
                            DateTime maxToDate = RateList.Where(d => d.MealPlan == item.MealPlan && d.MinStay == item.MinStay && d.MaxStay == item.MaxStay && d.MinRoom == item.MinRoom).Select(d => d.dToDate).ToList().Max();
                            arrRates.Add(new RateManager
                            {
                                ValidNationality = item.ValidNationality,
                                Checkin = minFromDate.ToString("dd-MM-yyyy"),
                                dFromDate = item.dFromDate,
                                Checkout = maxToDate.ToString("dd-MM-yyyy"),
                                dToDate = item.dToDate,
                                CurrencyCode = item.CurrencyCode,
                                RateType = item.RateType,
                                MealPlan_ID= item.MealPlan_ID,
                                MealPlan = item.MealPlan,
                                RatePlan = item.RatePlan,
                                MinStay = Convert.ToInt64(item.MinStay),
                                MaxStay = Convert.ToInt64(item.MaxStay),
                                MinRoom = Convert.ToInt64(item.MinRoom),
                                MaxRoom = Convert.ToInt64(item.MaxRoom),
                                HotelCode = Convert.ToInt64(item.HotelCode)
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return arrRates;
        }

        public static List<RateManager> GetTarrifRates(RateManager arrRatePlan)
        {
            List<RateManager> arrTarrif = new List<RateManager>();
            try
            {
                DateTime Checkin = DateTime.ParseExact(arrRatePlan.Checkin, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime CheckOut = DateTime.ParseExact(arrRatePlan.Checkout, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                double noDays = (CheckOut - Checkin).TotalDays;
                using (var DB = new Click_Hotel())
                {
                    List<RateManager> arrTarrifPlan = new List<RateManager>();
                    IQueryable<RateManager> ITarrifPlan = (from obj in DB.tbl_TarrifPlan
                                                           join objMeal in DB.tbl_MealPlan on obj.nMeal_PlanID.ToString() equals objMeal.ID.ToString()
                                                           join objRoom in DB.tbl_commonRoomDetails on obj.RoomTypeID.ToString() equals objRoom.RoomTypeId.ToString()
                                                           join objRoomDetails in DB.tbl_commonRoomType on objRoom.RoomTypeId.ToString() equals objRoomDetails.RoomTypeID.ToString()
                                                           where obj.nPlanID.ToString() == arrRatePlan.RatePlan.ToString() && objMeal.Meal_Plan== arrRatePlan.MealPlan

                                                             select new RateManager {
                        ValidNationality = arrRatePlan.ValidNationality,
                        Checkin = obj.sFromDate,
                        Checkout = obj.sToDate,
                        CurrencyCode = arrRatePlan.CurrencyCode,
                        RateType = arrRatePlan.RateType,
                        MealPlan_ID = arrRatePlan.MealPlan_ID,
                        MealPlan = arrRatePlan.MealPlan,
                        RatePlan = arrRatePlan.RatePlan,
                        RoomId =   obj.RoomTypeID.ToString(),
                        TarrifID= obj.nRateID,
                        RoomType = objRoomDetails.RoomType
                    });
                    if(ITarrifPlan.ToList().Count != 0)
                    {
                        arrTarrifPlan = ITarrifPlan.Distinct().ToList();
                        arrTarrifPlan.ToList().ForEach(d=>d.MinStay = Convert.ToInt64(arrRatePlan.MinStay));
                        arrTarrifPlan.ToList().ForEach(d=>d.MaxStay = Convert.ToInt64(arrRatePlan.MaxStay));
                        arrTarrifPlan.ToList().ForEach(d=>d.MinRoom = Convert.ToInt64(arrRatePlan.MinRoom));
                        arrTarrifPlan.ToList().ForEach(d => d.MaxRoom = Convert.ToInt64(arrRatePlan.MaxRoom));
                        arrTarrifPlan.ToList().ForEach(d=>d.HotelCode = Convert.ToInt64(arrRatePlan.HotelCode));
                        arrTarrifPlan.ToList().ForEach(d => d.dFromDate = DateTime.ParseExact(d.Checkin, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                        arrTarrifPlan.ToList().ForEach(d => d.dToDate = DateTime.ParseExact(d.Checkout, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture));
                    }
                    foreach (var objTarrif in arrTarrifPlan.ToList())
                    {
                        decimal EBRate = 0, CWBRate = 0, CNBRate = 0;
                        arrTarrif.Add(objTarrif);
                        arrTarrif.LastOrDefault().RR = RoomRateManager.GetTarrifRate(objTarrif.dFromDate, 
                            objTarrif.dToDate, arrRatePlan.RatePlan, 
                            Convert.ToInt64(objTarrif.RoomId), 
                            Convert.ToInt64(arrRatePlan.MealPlan_ID),out EBRate,out CWBRate,out CNBRate);
                        arrTarrif.LastOrDefault().EB = EBRate.ToString();
                        arrTarrif.LastOrDefault().CNB = CNBRate.ToString();
                        arrTarrif.LastOrDefault().CWB = CWBRate.ToString();
                        arrTarrif.LastOrDefault().DayWise = "NO";
                        arrTarrif.LastOrDefault().arrDayRate = RoomRateManager.GetDaysRate(arrTarrif.LastOrDefault().TarrifID);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return arrTarrif;
        }


    }
}