﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.EntityModal;
using System.Globalization;

namespace CutAdmin.DataLayer.HotelLayer
{
    public class OfferManager
    {
        public static void SaveOffer(List<tbl_CommonHotelOffer> arrOffers, List<tbl_RoomOfferRate> NewRates, long HotelCode , List<long> arrRoomID)
        {
            try
            {
                using (var db = new Click_Hotel())
                {
                    Random generator = new Random();
                    String OfferId = generator.Next(0, 999).ToString("D3");
                    arrOffers.ForEach(d => d.OfferID = Convert.ToInt32(OfferId));
                    db.tbl_CommonHotelOffer.AddRange(arrOffers);
                    db.SaveChanges();

                    /*Genrate Offer Dates*/
                    List<DateTime> arrOfferDate = new List<DateTime>();
                    List<string> arrsFrom = arrOffers.Where(d => d.DateType == "S").ToList().Select(F => F.ValidFrom).ToList();
                    List<string> sTo = arrOffers.Where(d => d.DateType == "S").ToList().Select(F => F.ValidTo).ToList();
                    foreach (var sFrom in arrsFrom)
                    {
                        DateTime dtFrom = DateTime.ParseExact(sFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        Common.Common.StartDate = dtFrom;
                        DateTime dtToDate = DateTime.ParseExact(sTo[arrsFrom.IndexOf(sFrom)], "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        Common.Common.EndDate = dtToDate;
                        List<DateTime> arrDates = Common.Common.Calander;
                        foreach (var Date in arrDates)
                        {
                            if(!arrOfferDate.Contains(Date))
                            {
                                arrOfferDate.Add(Date);
                            }
                        }
                    }
                    /*Assign Offer to Rate*/
                    RoomRateManager.AssignOffer(OfferId, arrOfferDate, HotelCode, arrRoomID);
                    List<tbl_RoomOfferRate> arrRates = new List<tbl_RoomOfferRate>(); ;
                    foreach (var offer in arrOffers.Where(d=>d.SeasonName != "Block Date"))
                    {
                        foreach (var Rate in NewRates)
                        {
                            Rate.OfferID = offer.OfferID;
                            arrRates.Add(Rate);
                        }
                    }
                    db.tbl_RoomOfferRate.AddRange(arrRates);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
              
            }
        }

        public static List<tbl_CommonHotelOffer> GetOfferByDay(long? PlanID,DateTime sRateDate)
        {
            List<tbl_CommonHotelOffer> arrOffer = new List<tbl_CommonHotelOffer>();
            try
            {
                using (var db = new Click_Hotel())
                {
                    IQueryable<tbl_TarrifPlan> IQueryableData = (from obj in db.tbl_TarrifPlan
                                                                 where obj.nPlanID == PlanID && obj.sOffer !=""
                                                                 select obj);
                    foreach (var tarrif in IQueryableData.ToList())
                    {
                       DateTime  dtFrom = DateTime.ParseExact(tarrif.sFromDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                       Common.Common.StartDate = dtFrom;
                       DateTime dtToDate = DateTime.ParseExact(tarrif.sToDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                       Common.Common.EndDate = dtToDate;
                       List<DateTime> arrDates = Common.Common.Calander;
                       if (arrDates.Contains(sRateDate))
                       {
                            List<string> arrOfferCode = tarrif.sOffer.Split(',').ToList();
                            IQueryable<tbl_CommonHotelOffer> IQueryableOffer =
                                     (from obj in db.tbl_CommonHotelOffer
                                      where arrOfferCode.Contains(obj.OfferID.ToString()) select obj);
                            foreach (var offer in IQueryableOffer.ToList())
                            {
                                arrOffer.Add(offer);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return arrOffer.Distinct().ToList();
        }
    }
}