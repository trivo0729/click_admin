﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CommonLib.Response;
using CutAdmin.Common;
using CutAdmin.EntityModal;
namespace CutAdmin.DataLayer
{
    public class GenralManager
    {
        public static CUT_LIVE_UATSTEntities db {get { return new CUT_LIVE_UATSTEntities();}}

        public static Click_Hotel db_Hotel { get { return new Click_Hotel(); } }

        public static CommonHotelDetails GetHotelInfo(string Search)
        {
            CommonHotelDetails objHotelInfo = new CommonHotelDetails();
            try
            {
                objHotelInfo = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Search];
            }
            catch (Exception)
            {
                
                throw;
            }
            return objHotelInfo;
        }

        public static RateGroup GetRateInfo(string Search,out string HotelID)
        {
            RateGroup objRates = new RateGroup();
            HotelID = "";
            CommonHotelDetails objHotelInfo = new CommonHotelDetails();
            try
            {
                objHotelInfo = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Search];
                HotelID = objHotelInfo.HotelId; 
                objRates = objHotelInfo.List_RateGroup[0];
            }
            catch (Exception)
            {

                throw;
            }
            return objRates;
        }

        public static RateGroup GetRateInfo(string Search,string Supplier,string HotelID)
        {
            RateGroup objRates = new RateGroup();
            CommonHotelDetails objHotelInfo = new CommonHotelDetails();
            try
            {
                HotelFilter objHotel = (HotelFilter)HttpContext.Current.Session["ModelHotel" + Search];
                objHotelInfo = objHotel.arrHotels.Where(d => d.HotelId == HotelID.ToString()).FirstOrDefault();
                objRates = objHotelInfo.List_RateGroup.Where(d=>d.Name==Supplier).FirstOrDefault();
            }
            catch (Exception)
            {

                throw;
            }
            return objRates;
        }

        public static List<HotelOccupancy> GetOccupancyInfo(string Search)
        {
            List<HotelOccupancy> arrOccupancy = new List<HotelOccupancy>();
            RateGroup objRates = new RateGroup();
            CommonHotelDetails objHotelInfo = new CommonHotelDetails();
            try
            {
                objHotelInfo = (CommonHotelDetails)HttpContext.Current.Session["AvailDetails" + Search];
                arrOccupancy = objHotelInfo.List_RateGroup[0].RoomOccupancy;
            }
            catch (Exception)
            {

                throw;
            }
            return arrOccupancy;
        }

        #region Supplier List
        public static List<tbl_APIDetails> GetSupplier(string Service)
        {
            List<tbl_APIDetails> arrSupplier = new List<tbl_APIDetails>();
            try
            {
                Int64 Uid = AccountManager.GetAdminByLogin();
                List<tbl_APIDetails> arrResult = (from obj in db.tbl_APIDetails
                                                  where (Service == "All" ||
                                                  ((Service == "Hotel" && obj.Hotel == true)
                                                  || (Service == "Flight" && obj.Flight == true)
                                                  || (Service == "Packages" && obj.Packages == true)
                                                  || (Service == "Activity" && obj.Activity == true)
                                                  )) && obj.ParentID == Uid
                                                  select obj).ToList();
                arrSupplier = arrResult.ToList();
            }
            catch (Exception ex)
            {
            }
            return arrSupplier;
        }
        #endregion

        #region Meals Details

        public static List<CutAdmin.EntityModal.Comm_AddOns> GetMeals()
        {
            Int64 Uid = AccountManager.GetSuperAdminID();
            List<CutAdmin.EntityModal.Comm_AddOns> arrMeals = new List<CutAdmin.EntityModal.Comm_AddOns>();
            try
            {
                arrMeals = (from obj in db_Hotel.Comm_AddOns
                                 where obj.UserId == Uid
                                 select obj).ToList();
            }
            catch (Exception ex)
            {
               
            }
            return arrMeals;
        }

        public static object GetMealPlans()
        {
            object arrMealPlan = new List<object>();
            try
            {
                Int64 Uid = AccountManager.GetAdminByLogin();
                using (var db_Hotel = new Click_Hotel())
                {
                    arrMealPlan = (from obj in db_Hotel.tbl_MealPlan 
                                   where obj.ParentID == Uid
                                   select new
                                   {
                                       obj.ID,
                                       obj.Meal_Plan,
                                       arrMealID= (from objMealMap in db_Hotel.tbl_MealPlanMapping where objMealMap.PlanID == obj.ID
                                        select objMealMap.MealID).ToList() 
                                   }).ToList();
                }
               
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
            }
            return arrMealPlan;
        }

        public static void SaveMealPlan(tbl_MealPlan arrMealPlan,List<tbl_MealPlanMapping> arrMeals)
        {
            try
            {
                using (var db_Hotel = new Click_Hotel())
                {
                    using (var Transaction = db_Hotel.Database.BeginTransaction())
                    {
                        if(arrMealPlan.ID ==0 )
                        {
                            arrMealPlan.ParentID = AccountManager.GetAdminByLogin();
                            db_Hotel.tbl_MealPlan.Add(arrMealPlan);
                            db_Hotel.SaveChanges();
                            arrMeals.ForEach(d => d.PlanID = arrMealPlan.ID);
                            db_Hotel.tbl_MealPlanMapping.AddRange(arrMeals);
                            db_Hotel.SaveChanges();
                        }
                        else
                        {
                            var arrPlans = db_Hotel.tbl_MealPlan.Where(d => d.ID == arrMealPlan.ID).FirstOrDefault();
                            arrPlans.Meal_Plan = arrMealPlan.Meal_Plan;
                            var arrMealsMaped = db_Hotel.tbl_MealPlanMapping.Where(d => d.PlanID == arrMealPlan.ID).ToList();
                            db_Hotel.tbl_MealPlanMapping.RemoveRange(arrMealsMaped);
                            db_Hotel.tbl_MealPlanMapping.AddRange(arrMeals);
                            db_Hotel.SaveChanges();
                        }
                        
                        Transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void DeletePlan(Int64 PlanID)
        {
            try
            {
                using (var db_Hotel = new Click_Hotel())
                {
                    using (var Transaction = db_Hotel.Database.BeginTransaction())
                    {
                        var arrPlan = db_Hotel.tbl_MealPlan.Where(d => d.ID == PlanID).FirstOrDefault();
                        db_Hotel.tbl_MealPlan.Remove(arrPlan);
                        db_Hotel.SaveChanges();
                        Transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
               
            }
        }
        #endregion

        #region Rate Type
        public static List<tbl_RateType> GetRateType(string HotelAdmin)
        {
            List<tbl_RateType> arrRateType = new List<tbl_RateType>();
            try
            {
                using (var db = new Click_Hotel())
                {
                    IQueryable<tbl_RateType> arrData = (from obj in db.tbl_RateType where obj.ParentID.ToString() == HotelAdmin select obj);
                    if (arrData.ToList().Count == 0)
                        throw new Exception("No Ratetypes.");
                    arrRateType = arrData.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return arrRateType;
        }

        public static void SaveRateType(tbl_RateType arrRateType)
        {
            try
            {
                using (var db = new Click_Hotel())
                {
                    if(db.tbl_RateType.Where(d=>d.RateType==  arrRateType.RateType && d.ParentID == arrRateType.ParentID).FirstOrDefault() != null )
                    {
                        var arrType = db.tbl_RateType.Where(d => d.RateType == arrRateType.RateType && d.ParentID == arrRateType.ParentID).FirstOrDefault();
                        arrType.InventoryType = arrRateType.InventoryType.TrimEnd(',');
                    }
                    else
                       db.tbl_RateType.Add(arrRateType);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}