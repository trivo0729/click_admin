﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;

namespace CutAdmin.DataLayer
{
    public class SeleniumManager
    {
        public static void SubmitQuery(RemoteWebDriver Driver, string App_No, string Sevc_Type, string vname, string v_gender, string p_nationality, string vdob)
        {
            try
            {
                Driver.ExecuteScript(SubmitQuery(App_No, Sevc_Type, vname, v_gender, p_nationality, vdob));

            }
            catch
            { }

        }
        public static string SubmitQuery(string App_No, string Sevc_Type, string vname, string v_gender, string p_nationality, string vdob)
        {
            string Query = "";
            Query += "document.pform.p_qry_no.value = '" + App_No + "';document.pform.p_qry_type.value ='" + Sevc_Type + "';document.pform.p_firstname.value = '" + vname + "';";
            Query += "document.pform.p_gender.value = '" + v_gender + "';document.pform.p_nationality.value = '" + p_nationality + "';document.pform.p_dob.value = '" + vdob + "'";
            return Query;
        }


        static public bool verify(IWebDriver driver, string elementName)
        {
            try
            {
                bool isElementDisplayed = driver.FindElement(By.XPath(elementName)).Displayed;
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}