﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CutAdmin.BL;
using CutAdmin.DataLayer;

namespace CutAdmin.DataLayer
{
    public class RoleManager
    {
        public static DBHelper.DBReturnCode GetAgentRoleList(string AgencyName, out DataTable dtResult)
        {
            //SqlParameter[] sqlParams = new SqlParameter[1];
            //sqlParams[0] = new SqlParameter("@AgencyName", AgencyName);
            //DBHelper.DBReturnCode retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadAllByAgencyName", out dtResult, sqlParams);
            //return retCode;
            dtResult = new DataTable();
            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            GlobalDefault objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
            if (objGlobalDefault.UserType == "Admin")
            {
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@AgencyName", AgencyName);
                retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadAllByAgencyName", out dtResult, sqlParams);
            }
            else if (objGlobalDefault.UserType == "Franchisee")
            {
                SqlParameter[] sqlParams = new SqlParameter[2];
                string Franchisee = objGlobalDefault.Franchisee;
                sqlParams[0] = new SqlParameter("@FranchiseeId", Franchisee);
                sqlParams[1] = new SqlParameter("@AgencyName", AgencyName);
                retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadAllByAgencyNameForFranchisee", out dtResult, sqlParams);
            }
            else if (objGlobalDefault.UserType == "AdminStaff")
            {
                Int64 uid = objGlobalDefault.sid;
                SqlParameter[] sqlParams = new SqlParameter[2];
                string Franchisee = objGlobalDefault.Franchisee;
                sqlParams[0] = new SqlParameter("@uid", uid);
                sqlParams[1] = new SqlParameter("@AgencyName", AgencyName);
                retCode = DBHelper.GetDataTable("Proc_tbl_AdminLoginLoadAllByAgencyNameForAdminStaff", out dtResult, sqlParams);
            }
            return retCode;
        }
    }
}