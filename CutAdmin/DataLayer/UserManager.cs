﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.dbml;
using CutAdmin.Flightdbml;
using CutAdmin.Models;

namespace CutAdmin.DataLayer
{
    public class UserManager
    {
                
        #region Staff Details
        public static List<tbl_StaffLogin> GetStaff(out int TotalCount)
        {
            var result = new List<tbl_StaffLogin>();
            string search = DTResult.sSearch;
            string sortOrder = DTResult.sSortDir_0;
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;
            using (var db = new helperDataContext())
            {
                result = db.tbl_StaffLogins.Where(p => (search == null
                || (p.ContactPerson != null && p.ContactPerson.ToLower().Contains(search.ToLower())
                || p.uid != null && p.uid.ToLower().Contains(search.ToLower())
                || p.StaffUniqueCode != null && p.StaffUniqueCode.ToString().ToLower().Contains(search.ToLower())
                || p.LoginFlag != null && p.LoginFlag.ToString().ToLower().Contains(search.ToLower())
                )) && p.ParentId == AccountManager.GetAdminByLogin() && (p.UserType == "AdminStaff" || p.UserType == "FranchiseeStaff")).ToList();
                TotalCount = result.Count;
                result = result.Skip(start-1).Take(length).ToList();
                int Sorting = DTResult.iSortingCols;
                if (Sorting == 1)
                {
                    string sortColumns = DTResult.iSortCol_0;
                    sortOrder = DTResult.sSortDir_0;
                    switch (sortColumns)
                    {
                        case "0":
                            if (sortOrder == "asc")
                                result = result.OrderBy(a => a.ContactPerson).ToList();
                            else
                                result = result.OrderByDescending(a => a.ContactPerson).ToList();
                            break;
                        case "1":
                            if (sortOrder == "asc")
                                result = result.OrderBy(a => a.uid).ToList();
                            else
                                result = result.OrderByDescending(a => a.uid).ToList();
                            break;
                        case "2":
                            if (sortOrder == "asc")
                                result = result.OrderBy(a => a.StaffUniqueCode).ToList();
                            else
                                result = result.OrderByDescending(a => a.StaffUniqueCode).ToList();
                            break;
                        case "3":
                            if (sortOrder == "asc")
                                result = result.OrderBy(a => a.LoginFlag).ToList();
                            else
                                result = result.OrderByDescending(a => a.LoginFlag).ToList();
                            break;
                    }
                }

                return result.ToList();
            }

        }


        #endregion

        #region Agent Details
        public class AgencyDetails
        {
            public Int64 sid { get; set; }
            public string AgencyName { get; set; }
            public string Agentuniquecode { get; set; }
            public string uid { get; set; }
            public string password { get; set; }
            public Decimal AvailableCredit { get; set; }
            public string GroupName { get; set; }
            public string Description { get; set; }
            public string Country { get; set; }
            public string ContactPerson { get; set; }
            public string Mobile { get; set; }
            public DateTime dtLastAccess { get; set; }
            public bool EnableCheckAccount { get; set; }
            public string LoginFlag { get; set; }
            public string CurrencyCode { get; set; }
            public Int64 ParentID { get; set; }
            public string agentCategory { get; set; }
            public string UserType { get; set; }
        }
        public static List<AgencyDetails> GetAgents(out int TotalCount)
        {
            TotalCount = 0;
            var arrData = DTResult.arrTable;
            string search = DTResult.sSearch;
            
            
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;
            using (var db = new helperDataContext())
            {
               IQueryable<AgencyDetails> result =  from obj in db.tbl_AdminLogins
                                                   from objContact in db.tbl_Contacts
                                                   from objCredit in db.tbl_AdminCreditLimits
                                                   where obj.ContactID == objContact.ContactID &&
                                                   obj.sid == objCredit.uid && (obj.UserType == "Agent" || obj.UserType == "Franchisee")
                                                   where (search == null
                                                   || (obj.ContactPerson != null && obj.ContactPerson.ToLower().Contains(search.ToLower())
                                                   || obj.uid != null && obj.uid.ToLower().Contains(search.ToLower())
                                                   || obj.Agentuniquecode != null && obj.Agentuniquecode.ToString().ToLower().Contains(search.ToLower())
                                                   || obj.LoginFlag != null && obj.LoginFlag.ToString().ToLower().Contains(search.ToLower())
                                                   || objCredit.AvailableCredit != null && objCredit.AvailableCredit.ToString().ToLower().Contains(search.ToLower())
                                                   )) && obj.ParentID == AccountManager.GetAdminByLogin()
                                                   select new AgencyDetails
                                                   {
                                                       sid = obj.sid,
                                                       AgencyName = obj.AgencyName,
                                                       Agentuniquecode = obj.Agentuniquecode,
                                                       uid = obj.uid,
                                                       password = obj.password,
                                                       AvailableCredit = Convert.ToDecimal(objCredit.AvailableCredit),
                                                       GroupName = "Group A",
                                                       Description = (from objCity in db.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Description,
                                                       Country = (from objCity in db.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Countryname,
                                                       ContactPerson = obj.ContactPerson,
                                                       Mobile = objContact.Mobile,
                                                       dtLastAccess = Convert.ToDateTime(obj.dtLastAccess),
                                                       EnableCheckAccount = Convert.ToBoolean(obj.EnableCheckAccount),
                                                       LoginFlag = obj.LoginFlag.ToString(),
                                                       CurrencyCode = obj.CurrencyCode,
                                                       ParentID = Convert.ToInt64(obj.ParentID),
                                                       agentCategory = obj.agentCategory,
                                                       UserType = obj.UserType
                                                   };
                TotalCount = result.Count();
                List<AgencyDetails> arrResult = result.ToList();
                arrResult = arrResult.Skip(start).Take(length).ToList();
                int Sorting = DTResult.iSortingCols;
               // if (Sorting == 0)
                {
                    string sortOrder = DTResult.sSortDir_0;
                    string sortColumns = Sorting.ToString();
                    sortOrder = DTResult.sSortDir_0;
                    switch (sortColumns)
                    {
                        case "0":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.AgencyName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.AgencyName).ToList();
                            break;
                        case "1":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.uid).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.uid).ToList();
                            break;
                        case "2":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Agentuniquecode).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Agentuniquecode).ToList();
                            break;
                        case "3":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.AvailableCredit).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.AvailableCredit).ToList();
                            break;
                        case "4":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.LoginFlag).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.LoginFlag).ToList();
                            break;
                    }
                }
             return arrResult;
            }

        }

        #region Seaching
        public static List<AgencyDetails> Search(string Name, string Type, string Code, string Group, string Status, string Country, string City, string MinBalance ,out int TotalCount)
        {
            bool UPStatus = false;
            if(Status == "0")
                UPStatus = false;
            if(Status == "1") 
                UPStatus = true;

            TotalCount = 0;
            var arrData = DTResult.arrTable;
            string search = DTResult.sSearch;

            string sortOrder = DTResult.sSortDir_0;
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;
            using (var db = new helperDataContext())
            {
                IQueryable<AgencyDetails> result = from obj in db.tbl_AdminLogins
                                                   from objContact in db.tbl_Contacts
                                                   from objCredit in db.tbl_AdminCreditLimits
                                                  // from ObjHcity in db.tbl_HCities
                                                   where obj.ContactID == objContact.ContactID && 
                                                   obj.sid == objCredit.uid && (obj.UserType == "Agent" || obj.UserType == "Franchisee") 
                                                   where (search == null
                                                   || (obj.ContactPerson != null && obj.ContactPerson.ToLower().Contains(search.ToLower())
                                                   || obj.uid != null && obj.uid.ToLower().Contains(search.ToLower())
                                                   || obj.Agentuniquecode != null && obj.Agentuniquecode.ToString().ToLower().Contains(search.ToLower())
                                                   || obj.LoginFlag != null && obj.LoginFlag.ToString().ToLower().Contains(search.ToLower())
                                                   || objCredit.AvailableCredit != null && objCredit.AvailableCredit.ToString().ToLower().Contains(search.ToLower())
                                                   )) && obj.ParentID == AccountManager.GetAdminByLogin()

                                                   select new AgencyDetails
                                                   {
                                                       sid = obj.sid,
                                                       AgencyName = obj.AgencyName,
                                                       Agentuniquecode = obj.Agentuniquecode,
                                                       uid = obj.uid,
                                                       password = obj.password,
                                                       AvailableCredit = Convert.ToDecimal(objCredit.AvailableCredit),
                                                       GroupName = "Group A",
                                                       Description = (from objCity in db.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Description,
                                                       Country = (from objCity in db.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Countryname,
                                                       ContactPerson = obj.ContactPerson,
                                                       Mobile = objContact.Mobile,
                                                       dtLastAccess = Convert.ToDateTime(obj.dtLastAccess),
                                                       EnableCheckAccount = Convert.ToBoolean(obj.EnableCheckAccount),
                                                       LoginFlag = obj.LoginFlag.ToString(),
                                                       CurrencyCode = obj.CurrencyCode,
                                                       ParentID = Convert.ToInt64(obj.ParentID),
                                                       agentCategory = obj.agentCategory,
                                                       UserType = obj.UserType
                                                   };
             //   TotalCount = result.Count();
                List<AgencyDetails> arrResult = result.ToList();
                var arrFilter = new List<AgencyDetails>();
                if(Code != "")
                {
                    arrFilter = arrResult.Where(item => item.Agentuniquecode == Code).ToList();
                    arrResult = arrFilter;
                }

                if (Name != "")
                {
                    arrFilter = arrResult.Where(item => item.uid == Name).ToList();
                    arrResult = arrFilter;
                }

                if (MinBalance != "")
                {
                    arrFilter = arrResult.Where(item => item.AvailableCredit >= Convert.ToDecimal(MinBalance)).ToList();
                    arrResult = arrFilter;
                }

                if (Status != "")
                {
                    arrFilter = arrResult.Where(item => item.LoginFlag == UPStatus.ToString()).ToList();
                    arrResult = arrFilter;
                }

                if (Country != "Select Any Country")
                {
                    arrFilter = arrResult.Where(item => item.Country == Country).ToList();
                    arrResult = arrFilter;
                }

                if (City != "Select Any City")
                {
                    arrFilter = arrResult.Where(item => item.Description == City).ToList();
                    arrResult = arrFilter;
                }
                TotalCount = arrResult.Count();
                arrResult = arrResult.Skip(start).Take(length).ToList();
                int Sorting = DTResult.iSortingCols;
                if (Sorting == 1)
                {
                    string sortColumns = DTResult.iSortCol_0;
                    sortOrder = DTResult.sSortDir_0;
                    switch (sortColumns)
                    {
                        case "0":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.AgencyName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.AgencyName).ToList();
                            break;
                        case "1":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.uid).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.uid).ToList();
                            break;
                        case "2":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Agentuniquecode).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Agentuniquecode).ToList();
                            break;
                        case "3":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.AvailableCredit).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.AvailableCredit).ToList();
                            break;
                        case "4":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.LoginFlag).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.LoginFlag).ToList();
                            break;
                    }
                }
                return arrResult;
            }

        }
        #endregion
        #endregion

       
    }
}