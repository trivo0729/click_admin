﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CommonLib.Response;
using CUT.DataLayer;
using System.Configuration;
using CutAdmin.EntityModal;

namespace CutAdmin.DataLayer
{
    public class MarkupTaxManager
    {

        #region Old Markup Work
        public HttpContext Context { get; set; }

        public static int NoNights { get; set; }

        public static string UserCurrency { get; set; }

        public static CUT.DataLayer.MarkupsAndTaxes objMarkupsAndTaxes { get; set; }

        public static float GetCutRoomAmount(float BaseAmt, string currency, Int64 NoofRooms, Int64 Night, string Supplier, out float AgentRoomMarkup)
        {
            try
            {
                float CutRoomAmtWithMarkup = 0;
                float CutRoomAmount = 0;

                //For Global Markup Amount // 
                float CutGlobalMarkupAmt = 0; float CutGlobalMarkupPer = 0; float ActualGlobalMarkupAmt = 0;

                //For Group Markup Amount // 
                float CutGroupMarkupAmt = 0; float CutGroupMarkupPer = 0; float ActualGroupMarkupAmt = 0;

                //For Individual Markup Amount // 
                float CutIndividualMarkupAmt = 0; float CutIndividualMarkupPer = 0; float ActualIndividualMarkupAmt = 0;

                //For Agent Own Markup Amount // 
                float ActualAgentRoomMarkup = 0; AgentRoomMarkup = 0; float AgentOwnMarkupPer = 0; float AgentOwnMarkupAmt = 0;

                float GlobalMarkup = 0; float GroupMarkup = 0; float IndividualMarkup = 0;

                float ServiceTaxAmount = 0;
                if (currency == "INR")
                {
                    // CutGlobalMarkup //
                    float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).GlobalMarkupPer;
                    CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * BaseAmt);
                    //CutGlobalMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GlobalMarkAmt;
                    ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                    GlobalMarkup = ActualGlobalMarkupAmt + BaseAmt;

                    // CutGroupMarkup //
                    float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).GroupMarkupPer;
                    CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                    //CutGroupMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GroupMarkAmt;
                    ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                    GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                    // CutIndividualMarkup //
                    if (objMarkupsAndTaxes.listIndividualMarkup != null)
                    {
                        float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).IndMarkupPer;
                        CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                        float IndMarkAmt = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).IndMarkAmt;
                        CutIndividualMarkupAmt = (NoofRooms * Night) * IndMarkAmt;
                        ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                        IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                    }


                    CutRoomAmtWithMarkup = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                    bool TaxOnMarkup = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).TaxOnMarkup;
                    if (TaxOnMarkup == true)
                    {
                        ServiceTaxAmount = GetMarkupServiceTax(ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt, objMarkupsAndTaxes.PerServiceTax);
                        CutRoomAmtWithMarkup = CutRoomAmtWithMarkup + ServiceTaxAmount;
                    }
                    // Start Agent Markup //

                    //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                    float AgentMarkupPer = 0;
                    if (objMarkupsAndTaxes.listAgentMarkup != null)
                    {
                        AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                        AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupAmt;
                    }

                    AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);

                    ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                    //End Agent Markup // 

                    CutRoomAmount = (BaseAmt + CutRoomAmtWithMarkup);
                    AgentRoomMarkup = ActualAgentRoomMarkup;


                }
                else
                {
                    // CutGlobalMarkup //
                    float GlobalMarkupPer = objMarkupsAndTaxes.listGlobalMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).GlobalMarkupPer;
                    CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * BaseAmt);
                    //CutGlobalMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GlobalMarkAmt;
                    ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, CutGlobalMarkupAmt);
                    GlobalMarkup = ActualGlobalMarkupAmt + BaseAmt;

                    // CutGroupMarkup //
                    float GroupMarkupPer = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).GroupMarkupPer;
                    CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                    //CutGroupMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.GroupMarkAmt;
                    ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, CutGroupMarkupAmt);
                    GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;

                    // CutIndividualMarkup //
                    if (objMarkupsAndTaxes.listIndividualMarkup != null)
                    {
                        float IndMarkupPer = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).IndMarkupPer;
                        CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                        float IndMarkAmt = objMarkupsAndTaxes.listIndividualMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).IndMarkAmt;
                        CutIndividualMarkupAmt = (NoofRooms * Night) * IndMarkAmt;
                        ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                        IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                    }


                    CutRoomAmtWithMarkup = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                    bool TaxOnMarkup = objMarkupsAndTaxes.listGroupMarkup.Single(Suppliers => Suppliers.Supplier == Supplier).TaxOnMarkup;
                    if (TaxOnMarkup == true)
                    {
                        ServiceTaxAmount = GetMarkupServiceTax(ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt, objMarkupsAndTaxes.PerServiceTax);
                        CutRoomAmtWithMarkup = CutRoomAmtWithMarkup + ServiceTaxAmount;
                    }
                    // Start Agent Markup //

                    //AgentOwnMarkupPer = ((objMarkupsAndTaxes.AgentOwnMarkupPer / 100) * IndividualMarkup);
                    float AgentMarkupPer = 0;
                    if (objMarkupsAndTaxes.listAgentMarkup != null)
                    {
                        AgentMarkupPer = objMarkupsAndTaxes.listAgentMarkup.Single(SupplierType => SupplierType.SupplierType == "1").AgentOwnMarkupPer;
                    }
                    AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                    //AgentOwnMarkupAmt = (NoofRooms * Night) * objMarkupsAndTaxes.AgentOwnMarkupAmt;
                    ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                    //End Agent Markup // 

                    CutRoomAmount = (BaseAmt + CutRoomAmtWithMarkup);
                    AgentRoomMarkup = ActualAgentRoomMarkup;


                }
                return CutRoomAmount;
            }
            catch (Exception ex)
            {
                string Line = ex.StackTrace.ToString();
                throw new Exception("");
            }




        }

        public static float GetMarkupServiceTax(float MarkupAmt, float ServiceTax)
        {
            return (MarkupAmt * ServiceTax / 100);
        }
        #endregion

        #region Markup Module
        public static MarkupCommission objMarkupCommission { get; set; }
        public static MarkupCommission GetMarkupTax(Int64 SupplierID, Int64 HotelCode)
        {
            MarkupCommission objMarkups = new MarkupCommission();
            objMarkups.objAdmin = new MarkupTaxces();
            objMarkups.objSupplierMarup = new MarkupTaxces();
            Int64 Uid = AccountManager.GetUserByLogin();
            Int64 ParentID = AccountManager.GetSupplierByUser();
            try
            {
                Int64 S2S_ID = ParentID;
                objMarkups.objAdmin = GetMarkupTaxces(ParentID);
                if (SupplierID != ParentID)
                    objMarkups.objSupplierMarup = GetMarkupTaxces(SupplierID);
            }
            catch
            {

            }
            return objMarkups;
        }

        public static MarkupTaxces GetMarkupTaxces(Int64 SupplierID)
        {
            MarkupTaxces objMarkups = new MarkupTaxces();
            objMarkups.ListTaxces = new List<TaxRate>();
            objMarkups.GlobalMarkup = new Markup();
            objMarkups.GroupMarkup = new Markup();
            objMarkups.IndividualMarkup = new Markup();
            objMarkups.SupplierMarkup = new Markup();
            Int64 Uid = AccountManager.GetUserByLogin();
            try
            {
                using (var db = new helperDataContext())
                {
                    using (var DB = new Click_Hotel())
                    {
                        #region Global Markup
                        var arrGlobal = (from obj in db.tbl_GlobalMarkups where obj.Type == 1 select obj).FirstOrDefault();
                        if (arrGlobal != null)
                        {
                            objMarkups.GlobalMarkup.MarkupPer = Convert.ToSingle(arrGlobal.MarkupPercentage);
                            objMarkups.GlobalMarkup.MarkupAmt = Convert.ToSingle(arrGlobal.MarkupAmmount);
                            objMarkups.GlobalMarkup.CommessionPer = Convert.ToSingle(arrGlobal.CommessionPercentage);
                            objMarkups.GlobalMarkup.CommessionAmt = Convert.ToSingle(arrGlobal.CommessionAmmount);
                        }
                        #endregion

                        #region Group Markup
                        var arrGroup = (from obj in db.tbl_GroupMarkupDetails
                                        from objMapGroup in db.tbl_AgentGroupMarkupMappings
                                        where obj.Type == 1 && obj.GroupId == objMapGroup.GroupId && objMapGroup.AgentId == Uid
                                        select new
                                        {
                                            obj.MarkupPercentage,
                                            obj.MarkupAmmount,
                                            obj.CommessionPercentage,
                                            obj.CommessionAmmount
                                        }).Distinct().FirstOrDefault();
                        if (arrGroup != null)
                        {
                            objMarkups.GroupMarkup.MarkupPer = Convert.ToSingle(arrGroup.MarkupPercentage);
                            objMarkups.GroupMarkup.MarkupAmt = Convert.ToSingle(arrGroup.MarkupAmmount);
                            objMarkups.GroupMarkup.CommessionPer = Convert.ToSingle(arrGroup.CommessionPercentage);
                            objMarkups.GroupMarkup.CommessionAmt = Convert.ToSingle(arrGroup.CommessionAmmount);
                        }
                        #endregion

                        #region Agent Individual
                        var arrIndividual = (from obj in db.tbl_IndividualMarkups
                                             where obj.AgentId == Uid && obj.Type == 1
                                             select new
                                             {
                                                 obj.MarkupPercentage,
                                                 obj.MarkupAmmount,
                                                 obj.CommessionPercentage,
                                                 obj.CommessionAmmount
                                             }).FirstOrDefault();
                        if (arrIndividual != null)
                        {
                            objMarkups.IndividualMarkup.MarkupPer = Convert.ToSingle(arrIndividual.MarkupPercentage);
                            objMarkups.IndividualMarkup.MarkupAmt = Convert.ToSingle(arrIndividual.MarkupAmmount);
                            objMarkups.IndividualMarkup.CommessionPer = Convert.ToSingle(arrIndividual.CommessionPercentage);
                            objMarkups.IndividualMarkup.CommessionAmt = Convert.ToSingle(arrIndividual.CommessionAmmount);
                        }
                        #endregion

                        #region Agent Own Markup
                        var arrSupplierMarkupOwn = (from obj in db.tbl_AgentMarkups where obj.uid == SupplierID && obj.ServiceType == 1 select obj).FirstOrDefault();
                        if (arrSupplierMarkupOwn != null)
                        {
                            objMarkups.SupplierMarkup.MarkupAmt = Convert.ToSingle(arrSupplierMarkupOwn.Amount);
                            objMarkups.SupplierMarkup.MarkupPer = Convert.ToSingle(arrSupplierMarkupOwn.Percentage);
                        }
                        #endregion

                        #region Tax Rates
                        var arrTaxes = DB.Comm_SupplierTax.Where(d => d.SupplierID == SupplierID).ToList();
                        foreach (Int64 objTax in arrTaxes.Select(d => d.TaxID).Distinct())
                        {
                            objMarkups.ListTaxces.Add(new TaxRate
                            {
                                ID = objTax,
                                BaseRate = 0,
                                Per = Convert.ToSingle(arrTaxes.Where(d => d.TaxID == objTax).FirstOrDefault().Value),
                                RateName = DB.Comm_Tax.Where(d => d.ID == objTax).FirstOrDefault().Name,
                                TotalRate = 0,
                                TaxOn = new List<Tax>(),
                            });
                            List<Tax> arrTax = new List<Tax>();
                            var ListTax = arrTaxes.Where(d => d.TaxID == objTax).ToList();
                            foreach (var compTax in ListTax)
                            {
                                string TaxName = ""; float TaxPer = 0, TotalRate = 0; Int64 TaxID = 0;
                                if (compTax.TaxOn == 0)
                                {
                                    TaxID = 0;
                                    TaxName = "Base Rate";
                                    TotalRate = 0;
                                }
                                else if (compTax.TaxOn == -1)
                                {
                                    TaxID = -1;
                                    TaxName = "Markup Rate";
                                    TaxPer = Convert.ToSingle(0);
                                    TotalRate = 0;
                                }
                                else
                                {
                                    TaxID = Convert.ToInt64(compTax.TaxOn);
                                    TaxName = DB.Comm_Tax.Where(d => d.ID == compTax.TaxOn).FirstOrDefault().Name;
                                    TaxPer = Convert.ToSingle(DB.Comm_Tax.Where(d => d.ID == compTax.TaxOn).FirstOrDefault().Value);
                                    TotalRate = TaxPer * Convert.ToSingle(arrTaxes.Where(d => d.TaxID == compTax.TaxOn).FirstOrDefault().Value);
                                }
                                arrTax.Add(new Tax { ID = Convert.ToInt64(compTax.TaxOn), TaxName = TaxName, TaxPer = TaxPer, TaxRate = TotalRate });
                            }
                            objMarkups.ListTaxces.LastOrDefault().TaxOn = arrTax;
                            objMarkups.ListTaxces.LastOrDefault().TotalRate = arrTax.Select(d => d.TaxRate).Sum();
                            #endregion
                        }
                    }

                  
                }
            }
            catch
            {

            }
            return objMarkups;
        }

        public static float GetRoomAmount(float BaseAmt, out float SupplierMarkup, out float S2SMarkup, out List<TaxRate> S2SRate, out List<TaxRate> B2BRate)
        {
            try
            {
                float TotalRate = 0; SupplierMarkup = 0; S2SMarkup = 0;
                S2SRate = new List<TaxRate>(); B2BRate = new List<TaxRate>();
                S2SMarkup = GetMarkupTax(BaseAmt, objMarkupCommission.objSupplierMarup, out S2SRate);
                BaseAmt = BaseAmt + S2SMarkup;
                SupplierMarkup = GetMarkupTax(BaseAmt, objMarkupCommission.objAdmin, out B2BRate);
                return SupplierMarkup;
            }
            catch (Exception ex)
            {
                string Line = ex.StackTrace.ToString();
                throw new Exception("");
            }
        }


        public static float GetMarkupTax(float BaseAmt, MarkupTaxces objMarkup, out List<TaxRate> arrTax)
        {
            try
            {
                arrTax = new List<TaxRate>();
                float CutRoomAmtWithMarkup = 0;
                float CutRoomAmount = 0;

                //For Global Markup Amount // 
                float CutGlobalMarkupAmt = 0; float CutGlobalMarkupPer = 0; float ActualGlobalMarkupAmt = 0;

                //For Group Markup Amount // 
                float CutGroupMarkupAmt = 0; float CutGroupMarkupPer = 0; float ActualGroupMarkupAmt = 0;

                //For Individual Markup Amount // 
                float CutIndividualMarkupAmt = 0; float CutIndividualMarkupPer = 0; float ActualIndividualMarkupAmt = 0;

                //For Agent Own Markup Amount // 
                float ActualAgentRoomMarkup = 0, SupplierMarkup = 0; float AgentOwnMarkupPer = 0; float AgentOwnMarkupAmt = 0;

                float GlobalMarkup = 0; float GroupMarkup = 0; float IndividualMarkup = 0;

                // Supplier to Supplier Markup
                float ActualS2SMarkupAmt = 0;
                float GlobalMarkupPer = 0;
                if (objMarkup.GlobalMarkup != null)
                {
                    GlobalMarkupPer = objMarkup.GlobalMarkup.MarkupPer;
                    CutGlobalMarkupPer = ((GlobalMarkupPer / 100) * BaseAmt);
                    ActualGlobalMarkupAmt = Getgreatervalue(CutGlobalMarkupPer, objMarkup.GlobalMarkup.MarkupAmt);
                    GlobalMarkup = ActualGlobalMarkupAmt + BaseAmt;
                }
                // CutGroupMarkup //
                float GroupMarkupPer = 0;
                if (objMarkup.GroupMarkup != null)
                {
                    GroupMarkupPer = objMarkup.GroupMarkup.MarkupPer;
                    CutGroupMarkupPer = ((GroupMarkupPer / 100) * GlobalMarkup);
                    ActualGroupMarkupAmt = Getgreatervalue(CutGroupMarkupPer, objMarkup.GroupMarkup.MarkupAmt);
                }
                GroupMarkup = ActualGroupMarkupAmt + GlobalMarkup;
                // CutIndividualMarkup //
                if (objMarkup.IndividualMarkup != null)
                {
                    float IndMarkupPer = objMarkup.IndividualMarkup.MarkupPer;
                    CutIndividualMarkupPer = ((IndMarkupPer / 100) * GroupMarkup);
                    float IndMarkAmt = objMarkup.IndividualMarkup.MarkupAmt;
                    CutIndividualMarkupAmt = IndMarkAmt;
                    ActualIndividualMarkupAmt = Getgreatervalue(CutIndividualMarkupPer, CutIndividualMarkupAmt);
                }
                IndividualMarkup = ActualIndividualMarkupAmt + GroupMarkup;
                CutRoomAmtWithMarkup = ActualGlobalMarkupAmt + ActualGroupMarkupAmt + ActualIndividualMarkupAmt;
                float AgentMarkupPer = 0;
                if (objMarkup.SupplierMarkup != null)
                {
                    AgentMarkupPer = objMarkup.SupplierMarkup.MarkupPer;
                    AgentOwnMarkupAmt = objMarkup.SupplierMarkup.MarkupAmt;
                }
                AgentOwnMarkupPer = ((AgentMarkupPer / 100) * IndividualMarkup);
                ActualAgentRoomMarkup = Getgreatervalue(AgentOwnMarkupPer, AgentOwnMarkupAmt);
                //End Agent Markup // 
                CutRoomAmount = (CutRoomAmtWithMarkup);
                SupplierMarkup = ActualAgentRoomMarkup;
                arrTax = TaxManager.GetTaxRates(objMarkup.ListTaxces, BaseAmt, CutRoomAmtWithMarkup);
                return CutRoomAmtWithMarkup;
            }
            catch (Exception ex)
            {
                string Line = ex.StackTrace.ToString();
                throw new Exception("");
            }
        }
        #endregion

        public static float Getgreatervalue(float Percentage, float Ammount)
        {
            if (Percentage > Ammount)
            {
                return Percentage;
            }
            else
            {
                return Ammount;
            }
        }

        public static List<ListGstTax> GetGstList(Int64 SupplierID, Int64 UserID)
        {
            helperDataContext db = new helperDataContext();
            Int64 ParentID = Convert.ToInt64(ConfigurationManager.AppSettings["AdminKey"]);
            // GST logic Start here  // 
            var GstTaxces = (from obj in db.tbl_TAXes where obj.ParentID == ParentID select obj).ToList();
            List<ListGstTax> objGstList = new List<ListGstTax>();
            Int64 aLogin = 0; Int64 fLogin = 0; Int64 sLogin = 0; Int64 uLogin = 0;
            var aContact = new tbl_Contact(); var fContact = new tbl_Contact(); var sContact = new tbl_Contact(); var uContact = new tbl_Contact();
            Boolean isCut = true;
            // Franchise GST Details
            if (SupplierID != ParentID)
            {
                // Admin Gst Details
                aLogin = (from obj in db.tbl_AdminLogins where obj.sid == ParentID select obj.ContactID).FirstOrDefault();
                aContact = (from obj in db.tbl_Contacts where obj.ContactID == aLogin select obj).FirstOrDefault();
                isCut = true;
                //  Franchise Login Details
                fLogin = (from obj in db.tbl_AdminLogins where obj.sid == SupplierID select obj.ContactID).FirstOrDefault();
                fContact = (from obj in db.tbl_Contacts where obj.ContactID == fLogin select obj).FirstOrDefault();
            }

            // Supplier Gst Details
            sLogin = (from obj in db.tbl_AdminLogins where obj.sid == SupplierID select obj.ContactID).FirstOrDefault();
            sContact = (from obj in db.tbl_Contacts where obj.ContactID == sLogin select obj).FirstOrDefault();

            //  User Login Details
            uLogin = (from obj in db.tbl_AdminLogins where obj.sid == UserID select obj.ContactID).FirstOrDefault();
            uContact = (from obj in db.tbl_Contacts where obj.ContactID == uLogin select obj).FirstOrDefault();

            string Country = (from obj in db.tbl_HCities where obj.Code == uContact.Code select obj).FirstOrDefault().Country;
            if (Country == "IN")
            {
                foreach (tbl_TAX objTax in GstTaxces)
                {
                    // Franchisee 
                    if (fContact != null && fContact.StateID == aContact.StateID && fContact.StateID != null && fContact.StateID != "")
                    {
                        float CGST = 0, SGST = 0, IGST = 0, UGST = 0;
                        if (Convert.ToBoolean(objTax.OnMarkup) == false)
                        {
                            CGST = Convert.ToSingle(objTax.GstTax / 2);
                            SGST = Convert.ToSingle(objTax.GstTax / 2);
                        }
                        else
                            UGST = Convert.ToSingle(objTax.GstTax / 2);

                        //  SITI : Sold Inside Ticket Inside
                        objGstList.Add(new ListGstTax
                        {
                            Service = objTax.Service,
                            OnMarkup = Convert.ToBoolean(objTax.OnMarkup),
                            CGST = CGST,
                            SGST = SGST,
                            IGST = IGST,
                            UGST = UGST,
                            IsCUTGST = isCut,
                        });

                    }
                    else if (fContact != null && fContact.StateID != aContact.StateID && fContact.StateID != null && fContact.StateID != "")
                    {
                        float CGST = 0, SGST = 0, IGST = 0, UGST = 0;
                        if (Convert.ToBoolean(objTax.OnMarkup) == false)
                        {
                            IGST = Convert.ToSingle(objTax.GstTax);
                        }
                        else
                            IGST = Convert.ToSingle(objTax.GstTax);
                        //  SITO : Sold Inside Ticket Outside
                        objGstList.Add(new ListGstTax
                        {
                            Service = objTax.Service,
                            OnMarkup = Convert.ToBoolean(objTax.OnMarkup),
                            CGST = CGST,
                            SGST = SGST,
                            IGST = IGST,
                            UGST = UGST,
                            IsCUTGST = isCut,
                        });
                    }

                    if (uContact.StateID == sContact.StateID && uContact.StateID != "")
                    {
                        float CGST = 0, SGST = 0, IGST = 0, UGST = 0;
                        if (Convert.ToBoolean(objTax.OnMarkup) == false)
                        {
                            CGST = Convert.ToSingle(objTax.GstTax / 2);
                            SGST = Convert.ToSingle(objTax.GstTax / 2);
                        }
                        else
                            UGST = Convert.ToSingle(objTax.GstTax / 2);

                        //  SITI : Sold Inside Ticket Inside
                        objGstList.Add(new ListGstTax
                        {
                            Service = objTax.Service,
                            OnMarkup = Convert.ToBoolean(objTax.OnMarkup),
                            CGST = CGST,
                            SGST = SGST,
                            IGST = IGST,
                            UGST = UGST,
                            IsCUTGST = true
                        });

                    }
                    else if (uContact.StateID != sContact.StateID && uContact.StateID != "")
                    {
                        float CGST = 0, SGST = 0, IGST = 0, UGST = 0;
                        if (Convert.ToBoolean(objTax.OnMarkup) == false)
                        {
                            IGST = Convert.ToSingle(objTax.GstTax);
                        }
                        else
                            IGST = Convert.ToSingle(objTax.GstTax);
                        //  SITO : Sold Inside Ticket Outside
                        objGstList.Add(new ListGstTax
                        {
                            Service = objTax.Service,
                            OnMarkup = Convert.ToBoolean(objTax.OnMarkup),
                            CGST = CGST,
                            SGST = SGST,
                            IGST = IGST,
                            UGST = UGST,
                            IsCUTGST = true
                        });
                    }
                }
            }

            return objGstList;
        }

        public static List<GSTdetails> GetGstDetails(float MarkupPrice, string Service)
        {
            float CGST = 0, SGST = 0, IGST = 0, UGST = 0;
            List<GSTdetails> ListGstDetails = new List<GSTdetails>();
            try
            {
                if (objMarkupsAndTaxes.ListGstTax.Count != 0)
                {
                    ListGstTax objGst = objMarkupsAndTaxes.ListGstTax.Where(d => d.Service == Service).FirstOrDefault();
                    if (objGst != null)
                    {
                        CGST = MarkupPrice * objGst.CGST / 100;
                        SGST = MarkupPrice * objGst.SGST / 100;
                        IGST = MarkupPrice * objGst.IGST / 100;
                        UGST = MarkupPrice * objGst.UGST / 100;

                        if (CGST != 0 || MarkupPrice == 0)
                            ListGstDetails.Add(new GSTdetails { OnMarkup = objGst.OnMarkup, Amount = CGST, Type = "CGST", Markup = MarkupPrice, PerCentage = objGst.CGST.ToString() });
                        if (SGST != 0 || MarkupPrice == 0)
                            ListGstDetails.Add(new GSTdetails { OnMarkup = objGst.OnMarkup, Amount = SGST, Type = "SGST", Markup = MarkupPrice, PerCentage = objGst.SGST.ToString() });
                        if (IGST != 0 || MarkupPrice == 0)
                            ListGstDetails.Add(new GSTdetails { OnMarkup = objGst.OnMarkup, Amount = IGST, Type = "IGST", Markup = MarkupPrice, PerCentage = objGst.IGST.ToString() });
                        //if (UGST != 0 || MarkupPrice == 0)
                        //    ListGstDetails.Add(new GSTdetails { OnMarkup = objGst.OnMarkup, Amount = UGST, Type = "CESS", Markup = MarkupPrice, PerCentage = objGst.UGST.ToString() });
                    }
                }
            }
            catch
            {

            }
            return ListGstDetails;
        }

    }
}