﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;using CutAdmin.dbml;
using CutAdmin.dbml;
namespace CutAdmin.DataLayer
{
    public class Supplier
    {
        public Int64 MarkupID { get; set; }
        public string Name { get; set; }
        public decimal MarkupAmt { get; set; }
        public decimal MarkupPer { get; set; }
        public decimal CommAmt { get; set; }
        public decimal CommPer { get; set; }
    }
    public class Service
    {
        public string Name { get; set; }
        public List<Supplier> ListSupplier { get; set; }
    }

    public class MarkupManger
    {
        public static List<string> ServiceName { get; set; }

        public static List<Service> GetService()
        {
            ServiceName = new List<string> { "Hotel", "Visa", "OTB", "Transfer", "Activity", "Packages", "Flight" };
            List<Service> ListService = new List<Service>();
            try
            {
                String UserType = AccountManager.GetUserType();
                using (var db = new helperDataContext())
                {
                   

                    foreach (var ojService in ServiceName)
                    {
                        if (UserType == "Admin" || UserType == "AdminStaff")
                        {
                            var arrSupplier = (from obj in db.tbl_GlobalMarkups
                                               where obj.Type == Convert.ToInt64(ServiceName.IndexOf(ojService) + 1)
                                               select new Supplier
                                               {
                                                   Name = obj.Supplier,
                                               }).ToList();
                            ListService.Add(new Service { Name = ojService, ListSupplier = arrSupplier });
                        }
                        else if(UserType == "Franchisee" || UserType == "FranchiseeStaff")
                        {
                            ListService.Add(new Service { Name = ojService, ListSupplier = new List<Supplier>() { new Supplier { Name = ojService, } } });
                        }
                      
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return ListService;
        }
    }
}