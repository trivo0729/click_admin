﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.Flightdbml;

namespace CutAdmin.DataLayer
{
    public class FlightReservation
    {
        public tbl_AirReservation Reservation { get; set; }
        public List<string> Paxces { get; set; }
        public List<tbl_AirSegment> Segments { get; set; }
        public tbl_AdminLogin Agency { get; set; }
    }

    public class FlightManager
    {
        #region Booking List
        public static List<FlightReservation> GetBookings()
        {
            var arrResrvation = new List<tbl_AirReservation>();
            List<FlightReservation> ListResrvation = new List<FlightReservation>();
          CUT.DataLayer.GlobalDefault  objGlobalDefault  = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            FlightHelperDataContext db = new FlightHelperDataContext();
            try
            {
                arrResrvation = (from obj in db.tbl_AirReservations  join
                                 objAgency in db.tbl_AdminLogins  on obj.uid equals objAgency.sid
                                 where objAgency.ParentID == AccountManager.GetAdminByLogin()
                                 select obj).ToList();
                foreach (var obj in arrResrvation)
                {
                    ListResrvation.Add(new FlightReservation
                    {
                        Reservation = obj,

                        Paxces = (from objPax in db.tbl_AirPaxDetails
                                  where objPax.BookingID == obj.BookingID
                                  select objPax.PaxTitle + " " + objPax.FirstName + " " + objPax.LastName).ToList(),
                        Segments = (from objSeg in db.tbl_AirSegments
                                    where objSeg.BookingID == obj.BookingID
                                    select objSeg).ToList(),
                        Agency = (from objAgency in db.tbl_AdminLogins
                                  where objAgency.sid == obj.uid
                                  select objAgency).FirstOrDefault(),
                    });
                }
                HttpContext.Current.Session["tbl_AirReservations"] = (List<FlightReservation>)ListResrvation;
            }
            catch
            {
            }
            return ListResrvation;
        }

        public static object GetSearch(string BooingStatus, string sArrival, string sDeparture, Int64 Agency, string[] Airlines, string InvoiceDate, string Reference, string Location, string Passenger)
        {
            List<FlightReservation> arrResrvation = new List<FlightReservation>();
            List<tbl_AirReservation> sReservation = new List<tbl_AirReservation>();
            List<FlightReservation> arrSearch = new List<FlightReservation>();
            try
            {
                if (HttpContext.Current.Session["tbl_AirReservations"] == null)
                    throw new Exception("No Search Found");
                arrResrvation = (List<FlightReservation>)HttpContext.Current.Session["tbl_AirReservations"];
                if (BooingStatus == "All")
                    sReservation = arrResrvation.Select(d => d.Reservation).ToList().Where(r => r.TicketStatus == "1" || r.TicketStatus == "Cancelled" || r.TicketStatus == "").ToList();
                else
                    sReservation = arrResrvation.Select(d => d.Reservation).ToList().Where(r => r.TicketStatus == BooingStatus).ToList();
                if (sDeparture != "")
                {
                    sReservation = arrResrvation.Select(d => d.Reservation).ToList().Where(r => DateTime.ParseExact(r.DepartureDate.Split(' ')[0], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) ==
                                                       DateTime.ParseExact(sDeparture, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture)
                                                        ).ToList();
                }
                if (sArrival != "")
                {
                    sReservation = arrResrvation.Select(d => d.Reservation).ToList().Where(r => DateTime.ParseExact(r.ArrivalDate.Split(' ')[0], "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture) ==
                                                       DateTime.ParseExact(sArrival, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture)
                                                        ).ToList();
                }
                if (InvoiceDate != "")
                {
                    sReservation = arrResrvation.Select(d => d.Reservation).ToList().Where(r => DateTime.ParseExact( r.InvoiceCreatedOn.Split(' ')[0],"dd-MM-yyyy",System.Globalization.CultureInfo.InvariantCulture) ==
                                                       DateTime.ParseExact(InvoiceDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture)
                                                        ).ToList();
                }
                if (Reference != "")
                {
                    sReservation = arrResrvation.Select(d => d.Reservation).ToList().Where(r => r.InvoiceNo == Reference).ToList();
                }
                foreach (var obj in sReservation)
                {
                    arrSearch.Add(new FlightReservation
                    {
                        Reservation = obj,
                        Agency = arrResrvation.Where(d => d.Reservation.BookingID == obj.BookingID).FirstOrDefault().Agency,
                        Paxces = arrResrvation.Where(d => d.Reservation.BookingID == obj.BookingID).FirstOrDefault().Paxces,
                        Segments = arrResrvation.Where(d => d.Reservation.BookingID == obj.BookingID).FirstOrDefault().Segments,
                    });
                }
                if (Passenger != "")
                {
                    arrSearch = new List<FlightReservation>();
                    foreach (var objResrvation in arrResrvation)
                    {

                        if(objResrvation.Paxces.Contains(Passenger))
                        {
                            arrSearch.Add(new FlightReservation
                            {
                                Reservation = objResrvation.Reservation,
                                Agency = arrResrvation.Where(d => d.Reservation.BookingID == objResrvation.Reservation.BookingID).FirstOrDefault().Agency,
                                Paxces = arrResrvation.Where(d => d.Reservation.BookingID == objResrvation.Reservation.BookingID).FirstOrDefault().Paxces,
                                Segments = arrResrvation.Where(d => d.Reservation.BookingID == objResrvation.Reservation.BookingID).FirstOrDefault().Segments,
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return arrSearch.Distinct().ToList();
        }

        #endregion
    }
}