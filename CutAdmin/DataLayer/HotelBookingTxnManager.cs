﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Web;

namespace CutAdmin.DataLayer
{
    public class HotelBookingTxnManager
    {
        public static DBHelper.DBReturnCode HotelBookingTxnAdd(string ReservationID, string TransactionDate, float BookingAmt, float SeviceTax, float BookingAmtWithTax, float CanAmount, float CanServiceTax, float CanAmtWithTax, float Balance, float SalesTax, float LuxuryTax, int DiscountPer, string BookingCreationDate, string BookingRemarks, int BookingCancelFlag, string Supplier, float SupplierBasicAmt, float AgentComm, float CUTComm, string BookedBy, string BookingStatus)
        {
            int rowsaffected;

            Int64 uid = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();
            AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
            {
                objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                uid = objAgentDetailsOnAdmin.sid;
                if (BookedBy == "")
                    BookedBy = objAgentDetailsOnAdmin.Agentuniquecode;
            }
            else if (HttpContext.Current.Session["LoginUser"] != null)
            {
                objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                uid = objGlobalDefault.sid;
                if (BookedBy == "")
                    BookedBy = objGlobalDefault.Agentuniquecode;
            }

            SqlParameter[] sqlParams = new SqlParameter[21];
            sqlParams[0] = new SqlParameter("@uid", uid);
            sqlParams[1] = new SqlParameter("@ReservationID", ReservationID);
            sqlParams[2] = new SqlParameter("@TransactionDate", TransactionDate);
            sqlParams[3] = new SqlParameter("@BookingAmt", BookingAmt);
            sqlParams[4] = new SqlParameter("@SeviceTax", SeviceTax);
            sqlParams[5] = new SqlParameter("@BookingAmtWithTax", BookingAmtWithTax);
            sqlParams[6] = new SqlParameter("@CanAmt", CanAmount);
            sqlParams[7] = new SqlParameter("@CanServiceTax", CanServiceTax);
            sqlParams[8] = new SqlParameter("@CanAmtWithTax", CanAmtWithTax);
            sqlParams[9] = new SqlParameter("@Balance", Balance);
            sqlParams[10] = new SqlParameter("@SalesTax", SalesTax);
            sqlParams[11] = new SqlParameter("@LuxuryTax", LuxuryTax);
            sqlParams[12] = new SqlParameter("@DiscountPer", DiscountPer);
            sqlParams[13] = new SqlParameter("@BookingCreationDate", BookingCreationDate);
            sqlParams[14] = new SqlParameter("@BookingRemarks", BookingRemarks);
            sqlParams[15] = new SqlParameter("@BookingCancelFlag", BookingCancelFlag);
            sqlParams[16] = new SqlParameter("@Supplier", Supplier);
            sqlParams[17] = new SqlParameter("@SupplierBasicAmt", SupplierBasicAmt);
            sqlParams[18] = new SqlParameter("@AgentComm", AgentComm);
            sqlParams[19] = new SqlParameter("@CUTComm", CUTComm);
            sqlParams[20] = new SqlParameter("@BookedBy", BookedBy);

            DBHelper.DBReturnCode retCode;
            if (BookingStatus == "Vouchered")
            {

                retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionAdd", out rowsaffected, sqlParams);
            }
            else
            {
                retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionAddHold", out rowsaffected, sqlParams);
            }
            return retCode;
        }
        #region New Transactions Manager

        public static DBHelper.DBReturnCode HotelBookingTxn(string ReservationID, string TransactionDate, float BookingAmt, float SeviceTax, float BookingAmtWithTax, float CanAmount, float CanServiceTax, float CanAmtWithTax, float Balance, float SalesTax, float LuxuryTax, int DiscountPer, string BookingCreationDate, string BookingRemarks, int BookingCancelFlag, string Supplier, float SupplierBasicAmt, float AgentComm, float CUTComm, string BookedBy, string BookingStatus)
        {
            int rowsaffected;
            Int64 uid = 0;
            GlobalDefault objGlobalDefault = new GlobalDefault();
            AgentDetailsOnAdmin objAgentDetailsOnAdmin = new AgentDetailsOnAdmin();
            if (HttpContext.Current.Session["AgentDetailsOnAdmin"] != null)
            {
                objAgentDetailsOnAdmin = (AgentDetailsOnAdmin)HttpContext.Current.Session["AgentDetailsOnAdmin"];
                uid = objAgentDetailsOnAdmin.sid;
                if (BookedBy == "")
                    BookedBy = objAgentDetailsOnAdmin.Agentuniquecode;
            }
            else if (HttpContext.Current.Session["LoginUser"] != null)
            {
                objGlobalDefault = (GlobalDefault)HttpContext.Current.Session["LoginUser"];
                uid = objGlobalDefault.sid;
                if (BookedBy == "")
                    BookedBy = objGlobalDefault.Agentuniquecode;
            }

            DBHelper.DBReturnCode retCode = DBHelper.DBReturnCode.EXCEPTION;
            if (BookingStatus == "Vouchered")
            {
                DataTable dtAvailableCredit;
                retCode = DataLayer.AccountManager.GetAvailableCredit(out dtAvailableCredit);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    float AvailableCredit = Convert.ToSingle(dtAvailableCredit.Rows[0]["AvailableCredit"]);
                    float @Creditlimit = Convert.ToSingle(dtAvailableCredit.Rows[0]["CreditAmount"]);
                    float @CreditAmount_Cmp = Convert.ToSingle(dtAvailableCredit.Rows[0]["CreditAmount_Cmp"]);
                    bool Credit_Flag = (bool)dtAvailableCredit.Rows[0]["Credit_Flag"];
                    bool OTC = (bool)dtAvailableCredit.Rows[0]["OTC"];
                    float @OTClimit = Convert.ToSingle(dtAvailableCredit.Rows[0]["MaxCreditLimit"]);
                    float @MAXCreditlimit_Cmp = Convert.ToSingle(dtAvailableCredit.Rows[0]["MaxCreditLimit"]);
                    //if ((AvailableCredit + @CreditAmount_Cmp + @OTClimit) >= BookingAmtWithTax)
                    //{ 

                    //}
                    //else
                    //    return retCode = DBHelper.DBReturnCode.EXCEPTION;
                    if (AvailableCredit >= 0 && AvailableCredit >= BookingAmtWithTax)
                    {
                        AvailableCredit = AvailableCredit - BookingAmtWithTax;
                    }
                    else if (@AvailableCredit <= 0 && @Creditlimit <= 0 && @OTClimit >= BookingAmtWithTax && @OTC == true)
                    {
                        AvailableCredit = AvailableCredit - BookingAmtWithTax;
                        @OTClimit = 0;
                        @MAXCreditlimit_Cmp = 0;
                        @OTC = false;
                    }
                    else if (@AvailableCredit >= 0 && (@AvailableCredit + @OTClimit + @Creditlimit) >= BookingAmtWithTax && @OTC == true && @Credit_Flag == true)
                    {
                        AvailableCredit = AvailableCredit - BookingAmtWithTax;
                        float OtcReturn = 0;
                        if (AvailableCredit < 0)
                        {
                            OtcReturn = @OTClimit + AvailableCredit;
                        }
                        if (Creditlimit >= OtcReturn)
                            Creditlimit = Creditlimit + OtcReturn;
                        @OTClimit = 0;
                        @MAXCreditlimit_Cmp = 0;
                        @OTC = false;
                    }
                    else if (@AvailableCredit >= 0 && (@AvailableCredit + @OTClimit) >= BookingAmtWithTax && @OTC == true)
                    {
                        AvailableCredit = AvailableCredit - BookingAmtWithTax;
                        @OTClimit = 0;
                        @MAXCreditlimit_Cmp = 0;
                        @OTC = false;
                    }
                    else if (@AvailableCredit >= 0 && (@AvailableCredit + @Creditlimit) >= BookingAmtWithTax && @Credit_Flag == true)
                    {
                        AvailableCredit = AvailableCredit - BookingAmtWithTax;
                        if (AvailableCredit < 0)
                            @Creditlimit = @Creditlimit + AvailableCredit;
                    }
                    else if (@AvailableCredit < 0 && @Creditlimit > 0 && @Creditlimit >= BookingAmtWithTax && @Credit_Flag == true)
                    {
                        AvailableCredit = AvailableCredit - BookingAmtWithTax;
                        @Creditlimit = @Creditlimit - BookingAmtWithTax;
                    }

                    else
                    {
                        return retCode = DBHelper.DBReturnCode.DUPLICATEENTRY;
                    }
                    if (retCode == DBHelper.DBReturnCode.SUCCESS)
                    {
                        using (TransactionScope objTransactionScope = new TransactionScope())
                        {
                            SqlParameter[] sqlParams1 = new SqlParameter[6];
                            sqlParams1[0] = new SqlParameter("@uid", uid);
                            sqlParams1[1] = new SqlParameter("@AvailableCredit", @AvailableCredit);
                            sqlParams1[2] = new SqlParameter("@CreditAmount", @Creditlimit);
                            sqlParams1[3] = new SqlParameter("@MaxCreditLimit", @OTClimit);
                            sqlParams1[4] = new SqlParameter("@MaxCreditLimit_Cmp", @MAXCreditlimit_Cmp);
                            sqlParams1[5] = new SqlParameter("@OTC", @OTC);
                            retCode = DBHelper.ExecuteNonQuery("Proc_tbl_AdminCreditLimitUpdateByBooking", out rowsaffected, sqlParams1);
                            if (retCode == DBHelper.DBReturnCode.SUCCESS)
                            {
                                SqlParameter[] sqlParams = new SqlParameter[21];
                                sqlParams[0] = new SqlParameter("@uid", uid);
                                sqlParams[1] = new SqlParameter("@ReservationID", ReservationID);
                                sqlParams[2] = new SqlParameter("@TransactionDate", TransactionDate);
                                sqlParams[3] = new SqlParameter("@BookingAmt", BookingAmt);
                                sqlParams[4] = new SqlParameter("@SeviceTax", SeviceTax);
                                sqlParams[5] = new SqlParameter("@BookingAmtWithTax", BookingAmtWithTax);
                                sqlParams[6] = new SqlParameter("@CanAmt", CanAmount);
                                sqlParams[7] = new SqlParameter("@CanServiceTax", CanServiceTax);
                                sqlParams[8] = new SqlParameter("@CanAmtWithTax", CanAmtWithTax);
                                sqlParams[9] = new SqlParameter("@Balance", Balance);
                                sqlParams[10] = new SqlParameter("@SalesTax", SalesTax);
                                sqlParams[11] = new SqlParameter("@LuxuryTax", LuxuryTax);
                                sqlParams[12] = new SqlParameter("@DiscountPer", DiscountPer);
                                sqlParams[13] = new SqlParameter("@BookingCreationDate", BookingCreationDate);
                                sqlParams[14] = new SqlParameter("@BookingRemarks", BookingRemarks);
                                sqlParams[15] = new SqlParameter("@BookingCancelFlag", BookingCancelFlag);
                                sqlParams[16] = new SqlParameter("@Supplier", Supplier);
                                sqlParams[17] = new SqlParameter("@SupplierBasicAmt", SupplierBasicAmt);
                                sqlParams[18] = new SqlParameter("@AgentComm", AgentComm);
                                sqlParams[19] = new SqlParameter("@CUTComm", CUTComm);
                                sqlParams[20] = new SqlParameter("@BookedBy", BookedBy);
                                retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionAddAfterDeduction", out rowsaffected, sqlParams);
                                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                                    objTransactionScope.Complete();
                                else
                                    objTransactionScope.Dispose();

                            }
                            else
                            {
                                objTransactionScope.Dispose();
                            }
                        }

                    }
                    else
                        return retCode = DBHelper.DBReturnCode.EXCEPTION;
                }
                else
                {
                    return retCode;
                }


            }
            else
            {
                retCode = DBHelper.DBReturnCode.SUCCESS;
                //SqlParameter[] sqlParams = new SqlParameter[21];
                //sqlParams[0] = new SqlParameter("@uid", uid);
                //sqlParams[1] = new SqlParameter("@ReservationID", ReservationID);
                //sqlParams[2] = new SqlParameter("@TransactionDate", TransactionDate);
                //sqlParams[3] = new SqlParameter("@BookingAmt", BookingAmt);
                //sqlParams[4] = new SqlParameter("@SeviceTax", SeviceTax);
                //sqlParams[5] = new SqlParameter("@BookingAmtWithTax", BookingAmtWithTax);
                //sqlParams[6] = new SqlParameter("@CanAmt", CanAmount);
                //sqlParams[7] = new SqlParameter("@CanServiceTax", CanServiceTax);
                //sqlParams[8] = new SqlParameter("@CanAmtWithTax", CanAmtWithTax);
                //sqlParams[9] = new SqlParameter("@Balance", Balance);
                //sqlParams[10] = new SqlParameter("@SalesTax", SalesTax);
                //sqlParams[11] = new SqlParameter("@LuxuryTax", LuxuryTax);
                //sqlParams[12] = new SqlParameter("@DiscountPer", DiscountPer);
                //sqlParams[13] = new SqlParameter("@BookingCreationDate", BookingCreationDate);
                //sqlParams[14] = new SqlParameter("@BookingRemarks", BookingRemarks);
                //sqlParams[15] = new SqlParameter("@BookingCancelFlag", BookingCancelFlag);
                //sqlParams[16] = new SqlParameter("@Supplier", Supplier);
                //sqlParams[17] = new SqlParameter("@SupplierBasicAmt", SupplierBasicAmt);
                //sqlParams[18] = new SqlParameter("@AgentComm", AgentComm);
                //sqlParams[19] = new SqlParameter("@CUTComm", CUTComm);
                //sqlParams[20] = new SqlParameter("@BookedBy", BookedBy);
                //retCode = DBHelper.ExecuteNonQuery("Proc_tbl_BookingTransactionAddHold", out rowsaffected, sqlParams);
            }
            return retCode;
        }


        #endregion
    }
}