﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.dbml;
using CutAdmin.Flightdbml;
using CutAdmin.Models;
using System.Data;
using CutAdmin.EntityModal;
using Elmah;

namespace CutAdmin.DataLayer
{
    public class BookingListManager
    {
        #region Flight
        public class FlightDetails
        {
            public Int64 sid { get; set; }
            public string AgencyName { get; set; }
            public string LeadingPaxName { get; set; }
            public string AirlineName { get; set; }
            public string AirlineCode { get; set; }
            public string PNR { get; set; }
            public string Status { get; set; }
            public string InvoiceAmount { get; set; }
            public string PublishedFare { get; set; }
            public string TicketStatus { get; set; }
            public string BookingStatus { get; set; }
            public string InvoiceNo { get; set; }
            public string Paxces { get; set; }
            public string DestinationAirport { get; set; }
            public string OriginAirport { get; set; }
            public string DepartureDate { get; set; }
            public string BookingID { get; set; }
            public string uid { get; set; }

            public string Date { get; set; }

        }

        public static List<FlightDetails> GetBookings(out int TotalCount)
        {
            TotalCount = 0;
            var arrData = DTResult.arrTable;
            string search = DTResult.sSearch;

            string sortOrder = DTResult.sSortDir_0;
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;
            using (var db = new FlightHelperDataContext())
            {
                IQueryable<FlightDetails> result = from objAgency in db.tbl_AdminLogins
                                                   from objReser in db.tbl_AirReservations
                                                   where objAgency.ParentID == AccountManager.GetAdminByLogin() && objReser.uid == objAgency.sid
                                                   where (search == null
                                                   || (objAgency.AgencyName != "" && objAgency.AgencyName.ToLower().Contains(search.ToLower())
                                                   || objReser.PNR != null && objReser.PNR.ToLower().Contains(search.ToLower())
                                                   || objReser.LeadingPaxName != null && objReser.LeadingPaxName.ToLower().Contains(search.ToLower())
                                                   || objReser.AirlineName != null && objReser.AirlineName.ToLower().Contains(search.ToLower())
                                                   || objReser.DepartureDate != null && objReser.DepartureDate.ToLower().Contains(search.ToLower())
                                                   || objReser.BookingStatus != null && objReser.BookingStatus.ToLower().Contains(search.ToLower())
                                                   ))
                                                   select new FlightDetails
                                                   {
                                                       AgencyName = objAgency.AgencyName,
                                                       LeadingPaxName = objReser.LeadingPaxName,
                                                       AirlineName = objReser.AirlineName,
                                                       PNR = objReser.PNR,
                                                       InvoiceNo = objReser.InvoiceNo,
                                                       Status = objReser.BookingStatus,
                                                       InvoiceAmount = Convert.ToString(objReser.InvoiceAmount),
                                                       PublishedFare = Convert.ToString(objReser.PublishedFare),
                                                       DepartureDate = objReser.DepartureDate,
                                                       Paxces = (from objPax in db.tbl_AirPaxDetails where objPax.BookingID == objReser.BookingID select objPax).Count().ToString(),
                                                       //Paxces= (from objPax in db.tbl_AirPaxDetails
                                                       //         where objPax.BookingID == objReser.BookingID
                                                       //         select objPax.PaxTitle + " " + objPax.FirstName + " " + objPax.LastName).ToList().ToString(),
                                                       BookingID = objReser.BookingID,
                                                       uid = Convert.ToString(objReser.uid),
                                                       Date = Convert.ToString(objReser.InvoiceCreatedOn),
                                                       TicketStatus = objReser.TicketStatus,
                                                       // Date = objAgency.InvoiceCreatedOn
                                                       // DestinationAirport =(from objSeg in db.tbl_AirSegments where objSeg.BookingID == objReser.BookingID select objSeg.DestinationAirport).ToString(),
                                                       // OriginAirport = (from objSeg in db.tbl_AirSegments where objSeg.BookingID == objReser.BookingID select objSeg.OriginAirport).ToString(),
                                                   };

                TotalCount = result.Count();
                List<FlightDetails> arrResult = result.ToList();
                arrResult = arrResult.Skip(start).Take(length).ToList();
                int Sorting = DTResult.iSortingCols;
                if (Sorting == 1)
                {
                    string sortColumns = DTResult.iSortCol_0;
                    sortOrder = DTResult.sSortDir_0;
                    switch (sortColumns)
                    {
                        case "0":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Date).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Date).ToList();
                            break;
                        case "1":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.InvoiceNo).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.InvoiceNo).ToList();
                            break;
                        case "2":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.AgencyName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.AgencyName).ToList();
                            break;
                        case "3":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.PNR).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.PNR).ToList();
                            break;
                        case "4":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Paxces).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Paxces).ToList();
                            break;
                        case "5":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.AirlineName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.AirlineName).ToList();
                            break;
                        case "6":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.BookingStatus).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.BookingStatus).ToList();
                            break;
                        case "7":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.PublishedFare).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.PublishedFare).ToList();
                            break;
                        case "8":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.uid).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.uid).ToList();
                            break;
                    }
                }
                return arrResult;
            }

        }

        #region Flight Search
        public static List<FlightDetails> SearchFlight(string TransactionNo, string AgencyName, string Paxces, string TicketStatus, out int TotalCount)
        {
            TotalCount = 0;
            var arrData = DTResult.arrTable;
            string search = DTResult.sSearch;

            string sortOrder = DTResult.sSortDir_0;
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;
            using (var db = new FlightHelperDataContext())
            {
                IQueryable<FlightDetails> result = from objAgency in db.tbl_AdminLogins
                                                   from objReser in db.tbl_AirReservations
                                                   where objAgency.ParentID == AccountManager.GetAdminByLogin() && objReser.uid == objAgency.sid
                                                   where (search == null
                                                   || (objAgency.AgencyName != "" && objAgency.AgencyName.ToLower().Contains(search.ToLower())
                                                   || objReser.PNR != null && objReser.PNR.ToLower().Contains(search.ToLower())
                                                   || objReser.LeadingPaxName != null && objReser.LeadingPaxName.ToLower().Contains(search.ToLower())
                                                   || objReser.AirlineName != null && objReser.AirlineName.ToLower().Contains(search.ToLower())
                                                   || objReser.DepartureDate != null && objReser.DepartureDate.ToLower().Contains(search.ToLower())
                                                   || objReser.BookingStatus != null && objReser.BookingStatus.ToLower().Contains(search.ToLower())
                                                   || objReser.InvoiceNo != null && objReser.InvoiceNo.ToLower().Contains(search.ToLower())
                                                   ))
                                                   select new FlightDetails
                                                   {
                                                       AgencyName = objAgency.AgencyName,
                                                       LeadingPaxName = objReser.LeadingPaxName,
                                                       AirlineName = objReser.AirlineName,
                                                       PNR = objReser.PNR,
                                                       InvoiceNo = objReser.InvoiceNo,
                                                       Status = objReser.BookingStatus,
                                                       InvoiceAmount = Convert.ToString(objReser.InvoiceAmount),
                                                       PublishedFare = Convert.ToString(objReser.PublishedFare),
                                                       DepartureDate = objReser.DepartureDate,
                                                       Paxces = (from objPax in db.tbl_AirPaxDetails where objPax.BookingID == objReser.BookingID select objPax).Count().ToString(),
                                                       //Paxces= (from objPax in db.tbl_AirPaxDetails
                                                       //         where objPax.BookingID == objReser.BookingID
                                                       //         select objPax.PaxTitle + " " + objPax.FirstName + " " + objPax.LastName).ToList().ToString(),
                                                       BookingID = objReser.BookingID,
                                                       uid = Convert.ToString(objReser.uid),
                                                       Date = Convert.ToString(objReser.InvoiceCreatedOn),
                                                       TicketStatus = objReser.TicketStatus,
                                                       // Date = objAgency.InvoiceCreatedOn
                                                       // DestinationAirport =(from objSeg in db.tbl_AirSegments where objSeg.BookingID == objReser.BookingID select objSeg.DestinationAirport).ToString(),
                                                       // OriginAirport = (from objSeg in db.tbl_AirSegments where objSeg.BookingID == objReser.BookingID select objSeg.OriginAirport).ToString(),
                                                   };
                List<FlightDetails> arrResult = result.ToList();
                var arrFilter = new List<FlightDetails>();
                if (TransactionNo != "")
                {
                    arrFilter = arrResult.Where(item => item.InvoiceNo == TransactionNo).ToList();
                    arrResult = arrFilter;
                }

                if (AgencyName != "")
                {
                    arrFilter = arrResult.Where(item => item.AgencyName == AgencyName.ToString()).ToList();
                    arrResult = arrFilter;
                }

                if (Paxces != "")
                {
                    arrFilter = arrResult.Where(item => item.LeadingPaxName == Paxces).ToList();
                    arrResult = arrFilter;
                }

                if (TicketStatus != "")
                {
                    arrFilter = arrResult.Where(item => item.Status == TicketStatus).ToList();
                    arrResult = arrFilter;
                }

                TotalCount = arrResult.Count();
                arrResult = arrResult.Skip(start).Take(length).ToList();
                int Sorting = DTResult.iSortingCols;
                if (Sorting == 1)
                {
                    string sortColumns = DTResult.iSortCol_0;
                    sortOrder = DTResult.sSortDir_0;
                    switch (sortColumns)
                    {
                        case "0":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Date).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Date).ToList();
                            break;
                        case "1":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.InvoiceNo).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.InvoiceNo).ToList();
                            break;
                        case "2":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.AgencyName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.AgencyName).ToList();
                            break;
                        case "3":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.PNR).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.PNR).ToList();
                            break;
                        case "4":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Paxces).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Paxces).ToList();
                            break;
                        case "5":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.AirlineName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.AirlineName).ToList();
                            break;
                        case "6":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.BookingStatus).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.BookingStatus).ToList();
                            break;
                        case "7":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.PublishedFare).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.PublishedFare).ToList();
                            break;
                        case "8":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.uid).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.uid).ToList();
                            break;
                    }
                }
                return arrResult;
            }

        }
        #endregion
        #endregion

        #region Activity
        public class ActivityDetails
        {
            public Int64 sid { get; set; }
            public string AgencyName { get; set; }
            public string Status { get; set; }
            public string ActivityName { get; set; }
            public string City { get; set; }
            public string Country { get; set; }
            public string InvoiceNo { get; set; }
            public string VoucherNo { get; set; }
            public string PassengerName { get; set; }
            public string BookingDate { get; set; }
            public string Date { get; set; }
            public string TotalFare { get; set; }
            public string Adults { get; set; }
            public string Childs1 { get; set; }
            public string Childs2 { get; set; }
            public string Request_Id { get; set; }
            public string User_Id { get; set; }
            public string BookingType { get; set; }
        }

        //public static List<ActivityDetails> ActivityBookingList(out int TotalCount)
        //{
        //    TotalCount = 0;
        //    var arrData = DTResult.arrTable;
        //    string search = DTResult.sSearch;

        //    string sortOrder = DTResult.sSortDir_0;
        //    int start = DTResult.iDisplayStart;
        //    int length = DTResult.iDisplayLength;
        //    using (var db = new helperDataContext())
        //    {
        //        IQueryable<ActivityDetails> result = from obj in db.tbl_aeActivityMasters
        //                                             from objAct in db.tbl_aeActivityVouchers
        //                                             from objAdmin in db.tbl_AdminLogins
        //                                             where objAct.User_Id == objAdmin.sid.ToString() && obj.Sr_No == objAct.Act_Id
        //                                             where (search == null
        //                                             || (obj.Act_Name != null && obj.Act_Name.ToLower().Contains(search.ToLower())
        //                                            || objAdmin.AgencyName != null && objAdmin.AgencyName.ToString().ToLower().Contains(search.ToLower())
        //                                            || objAct.Passenger_Name != null && objAct.Passenger_Name.ToString().ToLower().Contains(search.ToLower())
        //                                            )) && obj.ParentID == AccountManager.GetAdminByLogin()
        //                                             select new ActivityDetails
        //                                             {
        //                                                 AgencyName = objAdmin.AgencyName,
        //                                                 ActivityName = obj.Act_Name,
        //                                                 PassengerName = objAct.Passenger_Name,
        //                                                 Status = objAct.Status,
        //                                                 BookingDate = objAct.BookDate,
        //                                                 InvoiceNo = objAct.Invoice_No,
        //                                                 VoucherNo = objAct.Voucher_No,
        //                                                 Adults = objAct.Adults,
        //                                                 Childs1 = objAct.Childs1,
        //                                                 Childs2 = objAct.Childs2,
        //                                                 TotalFare = objAct.Total.ToString(),
        //                                                 Request_Id = objAct.Request_Id,
        //                                                 User_Id = objAct.User_Id,
        //                                                 BookingType = objAct.BookingType,
        //                                                 Date = objAct.Date,
        //                                                 City = obj.City,
        //                                                 Country = obj.Country,
        //                                             };

        //        TotalCount = result.Count();
        //        List<ActivityDetails> arrResult = result.ToList();
        //        arrResult = arrResult.Skip(start).Take(length).ToList();
        //        int Sorting = DTResult.iSortingCols;
        //        if (Sorting == 1)
        //        {
        //            string sortColumns = DTResult.iSortCol_0;
        //            sortOrder = DTResult.sSortDir_0;
        //            switch (sortColumns)
        //            {
        //                case "0":
        //                    if (sortOrder == "asc")
        //                        arrResult = arrResult.OrderBy(a => a.BookingDate).ToList();
        //                    else
        //                        arrResult = arrResult.OrderByDescending(a => a.BookingDate).ToList();
        //                    break;
        //                case "1":
        //                    if (sortOrder == "asc")
        //                        arrResult = arrResult.OrderBy(a => a.Date).ToList();
        //                    else
        //                        arrResult = arrResult.OrderByDescending(a => a.Date).ToList();
        //                    break;
        //                case "2":
        //                    if (sortOrder == "asc")
        //                        arrResult = arrResult.OrderBy(a => a.AgencyName).ToList();
        //                    else
        //                        arrResult = arrResult.OrderByDescending(a => a.AgencyName).ToList();
        //                    break;
        //                case "3":
        //                    if (sortOrder == "asc")
        //                        arrResult = arrResult.OrderBy(a => a.ActivityName).ToList();
        //                    else
        //                        arrResult = arrResult.OrderByDescending(a => a.ActivityName).ToList();
        //                    break;
        //                case "4":
        //                    if (sortOrder == "asc")
        //                        arrResult = arrResult.OrderBy(a => a.Status).ToList();
        //                    else
        //                        arrResult = arrResult.OrderByDescending(a => a.Status).ToList();
        //                    break;
        //                case "5":
        //                    if (sortOrder == "asc")
        //                        arrResult = arrResult.OrderBy(a => a.TotalFare).ToList();
        //                    else
        //                        arrResult = arrResult.OrderByDescending(a => a.TotalFare).ToList();
        //                    break;
        //                case "6":
        //                    if (sortOrder == "asc")
        //                        arrResult = arrResult.OrderBy(a => a.User_Id).ToList();
        //                    else
        //                        arrResult = arrResult.OrderByDescending(a => a.User_Id).ToList();
        //                    break;
        //            }
        //        }
        //        return arrResult;
        //    }

        //}

        public static List<ActivityDetails> ActivityBookingList(out int TotalCount)
        {
            TotalCount = 0;
            var arrData = DTResult.arrTable;
            string search = DTResult.sSearch;
            Int64 ParentID = AccountManager.GetAdminByLogin();
            string sortOrder = DTResult.sSortDir_0;
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;
            helperDataContext sdb = new helperDataContext();
            //var Admin = sdb.tbl_AdminLogins.AsEnumerable();
            using (var db = new CUT_LIVE_UATSTEntities())
            {
                IQueryable<ActivityDetails> result = from objAct in db.tbl_SightseeingBooking
                                                     join obj in db.tbl_SightseeingMaster on objAct.Activity_Id equals obj.Activity_Id
                                                     where objAct.ParentID == ParentID && objAct.BookingType == "B2B"
                                                     select new ActivityDetails
                                                     {
                                                         //AgencyName = objAdmin.AgencyName,
                                                         sid = objAct.Sr_No,
                                                         ActivityName = obj.Act_Name,
                                                         PassengerName = objAct.Passenger_Name,
                                                         Status = objAct.Status,
                                                         BookingDate = objAct.Booking_Date,
                                                         InvoiceNo = objAct.Invoice_No,
                                                         VoucherNo = objAct.Voucher_No,
                                                         Adults = objAct.Adults.ToString(),
                                                         Childs1 = objAct.Child1.ToString(),
                                                         Childs2 = objAct.Child2.ToString(),
                                                         TotalFare = objAct.TotalAmount.ToString(),
                                                         Request_Id = objAct.Request_Id.ToString(),
                                                         User_Id = objAct.User_Id,
                                                         BookingType = objAct.BookingType,
                                                         Date = objAct.Sightseeing_Date,
                                                         City = obj.City,
                                                         Country = obj.Country,
                                                     };
                
                TotalCount = result.Count();
                List<ActivityDetails> arrResult = result.ToList().OrderByDescending(d => d.sid).ToList();
                if (arrResult.ToList().Count() != 0)
                {
                    foreach (var item in arrResult)
                    {
                        item.AgencyName = GetAgencyName(item.User_Id);
                    }
                }
                arrResult = arrResult.Skip(start).Take(length).ToList();
                int Sorting = DTResult.iSortingCols;
                if (Sorting == 1)
                {
                    string sortColumns = DTResult.iSortCol_0;
                    sortOrder = DTResult.sSortDir_0;
                    switch (sortColumns)
                    {
                        case "0":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.BookingDate).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.BookingDate).ToList();
                            break;
                        case "1":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Date).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Date).ToList();
                            break;
                        case "2":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.AgencyName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.AgencyName).ToList();
                            break;
                        case "3":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.ActivityName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.ActivityName).ToList();
                            break;
                        case "4":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Status).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Status).ToList();
                            break;
                        case "5":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.TotalFare).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.TotalFare).ToList();
                            break;
                        case "6":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.User_Id).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.User_Id).ToList();
                            break;
                    }
                }
                return arrResult;
            }

        }

        public static string GetAgencyName(string UserId)
        {
            string AgencyName = "";
            try
            {
                using (var db = new helperDataContext())
                {
                    AgencyName = (from obj in db.tbl_AdminLogins where obj.sid == Convert.ToInt64(UserId) select obj.AgencyName).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromContext(HttpContext.Current).Raise(ex);
            }
            return AgencyName;
        }

        #region Search Activity Booking
        public static List<ActivityDetails> SearchAct(string BookDate, string ActDate, string Passenger, string ActivityName, string Status, out int TotalCount)
        {


            TotalCount = 0;
            var arrData = DTResult.arrTable;
            string search = DTResult.sSearch;
            Int64 ParentID = AccountManager.GetAdminByLogin();
            string sortOrder = DTResult.sSortDir_0;
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;
            using (var db = new CUT_LIVE_UATSTEntities())
            {
                IQueryable<ActivityDetails> result = from objAct in db.tbl_SightseeingBooking
                                                     join obj in db.tbl_SightseeingMaster on objAct.Activity_Id equals obj.Activity_Id
                                                     where objAct.ParentID == ParentID && objAct.BookingType == "B2B"
                                                     select new ActivityDetails
                                                     {
                                                         //AgencyName = objAdmin.AgencyName,
                                                         ActivityName = obj.Act_Name,
                                                         PassengerName = objAct.Passenger_Name,
                                                         Status = objAct.Status,
                                                         BookingDate = objAct.Booking_Date,
                                                         InvoiceNo = objAct.Invoice_No,
                                                         VoucherNo = objAct.Voucher_No,
                                                         Adults = objAct.Adults.ToString(),
                                                         Childs1 = objAct.Child1.ToString(),
                                                         Childs2 = objAct.Child2.ToString(),
                                                         TotalFare = objAct.TotalAmount.ToString(),
                                                         Request_Id = objAct.Request_Id.ToString(),
                                                         User_Id = objAct.User_Id,
                                                         BookingType = objAct.BookingType,
                                                         Date = objAct.Sightseeing_Date,
                                                         City = obj.City,
                                                         Country = obj.Country,
                                                     };
                //   TotalCount = result.Count();
                List<ActivityDetails> arrResult = result.ToList();
                var arrFilter = new List<ActivityDetails>();
                if (arrResult.ToList().Count() != 0)
                {
                    foreach (var item in arrResult)
                    {
                        item.AgencyName = GetAgencyName(item.User_Id);
                    }
                }
                if (BookDate != "")
                {
                    arrFilter = arrResult.Where(item => item.BookingDate == BookDate).ToList();
                    arrResult = arrFilter;
                }

                if (ActDate != "")
                {
                    arrFilter = arrResult.Where(item => item.Date == ActDate.ToString()).ToList();
                    arrResult = arrFilter;
                }

                if (Passenger != "")
                {
                    arrFilter = arrResult.Where(item => item.AgencyName.Contains(Passenger)).ToList();
                    arrResult = arrFilter;
                }

                if (ActivityName != "")
                {
                    arrFilter = arrResult.Where(item => item.ActivityName.Contains(ActivityName)).ToList();
                    arrResult = arrFilter;
                }
                if (Status != "" && Status != "All")
                {
                    arrFilter = arrResult.Where(item => item.Status == Status).ToList();
                    arrResult = arrFilter;
                }
                TotalCount = arrResult.Count();
                arrResult = arrResult.Skip(start).Take(length).ToList();
                int Sorting = DTResult.iSortingCols;
                if (Sorting == 1)
                {
                    string sortColumns = DTResult.iSortCol_0;
                    sortOrder = DTResult.sSortDir_0;
                    switch (sortColumns)
                    {
                        case "0":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.BookingDate).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.BookingDate).ToList();
                            break;
                        case "1":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Date).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Date).ToList();
                            break;
                        case "2":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.AgencyName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.AgencyName).ToList();
                            break;
                        case "3":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.ActivityName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.ActivityName).ToList();
                            break;
                        case "4":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Status).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Status).ToList();
                            break;
                        case "5":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.TotalFare).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.TotalFare).ToList();
                            break;
                        case "6":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.User_Id).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.User_Id).ToList();
                            break;
                    }
                }
                return arrResult;
            }

        }
        #endregion
        #endregion

        #region Package Booking details
        public class PackageReservation
        {
            public string Supplier { get; set; }
            public string TravelDate { get; set; }
            public string PaxName { get; set; }
            public string PackageName { get; set; }
            public string CatID { get; set; }
            public string Location { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }

        }
        public static List<PackageReservation> GetPackageBooking(out int TotalCount)
        {
            TotalCount = 0;
            var arrData = DTResult.arrTable;
            string search = DTResult.sSearch;

            string sortOrder = DTResult.sSortDir_0;
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;
            List<string> arrCategory = new List<string> { "Standard", "Deluxe", "Premium", "Luxury" };
            using (var DB = new CUT_LIVE_UATSTEntities())
            {
                IQueryable<PackageReservation> List = from Reserve in DB.tbl_PackageReservation
                                                      join Package in DB.tbl_Package on Reserve.PackageID equals Package.nID
                                                      //join Agency in DB.tbl_AdminLogins on Reserve.uid equals Agency.sid
                                                      where (search == null
                                                       || (/*Agency.AgencyName != null && Agency.AgencyName.ToLower().Contains(search.ToLower()) ||*/
                                                        Reserve.TravelDate != null && Reserve.TravelDate.ToLower().Contains(search.ToLower())
                                                       || Package.sPackageName != null && Package.sPackageName.ToString().ToLower().Contains(search.ToLower())
                                                       || Package.sPackageDestination != null && Package.sPackageDestination.ToString().ToLower().Contains(search.ToLower())
                                                       || Reserve.LeadingPax != null && Reserve.LeadingPax.ToString().ToLower().Contains(search.ToLower())
                                                       )) && Reserve.ParentId == AccountManager.GetAdminByLogin()
                                                      select new PackageReservation
                                                      {
                                                          //Supplier = Agency.AgencyName,
                                                          TravelDate = Reserve.TravelDate,
                                                          PaxName = Reserve.LeadingPax,
                                                          PackageName = Package.sPackageName,
                                                          CatID = arrCategory[Convert.ToInt32(Reserve.CategoryId) - 1],
                                                          Location = Package.sPackageDestination,
                                                          StartDate = Reserve.StartFrom,
                                                          EndDate = Reserve.EndDate,
                                                      };
                TotalCount = List.Count();
                List<PackageReservation> PackageResult = List.ToList();
                PackageResult = PackageResult.Skip(start).Take(length).ToList();
                int Sorting = DTResult.iSortingCols;
                if (Sorting == 1)
                {
                    string sortColumns = DTResult.iSortCol_0;
                    sortOrder = DTResult.sSortDir_0;
                    switch (sortColumns)
                    {
                        case "0":
                            if (sortOrder == "asc")
                                PackageResult = PackageResult.OrderBy(a => a.Supplier).ToList();
                            else
                                PackageResult = PackageResult.OrderByDescending(a => a.Supplier).ToList();
                            break;
                        case "1":
                            if (sortOrder == "asc")
                                PackageResult = PackageResult.OrderBy(a => a.TravelDate).ToList();
                            else
                                PackageResult = PackageResult.OrderByDescending(a => a.TravelDate).ToList();
                            break;
                        case "2":
                            if (sortOrder == "asc")
                                PackageResult = PackageResult.OrderBy(a => a.PaxName).ToList();
                            else
                                PackageResult = PackageResult.OrderByDescending(a => a.PaxName).ToList();
                            break;
                        case "3":
                            if (sortOrder == "asc")
                                PackageResult = PackageResult.OrderBy(a => a.PackageName).ToList();
                            else
                                PackageResult = PackageResult.OrderByDescending(a => a.PackageName).ToList();
                            break;
                        case "4":
                            if (sortOrder == "asc")
                                PackageResult = PackageResult.OrderBy(a => a.CatID).ToList();
                            else
                                PackageResult = PackageResult.OrderByDescending(a => a.CatID).ToList();
                            break;
                        case "5":
                            if (sortOrder == "asc")
                                PackageResult = PackageResult.OrderBy(a => a.Location).ToList();
                            else
                                PackageResult = PackageResult.OrderByDescending(a => a.Location).ToList();
                            break;
                        case "6":
                            if (sortOrder == "asc")
                                PackageResult = PackageResult.OrderBy(a => a.StartDate).ToList();
                            else
                                PackageResult = PackageResult.OrderByDescending(a => a.StartDate).ToList();
                            break;
                        case "7":
                            if (sortOrder == "asc")
                                PackageResult = PackageResult.OrderBy(a => a.EndDate).ToList();
                            else
                                PackageResult = PackageResult.OrderByDescending(a => a.EndDate).ToList();
                            break;
                    }
                }
                return PackageResult;
            }

        }
        #region Search Package Booking
        public static List<PackageReservation> Search(string Agency, string PassengerName, string BookingDate, string PackageName, string PackageType, string Location, out int TotalCount)
        {


            TotalCount = 0;
            var arrData = DTResult.arrTable;
            string search = DTResult.sSearch;

            string sortOrder = DTResult.sSortDir_0;
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;
            List<string> arrCategory = new List<string> { "Standard", "Deluxe", "Premium", "Luxury" };
            using (var DB = new CUT_LIVE_UATSTEntities())
            {
                var sDB = new helperDataContext();
                var Admin = sDB.tbl_AdminLogins.AsEnumerable();
                IQueryable<PackageReservation> result = from Reserve in DB.tbl_PackageReservation
                                                        join Package in DB.tbl_Package on Reserve.PackageID equals Package.nID
                                                        join meta in Admin on Reserve.uid equals meta.sid
                                                        where (search == null
                                                        || (meta.AgencyName != null && meta.AgencyName.ToLower().Contains(search.ToLower())
                                                         || Reserve.TravelDate != null && Reserve.TravelDate.ToLower().Contains(search.ToLower())
                                                        || Package.sPackageName != null && Package.sPackageName.ToString().ToLower().Contains(search.ToLower())
                                                        || Package.sPackageDestination != null && Package.sPackageDestination.ToString().ToLower().Contains(search.ToLower())
                                                        || Reserve.LeadingPax != null && Reserve.LeadingPax.ToString().ToLower().Contains(search.ToLower())
                                                        )) && Reserve.ParentId == AccountManager.GetAdminByLogin()

                                                        select new PackageReservation
                                                        {
                                                            //Supplier = meta.AgencyName,
                                                            TravelDate = Reserve.TravelDate,
                                                            PaxName = Reserve.LeadingPax,
                                                            PackageName = Package.sPackageName,
                                                            CatID = arrCategory[Convert.ToInt32(Reserve.CategoryId) - 1],
                                                            Location = Package.sPackageDestination,
                                                            StartDate = Reserve.StartFrom,
                                                            EndDate = Reserve.EndDate,
                                                        };
                //   TotalCount = result.Count();
                List<PackageReservation> arrResult = result.ToList();
                var arrFilter = new List<PackageReservation>();
                if (Agency != "")
                {
                    arrFilter = arrResult.Where(item => item.Supplier == Agency).ToList();
                    arrResult = arrFilter;
                }

                if (PassengerName != "")
                {
                    arrFilter = arrResult.Where(item => item.PaxName == PassengerName.ToString()).ToList();
                    arrResult = arrFilter;
                }

                if (Location != "")
                {
                    arrFilter = arrResult.Where(item => item.Location == Location).ToList();
                    arrResult = arrFilter;
                }

                if (PackageName != "")
                {
                    arrFilter = arrResult.Where(item => item.PackageName == PackageName).ToList();
                    arrResult = arrFilter;
                }
                if (BookingDate != "")
                {
                    arrFilter = arrResult.Where(item => item.TravelDate == BookingDate).ToList();
                    arrResult = arrFilter;
                }
                TotalCount = arrResult.Count();
                arrResult = arrResult.Skip(start).Take(length).ToList();
                int Sorting = DTResult.iSortingCols;
                if (Sorting == 1)
                {
                    string sortColumns = DTResult.iSortCol_0;
                    sortOrder = DTResult.sSortDir_0;
                    switch (sortColumns)
                    {
                        case "0":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Supplier).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Supplier).ToList();
                            break;
                        case "1":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.TravelDate).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.TravelDate).ToList();
                            break;
                        case "2":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.PaxName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.PaxName).ToList();
                            break;
                        case "3":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.PackageName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.PackageName).ToList();
                            break;
                        case "4":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.CatID).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.CatID).ToList();
                            break;
                        case "5":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Location).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Location).ToList();
                            break;
                        case "6":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.StartDate).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.StartDate).ToList();
                            break;
                        case "7":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.EndDate).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.EndDate).ToList();
                            break;

                    }
                }
                return arrResult;
            }

        }

        #endregion
        #endregion

        #region Visalist
        public class VisaBooking
        {
            public string sid { get; set; }

            public string AgencyName { get; set; }

            public string FirstName { get; set; }

            public string PassportNo { get; set; }

            public string VisaCountry { get; set; }

            public string Status { get; set; }

            public string IeService { get; set; }

            public string ActiveStatus { get; set; }

            public string AppliedDate { get; set; }

            public string Sponsor
            {
                get;
                set;
            }
            public string VStatus { get; set; }

            public string History { get; set; }

            public string UserId { get; set; }

            public string EdnrdUserName { get; set; }

            public string TReference { get; set; }

            public string TotalAmount { get; set; }

            public string Vcode { get; set; }

            public string IssuingDate { get; set; }

        }

        public static List<VisaBooking> GetVisaBookings(out int TotalCount)
        {
            TotalCount = 0;
            var arrData = DTResult.arrTable;
            string search = DTResult.sSearch;

            string sortOrder = DTResult.sSortDir_0;
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;

            using (var DB = new helperDataContext())
            {
                IQueryable<VisaBooking> list = from Reserve in DB.tbl_VisaDetails
                                               join Agency in DB.tbl_AdminLogins on Reserve.UserId equals Agency.sid
                                               where (search == null
                                                || (Agency.AgencyName != null && Agency.AgencyName.ToLower().Contains(search.ToLower())
                                                || Reserve.FirstName != null && Reserve.FirstName.ToLower().Contains(search.ToLower())
                                                || Reserve.PassportNo != null && Reserve.PassportNo.ToString().ToLower().Contains(search.ToLower())
                                                || Reserve.VisaCountry != null && Reserve.VisaCountry.ToString().ToLower().Contains(search.ToLower())
                                                || Reserve.IeService != null && Reserve.IeService.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.Status != null && Reserve.Status.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.Vcode != null && Reserve.Vcode.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.AppliedDate != null && Reserve.Vcode.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.Sponsor != null && Reserve.Sponsor.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.VStatus != null && Reserve.Sponsor.ToString().ToLower().Contains(search.ToLower())
                                              )) && Agency.ParentID == AccountManager.GetAdminByLogin()
                                               select new VisaBooking
                                               {
                                                   sid = Convert.ToString(Reserve.sid),
                                                   AgencyName = Agency.AgencyName,
                                                   FirstName = Reserve.FirstName,
                                                   PassportNo = Reserve.PassportNo,
                                                   VisaCountry = Reserve.VisaCountry,
                                                   IeService = Reserve.IeService,
                                                   Status = Reserve.Status,
                                                   ActiveStatus = Reserve.ActiveStatus,
                                                   AppliedDate = Reserve.AppliedDate,
                                                   Sponsor = Reserve.Sponsor,
                                                   VStatus = Convert.ToString(Reserve.VStatus),
                                                   History = Reserve.History,
                                                   UserId = Convert.ToString(Reserve.UserId),
                                                   EdnrdUserName = Reserve.EdnrdUserName,
                                                   TReference = Reserve.TReference,
                                                   TotalAmount = Reserve.TotalAmount,
                                                   IssuingDate = Reserve.IssuingDate,
                                                   Vcode = Reserve.Vcode
                                               };
                TotalCount = list.OrderByDescending(x => x.AppliedDate).Count();
                List<VisaBooking> VisaResult = list.ToList();
                VisaResult = VisaResult.Skip(start).Take(length).ToList();
                int Sorting = DTResult.iSortingCols;
                if (Sorting == 1)
                {
                    string sortColumns = DTResult.iSortCol_0;
                    sortOrder = DTResult.sSortDir_0;
                    switch (sortColumns)
                    {
                        case "0":
                            if (sortOrder == "asc")
                                VisaResult = VisaResult.OrderBy(a => a.sid).ToList();
                            else
                                VisaResult = VisaResult.OrderByDescending(a => a.sid).ToList();
                            break;
                        case "1":
                            if (sortOrder == "asc")
                                VisaResult = VisaResult.OrderBy(a => a.AgencyName).ToList();
                            else
                                VisaResult = VisaResult.OrderByDescending(a => a.AgencyName).ToList();
                            break;
                        case "2":
                            if (sortOrder == "asc")
                                VisaResult = VisaResult.OrderBy(a => a.FirstName).ToList();
                            else
                                VisaResult = VisaResult.OrderByDescending(a => a.FirstName).ToList();
                            break;
                        case "3":
                            if (sortOrder == "asc")
                                VisaResult = VisaResult.OrderBy(a => a.PassportNo).ToList();
                            else
                                VisaResult = VisaResult.OrderByDescending(a => a.PassportNo).ToList();
                            break;
                        case "4":
                            if (sortOrder == "asc")
                                VisaResult = VisaResult.OrderBy(a => a.VisaCountry).ToList();
                            else
                                VisaResult = VisaResult.OrderByDescending(a => a.VisaCountry).ToList();
                            break;
                        case "5":
                            if (sortOrder == "asc")
                                VisaResult = VisaResult.OrderBy(a => a.IeService).ToList();
                            else
                                VisaResult = VisaResult.OrderByDescending(a => a.IeService).ToList();
                            break;
                        case "6":
                            if (sortOrder == "asc")
                                VisaResult = VisaResult.OrderBy(a => a.Status).ToList();
                            else
                                VisaResult = VisaResult.OrderByDescending(a => a.Status).ToList();
                            break;
                        case "7":
                            if (sortOrder == "asc")
                                VisaResult = VisaResult.OrderBy(a => a.AppliedDate).ToList();
                            else
                                VisaResult = VisaResult.OrderByDescending(a => a.AppliedDate).ToList();
                            break;
                        case "8":
                            if (sortOrder == "asc")
                                VisaResult = VisaResult.OrderBy(a => a.Sponsor).ToList();
                            else
                                VisaResult = VisaResult.OrderByDescending(a => a.Sponsor).ToList();
                            break;
                        case "9":
                            if (sortOrder == "asc")
                                VisaResult = VisaResult.OrderBy(a => a.ActiveStatus).ToList();
                            else
                                VisaResult = VisaResult.OrderByDescending(a => a.ActiveStatus).ToList();
                            break;
                    }
                }
                return VisaResult;

            }



        }
        #region Search By Visa
        public static List<VisaBooking> SearchVisa(string sid, string FirstName, string PassportNo, string Status, string IeService, string AgencyName, out int TotalCount)
        {
            TotalCount = 0;
            var arrData = DTResult.arrTable;
            string search = DTResult.sSearch;

            string sortOrder = DTResult.sSortDir_0;
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;
            using (var DB = new helperDataContext())
            {
                IQueryable<VisaBooking> list = from Reserve in DB.tbl_VisaDetails
                                               join Agency in DB.tbl_AdminLogins on Reserve.UserId equals Agency.sid
                                               where (search == null
                                                || (Agency.AgencyName != null && Agency.AgencyName.ToLower().Contains(search.ToLower())
                                                || Reserve.FirstName != null && Reserve.FirstName.ToLower().Contains(search.ToLower())
                                                || Reserve.PassportNo != null && Reserve.PassportNo.ToString().ToLower().Contains(search.ToLower())
                                                || Reserve.VisaCountry != null && Reserve.VisaCountry.ToString().ToLower().Contains(search.ToLower())
                                                || Reserve.IeService != null && Reserve.IeService.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.Status != null && Reserve.Status.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.Vcode != null && Reserve.Vcode.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.AppliedDate != null && Reserve.Vcode.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.Sponsor != null && Reserve.Sponsor.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.VStatus != null && Reserve.Sponsor.ToString().ToLower().Contains(search.ToLower())
                                              )) && Agency.ParentID == AccountManager.GetAdminByLogin()
                                               select new VisaBooking
                                               {
                                                   sid = Convert.ToString(Reserve.sid),
                                                   AgencyName = Agency.AgencyName,
                                                   FirstName = Reserve.FirstName,
                                                   PassportNo = Reserve.PassportNo,
                                                   VisaCountry = Reserve.VisaCountry,
                                                   IeService = Reserve.IeService,
                                                   Status = Reserve.Status,
                                                   ActiveStatus = Reserve.ActiveStatus,
                                                   AppliedDate = Reserve.AppliedDate,
                                                   Sponsor = Reserve.Sponsor,
                                                   VStatus = Convert.ToString(Reserve.VStatus),
                                                   History = Reserve.History,
                                                   UserId = Convert.ToString(Reserve.UserId),
                                                   EdnrdUserName = Reserve.EdnrdUserName,
                                                   TReference = Reserve.TReference,
                                                   TotalAmount = Reserve.TotalAmount,
                                                   Vcode = Reserve.Vcode
                                               };
                //   TotalCount = result.Count();
                List<VisaBooking> arrResult = list.ToList();
                var arrFilter = new List<VisaBooking>();
                if (sid != "")
                {
                    arrFilter = arrResult.Where(item => item.sid == sid).ToList();
                    arrResult = arrFilter;
                }

                if (FirstName != "")
                {
                    arrFilter = arrResult.Where(item => item.FirstName == FirstName.ToString()).ToList();
                    arrResult = arrFilter;
                }

                if (PassportNo != "")
                {
                    arrFilter = arrResult.Where(item => item.PassportNo == PassportNo).ToList();
                    arrResult = arrFilter;
                }

                if (Status != "")
                {
                    arrFilter = arrResult.Where(item => item.Status == Status).ToList();
                    arrResult = arrFilter;
                }
                if (IeService != "")
                {
                    arrFilter = arrResult.Where(item => item.IeService == IeService).ToList();
                    arrResult = arrFilter;
                }
                if (AgencyName != "")
                {
                    arrFilter = arrResult.Where(item => item.AgencyName == AgencyName).ToList();
                    arrResult = arrFilter;
                }
                TotalCount = arrResult.Count();
                // List<VisaBooking> VisaResult = list.OrderBy(x => x.sid).ToList();
                arrResult = arrResult.Skip(start).Take(length).ToList();
                int Sorting = DTResult.iSortingCols;
                if (Sorting == 1)
                {
                    string sortColumns = DTResult.iSortCol_0;
                    sortOrder = DTResult.sSortDir_0;
                    switch (sortColumns)
                    {
                        case "0":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.sid).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.sid).ToList();
                            break;
                        case "1":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.AgencyName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.AgencyName).ToList();
                            break;
                        case "2":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.FirstName).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.FirstName).ToList();
                            break;
                        case "3":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.PassportNo).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.PassportNo).ToList();
                            break;
                        case "4":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.VisaCountry).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.VisaCountry).ToList();
                            break;
                        case "5":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.IeService).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.IeService).ToList();
                            break;
                        case "6":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Status).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Status).ToList();
                            break;
                        case "7":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.AppliedDate).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.AppliedDate).ToList();
                            break;
                        case "8":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Sponsor).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Sponsor).ToList();
                            break;
                        case "9":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.ActiveStatus).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.ActiveStatus).ToList();
                            break;

                    }
                }
                return arrResult;
            }


        }


        #endregion



        #endregion

        #region OTBDetails
        public class OTBBooking
        {
            public string Date { get; set; }
            public string ReferNo { get; set; }

            public string OTBAgency { get; set; }

            public string VisaType { get; set; }

            public string TravelDate { get; set; }

            public string Airline { get; set; }

            public string supplier { get; set; }

            public string status { get; set; }

            public string DocNo { get; set; }

            public string Applicant { get; set; }

            public string uid { get; set; }
            public string Name { get; set; }

            public string num_pax { get; set; }
        }

        public static List<OTBBooking> SearchOTB(string Date, string ReferNo, string OTBAgency, string VisaType, string TravelDate, out int TotalCount)
        {
            TotalCount = 0;
            var arrData = DTResult.arrTable;
            string search = DTResult.sSearch;

            string sortOrder = DTResult.sSortDir_0;
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;
            using (var DB = new helperDataContext())
            {
                //Select * from tbl_OTB inner join tbl_AdminLogin on tbl_OTB.uid=tbl_AdminLogin.sid  where tbl_AdminLogin.ParentID=@ParentId ORDER BY tbl_OTB.sid DESC      

                IQueryable<OTBBooking> list = from objotb in DB.tbl_OTBs
                                              join Agency in DB.tbl_AdminLogins on objotb.uid equals Agency.sid
                                              where (search == null
                                               || (Agency.AgencyName != null && Agency.AgencyName.ToLower().Contains(search.ToLower())
                                               || objotb.LastUpdateDate != null && objotb.LastUpdateDate.ToLower().Contains(search.ToLower())
                                               || objotb.OTBNo != null && objotb.OTBNo.ToString().ToLower().Contains(search.ToLower())
                                               || objotb.Name != null && objotb.Name.ToString().ToLower().Contains(search.ToLower())
                                               || objotb.LastName != null && objotb.LastName.ToString().ToLower().Contains(search.ToLower())
                                             || objotb.VisaNo != null && objotb.VisaNo.ToString().ToLower().Contains(search.ToLower())
                                             || objotb.VisaRefNo != null && objotb.VisaRefNo.ToString().ToLower().Contains(search.ToLower())
                                             || objotb.In_AirLine != null && objotb.In_AirLine.ToString().ToLower().Contains(search.ToLower())
                                             || objotb.In_DepartureDt != null && objotb.In_DepartureDt.ToString().ToLower().Contains(search.ToLower())
                                             || objotb.OTBStatus != null && objotb.OTBStatus.ToString().ToLower().Contains(search.ToLower())
                                             )) && Agency.ParentID == AccountManager.GetAdminByLogin()
                                              select new OTBBooking
                                              {
                                                  Date = objotb.LastUpdateDate,
                                                  ReferNo = objotb.OTBNo,
                                                  OTBAgency = Agency.AgencyName,
                                                  VisaType = objotb.VisaType,
                                                  TravelDate = objotb.In_DepartureDt,
                                                  Airline = objotb.In_AirLine,
                                                  status = objotb.OTBStatus,
                                                  DocNo = objotb.VisaNo,
                                                  Applicant = objotb.Name + "" + objotb.LastName,
                                                  uid = Convert.ToString(objotb.uid)
                                              };
                List<OTBBooking> arrResult = list.ToList();
                var arrFilter = new List<OTBBooking>();
                if (Date != "")
                {
                    arrFilter = arrResult.Where(item => item.Date == Date).ToList();
                    arrResult = arrFilter;
                }

                if (ReferNo != "")
                {
                    arrFilter = arrResult.Where(item => item.ReferNo == ReferNo.ToString()).ToList();
                    arrResult = arrFilter;
                }

                if (OTBAgency != "")
                {
                    arrFilter = arrResult.Where(item => item.OTBAgency == OTBAgency).ToList();
                    arrResult = arrFilter;
                }

                if (VisaType != "")
                {
                    arrFilter = arrResult.Where(item => item.VisaType == VisaType).ToList();
                    arrResult = arrFilter;
                }
                if (TravelDate != "")
                {
                    arrFilter = arrResult.Where(item => item.TravelDate == TravelDate).ToList();
                    arrResult = arrFilter;
                }
                TotalCount = arrResult.Count();
                // List<VisaBooking> VisaResult = list.OrderBy(x => x.sid).ToList();
                arrResult = arrResult.Skip(start).Take(length).ToList();
                int Sorting = DTResult.iSortingCols;
                if (Sorting == 1)
                {
                    string sortColumns = DTResult.iSortCol_0;
                    sortOrder = DTResult.sSortDir_0;
                    switch (sortColumns)
                    {
                        case "0":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Date).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Date).ToList();
                            break;
                        case "1":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.ReferNo).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.ReferNo).ToList();
                            break;
                        case "2":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.OTBAgency).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.OTBAgency).ToList();
                            break;
                        case "3":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Applicant).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Applicant).ToList();
                            break;
                        case "4":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.DocNo).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.DocNo).ToList();
                            break;
                        case "5":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.VisaType).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.VisaType).ToList();
                            break;
                        case "6":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Airline).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Airline).ToList();
                            break;
                        case "7":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.TravelDate).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.TravelDate).ToList();
                            break;
                        case "8":
                            if (sortOrder == "asc")
                                arrResult = arrResult.OrderBy(a => a.Airline).ToList();
                            else
                                arrResult = arrResult.OrderByDescending(a => a.Airline).ToList();
                            break;

                    }
                }
                return arrResult;

            }

        }

        public static List<OTBBooking> GetOTBBookings(out int TotalCount)
        {
            TotalCount = 0;
            var arrData = DTResult.arrTable;
            string search = DTResult.sSearch;
            DataSet dsResult = new DataSet();
            string sortOrder = DTResult.sSortDir_0;
            int start = DTResult.iDisplayStart;
            int length = DTResult.iDisplayLength;
            using (var DB = new helperDataContext())
            {
                //Select * from tbl_OTB inner join tbl_AdminLogin on tbl_OTB.uid=tbl_AdminLogin.sid  where tbl_AdminLogin.ParentID=@ParentId ORDER BY tbl_OTB.sid DESC      

                IQueryable<OTBBooking> list = from objotb in DB.tbl_OTBs
                                              join Agency in DB.tbl_AdminLogins on objotb.uid equals Agency.sid
                                              where (search == null
                                               || (Agency.AgencyName != null && Agency.AgencyName.ToLower().Contains(search.ToLower())
                                               || objotb.LastUpdateDate != null && objotb.LastUpdateDate.ToLower().Contains(search.ToLower())
                                               || objotb.OTBNo != null && objotb.OTBNo.ToString().ToLower().Contains(search.ToLower())
                                               || objotb.Name != null && objotb.Name.ToString().ToLower().Contains(search.ToLower())
                                               || objotb.LastName != null && objotb.LastName.ToString().ToLower().Contains(search.ToLower())
                                             || objotb.VisaNo != null && objotb.VisaNo.ToString().ToLower().Contains(search.ToLower())
                                             || objotb.VisaRefNo != null && objotb.VisaRefNo.ToString().ToLower().Contains(search.ToLower())
                                             || objotb.In_AirLine != null && objotb.In_AirLine.ToString().ToLower().Contains(search.ToLower())
                                             || objotb.In_DepartureDt != null && objotb.In_DepartureDt.ToString().ToLower().Contains(search.ToLower())
                                             || objotb.OTBStatus != null && objotb.OTBStatus.ToString().ToLower().Contains(search.ToLower())
                                             )) && Agency.ParentID == AccountManager.GetAdminByLogin()
                                              select new OTBBooking
                                              {
                                                  Date = objotb.LastUpdateDate,
                                                  ReferNo = objotb.OTBNo,
                                                  OTBAgency = Agency.AgencyName,
                                                  VisaType = objotb.VisaType,
                                                  TravelDate = objotb.In_DepartureDt,
                                                  Airline = objotb.In_AirLine,
                                                  status = objotb.OTBStatus,
                                                  DocNo = objotb.VisaNo,
                                                  Name = objotb.Name,
                                                  Applicant = objotb.Name + "" + objotb.LastName,
                                                  uid = Convert.ToString(objotb.uid)
                                                  // num_pax=Convert.ToString(OTBManager.GetOtbPaxNo(dsResult)) 
                                              };
                TotalCount = list.Count();
                List<OTBBooking> OTBResult = list.ToList();
                OTBResult = OTBResult.Skip(start).Take(length).ToList();
                int Sorting = DTResult.iSortingCols;
                if (Sorting == 1)
                {
                    string sortColumns = DTResult.iSortCol_0;
                    sortOrder = DTResult.sSortDir_0;
                    switch (sortColumns)
                    {
                        case "0":
                            if (sortOrder == "asc")
                                OTBResult = OTBResult.OrderBy(a => a.Date).ToList();
                            else
                                OTBResult = OTBResult.OrderByDescending(a => a.Date).ToList();
                            break;
                        case "1":
                            if (sortOrder == "asc")
                                OTBResult = OTBResult.OrderBy(a => a.ReferNo).ToList();
                            else
                                OTBResult = OTBResult.OrderByDescending(a => a.ReferNo).ToList();
                            break;
                        case "2":
                            if (sortOrder == "asc")
                                OTBResult = OTBResult.OrderBy(a => a.OTBAgency).ToList();
                            else
                                OTBResult = OTBResult.OrderByDescending(a => a.OTBAgency).ToList();
                            break;
                        case "3":
                            if (sortOrder == "asc")
                                OTBResult = OTBResult.OrderBy(a => a.Applicant).ToList();
                            else
                                OTBResult = OTBResult.OrderByDescending(a => a.Applicant).ToList();
                            break;
                        case "4":
                            if (sortOrder == "asc")
                                OTBResult = OTBResult.OrderBy(a => a.DocNo).ToList();
                            else
                                OTBResult = OTBResult.OrderByDescending(a => a.DocNo).ToList();
                            break;
                        case "5":
                            if (sortOrder == "asc")
                                OTBResult = OTBResult.OrderBy(a => a.VisaType).ToList();
                            else
                                OTBResult = OTBResult.OrderByDescending(a => a.VisaType).ToList();
                            break;
                        case "6":
                            if (sortOrder == "asc")
                                OTBResult = OTBResult.OrderBy(a => a.Airline).ToList();
                            else
                                OTBResult = OTBResult.OrderByDescending(a => a.Airline).ToList();
                            break;
                        case "7":
                            if (sortOrder == "asc")
                                OTBResult = OTBResult.OrderBy(a => a.TravelDate).ToList();
                            else
                                OTBResult = OTBResult.OrderByDescending(a => a.TravelDate).ToList();
                            break;
                        case "8":
                            if (sortOrder == "asc")
                                OTBResult = OTBResult.OrderBy(a => a.Airline).ToList();
                            else
                                OTBResult = OTBResult.OrderByDescending(a => a.Airline).ToList();
                            break;

                    }
                }
                return OTBResult;

            }


        }


        #endregion

    }
}

