﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CutAdmin.EntityModal;
namespace CutAdmin.DataLayer
{
    public class CancellationManager
    {
        public static Click_Hotel  db { get; set; }
        public static CUT_LIVE_UATSTEntities sdb { get; set; }
        public static void SaveCancellation(List<tbl_CommonCancelationPolicy> arrCancellation)
        {
            db = new Click_Hotel();
            using (var trasction  = db.Database.BeginTransaction())
            {
                try
                {
                    foreach (var objCancellation in arrCancellation)
                    {
                        var arrData = db.tbl_CommonCancelationPolicy.Where(d => d.CancelationPolicy == objCancellation.CancelationPolicy && d.SupplierID == objCancellation.SupplierID).FirstOrDefault();
                        if (arrData == null)
                        {
                            db.tbl_CommonCancelationPolicy.AddRange(arrCancellation);
                            db.SaveChanges();
                        }
                        else
                            objCancellation.CancelationID = arrData.CancelationID;
                      
                    }
                    trasction.Commit();
                }
                catch (Exception)
                {
                    trasction.Dispose();
                }
            }
        }


        public static void SaveSightseeingCancellation(List<tbl_SightseeingCancellationPolicy> arrCancellation)
        {
            sdb = new CUT_LIVE_UATSTEntities();
            using (var trasction = sdb.Database.BeginTransaction())
            {
                try
                {
                    foreach (var objCancellation in arrCancellation)
                    {
                        var arrData = sdb.tbl_SightseeingCancellationPolicy.Where(d => d.CancelationPolicy == objCancellation.CancelationPolicy && d.SupplierID == objCancellation.SupplierID).FirstOrDefault();
                        if (arrData == null)
                        {
                            sdb.tbl_SightseeingCancellationPolicy.AddRange(arrCancellation);
                            sdb.SaveChanges();
                        }
                        else
                            objCancellation.CancelationID = arrData.CancelationID;

                    }
                    trasction.Commit();
                }
                catch (Exception)
                {
                    trasction.Dispose();
                }
            }
        }
    }
}
