﻿using CutAdmin.dbml;
using CutAdmin.EntityModal;
using CutAdmin.Flightdbml;
using CutAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static CutAdmin.DataLayer.BookingListManager;

namespace CutAdmin.DataLayer
{
    public class ExcelManager
    {
        #region Staff
        public class StaffDetails
        {
            public Int64 sid { get; set; }
            public string uid { get; set; }
            public string password { get; set; }
            public string Description { get; set; }
            public string Country { get; set; }
            public string ContactPerson { get; set; }
            public string Mobile { get; set; }
            public DateTime dtLastAccess { get; set; }
            public bool EnableCheckAccount { get; set; }
            public string LoginFlag { get; set; }
            public string CurrencyCode { get; set; }
            public Int64 ParentID { get; set; }
            public string agentCategory { get; set; }
            public string UserType { get; set; }
            public string Staffuniquecode { get; set; }
            public string Designation { get; set; }
        }
        public static string search { get; set; }

        public static List<StaffDetails> GetStaff(out int TotalCount)
        {

            if (search == null)
                search = "";
            List<StaffDetails> arrResult = new List<StaffDetails>();
            using (var db = new helperDataContext())
            {
                IQueryable<StaffDetails> result = from obj in db.tbl_StaffLogins
                                                  from objContact in db.tbl_Contacts
                                                  where obj.ContactID == objContact.ContactID
                                                  && (obj.UserType == "AdminStaff" || obj.UserType == "FranchiseeStaff")
                                                  where (search == null
                                                  || (obj.ContactPerson != null && obj.ContactPerson.ToLower().Contains(search.ToLower())
                                                  || obj.uid != null && obj.uid.ToLower().Contains(search.ToLower())
                                                  || obj.LoginFlag != null && obj.LoginFlag.ToString().ToLower().Contains(search.ToLower())
                                                  )) && obj.ParentId == AccountManager.GetAdminByLogin()
                                                  select new StaffDetails
                                                  {
                                                      sid = obj.sid,
                                                      uid = obj.uid,
                                                      password = obj.password,
                                                      Description = (from objCity in db.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Description,
                                                      Country = (from objCity in db.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Countryname,
                                                      ContactPerson = obj.ContactPerson,
                                                      Mobile = objContact.Mobile,
                                                      dtLastAccess = Convert.ToDateTime(obj.dtLastAccess),
                                                      LoginFlag = obj.LoginFlag.ToString(),
                                                      ParentID = Convert.ToInt64(obj.ParentId),
                                                      UserType = obj.UserType,
                                                      Staffuniquecode = obj.StaffUniqueCode,
                                                      Designation = obj.Designation
                                                  };
                TotalCount = result.Count();
                arrResult = result.ToList();
            }
            return arrResult;
        }
        #endregion

        #region Agent
        public class AgencyDetails
        {
            public Int64 sid { get; set; }
            public string AgencyName { get; set; }
            public string Agentuniquecode { get; set; }
            public string uid { get; set; }
            public string password { get; set; }
            public Decimal AvailableCredit { get; set; }
            public string GroupName { get; set; }
            public string Description { get; set; }
            public string Country { get; set; }
            public string ContactPerson { get; set; }
            public string Mobile { get; set; }
            public DateTime dtLastAccess { get; set; }
            public bool EnableCheckAccount { get; set; }
            public string LoginFlag { get; set; }
            public string CurrencyCode { get; set; }
            public Int64 ParentID { get; set; }
            public string agentCategory { get; set; }
            public string UserType { get; set; }
        }

        public static List<AgencyDetails> GetAgents(string Name, string Type, string Code, string Group, string Status, string Country, string City, string MinBalance, out int TotalCount)
        {
            bool UPStatus = false;
            if (Status == "0")
                UPStatus = false;
            if (Status == "1")
                UPStatus = true;

            TotalCount = 0;
            if (search == null)
                search = "";
            using (var db = new helperDataContext())
            {
                IQueryable<AgencyDetails> result = from obj in db.tbl_AdminLogins
                                                   from objContact in db.tbl_Contacts
                                                   from objCredit in db.tbl_AdminCreditLimits
                                                       // from ObjHcity in db.tbl_HCities
                                                   where obj.ContactID == objContact.ContactID &&
                                                   obj.sid == objCredit.uid && (obj.UserType == "Agent" || obj.UserType == "Franchisee")
                                                   where (search == null
                                                   || (obj.ContactPerson != null && obj.ContactPerson.ToLower().Contains(search.ToLower())
                                                   || obj.uid != null && obj.uid.ToLower().Contains(search.ToLower())
                                                   || obj.Agentuniquecode != null && obj.Agentuniquecode.ToString().ToLower().Contains(search.ToLower())
                                                   || obj.LoginFlag != null && obj.LoginFlag.ToString().ToLower().Contains(search.ToLower())
                                                   || objCredit.AvailableCredit != null && objCredit.AvailableCredit.ToString().ToLower().Contains(search.ToLower())
                                                   )) && obj.ParentID == AccountManager.GetAdminByLogin()

                                                   select new AgencyDetails
                                                   {
                                                       sid = obj.sid,
                                                       AgencyName = obj.AgencyName,
                                                       Agentuniquecode = obj.Agentuniquecode,
                                                       uid = obj.uid,
                                                       password = obj.password,
                                                       AvailableCredit = Convert.ToDecimal(objCredit.AvailableCredit),
                                                       GroupName = "Group A",
                                                       Description = (from objCity in db.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Description,
                                                       Country = (from objCity in db.tbl_HCities where objCity.Code == objContact.Code select objCity).FirstOrDefault().Countryname,
                                                       ContactPerson = obj.ContactPerson,
                                                       Mobile = objContact.Mobile,
                                                       dtLastAccess = Convert.ToDateTime(obj.dtLastAccess),
                                                       EnableCheckAccount = Convert.ToBoolean(obj.EnableCheckAccount),
                                                       LoginFlag = obj.LoginFlag.ToString(),
                                                       CurrencyCode = obj.CurrencyCode,
                                                       ParentID = Convert.ToInt64(obj.ParentID),
                                                       agentCategory = obj.agentCategory,
                                                       UserType = obj.UserType
                                                   };
                //   TotalCount = result.Count();
                List<AgencyDetails> arrResult = result.ToList();
                var arrFilter = new List<AgencyDetails>();
                if (Code != "")
                {
                    arrFilter = arrResult.Where(item => item.Agentuniquecode == Code).ToList();
                    arrResult = arrFilter;
                }

                if (Name != "")
                {
                    arrFilter = arrResult.Where(item => item.uid == Name).ToList();
                    arrResult = arrFilter;
                }

                if (MinBalance != "")
                {
                    arrFilter = arrResult.Where(item => item.AvailableCredit >= Convert.ToDecimal(MinBalance)).ToList();
                    arrResult = arrFilter;
                }

                if (Status != "")
                {
                    arrFilter = arrResult.Where(item => item.LoginFlag == UPStatus.ToString()).ToList();
                    arrResult = arrFilter;
                }

                if (Country != "Select Any Country")
                {
                    arrFilter = arrResult.Where(item => item.Country == Country).ToList();
                    arrResult = arrFilter;
                }

                if (City != "Select Any City")
                {
                    arrFilter = arrResult.Where(item => item.Description == City).ToList();
                    arrResult = arrFilter;
                }
                TotalCount = arrResult.Count();
                return arrResult;
            }

        }
        #endregion

        #region Booking
        #region Hotel


        public class Reservations
        {
            public int Sid { get; set; }
            public string ReservationID { get; set; }
            public string ReservationDate { get; set; }
            public string AgencyName { get; set; }
            public string Status { get; set; }
            public string BookStatus { get; set; }
            public string bookingname { get; set; }
            public decimal? TotalFare { get; set; }
            public int TotalRooms { get; set; }
            public string HotelName { get; set; }
            public string sCheckIn { get; set; }
            public string CheckIn { get; set; }
            public string CheckOut { get; set; }
            public string City { get; set; }
            public int? Children { get; set; }
            public string BookingStatus { get; set; }
            public int? NoOfAdults { get; set; }
            public string Source { get; set; }
            public Int64 Uid { get; set; }
            public string LatitudeMGH { get; set; }
            public string LongitudeMGH { get; set; }
            public string CurrencyCode { get; set; }
            public bool? IsConfirm { get; set; }
            public bool? isReconfirm { get; set; }
            public string HotelConfirmID { get; set; }
            public string Deadline { get; set; }
        }

        public static List<Reservations> GetSearchtBooking(string CheckIn, string CheckOut, string Passenger, string BookingDate, string Reference, string HotelName, string Location, string ReservationStatus, out int TotalCount)
        {
            if (search == null)
                search = "";
            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            Int64 Uid = AccountManager.GetAdminByLogin();
            List<Reservations> ListReservation = new List<Reservations>();
            List<Reservations> arrResult = new List<Reservations>();
            TotalCount = 0;
            using (var db = new CutAdmin.dbml.helperDataContext())
            {
                IQueryable<Reservations> result = (from obj in db.tbl_HotelReservations
                                                   join AgName in db.tbl_AdminLogins on obj.Uid equals AgName.sid
                                                   where (search == null
                                                   || (obj.ReservationDate != null && obj.ReservationDate.ToLower().Contains(search.ToLower())
                                                   || AgName.AgencyName != null && obj.AgencyName.ToLower().Contains(search.ToLower())
                                                   || obj.bookingname != null && obj.bookingname.ToString().ToLower().Contains(search.ToLower())
                                                   || obj.HotelName != null && obj.HotelName.ToString().ToLower().Contains(search.ToLower())
                                                   || obj.CheckIn != null && obj.CheckIn.ToString().ToLower().Contains(search.ToLower())
                                                   || obj.TotalRooms != null && obj.TotalRooms.ToString().ToLower().Contains(search.ToLower())
                                                   || obj.TotalFare != null && obj.TotalFare.ToString().ToLower().Contains(search.ToLower())))
                                                   && AgName.ParentID == Uid
                                                   select new Reservations
                                                   {
                                                       AgencyName = AgName.AgencyName,
                                                       bookingname = obj.bookingname,
                                                       BookingStatus = obj.BookingStatus,
                                                       CurrencyCode = AgName.CurrencyCode,
                                                       IsConfirm = obj.IsConfirm,
                                                       CheckIn = obj.CheckIn,
                                                       sCheckIn = obj.CheckIn,
                                                       CheckOut = obj.CheckOut,
                                                       Children = obj.Children,
                                                       City = obj.City,
                                                       HotelName = obj.HotelName,
                                                       LatitudeMGH = obj.LatitudeMGH,
                                                       LongitudeMGH = obj.LongitudeMGH,
                                                       NoOfAdults = obj.NoOfAdults,
                                                       ReservationDate = obj.ReservationDate,
                                                       ReservationID = obj.ReservationID,
                                                       Sid = obj.Sid,
                                                       Source = obj.Source != AccountManager.GetSuperAdminID().ToString() ? obj.Source : objGlobalDefault.AgencyName,
                                                       Status = obj.Status,
                                                       BookStatus = obj.BookingStatus,
                                                       TotalFare = obj.TotalFare,
                                                       TotalRooms = Convert.ToInt16(obj.TotalRooms),
                                                       Uid = Convert.ToInt64(obj.Uid),
                                                       isReconfirm = obj.HotelConfirmationNo != "",
                                                       HotelConfirmID = obj.HotelConfirmationNo,
                                                       Deadline = obj.DeadLine
                                                   });
                ListReservation = result.ToList();
                arrResult = result.ToList();
                var arrFilter = new List<Reservations>();
                if (CheckIn != "")
                {
                    arrFilter = arrResult.Where(item => item.CheckIn == CheckIn).ToList();
                    arrResult = arrFilter;
                }

                if (CheckOut != "")
                {
                    arrFilter = arrResult.Where(item => item.CheckOut == CheckOut).ToList();
                    arrResult = arrFilter;
                }

                if (Passenger != "")
                {
                    arrFilter = arrResult.Where(item => item.bookingname == Passenger).ToList();
                    arrResult = arrFilter;
                }

                if (BookingDate != "")
                {
                    arrFilter = arrResult.Where(item => item.ReservationDate == BookingDate).ToList();
                    arrResult = arrFilter;
                }

                if (Reference != "")
                {
                    arrFilter = arrResult.Where(item => item.ReservationID == Reference).ToList();
                    arrResult = arrFilter;
                }

                if (HotelName != "")
                {
                    arrFilter = arrResult.Where(item => item.HotelName == HotelName).ToList();
                    arrResult = arrFilter;
                }

                if (Location != "")
                {
                    arrFilter = arrResult.Where(item => item.City == Location).ToList();
                    arrResult = arrFilter;
                }

                if (ReservationStatus != "" && ReservationStatus != "All")
                {
                    arrFilter = arrResult.Where(item => item.Status == ReservationStatus).ToList();
                    arrResult = arrFilter;
                }
                TotalCount = arrResult.Count();
            }
            return arrResult;
        }
        #endregion
        #region Activity
        public class ActivityDetails
        {
            public Int64 sid { get; set; }
            public string AgencyName { get; set; }
            public string Status { get; set; }
            public string ActivityName { get; set; }
            public string City { get; set; }
            public string Country { get; set; }
            public string InvoiceNo { get; set; }
            public string VoucherNo { get; set; }
            public string PassengerName { get; set; }
            public string BookingDate { get; set; }
            public string Date { get; set; }
            public string TotalFare { get; set; }
            public string Adults { get; set; }
            public string Childs1 { get; set; }
            public string Childs2 { get; set; }
            public string Request_Id { get; set; }
            public string User_Id { get; set; }
            public string BookingType { get; set; }
        }
        public static List<ActivityDetails> SearchAct(string BookDate, string ActDate, string Passenger, string ActivityName, string Status, out int TotalCount)
        {
            if (search == null)
                search = "";
            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            List<ActivityDetails> ListActivityDetails = new List<ActivityDetails>();
            List<ActivityDetails> arrResult = new List<ActivityDetails>();
            TotalCount = 0;
            Int64 ParentID = AccountManager.GetAdminByLogin();
            using (var db = new CUT_LIVE_UATSTEntities())
            {
                IQueryable<ActivityDetails> result = from objAct in db.tbl_SightseeingBooking
                                                     join obj in db.tbl_SightseeingMaster on objAct.Activity_Id equals obj.Activity_Id
                                                     where objAct.ParentID == ParentID && objAct.BookingType == "B2B"
                                                     select new ActivityDetails
                                                     {
                                                         //AgencyName = objAdmin.AgencyName,
                                                         ActivityName = obj.Act_Name,
                                                         PassengerName = objAct.Passenger_Name,
                                                         Status = objAct.Status,
                                                         BookingDate = objAct.Booking_Date,
                                                         InvoiceNo = objAct.Invoice_No,
                                                         VoucherNo = objAct.Voucher_No,
                                                         Adults = objAct.Adults.ToString(),
                                                         Childs1 = objAct.Child1.ToString(),
                                                         Childs2 = objAct.Child2.ToString(),
                                                         TotalFare = objAct.TotalAmount.ToString(),
                                                         Request_Id = objAct.Request_Id.ToString(),
                                                         User_Id = objAct.User_Id,
                                                         BookingType = objAct.BookingType,
                                                         Date = objAct.Sightseeing_Date,
                                                         City = obj.City,
                                                         Country = obj.Country,
                                                     };
                ListActivityDetails = result.ToList();
                arrResult = result.ToList();
                var arrFilter = new List<ActivityDetails>();
                if (arrResult.ToList().Count() != 0)
                {
                    foreach (var item in arrResult)
                    {
                        item.AgencyName = GetAgencyName(item.User_Id);
                    }
                }
                if (BookDate != "")
                {
                    arrFilter = arrResult.Where(item => item.BookingDate == BookDate).ToList();
                    arrResult = arrFilter;
                }

                if (ActDate != "")
                {
                    arrFilter = arrResult.Where(item => item.Date == ActDate.ToString()).ToList();
                    arrResult = arrFilter;
                }

                if (Passenger != "")
                {
                    arrFilter = arrResult.Where(item => item.AgencyName == Passenger).ToList();
                    arrResult = arrFilter;
                }

                if (ActivityName != "")
                {
                    arrFilter = arrResult.Where(item => item.ActivityName == ActivityName).ToList();
                    arrResult = arrFilter;
                }
                if (Status != "" && Status != "All")
                {
                    arrFilter = arrResult.Where(item => item.Status == Status).ToList();
                    arrResult = arrFilter;
                }
                TotalCount = arrResult.Count();
            }
            return arrResult;
        }


        #endregion
        #region Package
        public class PackageReservation
        {
            public string Supplier { get; set; }
            public string TravelDate { get; set; }
            public string PaxName { get; set; }
            public string PackageName { get; set; }
            public string CatID { get; set; }
            public string Location { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }

        }
        #region Search Package Booking
        public static List<PackageReservation> Search(string Agency, string PassengerName, string BookingDate, string PackageName, string PackageType, string Location, out int TotalCount)
        {

            if (search == null)
                search = "";
            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            Int64 Uid = AccountManager.GetAdminByLogin();
            List<PackageReservation> ListPackage = new List<PackageReservation>();
            List<PackageReservation> arrResult = new List<PackageReservation>();
            TotalCount = 0;
            //
            List<string> arrCategory = new List<string> { "Standard", "Deluxe", "Premium", "Luxury" };
            using (var DB = new CUT_LIVE_UATSTEntities())
            {
                var sDB = new helperDataContext();
                var Admin = sDB.tbl_AdminLogins.AsEnumerable();
                IQueryable<PackageReservation> result = from Reserve in DB.tbl_PackageReservation
                                                        join Package in DB.tbl_Package on Reserve.PackageID equals Package.nID
                                                        join meta in Admin on Reserve.uid equals meta.sid
                                                        where (search == null
                                                        || (meta.AgencyName != null && meta.AgencyName.ToLower().Contains(search.ToLower())
                                                       || Reserve.TravelDate != null && Reserve.TravelDate.ToLower().Contains(search.ToLower())
                                                        || Package.sPackageName != null && Package.sPackageName.ToString().ToLower().Contains(search.ToLower())
                                                        || Package.sPackageDestination != null && Package.sPackageDestination.ToString().ToLower().Contains(search.ToLower())
                                                        || Reserve.LeadingPax != null && Reserve.LeadingPax.ToString().ToLower().Contains(search.ToLower())
                                                        )) && Reserve.ParentId == AccountManager.GetAdminByLogin()

                                                        select new PackageReservation
                                                        {
                                                            //Supplier = meta.AgencyName,
                                                            TravelDate = Reserve.TravelDate,
                                                            PaxName = Reserve.LeadingPax,
                                                            PackageName = Package.sPackageName,
                                                            CatID = arrCategory[Convert.ToInt32(Reserve.CategoryId) - 1],
                                                            Location = Package.sPackageDestination,
                                                            StartDate = Reserve.StartFrom,
                                                            EndDate = Reserve.EndDate,
                                                        };
                //   TotalCount = result.Count();
                ListPackage = result.ToList();
                arrResult = result.ToList();
                var arrFilter = new List<PackageReservation>();
                if (Agency != "")
                {
                    arrFilter = arrResult.Where(item => item.Supplier == Agency).ToList();
                    arrResult = arrFilter;
                }

                if (PassengerName != "")
                {
                    arrFilter = arrResult.Where(item => item.PaxName == PassengerName.ToString()).ToList();
                    arrResult = arrFilter;
                }

                if (Location != "")
                {
                    arrFilter = arrResult.Where(item => item.Location == Location).ToList();
                    arrResult = arrFilter;
                }

                if (PackageName != "")
                {
                    arrFilter = arrResult.Where(item => item.PackageName == PackageName).ToList();
                    arrResult = arrFilter;
                }
                if (BookingDate != "")
                {
                    arrFilter = arrResult.Where(item => item.TravelDate == BookingDate).ToList();
                    arrResult = arrFilter;
                }
                TotalCount = arrResult.Count();
            }

            return arrResult;
        }



        #endregion
        #endregion
        #region Visa
        public class VisaBooking
        {
            public string sid { get; set; }

            public string AgencyName { get; set; }

            public string FirstName { get; set; }

            public string PassportNo { get; set; }

            public string VisaCountry { get; set; }

            public string Status { get; set; }

            public string IeService { get; set; }

            public string ActiveStatus { get; set; }

            public string AppliedDate { get; set; }

            public string Sponsor
            {
                get;
                set;
            }
            public string VStatus { get; set; }

            public string History { get; set; }

            public string UserId { get; set; }

            public string EdnrdUserName { get; set; }

            public string TReference { get; set; }

            public string TotalAmount { get; set; }

            public string Vcode { get; set; }

            public string IssuingDate { get; set; }

        }

        #region Search By Visa
        public static List<VisaBooking> SearchVisa(string sid, string FirstName, string PassportNo, string Status, string IeService, string AgencyName, out int TotalCount)
        {
            if (search == null)
                search = "";
            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            Int64 Uid = AccountManager.GetAdminByLogin();
            List<VisaBooking> ListVisa = new List<VisaBooking>();
            List<VisaBooking> arrResult = new List<VisaBooking>();
            TotalCount = 0;
            using (var DB = new helperDataContext())
            {
                IQueryable<VisaBooking> list = from Reserve in DB.tbl_VisaDetails
                                               join Agency in DB.tbl_AdminLogins on Reserve.UserId equals Agency.sid
                                               where (search == null
                                                || (Agency.AgencyName != null && Agency.AgencyName.ToLower().Contains(search.ToLower())
                                                || Reserve.FirstName != null && Reserve.FirstName.ToLower().Contains(search.ToLower())
                                                || Reserve.PassportNo != null && Reserve.PassportNo.ToString().ToLower().Contains(search.ToLower())
                                                || Reserve.VisaCountry != null && Reserve.VisaCountry.ToString().ToLower().Contains(search.ToLower())
                                                || Reserve.IeService != null && Reserve.IeService.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.Status != null && Reserve.Status.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.Vcode != null && Reserve.Vcode.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.AppliedDate != null && Reserve.Vcode.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.Sponsor != null && Reserve.Sponsor.ToString().ToLower().Contains(search.ToLower())
                                              || Reserve.VStatus != null && Reserve.Sponsor.ToString().ToLower().Contains(search.ToLower())
                                              )) && Agency.ParentID == AccountManager.GetAdminByLogin()
                                               select new VisaBooking
                                               {
                                                   sid = Convert.ToString(Reserve.sid),
                                                   AgencyName = Agency.AgencyName,
                                                   FirstName = Reserve.FirstName,
                                                   PassportNo = Reserve.PassportNo,
                                                   VisaCountry = Reserve.VisaCountry,
                                                   IeService = Reserve.IeService,
                                                   Status = Reserve.Status,
                                                   ActiveStatus = Reserve.ActiveStatus,
                                                   AppliedDate = Reserve.AppliedDate,
                                                   Sponsor = Reserve.Sponsor,
                                                   VStatus = Convert.ToString(Reserve.VStatus),
                                                   History = Reserve.History,
                                                   UserId = Convert.ToString(Reserve.UserId),
                                                   EdnrdUserName = Reserve.EdnrdUserName,
                                                   TReference = Reserve.TReference,
                                                   TotalAmount = Reserve.TotalAmount,
                                                   Vcode = Reserve.Vcode
                                               };
                ListVisa = list.ToList();
                arrResult = list.ToList();
                var arrFilter = new List<VisaBooking>();
                if (sid != "")
                {
                    arrFilter = arrResult.Where(item => item.sid == sid).ToList();
                    arrResult = arrFilter;
                }

                if (FirstName != "")
                {
                    arrFilter = arrResult.Where(item => item.FirstName == FirstName.ToString()).ToList();
                    arrResult = arrFilter;
                }

                if (PassportNo != "")
                {
                    arrFilter = arrResult.Where(item => item.PassportNo == PassportNo).ToList();
                    arrResult = arrFilter;
                }

                if (Status != "")
                {
                    arrFilter = arrResult.Where(item => item.Status == Status).ToList();
                    arrResult = arrFilter;
                }
                if (IeService != "")
                {
                    arrFilter = arrResult.Where(item => item.IeService == IeService).ToList();
                    arrResult = arrFilter;
                }
                if (AgencyName != "")
                {
                    arrFilter = arrResult.Where(item => item.AgencyName == AgencyName).ToList();
                    arrResult = arrFilter;
                }
                TotalCount = arrResult.Count();
                return arrResult;

            }
           

        }


        #endregion
        #endregion

        #region Flight
        public static List<FlightDetails> SearchFlight(string InvoiceNo, string AgencyName, string Ticket, string TicketStatus, out int TotalCount)
        {
            if (search == null)
                search = "";
            CUT.DataLayer.GlobalDefault objGlobalDefault = (CUT.DataLayer.GlobalDefault)AccountManager.GetContext().Session["LoginUser"];
            Int64 Uid = AccountManager.GetAdminByLogin();
            List<FlightDetails> ListFlight = new List<FlightDetails>();
            List<FlightDetails> arrResult = new List<FlightDetails>();
            TotalCount = 0;
            using (var db = new FlightHelperDataContext())
            {
                IQueryable<FlightDetails> result = from objAgency in db.tbl_AdminLogins
                                                   from objReser in db.tbl_AirReservations
                                                   where objAgency.ParentID == AccountManager.GetAdminByLogin() && objReser.uid == objAgency.sid
                                                   where (search == null
                                                   || (objAgency.AgencyName != "" && objAgency.AgencyName.ToLower().Contains(search.ToLower())
                                                   || objReser.PNR != null && objReser.PNR.ToLower().Contains(search.ToLower())
                                                   || objReser.LeadingPaxName != null && objReser.LeadingPaxName.ToLower().Contains(search.ToLower())
                                                   || objReser.AirlineName != null && objReser.AirlineName.ToLower().Contains(search.ToLower())
                                                   || objReser.DepartureDate != null && objReser.DepartureDate.ToLower().Contains(search.ToLower())
                                                   || objReser.BookingStatus != null && objReser.BookingStatus.ToLower().Contains(search.ToLower())
                                                   || objReser.InvoiceNo != null && objReser.InvoiceNo.ToLower().Contains(search.ToLower())
                                                   ))
                                                   select new FlightDetails
                                                   {
                                                       AgencyName = objAgency.AgencyName,
                                                       LeadingPaxName = objReser.LeadingPaxName,
                                                       AirlineName = objReser.AirlineName,
                                                       PNR = objReser.PNR,
                                                       InvoiceNo = objReser.InvoiceNo,
                                                       Status = objReser.BookingStatus,
                                                       InvoiceAmount = Convert.ToString(objReser.InvoiceAmount),
                                                       PublishedFare = Convert.ToString(objReser.PublishedFare),
                                                       DepartureDate = objReser.DepartureDate,
                                                       Paxces = (from objPax in db.tbl_AirPaxDetails where objPax.BookingID == objReser.BookingID select objPax).Count().ToString(),
                                                       //Paxces= (from objPax in db.tbl_AirPaxDetails
                                                       //         where objPax.BookingID == objReser.BookingID
                                                       //         select objPax.PaxTitle + " " + objPax.FirstName + " " + objPax.LastName).ToList().ToString(),
                                                       BookingID = objReser.BookingID,
                                                       uid = Convert.ToString(objReser.uid),
                                                       Date = Convert.ToString(objReser.InvoiceCreatedOn),
                                                       TicketStatus = objReser.TicketStatus,
                                                       // Date = objAgency.InvoiceCreatedOn
                                                       // DestinationAirport =(from objSeg in db.tbl_AirSegments where objSeg.BookingID == objReser.BookingID select objSeg.DestinationAirport).ToString(),
                                                       // OriginAirport = (from objSeg in db.tbl_AirSegments where objSeg.BookingID == objReser.BookingID select objSeg.OriginAirport).ToString(),
                                                   };
                ListFlight = result.ToList();
                arrResult = result.ToList();
                var arrFilter = new List<FlightDetails>();

                if (AgencyName != "")
                {
                    arrFilter = arrResult.Where(item => item.AgencyName == AgencyName.ToString()).ToList();
                    arrResult = arrFilter;
                }

                if (Ticket != "")
                {
                    arrFilter = arrResult.Where(item => item.PNR == Ticket).ToList();
                    arrResult = arrFilter;
                }

                if (TicketStatus != "")
                {
                    arrFilter = arrResult.Where(item => item.TicketStatus == TicketStatus).ToList();
                    arrResult = arrFilter;
                }
                TotalCount = arrResult.Count();
                return arrResult;
            }

        }



        #endregion



        #endregion
    }
}
