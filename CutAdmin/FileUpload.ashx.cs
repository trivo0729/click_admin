﻿using CutAdmin.BL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CutAdmin
{
    /// <summary>
    /// Summary description for FileUpload
    /// </summary>
    public class FileUpload : IHttpHandler
    {

        int mFileSize = 0;
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string foldername = "";
                string sFileName = "";
                string sCategoryName = "";
                Int64 imageIndex = 0;
                Int64 nCategoryID = 0;
                foldername = System.Convert.ToString(context.Request.QueryString["id"]);
                sFileName = System.Convert.ToString(context.Request.QueryString["filename"]);
                sCategoryName = System.Convert.ToString(context.Request.QueryString["nCategoryName"]);
                imageIndex = System.Convert.ToInt64(context.Request.QueryString["imageIndex"]);
                nCategoryID = System.Convert.ToInt64(context.Request.QueryString["nCategoryID"]);
                int count = 1;
                var ImageNameArray = new List<string>();
                if (context.Request.Files.Count > 0)
                {
                    HttpFileCollection file = context.Request.Files;
                    for (int i = 0; i < file.Count; i++)
                    {
                        string Serverpath = System.Web.HttpContext.Current.Server.MapPath("~/HotelImagesFolder/");
                        mFileSize = file[i].ContentLength / 1048576;
                        HttpPostedFile filess = file[i];
                        Serverpath = Serverpath + "\\" + foldername + "\\" + sCategoryName;
                        if (!Directory.Exists(Serverpath))
                        {
                            Directory.CreateDirectory(Serverpath);
                        }
                        else
                        {
                            string[] files = Directory.GetFiles(Serverpath);
                            foreach (string fileName in files)
                            {
                                if (fileName.Contains(sFileName.Substring(0, sFileName.LastIndexOf('.'))))
                                {
                                    File.Delete(fileName);
                                }
                            }
                        }
                        string fileDirectory = Serverpath + "\\" + sFileName;
                        filess.SaveAs(fileDirectory);
                        ImageNameArray.Add(sFileName);
                        string sImageName = "sHotelImage" + imageIndex;
                        DBHelper.DBReturnCode retCode = CutAdmin.DataLayer.PackageManager.Update_HotelImages(Convert.ToInt64(foldername), nCategoryID, sCategoryName, sImageName, sFileName);

                        string msg = "{";
                        msg += string.Format("error:'{0}',\n", string.Empty);
                        msg += string.Format("upfile:'{0}'\n", sFileName);
                        msg += "}";
                        context.Response.Write(msg);
                    }

                }
            }

            catch (Exception ex)
            {
                //context.Response.Write("Error: " + ex.Message);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}