﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SightseeingLib
{
    public class ActivityDetails
    {
        public string ActivityID { get; set; }
        public DateTime CheckIN { get; set; }
        public string City { get; set; }
        public string Closing { get; set; }
        public string Country { get; set; }
        public string CurrencyClass { get; set; }
        public string Description { get; set; }
        public TimeSpan Duration { get; set; }
        public Image ListImage { get; set; }
        public List<TourType> List_Type { get; set; }
        public string Name { get; set; }
        public string Opening { get; set; }
        public string Supplier { get; set; }
        public string Tooltip { get; set; }
        public string TotalPrice { get; set; }
        public string Tour_Note { get; set; }
        public long? LocationID { get; set; }
        public LocationDetail LocationDetail { get; set; }
        public string OperationDays { get; set; }
        public int? MinPax { get; set; }
        public int? MaxPax { get; set; }
    }

    public class LocationDetail
    {
        public string LocationName { get; set; }
        public string Latitude { get; set; }
        public string Longitutde { get; set; }
    }


    public class Image
    {

        public int Count { get; set; }
        public bool IsDefault { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
    }

    public class TourType
    {

        public string ID { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public int ActivityCount { get; set; }
    }
    public class SightseeingDetail
    {

    }

    public class RateTypes
    {
        public string Type { get; set; }
    }
    public class DateRange
    {
        public string sDate { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
        public bool Available { get; set; }
        public string fDate { get; set; }
    }

    public class Slots
    {
        public long ID { get; set; }
        public string SlotsStartTime { get; set; }
        public string SlotsEndTime { get; set; }
    }

    public class PaxTyp
    {
        public string Pax_Type { get; set; }
        public string Rate { get; set; }
        public string Age { get; set; }
    }

    public class TicketType
    {
        public string Ticket { get; set; }
        public string TicketID { get; set; }
        public PaxTyp Pax { get; set; }
        public List<PaxTyp> arrayPax { get; set; }
        public string Currency { get; set; }
        public int? PlanId { get; set; }
        public bool? nonRefundable { get; set; }
        public long? CancellationID { get; set; }
        public long? noShowID { get; set; }
        public List<CancellationPolicy> arrCancellationPolicy { get; set; }
        public string Inclusions { get; set; }
        public string Exclusions { get; set; }
    }

    public class CancellationPolicy
    {
        public string RefundType { get; set; }
        public string CancelationPolicy { get; set; }
        public string ChargesType { get; set; }
        public string DaysPrior { get; set; }
        public string PercentageToCharge { get; set; }
        public string AmountToCharge { get; set; }
        public string IsDaysPrior { get; set; }
    }

    public class ActivityPriceRange
    {
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
    }

    public  class BookinDetail
    {
        public string ActivityId { get; set; }
        public string BookDate { get; set; }
        public string SightseeingDate { get; set; }
        public string Name { get; set; }
        public string PassengerName { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public List<Image> ActImage { get; set; }
        public string Adult { get; set; }
        public string Child1 { get; set; }
        public string Child2 { get; set; }
        public string Infant { get; set; }
        public string TotalPax { get; set; }
        public string TotalAmount { get; set; }
        public string BookingID { get; set; }
        public string Status { get; set; }
        public string Location { get; set; }
        public decimal? AdultCost { get; set; }
        public decimal? Child1Cost { get; set; }
        public decimal? Child2Cost { get; set; }
        public decimal? InfantCost { get; set; }
        public string TicketType { get; set; }
        public string Slot { get; set; }
        public string Duration { get; set; }
        public string Inclusions { get; set; }
        public string Exclusions { get; set; }
        public decimal? AddOns { get; set; }
        public decimal? Tax { get; set; }
        public string PricingNote { get; set; }
        public string ImpNote { get; set; }
        public string RateType { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Currency { get; set; }
        public string Child1Age { get; set; }
        public string Child2Age { get; set; }
    }
}
