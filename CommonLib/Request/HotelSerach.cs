﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Request
{
   public class HotelSerach
    {
        public Int64[] HotelCode { get; set; }
        public string Destination { get; set; }
        public string Checkin { get; set; }
        public string Checkout { get; set; }
        public string[] nationality { get; set; }
        public int Nights { get; set; }
        public int Adults { get; set; }
        public int Childs { get; set; }
        public string Supplier { get; set; }
        public string MealPlan { get; set; }
        public string CurrencyCode { get; set; }
        public string AddSearchsession { get; set; }
        public string SearchType { get; set; }
    }
}
