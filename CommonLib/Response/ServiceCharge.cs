﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLib.Response
{
   public class ServiceCharge
    {
        /* Markup & Charge Component */
        public string Type { get; set; }
        /* Hotel Rates & Taxes*/
        public float Rate { get; set; }/*Base Rates*/
        public List<TaxRate> HotelTaxes { get; set; }
        public float HotelRate { get; set; }
        public float RoomRate { get; set; }

        /*ClickUrHotel Charge*/
        public List<TaxRate> CUHTaxes { get; set; }
        public float CUHMarkup { get; set; }
        public float CUTMarkup { get; set; }
        public float CUHTotal { get; set; }

        /*Supplier Charge */
        public List<TaxRate> S2STaxes { get; set; }
        public float S2SMarkup { get; set; }
        public float SupplierTotal { get; set; }

        /*Purchese Charge*/
        public List<TaxRate> b2bTaxes { get; set; }
        public float AdminMarkup { get; set; }
        public float Discount { get; set; }
        public float TotalPrice { get; set; }   /* Supplier Amount*/
    }
}
